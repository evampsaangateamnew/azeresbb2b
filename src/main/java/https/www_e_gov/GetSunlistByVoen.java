
package https.www_e_gov;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import net.x_rd.az.voen9900037691.producer.GetSunlistByVoenRequest;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetSunlistByVoenRequest" type="{http://voen9900037691.az.x-rd.net/producer}GetSunlistByVoenRequest" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getSunlistByVoenRequest"
})
@XmlRootElement(name = "GetSunlistByVoen")
public class GetSunlistByVoen {

    @XmlElement(name = "GetSunlistByVoenRequest")
    protected GetSunlistByVoenRequest getSunlistByVoenRequest;

    /**
     * Gets the value of the getSunlistByVoenRequest property.
     * 
     * @return
     *     possible object is
     *     {@link GetSunlistByVoenRequest }
     *     
     */
    public GetSunlistByVoenRequest getGetSunlistByVoenRequest() {
        return getSunlistByVoenRequest;
    }

    /**
     * Sets the value of the getSunlistByVoenRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetSunlistByVoenRequest }
     *     
     */
    public void setGetSunlistByVoenRequest(GetSunlistByVoenRequest value) {
        this.getSunlistByVoenRequest = value;
    }

}
