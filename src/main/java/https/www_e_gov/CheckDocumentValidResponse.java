
package https.www_e_gov;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import https.wwww_e_gov.ResponseInformationByPin;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CheckDocumentValidResult" type="{https://wwww.e-gov.az}ResponseInformationByPin" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "checkDocumentValidResult"
})
@XmlRootElement(name = "CheckDocumentValidResponse")
public class CheckDocumentValidResponse {

    @XmlElement(name = "CheckDocumentValidResult")
    protected ResponseInformationByPin checkDocumentValidResult;

    /**
     * Gets the value of the checkDocumentValidResult property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseInformationByPin }
     *     
     */
    public ResponseInformationByPin getCheckDocumentValidResult() {
        return checkDocumentValidResult;
    }

    /**
     * Sets the value of the checkDocumentValidResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseInformationByPin }
     *     
     */
    public void setCheckDocumentValidResult(ResponseInformationByPin value) {
        this.checkDocumentValidResult = value;
    }

}
