
package https.www_e_gov;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import https.wwww_e_gov.ResponseApplicationInfo;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AddApplicationResult" type="{https://wwww.e-gov.az}ResponseApplicationInfo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "addApplicationResult"
})
@XmlRootElement(name = "AddApplicationResponse")
public class AddApplicationResponse {

    @XmlElement(name = "AddApplicationResult")
    protected ResponseApplicationInfo addApplicationResult;

    /**
     * Gets the value of the addApplicationResult property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseApplicationInfo }
     *     
     */
    public ResponseApplicationInfo getAddApplicationResult() {
        return addApplicationResult;
    }

    /**
     * Sets the value of the addApplicationResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseApplicationInfo }
     *     
     */
    public void setAddApplicationResult(ResponseApplicationInfo value) {
        this.addApplicationResult = value;
    }

}
