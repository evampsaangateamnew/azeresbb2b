
package https.wwww_e_gov;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfResponseVoenBySunItems complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfResponseVoenBySunItems">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ResponseVoenBySunItems" type="{https://wwww.e-gov.az}ResponseVoenBySunItems" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfResponseVoenBySunItems", propOrder = {
    "responseVoenBySunItems"
})
public class ArrayOfResponseVoenBySunItems {

    @XmlElement(name = "ResponseVoenBySunItems", nillable = true)
    protected List<ResponseVoenBySunItems> responseVoenBySunItems;

    /**
     * Gets the value of the responseVoenBySunItems property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the responseVoenBySunItems property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getResponseVoenBySunItems().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ResponseVoenBySunItems }
     * 
     * 
     */
    public List<ResponseVoenBySunItems> getResponseVoenBySunItems() {
        if (responseVoenBySunItems == null) {
            responseVoenBySunItems = new ArrayList<ResponseVoenBySunItems>();
        }
        return this.responseVoenBySunItems;
    }

}
