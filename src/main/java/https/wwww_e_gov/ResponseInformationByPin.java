
package https.wwww_e_gov;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ResponseInformationByPin complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ResponseInformationByPin">
 *   &lt;complexContent>
 *     &lt;extension base="{https://wwww.e-gov.az}ResponseInformationByDocuments">
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResponseInformationByPin")
public class ResponseInformationByPin
    extends ResponseInformationByDocuments
{


}
