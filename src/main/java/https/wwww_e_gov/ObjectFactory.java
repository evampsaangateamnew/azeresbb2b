
package https.wwww_e_gov;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the https.wwww_e_gov package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: https.wwww_e_gov
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ResponseInformationByVoen }
     * 
     */
    public ResponseInformationByVoen createResponseInformationByVoen() {
        return new ResponseInformationByVoen();
    }

    /**
     * Create an instance of {@link ResponseApplicationInfo }
     * 
     */
    public ResponseApplicationInfo createResponseApplicationInfo() {
        return new ResponseApplicationInfo();
    }

    /**
     * Create an instance of {@link ResponseVoenBySun }
     * 
     */
    public ResponseVoenBySun createResponseVoenBySun() {
        return new ResponseVoenBySun();
    }

    /**
     * Create an instance of {@link ResponseInformationByPin }
     * 
     */
    public ResponseInformationByPin createResponseInformationByPin() {
        return new ResponseInformationByPin();
    }

    /**
     * Create an instance of {@link RequestVoenInformation }
     * 
     */
    public RequestVoenInformation createRequestVoenInformation() {
        return new RequestVoenInformation();
    }

    /**
     * Create an instance of {@link ResponseInformationByDocuments }
     * 
     */
    public ResponseInformationByDocuments createResponseInformationByDocuments() {
        return new ResponseInformationByDocuments();
    }

    /**
     * Create an instance of {@link RequestDocumentInformation }
     * 
     */
    public RequestDocumentInformation createRequestDocumentInformation() {
        return new RequestDocumentInformation();
    }

    /**
     * Create an instance of {@link RequestApplicationTypes }
     * 
     */
    public RequestApplicationTypes createRequestApplicationTypes() {
        return new RequestApplicationTypes();
    }

    /**
     * Create an instance of {@link ResponseVoenBySunItems }
     * 
     */
    public ResponseVoenBySunItems createResponseVoenBySunItems() {
        return new ResponseVoenBySunItems();
    }

    /**
     * Create an instance of {@link RequestApplication }
     * 
     */
    public RequestApplication createRequestApplication() {
        return new RequestApplication();
    }

    /**
     * Create an instance of {@link RequestSellerInformation }
     * 
     */
    public RequestSellerInformation createRequestSellerInformation() {
        return new RequestSellerInformation();
    }

    /**
     * Create an instance of {@link ResponseDocumentInfo }
     * 
     */
    public ResponseDocumentInfo createResponseDocumentInfo() {
        return new ResponseDocumentInfo();
    }

    /**
     * Create an instance of {@link AddressDetail }
     * 
     */
    public AddressDetail createAddressDetail() {
        return new AddressDetail();
    }

    /**
     * Create an instance of {@link ResponseStatus }
     * 
     */
    public ResponseStatus createResponseStatus() {
        return new ResponseStatus();
    }

    /**
     * Create an instance of {@link RequestAuthentication }
     * 
     */
    public RequestAuthentication createRequestAuthentication() {
        return new RequestAuthentication();
    }

    /**
     * Create an instance of {@link ArrayOfResponseVoenBySunItems }
     * 
     */
    public ArrayOfResponseVoenBySunItems createArrayOfResponseVoenBySunItems() {
        return new ArrayOfResponseVoenBySunItems();
    }

    /**
     * Create an instance of {@link ResponseVoenInfo }
     * 
     */
    public ResponseVoenInfo createResponseVoenInfo() {
        return new ResponseVoenInfo();
    }

}
