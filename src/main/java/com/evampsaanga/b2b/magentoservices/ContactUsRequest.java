package com.evampsaanga.b2b.magentoservices;

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;

public class ContactUsRequest extends BaseRequest {
	String storeId = "";

	/**
	 * @return the storeId
	 */
	public String getStoreId() {
		return storeId;
	}

	/**
	 * @param storeId
	 *            the storeId to set
	 */
	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}
}
