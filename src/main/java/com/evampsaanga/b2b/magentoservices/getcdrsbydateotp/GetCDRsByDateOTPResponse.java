package com.evampsaanga.b2b.magentoservices.getcdrsbydateotp;

import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;

public class GetCDRsByDateOTPResponse extends BaseResponse {
	String pin = "";

	/**
	 * @return the pin
	 */
	public String getPin() {
		return pin;
	}

	/**
	 * @param pin
	 *            the pin to set
	 */
	public void setPin(String pin) {
		this.pin = pin;
	}
}
