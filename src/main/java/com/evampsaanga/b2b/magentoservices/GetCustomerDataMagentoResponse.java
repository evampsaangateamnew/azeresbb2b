package com.evampsaanga.b2b.magentoservices;

import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;

public class GetCustomerDataMagentoResponse extends BaseResponse {
	public GetCustomerDataMagentoResponse() {
		super();
	}

	com.evampsaanga.b2b.magentoservices.CustomerDataMagento customerData = new com.evampsaanga.b2b.magentoservices.CustomerDataMagento();

	/**
	 * @return the customerData
	 */
	public com.evampsaanga.b2b.magentoservices.CustomerDataMagento getCustomerData() {
		return customerData;
	}

	/**
	 * @param customerData
	 *            the customerData to set
	 */
	public void setCustomerData(com.evampsaanga.b2b.magentoservices.CustomerDataMagento customerData) {
		this.customerData = customerData;
	}
}
