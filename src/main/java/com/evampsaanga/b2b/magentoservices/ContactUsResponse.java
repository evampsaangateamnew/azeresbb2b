package com.evampsaanga.b2b.magentoservices;

import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ContactUsResponse extends BaseResponse {
	@JsonProperty("data")
	private Data data;

	/**
	 * @return the data
	 */
	public Data getData() {
		return data;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	public void setData(Data data) {
		this.data = data;
	}
}
