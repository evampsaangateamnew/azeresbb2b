package com.evampsaanga.b2b.magentoservices;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "address", "phone", "fax", "email", "website", "customerCareNo", "facebookLink", "twitterLink",
		"googleLink", "youtubeLink", "linkedinLink" })
public class Data {
	@JsonProperty("address")
	private String address = "";
	@JsonProperty("phone")
	private String phone = "";
	@JsonProperty("fax")
	private String fax = "";
	@JsonProperty("email")
	private String email = "";
	@JsonProperty("website")
	private String website = "";
	@JsonProperty("customerCareNo")
	private String customerCareNo = "";
	@JsonProperty("facebookLink")
	private String facebookLink = "";
	@JsonProperty("twitterLink")
	private String twitterLink = "";
	@JsonProperty("googleLink")
	private String googleLink = "";
	@JsonProperty("youtubeLink")
	private String youtubeLink = "";
	@JsonProperty("linkedinLink")
	private String linkedinLink = "";
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("address")
	public String getAddress() {
		return address;
	}

	@JsonProperty("address")
	public void setAddress(String address) {
		this.address = address;
	}

	@JsonProperty("phone")
	public String getPhone() {
		return phone;
	}

	@JsonProperty("phone")
	public void setPhone(String phone) {
		this.phone = phone;
	}

	@JsonProperty("fax")
	public String getFax() {
		return fax;
	}

	@JsonProperty("fax")
	public void setFax(String fax) {
		this.fax = fax;
	}

	@JsonProperty("email")
	public String getEmail() {
		return email;
	}

	@JsonProperty("email")
	public void setEmail(String email) {
		this.email = email;
	}

	@JsonProperty("website")
	public String getWebsite() {
		return website;
	}

	@JsonProperty("website")
	public void setWebsite(String website) {
		this.website = website;
	}

	@JsonProperty("customerCareNo")
	public String getCustomerCareNo() {
		return customerCareNo;
	}

	@JsonProperty("customerCareNo")
	public void setCustomerCareNo(String customerCareNo) {
		this.customerCareNo = customerCareNo;
	}

	@JsonProperty("facebookLink")
	public String getFacebookLink() {
		return facebookLink;
	}

	@JsonProperty("facebookLink")
	public void setFacebookLink(String facebookLink) {
		this.facebookLink = facebookLink;
	}

	@JsonProperty("twitterLink")
	public String getTwitterLink() {
		return twitterLink;
	}

	@JsonProperty("twitterLink")
	public void setTwitterLink(String twitterLink) {
		this.twitterLink = twitterLink;
	}

	@JsonProperty("googleLink")
	public String getGoogleLink() {
		return googleLink;
	}

	@JsonProperty("googleLink")
	public void setGoogleLink(String googleLink) {
		this.googleLink = googleLink;
	}

	@JsonProperty("youtubeLink")
	public String getYoutubeLink() {
		return youtubeLink;
	}

	@JsonProperty("youtubeLink")
	public void setYoutubeLink(String youtubeLink) {
		this.youtubeLink = youtubeLink;
	}

	@JsonProperty("linkedinLink")
	public String getLinkedinLink() {
		return linkedinLink;
	}

	@JsonProperty("linkedinLink")
	public void setLinkedinLink(String linkedinLink) {
		this.linkedinLink = linkedinLink;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}
}
