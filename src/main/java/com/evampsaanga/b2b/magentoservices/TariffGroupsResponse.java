package com.evampsaanga.b2b.magentoservices;

import java.util.List;

import com.evampsaanga.b2b.azerfon.magento.tariffdetails.Data;
import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;

public class TariffGroupsResponse extends BaseResponse{
	 public List<Data> getData() {
		return data;
	}

	public void setData(List<Data> data) {
		this.data = data;
	}

	private List<Data> data = null;

}
