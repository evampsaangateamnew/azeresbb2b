package com.evampsaanga.b2b.configs;

public class ResponseCodes {

	public static final String TARIFF_DETAILS_VERSION_2_SUCCESSFUL="272";
	public static final String THIRD_PARTY_LOAN_STATUS_CODE_OK = "OK";
	public static final String THIRD_PARTY_LOAN_STATUS_CODE_NO_HISTORY = "NoHistory";
	public static final String NO_HISTORY_DESCRIPTION = "No History";
	public static final String THIRD_PARTY_LOAN_STATUS_CODE_INVALID_SUBSCRIBER_ID = "InvalidSubscriberID";
	public static final String INVALID_SUBSCRIBER_ID_CODE = "180";
	public static final String INVALID_SUBSCRIBER_ID_DESCRIPTION = "Invalid Subscriber ID";
	public static final String THIRD_PARTY_LOAN_STATUS_CODE_DATE_INTERVAL_OUT_OF_RANGE = "DateIntervalOutOfRange";
	public static final String DATE_INTERVAL_OUT_OF_RANGE_CODE = "181";
	public static final String DATE_INTERVAL_OUT_OF_RANGE_DESCRIPTION = "Date Interval Out Of Range";
	

	public static final String THIRD_PARTY_FAILURE="Third Party Call Failure";
	public static final String EMPTY_PASSWORD_CODE="7";
	public static final String EMPTY_PASSWORD_MSG="Empty Password from Magento";
	public static final String GROUP_RESPONSE_MSG="No Groups Against This MSISDN";
	

	public static final String GENERIC_ERROR_DES = "sorry, due to connectivity problems, the last command could not be completed";
	public static final String GENERIC_ERROR_CODE = "500";
	public static final String CONNECTIVITY_PROBLEM_DES = "sorry, due to connectivity problems, the last command could not be completed";
	public static final String CONNECTIVITY_PROBLEM_CODE = "50";
	public static final String MSISDN_NOT_FOUND_BK_CODE = "1211000402";
	public static final String MSISDN_NOT_FOUND_BK_DES = "MSISDN does not exist";
	public static final String MISSING_PARAMETER_CODE = "199";
	public static final String START_DATE_CANNOT_BE_IN_FUTURE_CODE = "141";
	public static final String END_DATE_CANNOT_BE_IN_FUTURE_CODE = "142";
	public static final String START_DATE_GRATER_END_DATE_CODE = "143";
	public static final String START_DATE_CANNOT_BE_IN_FUTURE_DES = "Start date cannot be in future.";
	public static final String END_DATE_CANNOT_BE_IN_FUTURE_DES = "End date cannot be in future.";
	public static final String START_DATE_GRATER_END_DATE_DES = "Start date cannot be greater then end date";

	public static final String START_DATE_INVALID_CODE = "144";
	public static final String START_DATE_INVALID_DES = "Invalid Start date format,  Should be in yyyy-MM-dd format";
	public static final String END_DATE_INVALID_CODE = "145";
	public static final String END_DATE_INVALID_DES = "Invalid End date format,  Should be in yyyy-MM-dd format";

	public static final String MISSING_PARAMETER_DES = "one of more parameter is missing or of invalid type";
	public static final String ERROR_401_CODE = "401";
	public static final String ERROR_401 = "access Not authorized";
	public static final String REPORT_LOST_SIM_OUTSTANDING_BALANCE_CODE = "150";
	public static final String REPORT_LOST_SIM_OUTSTANDING_BALANCE_MSG = "User have some outstanding debt";
	public static final String REPORT_LOST_SIM_RESTRICTION_CODE="151";
	public static final String REPORT_LOST_SIM_INVALID_CODE_MSG="Reason Code is invalid";
	public static final String REPORT_LOST_SIM_RESTRICTION_MSG="Suspend Permission not allowed";
	public static final String REPORT_LOST_SIM_GROUP_NAME_CODE="152";
	public static final String REPORT_LOST_SIM_GROUP_NAME_CODE_MSG="No Group Name Against This MSISDN";
	public static final String REPORT_LOST_SIM_NEGATIVE_BALANCE_CODE="153";
	public static final String REPORT_LOST_SIM_NEGATIVE_BALANCE_MSG="User have negative balance";
	
	public static final String ERROR_MSISDN = "Not a valid msisdn number";
	public static final String ERROR_MSISDN_CODE = "-1";
	public static final String ERROR_400 = "Bad Request";
	public static final String ERROR_400_CODE = "400";
	public static final String SUCESS_DES_200 = "Sucessfull";
	public static final String SUCESS_CODE_200 = "200";
	public static final String CALL_FORWARD_TO_SAME_NUMBER_CODE = "1300";
	public static final String CALL_FORWARD_TO_SAME_NUMBER_DES = "You cannot forward to signed up number.";
	public static final String CALL_FORWARD_TO_INVALID_NUMBER_CODE = "1301";
	public static final String CALL_FORWARD_TO_INVALID_NUMBER_DES = "Number is incorrect.";
	public static final String INTERNAL_SERVER_ERROR_CODE = "500";
	public static final String INTERNAL_SERVER_ERROR_DES = "Internal Server error please report to authority";
	public static final String MAGENTO_SERVER_ERROR_CODE = "402";
	public static final String MAGENTO_SERVER_ERROR_DES = "Magento Server issue";
	public static final String CUSTOMER_TYPE_ERROR_CODE = "5001";
	public static final String CUSTOMER_TYPE__ERROR_DES = "customer type prepaid/postpaid not defined";
	public static final String CUSTOMER_STATUS_ERROR_CODE = "5002";
	public static final String CUSTOMER_STATUS_ERROR_DES = "Forgot password allowed only in Active state and B1W.";
	public static final String INVALID_AMOUNT_ERROR_CODE = "4444";

	public static final String TRANSFERER_AND_TRANSFREE_SHOUULD_BE_DIFFERENT_CODE = "1200";
	public static final String TRANSFERER_AND_TRANSFREE_SHOUULD_BE_DIFFERENT_DES = "You cannot transfer money to signed up number.";

	public static final String INVALID_AMOUNT_ERROR_DES = "Invalid Amount";
	public static final String INSUFFICIENT_BALANCE_ERROR_CODE = "4443";
	public static final String INSUFFICIENT_BALANCE_ERROR_DES = "4443";
	public static final String SMS_LIMIT_CODE_199 = "199";
	public static final String SMS_LIMIT_CODE_198 = "198";
	public static final String MONEY_TRANSFER_TO_POSTPAID_CODE = "11";
	public static final String MONEY_TRANSFER_TO_POSTPAID_DES = "Transfer to postpaid user not allowed";

	// for magento phase2
	public static final String TARIF_DETAILS_VERSION_2_SUCCESSFUL = "272";

	// unable to send sms
	public static final String UNSUCCESS_CODE = "444";
	public static final String UNSUCCESS_DESC = "Unable to Send SMS. Check Reciever MSISDN:";
	public static final String UNSUCCESS_DESC_CS = "Balance Is Not Suffient To Activate The Requested Offer";

	public static final String UNSUCCESS_CODE_CANCEL = "445";
	public static final String UNSUCCESS_CANCEL_DESC = "No Orders Found";

	public static final String NO_FAILED_CODE = "446";
	public static final String NO_FAILED_DES = "No Failed Orders Are Found!";

	// ulduzum code generate error code
	public static final String CODE_GENERATE_ERROR_CODE = "447";

	// getcdrssummar (usagehistory) , getOperationShitsoty
	public static final String ERROR_CODE_SERVICE_UNAVAILABLE_AT_12AM_TO_5AM = "448";
	public static final String ERROR_MESSAGE_SERVICE_UNAVAILABLE_AT_12AM_TO_5AM = "Sory this Service is Unavailable Betweeen 12:00 A.M to 5:00 A.M";

	public static final String PASSWORD_MATCH_FAILED_CODE = "43";
	public static final String PASSWORD_MATCH_FAILED_DESCRIPTION = "Wrong Password Attempt";
	public static final String USER_NOT_FOUND_CODE = "44";
	public static final String USER_NOT_FOUND_DESCRIPTION = "User Not Found";

	public static final String USER_CEARED_FROM_CACHE_DESCRIPTION = "User cleared From Cache and below is the information of that user";
	public static final String PASSWORD_AUTHENTICATION_FAILED = "104";
	public static final String PASSWORD_AUTHENTICATION_FAILED_MSG = "Password authentication failed";
	public static final String LOGIN_ATTEMPT_FAILED = "45";
	public static final String LOGIN_ATTEMPT_FAILED_DESCRIPTION = "Password attempt failed for 3 times";
	public static final String USER_DOESNOT_EXIST_CODE = "06";
	public static final String USER_DOESNOT_EXIST_DESC = "User does not exist";
	public static final String USER_ALREADY_EXIST_CODE = "03";
	public static final String USER_ALREADY_EXIST_DESC = "User already exist";

	// ChangePayment Relation b2B bulk, response codes
	public static final String LOWER_LIMIT_LESS_THAN_MRC_OR_BALANCE_ERROR_CODE = "450";
	public static final String LOWER_LIMIT_LESS_THAN_MRC_OR_BALANCE_ERROR_DESCRIPTION = "Your limit is Less than the Corporate Balance or current MRC";

	public static final String MAX_LIMIT_GREATER_THAN_MRC_OR_COMAPNY_VALUE_ERROR_CODE = "451";
	public static final String MAX_LIMIT_GREATER_THAN_MRC_OR_COMAPNY_VALUE_ERROR_DESCRIPTION = "Your Maximum should be greater than 0 or less than the CompanyValue+current MRC";

	// Exchange Service Response codes
	public static final String MBS_GREATER_THAN_FREEUNIT_DATA_CODE = "452";
	public static final String MBS_GREATER_THAN_FREEUNIT_DATA_DESC = "Free Resources MBs you want to convert cannot be greater than the free unit Data";
	public static final String MINUTES_GREATER_THAN_FREEUNIT_MINUTES_CODE = "453";
	public static final String MINUTES_GREATER_THAN_FREEUNIT_MINUTES_DESC = "Free Resources Minutes you want to convert cannot be greater than the free unit Minutes";

	public static final String MINUTES_MBs_EMPT_FROM_AZERFON_FREEUNIT_CODE = "454";
	public static final String MINUTES_MBs_EMPT_FROM_AZERFON_FREEUNIT_DESC = "Minutes or MBs from Free Unit should not be empty";

	public static final String EXCHANGE_SERVICE_RECORD_DOESNT_EXIST_CODE = "455";
	public static final String EXCHANGE_SERVICE_RECORD_DOESNT_EXIST_DESC = "Record for this msisdn Doesnt not exist";

	public static final String EXCHNAGE_SERVICE_NOT_ELIGITBLE_CODE = "456";
	public static final String EXCHNAGE_SERVICE_NOT_ELIGITBLE_DESC = "Sorry your are already consumed this service 5 times within 1 month";

	public static final String FNF_FIRST_ADD_CODE = "457";
	public static final String FNF_FIRST_ADD_CODE_DESC = "Success Response with first addition of number";

	public static final String NARTV_THIRDPARY_RESPONSE_NULL_CODE = "458";
	public static final String NARTV_THIRDPARY_RESPONSE_NULL_DESC = "Response is null from thirParty";

	public static final String NARTV_THIRDPARY_RESPONSE_DATA_NULL_CODE = "460";
	public static final String NARTV_THIRDPARY_RESPONSE_DATA_NULL_DESC = "Authorization Data is null or Empty from thirParty";

	public static final String NARTV_THIRDPARY_RESPONSE_DATA_NULL_SUBSCRIBER_CODE = "461";
	public static final String NARTV_THIRDPARY_RESPONSE_DATA_NULL_SUBSCRIBER_DESC = "Subscriber Data is null or Empty from thirParty";

	public static final String NARTV_THIRDPARY_RESPONSE_DATA_NULL_SUBSCRIPTIONS_CODE = "462";
	public static final String NARTV_THIRDPARY_RESPONSE_DATA_NULL_SUBSCRIPTIONS_DESC = "Subscriptions Data is null or Empty from thirParty";

	public static final String NAR_TV_THIRD_PARTY_RESPONSE_NO_ACTIVE_PLAN_CODE = "1411";
	public static final String NAR_TV_THIRD_PARTY_RESPONSE_NO_ACTIVE_PLAN_DESC = "No Active Plan";
	
	public static final String NAR_TV_THIRD_PARTY_RESPONSE_STATE_NOT_SUCCESS_CODE = "1412";
	public static final String NAR_TV_THIRD_PARTY_RESPONSE_STATE_NOT_SUCCESS_DESC = "Response State Not Success";

	public static final String SUBSCRIBER_NOT_FOUND_CODE = "1412";
	public static final String SUBSCRIBER_NOT_FOUND_DESC = "msisdn is not Azerfon Subscriber";

	
	public static final String OFFNET_SMS_NOTALLOWED_CODE = "463";
	public static final String OFFNET_SMS_NOTALLOWED_DESC = "Offfnet SMS not allowed";
	
	public static final String BALANCE_LESS_THAN_REQUIRED_CODE = "464";
	public static final String BALANCE_LESS_THAN_REQUIRED_DESC = "Balance is less than 0.20 Azn";
	
	public static final String INVALID_CSV_MSG="CSV file is invalid";
	public static final String CHANGE_LIMIT_MSG="Offering id not found";
	
	

}
