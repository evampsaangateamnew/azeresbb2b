package com.evampsaanga.b2b.configs;

public class Config {

	// public static final String CONF_HOME =
	// "D:\\servicetest\\src\\main\\resources\\";

	public static final String CONF_HOME = "/opt/jboss-fuse/conf/";
	public static final String NGBSS_API_PROPERTIES = CONF_HOME + "ngbss_api.properties";
	public static final String MAGENTO_PROPERTIES_FILE = CONF_HOME + "magento_access.properties";
	public static final String APPSERVER_PROPERTIES_FILE = CONF_HOME + "appserver_access.properties";
	public static final String CORE_SERVICES_PROPERTIES_FILE = CONF_HOME + "coreservices.properties";
	public static final String CORE_SERVICES_PROPERTIES_FILE_AZ = CONF_HOME + "coreservices-az.properties";
	public static final String CORE_SERVICES_PROPERTIES_FILE_RU = CONF_HOME + "coreservices-ru.properties";
	public static final String REGX_PROPETIES_FILE = CONF_HOME + "validator.properties";
	public static final String DATABASE_FILE = CONF_HOME + "database.properties";
	public static final String MAPPING = CONF_HOME + "mapping.properties";
	public static final String ERROR_CODE_MAPPING = CONF_HOME + "errorcode.properties";
	public static final String QUEUE_PROPERTIES_NAME = CONF_HOME + "queue.properties";
	public static final String USERS_DATA_FILE= CONF_HOME+"B2B_F_SUB.csv";
	public static final String USERS_DATA_FILE_BACK_UP= CONF_HOME+"B2B_F_SUB_BACKUP.csv";

	public static final String WSDL_BASE_PATH = "file:/opt/jboss-fuse/wsdls/";
	public static final String SADM_WSDL_PATH = WSDL_BASE_PATH + "SADM_ProvisioningService_ECare.wsdl";
	public static final String ORDER_HANDLE_WSDL_PATH = WSDL_BASE_PATH + "OrderHandle.wsdl";
	public static final String HLRWEB_WSDL_PATH = WSDL_BASE_PATH + "HWBSS_HLRWEB_V10.wsdl";
	public static final String USSD_WSDL_PATH = WSDL_BASE_PATH + "HWBSS_USSDGateWay_V1.0.wsdl";
	public static final String CRM_WSDL_PATH = WSDL_BASE_PATH + "OrderQuery.wsdl";
	public static final String CRM_AZER_WSDL_PATH = WSDL_BASE_PATH + "azerwsdls/OrderQueryAZCtz.wsdl";

	// public static final String CBS_AR_WSDL_PATH = WSDL_BASE_PATH +
	// "CBSInterface_AR_Services_ECare.wsdl";

	public static final String CBS_AR_WSDL_PATH = WSDL_BASE_PATH + "CBSInterface_AR_Services.wsdl";
	public static final String CBS_BB_WSDL_PATH = WSDL_BASE_PATH + "CBSInterface_BB_Services_ECare.wsdl";
	public static final String CBS_BC_WSDL_PATH_SUBLIFE = WSDL_BASE_PATH
			+ "cbsnewwsdl/CBSInterface_BC_Services_ECareNew.wsdl";
	public static final String CBS_BC_WSDL_PATH = WSDL_BASE_PATH + "CBSInterface_BC_Services_ECare.wsdl";
	public static final String HWBSS_MNP_WSDL_PATH = WSDL_BASE_PATH + "HWBSS_MNP_V10_For_ECare.wsdl";
	public static final String HWBSS_SUBSCRIBER_WSDL_PATH = WSDL_BASE_PATH + "HWBSS_Subscriber_V10.wsdl";
	public static final String ORDER_QUERY_WSDL_PATH = WSDL_BASE_PATH + "OrderQuery.wsdl";
	public static final String ORDER_QUERY_4DPC_WSDL_PATH = WSDL_BASE_PATH + "mnp/OrderQuery4DPC.wsdl";
	public static final String CHARGE_SUSCRIBER_WSDL_PATH = WSDL_BASE_PATH + "chargesubscriber/HWBSS_ChargeSubscriber_V10_1.wsdl";
	
}
