package com.evampsaanga.b2b.configs;

import org.apache.log4j.Logger;

public class Constants {

	// NAR TV
	public static final String NAR_TV_SUBSCRIBER_STATE = "ACTIVE";
	public static final String NAR_TV_SUBSCRIBER_STATUS_TRUE = "true";
	public static final String NAR_TV_SUBSCRIBER_STATUS_FALSE = "false";
	public static final String NAR_TV_SUBSCRIBE_STATE_SUCCESS = "SUCCESS";

	// LOAN
	public static final String SUBSCRIBER_ID_PREFIX = "994";
	public static final String USE_CASE_REQUEST_HISTORY = "requestHistory";
	public static final String USE_CASE_PAYMENT_HISTORY = "paymentHistory";
	public static final String STATUS_PAID = "Paid";
	public static final String STATUS_IN_PROGRESS = "In Progress";

	public static final String SQL_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";
	public static final String SQL_DATE_FORMAT_TRIMMED = "yyyyMMddHHmmss";
	public static final String SQL_DATE_FORMAT_required = "yyyy-MM-dd HH:mm:ss";
	public static final String SQL_DATE_FORMAT_TRIMMED_Request = "dd/MM/yyyy";
	public static final String SQL_DATE_FORMAT_History = "dd/MM/yy HH:mm:ss";
	public static final String VOICE = "VOICE";
	public static final String SMS = "SMS";
	public static final String DATA = "DATA";
	public static final String OTHER = "OTHERS";
	public static final String M2MBalance = "M2MBALANCE";
	public static final String ROAMING_VOICE = "RVOICE";
	public static final String ROAMING_SMS = "RSMS";
	public static final String ROAMING_DATA = "RDATA";
	public static final String PREPAID = "prepaid";
	public static final String POSTPAID = "postpaid";
	public static final String CREDENTIALS = "no%$Passw@@#$@#$#$";
	public static final String CREDENTIALSUNCODED = "RQ/Rjcf8Gn2+7srAiXNO0ZGuRnJORSir";
	public static final long MONEY_DIVIDEND = 100000;
	public static final String AZERI_COUNTRY_CODE = "994";
	public static final Double INSTALLMENTS_FEE_LIMIT = 10.00;
	public static final String CURRENCY_AZN = "AZN";
	public static final String QUERY_SUBSCRIBER_LIFE_CYCLE_STATUS_B1W = "Block one way";
	public static final String QUERY_SUBSCRIBER_LIFE_CYCLE_STATUS_B2W = "Block two way";
	public static final String QUERY_SUBSCRIBER_LIFE_CYCLE_STATUS_EXP = "To be expired";
	public static final String MRC_LIMIT = "3";
	public static final String MRC_TYPE_DAILY = "daily";
	public static final String MRC_TYPE_WEEKLY = "weekly";
	public static final String MRC_TYPE_MONTHLY = "monthly";
	public static final String LOAN_STATUS_OPEN = "O";
	public static final String LOAN_STATUS_CLOSED = "C";
	public static final String CUSTOMER_TYPE_PREPAID = "prepaid";
	public static final String CUSTOMER_TYPE_POSTPAID = "post paid";
	public static final String I_AM_BACK_OFFERING_ID = "102341307";
	public static final String I_CALLED_YOU_OFFERING_ID = "519637024";
	public static final String I_AM_BUSY = "1479418507";
	public static final String CALL_FORWARDING_OFFERING_ID = "519637024";
	public static final String CORE_SERVICE_ACTIVE_STATUS = "C01";
	public static final String CIN_BRAND_ID = "1970006532";
	public static final String KLASS_BRAND_ID = "1770078090";
	public static final String WTTX_PREPAID_OFFERINGID = "900629902";
	public static final String GUNBOUY_OFFERING_ID = "1770083072";
	public static final String USER_TYPE_PREPAID = "prepaid";
	public static final String USER_TYPE_POSTPAID = "postpaid";
	public static final String MRC_STATUS_PAID = "Paid";
	public static final String MRC_STATUS_UNPAID = "Unpaid";
	public static final String COUNTRY_WIDE = "Countrywide";
	public static final String BONUS_WALLET = "Bonus Wallet";
	public static final String MAIN_WALLET = "Main Wallet";
	public static final String SCENARIO_NAME = "Default";
	public static final String TARIFF_CHANGE_ACTION_RENEWAL = "renewal";
	public static final String CRMSUBACCESSCODE = "0";
	public static final String TRANSLATION_CDRS_KEY_SEPARATOR = "@";
	public static final String MAGENTO_CATALOGUE_THIRD_PARTY_NAME = "Catalogue";
	/**
	 * Below block defines variables for Core Services API definition
	 */
	public static final String ORDER_TYPE = "CO025";
	public static final String ORDER_ITEM_TYPE = "CO025";
	public static final String CUSTOMER_NOTIFICATION = "0";
	public static final String PARAMETER_NOTIFICATION = "0";
	public static final String EFFECTIVE_MODE = "0";
	public static final String ACTION_MODE = "A";
	public static final String EXTERNAL_PARAMETER_INFO_PARAM_NAME = "26650";
	public static final String TENANT_ID = "101";
	/**
	 * Below block defines variables for ChangeLanguage API definition
	 */
	public static final String VERSION = "1";
	public static final String TECHNICAL_CHANEL_ID = "53";
	public static final String CL_ORDER_ITEM = "CO019";
	public static final String CL_ORDER_ITEM_TYPE = "CO019";
	public static final String NETWORK_SETTINGS_ORDER_ITEM_TYPE = "CO042";

	/**
	 * Below block defines variables for USSD API definition
	 */
	public static final String USSD_IS_CONFIRMED = "Y";
	/**
	 * Below block defines variables for Change Tariff API definition
	 */
	public static final String CHANGE_TARIFF_ORDER_TYPE = "CO024";
	public static final String CHANGE_TARIFF_ORDER_ITEM_TYPE = "CO024";
	public static final String CHANGE_TARIFF_CUSTOMER_NOTIFICATION = "0";
	public static final String CHANGE_TARIFF_PARAMETER_NOTIFICATION = "0";
	public static final String CHANGE_TARIFF_EFFECTIVE_MODE = "0";
	public static final String CHANGE_TARIFF_ACTION_MODE = "A";
	public static final String CHANGE_TARIFF_ACTION_TYPE = "9";
	public static final String CHANGE_TARIFF_EXTERNAL_PARAMETER_INFO_PARAM_NAME = "509703";
	/**
	 * Below block defines variables for Loan Log API definition
	 */
	public static final String LL_VERSION = "1";
	public static final String LL_BUSINESS_CODE = "1";
	public static final String LL_BEID = "101";
	public static final String LL_BRID = "101";
	public static final String LL_CHANNEL_ID = "1";
	public static final String LL_OPERATOR_ID = "101";
	public static final String BEGIN_ROW_NUM = "0";
	public static final String FETCH_ROW_NUM = "100";
	public static final String TOTAL_ROWNUM = "300";
	/**
	 * Below block defines variables for Report Lost API definition
	 */
	public static final String RL_VERSION = "1";
	public static final String RL_TECHNICAL_CHANEL_ID = "53";
	public static final String RL_ORDER_ITEM = "CO068";
	public static final String RL_ORDER_ITEM_TYPE = "CO068";
	public static final String RL_REASON_CODE = "SC999";
	public static final String RL_REASON_TYPE = "RT002";
	public static final String RL_OPERATOR_TYPE = "5";
	/**
	 * Below block defines variables for Manipulate FNF API definition
	 */
	public static final String FNF_ACTION_TYPE = "2";
	public static final String FNF_ORDER_ITEM = "CO075";
	public static final String FNF_ORDER_ITEM_TYPE = "CO075";
	public static final String FNF_PODUCT_ID = "1023";
	public static final String FNF_SELECT_FLAG = "1";
	public static final String FNF_INFO = "C_FNINFO";
	public static final String FNF_SERIAL_NUMBER = "C_FN_SERIAL_NO";
	public static final String FNF_NUMBER = "C_FN_NUMBER";
	/**
	 * Below block defines variables for Query Subscriber Life Cycle API
	 * definition
	 */
	public static final String QSL_VERSION = "1";
	public static final String QSL_BEID = "101";
	public static final String QSL_BRID = "101";
	public static final String QSL_BUSINESS_CODE = "1";
	public static final String QSL_OPERATOR_ID = "101";
	/**
	 * Below block defines variables for BBService Query Free Unit API
	 * definition
	 */
	public static final String QF_VERSION = "ensESB";
	public static final String QF_BRID = "101";
	public static final String QF_BEID = "101";
	/**
	 * Below block defines variables for TOPUP Post paid API definition
	 */
	public static final String TOPUP_POSTPAID_VERSION = "1";
	public static final String TOPUP_POSTPAID_BUSINESS_CODE = "PAYMENT";
	public static final String TOPUP_POSTPAID_BEID = "101";
	public static final String TOPUP_POSTPAID_BRID = "101";
	public static final String TOPUP_POSTPAID_OPERATORID = "101";
	public static final String TOPUP_POSTPAID_CHANNELID = "41";
	public static final String TOPUP_POSTPAID_ACCESS_MODE = "3";
	public static final String TOPUP_POSTPAID_LANGUAGE_CODE = "2002";
	public static final String TOPUP_POSTPAID_TIME_TYPE = "2";
	public static final String TOPUP_POSTPAID_TIME_ZONEID = "2020";
	public static final String TOPUP_POSTPAID_PAYMENT_CHANNELID = "3";
	public static final String TOPUP_POSTPAID_OPERATION_TYPE = "2";
	public static final String TOPUP_POSTPAID_PAY_TYPE = "2";
	/**
	 * Below block defines variables for Money Transfer API definition
	 */
	public static final String MONEY_TRANSFER_VERSION = "1";
	public static final String MONEY_TRANSFER_BUSINESS_CODE = "1";
	public static final String MONEY_TRANSFER_BEID = "101";
	public static final String MONEY_TRANSFER_BRID = "101";
	public static final String MONEY_TRANSFER_OPERATORID = "101";
	public static final String MONEY_TRANSFER_TYPE = "2";
	public static final String MONEY_TRANSFER_SRC_BALANCE_TYPE = "C_MAIN_ACCOUNT";
	public static final String MONEY_TRANSFER_DEST_BALANCE_TYPE = "C_MAIN_ACCOUNT";
	/**
	 * Below block defines variables for CRMr API definition
	 */
	public static final String CRM_SUBSCRIBER_INCLUDE_OFFERING = "0";

	public static final String MY_SUBSCRIPTIONS_FREE_USAGE_INTERNET = "511";
	public static final String MY_SUBSCRIPTIONS_FREE_USAGE_HYBRID = "512";
	public static final String MY_SUBSCRIPTIONS_FREE_USAGE_CALL = "513";
	public static final String MY_SUBSCRIPTIONS_FREE_USAGE_SMS = "514";
	public static final String MY_SUBSCRIPTIONS_FREE_USAGE_TM = "515";
	public static final String MY_SUBSCRIPTIONS_FREE_USAGE_CAMPAIGN = "516";
	public static final String MY_SUBSCRIPTIONS_FREE_USAGE_ROAMING = "517";
	/**
	 * NGBSS API SUCCESS CODE
	 */
	public static final String CRM_SUCCESS_CODE = "0";
	/**
	 * Subscriber type mapping for getCustomer and Get Subscriber API
	 */
	public static final String INDIVIDUAL_CUSTOMER = "0";
	public static final String CORPORATE_CUSTOMER = "1";

	/**
	 * Below are the variables used for identifying order type
	 */
	public static final String BROADCAST_SMS = "S001";
	public static final String BROADCAST_SMS_TYPE = "BroadcastSMS B2B";
	public static final String CORE_SERVICES = "P002";
	public static final String CORE_SERVICES_TYPE = "Process Core Services B2B";
	public static final String CHANGE_SUPPLEMENTARY = "C003";
	public static final String CHANGE_SUPPLEMENTARY_TYPE = "Change Supplementary Offerings B2B";
	public static final String RETRY_FAILED = "R004";
	public static final String RETRY_FAILED_TYPE = "Retry Failed Order B2B";
	public static final String CANCEL_PENDING = "U005";
	public static final String CHANGEL_TARIFF = "CH06";
	public static final String CHANGEL_TARIFF_TYPE = "Change Tariff B2B";
	public static final String CHANGEL_PAYMENT_RELATION_KEY = "CPR007";
	public static final String CHANGEL_PAYMENT_RELATION = "Change Payment Relation B2B";

	public static final String CHANGEL_GROUP = "CG008";
	public static final String CHANGEL_GROUP_TYPE = "Change Group B2B";

	public static final String CHANGEL_CUG = "CUG009";
	public static final String CHANGEL_CUG_TYPE = "Close User Group B2B";

	public static final String SIM_SWAP = "SW009";
	public static final String SIM_SWAP_TYPE = "Sim Swap B2B";
	
	public static final String ORDER_MANAGEMENT_LOGS_FILEPATH = "/opt/jboss-fuse/data/log/OrderManagementLogs/";
	public static final String CACHE_IP_ADDRESS = "10.220.48.198:5701";
	public static final Object MAGENTO_SUCESS_CODE_DELETE_API = "173";
	public static final String UNABLE_TO_DELETE_DESC = "100";
	public static final String USER_NOT_EXIST_IN_DB_OR_CACHE_DESC = "06";
	public static final String USER_ALREADY_EXIST = "03";
	public static final String GET_MESSAGE_TEMPLATE_KEY = "messageTemplates";
	public static final Logger logger = Logger.getLogger("azerfon-esb");

	// Languages Mappings
	public static final String LANGUAGE_RUSSIAN_MAPPING = "2";
	public static final String LANGUAGE_ENGLISH_MAPPING = "3";
	public static final String LANGUAGE_AZERI_MAPPING = "4";

	public static final String LANGUAGE_RUSSIAN_MAPPING_DESC = "ru";
	public static final String LANGUAGE_ENGLISH_MAPPING_DESC = "en";
	public static final String LANGUAGE_AZERI_MAPPING_DESC = "az";
	public static final String FNF_OFFERING_ID = "566577110";
	public static final String DEFAULT_CUSTOMER_TYPE_IF_ODS_IS_DOWN = "Individual Customer";
	public static final String CORPORATE_CUSTOMER_TYPE_ID_FROM_ODS = "Corporate Customer";
	public static final String WTTX_CORPORATE_CUSTOMER_TYPE_ID_FROM_ODS = "Data Sim Postpaid Corporate";
	public static final String WTTX_INDIVIDUAL_CUSTOMER_TYPE_ID_FROM_ODS = "Data Sim Postpaid Individual";
	public static final String LIMIT_USERS = "2500";

	public static final String DEFAULT_MAIL_ADDRESS = "azerfon.com";
	public static final String SIM_SWAP_NOTIFICATION = "0";
	public static final String SIM_SWAP_ORDERTYPE = "CO016";
	public static final String SIM_SWAP_ISPARTNER_NOTIFICATION = "0";
	public static final String PDF_EXT = ".pdf";

	// Core services B2B Offering Ids
	public static final String CORE_SERVICES_ROAMING_INTERNATIONAL_NATIONAL = "1665026521";
	public static final String CORE_SERVICES_EXTENDED_ROAMING = "1365026722";
	public static final String CORE_SERVICES_INTERNATIONAL_ACCESS = "1666932367";
	public static final String NGBSS_QUERY_FREE_UNITS_SUCCESS = "0";

}