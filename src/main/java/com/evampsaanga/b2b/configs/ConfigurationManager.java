package com.evampsaanga.b2b.configs;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;

import com.evampsaanga.azerfon.appserver.refreshappservercache.BuildCacheRequestLand;
import com.evampsaanga.b2b.azerfon.db.DBFactory;
import com.evampsaanga.b2b.azerfon.otp.MessageTemplateResponse;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.saanga.magento.apiclient.RestClient;

public class ConfigurationManager {

	public static final Logger logger = Logger.getLogger("azerfon-esb");

//	public static HashMap<String, String> propertiesCacheHashMap = new HashMap<>();
	public static HashMap<String, MessageTemplateResponse> propertiesMessageTemplates = new HashMap<>();
	public static HashMap<String, com.evampsaanga.b2b.magento.specialoffers.Datum> specialGroupsCache = new HashMap<>();
	private static Properties propsDB = null;
	private static Properties propsMapping = null;
	private static Properties propsNGBSSAPI = null;
	private static Properties propsMappingISO = null;
	public static final String MAPPING_BALANCE_LOWER_LIMIT = "balance.lower.limit";
	public static final String MAPPING_CRM_CUSTOMER_TYPE = "crm.customertype.";
	public static final String MAPPING_CRM_TITLE = "crm.title.";
	public static final String MAPPING_CRM_GENDER = "crm.gender.";
	public static final String MAPPING_LOYALTY = "loyalty.";
	public static final String MAPPING_HLR_LANGUAGE = "hlr.language.";
	public static final String MAPPING_CRM_LANGUAGE = "crm.language.";
	public static final String MAPPING_HLR_SUBSCRIBER_TYPE = "hlr.subscriberType.";
	public static final String MAPPING_HLR_STATUS = "hlr.status.";
	public static final String MAPPING_CRM_STATUS = "crm.status.";
	public static final String MAPPING_CRM_HOMEPAGE_STATUS = "status.";
	public static final String MAPPING_HLR_BRANDID = "hlr.brandId.";
	public static final String MAPPING_HLR_GROUPID = "hlr.groupId.";
	public static final String MAPPING_INSTALLMENTSTATUS = "handset.status.";
	public static final String MAPPING_LOANLOG_STATUS = "cbs.ar.loanlog.";
	public static final String MAPPING_BALANCE_MAIN = "hlrBonus.balance.";
	public static final String MAPPING_GROUPID = "groupId.";
	public static final String MAPPING_GROUPIDTRANS = "groupIdtrans.";
	
	public static final String ULDUZUM_LANGUAGE_MAPPING = "ulduzum.subscriber.language.";
	public static final String MSISDN_CODE_START = "994";

	public static String getMappingValueByKey(String key) {
		if (propsMapping == null)
			loadProperties();
		return propsMapping.getProperty(key, "");
	}

	/**
	 * Method accepts key as input and returns value against that key from loaded
	 * properties
	 * 
	 * @param key
	 * @return
	 */
	public static String getNGBSSAPIProperties(String key) {
		if (propsNGBSSAPI == null)
			loadNGBSSAPIProperties();
		String value = "";
		value = propsNGBSSAPI.getProperty(key, "");
		return value;
	}

	public static void loadPropertiesISO() {
		FileInputStream fileInput = null;
		Reader reader = null;
		try {
			File file = new File(Config.MAPPING);
			fileInput = new FileInputStream(file);
			reader = new InputStreamReader(fileInput, "ISO-8859-1");
			propsMappingISO = new Properties();
			propsMappingISO.load(reader);
		} catch (Exception e) {
			logger.error(Helper.GetException(e));
		} finally {
			try {
				if (fileInput != null)
					fileInput.close();
				if (reader != null)
					reader.close();
			} catch (Exception e) {
				logger.error(Helper.GetException(e));
			}
		}
	}

	public static String getMappingValueByKeyISO(String key) {
		if (propsMappingISO == null)
			loadPropertiesISO();
		return propsMappingISO.getProperty(key, "");
	}

	public static String getLoyaltyMapping(String key) {
		if (propsMapping == null)
			loadProperties();
		return propsMapping.getProperty(key, "Low");
	}

	/**
	 * Method loads properties for CRM
	 */
	public static void loadNGBSSAPIProperties() {
		FileInputStream fileInput = null;
		Reader reader = null;
		try {
			File file = new File(Config.NGBSS_API_PROPERTIES);
			fileInput = new FileInputStream(file);
			reader = new InputStreamReader(fileInput, "UTF-8");
			propsNGBSSAPI = new Properties();
			propsNGBSSAPI.load(reader);
		} catch (Exception e) {
			logger.error(Helper.GetException(e));
		} finally {
			try {
				if (fileInput != null)
					fileInput.close();
				if (reader != null)
					reader.close();
			} catch (Exception e) {
				logger.error(Helper.GetException(e));
			}
		}
	}

	public static void loadProperties() {
		FileInputStream fileInput = null;
		Reader reader = null;
		try {
			File file = new File(Config.MAPPING);
			fileInput = new FileInputStream(file);
			reader = new InputStreamReader(fileInput, "UTF-8");
			propsMapping = new Properties();
			propsMapping.load(reader);
		} catch (Exception e) {
			logger.error(Helper.GetException(e));
		} finally {
			try {
				if (fileInput != null)
					fileInput.close();
				if (reader != null)
					reader.close();
			} catch (Exception e) {
				logger.error(Helper.GetException(e));
			}
		}
	}

	public static void reloadPropertiesCache() 
	{
		
		if (BuildCacheRequestLand.configurationCache == null)
			BuildCacheRequestLand.initHazelcast();
		if (BuildCacheRequestLand.configurationCache != null && BuildCacheRequestLand.configurationCache.size() == 0) 
		{
			PreparedStatement preparedStatement = null;
			ResultSet resultSet = null;
			try {
				String getAllConfiguration = "select * from configuration_items";
				preparedStatement = DBFactory.getMagentoDBConnection().prepareStatement(getAllConfiguration);
				resultSet = preparedStatement.executeQuery();
				while (resultSet.next())
					BuildCacheRequestLand.configurationCache.put(resultSet.getString("key"), resultSet.getString("value"));
			} catch (Exception e) {
				logger.error(Helper.GetException(e));
			} finally {
				try {
					if (resultSet != null)
						resultSet.close();
					if (preparedStatement != null)
						preparedStatement.close();
				} catch (Exception e) {
					logger.error(Helper.GetException(e));
				}
			}
		}
		
		
		
//		if (propertiesCacheHashMap != null && propertiesCacheHashMap.size() == 0) {
//			PreparedStatement preparedStatement = null;
//			ResultSet resultSet = null;
//			try {
//				String getAllConfiguration = "select * from configuration_items";
//				preparedStatement = DBFactory.getMagentoDBConnection().prepareStatement(getAllConfiguration);
//				resultSet = preparedStatement.executeQuery();
//				while (resultSet.next())
//					propertiesCacheHashMap.put(resultSet.getString("key"), resultSet.getString("value"));
//			} catch (Exception e) {
//				logger.error(Helper.GetException(e));
//			} finally {
//				try {
//					if (resultSet != null)
//						resultSet.close();
//					if (preparedStatement != null)
//						preparedStatement.close();
//				} catch (Exception e) {
//					logger.error(Helper.GetException(e));
//				}
//			}
//		}
	}

	public static void reloadPropertiesMessageTemplates(String token) {
		if (propertiesMessageTemplates != null && propertiesMessageTemplates.size() == 0) {
			try {
				String response = RestClient.SendCallToMagento(token,
						ConfigurationManager.getConfigurationFromCache("magento.app.signupflow.getMessages"), "");
				MessageTemplateResponse messageTemplateResponse = Helper.JsonToObject(response,
						MessageTemplateResponse.class);
				propertiesMessageTemplates.put(Constants.GET_MESSAGE_TEMPLATE_KEY, messageTemplateResponse);

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.info(Helper.GetException(e));
			}

		}
	}

	public static MessageTemplateResponse getMessageTemplateFromCache(String key,String token) {
		logger.debug("Cache Get Message template for key " + key);
		logger.debug("Cache Check in HashMap " + propertiesMessageTemplates.containsKey(key));
		if (propertiesMessageTemplates.containsKey(key)) {

			logger.debug("Cache " + propertiesMessageTemplates.get(key));
			return propertiesMessageTemplates.get(key);
		} else {
			logger.debug("Cache Reloading properties");
			reloadPropertiesMessageTemplates(token);
			logger.debug("Cache Check in HashMap after reload" + propertiesMessageTemplates.containsKey(key));
			if (propertiesMessageTemplates.containsKey(key))
				return propertiesMessageTemplates.get(key);
			else
				return null;
		}
	}

	public static String getConfigurationFromCache(String key) {
		
		if(BuildCacheRequestLand.configurationCache == null)
			BuildCacheRequestLand.initHazelcast();

		if (BuildCacheRequestLand.configurationCache.containsKey(key)) {

			logger.debug("Message against key:(" + BuildCacheRequestLand.configurationCache.get(key) + ") returned from cache.");

			return BuildCacheRequestLand.configurationCache.get(key);

		} else {

			logger.debug("Message against key:(" + key + ") not found in cache. Request Call Read Messages From DB");

			reloadPropertiesCache();

			logger.debug("Cache check in HashMap after reload-" + BuildCacheRequestLand.configurationCache.containsKey(key));

			if (BuildCacheRequestLand.configurationCache.containsKey(key))
				return BuildCacheRequestLand.configurationCache.get(key);
			else
				return "";
		}
	}

	public static boolean getContainsValueByKey(String key) {
		if (BuildCacheRequestLand.configurationCache.containsKey(key))
			return true;
		else
			reloadPropertiesCache();
		return BuildCacheRequestLand.configurationCache.containsKey(key);
	}

	public static boolean getMessageTemplateValueByKey(String key,String token) {
		if (propertiesMessageTemplates.containsKey(key))
			return true;
		else
			reloadPropertiesMessageTemplates(token);
		return propertiesMessageTemplates.containsKey(key);
	}

	/**
	 * Method loads properties for Local DB
	 */
	public static void loadDBProperties() {
		FileInputStream fileInput = null;
		Reader reader = null;
		try {
			File file = new File(Config.DATABASE_FILE);
			fileInput = new FileInputStream(file);
			reader = new InputStreamReader(fileInput, "UTF-8");
			propsDB = new Properties();
			propsDB.load(reader);
		} catch (Exception e) {
			logger.error(Helper.GetException(e));
		} finally {
			try {
				if (fileInput != null)
					fileInput.close();
				if (reader != null)
					reader.close();
			} catch (Exception e) {
				logger.error(Helper.GetException(e));
			}
		}
	}

	/**
	 * Method accepts key as input and returns value against that key from loaded
	 * properties
	 * 
	 * @param key
	 * @return
	 */
	public static String getDBProperties(String key) {
		if (propsDB == null)
			loadDBProperties();
		String value = "";
		value = propsDB.getProperty(key, "");
		return value;
	}
}
