package com.evampsaanga.b2b.exceptions;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class CamelException implements ExceptionMapper<Exception> {
	@Override
	public Response toResponse(Exception exception) {
		System.out.println("Exception type:" + exception.getClass().getCanonicalName());
		if (exception instanceof InternalServerErrorException) {
			return Response.status(500).header("custom ki exception", "exception").build();
		} else if (exception instanceof BadRequestException) {
			return Response.status(Response.Status.BAD_REQUEST).header("custom ki exception", "exception").build();
		} else if (exception instanceof org.apache.camel.InvalidPayloadException) {
			return Response.status(Response.Status.BAD_REQUEST).header("custom ki exception", "exception").build();
		} else if (exception instanceof org.apache.camel.CamelExecutionException) {
			return Response.status(Response.Status.BAD_REQUEST).header("custom ki exception", "exception").build();
		}
		System.out.println("custom Exception" + exception.getMessage());
		return Response.status(Response.Status.REQUEST_TIMEOUT).build();
	}
}