package com.evampsaanga.b2b.restresponses;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import org.apache.log4j.Logger;

import com.evampsaanga.b2b.developer.utils.Helper;

@Provider
public class ExceptionHandler implements ExceptionMapper<Throwable> {
	public static final Logger logger = Logger.getLogger("azerfon-esb");

	@Override
	public Response toResponse(Throwable exception) {
		logger.info("Exception type:" + exception.getClass().getCanonicalName());
		logger.info(Helper.GetException(exception));
		if (exception instanceof InternalServerErrorException) {
			return Response.status(500).header("Problem", "InternalServerErrorException").build();
		} else if (exception instanceof BadRequestException) {
			return Response.status(Response.Status.BAD_REQUEST)
					.header("unexpected request data", "BadRequestExceptiont").build();
		} else if (exception instanceof org.apache.camel.InvalidPayloadException) {
			return Response.status(Response.Status.BAD_REQUEST).header("Problem", "InvalidPayloadException").build();
		} else if (exception instanceof org.apache.camel.CamelExecutionException) {
			return Response.status(Response.Status.BAD_REQUEST).header("Problem", "CamelExecutionException").build();
		}
		if (exception.getClass().getCanonicalName()
				.equals("org.apache.camel.processor.ThrottlerRejectedExecutionException")) {
			System.out.println("got throttle exception");
			return Response.status(Response.Status.SERVICE_UNAVAILABLE)
					.header("Throttle", "Too much requests please try again later").build();
		}
		return Response.status(Response.Status.REQUEST_TIMEOUT).header("Problem", "yes problem").build();
	}
}