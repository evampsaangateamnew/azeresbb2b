package com.evampsaanga.b2b.models;

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;

public class LoginRequest extends BaseRequest{
	
	private String password;
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
}
