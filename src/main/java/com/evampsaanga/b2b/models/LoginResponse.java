package com.evampsaanga.b2b.models;

import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;
import com.evampsaanga.b2b.magento.getpredefineddataV2.GetPredefinedDataResponseData;

public class LoginResponse extends BaseResponse {

	private LoginData loginData;
	private GetPredefinedDataResponseData predefinedData;

	public LoginData getLoginData() {
		return loginData;
	}

	public void setLoginData(LoginData loginData) {
		this.loginData = loginData;
	}

	public GetPredefinedDataResponseData getPredefinedData() {
		return predefinedData;
	}

	public void setPredefinedData(GetPredefinedDataResponseData predefinedData) {
		this.predefinedData = predefinedData;
	}

}
