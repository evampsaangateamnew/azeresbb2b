
package com.evampsaanga.b2b.models;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "entity_id",
    "website_id",
    "email",
    "group_id",
    "increment_id",
    "store_id",
    "created_at",
    "updated_at",
    "is_active",
    "disable_auto_group_change",
    "created_in",
    "prefix",
    "firstname",
    "middlename",
    "lastname",
    "suffix",
    "dob",
    "password_hash",
    "rp_token",
    "rp_token_created_at",
    "default_billing",
    "default_shipping",
    "taxvat",
    "confirmation",
    "gender",
    "failures_num",
    "first_failure",
    "lock_expires",
    "msisdn",
    "reward_update_notification",
    "reward_warning_notification",
    "pic_status",
    "pic_new_reset",
    "rateus_android",
    "rateus_ios",
    "pic_tnc",
    "customer_type",
    "account_id",
    "language",
    "title",
    "customer_id",
    "channel",
    "account_code",
    "cug",
    "company_level",
    "vpn",
    "group_type",
    "otp_source",
    "pic_pin",
    "pic_allowed_tariffs",
    "pic_allowed_offers",
    "password_unhashed",
    "otp_msisdn",
    "company_name",
    "contact_number",
    "pic_attribute1",
    "vpc",
    "group_code",
    "document_type",
    "sun",
    "pic_voen"
})
public class LoginData {

	
	@JsonProperty("pic_voen")
	private String pic_voen;
	@JsonProperty("sun")
	private String sun;
    @JsonProperty("entity_id")
    private String entityId;
    @JsonProperty("website_id")
    private String websiteId;
    @JsonProperty("email")
    private String email;
    @JsonProperty("group_id")
    private String groupId;
    @JsonProperty("increment_id")
    private Object incrementId;
    @JsonProperty("store_id")
    private String storeId;
    @JsonProperty("created_at")
    private String createdAt;
    @JsonProperty("updated_at")
    private String updatedAt;
    @JsonProperty("is_active")
    private String isActive;
    @JsonProperty("disable_auto_group_change")
    private String disableAutoGroupChange;
    @JsonProperty("created_in")
    private String createdIn;
    @JsonProperty("prefix")
    private Object prefix;
    @JsonProperty("firstname")
    private String firstname;
    @JsonProperty("middlename")
    private String middlename;
    @JsonProperty("lastname")
    private String lastname;
    @JsonProperty("suffix")
    private Object suffix;
    @JsonProperty("dob")
    private Object dob;
    @JsonProperty("password_hash")
    private String passwordHash;
    @JsonProperty("rp_token")
    private String rpToken;
    @JsonProperty("rp_token_created_at")
    private String rpTokenCreatedAt;
    @JsonProperty("default_billing")
    private String defaultBilling;
    @JsonProperty("default_shipping")
    private String defaultShipping;
    @JsonProperty("taxvat")
    private Object taxvat;
    @JsonProperty("confirmation")
    private Object confirmation;
    @JsonProperty("gender")
    private String gender;
    @JsonProperty("failures_num")
    private String failuresNum;
    @JsonProperty("first_failure")
    private Object firstFailure;
    @JsonProperty("lock_expires")
    private Object lockExpires;
    @JsonProperty("msisdn")
    private String msisdn;
    @JsonProperty("reward_update_notification")
    private String rewardUpdateNotification;
    @JsonProperty("reward_warning_notification")
    private String rewardWarningNotification;
    @JsonProperty("pic_status")
    private String picStatus;
    @JsonProperty("pic_new_reset")
    private String picNewReset;
    @JsonProperty("rateus_android")
    private String rateusAndroid;
    @JsonProperty("rateus_ios")
    private String rateusIos;
    @JsonProperty("pic_tnc")
    private String picTnc;
    @JsonProperty("customer_type")
    private String customerType;
    @JsonProperty("account_id")
    private String accountId;
    @JsonProperty("language")
    private String language;
    @JsonProperty("title")
    private String title;
    @JsonProperty("customer_id")
    private String customerId;
    @JsonProperty("channel")
    private String channel;
    @JsonProperty("account_code")
    private String accountCode;
    @JsonProperty("cug")
    private String cug;
    @JsonProperty("company_level")
    private String companyLevel;
    @JsonProperty("vpn")
    private String vpn;
    @JsonProperty("group_type")
    private String groupType;
    @JsonProperty("otp_source")
    private String otpSource;
    @JsonProperty("pic_pin")
    private String picPin;
    @JsonProperty("pic_allowed_tariffs")
    private String picAllowedTariffs;
    @JsonProperty("pic_allowed_offers")
    private String picAllowedOffers;
    @JsonProperty("password_unhashed")
    private String passwordUnhashed;
    @JsonProperty("otp_msisdn")
    private String otpMsisdn;
    @JsonProperty("company_name")
    private String companyName;
    @JsonProperty("contact_number")
    private String contactNumber;
    @JsonProperty("pic_attribute1")
	private String pic_attribute1;
    @JsonProperty("vpc")
	private String vpc;
    @JsonProperty("group_code")
	private String groupCode;
    @JsonProperty("document_type")
	private String document_type;
    
    
    private String custom_profile_image;
    private String customer_id;
    
    
    
    public String getPic_attribute1() {
		return pic_attribute1;
	}

	public void setPic_attribute1(String pic_attribute1) {
		this.pic_attribute1 = pic_attribute1;
	}


    public String getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}

	public String getCustom_profile_image() {
		return custom_profile_image;
	}

	public void setCustom_profile_image(String custom_profile_image) {
		this.custom_profile_image = custom_profile_image;
	}

	@JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("entity_id")
    public String getEntityId() {
        return entityId;
    }

    @JsonProperty("entity_id")
    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    @JsonProperty("website_id")
    public String getWebsiteId() {
        return websiteId;
    }

    @JsonProperty("website_id")
    public void setWebsiteId(String websiteId) {
        this.websiteId = websiteId;
    }

    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }

    @JsonProperty("group_id")
    public String getGroupId() {
        return groupId;
    }

    @JsonProperty("group_id")
    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    @JsonProperty("increment_id")
    public Object getIncrementId() {
        return incrementId;
    }

    @JsonProperty("increment_id")
    public void setIncrementId(Object incrementId) {
        this.incrementId = incrementId;
    }

    @JsonProperty("store_id")
    public String getStoreId() {
        return storeId;
    }

    @JsonProperty("store_id")
    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    @JsonProperty("created_at")
    public String getCreatedAt() {
        return createdAt;
    }

    @JsonProperty("created_at")
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    @JsonProperty("updated_at")
    public String getUpdatedAt() {
        return updatedAt;
    }

    @JsonProperty("updated_at")
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    @JsonProperty("is_active")
    public String getIsActive() {
        return isActive;
    }

    @JsonProperty("is_active")
    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    @JsonProperty("disable_auto_group_change")
    public String getDisableAutoGroupChange() {
        return disableAutoGroupChange;
    }

    @JsonProperty("disable_auto_group_change")
    public void setDisableAutoGroupChange(String disableAutoGroupChange) {
        this.disableAutoGroupChange = disableAutoGroupChange;
    }

    @JsonProperty("created_in")
    public String getCreatedIn() {
        return createdIn;
    }

    @JsonProperty("created_in")
    public void setCreatedIn(String createdIn) {
        this.createdIn = createdIn;
    }

    @JsonProperty("prefix")
    public Object getPrefix() {
        return prefix;
    }

    @JsonProperty("prefix")
    public void setPrefix(Object prefix) {
        this.prefix = prefix;
    }

    @JsonProperty("firstname")
    public String getFirstname() {
        return firstname;
    }

    @JsonProperty("firstname")
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    @JsonProperty("middlename")
    public String getMiddlename() {
        return middlename;
    }

    @JsonProperty("middlename")
    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    @JsonProperty("lastname")
    public String getLastname() {
        return lastname;
    }

    @JsonProperty("lastname")
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    @JsonProperty("suffix")
    public Object getSuffix() {
        return suffix;
    }

    @JsonProperty("suffix")
    public void setSuffix(Object suffix) {
        this.suffix = suffix;
    }

    @JsonProperty("dob")
    public Object getDob() {
        return dob;
    }

    @JsonProperty("dob")
    public void setDob(Object dob) {
        this.dob = dob;
    }

    @JsonProperty("password_hash")
    public String getPasswordHash() {
        return passwordHash;
    }

    @JsonProperty("password_hash")
    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    @JsonProperty("rp_token")
    public String getRpToken() {
        return rpToken;
    }

    @JsonProperty("rp_token")
    public void setRpToken(String rpToken) {
        this.rpToken = rpToken;
    }

    @JsonProperty("rp_token_created_at")
    public String getRpTokenCreatedAt() {
        return rpTokenCreatedAt;
    }

    @JsonProperty("rp_token_created_at")
    public void setRpTokenCreatedAt(String rpTokenCreatedAt) {
        this.rpTokenCreatedAt = rpTokenCreatedAt;
    }

    @JsonProperty("default_billing")
    public String getDefaultBilling() {
        return defaultBilling;
    }

    @JsonProperty("default_billing")
    public void setDefaultBilling(String defaultBilling) {
        this.defaultBilling = defaultBilling;
    }

    @JsonProperty("default_shipping")
    public String getDefaultShipping() {
        return defaultShipping;
    }

    @JsonProperty("default_shipping")
    public void setDefaultShipping(String defaultShipping) {
        this.defaultShipping = defaultShipping;
    }

    @JsonProperty("taxvat")
    public Object getTaxvat() {
        return taxvat;
    }

    @JsonProperty("taxvat")
    public void setTaxvat(Object taxvat) {
        this.taxvat = taxvat;
    }

    @JsonProperty("confirmation")
    public Object getConfirmation() {
        return confirmation;
    }

    @JsonProperty("confirmation")
    public void setConfirmation(Object confirmation) {
        this.confirmation = confirmation;
    }

    @JsonProperty("gender")
    public String getGender() {
        return gender;
    }

    @JsonProperty("gender")
    public void setGender(String gender) {
        this.gender = gender;
    }

    @JsonProperty("failures_num")
    public String getFailuresNum() {
        return failuresNum;
    }

    @JsonProperty("failures_num")
    public void setFailuresNum(String failuresNum) {
        this.failuresNum = failuresNum;
    }

    @JsonProperty("first_failure")
    public Object getFirstFailure() {
        return firstFailure;
    }

    @JsonProperty("first_failure")
    public void setFirstFailure(Object firstFailure) {
        this.firstFailure = firstFailure;
    }

    @JsonProperty("lock_expires")
    public Object getLockExpires() {
        return lockExpires;
    }

    @JsonProperty("lock_expires")
    public void setLockExpires(Object lockExpires) {
        this.lockExpires = lockExpires;
    }

    @JsonProperty("msisdn")
    public String getMsisdn() {
        return msisdn;
    }

    @JsonProperty("msisdn")
    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    @JsonProperty("reward_update_notification")
    public String getRewardUpdateNotification() {
        return rewardUpdateNotification;
    }

    @JsonProperty("reward_update_notification")
    public void setRewardUpdateNotification(String rewardUpdateNotification) {
        this.rewardUpdateNotification = rewardUpdateNotification;
    }

    @JsonProperty("reward_warning_notification")
    public String getRewardWarningNotification() {
        return rewardWarningNotification;
    }

    @JsonProperty("reward_warning_notification")
    public void setRewardWarningNotification(String rewardWarningNotification) {
        this.rewardWarningNotification = rewardWarningNotification;
    }

    @JsonProperty("pic_status")
    public String getPicStatus() {
        return picStatus;
    }

    @JsonProperty("pic_status")
    public void setPicStatus(String picStatus) {
        this.picStatus = picStatus;
    }

    @JsonProperty("pic_new_reset")
    public String getPicNewReset() {
        return picNewReset;
    }

    @JsonProperty("pic_new_reset")
    public void setPicNewReset(String picNewReset) {
        this.picNewReset = picNewReset;
    }

    @JsonProperty("rateus_android")
    public String getRateusAndroid() {
        return rateusAndroid;
    }

    @JsonProperty("rateus_android")
    public void setRateusAndroid(String rateusAndroid) {
        this.rateusAndroid = rateusAndroid;
    }

    @JsonProperty("rateus_ios")
    public String getRateusIos() {
        return rateusIos;
    }

    @JsonProperty("rateus_ios")
    public void setRateusIos(String rateusIos) {
        this.rateusIos = rateusIos;
    }

    @JsonProperty("pic_tnc")
    public String getPicTnc() {
        return picTnc;
    }

    @JsonProperty("pic_tnc")
    public void setPicTnc(String picTnc) {
        this.picTnc = picTnc;
    }

    @JsonProperty("customer_type")
    public String getCustomerType() {
        return customerType;
    }

    @JsonProperty("customer_type")
    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    @JsonProperty("account_id")
    public String getAccountId() {
        return accountId;
    }

    @JsonProperty("account_id")
    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    @JsonProperty("language")
    public String getLanguage() {
        return language;
    }

    @JsonProperty("language")
    public void setLanguage(String language) {
        this.language = language;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty("customer_id")
    public String getCustomerId() {
        return customerId;
    }

    @JsonProperty("customer_id")
    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    @JsonProperty("channel")
    public String getChannel() {
        return channel;
    }

    @JsonProperty("channel")
    public void setChannel(String channel) {
        this.channel = channel;
    }

    @JsonProperty("account_code")
    public String getAccountCode() {
        return accountCode;
    }

    @JsonProperty("account_code")
    public void setAccountCode(String accountCode) {
        this.accountCode = accountCode;
    }

    @JsonProperty("cug")
    public String getCug() {
        return cug;
    }

    @JsonProperty("cug")
    public void setCug(String cug) {
        this.cug = cug;
    }

    @JsonProperty("company_level")
    public String getCompanyLevel() {
        return companyLevel;
    }

    @JsonProperty("company_level")
    public void setCompanyLevel(String companyLevel) {
        this.companyLevel = companyLevel;
    }

    @JsonProperty("vpn")
    public String getVpn() {
        return vpn;
    }

    @JsonProperty("vpn")
    public void setVpn(String vpn) {
        this.vpn = vpn;
    }

    @JsonProperty("group_type")
    public String getGroupType() {
        return groupType;
    }

    @JsonProperty("group_type")
    public void setGroupType(String groupType) {
        this.groupType = groupType;
    }

    @JsonProperty("otp_source")
    public String getOtpSource() {
        return otpSource;
    }

    @JsonProperty("otp_source")
    public void setOtpSource(String otpSource) {
        this.otpSource = otpSource;
    }

    @JsonProperty("pic_pin")
    public String getPicPin() {
        return picPin;
    }

    @JsonProperty("pic_pin")
    public void setPicPin(String picPin) {
        this.picPin = picPin;
    }

    @JsonProperty("pic_allowed_tariffs")
    public String getPicAllowedTariffs() {
        return picAllowedTariffs;
    }

    @JsonProperty("pic_allowed_tariffs")
    public void setPicAllowedTariffs(String picAllowedTariffs) {
        this.picAllowedTariffs = picAllowedTariffs;
    }

    @JsonProperty("pic_allowed_offers")
    public String getPicAllowedOffers() {
        return picAllowedOffers;
    }

    @JsonProperty("pic_allowed_offers")
    public void setPicAllowedOffers(String picAllowedOffers) {
        this.picAllowedOffers = picAllowedOffers;
    }

    @JsonProperty("password_unhashed")
    public String getPasswordUnhashed() {
        return passwordUnhashed;
    }

    @JsonProperty("password_unhashed")
    public void setPasswordUnhashed(String passwordUnhashed) {
        this.passwordUnhashed = passwordUnhashed;
    }

    @JsonProperty("otp_msisdn")
    public String getOtpMsisdn() {
        return otpMsisdn;
    }

    @JsonProperty("otp_msisdn")
    public void setOtpMsisdn(String otpMsisdn) {
        this.otpMsisdn = otpMsisdn;
    }

    @JsonProperty("company_name")
    public String getCompanyName() {
        return companyName;
    }

    @JsonProperty("company_name")
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    @JsonProperty("contact_number")
    public String getContactNumber() {
        return contactNumber;
    }

    @JsonProperty("contact_number")
    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

	public String getVpc() {
		return vpc;
	}

	public void setVpc(String vpc) {
		this.vpc = vpc;
	}

	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public String getDocument_type() {
		return document_type;
	}

	public void setDocument_type(String document_type) {
		this.document_type = document_type;
	}

	public String getSun() {
		return sun;
	}

	public void setSun(String sun) {
		this.sun = sun;
	}
	
	

}
