package com.evampsaanga.b2b.mhmclient;

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"documentType",
"documentNumber",
"documentPin",
"placeType",
"regions",
"placeName",
"placeAddress",
"sellerUsername",
"sellerDocumentPin",
"selletDocumentNumber",
"sellerFullName",
"addType",
"contact",
"email",
"gsmNumber",
"iccid",
"imei",
"imsi",
"transactionId",
"customerId",
"voen",
"sun"
})
public class SimSwapRequest extends BaseRequest {

	
	
@JsonProperty("voen")
private String voen;
@JsonProperty("sun")
private String sun;
	
@JsonProperty("documentType")
private String documentType;
@JsonProperty("customerId")
private String customerId;
@JsonProperty("documentNumber")
private String documentNumber;
@JsonProperty("documentPin")
private String documentPin;
@JsonProperty("placeType")
private String placeType;
@JsonProperty("regions")
private String regions;
@JsonProperty("placeName")
private String placeName;
@JsonProperty("placeAddress")
private String placeAddress;
@JsonProperty("sellerUsername")
private String sellerUsername;
@JsonProperty("sellerDocumentPin")
private String sellerDocumentPin;
@JsonProperty("selletDocumentNumber")
private String selletDocumentNumber;
@JsonProperty("sellerFullName")
private String sellerFullName;
@JsonProperty("addType")
private String addType;
@JsonProperty("contact")
private String contact;
@JsonProperty("email")
private String email;
@JsonProperty("gsmNumber")
private String gsmNumber;
@JsonProperty("iccid")
private String iccid;
@JsonProperty("imei")
private String imei;
@JsonProperty("imsi")
private String imsi;
@JsonProperty("transactionId")
private String transactionId;

@JsonProperty("customerId")
public String getCustomerId() {
	return customerId;
}
@JsonProperty("customerId")
public void setCustomerId(String customerId) {
	this.customerId = customerId;
}

@JsonProperty("documentType")
public String getDocumentType() {
return documentType;
}

@JsonProperty("documentType")
public void setDocumentType(String documentType) {
this.documentType = documentType;
}

@JsonProperty("documentNumber")
public String getDocumentNumber() {
return documentNumber;
}

@JsonProperty("documentNumber")
public void setDocumentNumber(String documentNumber) {
this.documentNumber = documentNumber;
}

@JsonProperty("documentPin")
public String getDocumentPin() {
return documentPin;
}

@JsonProperty("documentPin")
public void setDocumentPin(String documentPin) {
this.documentPin = documentPin;
}

@JsonProperty("placeType")
public String getPlaceType() {
return placeType;
}

@JsonProperty("placeType")
public void setPlaceType(String placeType) {
this.placeType = placeType;
}

@JsonProperty("regions")
public String getRegions() {
return regions;
}

@JsonProperty("regions")
public void setRegions(String regions) {
this.regions = regions;
}

@JsonProperty("placeName")
public String getPlaceName() {
return placeName;
}

@JsonProperty("placeName")
public void setPlaceName(String placeName) {
this.placeName = placeName;
}

@JsonProperty("placeAddress")
public String getPlaceAddress() {
return placeAddress;
}

@JsonProperty("placeAddress")
public void setPlaceAddress(String placeAddress) {
this.placeAddress = placeAddress;
}

@JsonProperty("sellerUsername")
public String getSellerUsername() {
return sellerUsername;
}

@JsonProperty("sellerUsername")
public void setSellerUsername(String sellerUsername) {
this.sellerUsername = sellerUsername;
}

@JsonProperty("sellerDocumentPin")
public String getSellerDocumentPin() {
return sellerDocumentPin;
}

@JsonProperty("sellerDocumentPin")
public void setSellerDocumentPin(String sellerDocumentPin) {
this.sellerDocumentPin = sellerDocumentPin;
}

@JsonProperty("selletDocumentNumber")
public String getSelletDocumentNumber() {
return selletDocumentNumber;
}

@JsonProperty("selletDocumentNumber")
public void setSelletDocumentNumber(String selletDocumentNumber) {
this.selletDocumentNumber = selletDocumentNumber;
}

@JsonProperty("sellerFullName")
public String getSellerFullName() {
return sellerFullName;
}

@JsonProperty("sellerFullName")
public void setSellerFullName(String sellerFullName) {
this.sellerFullName = sellerFullName;
}

@JsonProperty("addType")
public String getAddType() {
return addType;
}

@JsonProperty("addType")
public void setAddType(String addType) {
this.addType = addType;
}

@JsonProperty("contact")
public String getContact() {
return contact;
}

@JsonProperty("contact")
public void setContact(String contact) {
this.contact = contact;
}

@JsonProperty("email")
public String getEmail() {
return email;
}

@JsonProperty("email")
public void setEmail(String email) {
this.email = email;
}

@JsonProperty("gsmNumber")
public String getGsmNumber() {
return gsmNumber;
}

@JsonProperty("gsmNumber")
public void setGsmNumber(String gsmNumber) {
this.gsmNumber = gsmNumber;
}

@JsonProperty("iccid")
public String getIccid() {
return iccid;
}

@JsonProperty("iccid")
public void setIccid(String iccid) {
this.iccid = iccid;
}

@JsonProperty("imei")
public String getImei() {
return imei;
}

@JsonProperty("imei")
public void setImei(String imei) {
this.imei = imei;
}

@JsonProperty("imsi")
public String getImsi() {
return imsi;
}

@JsonProperty("imsi")
public void setImsi(String imsi) {
this.imsi = imsi;
}

@JsonProperty("transactionId")
public String getTransactionId() {
return transactionId;
}

@JsonProperty("transactionId")
public void setTransactionId(String transactionId) {
this.transactionId = transactionId;
}

@Override
public String toString() {
return new ToStringBuilder(this).append("documentType", documentType).append("documentNumber", documentNumber).append("documentPin", documentPin).append("placeType", placeType).append("regions", regions).append("placeName", placeName).append("placeAddress", placeAddress).append("sellerUsername", sellerUsername).append("sellerDocumentPin", sellerDocumentPin).append("selletDocumentNumber", selletDocumentNumber).append("sellerFullName", sellerFullName).append("addType", addType).append("contact", contact).append("email", email).append("gsmNumber", gsmNumber).append("iccid", iccid).append("imei", imei).append("imsi", imsi).append("transactionId", transactionId).toString();
}

public String getSun() {
	return sun;
}
public void setSun(String sun) {
	this.sun = sun;
}
public String getVoen() {
	return voen;
}
public void setVoen(String voen) {
	this.voen = voen;
}

}