package com.evampsaanga.b2b.mhmclient;

public class SimSwapResponse {
	private String returnCode;
	private String returnMsg;
	private String transactionId;
	private String specialGroup;
	
	public String getReturnCode() {
		return returnCode;
	}
	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}
	public String getReturnMsg() {
		return returnMsg;
	}
	public void setReturnMsg(String returnMsg) {
		this.returnMsg = returnMsg;
	}
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public String getSpecialGroup() {
		return specialGroup;
	}
	public void setSpecialGroup(String specialGroup) {
		this.specialGroup = specialGroup;
	}
	

}
