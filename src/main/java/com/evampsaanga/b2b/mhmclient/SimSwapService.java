package com.evampsaanga.b2b.mhmclient;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;

import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.azerfon.getsubscriber.CRMSubscriberService;
import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.ThirdPartyNames;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Decrypter;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.evampsaanga.b2b.developer.utils.SOAPLoggingHandler;
import com.google.gson.JsonObject;
import com.huawei.crm.azerfon.bss.query.GetSubscriberResponse;
import com.huawei.crm.basetype.ens.BusinessFee;
import com.huawei.crm.basetype.ens.ChargeFeeInfo;
import com.huawei.crm.basetype.ens.ChargeFeeList;
import com.huawei.crm.basetype.ens.ChargeInfo;
import com.huawei.crm.basetype.ens.OrderInfo;
import com.huawei.crm.basetype.ens.OrderItemInfo;
import com.huawei.crm.basetype.ens.OrderItemValue;
import com.huawei.crm.basetype.ens.OrderItems;
import com.huawei.crm.basetype.ens.SubscriberInfo;
import com.huawei.crm.query.GetCustomerIn;
import com.huawei.crm.query.GetCustomerRequest;
import com.huawei.crm.query.GetCustomerResponse;
import com.huawei.crm.query.GetGroupDataIn;
import com.huawei.crm.query.GetGroupRequest;
import com.huawei.crm.query.GetGroupResponse;
import com.ngbss.evampsaanga.services.CRMServices;
import com.ngbss.evampsaanga.services.OrderHandleService;
import com.ngbss.evampsaanga.services.ThirdPartyCall;
import com.saanga.magento.apiclient.RestClient;

import https.www_e_gov.AddApplicationResponse;
import https.www_e_gov.CheckVoenValidResponse;
import https.wwww_e_gov.RequestApplication;
import https.wwww_e_gov.RequestApplicationTypes;
import https.wwww_e_gov.RequestAuthentication;
import https.wwww_e_gov.RequestDocumentInformation;
import https.wwww_e_gov.RequestSellerInformation;
import https.wwww_e_gov.RequestVoenInformation;
import https.wwww_e_gov.ResponseApplicationInfo;
import https.wwww_e_gov.ResponseInformationByPin;
import https.wwww_e_gov.ResponseInformationByVoen;
import net.x_rd.az.voen9900037691.producer.AddApplicationRequest;
import net.x_rd.az.voen9900037691.producer.ArrayOfRequestApplication;
import net.x_rd.az.voen9900037691.producer.CheckDocumentValidRequest;
import net.x_rd.az.voen9900037691.producer.CheckVoenValidRequest;

@Path("/azerfon")
public class SimSwapService {
	public static final Logger logger = Logger.getLogger("azerfon-esb");

	public static String modeulName = "SimSWAP";

	@POST
	@Path("/verify")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public SimSwapResponse verify(@Body String requestBody, @Header("credentials") String credential) {
		logger.info(modeulName + "Request: Verify" + requestBody);
		logger.info(modeulName + "credential: verify" + credential);
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.SIM_SWAP_VERIFY);
		logs.setThirdPartyName(ThirdPartyNames.THIRD_PARTY_SIM_SWAP);
		// logs.setTableType(LogsType.verify);
		SimSwapRequest cclient = null;
		SimSwapResponse resp = new SimSwapResponse();
		try {
			cclient = Helper.JsonToObject(requestBody, SimSwapRequest.class);
			if (cclient != null) {
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				logs.setLang(cclient.getLang());
			}
		} catch (Exception ex) {
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_401);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		if (cclient != null) {
			String credentials = null;
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
			} catch (Exception ex) {
				SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials == null) {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
				String verification = Helper.validateRequest(cclient);
				if (!verification.equals("")) {
					resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
					resp.setReturnMsg(verification);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {

				try {

					if (cclient.getDocumentType() != null && cclient.getDocumentNumber() != null
							&& cclient.getDocumentPin() != null) {
						logger.info(modeulName + ":" + Helper.ObjectToJson(cclient));
						// Temporary Commenting This To HardCode SuCcess And
						// Give It to Junaid Hasan
						CheckVoenValidResponse checkVoenValidResponse = checkVoienValid(cclient);
						logger.info(modeulName + ":" + Helper.ObjectToJson(checkVoenValidResponse));

						// This API Sucess Code is 200.
						if (checkVoenValidResponse.getCheckVoenValidResult().getStatus().getCode() == 200) {

							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
							resp.setTransactionId(checkVoenValidResponse.getCheckVoenValidResult().getTransactionId());
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}

						else {
							resp.setReturnCode(String
									.valueOf(checkVoenValidResponse.getCheckVoenValidResult().getStatus().getCode()));
							
							resp.setReturnMsg(ConfigurationManager
									.getConfigurationFromCache("simswap."+String
											.valueOf(checkVoenValidResponse.getCheckVoenValidResult().getStatus().getCode())+"."+cclient.getLang()));
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							return resp;
						}

						// resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
						// resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
						// logs.setResponseCode(resp.getReturnCode());
						// logs.setResponseDescription(resp.getReturnMsg());
						// logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						// logs.updateLog(logs);
						// return resp;

					} else {
						resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
						resp.setReturnMsg(ResponseCodes.ERROR_400);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				} catch (Exception ex) {
					logger.info(Helper.GetException(ex));
					resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
					resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;

				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		}
		logs.updateLog(logs);
		return resp;
	}

	/**
	 * 
	 * @param requestBody
	 * @param credential
	 * @return App Faq Response
	 */
	// for phase 2

	@POST
	@Path("/documentValid")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public SimSwapResponse documentValid(@Body String requestBody, @Header("credentials") String credential) {
		Helper.logInfoMessageV2("Request Data: upload Document" + requestBody);
		Helper.logInfoMessageV2("credential: upload Document" + credential);
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.SIM_SWAP_UPLOAD_FILE);
		logs.setThirdPartyName(ThirdPartyNames.THIRD_PARTY_SIM_SWAP);
		// logs.setTableType(LogsType.UploadFile);
		SimSwapRequest cclient = null;
		SimSwapResponse resp = new SimSwapResponse();
		try {
			cclient = Helper.JsonToObject(requestBody, SimSwapRequest.class);
			if (cclient != null) {
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				logs.setLang(cclient.getLang());
			}
		} catch (Exception ex) {
			logger.error("Error:", ex);
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_401);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		if (cclient != null) {
			String credentials = null;
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
				Helper.logInfoMessageV2(cclient.getmsisdn() + " - Credentials " + credential);
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials == null) {
				Helper.logInfoMessageV2(cclient.getmsisdn() + " - Credentials are null");
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
				String verification = Helper.validateRequest(cclient);
				if (!verification.equals("")) {
					Helper.logInfoMessageV2(cclient.getmsisdn() + " - Verification failed");
					resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
					resp.setReturnMsg(verification);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				Helper.logInfoMessageV2(cclient.getmsisdn() + " - Incorrect MSISDN");
				resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {

				try {

					if (cclient.getGsmNumber() != null && cclient.getIccid() != null
							&& cclient.getTransactionId() != null && cclient.getCustomerId() != null) {
						
						logger.info(modeulName + ":" + "Calling Group Info");
						GetGroupRequest getGroupDataRequestMsgReq = new GetGroupRequest();
						GetGroupDataIn getGroupDataIn = new GetGroupDataIn();
						getGroupDataIn.setServiceNumber(cclient.getGsmNumber());
						getGroupDataRequestMsgReq.setGetGroupBody(getGroupDataIn);
						getGroupDataRequestMsgReq.setRequestHeader(getReqHeaderForGETNetworkSetting());
						GetGroupResponse getGroupResponse = ThirdPartyCall.getGroupData(getGroupDataRequestMsgReq);

						if (getGroupResponse.getGetGroupBody() == null) {
							logger.info(modeulName + ":" + "Returning Because User is not in Special Group");
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ConfigurationManager.getConfigurationFromCache("sim.swap.simplegroup.message." + cclient.getLang()).trim());
							resp.setSpecialGroup("0");
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
						
						
						logger.info(modeulName + ":" + "Calling checkSpecialGroup");
						int checkSpecialGroup = checkSpecialGroup(getGroupResponse);
						
						if (checkSpecialGroup == 1)
						{
							logger.info(modeulName + ":" + "Returning Because User is in Special Group");
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ConfigurationManager.getConfigurationFromCache("sim.swap.specialgroup.message." + cclient.getLang()).trim());
							resp.setSpecialGroup("1");//1 for user is in special group
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
						
						else
						{
							logger.info(modeulName + ":" + "Returning Because User is not in Special Group");
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ConfigurationManager.getConfigurationFromCache("sim.swap.simplegroup.message." + cclient.getLang()).trim());
							resp.setSpecialGroup("0");
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					
					} else {
						resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
						resp.setReturnMsg(ResponseCodes.ERROR_400);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}

				} catch (Exception ex) {
					logger.info(Helper.GetException(ex));
					resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
					resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;

				}
			} else {
				Helper.logInfoMessageV2(cclient.getmsisdn() + " - 401 Access Not Authorized ");
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		}
		logs.updateLog(logs);
		return resp;
	}

//	@POST
//	@Path("/addApplication")
//	@Produces(MediaType.APPLICATION_JSON)
//	@Consumes(MediaType.APPLICATION_JSON)
//	public SimSwapResponse addApplication(@Body String requestBody, @Header("credentials") String credential) {
//		Helper.logInfoMessageV2("Request Data: add Application" + requestBody);
//		Helper.logInfoMessageV2("credential: add Application" + credential);
//		Logs logs = new Logs();
//		logs.setTransactionName(Transactions.SIM_SWAP_ADD_APPLICATION);
//		logs.setThirdPartyName(ThirdPartyNames.THIRD_PARTY_SIM_SWAP);
//		// logs.setTableType(LogsType.AddApplication);
//		SimSwapRequest cclient = null;
//		SimSwapResponse resp = new SimSwapResponse();
//		try {
//			cclient = Helper.JsonToObject(requestBody, SimSwapRequest.class);
//			if (cclient != null) {
//				logs.setIp(cclient.getiP());
//				logs.setChannel(cclient.getChannel());
//				logs.setMsisdn(cclient.getmsisdn());
//				logs.setLang(cclient.getLang());
//			}
//		} catch (Exception ex) {
//			logger.error("Error:", ex);
//			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
//			resp.setReturnMsg(ResponseCodes.ERROR_401);
//			logs.setResponseCode(resp.getReturnCode());
//			logs.setResponseDescription(resp.getReturnMsg());
//			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
//			logs.updateLog(logs);
//			return resp;
//		}
//		if (cclient != null) {
//			String credentials = null;
//			try {
//				credentials = Decrypter.getInstance().decrypt(credential);
//				Helper.logInfoMessageV2(cclient.getmsisdn() + " - Credentials " + credential);
//			} catch (Exception ex) {
//				logger.error(Helper.GetException(ex));
//				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
//				resp.setReturnMsg(ResponseCodes.ERROR_401);
//				logs.setResponseCode(resp.getReturnCode());
//				logs.setResponseDescription(resp.getReturnMsg());
//				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
//				logs.updateLog(logs);
//				return resp;
//			}
//			if (credentials == null) {
//				Helper.logInfoMessageV2(cclient.getmsisdn() + " - Credentials are null");
//				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
//				resp.setReturnMsg(ResponseCodes.ERROR_401);
//				logs.setResponseCode(resp.getReturnCode());
//				logs.setResponseDescription(resp.getReturnMsg());
//				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
//				logs.updateLog(logs);
//				return resp;
//			}
//			if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
//				String verification = Helper.validateRequest(cclient);
//				if (!verification.equals("")) {
//					Helper.logInfoMessageV2(cclient.getmsisdn() + " - Verification failed");
//					resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
//					resp.setReturnMsg(verification);
//					logs.setResponseCode(resp.getReturnCode());
//					logs.setResponseDescription(resp.getReturnMsg());
//					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
//					logs.updateLog(logs);
//					return resp;
//				}
//			} else {
//				Helper.logInfoMessageV2(cclient.getmsisdn() + " - Incorrect MSISDN");
//				resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
//				resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
//				logs.setResponseCode(resp.getReturnCode());
//				logs.setResponseDescription(resp.getReturnMsg());
//				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
//				logs.updateLog(logs);
//				return resp;
//			}
//			if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
//
//				try {
//
//					logger.info(modeulName + ":" + Helper.ObjectToJson(cclient));
//
//						logger.info(modeulName + ":" + "Calling Submit Order");
//						com.huawei.crm.service.ens.SubmitOrderResponse submitOrderResponse = ngbssForSimSWAP(
//								cclient.getmsisdn(), cclient.getGsmNumber(), cclient.getIccid(),
//								cclient.getCustomerId(),modeulName,);
//
//						if (submitOrderResponse.getResponseHeader().getRetCode().equals("0")) {
//							
//							
//							logger.info(modeulName + ":" + "Waiting for The New Imsi");
//							Thread.sleep(26000);
//							
//							logger.info(modeulName + ":" + "Calling  GetSubscriber ");
//							GetSubscriberResponse respns = new com.evampsaanga.b2b.azerfon.getsubscriber.CRMSubscriberService()
//									.GetSubscriberRequest(cclient.getGsmNumber());
//							
//							logger.info(modeulName + ":" + " GetSubscriber Response is "+Helper.ObjectToJson(respns));
//							
//							if(respns.getResponseHeader().getRetCode().equalsIgnoreCase("0"))
//							{
//								
//								cclient.setImsi(respns.getGetSubscriberBody().getIMSI());
//								logger.info(modeulName + ":" + "New IMSI NUMBER IS "+cclient.getImsi());
//							}
//							
//							else
//							{
//								
//								logger.info(modeulName + ":" + "GetSubscriberResponse is not success");
//								resp.setReturnCode(respns.getResponseHeader().getRetCode());
//								resp.setReturnMsg(respns.getResponseHeader().getRetMsg());
//								return resp;
//								
//							}
//							
//							
//							
//							logger.info(modeulName + ":" + "Calling AddApplication after submit order success");
//							
//							
//							AddApplicationResponse addApplicationResponse = addApplication(cclient);
//							logger.info(modeulName + ":" + Helper.ObjectToJson(addApplicationResponse));
//
//							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
//							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
//							logs.setResponseCode(resp.getReturnCode());
//							logs.setResponseDescription(resp.getReturnMsg());
//							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
//							logs.updateLog(logs);
//						} else {
//							logger.info(modeulName + ":"
//									+ "Returning response because submit order api response was not success");
//							resp.setReturnCode(submitOrderResponse.getResponseHeader().getRetCode());
//							resp.setReturnMsg(submitOrderResponse.getResponseHeader().getRetMsg());
//							logs.setResponseCode(resp.getReturnCode());
//							logs.setResponseDescription(resp.getReturnMsg());
//							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
//							logs.updateLog(logs);
//						}
//
//
//					return resp;
//
//
//				} catch (Exception ex) {
//					logger.info(Helper.GetException(ex));
//					resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
//					resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
//					logs.setResponseCode(resp.getReturnCode());
//					logs.setResponseDescription(resp.getReturnMsg());
//					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
//					logs.updateLog(logs);
//					return resp;
//
//				}
//			} else {
//				Helper.logInfoMessageV2(cclient.getmsisdn() + " - 401 Access Not Authorized ");
//				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
//				resp.setReturnMsg(ResponseCodes.ERROR_401);
//				logs.setResponseCode(resp.getReturnCode());
//				logs.setResponseDescription(resp.getReturnMsg());
//				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
//				logs.updateLog(logs);
//				return resp;
//			}
//		}
//		// logs.updateLog(logs);
//		return resp;
//	}

	public com.huawei.crm.service.ens.SubmitOrderResponse ngbssForSimSWAP(String corporateMsisdn, String simMsisdn,
			String iccid, String customerId, String token , String specialGroup ) throws IOException, Exception {

		// String customerCrmAccountId =
		// Helper.getCustomerCRMAccountId(customerId,corporateMsisdn,simMsisdn);

		// TODO Auto-generated method stub
		com.huawei.crm.service.ens.SubmitOrderRequest submitOrderRequestMsgReq = new com.huawei.crm.service.ens.SubmitOrderRequest();
		submitOrderRequestMsgReq.setRequestHeader(getReqHeaderForGETNetworkSettingsimswap());
		com.huawei.crm.service.ens.SubmitRequestBody submitRequestBody = new com.huawei.crm.service.ens.SubmitRequestBody();
		OrderInfo oInfo = new OrderInfo();
		oInfo.setOrderType(Constants.SIM_SWAP_ORDERTYPE);// CO016
		OrderItemInfo oitemInfo = new OrderItemInfo();
		oitemInfo.setOrderItemType(Constants.SIM_SWAP_ORDERTYPE);
		oitemInfo.setIsCustomerNotification(Constants.SIM_SWAP_NOTIFICATION);
		oitemInfo.setIsPartnerNotification(Constants.SIM_SWAP_ISPARTNER_NOTIFICATION);
		submitRequestBody.setOrder(oInfo);
		OrderItems OrderItems = new OrderItems();
		OrderItemValue OrderItemValue = new OrderItemValue();
		OrderItemValue.setOrderItemInfo(oitemInfo);
		SubscriberInfo subscriber = new SubscriberInfo();
		subscriber.setServiceNumber(simMsisdn);
		subscriber.setICCID(iccid);
		BusinessFee businessFee = new BusinessFee();
		ChargeInfo chargeInfo = new ChargeInfo();
		chargeInfo.setFeeActionType("1");
		// chargeInfo.setAccountId(Long.parseLong(customerCrmAccountId));
		ChargeFeeList chargeFeeList = new ChargeFeeList();
		ChargeFeeInfo chargeFeeInfo = new ChargeFeeInfo();
		chargeFeeInfo.setChargeType("CC_CHANGE_SIM");
		chargeFeeInfo.setPayType("2");
		chargeFeeInfo.setCurrency("1009");
		
		
		//User is not in UG And We Are Charging
		if(specialGroup.equalsIgnoreCase("0"))
		{
			chargeFeeInfo.setUnitPrice(0L);
			chargeFeeInfo.setAmount(0L);
			chargeFeeInfo.setTaxAmount(0L);
		}
		
		//User is in UG And We Are not Charging
		if(specialGroup.equalsIgnoreCase("1"))
		{
			chargeFeeInfo.setUnitPrice(0L);
			chargeFeeInfo.setAmount(0L);
			chargeFeeInfo.setTaxAmount(0L);
		}
		
		chargeFeeInfo.setQuantity("1");
		chargeFeeInfo.setChargeRemark("test");
		chargeFeeList.getChargeFeeInfo().add(chargeFeeInfo);
		chargeInfo.setChargeFeeList(chargeFeeList);
		//chargeInfo.setFeeActionType("2");
		businessFee.setCharge(chargeInfo);

		

		OrderItemValue.setSubscriber(subscriber);
		OrderItemValue.setBusinessFee(businessFee);
		OrderItems.getOrderItem().add(OrderItemValue);
	
		submitRequestBody.setOrderItems(OrderItems);
		submitOrderRequestMsgReq.setSubmitRequestBody(submitRequestBody);
		
		
		
		
		
		com.huawei.crm.service.ens.SubmitOrderResponse response = OrderHandleService.getInstance()
				.submitOrder(submitOrderRequestMsgReq);
		logger.info(token + ":" + " SimSwap NGBSS Request "+Helper.ObjectToJson(submitOrderRequestMsgReq));
		return response;

	}

	public AddApplicationResponse addApplication(SimSwapRequest cclient) {
		AddApplicationRequest addApplicationRequest = new AddApplicationRequest();
		RequestAuthentication requestAuthentication = new RequestAuthentication();
		requestAuthentication.setRequestKey("JvCOn8H0jWuQ9U4mCfkW49OTo0");
		requestAuthentication.setRequestName("azerfon");
		addApplicationRequest.setAuthentication(requestAuthentication);
		RequestApplication requestApplication = new RequestApplication();
		// if(cclient.getAddType()!=null)
		// requestApplication.setAddType(cclient.getAddType());
		if (cclient.getContact() != null)
			requestApplication.setContact(cclient.getContact());
		if (cclient.getEmail() != null)
			requestApplication.setEmail(cclient.getEmail());
		if (cclient.getGsmNumber() != null)
			requestApplication.setGsmNumber(cclient.getGsmNumber());
		if (cclient.getIccid() != null)
			requestApplication.setICCID(cclient.getIccid());
		if (cclient.getImei() != null)
			requestApplication.setIMEI(cclient.getImei());
		if (cclient.getImsi() != null)
			requestApplication.setIMSI(cclient.getImsi());

		///////////////////// Adding Transaction Id in add
		///////////////////// Application///////////////////
		if (cclient.getTransactionId() != null)
			addApplicationRequest.setTransactionId(cclient.getTransactionId());
		///////////////////// ArrayOfRequestApplication
		///////////////////// //////////////////////////////////
		
		RequestApplicationTypes value = new RequestApplicationTypes();
		value.setAddType("3");
		value.setDeviceType("90");
		value.setUsingType("1");
		value.setSignatureType("1");
		addApplicationRequest.setApplicationTypes(value);
		ArrayOfRequestApplication arrayOfRequestApplication = new ArrayOfRequestApplication();
		arrayOfRequestApplication.getRequestApplication().add(requestApplication);
		addApplicationRequest.setApplicationInformation(arrayOfRequestApplication);
		/////////////////////////////////////////////////////////////////////////////////
		///////////////////// Request Application Types
		///////////////////////////////////////////////////////////////////////////////// ////////////////////////////////
		// RequestApplicationTypes requestApplicationTypes = new
		// RequestApplicationTypes();
		// requestApplicationTypes.setAddType("");
		// requestApplicationTypes.setDeviceType("");
		// requestApplicationTypes.setSignatureType("");
		// requestApplicationTypes.setUsingType("");
		// addApplicationRequest.setApplicationTypes(requestApplicationTypes);
		//////////////////////////////////////////////////////////////////////////////////

		ResponseApplicationInfo responseApplicationInfo = MHMService.getInstance()
				.addApplication(addApplicationRequest);
		AddApplicationResponse addApplicationResponse = new AddApplicationResponse();
		addApplicationResponse.setAddApplicationResult(responseApplicationInfo);
		return addApplicationResponse;
	}

	private static ResponseInformationByPin checkDocumentValid(SimSwapRequest cclient) {
		// TODO Auto-generated method stub

		CheckDocumentValidRequest checkDocumentValidRequest = new CheckDocumentValidRequest();
		RequestAuthentication requestAuthentication = new RequestAuthentication();
		requestAuthentication.setRequestKey("ywEZtV5u0KRhY3MRH8WXNQn00");
		requestAuthentication.setRequestName("bakcell");
		checkDocumentValidRequest.setAuthentication(requestAuthentication);

		RequestSellerInformation requestSellerInformation = new RequestSellerInformation();
		requestSellerInformation.setPlaceAddress("Bakcellim");
		requestSellerInformation.setPlaceCode("555909483");
		requestSellerInformation.setPlaceName("Bakcellim");
		requestSellerInformation.setPlaceType("1");

		requestSellerInformation.setSellerContact("555902541");
		requestSellerInformation.setSellerDocumentNumber("Test");
		requestSellerInformation.setSellerDocumentPin("Test");
		requestSellerInformation.setSellerFullname("Test");
		requestSellerInformation.setSellerUsername("994555909483");
		requestSellerInformation.setRegions("10200");

		RequestDocumentInformation requestDocumentInformation = new RequestDocumentInformation();
		if (cclient.getDocumentNumber() != null) {
			requestDocumentInformation.setDocumentNumber(cclient.getDocumentNumber());
		}
		if (cclient.getDocumentPin() != null)
			requestDocumentInformation.setDocumentPin(cclient.getDocumentPin());
		if (cclient.getDocumentType() != null) {
			if (cclient.getDocumentType().equals("2"))
				requestDocumentInformation.setDocumentType("4");
			else
				requestDocumentInformation.setDocumentType(cclient.getDocumentType());
		}
		checkDocumentValidRequest.setSellerInformation(requestSellerInformation);
		checkDocumentValidRequest.setDocumentInformation(requestDocumentInformation);

		ResponseInformationByPin responseInformationByPin = MHMService.getInstance()
				.checkDocumentValid(checkDocumentValidRequest);
		return responseInformationByPin;

	}

	private static com.huawei.crm.basetype.ens.RequestHeader getReqHeaderForGETNetworkSettingsimswap() {
		com.huawei.crm.basetype.ens.RequestHeader reqH = new com.huawei.crm.basetype.ens.RequestHeader();
		reqH.setVersion("1");
		reqH.setChannelId("3");
		reqH.setTechnicalChannelId("53");
		reqH.setAccessUser("ecare");
		reqH.setAccessPwd("Abc1234%");
		reqH.setTransactionId(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
		reqH.setTenantId("101");
		reqH.setTestFlag("0");
		reqH.setLanguage("2002");

		return reqH;
	}
	
	private static com.huawei.crm.basetype.RequestHeader getReqHeaderForCustomerData() {
		com.huawei.crm.basetype.RequestHeader reqH = new com.huawei.crm.basetype.RequestHeader();
		reqH.setVersion("1");
		reqH.setChannelId("3");
		reqH.setTechnicalChannelId("53");
		reqH.setAccessUser("ecare");
		reqH.setAccessPwd("Abc1234%");
		reqH.setTransactionId(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
		reqH.setTenantId("101");
		reqH.setTestFlag("0");
		reqH.setLanguage("2002");

		return reqH;
	}
	

	private static com.huawei.crm.basetype.RequestHeader getReqHeaderForGETNetworkSetting() {
		com.huawei.crm.basetype.RequestHeader reqH = new com.huawei.crm.basetype.RequestHeader();
		reqH.setVersion("1");
		reqH.setChannelId("3");
		reqH.setTechnicalChannelId("53");
		reqH.setAccessUser("ecare");
		reqH.setAccessPwd("Abc1234%");
		reqH.setTransactionId(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
		reqH.setTenantId("101");
		reqH.setTestFlag("0");
		reqH.setLanguage("2002");

		return reqH;
	}

	private static CheckVoenValidResponse checkVoienValid(SimSwapRequest cclient) {
		// TODO Auto-generated method stub
		CheckVoenValidRequest checkVoenValidRequest = new CheckVoenValidRequest();
		RequestAuthentication requestAuthentication = new RequestAuthentication();
		requestAuthentication.setRequestName("azerfon");
		requestAuthentication.setRequestKey("JvCOn8H0jWuQ9U4mCfkW49OTo0");

		checkVoenValidRequest.setAuthentication(requestAuthentication);

		RequestVoenInformation requestVoenInformation = new RequestVoenInformation();
		requestVoenInformation.setSun(cclient.getSun());
		requestVoenInformation.setVoenNumber(cclient.getVoen());

		checkVoenValidRequest.setVoenInformation(requestVoenInformation);

		RequestDocumentInformation requestDocumentInformation = new RequestDocumentInformation();
		if (cclient.getDocumentNumber() != null)
			requestDocumentInformation.setDocumentNumber(cclient.getDocumentNumber());
		if (cclient.getDocumentPin() != null)
			requestDocumentInformation.setDocumentPin(cclient.getDocumentPin());
		if (cclient.getDocumentType() != null)
			requestDocumentInformation.setDocumentType(cclient.getDocumentType());

		checkVoenValidRequest.setDocumentInformation(requestDocumentInformation);

		RequestSellerInformation requestSellerInformation = new RequestSellerInformation();
		// requestSellerInformation.setPlaceType(cclient.getPlaceType());
		// requestSellerInformation.setPlaceName(cclient.getPlaceName());
		// requestSellerInformation.setPlaceAddress(cclient.getPlaceAddress());
		// requestSellerInformation.setSellerUsername(cclient.getSellerUsername());
		// requestSellerInformation.setSellerDocumentNumber(cclient.getSellerDocumentPin());
		// requestSellerInformation.setSellerDocumentPin(cclient.getSellerDocumentPin());
		// requestSellerInformation.setSellerFullname(cclient.getSellerFullName());
		// requestSellerInformation.setSellerContact(cclient.getSellerContact());

		// HardCoding for Testing
		requestSellerInformation.setPlaceType("1");
		requestSellerInformation.setRegions("10130");
		requestSellerInformation.setPlaceCode("7777");
		requestSellerInformation.setPlaceName("B2B Sales");
		requestSellerInformation.setPlaceAddress("Neftchiler Ave.Port Baku Business Center");
		requestSellerInformation.setSellerUsername("Azerfon LLC");
		requestSellerInformation.setSellerFullname("B2B Sales");
		requestSellerInformation.setSellerContact("772013333");

		checkVoenValidRequest.setSellerInformation(requestSellerInformation);
		ResponseInformationByVoen responseInformationByPin = MHMService.getInstance()
				.checkVoenValid(checkVoenValidRequest);
		CheckVoenValidResponse checkVoenValidResponse = new CheckVoenValidResponse();
		checkVoenValidResponse.setCheckVoenValidResult(responseInformationByPin);
		return checkVoenValidResponse;

	}

	public static int checkSpecialGroup(GetGroupResponse getGroupResponse) {
		logger.info(modeulName + ":" + "Landing in checkSpecialGroup method ");
		if (getGroupResponse.getGetGroupBody().getGetGroupDataList().size() > 0) {
			ArrayList<String> groupIds = new ArrayList<>();
			for (int i = 0; i < getGroupResponse.getGetGroupBody().getGetGroupDataList().size(); i++) {

				groupIds.add(getGroupResponse.getGetGroupBody().getGetGroupDataList().get(i).getGroupNo());
			}

			String groupIdsDb = ConfigurationManager.getConfigurationFromCache("sim.swap.offering.ids");

			List<String> groupList = new ArrayList<>();
			groupList = Arrays.asList(groupIdsDb.split("\\s*,\\s*"));

			for (int i = 0; i < groupIds.size(); i++) {
				for (int j = 0; j < groupList.size(); j++) {
					if (groupIds.get(i).equalsIgnoreCase(groupList.get(j))) {

						logger.info(modeulName + ":" + "Returning 1 due to match found  in special group");
						return 1;
					}
				}

			}
			logger.info(modeulName + ":" + "Returning 0 due to no match found  in special group");
			return 0;
		}

		logger.info(modeulName + ":" + "Returning 0 due to  there is no groups against this msisdn");
		return 0;
	}

	public static void main(String[] args) {
		String groupIdsDb = "1222,2222,222333,444";
		List<String> groupList = new ArrayList<>();
		groupList = Arrays.asList(groupIdsDb.split("\\s*,\\s*"));
		for (int i = 0; i < groupList.size(); i++) {
			System.out.println(groupList.get(i));
		}
	}

}
