
package com.evampsaanga.b2b.magento.tariffdetailsv2;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "pricesSectionLabel",
    "Call",
    "SMS",
    "Internet"
})
public class Prices {

    @JsonProperty("pricesSectionLabel")
    private String pricesSectionLabel;
    @JsonProperty("Call")
    private Call___ call;
    @JsonProperty("SMS")
    private SMS__ sMS;
    @JsonProperty("Internet")
    private Internet__ internet;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("pricesSectionLabel")
    public String getPricesSectionLabel() {
        return pricesSectionLabel;
    }

    @JsonProperty("pricesSectionLabel")
    public void setPricesSectionLabel(String pricesSectionLabel) {
        this.pricesSectionLabel = pricesSectionLabel;
    }

    @JsonProperty("Call")
    public Call___ getCall() {
        return call;
    }

    @JsonProperty("Call")
    public void setCall(Call___ call) {
        this.call = call;
    }

    @JsonProperty("SMS")
    public SMS__ getSMS() {
        return sMS;
    }

    @JsonProperty("SMS")
    public void setSMS(SMS__ sMS) {
        this.sMS = sMS;
    }

    @JsonProperty("Internet")
    public Internet__ getInternet() {
        return internet;
    }

    @JsonProperty("Internet")
    public void setInternet(Internet__ internet) {
        this.internet = internet;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
