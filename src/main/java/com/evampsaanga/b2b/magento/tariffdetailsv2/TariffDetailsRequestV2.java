package com.evampsaanga.b2b.magento.tariffdetailsv2;

import java.util.Arrays;

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;

public class TariffDetailsRequestV2 extends BaseRequest {
	private String[] offeringId;
	private String lang;

	public String[] getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(String[] offeringId) {
		this.offeringId = offeringId;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	@Override
	public String toString() {
		return "TariffDetailsRequestV2 [offeringId=" + Arrays.toString(offeringId) + ", lang=" + lang + "]";
	}

}
