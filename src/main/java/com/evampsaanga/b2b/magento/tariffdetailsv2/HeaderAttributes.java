package com.evampsaanga.b2b.magento.tariffdetailsv2;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class HeaderAttributes {
	 @JsonProperty("type")
	    private String type;
	    @JsonProperty("tariffType")
	    private String tariffType;
	    @JsonProperty("sortOrder")
	    private String sortOrder;
	    @JsonProperty("id")
	    private String id;
	    @JsonProperty("offeringName")
	    private String offeringName;
	    @JsonProperty("offeringId")
	    private String offeringId;
	    @JsonProperty("tagIcon")
	    private String tagIcon;
	    @JsonProperty("tag")
	    private String tag;
	    @JsonProperty("currency")
	    private String currency;
	    @JsonProperty("name")
	    private String name;
	    @JsonProperty("mrcLabel")
	    private String mrcLabel;
	    @JsonProperty("mrcValue")
	    private String mrcValue;
	    
	    private List<Datum> attributes = null;

}
