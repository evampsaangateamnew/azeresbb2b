
package com.evampsaanga.b2b.magento.tariffdetailsv2;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "callIcon",
    "callLabel",
    "callValue",
    "callOnnetOutOfRegionLabel",
    "callOnnetOutOfRegionValue",
    "callOffnetOutOfRegionLabel",
    "callOffnetOutOfRegionValue",
    "titleThree",
    "valueThree",
    "titleFour",
    "valueFour",
    "callDescription"
})
public class Call__ {

    @JsonProperty("callIcon")
    private String callIcon;
    @JsonProperty("callLabel")
    private String callLabel;
    @JsonProperty("callValue")
    private String callValue;
    @JsonProperty("callOnnetOutOfRegionLabel")
    private String callOnnetOutOfRegionLabel;
    @JsonProperty("callOnnetOutOfRegionValue")
    private String callOnnetOutOfRegionValue;
    @JsonProperty("callOffnetOutOfRegionLabel")
    private String callOffnetOutOfRegionLabel;
    @JsonProperty("callOffnetOutOfRegionValue")
    private String callOffnetOutOfRegionValue;
    @JsonProperty("titleThree")
    private String titleThree;
    @JsonProperty("valueThree")
    private String valueThree;
    @JsonProperty("titleFour")
    private String titleFour;
    @JsonProperty("valueFour")
    private String valueFour;
    @JsonProperty("callDescription")
    private String callDescription;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("callIcon")
    public String getCallIcon() {
        return callIcon;
    }

    @JsonProperty("callIcon")
    public void setCallIcon(String callIcon) {
        this.callIcon = callIcon;
    }

    @JsonProperty("callLabel")
    public String getCallLabel() {
        return callLabel;
    }

    @JsonProperty("callLabel")
    public void setCallLabel(String callLabel) {
        this.callLabel = callLabel;
    }

    @JsonProperty("callValue")
    public String getCallValue() {
        return callValue;
    }

    @JsonProperty("callValue")
    public void setCallValue(String callValue) {
        this.callValue = callValue;
    }

    @JsonProperty("callOnnetOutOfRegionLabel")
    public String getCallOnnetOutOfRegionLabel() {
        return callOnnetOutOfRegionLabel;
    }

    @JsonProperty("callOnnetOutOfRegionLabel")
    public void setCallOnnetOutOfRegionLabel(String callOnnetOutOfRegionLabel) {
        this.callOnnetOutOfRegionLabel = callOnnetOutOfRegionLabel;
    }

    @JsonProperty("callOnnetOutOfRegionValue")
    public String getCallOnnetOutOfRegionValue() {
        return callOnnetOutOfRegionValue;
    }

    @JsonProperty("callOnnetOutOfRegionValue")
    public void setCallOnnetOutOfRegionValue(String callOnnetOutOfRegionValue) {
        this.callOnnetOutOfRegionValue = callOnnetOutOfRegionValue;
    }

    @JsonProperty("callOffnetOutOfRegionLabel")
    public String getCallOffnetOutOfRegionLabel() {
        return callOffnetOutOfRegionLabel;
    }

    @JsonProperty("callOffnetOutOfRegionLabel")
    public void setCallOffnetOutOfRegionLabel(String callOffnetOutOfRegionLabel) {
        this.callOffnetOutOfRegionLabel = callOffnetOutOfRegionLabel;
    }

    @JsonProperty("callOffnetOutOfRegionValue")
    public String getCallOffnetOutOfRegionValue() {
        return callOffnetOutOfRegionValue;
    }

    @JsonProperty("callOffnetOutOfRegionValue")
    public void setCallOffnetOutOfRegionValue(String callOffnetOutOfRegionValue) {
        this.callOffnetOutOfRegionValue = callOffnetOutOfRegionValue;
    }

    @JsonProperty("titleThree")
    public String getTitleThree() {
        return titleThree;
    }

    @JsonProperty("titleThree")
    public void setTitleThree(String titleThree) {
        this.titleThree = titleThree;
    }

    @JsonProperty("valueThree")
    public String getValueThree() {
        return valueThree;
    }

    @JsonProperty("valueThree")
    public void setValueThree(String valueThree) {
        this.valueThree = valueThree;
    }

    @JsonProperty("titleFour")
    public String getTitleFour() {
        return titleFour;
    }

    @JsonProperty("titleFour")
    public void setTitleFour(String titleFour) {
        this.titleFour = titleFour;
    }

    @JsonProperty("valueFour")
    public String getValueFour() {
        return valueFour;
    }

    @JsonProperty("valueFour")
    public void setValueFour(String valueFour) {
        this.valueFour = valueFour;
    }

    @JsonProperty("callDescription")
    public String getCallDescription() {
        return callDescription;
    }

    @JsonProperty("callDescription")
    public void setCallDescription(String callDescription) {
        this.callDescription = callDescription;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
