
package com.evampsaanga.b2b.magento.tariffdetailsv2;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "countryWideIcon",
    "countryWideLabel",
    "countryWideValue",
    "countryWideMetrics"
})
public class CountryWide {

    @JsonProperty("countryWideIcon")
    private String countryWideIcon;
    @JsonProperty("countryWideLabel")
    private String countryWideLabel;
    @JsonProperty("countryWideValue")
    private String countryWideValue;
    @JsonProperty("countryWideMetrics")
    private String countryWideMetrics;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("countryWideIcon")
    public String getCountryWideIcon() {
        return countryWideIcon;
    }

    @JsonProperty("countryWideIcon")
    public void setCountryWideIcon(String countryWideIcon) {
        this.countryWideIcon = countryWideIcon;
    }

    @JsonProperty("countryWideLabel")
    public String getCountryWideLabel() {
        return countryWideLabel;
    }

    @JsonProperty("countryWideLabel")
    public void setCountryWideLabel(String countryWideLabel) {
        this.countryWideLabel = countryWideLabel;
    }

    @JsonProperty("countryWideValue")
    public String getCountryWideValue() {
        return countryWideValue;
    }

    @JsonProperty("countryWideValue")
    public void setCountryWideValue(String countryWideValue) {
        this.countryWideValue = countryWideValue;
    }

    @JsonProperty("countryWideMetrics")
    public String getCountryWideMetrics() {
        return countryWideMetrics;
    }

    @JsonProperty("countryWideMetrics")
    public void setCountryWideMetrics(String countryWideMetrics) {
        this.countryWideMetrics = countryWideMetrics;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
