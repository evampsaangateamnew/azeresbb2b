
package com.evampsaanga.b2b.magento.tariffdetailsv2;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "whatsappIcon",
    "whatsappLabel",
    "whatsappValue",
    "whatsappMetrics"
})
public class Whatsapp {

    @JsonProperty("whatsappIcon")
    private String whatsappIcon;
    @JsonProperty("whatsappLabel")
    private String whatsappLabel;
    @JsonProperty("whatsappValue")
    private String whatsappValue;
    @JsonProperty("whatsappMetrics")
    private String whatsappMetrics;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("whatsappIcon")
    public String getWhatsappIcon() {
        return whatsappIcon;
    }

    @JsonProperty("whatsappIcon")
    public void setWhatsappIcon(String whatsappIcon) {
        this.whatsappIcon = whatsappIcon;
    }

    @JsonProperty("whatsappLabel")
    public String getWhatsappLabel() {
        return whatsappLabel;
    }

    @JsonProperty("whatsappLabel")
    public void setWhatsappLabel(String whatsappLabel) {
        this.whatsappLabel = whatsappLabel;
    }

    @JsonProperty("whatsappValue")
    public String getWhatsappValue() {
        return whatsappValue;
    }

    @JsonProperty("whatsappValue")
    public void setWhatsappValue(String whatsappValue) {
        this.whatsappValue = whatsappValue;
    }

    @JsonProperty("whatsappMetrics")
    public String getWhatsappMetrics() {
        return whatsappMetrics;
    }

    @JsonProperty("whatsappMetrics")
    public void setWhatsappMetrics(String whatsappMetrics) {
        this.whatsappMetrics = whatsappMetrics;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
