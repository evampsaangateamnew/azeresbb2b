
package com.evampsaanga.b2b.magento.tariffdetailsv2;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "tariffType",
    "header",
    "packagePrices",
    "details",
    "prices",
    "description"
})
public class Datum {

    @JsonProperty("tariffType")
    private String tariffType;
    @JsonProperty("header")
    private Header header;
    @JsonProperty("packagePrices")
    private PackagePrices packagePrices;
    @JsonProperty("details")
    private Details details;
    @JsonProperty("prices")
    private Prices prices;
    @JsonProperty("description")
    private Description description;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("tariffType")
    public String getTariffType() {
        return tariffType;
    }

    @JsonProperty("tariffType")
    public void setTariffType(String tariffType) {
        this.tariffType = tariffType;
    }

    @JsonProperty("header")
    public Header getHeader() {
        return header;
    }

    @JsonProperty("header")
    public void setHeader(Header header) {
        this.header = header;
    }

    @JsonProperty("packagePrices")
    public PackagePrices getPackagePrices() {
        return packagePrices;
    }

    @JsonProperty("packagePrices")
    public void setPackagePrices(PackagePrices packagePrices) {
        this.packagePrices = packagePrices;
    }

    @JsonProperty("details")
    public Details getDetails() {
        return details;
    }

    @JsonProperty("details")
    public void setDetails(Details details) {
        this.details = details;
    }

    @JsonProperty("prices")
    public Prices getPrices() {
        return prices;
    }

    @JsonProperty("prices")
    public void setPrices(Prices prices) {
        this.prices = prices;
    }

    @JsonProperty("description")
    public Description getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(Description description) {
        this.description = description;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
