
package com.evampsaanga.b2b.magento.tariffdetailsv2;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "priceTemplate",
    "callIcon",
    "callLabel",
    "callValueA",
    "callValueB",
    "onnetLabel",
    "onnetValueA",
    "onnetValueB",
    "offnetLabel",
    "offnetValueA",
    "offnetValueB",
    "shortDescription"
})
public class Call_ {

    @JsonProperty("priceTemplate")
    private String priceTemplate;
    @JsonProperty("callIcon")
    private String callIcon;
    @JsonProperty("callLabel")
    private String callLabel;
    @JsonProperty("callValueA")
    private String callValueA;
    @JsonProperty("callValueB")
    private String callValueB;
    @JsonProperty("onnetLabel")
    private String onnetLabel;
    @JsonProperty("onnetValueA")
    private String onnetValueA;
    @JsonProperty("onnetValueB")
    private String onnetValueB;
    @JsonProperty("offnetLabel")
    private String offnetLabel;
    @JsonProperty("offnetValueA")
    private String offnetValueA;
    @JsonProperty("offnetValueB")
    private String offnetValueB;
    @JsonProperty("shortDescription")
    private String shortDescription;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("priceTemplate")
    public String getPriceTemplate() {
        return priceTemplate;
    }

    @JsonProperty("priceTemplate")
    public void setPriceTemplate(String priceTemplate) {
        this.priceTemplate = priceTemplate;
    }

    @JsonProperty("callIcon")
    public String getCallIcon() {
        return callIcon;
    }

    @JsonProperty("callIcon")
    public void setCallIcon(String callIcon) {
        this.callIcon = callIcon;
    }

    @JsonProperty("callLabel")
    public String getCallLabel() {
        return callLabel;
    }

    @JsonProperty("callLabel")
    public void setCallLabel(String callLabel) {
        this.callLabel = callLabel;
    }

    @JsonProperty("callValueA")
    public String getCallValueA() {
        return callValueA;
    }

    @JsonProperty("callValueA")
    public void setCallValueA(String callValueA) {
        this.callValueA = callValueA;
    }

    @JsonProperty("callValueB")
    public String getCallValueB() {
        return callValueB;
    }

    @JsonProperty("callValueB")
    public void setCallValueB(String callValueB) {
        this.callValueB = callValueB;
    }

    @JsonProperty("onnetLabel")
    public String getOnnetLabel() {
        return onnetLabel;
    }

    @JsonProperty("onnetLabel")
    public void setOnnetLabel(String onnetLabel) {
        this.onnetLabel = onnetLabel;
    }

    @JsonProperty("onnetValueA")
    public String getOnnetValueA() {
        return onnetValueA;
    }

    @JsonProperty("onnetValueA")
    public void setOnnetValueA(String onnetValueA) {
        this.onnetValueA = onnetValueA;
    }

    @JsonProperty("onnetValueB")
    public String getOnnetValueB() {
        return onnetValueB;
    }

    @JsonProperty("onnetValueB")
    public void setOnnetValueB(String onnetValueB) {
        this.onnetValueB = onnetValueB;
    }

    @JsonProperty("offnetLabel")
    public String getOffnetLabel() {
        return offnetLabel;
    }

    @JsonProperty("offnetLabel")
    public void setOffnetLabel(String offnetLabel) {
        this.offnetLabel = offnetLabel;
    }

    @JsonProperty("offnetValueA")
    public String getOffnetValueA() {
        return offnetValueA;
    }

    @JsonProperty("offnetValueA")
    public void setOffnetValueA(String offnetValueA) {
        this.offnetValueA = offnetValueA;
    }

    @JsonProperty("offnetValueB")
    public String getOffnetValueB() {
        return offnetValueB;
    }

    @JsonProperty("offnetValueB")
    public void setOffnetValueB(String offnetValueB) {
        this.offnetValueB = offnetValueB;
    }

    @JsonProperty("shortDescription")
    public String getShortDescription() {
        return shortDescription;
    }

    @JsonProperty("shortDescription")
    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
