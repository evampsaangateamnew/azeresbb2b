
package com.evampsaanga.b2b.magento.tariffdetailsv2;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "descriptionSectionLabel",
    "advantagesLabel",
    "advantagesValue",
    "clarificationLabel",
    "clarificationValue"
})
public class Description {

    @JsonProperty("descriptionSectionLabel")
    private String descriptionSectionLabel;
    @JsonProperty("advantagesLabel")
    private String advantagesLabel;
    @JsonProperty("advantagesValue")
    private String advantagesValue;
    @JsonProperty("clarificationLabel")
    private String clarificationLabel;
    @JsonProperty("clarificationValue")
    private String clarificationValue;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("descriptionSectionLabel")
    public String getDescriptionSectionLabel() {
        return descriptionSectionLabel;
    }

    @JsonProperty("descriptionSectionLabel")
    public void setDescriptionSectionLabel(String descriptionSectionLabel) {
        this.descriptionSectionLabel = descriptionSectionLabel;
    }

    @JsonProperty("advantagesLabel")
    public String getAdvantagesLabel() {
        return advantagesLabel;
    }

    @JsonProperty("advantagesLabel")
    public void setAdvantagesLabel(String advantagesLabel) {
        this.advantagesLabel = advantagesLabel;
    }

    @JsonProperty("advantagesValue")
    public String getAdvantagesValue() {
        return advantagesValue;
    }

    @JsonProperty("advantagesValue")
    public void setAdvantagesValue(String advantagesValue) {
        this.advantagesValue = advantagesValue;
    }

    @JsonProperty("clarificationLabel")
    public String getClarificationLabel() {
        return clarificationLabel;
    }

    @JsonProperty("clarificationLabel")
    public void setClarificationLabel(String clarificationLabel) {
        this.clarificationLabel = clarificationLabel;
    }

    @JsonProperty("clarificationValue")
    public String getClarificationValue() {
        return clarificationValue;
    }

    @JsonProperty("clarificationValue")
    public void setClarificationValue(String clarificationValue) {
        this.clarificationValue = clarificationValue;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
