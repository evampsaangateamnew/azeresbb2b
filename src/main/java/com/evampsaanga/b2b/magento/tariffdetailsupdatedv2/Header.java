
package com.evampsaanga.b2b.magento.tariffdetailsupdatedv2;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "type",
    "sortOrder",
    "id",
    "offeringName",
    "offeringId",
    "tagIcon",
    "tag",
    "currency",
    "name",
    "mrcLabel",
    "mrcValue",
    "attributes"
})
public class Header {

    @JsonProperty("type")
    private String type;
    @JsonProperty("sortOrder")
    private String sortOrder;
    @JsonProperty("id")
    private String id;
    @JsonProperty("offeringName")
    private String offeringName;
    @JsonProperty("offeringId")
    private String offeringId;
    @JsonProperty("tagIcon")
    private String tagIcon;
    @JsonProperty("tag")
    private String tag;
    @JsonProperty("currency")
    private String currency;
    @JsonProperty("name")
    private String name;
    @JsonProperty("mrcLabel")
    private String mrcLabel;
    @JsonProperty("mrcValue")
    private String mrcValue;
    @JsonProperty("attributes")
    private List<Attribute> attributes = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("sortOrder")
    public String getSortOrder() {
        return sortOrder;
    }

    @JsonProperty("sortOrder")
    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("offeringName")
    public String getOfferingName() {
        return offeringName;
    }

    @JsonProperty("offeringName")
    public void setOfferingName(String offeringName) {
        this.offeringName = offeringName;
    }

    @JsonProperty("offeringId")
    public String getOfferingId() {
        return offeringId;
    }

    @JsonProperty("offeringId")
    public void setOfferingId(String offeringId) {
        this.offeringId = offeringId;
    }

    @JsonProperty("tagIcon")
    public String getTagIcon() {
        return tagIcon;
    }

    @JsonProperty("tagIcon")
    public void setTagIcon(String tagIcon) {
        this.tagIcon = tagIcon;
    }

    @JsonProperty("tag")
    public String getTag() {
        return tag;
    }

    @JsonProperty("tag")
    public void setTag(String tag) {
        this.tag = tag;
    }

    @JsonProperty("currency")
    public String getCurrency() {
        return currency;
    }

    @JsonProperty("currency")
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("mrcLabel")
    public String getMrcLabel() {
        return mrcLabel;
    }

    @JsonProperty("mrcLabel")
    public void setMrcLabel(String mrcLabel) {
        this.mrcLabel = mrcLabel;
    }

    @JsonProperty("mrcValue")
    public String getMrcValue() {
        return mrcValue;
    }

    @JsonProperty("mrcValue")
    public void setMrcValue(String mrcValue) {
        this.mrcValue = mrcValue;
    }

    @JsonProperty("attributes")
    public List<Attribute> getAttributes() {
        return attributes;
    }

    @JsonProperty("attributes")
    public void setAttributes(List<Attribute> attributes) {
        this.attributes = attributes;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
