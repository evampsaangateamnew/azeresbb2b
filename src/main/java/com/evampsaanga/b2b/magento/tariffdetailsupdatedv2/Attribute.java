
package com.evampsaanga.b2b.magento.tariffdetailsupdatedv2;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "callIcon",
    "callLabel",
    "callValue",
    "callMetrics",
    "smsIcon",
    "smsLabel",
    "smsValue",
    "smsMetrics",
    "internetIcon",
    "internetLabel",
    "internetValue",
    "internetMetrics",
    "countryWideIcon",
    "countryWideLabel",
    "countryWideValue",
    "countryWideMetrics",
    "whatsappIcon",
    "whatsappLabel",
    "whatsappValue",
    "whatsappMetrics",
    "icon",
    "label",
    "value",
    "metrics"
})
public class Attribute {

    @JsonProperty("callIcon")
    private String callIcon;
    @JsonProperty("callLabel")
    private String callLabel;
    @JsonProperty("callValue")
    private String callValue;
    @JsonProperty("callMetrics")
    private String callMetrics;
    @JsonProperty("smsIcon")
    private String smsIcon;
    @JsonProperty("smsLabel")
    private String smsLabel;
    @JsonProperty("smsValue")
    private String smsValue;
    @JsonProperty("smsMetrics")
    private String smsMetrics;
    @JsonProperty("internetIcon")
    private String internetIcon;
    @JsonProperty("internetLabel")
    private String internetLabel;
    @JsonProperty("internetValue")
    private String internetValue;
    @JsonProperty("internetMetrics")
    private String internetMetrics;
    @JsonProperty("countryWideIcon")
    private String countryWideIcon;
    @JsonProperty("countryWideLabel")
    private String countryWideLabel;
    @JsonProperty("countryWideValue")
    private String countryWideValue;
    @JsonProperty("countryWideMetrics")
    private String countryWideMetrics;
    @JsonProperty("whatsappIcon")
    private String whatsappIcon;
    @JsonProperty("whatsappLabel")
    private String whatsappLabel;
    @JsonProperty("whatsappValue")
    private String whatsappValue;
    @JsonProperty("whatsappMetrics")
    private String whatsappMetrics;
    @JsonProperty("icon")
    private String icon;
    @JsonProperty("label")
    private String label;
    @JsonProperty("value")
    private String value;
    @JsonProperty("metrics")
    private String metrics;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("callIcon")
    public String getCallIcon() {
        return callIcon;
    }

    @JsonProperty("callIcon")
    public void setCallIcon(String callIcon) {
        this.callIcon = callIcon;
    }

    @JsonProperty("callLabel")
    public String getCallLabel() {
        return callLabel;
    }

    @JsonProperty("callLabel")
    public void setCallLabel(String callLabel) {
        this.callLabel = callLabel;
    }

    @JsonProperty("callValue")
    public String getCallValue() {
        return callValue;
    }

    @JsonProperty("callValue")
    public void setCallValue(String callValue) {
        this.callValue = callValue;
    }

    @JsonProperty("callMetrics")
    public String getCallMetrics() {
        return callMetrics;
    }

    @JsonProperty("callMetrics")
    public void setCallMetrics(String callMetrics) {
        this.callMetrics = callMetrics;
    }

    @JsonProperty("smsIcon")
    public String getSmsIcon() {
        return smsIcon;
    }

    @JsonProperty("smsIcon")
    public void setSmsIcon(String smsIcon) {
        this.smsIcon = smsIcon;
    }

    @JsonProperty("smsLabel")
    public String getSmsLabel() {
        return smsLabel;
    }

    @JsonProperty("smsLabel")
    public void setSmsLabel(String smsLabel) {
        this.smsLabel = smsLabel;
    }

    @JsonProperty("smsValue")
    public String getSmsValue() {
        return smsValue;
    }

    @JsonProperty("smsValue")
    public void setSmsValue(String smsValue) {
        this.smsValue = smsValue;
    }

    @JsonProperty("smsMetrics")
    public String getSmsMetrics() {
        return smsMetrics;
    }

    @JsonProperty("smsMetrics")
    public void setSmsMetrics(String smsMetrics) {
        this.smsMetrics = smsMetrics;
    }

    @JsonProperty("internetIcon")
    public String getInternetIcon() {
        return internetIcon;
    }

    @JsonProperty("internetIcon")
    public void setInternetIcon(String internetIcon) {
        this.internetIcon = internetIcon;
    }

    @JsonProperty("internetLabel")
    public String getInternetLabel() {
        return internetLabel;
    }

    @JsonProperty("internetLabel")
    public void setInternetLabel(String internetLabel) {
        this.internetLabel = internetLabel;
    }

    @JsonProperty("internetValue")
    public String getInternetValue() {
        return internetValue;
    }

    @JsonProperty("internetValue")
    public void setInternetValue(String internetValue) {
        this.internetValue = internetValue;
    }

    @JsonProperty("internetMetrics")
    public String getInternetMetrics() {
        return internetMetrics;
    }

    @JsonProperty("internetMetrics")
    public void setInternetMetrics(String internetMetrics) {
        this.internetMetrics = internetMetrics;
    }

    @JsonProperty("countryWideIcon")
    public String getCountryWideIcon() {
        return countryWideIcon;
    }

    @JsonProperty("countryWideIcon")
    public void setCountryWideIcon(String countryWideIcon) {
        this.countryWideIcon = countryWideIcon;
    }

    @JsonProperty("countryWideLabel")
    public String getCountryWideLabel() {
        return countryWideLabel;
    }

    @JsonProperty("countryWideLabel")
    public void setCountryWideLabel(String countryWideLabel) {
        this.countryWideLabel = countryWideLabel;
    }

    @JsonProperty("countryWideValue")
    public String getCountryWideValue() {
        return countryWideValue;
    }

    @JsonProperty("countryWideValue")
    public void setCountryWideValue(String countryWideValue) {
        this.countryWideValue = countryWideValue;
    }

    @JsonProperty("countryWideMetrics")
    public String getCountryWideMetrics() {
        return countryWideMetrics;
    }

    @JsonProperty("countryWideMetrics")
    public void setCountryWideMetrics(String countryWideMetrics) {
        this.countryWideMetrics = countryWideMetrics;
    }

    @JsonProperty("whatsappIcon")
    public String getWhatsappIcon() {
        return whatsappIcon;
    }

    @JsonProperty("whatsappIcon")
    public void setWhatsappIcon(String whatsappIcon) {
        this.whatsappIcon = whatsappIcon;
    }

    @JsonProperty("whatsappLabel")
    public String getWhatsappLabel() {
        return whatsappLabel;
    }

    @JsonProperty("whatsappLabel")
    public void setWhatsappLabel(String whatsappLabel) {
        this.whatsappLabel = whatsappLabel;
    }

    @JsonProperty("whatsappValue")
    public String getWhatsappValue() {
        return whatsappValue;
    }

    @JsonProperty("whatsappValue")
    public void setWhatsappValue(String whatsappValue) {
        this.whatsappValue = whatsappValue;
    }

    @JsonProperty("whatsappMetrics")
    public String getWhatsappMetrics() {
        return whatsappMetrics;
    }

    @JsonProperty("whatsappMetrics")
    public void setWhatsappMetrics(String whatsappMetrics) {
        this.whatsappMetrics = whatsappMetrics;
    }

    @JsonProperty("icon")
    public String getIcon() {
        return icon;
    }

    @JsonProperty("icon")
    public void setIcon(String icon) {
        this.icon = icon;
    }

    @JsonProperty("label")
    public String getLabel() {
        return label;
    }

    @JsonProperty("label")
    public void setLabel(String label) {
        this.label = label;
    }

    @JsonProperty("value")
    public String getValue() {
        return value;
    }

    @JsonProperty("value")
    public void setValue(String value) {
        this.value = value;
    }

    @JsonProperty("metrics")
    public String getMetrics() {
        return metrics;
    }

    @JsonProperty("metrics")
    public void setMetrics(String metrics) {
        this.metrics = metrics;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
