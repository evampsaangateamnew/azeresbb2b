
package com.evampsaanga.b2b.magento.specialoffers;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "migration_id",
    "offeringId",
    "price"
})
public class Offer {

    @JsonProperty("migration_id")
    private String migrationId;
    @JsonProperty("offeringId")
    private String offeringId;
    @JsonProperty("price")
    private String price;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("migration_id")
    public String getMigrationId() {
        return migrationId;
    }

    @JsonProperty("migration_id")
    public void setMigrationId(String migrationId) {
        this.migrationId = migrationId;
    }

    @JsonProperty("offeringId")
    public String getOfferingId() {
        return offeringId;
    }

    @JsonProperty("offeringId")
    public void setOfferingId(String offeringId) {
        this.offeringId = offeringId;
    }

    @JsonProperty("price")
    public String getPrice() {
        return price;
    }

    @JsonProperty("price")
    public void setPrice(String price) {
        this.price = price;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
