
package com.evampsaanga.b2b.magento.specialoffers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "Tariff",
    "Offer",
    "customer_group"
})
public class Datum {

    @JsonProperty("Tariff")
    private List<Tariff> tariff = null;
    @JsonProperty("Offer")
    private List<Offer> offer = null;
    @JsonProperty("customer_group")
    private String customerGroup;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("Tariff")
    public List<Tariff> getTariff() {
        return tariff;
    }

    @JsonProperty("Tariff")
    public void setTariff(List<Tariff> tariff) {
        this.tariff = tariff;
    }

    @JsonProperty("Offer")
    public List<Offer> getOffer() {
        return offer;
    }

    @JsonProperty("Offer")
    public void setOffer(List<Offer> offer) {
        this.offer = offer;
    }

    @JsonProperty("customer_group")
    public String getCustomerGroup() {
        return customerGroup;
    }

    @JsonProperty("customer_group")
    public void setCustomerGroup(String customerGroup) {
        this.customerGroup = customerGroup;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
