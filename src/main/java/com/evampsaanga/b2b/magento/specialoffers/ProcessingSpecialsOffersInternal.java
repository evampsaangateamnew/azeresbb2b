package com.evampsaanga.b2b.magento.specialoffers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map.Entry;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.saanga.magento.apiclient.RestClient;
import org.apache.log4j.Logger;

public class ProcessingSpecialsOffersInternal {
	
	public static SpecialOffersResponse sepcialRespponse(SpecialOffersRequest cclient,SpecialOffersResponse resp,Logger logger,Logs logs, String token)
	{

		try {

			JSONObject jsonObject = new JSONObject();
			jsonObject.put("lang", cclient.getLang());
			jsonObject.put("customerGroup", cclient.getCustomerGroup());
			logger.info(token + "Request from specialOffers: " + jsonObject.toString());      
			
			String [] items = cclient.getCustomerGroup().split(",");
			List<String> container = Arrays.asList(items);
			String groupFoundInCache="1";
			for(int i=0;i<container.size();i++)
			{
				if(ConfigurationManager.specialGroupsCache.containsKey(container.get(i)))
				{
					groupFoundInCache="0";
					logger.info(token + "ResponseSpecials In Cache --Found Group: " +container.get(i));
				}
				else
				{
					groupFoundInCache="1";
					logger.info(token + "ResponseSpecialsIn Cache --NOT Found Group: " +container.get(i));
					i=container.size();
				}
			}
			logger.info(token+"VResponseSpecials ofgroupFoundInCache "+groupFoundInCache);
			if(groupFoundInCache.equals("0"))
			{
				ArrayList<Datum> finalData=new ArrayList<>();
				/*for(Entry<String, Datum> entry:ConfigurationManager.specialGroupsCache.entrySet()){
				   logger.info(token+"Values From specialGroupsCache"+entry.getKey() + " : " + entry.getValue());
				    finalData.add(entry.getValue());
				}*/
				for(int i=0;i<container.size();i++)
				{
					finalData.add(ConfigurationManager.specialGroupsCache.get(container.get(i)));
				}
				
				resp.setData(finalData);
				resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
				resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
				logger.info(token+"Response Found in cache");
			
				
			}
			else if(groupFoundInCache.equals("1"))
			{
				logger.info(token+"ResponseSpecials NOT FOUND in cache");
			String response = RestClient.SendCallToMagento(token,
					ConfigurationManager.getConfigurationFromCache("magento.app.specialoffers"),
					jsonObject.toString());
			logger.info(token + "ResponseSpecials from SpecialOffers Magento: " + response);
			try {
				SpecialOffersMagentoResponse data = Helper.JsonToObject(response,
						SpecialOffersMagentoResponse.class);
				if (data.getResultCode().equals("401")) {
					resp.setData(data.getData());
					for(int i=0;i<data.getData().size();i++)
					{
						logger.info(token+"ResponseSpecials :"+data.getData().get(i).getCustomerGroup());
						logger.info(token+" ResponseSpecials to Be put in specialGroupsCache :"+Helper.ObjectToJson(data.getData().get(i)));
						
						ConfigurationManager.specialGroupsCache.put(data.getData().get(i).getCustomerGroup(), data.getData().get(i));
						
					}
					resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
					resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
					logs.setResponseCode(ResponseCodes.SUCESS_CODE_200);
					logs.setResponseDescription(ResponseCodes.SUCESS_DES_200);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);

				} else {
					resp.setReturnCode(data.getResultCode());
					resp.setReturnMsg(data.getMsg());
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					
				}
			} catch (Exception ex) {
				logger.info(token+Helper.GetException(ex));
				resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
				resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				
			}
		}
			logger.info(token+Helper.ObjectToJson(resp));
			return resp;
		} catch (JSONException ex) {
			logger.info(token + Helper.GetException(ex));
			resp.setReturnCode(ResponseCodes.MAGENTO_SERVER_ERROR_CODE);
			resp.setReturnMsg(ResponseCodes.MAGENTO_SERVER_ERROR_DES);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		} catch (Exception ex) {
			logger.info(token + Helper.GetException(ex));
			resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
			resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			
			return resp;
		}
	
	}

	public static ProcessSpecialcustomerData specialsForcustomerData(SpecialOffersRequest cclient,Logger logger,Logs logs, String token)
	{
		logger.info(token+"*******Request Received in specialsForcustomerData MEthod **********");
		 ProcessSpecialcustomerData specialData=new ProcessSpecialcustomerData();
		try{
		
		SpecialOffersResponse resp=new SpecialOffersResponse();
		 resp= ProcessingSpecialsOffersInternal.sepcialRespponse( cclient, resp, logger, logs,  token);
	
		 logger.info(token+"*******Responsecode From "+resp.getReturnCode()+"*******");
		 ArrayList<String> specialoffer=new ArrayList<>();
		 ArrayList<String> specialtariff=new ArrayList<>();
		 String OffersMenu="hide";
		 String tariffMenu="hide";
		 for(int i=0 ;i<resp.getData().size();i++)
		 {
			 
			if(resp.getData().get(i).getOffer()!=null && !resp.getData().get(i).getOffer().isEmpty() && resp.getData().get(i).getOffer().size()>0)
			{
				OffersMenu="show";
				for(int j=0;j<resp.getData().get(i).getOffer().size();j++) 
						{
					specialoffer.add(resp.getData().get(i).getOffer().get(j).getOfferingId());
						}
			}
			if(resp.getData().get(i).getTariff()!=null && !resp.getData().get(i).getTariff().isEmpty() && resp.getData().get(i).getTariff().size()>0)
			{
				tariffMenu="show";
				for(int j=0;j<resp.getData().get(i).getTariff().size();j++) 
						{
					specialtariff.add(resp.getData().get(i).getTariff().get(j).getMigrateTo());
						}
			}
		 }
		 specialData.setOffersSpecialIds(specialoffer);
		 specialData.setTariffSpecialIds(specialtariff);
		 specialData.setSpecialOffersMenu(OffersMenu);
		 specialData.setSpecialTariffMenu(tariffMenu);
		 logger.info(token+"Special Offers Ids"+specialoffer);
		return specialData;
		}
		catch (Exception e) {
			logger.info(token+Helper.GetException(e));
		}
		return specialData;
	}

}
