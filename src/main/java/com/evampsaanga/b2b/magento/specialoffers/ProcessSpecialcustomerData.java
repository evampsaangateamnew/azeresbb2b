package com.evampsaanga.b2b.magento.specialoffers;

import java.util.ArrayList;

public class ProcessSpecialcustomerData {
	public ArrayList<String> getTariffSpecialIds() {
		return tariffSpecialIds;
	}
	public void setTariffSpecialIds(ArrayList<String> tariffSpecialIds) {
		this.tariffSpecialIds = tariffSpecialIds;
	}
	public ArrayList<String> getOffersSpecialIds() {
		return offersSpecialIds;
	}
	public void setOffersSpecialIds(ArrayList<String> offersSpecialIds) {
		this.offersSpecialIds = offersSpecialIds;
	}
	public String getSpecialOffersMenu() {
		return specialOffersMenu;
	}
	public void setSpecialOffersMenu(String specialOffersMenu) {
		this.specialOffersMenu = specialOffersMenu;
	}
	public String getSpecialTariffMenu() {
		return specialTariffMenu;
	}
	public void setSpecialTariffMenu(String specialTariffMenu) {
		this.specialTariffMenu = specialTariffMenu;
	}
	private ArrayList<String> tariffSpecialIds=new ArrayList<>();
	private ArrayList<String> offersSpecialIds=new ArrayList<>();
	private String specialOffersMenu;
	private String specialTariffMenu;

}
