
package com.evampsaanga.b2b.magento.specialoffers;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "migration_id",
    "migrate_to",
    "migrate_from",
    "migration_price",
    "message_az",
    "message_ru",
    "message_en",
    "mrc"
})
public class Tariff {

    @JsonProperty("migration_id")
    private String migrationId;
    @JsonProperty("migrate_to")
    private String migrateTo;
    @JsonProperty("migrate_from")
    private String migrateFrom;
    @JsonProperty("migration_price")
    private String migrationPrice;
    @JsonProperty("message_az")
    private String messageAz;
    @JsonProperty("message_ru")
    private String messageRu;
    @JsonProperty("message_en")
    private String messageEn;
    @JsonProperty("mrc")
    private String mrc;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("migration_id")
    public String getMigrationId() {
        return migrationId;
    }

    @JsonProperty("migration_id")
    public void setMigrationId(String migrationId) {
        this.migrationId = migrationId;
    }

    @JsonProperty("migrate_to")
    public String getMigrateTo() {
        return migrateTo;
    }

    @JsonProperty("migrate_to")
    public void setMigrateTo(String migrateTo) {
        this.migrateTo = migrateTo;
    }

    @JsonProperty("migrate_from")
    public String getMigrateFrom() {
        return migrateFrom;
    }

    @JsonProperty("migrate_from")
    public void setMigrateFrom(String migrateFrom) {
        this.migrateFrom = migrateFrom;
    }

    @JsonProperty("migration_price")
    public String getMigrationPrice() {
        return migrationPrice;
    }

    @JsonProperty("migration_price")
    public void setMigrationPrice(String migrationPrice) {
        this.migrationPrice = migrationPrice;
    }

    @JsonProperty("message_az")
    public String getMessageAz() {
        return messageAz;
    }

    @JsonProperty("message_az")
    public void setMessageAz(String messageAz) {
        this.messageAz = messageAz;
    }

    @JsonProperty("message_ru")
    public String getMessageRu() {
        return messageRu;
    }

    @JsonProperty("message_ru")
    public void setMessageRu(String messageRu) {
        this.messageRu = messageRu;
    }

    @JsonProperty("message_en")
    public String getMessageEn() {
        return messageEn;
    }

    @JsonProperty("message_en")
    public void setMessageEn(String messageEn) {
        this.messageEn = messageEn;
    }

    @JsonProperty("mrc")
    public String getMrc() {
        return mrc;
    }

    @JsonProperty("mrc")
    public void setMrc(String mrc) {
        this.mrc = mrc;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
