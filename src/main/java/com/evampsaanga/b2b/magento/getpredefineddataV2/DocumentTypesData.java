package com.evampsaanga.b2b.magento.getpredefineddataV2;

public class DocumentTypesData {
	String id;
	String value;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
}
