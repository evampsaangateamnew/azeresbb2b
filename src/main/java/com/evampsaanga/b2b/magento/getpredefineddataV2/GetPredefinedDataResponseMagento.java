package com.evampsaanga.b2b.magento.getpredefineddataV2;

public class GetPredefinedDataResponseMagento {
	private String resultCode;
	private String execTime;
	private String msg;
	private GetPredefinedDataResponseData data;

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public String getExecTime() {
		return execTime;
	}

	public void setExecTime(String execTime) {
		this.execTime = execTime;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public GetPredefinedDataResponseData getData() {
		return data;
	}

	public void setData(GetPredefinedDataResponseData data) {
		this.data = data;
	}

}
