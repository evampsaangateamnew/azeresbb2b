package com.evampsaanga.b2b.magento.getpredefineddataV2;

import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;

public class GetPredefinedDataResponse extends BaseResponse {
	private GetPredefinedDataResponseData data;

	public GetPredefinedDataResponseData getData() {
		return data;
	}

	public void setData(GetPredefinedDataResponseData data) {
		this.data = data;
	}

}
