package com.evampsaanga.b2b.magento.getpredefineddataV2;

import java.util.List;

public class GetPredefinedDataResponseData {

	private String content;
	private String firstPopup;
	private String lateOnPopup;
	private String popupTitle;
	private String popupContent;
	private String changeLimitLabel;
	private String groupNamePartPay;
	private String groupNamePayBySubs;
	private List<GroupNamesData> groupName;
	private String changeGroupLabel;
	private List<DocumentTypesData> documentTypes;
	
	public String getContent() {
		return content;
	}

	public List<GroupNamesData> getGroupName() {
		return groupName;
	}

	public void setGroupName(List<GroupNamesData> groupName) {
		this.groupName = groupName;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getFirstPopup() {
		return firstPopup;
	}

	public void setFirstPopup(String firstPopup) {
		this.firstPopup = firstPopup;
	}

	public String getLateOnPopup() {
		return lateOnPopup;
	}

	public void setLateOnPopup(String lateOnPopup) {
		this.lateOnPopup = lateOnPopup;
	}

	public String getPopupTitle() {
		return popupTitle;
	}

	public void setPopupTitle(String popupTitle) {
		this.popupTitle = popupTitle;
	}

	public String getPopupContent() {
		return popupContent;
	}

	public void setPopupContent(String popupContent) {
		this.popupContent = popupContent;
	}

	public String getChangeLimitLabel() {
		return changeLimitLabel;
	}

	public void setChangeLimitLabel(String changeLimitLabel) {
		this.changeLimitLabel = changeLimitLabel;
	}

	public String getGroupNamePartPay() {
		return groupNamePartPay;
	}

	public void setGroupNamePartPay(String groupNamePartPay) {
		this.groupNamePartPay = groupNamePartPay;
	}

	public String getGroupNamePayBySubs() {
		return groupNamePayBySubs;
	}

	public void setGroupNamePayBySubs(String groupNamePayBySubs) {
		this.groupNamePayBySubs = groupNamePayBySubs;
	}

	public String getChangeGroupLabel() {
		return changeGroupLabel;
	}

	public void setChangeGroupLabel(String changeGroupLabel) {
		this.changeGroupLabel = changeGroupLabel;
	}

	public List<DocumentTypesData> getDocumentTypes() {
		return documentTypes;
	}

	public void setDocumentTypes(List<DocumentTypesData> documentTypes) {
		this.documentTypes = documentTypes;
	}

}
