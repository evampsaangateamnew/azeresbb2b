package com.evampsaanga.b2b.magento.tariffdetails;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Payg {
	private String paygSectionLabel;
	@JsonProperty("Call")
	private KlassPaygCall Call;
	@JsonProperty("SMS")
	private SMS SMS;
	@JsonProperty("Internet")
	private Internet Internet;

	public String getPaygSectionLabel() {
		return paygSectionLabel;
	}

	public void setPaygSectionLabel(String paygSectionLabel) {
		this.paygSectionLabel = paygSectionLabel;
	}

	@Override
	public String toString() {
		return "ClassPojo [paygSectionLabel = " + paygSectionLabel + ", Call = " + Call + ", SMS = " + SMS
				+ ", Internet = " + Internet + "]";
	}
}