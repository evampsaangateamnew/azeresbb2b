package com.evampsaanga.b2b.magento.tariffdetails;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Klass {
	private String subscribable = "";
	@JsonProperty
	private Details details;
	@JsonProperty
	private PackagePrices packagePrices;
	@JsonProperty
	private Payg payg;
	@JsonProperty
	private KlassHeader header;

	public String getSubscribable() {
		return subscribable;
	}

	public void setSubscribable(String subscribable) {
		this.subscribable = subscribable;
	}

	public Details getDetails() {
		return details;
	}

	public void setDetails(Details details) {
		this.details = details;
	}

	public PackagePrices getPackagePrices() {
		return packagePrices;
	}

	public void setPackagePrices(PackagePrices packagePrices) {
		this.packagePrices = packagePrices;
	}

	public Payg getPayg() {
		return payg;
	}

	public void setPayg(Payg payg) {
		this.payg = payg;
	}

	public KlassHeader getHeader() {
		return header;
	}

	public void setHeader(KlassHeader header) {
		this.header = header;
	}

	@Override
	public String toString() {
		return "ClassPojo [subscribable = " + subscribable + ", details = " + details + ", packagePrices = "
				+ packagePrices + ", payg = " + payg + ", header = " + header + "]";
	}
}