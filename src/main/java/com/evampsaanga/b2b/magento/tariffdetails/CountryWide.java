package com.evampsaanga.b2b.magento.tariffdetails;

public class CountryWide {
	private String countryWideValue = "";
	private String countryWideLabel = "";
	private String countryWideIcon = "";
	private String countryWideMetrics = "";

	public String getCountryWideValue() {
		return countryWideValue;
	}

	public void setCountryWideValue(String countryWideValue) {
		this.countryWideValue = countryWideValue;
	}

	public String getCountryWideLabel() {
		return countryWideLabel;
	}

	public void setCountryWideLabel(String countryWideLabel) {
		this.countryWideLabel = countryWideLabel;
	}

	public String getCountryWideIcon() {
		return countryWideIcon;
	}

	public void setCountryWideIcon(String countryWideIcon) {
		this.countryWideIcon = countryWideIcon;
	}

	public String getCountryWideMetrics() {
		return countryWideMetrics;
	}

	public void setCountryWideMetrics(String countryWideMetrics) {
		this.countryWideMetrics = countryWideMetrics;
	}

	@Override
	public String toString() {
		return "ClassPojo [countryWideValue = " + countryWideValue + ", countryWideLabel = " + countryWideLabel
				+ ", countryWideIcon = " + countryWideIcon + ", countryWideMetrics = " + countryWideMetrics + "]";
	}
}
