package com.evampsaanga.b2b.magento.tariffdetails;

import com.fasterxml.jackson.annotation.JsonProperty;

public class KlassPackagePricesCall {
	@JsonProperty
	private String callValueA = "";
	@JsonProperty
	private String callValueB = "";
	@JsonProperty
	private String callLabel = "";
	@JsonProperty
	private String shortDescription = "";
	@JsonProperty
	private String offnetLabel = "";
	@JsonProperty
	private String callIcon = "";
	@JsonProperty
	private String offnetValueB = "";
	@JsonProperty
	private String offnetValueA = "";
	@JsonProperty
	private String priceTemplate = "";
	@JsonProperty
	private String onnetLabel = "";
	@JsonProperty
	private String onnetValueA = "";
	@JsonProperty
	private String onnetValueB = "";

	public String getCallValueA() {
		return callValueA;
	}

	public void setCallValueA(String callValueA) {
		this.callValueA = callValueA;
	}

	public String getCallValueB() {
		return callValueB;
	}

	public void setCallValueB(String callValueB) {
		this.callValueB = callValueB;
	}

	public String getCallLabel() {
		return callLabel;
	}

	public void setCallLabel(String callLabel) {
		this.callLabel = callLabel;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	public String getOffnetLabel() {
		return offnetLabel;
	}

	public void setOffnetLabel(String offnetLabel) {
		this.offnetLabel = offnetLabel;
	}

	public String getCallIcon() {
		return callIcon;
	}

	public void setCallIcon(String callIcon) {
		this.callIcon = callIcon;
	}

	public String getOffnetValueB() {
		return offnetValueB;
	}

	public void setOffnetValueB(String offnetValueB) {
		this.offnetValueB = offnetValueB;
	}

	public String getOffnetValueA() {
		return offnetValueA;
	}

	public void setOffnetValueA(String offnetValueA) {
		this.offnetValueA = offnetValueA;
	}

	public String getPriceTemplate() {
		return priceTemplate;
	}

	public void setPriceTemplate(String priceTemplate) {
		this.priceTemplate = priceTemplate;
	}

	public String getOnnetLabel() {
		return onnetLabel;
	}

	public void setOnnetLabel(String onnetLabel) {
		this.onnetLabel = onnetLabel;
	}

	public String getOnnetValueA() {
		return onnetValueA;
	}

	public void setOnnetValueA(String onnetValueA) {
		this.onnetValueA = onnetValueA;
	}

	public String getOnnetValueB() {
		return onnetValueB;
	}

	public void setOnnetValueB(String onnetValueB) {
		this.onnetValueB = onnetValueB;
	}

	@Override
	public String toString() {
		return "ClassPojo [callValueA = " + callValueA + ", callValueB = " + callValueB + ", callLabel = " + callLabel
				+ ", shortDescription = " + shortDescription + ", offnetLabel = " + offnetLabel + ", callIcon = "
				+ callIcon + ", offnetValueB = " + offnetValueB + ", offnetValueA = " + offnetValueA
				+ ", priceTemplate = " + priceTemplate + ", onnetLabel = " + onnetLabel + ", onnetValueA = "
				+ onnetValueA + ", onnetValueB = " + onnetValueB + "]";
	}
}