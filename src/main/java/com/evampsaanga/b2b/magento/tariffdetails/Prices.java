package com.evampsaanga.b2b.magento.tariffdetails;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Prices {
	@JsonProperty
	private KlassPackagePricesCall Call;
	private String pricesSectionLabel;
	@JsonProperty
	private SMS SMS;
	@JsonProperty
	private Internet Internet;

	public String getPricesSectionLabel() {
		return pricesSectionLabel;
	}

	public void setPricesSectionLabel(String pricesSectionLabel) {
		this.pricesSectionLabel = pricesSectionLabel;
	}
}
