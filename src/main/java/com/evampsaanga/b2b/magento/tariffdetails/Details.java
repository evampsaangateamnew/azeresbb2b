package com.evampsaanga.b2b.magento.tariffdetails;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Details {
	@JsonProperty
	private TitleSubtitle titleSubtitle;
	private FreeResource freeResource;
	@JsonProperty
	private Time time;
	@JsonProperty
	private Advantages advantages;
	@JsonProperty
	private FragmentD fragmentD;
	@JsonProperty
	private FragmentC fragmentC;
	@JsonProperty
	private FragmentB fragmentB;
	@JsonProperty
	private Call Call;
	@JsonProperty
	private String detailSectionLabel;
	private Roaming roaming;
	@JsonProperty
	private Date date;
	@JsonProperty
	private Destination destination;

	public TitleSubtitle getTitleSubtitle() {
		return titleSubtitle;
	}

	public void setTitleSubtitle(TitleSubtitle titleSubtitle) {
		this.titleSubtitle = titleSubtitle;
	}

	public FreeResource getFreeResource() {
		return freeResource;
	}

	public void setFreeResource(FreeResource freeResource) {
		this.freeResource = freeResource;
	}

	public Time getTime() {
		return time;
	}

	public void setTime(Time time) {
		this.time = time;
	}

	public Advantages getAdvantages() {
		return advantages;
	}

	public void setAdvantages(Advantages advantages) {
		this.advantages = advantages;
	}

	public FragmentD getFragmentD() {
		return fragmentD;
	}

	public void setFragmentD(FragmentD fragmentD) {
		this.fragmentD = fragmentD;
	}

	public FragmentC getFragmentC() {
		return fragmentC;
	}

	public void setFragmentC(FragmentC fragmentC) {
		this.fragmentC = fragmentC;
	}

	public FragmentB getFragmentB() {
		return fragmentB;
	}

	public void setFragmentB(FragmentB fragmentB) {
		this.fragmentB = fragmentB;
	}

	public String getDetailSectionLabel() {
		return detailSectionLabel;
	}

	public void setDetailSectionLabel(String detailSectionLabel) {
		this.detailSectionLabel = detailSectionLabel;
	}

	public Roaming getRoaming() {
		return roaming;
	}

	public void setRoaming(Roaming roaming) {
		this.roaming = roaming;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Destination getDestination() {
		return destination;
	}

	public void setDestination(Destination destination) {
		this.destination = destination;
	}

	@Override
	public String toString() {
		return "ClassPojo [titleSubtitle = " + titleSubtitle + ", freeResource = " + freeResource + ", time = " + time
				+ ", advantages = " + advantages + ", fragmentD = " + fragmentD + ", fragmentC = " + fragmentC
				+ ", fragmentB = " + fragmentB + ", Call = " + Call + ", detailSectionLabel = " + detailSectionLabel
				+ ", roaming = " + roaming + ", date = " + date + ", destination = " + destination + "]";
	}
}
