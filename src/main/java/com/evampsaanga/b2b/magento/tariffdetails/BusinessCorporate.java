package com.evampsaanga.b2b.magento.tariffdetails;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BusinessCorporate {
	private String subscribable;
	private Details details;
	private Description description;
	private Prices prices;
	@JsonProperty
	private KlassHeader header;

	
	public String getSubscribable() {
		return subscribable;
	}

	public void setSubscribable(String subscribable) {
		this.subscribable = subscribable;
	}

	public Details getDetails() {
		return details;
	}

	public void setDetails(Details details) {
		this.details = details;
	}

	public Description getDescription() {
		return description;
	}

	public void setDescription(Description description) {
		this.description = description;
	}

	public Prices getPrices() {
		return prices;
	}

	public void setPrices(Prices prices) {
		this.prices = prices;
	}
}