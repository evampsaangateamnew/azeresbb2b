package com.evampsaanga.b2b.magento.tariffdetails;

public class BonusSeven {
	private String icon = "";
	private String metrics = "";
	private String value = "";
	private String label = "";

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getMetrics() {
		return metrics;
	}

	public void setMetrics(String metrics) {
		this.metrics = metrics;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	@Override
	public String toString() {
		return "ClassPojo [icon = " + icon + ", metrics = " + metrics + ", value = " + value + ", label = " + label
				+ "]";
	}
}