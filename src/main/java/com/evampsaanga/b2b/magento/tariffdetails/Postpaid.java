package com.evampsaanga.b2b.magento.tariffdetails;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Postpaid extends TarrifDataV2{
	@JsonProperty("BusinessCorporate")
	private BusinessCorporate[] BusinessCorporate;
	@JsonProperty("KlassPostpaid")
	private KlassPostpaid[] KlassPostpaid;
	@JsonProperty("BusinessIndividual")
	private BusinessIndividual[] BusinessIndividual;
	public BusinessCorporate[] getBusinessCorporate() {
		return BusinessCorporate;
	}
	public void setBusinessCorporate(BusinessCorporate[] businessCorporate) {
		BusinessCorporate = businessCorporate;
	}
	public KlassPostpaid[] getKlassPostpaid() {
		return KlassPostpaid;
	}
	public void setKlassPostpaid(KlassPostpaid[] klassPostpaid) {
		KlassPostpaid = klassPostpaid;
	}
	public BusinessIndividual[] getBusinessIndividual() {
		return BusinessIndividual;
	}
	public void setBusinessIndividual(BusinessIndividual[] businessIndividual) {
		BusinessIndividual = businessIndividual;
	}
	
	
}
