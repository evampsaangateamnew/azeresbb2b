package com.evampsaanga.b2b.magento.tariffdetails;

public class FragmentC {
	private String titleFour = "";
	private String titleThree = "";
	private String callValue = "";
	private String callLabel = "";
	private String callIcon = "";
	private String callOffnetOutOfRegionValue = "";
	private String callOffnetOutOfRegionLabel = "";
	private String callOnnetOutOfRegionValue = "";
	private String valueFour = "";
	private String callOnnetOutOfRegionLabel = "";
	private String callDescription = "";
	private String valueThree = "";

	public String getTitleFour() {
		return titleFour;
	}

	public void setTitleFour(String titleFour) {
		this.titleFour = titleFour;
	}

	public String getTitleThree() {
		return titleThree;
	}

	public void setTitleThree(String titleThree) {
		this.titleThree = titleThree;
	}

	public String getCallValue() {
		return callValue;
	}

	public void setCallValue(String callValue) {
		this.callValue = callValue;
	}

	public String getCallLabel() {
		return callLabel;
	}

	public void setCallLabel(String callLabel) {
		this.callLabel = callLabel;
	}

	public String getCallIcon() {
		return callIcon;
	}

	public void setCallIcon(String callIcon) {
		this.callIcon = callIcon;
	}

	public String getCallOffnetOutOfRegionValue() {
		return callOffnetOutOfRegionValue;
	}

	public void setCallOffnetOutOfRegionValue(String callOffnetOutOfRegionValue) {
		this.callOffnetOutOfRegionValue = callOffnetOutOfRegionValue;
	}

	public String getCallOffnetOutOfRegionLabel() {
		return callOffnetOutOfRegionLabel;
	}

	public void setCallOffnetOutOfRegionLabel(String callOffnetOutOfRegionLabel) {
		this.callOffnetOutOfRegionLabel = callOffnetOutOfRegionLabel;
	}

	public String getCallOnnetOutOfRegionValue() {
		return callOnnetOutOfRegionValue;
	}

	public void setCallOnnetOutOfRegionValue(String callOnnetOutOfRegionValue) {
		this.callOnnetOutOfRegionValue = callOnnetOutOfRegionValue;
	}

	public String getValueFour() {
		return valueFour;
	}

	public void setValueFour(String valueFour) {
		this.valueFour = valueFour;
	}

	public String getCallOnnetOutOfRegionLabel() {
		return callOnnetOutOfRegionLabel;
	}

	public void setCallOnnetOutOfRegionLabel(String callOnnetOutOfRegionLabel) {
		this.callOnnetOutOfRegionLabel = callOnnetOutOfRegionLabel;
	}

	public String getCallDescription() {
		return callDescription;
	}

	public void setCallDescription(String callDescription) {
		this.callDescription = callDescription;
	}

	public String getValueThree() {
		return valueThree;
	}

	public void setValueThree(String valueThree) {
		this.valueThree = valueThree;
	}

	@Override
	public String toString() {
		return "ClassPojo [titleFour = " + titleFour + ", titleThree = " + titleThree + ", callValue = " + callValue
				+ ", callLabel = " + callLabel + ", callIcon = " + callIcon + ", callOffnetOutOfRegionValue = "
				+ callOffnetOutOfRegionValue + ", callOffnetOutOfRegionLabel = " + callOffnetOutOfRegionLabel
				+ ", callOnnetOutOfRegionValue = " + callOnnetOutOfRegionValue + ", valueFour = " + valueFour
				+ ", callOnnetOutOfRegionLabel = " + callOnnetOutOfRegionLabel + ", callDescription = "
				+ callDescription + ", valueThree = " + valueThree + "]";
	}
}
