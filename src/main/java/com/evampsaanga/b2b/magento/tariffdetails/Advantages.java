package com.evampsaanga.b2b.magento.tariffdetails;

public class Advantages {
	private String description = "";
	private String advantagesLabel = "";

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAdvantagesLabel() {
		return advantagesLabel;
	}

	public void setAdvantagesLabel(String advantagesLabel) {
		this.advantagesLabel = advantagesLabel;
	}

	@Override
	public String toString() {
		return "ClassPojo [description = " + description + ", advantagesLabel = " + advantagesLabel + "]";
	}
}