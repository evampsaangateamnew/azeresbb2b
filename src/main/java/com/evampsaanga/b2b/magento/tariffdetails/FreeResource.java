package com.evampsaanga.b2b.magento.tariffdetails;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FreeResource {
	@JsonProperty
	private String freeResourceValue = "";
	@JsonProperty
	private String onnetFreeResourceValue = "";
	@JsonProperty
	private String freeResourceLabel = "";
	@JsonProperty
	private String onnetFreeResourceLabel = "";
	@JsonProperty
	private String descriptionFreeResource = "";

	public String getFreeResourceValue() {
		return freeResourceValue;
	}

	public void setFreeResourceValue(String freeResourceValue) {
		this.freeResourceValue = freeResourceValue;
	}

	public String getOnnetFreeResourceValue() {
		return onnetFreeResourceValue;
	}

	public void setOnnetFreeResourceValue(String onnetFreeResourceValue) {
		this.onnetFreeResourceValue = onnetFreeResourceValue;
	}

	public String getFreeResourceLabel() {
		return freeResourceLabel;
	}

	public void setFreeResourceLabel(String freeResourceLabel) {
		this.freeResourceLabel = freeResourceLabel;
	}

	public String getOnnetFreeResourceLabel() {
		return onnetFreeResourceLabel;
	}

	public void setOnnetFreeResourceLabel(String onnetFreeResourceLabel) {
		this.onnetFreeResourceLabel = onnetFreeResourceLabel;
	}

	public String getDescriptionFreeResource() {
		return descriptionFreeResource;
	}

	public void setDescriptionFreeResource(String descriptionFreeResource) {
		this.descriptionFreeResource = descriptionFreeResource;
	}

	@Override
	public String toString() {
		return "ClassPojo [freeResourceValue = " + freeResourceValue + ", onnetFreeResourceValue = "
				+ onnetFreeResourceValue + ", freeResourceLabel = " + freeResourceLabel + ", onnetFreeResourceLabel = "
				+ onnetFreeResourceLabel + ", descriptionFreeResource = " + descriptionFreeResource + "]";
	}
}