package com.evampsaanga.b2b.magento.tariffdetails;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Description {
	@JsonProperty
	private String clarificationValue = "";
	@JsonProperty
	private String advantagesLabel = "";
	@JsonProperty
	private String descriptionSectionLabel = "";
	@JsonProperty
	private String advantagesValue = "";
	@JsonProperty
	private String clarificationLabel = "";

	public String getClarificationValue() {
		return clarificationValue;
	}

	public void setClarificationValue(String clarificationValue) {
		this.clarificationValue = clarificationValue;
	}

	public String getAdvantagesLabel() {
		return advantagesLabel;
	}

	public void setAdvantagesLabel(String advantagesLabel) {
		this.advantagesLabel = advantagesLabel;
	}

	public String getDescriptionSectionLabel() {
		return descriptionSectionLabel;
	}

	public void setDescriptionSectionLabel(String descriptionSectionLabel) {
		this.descriptionSectionLabel = descriptionSectionLabel;
	}

	public String getAdvantagesValue() {
		return advantagesValue;
	}

	public void setAdvantagesValue(String advantagesValue) {
		this.advantagesValue = advantagesValue;
	}

	public String getClarificationLabel() {
		return clarificationLabel;
	}

	public void setClarificationLabel(String clarificationLabel) {
		this.clarificationLabel = clarificationLabel;
	}

	@Override
	public String toString() {
		return "ClassPojo [clarificationValue = " + clarificationValue + ", advantagesLabel = " + advantagesLabel
				+ ", descriptionSectionLabel = " + descriptionSectionLabel + ", advantagesValue = " + advantagesValue
				+ ", clarificationLabel = " + clarificationLabel + "]";
	}
}
