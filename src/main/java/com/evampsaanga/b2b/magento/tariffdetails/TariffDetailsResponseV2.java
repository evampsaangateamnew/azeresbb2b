package com.evampsaanga.b2b.magento.tariffdetails;

import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;

public class TariffDetailsResponseV2 extends BaseResponse {
	private TarrifDataV2 data;

	public TarrifDataV2 getData() {
		return data;
	}

	public void setData(TarrifDataV2 data) {
		this.data = data;
	}
	
	

}
