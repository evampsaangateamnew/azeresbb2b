package com.evampsaanga.b2b.magento.tariffdetails;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Prepaid extends TarrifDataV2{
	@JsonProperty("Cin")
	private Cin[] Cin;
	@JsonProperty("Klass")
	private Klass[] Klass;
	public Cin[] getCin() {
		return Cin;
	}
	public void setCin(Cin[] cin) {
		Cin = cin;
	}
	public Klass[] getKlass() {
		return Klass;
	}
	public void setKlass(Klass[] klass) {
		Klass = klass;
	}
	
	
}