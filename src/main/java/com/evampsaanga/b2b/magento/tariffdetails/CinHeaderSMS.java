package com.evampsaanga.b2b.magento.tariffdetails;

public class CinHeaderSMS {
	private String smsCountryWideLabel = "";
	private String smsIcon = "";
	private String smsLabel = "";
	private String smsInternationalLabel = "";
	private String smsCountryWideValue = "";
	private String smsInternationalValue = "";
	private String smsValue = "";

	public String getSmsCountryWideLabel() {
		return smsCountryWideLabel;
	}

	public void setSmsCountryWideLabel(String smsCountryWideLabel) {
		this.smsCountryWideLabel = smsCountryWideLabel;
	}

	public String getSmsIcon() {
		return smsIcon;
	}

	public void setSmsIcon(String smsIcon) {
		this.smsIcon = smsIcon;
	}

	public String getSmsLabel() {
		return smsLabel;
	}

	public void setSmsLabel(String smsLabel) {
		this.smsLabel = smsLabel;
	}

	public String getSmsInternationalLabel() {
		return smsInternationalLabel;
	}

	public void setSmsInternationalLabel(String smsInternationalLabel) {
		this.smsInternationalLabel = smsInternationalLabel;
	}

	public String getSmsCountryWideValue() {
		return smsCountryWideValue;
	}

	public void setSmsCountryWideValue(String smsCountryWideValue) {
		this.smsCountryWideValue = smsCountryWideValue;
	}

	public String getSmsInternationalValue() {
		return smsInternationalValue;
	}

	public void setSmsInternationalValue(String smsInternationalValue) {
		this.smsInternationalValue = smsInternationalValue;
	}

	public String getSmsValue() {
		return smsValue;
	}

	public void setSmsValue(String smsValue) {
		this.smsValue = smsValue;
	}

	@Override
	public String toString() {
		return "ClassPojo [smsCountryWideLabel = " + smsCountryWideLabel + ", smsIcon = " + smsIcon + ", smsLabel = "
				+ smsLabel + ", smsInternationalLabel = " + smsInternationalLabel + ", smsCountryWideValue = "
				+ smsCountryWideValue + ", smsInternationalValue = " + smsInternationalValue + ", smsValue = "
				+ smsValue + "]";
	}
}