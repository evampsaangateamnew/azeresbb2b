package com.evampsaanga.b2b.magento.tariffdetails;

public class Whatsapp {
	private String whatsappLabel = "";
	private String whatsappValue = "";
	private String whatsappMetrics = "";
	private String whatsappIcon = "";

	public String getWhatsappLabel() {
		return whatsappLabel;
	}

	public void setWhatsappLabel(String whatsappLabel) {
		this.whatsappLabel = whatsappLabel;
	}

	public String getWhatsappValue() {
		return whatsappValue;
	}

	public void setWhatsappValue(String whatsappValue) {
		this.whatsappValue = whatsappValue;
	}

	public String getWhatsappMetrics() {
		return whatsappMetrics;
	}

	public void setWhatsappMetrics(String whatsappMetrics) {
		this.whatsappMetrics = whatsappMetrics;
	}

	public String getWhatsappIcon() {
		return whatsappIcon;
	}

	public void setWhatsappIcon(String whatsappIcon) {
		this.whatsappIcon = whatsappIcon;
	}

	@Override
	public String toString() {
		return "ClassPojo [whatsappLabel = " + whatsappLabel + ", whatsappValue = " + whatsappValue
				+ ", whatsappMetrics = " + whatsappMetrics + ", whatsappIcon = " + whatsappIcon + "]";
	}
}
