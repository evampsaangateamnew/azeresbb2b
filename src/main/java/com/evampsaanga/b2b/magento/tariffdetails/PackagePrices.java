package com.evampsaanga.b2b.magento.tariffdetails;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PackagePrices {
	@JsonProperty
	private KlassPackagePricesCall Call;
	@JsonProperty
	private SMS SMS;
	@JsonProperty
	private Internet Internet;
	private String packagePricesSectionLabel = "";

	public String getPackagePricesSectionLabel() {
		return packagePricesSectionLabel;
	}

	public void setPackagePricesSectionLabel(String packagePricesSectionLabel) {
		this.packagePricesSectionLabel = packagePricesSectionLabel;
	}

	@Override
	public String toString() {
		return "ClassPojo [Call = " + Call + ", SMS = " + SMS + ", Internet = " + Internet
				+ ", packagePricesSectionLabel = " + packagePricesSectionLabel + "]";
	}
}