package com.evampsaanga.b2b.magento.tariffdetails;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TitleSubtitle {
	@JsonProperty
	private String subtitle1Value = "";
	@JsonProperty
	private String subtitle4Value = "";
	@JsonProperty
	private String titleLabel = "";
	@JsonProperty
	private String shortDescription = "";
	@JsonProperty
	private String subtitle3Label = "";
	@JsonProperty
	private String subtitle4Label = "";
	@JsonProperty
	private String subtitle2Value = "";
	@JsonProperty
	private String subtitle2Label = "";
	@JsonProperty
	private String subtitle3Value = "";
	@JsonProperty
	private String subtitle1Label = "";

	public String getSubtitle1Value() {
		return subtitle1Value;
	}

	public void setSubtitle1Value(String subtitle1Value) {
		this.subtitle1Value = subtitle1Value;
	}

	public String getSubtitle4Value() {
		return subtitle4Value;
	}

	public void setSubtitle4Value(String subtitle4Value) {
		this.subtitle4Value = subtitle4Value;
	}

	public String getTitleLabel() {
		return titleLabel;
	}

	public void setTitleLabel(String titleLabel) {
		this.titleLabel = titleLabel;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	public String getSubtitle3Label() {
		return subtitle3Label;
	}

	public void setSubtitle3Label(String subtitle3Label) {
		this.subtitle3Label = subtitle3Label;
	}

	public String getSubtitle4Label() {
		return subtitle4Label;
	}

	public void setSubtitle4Label(String subtitle4Label) {
		this.subtitle4Label = subtitle4Label;
	}

	public String getSubtitle2Value() {
		return subtitle2Value;
	}

	public void setSubtitle2Value(String subtitle2Value) {
		this.subtitle2Value = subtitle2Value;
	}

	public String getSubtitle2Label() {
		return subtitle2Label;
	}

	public void setSubtitle2Label(String subtitle2Label) {
		this.subtitle2Label = subtitle2Label;
	}

	public String getSubtitle3Value() {
		return subtitle3Value;
	}

	public void setSubtitle3Value(String subtitle3Value) {
		this.subtitle3Value = subtitle3Value;
	}

	public String getSubtitle1Label() {
		return subtitle1Label;
	}

	public void setSubtitle1Label(String subtitle1Label) {
		this.subtitle1Label = subtitle1Label;
	}

	@Override
	public String toString() {
		return "ClassPojo [subtitle1Value = " + subtitle1Value + ", subtitle4Value = " + subtitle4Value
				+ ", titleLabel = " + titleLabel + ", shortDescription = " + shortDescription + ", subtitle3Label = "
				+ subtitle3Label + ", subtitle4Label = " + subtitle4Label + ", subtitle2Value = " + subtitle2Value
				+ ", subtitle2Label = " + subtitle2Label + ", subtitle3Value = " + subtitle3Value
				+ ", subtitle1Label = " + subtitle1Label + "]";
	}
}