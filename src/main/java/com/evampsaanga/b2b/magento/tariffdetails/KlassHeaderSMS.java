package com.evampsaanga.b2b.magento.tariffdetails;

public class KlassHeaderSMS {
	private String smsIcon = "";
	private String smsLabel = "";
	private String smsMetrics = "";
	private String smsValue = "";

	public String getSmsIcon() {
		return smsIcon;
	}

	public void setSmsIcon(String smsIcon) {
		this.smsIcon = smsIcon;
	}

	public String getSmsLabel() {
		return smsLabel;
	}

	public void setSmsLabel(String smsLabel) {
		this.smsLabel = smsLabel;
	}

	public String getSmsMetrics() {
		return smsMetrics;
	}

	public void setSmsMetrics(String smsMetrics) {
		this.smsMetrics = smsMetrics;
	}

	public String getSmsValue() {
		return smsValue;
	}

	public void setSmsValue(String smsValue) {
		this.smsValue = smsValue;
	}
}