package com.evampsaanga.b2b.magento.tariffdetails;

public class KlassHeaderInternet {
	private String internetMetrics = "";
	private String internetValue = "";
	private String internetLabel = "";
	private String internetIcon = "";

	public String getInternetMetrics() {
		return internetMetrics;
	}

	public void setInternetMetrics(String internetMetrics) {
		this.internetMetrics = internetMetrics;
	}

	public String getInternetValue() {
		return internetValue;
	}

	public void setInternetValue(String internetValue) {
		this.internetValue = internetValue;
	}

	public String getInternetLabel() {
		return internetLabel;
	}

	public void setInternetLabel(String internetLabel) {
		this.internetLabel = internetLabel;
	}

	public String getInternetIcon() {
		return internetIcon;
	}

	public void setInternetIcon(String internetIcon) {
		this.internetIcon = internetIcon;
	}

	@Override
	public String toString() {
		return "ClassPojo [internetMetrics = " + internetMetrics + ", internetValue = " + internetValue
				+ ", internetLabel = " + internetLabel + ", internetIcon = " + internetIcon + "]";
	}
}