package com.evampsaanga.b2b.magento.tariffdetails;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BusinessIndividual {
	private String subscribable = "";
	@JsonProperty("details")
	private Details details;
	@JsonProperty("description")
	private Description description;
	// @JsonProperty("prices")
	// private Prices prices;
	@JsonProperty("header")
	private PostpaidHeaderBusinessIndividual header;

	
	
	
	public String getSubscribable() {
		return subscribable;
	}

	public void setSubscribable(String subscribable) {
		this.subscribable = subscribable;
	}

	public Details getDetails() {
		return details;
	}

	public void setDetails(Details details) {
		this.details = details;
	}

	public Description getDescription() {
		return description;
	}

	public void setDescription(Description description) {
		this.description = description;
	}
}