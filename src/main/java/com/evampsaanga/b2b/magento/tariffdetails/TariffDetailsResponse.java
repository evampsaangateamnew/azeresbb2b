package com.evampsaanga.b2b.magento.tariffdetails;

import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;

public class TariffDetailsResponse extends BaseResponse {
	private com.evampsaanga.b2b.magento.tariffdetails.Data data = new Data();

	public com.evampsaanga.b2b.magento.tariffdetails.Data getData() {
		return data;
	}

	public void setData(com.evampsaanga.b2b.magento.tariffdetails.Data data) {
		this.data = data;
	}
}
