package com.evampsaanga.b2b.magento.getpredefineddata;

public class Fnf {
	private String maxFNFCount;
	private String fnFAllowed;

	public String getMaxFNFCount() {
		return maxFNFCount;
	}

	public void setMaxFNFCount(String maxFNFCount) {
		this.maxFNFCount = maxFNFCount;
	}

	@Override
	public String toString() {
		return "ClassPojo [maxFNFCount = " + maxFNFCount + "]";
	}

	public String getFnFAllowed() {
		return fnFAllowed;
	}

	public void setFnFAllowed(String fnFAllowed) {
		this.fnFAllowed = fnFAllowed;
	}
}
