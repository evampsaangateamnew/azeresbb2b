package com.evampsaanga.b2b.magento.getpredefineddata;

public class NarTv {

	private String narTvRedirectionLinkAndroid;
	private String narTvRedirectionLinkIos;
	private String narTvRedirectionLinkWebEnglish;
	private String narTvRedirectionLinkWebAzeri;
	private String narTvRedirectionLinkWebRussian;

	public String getNarTvRedirectionLinkAndroid() {
		return narTvRedirectionLinkAndroid;
	}

	public void setNarTvRedirectionLinkAndroid(String narTvRedirectionLinkAndroid) {
		this.narTvRedirectionLinkAndroid = narTvRedirectionLinkAndroid;
	}

	public String getNarTvRedirectionLinkIos() {
		return narTvRedirectionLinkIos;
	}

	public void setNarTvRedirectionLinkIos(String narTvRedirectionLinkIos) {
		this.narTvRedirectionLinkIos = narTvRedirectionLinkIos;
	}

	public String getNarTvRedirectionLinkWebEnglish() {
		return narTvRedirectionLinkWebEnglish;
	}

	public void setNarTvRedirectionLinkWebEnglish(String narTvRedirectionLinkWebEnglish) {
		this.narTvRedirectionLinkWebEnglish = narTvRedirectionLinkWebEnglish;
	}

	public String getNarTvRedirectionLinkWebAzeri() {
		return narTvRedirectionLinkWebAzeri;
	}

	public void setNarTvRedirectionLinkWebAzeri(String narTvRedirectionLinkWebAzeri) {
		this.narTvRedirectionLinkWebAzeri = narTvRedirectionLinkWebAzeri;
	}

	public String getNarTvRedirectionLinkWebRussian() {
		return narTvRedirectionLinkWebRussian;
	}

	public void setNarTvRedirectionLinkWebRussian(String narTvRedirectionLinkWebRussian) {
		this.narTvRedirectionLinkWebRussian = narTvRedirectionLinkWebRussian;
	}

	@Override
	public String toString() {
		return "NarTv [narTvRedirectionLinkAndroid=" + narTvRedirectionLinkAndroid + ", narTvRedirectionLinkIos="
				+ narTvRedirectionLinkIos + ", narTvRedirectionLinkWebEnglish=" + narTvRedirectionLinkWebEnglish
				+ ", narTvRedirectionLinkWebAzeri=" + narTvRedirectionLinkWebAzeri + ", narTvRedirectionLinkWebRussian="
				+ narTvRedirectionLinkWebRussian + "]";
	}

}
