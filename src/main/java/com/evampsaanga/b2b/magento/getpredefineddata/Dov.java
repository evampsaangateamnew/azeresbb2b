package com.evampsaanga.b2b.magento.getpredefineddata;

public class Dov {

	private String header;
	private String footer;

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public String getFooter() {
		return footer;
	}

	public void setFooter(String footer) {
		this.footer = footer;
	}

	@Override
	public String toString() {
		return "Dov [header=" + header + ", footer=" + footer + "]";
	}

}
