package com.evampsaanga.b2b.magento.getpredefineddata;

public class Topup {

	private GetLoan getLoan;
	private MoneyTransfer moneyTransfer;

	public GetLoan getGetLoan() {
		return getLoan;
	}

	public void setGetLoan(GetLoan getLoan) {
		this.getLoan = getLoan;
	}

	public MoneyTransfer getMoneyTransfer() {
		return moneyTransfer;
	}

	public void setMoneyTransfer(MoneyTransfer moneyTransfer) {
		this.moneyTransfer = moneyTransfer;
	}
}
