package com.evampsaanga.b2b.magento.getpredefineddata;

public class MoneyTransfer {
	private SelectAmount selectAmount;
	private TermsAndCondition termsAndCondition;

	public SelectAmount getSelectAmount() {
		return selectAmount;
	}

	public void setSelectAmount(SelectAmount selectAmount) {
		this.selectAmount = selectAmount;
	}

	public TermsAndCondition getTermsAndCondition() {
		return termsAndCondition;
	}

	public void setTermsAndCondition(TermsAndCondition termsAndCondition) {
		this.termsAndCondition = termsAndCondition;
	}

	@Override
	public String toString() {
		return "ClassPojo [selectAmount = " + selectAmount + ", termsAndCondition = " + termsAndCondition + "]";
	}
}
