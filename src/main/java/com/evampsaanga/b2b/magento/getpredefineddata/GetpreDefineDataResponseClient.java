package com.evampsaanga.b2b.magento.getpredefineddata;

import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;

public class GetpreDefineDataResponseClient extends BaseResponse {

	com.evampsaanga.b2b.magento.getpredefineddata.Data data = new Data();

	/**
	 * @return the data
	 */
	public com.evampsaanga.b2b.magento.getpredefineddata.Data getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(com.evampsaanga.b2b.magento.getpredefineddata.Data data) {
		this.data = data;
	}
}
