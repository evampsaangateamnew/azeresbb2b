package com.evampsaanga.b2b.magento.getpredefineddata;

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;

public class GetpreDefineDataRequestClient extends BaseRequest {
	private String offeringId;

	public String getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(String offeringId) {
		this.offeringId = offeringId;
	}
}
