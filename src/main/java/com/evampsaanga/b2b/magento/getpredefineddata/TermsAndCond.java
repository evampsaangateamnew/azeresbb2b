package com.evampsaanga.b2b.magento.getpredefineddata;

public class TermsAndCond {
	private String amount;
	private String text;
	private String currency;

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	@Override
	public String toString() {
		return "ClassPojo [amount = " + amount + ", text = " + text + ", currency = " + currency + "]";
	}
}
