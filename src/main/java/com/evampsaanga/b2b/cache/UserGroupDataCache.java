package com.evampsaanga.b2b.cache;

/**
 * @author Aqeel Abbas
 * Pic user data and group data caching 
 */
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import com.evampsaanga.azerfon.appserver.refreshappservercache.BuildCacheRequestLand;
import com.evampsaanga.b2b.azerfon.db.DBAzerfonFactory;
import com.evampsaanga.b2b.azerfon.db.DBFactory;
import com.evampsaanga.b2b.azerfon.getUsersV2.UsersGroupData;
import com.evampsaanga.b2b.azerfon.getUsersV2.UsersGroupRequest;
import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.hazelcast.nio.Connection;
import com.sun.istack.logging.Logger;

public class UserGroupDataCache {
	//public static HashMap<String, List<UsersGroupData>> users = new HashMap<>();
	public static HashMap<String, String> usersCount = new HashMap<>();

	public static final Logger logger = Logger.getLogger("azerfon-esb", UserGroupDataCache.class);

	public List<UsersGroupData> checkUsersInCache(String request, String token) throws IOException, Exception {
		// printdb();
		UsersGroupRequest usersGroupRequest = Helper.JsonToObject(request, UsersGroupRequest.class);
		String virtualCorpCode = usersGroupRequest.getVirtualCorpCode();
		
		if(usersGroupRequest.getGroupType()!=null && !usersGroupRequest.getGroupType().isEmpty())
		{
			if (BuildCacheRequestLand.usersCache.containsKey(virtualCorpCode + "." + usersGroupRequest.getLang())) {
				Helper.logInfoMessageV2("User with virtualCorpCode " + virtualCorpCode + " is found in Cache");
				//return BuildCacheRequestLand.usersCache.get(virtualCorpCode + "." + usersGroupRequest.getLang());
				
				
				List<UsersGroupData> list = BuildCacheRequestLand.usersCache.get(virtualCorpCode + "." + usersGroupRequest.getLang());
				
				Helper.logInfoMessageV2("List size against virtual corp code"+usersGroupRequest.getVirtualCorpCode()+"is :::"+list.size());
				
				
				
				if(usersGroupRequest.getGroupType().equalsIgnoreCase("PartPay"))
				{
					Helper.logInfoMessageV2("Group Type is"+usersGroupRequest.getGroupType()+ " Filtering PartPay Users");
					List<UsersGroupData> listPartPay=new ArrayList<UsersGroupData>();
					for(int i=0;i<list.size();i++)
					{
//						if(list.get(i).getVirtualCorpCode()!=null && !list.get(i).getVirtualCorpCode().isEmpty() && !list.get(i).getVirtualCorpCode().equalsIgnoreCase("") 
//								&& list.get(i).getGroupCode()!=null && !list.get(i).getGroupCode().isEmpty() && !list.get(i).getGroupCode().equalsIgnoreCase("") )
						
						if(list.get(i).getVirtualCorpCode()!=null && !list.get(i).getVirtualCorpCode().isEmpty() && !list.get(i).getVirtualCorpCode().equalsIgnoreCase("") 
								&& list.get(i).getGroupCode().equalsIgnoreCase(usersGroupRequest.getGroupId()))
						{
							listPartPay.add(list.get(i));
						}
					}
					Helper.logInfoMessageV2("Pagination Started");
					int limitRecords=Integer.parseInt(usersGroupRequest.getLimitUsers());
					limitRecords=(Integer.parseInt(usersGroupRequest.getPageNum()))*limitRecords;
					Helper.logInfoMessageV2("Max limit is"+limitRecords);
					int start	=       (Integer.parseInt(usersGroupRequest.getPageNum())-1)*Integer.parseInt(usersGroupRequest.getLimitUsers());
					Helper.logInfoMessageV2("Start limit is"+start);
					List<UsersGroupData> usersGroupData = new ArrayList<UsersGroupData>();
					Helper.logInfoMessageV2("Loop Started for Pagination");
					for(int i=start;i<limitRecords && i<listPartPay.size();i++)
					{
						usersGroupData.add(listPartPay.get(i));
					}
					Helper.logInfoMessageV2("Loop Ended of Pagination");
					Helper.logInfoMessageV2("Returning Part Pay Users");
					
					return usersGroupData;
				}
				else
				{
					Helper.logInfoMessageV2("Group Type is"+usersGroupRequest.getGroupType()+ " Filtering Pay By Subs Users");
					List<UsersGroupData> listPayBySubs=new ArrayList<UsersGroupData>();
					
					if(usersGroupRequest.getType().equalsIgnoreCase("voice"))
					{
						for(int i=0;i<list.size();i++)
						{
							if(list.get(i).getVirtualCorpCode()!=null && !list.get(i).getVirtualCorpCode().isEmpty() && !list.get(i).getVirtualCorpCode().equalsIgnoreCase("")
									&& (list.get(i).getGroupCode()==null || list.get(i).getGroupCode().isEmpty() && list.get(i).getGroupCode().equalsIgnoreCase("")))
							{
								listPayBySubs.add(list.get(i));
							}
						}
						Helper.logInfoMessageV2("Pagination Started");
						int limitRecords=Integer.parseInt(usersGroupRequest.getLimitUsers());
						limitRecords=(Integer.parseInt(usersGroupRequest.getPageNum()))*limitRecords;
						Helper.logInfoMessageV2("Max limit is"+limitRecords);
						int start	=       (Integer.parseInt(usersGroupRequest.getPageNum())-1)*Integer.parseInt(usersGroupRequest.getLimitUsers());
						Helper.logInfoMessageV2("Start limit is"+start);
						List<UsersGroupData> usersGroupData = new ArrayList<UsersGroupData>();
						Helper.logInfoMessageV2("Loop Started for Pagination");
						for(int i=start;i<limitRecords && i<listPayBySubs.size();i++)
						{
							usersGroupData.add(listPayBySubs.get(i));
						}
						Helper.logInfoMessageV2("Loop Ended of Pagination");
						Helper.logInfoMessageV2("Returning Pay By Subs Users");
						
						return usersGroupData;
					}
					
					else if(usersGroupRequest.getType().equalsIgnoreCase("data"))
					{
						/* 
						 * Returning Empty List Because of This reason
						 * if PIC account is data related type, then need to reflect ONLY numbers with indicated group ID (during sign up)
						 */
						List<UsersGroupData> usersGroupData = new ArrayList<UsersGroupData>();
						Helper.logInfoMessageV2("PayBySub is not valid for data type Pic user");
						return usersGroupData;
						
					}
					
					else
					{
						Helper.logInfoMessageV2("Expected Type is not provided (Voice/Data)");
						List<UsersGroupData> usersGroupData = new ArrayList<UsersGroupData>();
						
						return usersGroupData;
					}
					
				
				}
				
			} else {
				Helper.logInfoMessageV2(token+"checkUsersFromDb:  with virtualCorpCode: " + virtualCorpCode);
				List<UsersGroupData> list = getUsersGroupDataDB(usersGroupRequest,token);
				Helper.logInfoMessageV2("List size against virtual corp code"+usersGroupRequest.getVirtualCorpCode()+"is :::"+list.size());
				BuildCacheRequestLand.usersCache.put(virtualCorpCode + "." + usersGroupRequest.getLang(), list);
				//
				if(usersGroupRequest.getGroupType().equalsIgnoreCase("PartPay"))
				{
					Helper.logInfoMessageV2("Group Type is"+usersGroupRequest.getGroupType()+ " Filtering PartPay Users");
					List<UsersGroupData> listPartPay=new ArrayList<UsersGroupData>();
					for(int i=0;i<list.size();i++)
					{
//						if(list.get(i).getVirtualCorpCode()!=null && !list.get(i).getVirtualCorpCode().isEmpty() && !list.get(i).getVirtualCorpCode().equalsIgnoreCase("") 
//								&& list.get(i).getGroupCode()!=null && !list.get(i).getGroupCode().isEmpty() && !list.get(i).getGroupCode().equalsIgnoreCase("") )
						if(list.get(i).getVirtualCorpCode()!=null && !list.get(i).getVirtualCorpCode().isEmpty() && !list.get(i).getVirtualCorpCode().equalsIgnoreCase("") 
								&& list.get(i).getGroupCode().equalsIgnoreCase(usersGroupRequest.getGroupId()))
						
						{
							listPartPay.add(list.get(i));
						}
					}
					Helper.logInfoMessageV2("Pagination Started");
					int limitRecords=Integer.parseInt(usersGroupRequest.getLimitUsers());
					limitRecords=(Integer.parseInt(usersGroupRequest.getPageNum()))*limitRecords;
					Helper.logInfoMessageV2("Max limit is"+limitRecords);
					int start	=       (Integer.parseInt(usersGroupRequest.getPageNum())-1)*Integer.parseInt(usersGroupRequest.getLimitUsers());
					Helper.logInfoMessageV2("Start limit is"+start);
					List<UsersGroupData> usersGroupData = new ArrayList<UsersGroupData>();
					Helper.logInfoMessageV2("Loop Started for Pagination");
					for(int i=start;i<limitRecords && i<listPartPay.size();i++)
					{
						usersGroupData.add(listPartPay.get(i));
					}
					Helper.logInfoMessageV2("Loop Ended of Pagination");
					Helper.logInfoMessageV2("Returning Part Pay Users");
					return usersGroupData;
				}
				else
				{
					Helper.logInfoMessageV2("Group Type is"+usersGroupRequest.getGroupType()+ " Filtering Pay By Subs Users");
					List<UsersGroupData> listPayBySubs=new ArrayList<UsersGroupData>();
					
					if(usersGroupRequest.getType().equalsIgnoreCase("voice")){
						for(int i=0;i<list.size();i++)
						{
							if(list.get(i).getVirtualCorpCode()!=null && !list.get(i).getVirtualCorpCode().isEmpty() && !list.get(i).getVirtualCorpCode().equalsIgnoreCase("")
									&& (list.get(i).getGroupCode()==null || list.get(i).getGroupCode().isEmpty() && list.get(i).getGroupCode().equalsIgnoreCase("")))
							{
								listPayBySubs.add(list.get(i));
							}
						}
						Helper.logInfoMessageV2("Pagination Started");
						int limitRecords=Integer.parseInt(usersGroupRequest.getLimitUsers());
						limitRecords=(Integer.parseInt(usersGroupRequest.getPageNum()))*limitRecords;
						Helper.logInfoMessageV2("Max limit is"+limitRecords);
						int start	=       (Integer.parseInt(usersGroupRequest.getPageNum())-1)*Integer.parseInt(usersGroupRequest.getLimitUsers());
						Helper.logInfoMessageV2("Start limit is"+start);
						List<UsersGroupData> usersGroupData = new ArrayList<UsersGroupData>();
						Helper.logInfoMessageV2("Loop Started for Pagination");
						for(int i=start;i<limitRecords && i<listPayBySubs.size();i++)
						{
							usersGroupData.add(listPayBySubs.get(i));
						}
						Helper.logInfoMessageV2("Loop Ended of Pagination");
						Helper.logInfoMessageV2("Returning Pay By Subs Users");
						return usersGroupData;
					}
					
					else if(usersGroupRequest.getType().equalsIgnoreCase("data"))
					{
						/* 
						 * Returning Empty List Because of This reason
						 * if PIC account is data related type, then need to reflect ONLY numbers with indicated group ID (during sign up)
						 */
						List<UsersGroupData> usersGroupData = new ArrayList<UsersGroupData>();
						Helper.logInfoMessageV2("PayBySub is not valid for data type Pic user");
						return usersGroupData;
						
					}
					
					else
					{
						Helper.logInfoMessageV2("Expected Type is not provided (Voice/Data)");
						List<UsersGroupData> usersGroupData = new ArrayList<UsersGroupData>();
						
						return usersGroupData;
					}
				
				}
			}
		}
		
		//in azerfon we have removed cache as we are not getting users from ods now,  we are using our own database
		
		else{
			if (BuildCacheRequestLand.usersCache.containsKey(virtualCorpCode + "." + usersGroupRequest.getLang())) {
				Helper.logInfoMessageV2("User with virtualCorpCode " + virtualCorpCode + " is found in Cache");
				//return BuildCacheRequestLand.usersCache.get(virtualCorpCode + "." + usersGroupRequest.getLang());
				
				
				List<UsersGroupData> list = BuildCacheRequestLand.usersCache.get(virtualCorpCode + "." + usersGroupRequest.getLang());
				Helper.logInfoMessageV2("List size against virtual corp code"+usersGroupRequest.getVirtualCorpCode()+"is :::"+list.size());
				
				List<UsersGroupData> listType = new ArrayList<UsersGroupData>();
				if(usersGroupRequest.getType().equalsIgnoreCase("voice")){
					for (int i = 0; i < list.size(); i++) {
						
						if(list.get(i).getGroupCode()==null ||list.get(i).getGroupCode().isEmpty() || list.get(i).getGroupCode().equals(usersGroupRequest.getGroupId()))
						{
							listType.add(list.get(i));
						}
					}
				}
				
				else if(usersGroupRequest.getType().equalsIgnoreCase("data"))
				{
						for (int i = 0; i < list.size(); i++) {
						
						if(list.get(i).getGroupCode().equals(usersGroupRequest.getGroupId()))
						{
							listType.add(list.get(i));
						}
					}
					
				}
				
				
				Helper.logInfoMessageV2("Pagination Started");
				int limitRecords=Integer.parseInt(usersGroupRequest.getLimitUsers());
				limitRecords=(Integer.parseInt(usersGroupRequest.getPageNum()))*limitRecords;
				Helper.logInfoMessageV2("Max limit is"+limitRecords);
				int start	=       (Integer.parseInt(usersGroupRequest.getPageNum())-1)*Integer.parseInt(usersGroupRequest.getLimitUsers());
				Helper.logInfoMessageV2("Start limit is"+start);
				List<UsersGroupData> usersGroupData = new ArrayList<UsersGroupData>();
				Helper.logInfoMessageV2("Loop Started for Pagination");
				for(int i=start;i<limitRecords && i<listType.size();i++)
				{
					usersGroupData.add(listType.get(i));
				}
				Helper.logInfoMessageV2("Loop Ended of Pagination");
				
				return usersGroupData;
			} else {
				Helper.logInfoMessageV2(token+"checkUsersFromDb:  with virtualCorpCode: " + virtualCorpCode);
				List<UsersGroupData> list = getUsersGroupDataDB(usersGroupRequest,token);
				Helper.logInfoMessageV2("List size against virtual corp code"+usersGroupRequest.getVirtualCorpCode()+"is :::"+list.size());
				BuildCacheRequestLand.usersCache.put(virtualCorpCode + "." + usersGroupRequest.getLang(), list);
				//
				List<UsersGroupData> listType = new ArrayList<UsersGroupData>();
				if(usersGroupRequest.getType().equalsIgnoreCase("voice")){
					for (int i = 0; i < list.size(); i++) {
						
						if(list.get(i).getGroupCode()==null ||list.get(i).getGroupCode().isEmpty() || list.get(i).getGroupCode().equals(usersGroupRequest.getGroupId()))
						{
							listType.add(list.get(i));
						}
					}
				}
				
				else if(usersGroupRequest.getType().equalsIgnoreCase("data"))
				{
						for (int i = 0; i < list.size(); i++) {
						
						if(list.get(i).getGroupCode().equals(usersGroupRequest.getGroupId()))
						{
							listType.add(list.get(i));
						}
					}
					
				}
				
				Helper.logInfoMessageV2("List size against virtual corp code of type"+usersGroupRequest.getType()+"is :::"+listType.size());
				
				Helper.logInfoMessageV2("Pagination Started");
				int limitRecords=Integer.parseInt(usersGroupRequest.getLimitUsers());
				limitRecords=(Integer.parseInt(usersGroupRequest.getPageNum()))*limitRecords;
				Helper.logInfoMessageV2("Max limit is"+limitRecords);
				int start	=       (Integer.parseInt(usersGroupRequest.getPageNum())-1)*Integer.parseInt(usersGroupRequest.getLimitUsers());
				Helper.logInfoMessageV2("Start limit is"+start);
				List<UsersGroupData> usersGroupData = new ArrayList<UsersGroupData>();
				Helper.logInfoMessageV2("Loop Started for Pagination");
				for(int i=start;i<limitRecords && i<listType.size();i++)
				{
					usersGroupData.add(listType.get(i));
				}
				Helper.logInfoMessageV2("Loop Ended of Pagination");
				
				return usersGroupData;
				//return usersGroupData;
			}
		}
		

	}
/*
 * 
 */
	
	private List<UsersGroupData> getUsersGroupDataDB(UsersGroupRequest usersGroupRequest, String token) {
		List<UsersGroupData> usersData = new ArrayList<UsersGroupData>();
		// MagentoServices services = new MagentoServices();
		// printing DB Data
		// services.printDB();

		try {
			
//			int limitRecords=Integer.parseInt(usersGroupRequest.getLimitUsers());
//			Helper.logInfoMessageV2(token+ "Limit record Value : "+limitRecords);
//			int limits=       (Integer.parseInt(usersGroupRequest.getPageNum())-1)*limitRecords;
//			Helper.logInfoMessageV2(token+ "Limit alculated record Value : "+limits);
			
			String sqlQuery="SELECT * FROM user_data WHERE VIRTUAL_CORP_CODE='"+usersGroupRequest.getVirtualCorpCode()+"'";
			Helper.logInfoMessageV2(token+  " --   SQL Query @@GetusersGroupData is :" + sqlQuery);
			ResultSet rs = DBFactory.getDbConnection().prepareStatement(sqlQuery).executeQuery();
			/*
			 * ResultSetMetaData metadata = rs.getMetaData(); int columnCount =
			 * metadata.getColumnCount(); for (int i = 1; i <= columnCount; i++)
			 * { Helper.logInfoMessage(metadata.getColumnName(i) + ", "); }
			 */
		
			while (rs.next()) {
				Helper.logInfoMessageV2(token+" SQL Query Result :");
				UsersGroupData users = new UsersGroupData();
				users.setMsisdn(rs.getString("MSISDN"));
				users.setTariffPlan(rs.getString("TARIFF_PLAN"));
				users.setCrmSubId(rs.getString("CRM_SUB_ID"));
				users.setCrmCustId(rs.getString("CRM_CUST_ID"));
				users.setCrmCustCode(rs.getString("CUST_CODE"));
				users.setVirtualCorpCode(rs.getString("VIRTUAL_CORP_CODE"));
				users.setCrmCorpCustId(rs.getString("CRM_CORP_CUST_ID"));
				users.setCugGroupCode(rs.getString("CUG_GROUP_CODE"));
				users.setGroupCode(rs.getString("GROUP_CODE"));
				users.setCrmAccountId(rs.getString("CRM_ACCT_ID"));
				users.setAccCode(rs.getString("ACCT_CODE"));
				users.setCrmAccIdPaid(rs.getString("CRM_ACCT_ID_PAID"));
				users.setTariffNameDisplay(ConfigurationManager.getConfigurationFromCache("tariff.name."+rs.getString("TARIFF_PLAN")+"."+usersGroupRequest.getLang()));
				
				
				users.setGroupName( groupName( users.getVirtualCorpCode(),  users.getGroupCode(),  usersGroupRequest.getType()));
				String disp_name = ConfigurationManager.getConfigurationFromCache(ConfigurationManager.MAPPING_GROUPIDTRANS + usersGroupRequest.getLang() + "." + users.getGroupName());
				users.setGroupNameDisplay(disp_name);
				// logger.info("@@@@@@@@@@@@@@@@ users data :" + users);
				usersData.add(users);
			}
			rs.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			Helper.logInfoMessageV2(token+"SQLException", Helper.GetException(e));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Helper.logInfoMessageV2(token+"Exception", Helper.GetException(e));
		}
		return usersData;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
//	private List<UsersGroupData> getUsersGroupDataDB(UsersGroupRequest usersGroupRequest, String token) {
//		List<UsersGroupData> usersData = new ArrayList<UsersGroupData>();
//		// MagentoServices services = new MagentoServices();
//		// printing DB Data
//		// services.printDB();
//
//		try {
//			/*String sqlQuery = "select PRI_IDENTITY,PROFILE_ID,BRAND_ID,CORP_ID,GROUP_SUBS_ID,GROUP_CUST_NAME,GROUP_ID,"
//					+ "CORP_CRM_CUST_ID,PIC_NAME,CORP_ACCT_CODE,CORP_CRM_ACCT_ID,CUST_FST_NAME,CUST_LAST_NAME from V_E_CARE_CORP_INFO where CORP_CRM_CUST_ID = "
//					+ customerID + "order by PRI_IDENTITY";*/
//			int limitRecords=Integer.parseInt(usersGroupRequest.getLimitUsers());
//			Helper.logInfoMessageV2(token+ "Limit record Value : "+limitRecords);
//			int limits=       (Integer.parseInt(usersGroupRequest.getPageNum())-1)*limitRecords;
//			Helper.logInfoMessageV2(token+ "Limit alculated record Value : "+limits);
//			
//			String sqlQuery="SELECT * FROM user_data WHERE VIRTUAL_CORP_CODE='"+usersGroupRequest.getVirtualCorpCode()+"' LIMIT "+limits+" , "+limitRecords+"  ";
//			Helper.logInfoMessageV2(token+  " --   SQL Query @@GetusersGroupData is :" + sqlQuery);
//			ResultSet rs = DBFactory.getDbConnection().prepareStatement(sqlQuery).executeQuery();
//			/*
//			 * ResultSetMetaData metadata = rs.getMetaData(); int columnCount =
//			 * metadata.getColumnCount(); for (int i = 1; i <= columnCount; i++)
//			 * { Helper.logInfoMessage(metadata.getColumnName(i) + ", "); }
//			 */
//			
//			String row = "";
//			while (rs.next()) {
//				Helper.logInfoMessageV2(token+" SQL Query Result :" + row);
//				UsersGroupData users = new UsersGroupData();
//				users.setMsisdn(rs.getString("MSISDN"));
//				users.setTariffPlan(rs.getString("TARIFF_PLAN"));
//				users.setCrmSubId(rs.getString("CRM_SUB_ID"));
//				users.setCrmCustId(rs.getString("CRM_CUST_ID"));
//				users.setCrmCustCode(rs.getString("CUST_CODE"));
//				users.setVirtualCorpCode(rs.getString("VIRTUAL_CORP_CODE"));
//				users.setCrmCorpCustId(rs.getString("CRM_CORP_CUST_ID"));
//				users.setCugGroupCode(rs.getString("CUG_GROUP_CODE"));
//				users.setGroupCode(rs.getString("GROUP_CODE"));
//				users.setCrmAccountId(rs.getString("CRM_ACCT_ID"));
//				users.setAccCode(rs.getString("ACCT_CODE"));
//				users.setCrmAccIdPaid(rs.getString("CRM_ACCT_ID_PAID"));
//				users.setTariffNameDisplay(ConfigurationManager.getConfigurationFromCache("tariff.name."+rs.getString("TARIFF_PLAN")+"."+usersGroupRequest.getLang()));
//				
//				
//				users.setGroupName( groupName( users.getVirtualCorpCode(),  users.getGroupCode(),  usersGroupRequest.getType()));
//				String disp_name = ConfigurationManager.getConfigurationFromCache(ConfigurationManager.MAPPING_GROUPIDTRANS + usersGroupRequest.getLang() + "." + users.getGroupName());
//				users.setGroupNameDisplay(disp_name);
//				// logger.info("@@@@@@@@@@@@@@@@ users data :" + users);
//				usersData.add(users);
//			}
//			rs.close();
//
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			Helper.logInfoMessageV2(token+"SQLException", Helper.GetException(e));
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			Helper.logInfoMessageV2(token+"Exception", Helper.GetException(e));
//		}
//		return usersData;
//	}
	
	
	
	public List<UsersGroupData> searchUsers(String request, String token) throws IOException, Exception {
		
		UsersGroupRequest usersGroupRequest = Helper.JsonToObject(request, UsersGroupRequest.class);
		String searchKey = usersGroupRequest.getSearchUsers();
		
	
			Helper.logInfoMessageV2(token+"checkUsersInCache:  with Search Key: " + searchKey);
			List<UsersGroupData> usersGroupData = getUsersGroupDataSearch(usersGroupRequest,token);
			List<UsersGroupData> usersGroupDataSearch = new ArrayList<UsersGroupData>();
			if(usersGroupRequest.getType().equalsIgnoreCase("data"))
				{
				Helper.logInfoMessageV2(token+"Data Block Before Filter List Size is:: " + usersGroupData.size());
				usersGroupDataSearch =usersGroupData.stream().filter(item-> item.getGroupCode().equalsIgnoreCase(usersGroupRequest.getGroupId())).collect(Collectors.toList());
						
				Helper.logInfoMessageV2(token+"Data Block After Filter List Size is:: " + usersGroupDataSearch.size());
//					for (int i = 0; i < usersGroupData.size(); i++) {
//						if(usersGroupData.get(i).getGroupCode().equalsIgnoreCase(usersGroupRequest.getGroupId()))
//						{
//							usersGroupDataSearch.add(usersGroupData.get(i));
//						}
//					}
				}
			
			if(usersGroupRequest.getType().equalsIgnoreCase("voice"))
			{
				Helper.logInfoMessageV2(token+"Voice Block Before Filter List Size is:: " + usersGroupData.size());
				Predicate<UsersGroupData> con1 =item-> item.getGroupCode().equalsIgnoreCase(usersGroupRequest.getGroupId());
				Predicate<UsersGroupData> con2 =item-> item.getGroupCode().equalsIgnoreCase("");
				
				usersGroupDataSearch =usersGroupData.stream().filter(con1.or(con2)).collect(Collectors.toList());
				Helper.logInfoMessageV2(token+"Voice Block After Filter List Size is:: " + usersGroupDataSearch.size());
			}
			
			return usersGroupDataSearch;
	
		
	}
	
	private List<UsersGroupData> getUsersGroupDataSearch(UsersGroupRequest usersGroupRequest, String token) {
		List<UsersGroupData> usersData = new ArrayList<UsersGroupData>();
		// MagentoServices services = new MagentoServices();
		// printing DB Data
		// services.printDB();

		try {
			/*String sqlQuery = "select PRI_IDENTITY,PROFILE_ID,BRAND_ID,CORP_ID,GROUP_SUBS_ID,GROUP_CUST_NAME,GROUP_ID,"
					+ "CORP_CRM_CUST_ID,PIC_NAME,CORP_ACCT_CODE,CORP_CRM_ACCT_ID,CUST_FST_NAME,CUST_LAST_NAME from V_E_CARE_CORP_INFO where CORP_CRM_CUST_ID = "
					+ customerID + "order by PRI_IDENTITY";*/
			int limitRecords=Integer.parseInt(usersGroupRequest.getLimitUsers());
			Helper.logInfoMessageV2(token+ "Limit record Value : "+limitRecords);
			int limits=       (Integer.parseInt(usersGroupRequest.getPageNum())-1)*limitRecords;
			Helper.logInfoMessageV2(token+ "Limit alculated record Value : "+limits);
			
			String sqlQuery="SELECT * FROM user_data WHERE VIRTUAL_CORP_CODE='"+usersGroupRequest.getVirtualCorpCode()+"' AND MSISDN LIKE '%"+usersGroupRequest.getSearchUsers()+"%' LIMIT "+limits+" , "+limitRecords+"  ";
			Helper.logInfoMessageV2(token+  " --   SQL Query @@GetusersGroupDataSearch is :" + sqlQuery);
			ResultSet rs = DBFactory.getDbConnection().prepareStatement(sqlQuery).executeQuery();
		
			
			String row = "";
			while (rs.next()) {
				Helper.logInfoMessageV2(token+" SQL Query Result :" + row);
				UsersGroupData users = new UsersGroupData();
				users.setMsisdn(rs.getString("MSISDN"));
				users.setTariffPlan(rs.getString("TARIFF_PLAN"));
				users.setCrmSubId(rs.getString("CRM_SUB_ID"));
				users.setCrmCustId(rs.getString("CRM_CUST_ID"));
				users.setCrmCustCode(rs.getString("CUST_CODE"));
				users.setVirtualCorpCode(rs.getString("VIRTUAL_CORP_CODE"));
				users.setCrmCorpCustId(rs.getString("CRM_CORP_CUST_ID"));
				users.setCugGroupCode(rs.getString("CUG_GROUP_CODE"));
				users.setGroupCode(rs.getString("GROUP_CODE"));
				users.setCrmAccountId(rs.getString("CRM_ACCT_ID"));
				users.setAccCode(rs.getString("ACCT_CODE"));
				users.setCrmAccIdPaid(rs.getString("CRM_ACCT_ID_PAID"));
				users.setTariffNameDisplay(ConfigurationManager.getConfigurationFromCache("tariff.name."+rs.getString("TARIFF_PLAN")+"."+usersGroupRequest.getLang()));
				users.setGroupName( groupName( users.getVirtualCorpCode(),  users.getGroupCode(),  usersGroupRequest.getType()));
				String disp_name = ConfigurationManager.getConfigurationFromCache(ConfigurationManager.MAPPING_GROUPIDTRANS + usersGroupRequest.getLang() + "." + users.getGroupName());
				users.setGroupNameDisplay(disp_name);
				// logger.info("@@@@@@@@@@@@@@@@ users data :" + users);
				usersData.add(users);
			}
			rs.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			Helper.logInfoMessageV2(token+"SQLException", Helper.GetException(e));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Helper.logInfoMessageV2(token+"Exception", Helper.GetException(e));
		}
		return usersData;
	}

	public String countUsers(String virtualCorpCode, String groupCode, String groupType) {

		if (usersCount.containsKey(virtualCorpCode+"_"+groupType)) {
			Helper.logInfoMessageV2("User with virtualCorpCode" + virtualCorpCode+"_"+groupType + " is found in Cache");
			return usersCount.get(virtualCorpCode+"_"+groupType);
		} else {
			Helper.logInfoMessageV2("Populating Cache of User Count with virtualCorpCode: " + virtualCorpCode+"_"+groupType);
			String userCount = getUsersCount(virtualCorpCode,groupCode,groupType);
			usersCount.put(virtualCorpCode+"_"+groupType, userCount);
			return userCount;
		}

	}

	private String getUsersCount(String virtualCorpCode, String groupCode, String groupType) {
		String userCount = "0";
		String query="";
		if(groupType.equalsIgnoreCase("data"))
		{
			
			query = "select count(*) from user_data where VIRTUAL_CORP_CODE = " + virtualCorpCode +" AND GROUP_CODE = "+ groupCode;
			Helper.logInfoMessageV2("Calculating Users Count for data with query::"+query);
		}
		
		if(groupType.equalsIgnoreCase("voice"))
		{
			query = "select count(*) from user_data where VIRTUAL_CORP_CODE = " + virtualCorpCode +" && (GROUP_CODE = "+ groupCode + " || GROUP_CODE = '' )";
			Helper.logInfoMessageV2("Calculating Users Count for voice with query::"+query);
		}
		
		
		try {
			Helper.logInfoMessageV2("Query to count users: " + query);
			ResultSet rs = DBFactory.getDbConnection().prepareStatement(query).executeQuery();
			while (rs.next()) {
				userCount = rs.getString(1);
			}
		} catch (SQLException e) {
			logger.info("ERROR: ", e);
		}
		Helper.logInfoMessageV2("User Count is: " + userCount);
		return userCount;
	}
	
	public String getUsersCountSearch(UsersGroupRequest request) {
		String userCount = "0";
		String query = "";
		
		if(request.getType().equalsIgnoreCase("data"))
		{
			
			query ="select count(*) from user_data WHERE MSISDN Like " +"'%"+request.getSearchUsers()+"%'" +"AND VIRTUAL_CORP_CODE='"+request.getVirtualCorpCode()+"' AND GROUP_CODE = "+ request.getGroupId();
			Helper.logInfoMessageV2("Calculating Users Count for Search of data with query::"+query);
		}
		
		if(request.getType().equalsIgnoreCase("voice"))
		{
			query = "select count(*) from user_data where MSISDN Like " +"'%"+request.getSearchUsers()+"%'" +" AND VIRTUAL_CORP_CODE = " + request.getVirtualCorpCode() +" && (GROUP_CODE = "+ request.getGroupId() + " || GROUP_CODE = '' )";
			Helper.logInfoMessageV2("Calculating Users Count  for Search of voice with query::"+query);
		}
		
		try {
			Helper.logInfoMessageV2("Query to count users: " + query);
			ResultSet rs = DBFactory.getDbConnection().prepareStatement(query).executeQuery();
			while (rs.next()) {
				userCount = rs.getString(1);
			}
		} catch (SQLException e) {
			logger.info("ERROR: ", e);
		}
		Helper.logInfoMessageV2("User Count is: " + userCount);
		return userCount;
	}

	public void printdb() {
		String query = "select distinct(GROUP_CODE) from V_E_CARE_CORP_INFO ";
		try {
			ResultSet rs = DBAzerfonFactory.getConnection().prepareStatement(query).executeQuery();
			ResultSetMetaData metadata = rs.getMetaData();
			int columnCount = metadata.getColumnCount();
			Helper.logInfoMessageV2("<<<<<<<<< E&S Group Data >>>>>>>>>");
			for (int i = 1; i <= columnCount; i++) {
				Helper.logInfoMessage("E&S-- " + metadata.getColumnName(i) + ", ");
			}
			// String row = "";
			for (int i = 0; i < rs.getFetchSize(); i++) {
				Helper.logInfoMessageV2("E&S-- " + rs.getString(i));
			}
			/*
			 * while (rs.next() || rs.getFetchSize()) { System.out.println(row);
			 * System.out.print(rs.get); }
			 */

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			logger.info("ERROR: ", e);
		}

	}
	public String groupName(String virtualCorpCode, String groupCode, String type)
	{
		String groupName="";
		
		if(type.equalsIgnoreCase("data"))
		{
			if(virtualCorpCode!=null && !virtualCorpCode.isEmpty() && !virtualCorpCode.equalsIgnoreCase("") 
					&& groupCode!=null && !groupCode.isEmpty() && !groupCode.equalsIgnoreCase("") )
			{
				groupName="PartPay";
			}
			
			else if(virtualCorpCode!=null && !virtualCorpCode.isEmpty() && !virtualCorpCode.equalsIgnoreCase("")
					&& (groupCode==null || groupCode.isEmpty() && groupCode.equalsIgnoreCase("")))
			{
				groupName="PartPay";
			}
		}
		else if(type.equalsIgnoreCase("voice"))
		{
			if(virtualCorpCode!=null && !virtualCorpCode.isEmpty() && !virtualCorpCode.equalsIgnoreCase("") 
					&& groupCode!=null && !groupCode.isEmpty() && !groupCode.equalsIgnoreCase("") )
			{
				groupName="PartPay";
			}
			else if(virtualCorpCode!=null && !virtualCorpCode.isEmpty() && !virtualCorpCode.equalsIgnoreCase("")
					&& (groupCode==null || groupCode.isEmpty() && groupCode.equalsIgnoreCase("")))
			{
				groupName="PayBySubs";
			}
		}
		return groupName;
		
	}

}
