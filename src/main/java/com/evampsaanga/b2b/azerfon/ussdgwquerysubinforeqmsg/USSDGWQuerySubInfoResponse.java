package com.evampsaanga.b2b.azerfon.ussdgwquerysubinforeqmsg;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.crm.query.GetSubscriberOut;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "USSDGWQuerySubInfoResponse", propOrder = { "responseBody" })
@XmlRootElement(name = "USSDGWQuerySubInfoResponse")
public class USSDGWQuerySubInfoResponse extends com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse {
	@XmlElement(name = "responseBody", required = true)
	private GetSubscriberOut responseBody = new GetSubscriberOut();

	public USSDGWQuerySubInfoResponse(GetSubscriberOut responseBody) {
		super();
		this.responseBody = responseBody;
	}

	public USSDGWQuerySubInfoResponse() {
		super();
	}

	public GetSubscriberOut getResponseBody() {
		return responseBody;
	}

	public void setResponseBody(GetSubscriberOut responseBody) {
		this.responseBody = responseBody;
	}
}
