package com.evampsaanga.b2b.azerfon.ussdgwquerysubinforeqmsg;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.developer.utils.Decrypter;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.evampsaanga.b2b.developer.utils.SOAPLoggingHandler;
import com.evampsaanga.b2b.gethomepage.GetHomePageResponse;
import com.huawei.crm.basetype.RequestHeader;
import com.ngbss.evampsaanga.services.ThirdPartyCall;

@Path("/bakcell")
public class USSDGWQuerySubinfoLand {
	public static final Logger logger = Logger.getLogger("azerfon-esb");

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public USSDGWQuerySubInfoResponse Get(@Header("credentials") String credential, @Body() String requestBody) {
		try {
			logger.info("Request Landed on USSDGWQuerySubinfoLand:" + requestBody);
			USSDGWQuerySubInfoRequestClient cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, USSDGWQuerySubInfoRequestClient.class);
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				USSDGWQuerySubInfoResponse resp = new USSDGWQuerySubInfoResponse();
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
					GetHomePageResponse resp = new GetHomePageResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
				}
				if (credentials == null) {
					USSDGWQuerySubInfoResponse resp = new USSDGWQuerySubInfoResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						USSDGWQuerySubInfoResponse res = new USSDGWQuerySubInfoResponse();
						res.setReturnCode(ResponseCodes.ERROR_400);
						res.setReturnMsg(verification);
						return res;
					}
				} else {
					USSDGWQuerySubInfoResponse resp = new USSDGWQuerySubInfoResponse();
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					USSDGWQuerySubInfoResponse res = new USSDGWQuerySubInfoResponse();
					com.huawei.crm.query.GetSubscriberResponse subresponse = null;
					try {
						subresponse = GetSubscriberRequest(cclient.getmsisdn());
						if (subresponse.getResponseHeader().getRetCode().equals("0")) {
							res.setResponseBody(subresponse.getGetSubscriberBody());
						}
					} catch (Exception ex) {
						logger.error(Helper.GetException(ex));
					}
					res.setReturnCode(ResponseCodes.SUCESS_CODE_200);
					res.setReturnMsg(ResponseCodes.SUCESS_DES_200);
					return res;
				} else {
					USSDGWQuerySubInfoResponse resp = new USSDGWQuerySubInfoResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		USSDGWQuerySubInfoResponse resp = new USSDGWQuerySubInfoResponse();
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		return resp;
	}

	public com.huawei.crm.query.GetSubscriberResponse GetSubscriberRequest(String msisdn) {
		try {
//			HuaweiCRMPortType crmPortType = CRMServices.getInstance();
			com.huawei.crm.query.GetSubscriberIn getsubIn = new com.huawei.crm.query.GetSubscriberIn();
			getsubIn.setServiceNumber(msisdn);
			getsubIn.setIncludeOfferFlag(Constants.CRM_SUBSCRIBER_INCLUDE_OFFERING);
			com.huawei.crm.query.GetSubscriberRequest getSubReq = new com.huawei.crm.query.GetSubscriberRequest();
			getSubReq.setGetSubscriberBody(getsubIn);
			getSubReq.setRequestHeader(getRequestHeaderForCRM());
//			com.huawei.crm.query.GetSubscriberResponse response = crmPortType.getSubscriberData(getSubReq);
			com.huawei.crm.query.GetSubscriberResponse response = ThirdPartyCall.getSubscriberData(getSubReq);
			return response;
		} catch (Exception ee) {
			logger.error(Helper.GetException(ee));
		}
		return null;
	}

	public RequestHeader getRequestHeaderForCRM() {
		RequestHeader reqhForCRM = new RequestHeader();
		reqhForCRM.setChannelId(ConfigurationManager.getConfigurationFromCache("crm.sub.ChannelId").trim());
		reqhForCRM.setTechnicalChannelId(ConfigurationManager.getConfigurationFromCache("crm.sub.TechnicalChannelId").trim());
		reqhForCRM.setAccessUser(ConfigurationManager.getConfigurationFromCache("crm.sub.AccessUser").trim());
		reqhForCRM.setTenantId(ConfigurationManager.getConfigurationFromCache("crm.sub.TenantId").trim());
		reqhForCRM.setAccessPwd(ConfigurationManager.getConfigurationFromCache("crm.sub.AccessPwd").trim());
		reqhForCRM.setTestFlag(ConfigurationManager.getConfigurationFromCache("crm.sub.TestFlag").trim());
		reqhForCRM.setTransactionId(Helper.generateTransactionID());
		reqhForCRM.setLanguage(ConfigurationManager.getConfigurationFromCache("crm.sub.Language").trim());
		return reqhForCRM;
	}
}
