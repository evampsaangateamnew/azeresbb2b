package com.evampsaanga.b2b.azerfon.ussdgwquerysubinforeqmsg;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "USSDGWQuerySubInfoRequestClient", propOrder = { "requestBody" })
@XmlRootElement(name = "USSDGWQuerySubInfoRequestClient")
public class USSDGWQuerySubInfoRequestClient extends com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest {
	@XmlElement(name = "requestBody", required = true)
	private String requestBody = "";

	public USSDGWQuerySubInfoRequestClient() {
		super();
	}

	public String getRequestBody() {
		return requestBody;
	}

	public void setRequestBody(String body) {
		this.requestBody = body;
	}
}
