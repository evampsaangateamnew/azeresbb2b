package com.evampsaanga.b2b.azerfon.getfnfdatarequest;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.evampsaanga.b2b.azerfon.FNF;
import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetFnFDataResponseClient", propOrder = { "responseBody" })
@XmlRootElement(name = "GetFnFDataResponseClient")
public class GetFnFDataResponseClient extends BaseResponse {
	private ArrayList<FNF> list = new ArrayList<FNF>();
	private String fnfLimit = "5";

	public GetFnFDataResponseClient() {
		super();
	}

	/**
	 * @return the list
	 */
	public ArrayList<FNF> getList() {
		return list;
	}

	/**
	 * @param list
	 *            the list to set
	 */
	public void setList(ArrayList<FNF> list) {
		this.list = list;
	}

	public GetFnFDataResponseClient(ArrayList<FNF> list) {
		super();
		this.list = list;
	}

	public String getFnfLimit() {
		return fnfLimit;
	}

	public void setFnfLimit(String fnfLimit) {
		this.fnfLimit = fnfLimit;
	}
}
