package com.evampsaanga.b2b.azerfon.getfnfdatarequest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetFnFDataRequestClient", propOrder = { "requestBody" })
@XmlRootElement(name = "GetFnFDataRequestClient")
public class GetFnFDataRequestClient extends com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest {
	private String offeringId = "";

	public String getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(String offeringId) {
		this.offeringId = offeringId;
	}
}
