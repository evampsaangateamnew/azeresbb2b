package com.evampsaanga.b2b.azerfon.getfnfdatarequest;

import java.io.IOException;
import java.text.SimpleDateFormat;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.ws.WebServiceException;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.amqimplementationsesb.LogsType;
import com.evampsaanga.b2b.azerfon.FNF;
import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.ThirdPartyNames;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Decrypter;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.evampsaanga.b2b.developer.utils.SOAPLoggingHandler;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.huawei.crm.basetype.RequestHeader;
import com.huawei.crm.query.FnFGroupInfo;
import com.huawei.crm.query.FnFInfo;
import com.huawei.crm.query.GetFnFDataIn;
import com.huawei.crm.query.GetFnFDataRequest;
import com.huawei.crm.query.GetFnFDataResponse;
import com.ngbss.evampsaanga.services.ThirdPartyCall;

@Path("/azerfon")
public class GetFNFDataLand {
	RequestHeader reqH = null;
	public static final Logger logger = Logger.getLogger("azerfon-esb");

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetFnFDataResponseClient Get(@Header("credentials") String credential, @Body() String requestBody) {
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.GET_FNF_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.GET_FNF);
		logs.setTableType(LogsType.GetFnF);
		GetFnFDataRequestClient cclient = null;
		try {
			logger.info("Request lanaded in GetFNFDataLand :" + requestBody);
			try {
				cclient = Helper.JsonToObject(requestBody, GetFnFDataRequestClient.class);
				if (cclient != null) {
					if (cclient.getIsB2B() != null && cclient.getIsB2B().equals("true"))
						logs.setTransactionName(Transactions.GET_FNF_TRANSACTION_NAME_B2B);
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
				}
			} catch (JsonParseException ex1) {
				logger.error(Helper.GetException(ex1));
				GetFnFDataResponseClient resp = new GetFnFDataResponseClient();
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			} catch (JsonMappingException ex1) {
				GetFnFDataResponseClient resp = new GetFnFDataResponseClient();
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logger.error(Helper.GetException(ex1));
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			} catch (IOException ex1) {
				GetFnFDataResponseClient resp = new GetFnFDataResponseClient();
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logger.error(Helper.GetException(ex1));
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					GetFnFDataResponseClient resp = new GetFnFDataResponseClient();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					resp.setFnfLimit(getFNFLimit(cclient.getOfferingId()));
					logs.updateLog(logs);
					return resp;
				}
				String verification = Helper.validateRequest(cclient);
				if (!verification.equals("")) {
					GetFnFDataResponseClient resp = new GetFnFDataResponseClient();
					resp.setReturnCode(ResponseCodes.ERROR_400);
					resp.setReturnMsg(verification);
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					resp.setFnfLimit(getFNFLimit(cclient.getOfferingId()));
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					return RequestSoap(getRequestHeader(), cclient, logs);
				} else {
					GetFnFDataResponseClient resp = new GetFnFDataResponseClient();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					resp.setFnfLimit(getFNFLimit(cclient.getOfferingId()));
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		GetFnFDataResponseClient resp = new GetFnFDataResponseClient();
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(resp.getReturnCode());
		logs.setResponseDescription(resp.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		if (cclient != null)
			resp.setFnfLimit(getFNFLimit(cclient.getOfferingId()));
		return resp;
	}

	private String getFNFLimit(String offeringID) {
		String fnfNdOfferingId = ConfigurationManager.getConfigurationFromCache("fnf.offering.id.limit." + offeringID);
		if (fnfNdOfferingId != null && !fnfNdOfferingId.equals("")) {
			String[] token = fnfNdOfferingId.split(",");
			String fnf = token[0];
			return fnf;
		} else {
			return "2";
		}

	}

	public static GetFnFDataResponseClient RequestSoap(RequestHeader reqh, GetFnFDataRequestClient cclient, Logs logs) {
		try {
			GetFnFDataRequest cDR = new GetFnFDataRequest();
			cDR.setRequestHeader(reqh);
			GetFnFDataIn gCI = new GetFnFDataIn();
			gCI.setServiceNumber(cclient.getmsisdn());
			cDR.setGetFnFDataBody(gCI);
			GetFnFDataResponseClient response = new GetFnFDataResponseClient();
			String fnf = "2";
			String fnfNdOfferingId = ConfigurationManager
					.getConfigurationFromCache("fnf.offering.id.limit.1962950177" );//+ cclient.getOfferingId()
			if (fnfNdOfferingId != null && !fnfNdOfferingId.equals("")) {
				String[] token = fnfNdOfferingId.split(",");
				fnf = token[0];
			}
			else
			{
				fnf = Constants.FNF_OFFERING_ID;
			}
			response.setFnfLimit(fnf);
			try {
//				SubmitOrderResponse submitOrderResponse = AzerfonThirdPartyCalls.changeSupplimentaryOffers(cclient.getmsisdn(), fnfNdOfferingId.split(",")[1], "1");
//				if(submitOrderResponse.getResponseHeader().getRetCode().equals("0") || submitOrderResponse.getResponseHeader().getRetCode().equals("1221080076"))
//				{
//					GetFnFDataResponse responseFromBak = CRMServices.getInstance().getFnFData(cDR);
				GetFnFDataResponse responseFromBak = ThirdPartyCall.getFnFData(cDR);
					if (responseFromBak.getResponseHeader().getRetCode().equals("0")) 
					{
						for (FnFGroupInfo groupList : responseFromBak.getGetFnFDataBody().getGetFnFGroupListInfo()) 
						{
							for (FnFInfo fnfList : groupList.getFnFList().getGetFnFListInfo()) {
								try {
									response.getList()
											.add(new FNF(fnfList.getServiceNumber().replaceFirst("994", ""),
													new SimpleDateFormat(Constants.SQL_DATE_FORMAT_TRIMMED_Request)
															.format((new SimpleDateFormat(Constants.SQL_DATE_FORMAT_TRIMMED)
																	.parse(fnfList.getEffDate())))));
								} catch (Exception ex) {
									logger.error(Helper.GetException(ex));
								}
							}
						}
						
						response.setReturnCode(ResponseCodes.SUCESS_CODE_200);
						response.setReturnMsg(ResponseCodes.SUCESS_DES_200);
						logs.setResponseDescription(response.getReturnMsg());
						logs.setResponseCode(response.getReturnCode());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return response;
					} else {
						response.setReturnCode(responseFromBak.getResponseHeader().getRetCode());
						response.setReturnMsg(responseFromBak.getResponseHeader().getRetMsg());
						response.setFnfLimit(fnf);
						logs.setResponseDescription(response.getReturnMsg());
						logs.setResponseCode(response.getReturnCode());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return response;
					}
//				}
//				else
//				{
//					response.setReturnCode(submitOrderResponse.getResponseHeader().getRetCode());
//					response.setReturnMsg(submitOrderResponse.getResponseHeader().getRetMsg());
//					logs.setResponseDescription(response.getReturnMsg());
//					logs.setResponseCode(response.getReturnCode());
//					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
//					logs.updateLog(logs);
//					return response;
//				}
			} catch (Exception ee) {
				if (ee instanceof WebServiceException) {
					response.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
					response.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
					logs.setResponseDescription(response.getReturnMsg());
					logs.setResponseCode(response.getReturnCode());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				} else {
					response.setReturnCode(ResponseCodes.SUCESS_CODE_200);
					response.setReturnMsg(ResponseCodes.SUCESS_DES_200);
					logs.setResponseDescription(response.getReturnMsg());
					logs.setResponseCode(response.getReturnCode());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				}
			}
			logs.updateLog(logs);
			return response;
		} catch (Exception ee) {
			logger.error(Helper.GetException(ee));
		}
		return null;
	}

	private RequestHeader getRequestHeader() {
		RequestHeader reqh = new RequestHeader();
		reqh.setChannelId(ConfigurationManager.getConfigurationFromCache("crm.sub.ChannelId").trim());
		reqh.setTechnicalChannelId(ConfigurationManager.getConfigurationFromCache("crm.sub.TechnicalChannelId").trim());
		reqh.setAccessUser(ConfigurationManager.getConfigurationFromCache("crm.sub.AccessUser").trim());
		reqh.setTenantId(ConfigurationManager.getConfigurationFromCache("crm.sub.TenantId").trim());
		reqh.setAccessPwd(ConfigurationManager.getConfigurationFromCache("crm.sub.AccessPwd").trim());
		reqh.setTestFlag(ConfigurationManager.getConfigurationFromCache("crm.sub.TestFlag").trim());
		reqh.setLanguage(ConfigurationManager.getConfigurationFromCache("crm.sub.Language").trim());
		reqh.setTransactionId(Helper.generateTransactionID());
		return reqh;
	}
}
