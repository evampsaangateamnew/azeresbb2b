package com.evampsaanga.b2b.azerfon.changepassword;

import javax.xml.bind.annotation.XmlElement;

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;

//oldPassword":"1234","newPassword":"12345","confirmNewPassword":"12345"
public class ChangePasswordRequest extends BaseRequest {
	@XmlElement(name = "oldPassword", required = true)
	private String oldPassword = "";
	@XmlElement(name = "newPassword", required = true)
	private String newPassword = "";
	@XmlElement(name = "confirmNewPassword", required = true)
	private String confirmNewPassword = "";

	/**
	 * @return the oldPassword
	 */
	public String getOldPassword() {
		return oldPassword;
	}

	/**
	 * @param oldPassword
	 *            the oldPassword to set
	 */
	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	/**
	 * @return the newPassword
	 */
	public String getNewPassword() {
		return newPassword;
	}

	/**
	 * @param newPassword
	 *            the newPassword to set
	 */
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	/**
	 * @return the confirmNewPassword
	 */
	public String getConfirmNewPassword() {
		return confirmNewPassword;
	}

	/**
	 * @param confirmNewPassword
	 *            the confirmNewPassword to set
	 */
	public void setConfirmNewPassword(String confirmNewPassword) {
		this.confirmNewPassword = confirmNewPassword;
	}
}
