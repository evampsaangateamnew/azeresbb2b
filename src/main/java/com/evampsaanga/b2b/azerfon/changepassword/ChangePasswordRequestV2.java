package com.evampsaanga.b2b.azerfon.changepassword;

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;
/**
 * Change Password Request Container
 * @author Aqeel Abbas
 *
 */
//oldPassword":"1234","newPassword":"12345","confirmNewPassword":"12345"
public class ChangePasswordRequestV2 extends BaseRequest {
	private String userName;
	private String oldPassword;
	private String newPassword;
	private String confirmNewPassword;

	

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getConfirmNewPassword() {
		return confirmNewPassword;
	}

	public void setConfirmNewPassword(String confirmNewPassword) {
		this.confirmNewPassword = confirmNewPassword;
	}

	@Override
	public String toString() {
		return "ChangePasswordRequestV2 [username=" + userName + ", oldPassword=" + oldPassword + ", newPassword="
				+ newPassword + ", confirmNewPassword=" + confirmNewPassword + "]";
	}

}
