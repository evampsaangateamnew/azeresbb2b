package com.evampsaanga.b2b.azerfon.signupresendotp;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.evampsaanga.b2b.azerfon.getcustomerrequest.CustomerData;
import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "SignUpRequest")
public class SignUpResponse extends BaseResponse {
	@XmlElement(name = "pinMsg", required = true)
	private String pinMsg = "";

	/**
	 * @return the pinMsg
	 */
	public String getPinMsg() {
		return pinMsg;
	}

	/**
	 * @param pinMsg
	 *            the pinMsg to set
	 */
	public void setPinMsg(String pinMsg) {
		this.pinMsg = pinMsg;
	}

	CustomerData customerData = new CustomerData();

	/**
	 * @return the customerData
	 */
	public CustomerData getCustomerData() {
		return customerData;
	}

	/**
	 * @param customerData
	 *            the customerData to set
	 */
	public void setCustomerData(CustomerData customerData) {
		this.customerData = customerData;
	}
}
