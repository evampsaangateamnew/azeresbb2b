package com.evampsaanga.b2b.azerfon.changesuplimentry;

public class RequestBulkData {
	private String msisdn;
	private String groupType;

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getGroupType() {
		return groupType;
	}

	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}

	@Override
	public String toString() {
		return "RequestBulkData [msisdn=" + msisdn + ", groupType=" + groupType + "]";
	}

	
	
}
