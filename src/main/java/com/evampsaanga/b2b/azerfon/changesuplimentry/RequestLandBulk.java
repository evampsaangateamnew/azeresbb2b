package com.evampsaanga.b2b.azerfon.changesuplimentry;

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;

public class RequestLandBulk extends BaseRequest {
	private String actionType;
	private String actPrice;
	private String offeringId;
	private String groupType;
	
	public String getGroupType() {
		return groupType;
	}

	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}

	public String getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(String offeringId) {
		this.offeringId = offeringId;
	}

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public String getActPrice() {
		return actPrice;
	}

	public void setActPrice(String actPrice) {
		this.actPrice = actPrice;
	}

	@Override
	public String toString() {
		return "RequestLandBulk [actionType=" + actionType + ", actPrice=" + actPrice + ", offeringId=" + offeringId
				+ ", groupType=" + groupType + "]";
	}

}
