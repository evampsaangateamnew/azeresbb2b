package com.evampsaanga.b2b.azerfon.changesuplimentry;

import java.util.ArrayList;

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;

public class RequestBulk extends BaseRequest {
	private String actionType;
	private String type;
	private ArrayList<RequestBulkData> users;
	private String orderKey;
	private String offeringId;
	private String actPrice;

	public String getOrderKey() {
		return orderKey;
	}

	public void setOrderKey(String orderKey) {
		this.orderKey = orderKey;
	}

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public ArrayList<RequestBulkData> getUsers() {
		return users;
	}

	public void setUsers(ArrayList<RequestBulkData> users) {
		this.users = users;
	}

	public String getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(String offeringId) {
		this.offeringId = offeringId;
	}

	public String getActPrice() {
		return actPrice;
	}

	public void setActPrice(String actPrice) {
		this.actPrice = actPrice;
	}

	@Override
	public String toString() {
		return "RequestBulk [actionType=" + actionType + ", type=" + type + ", users=" + users + ", orderKey="
				+ orderKey + ", offeringId=" + offeringId + ", actPrice=" + actPrice + ", getOrderKey()="
				+ getOrderKey() + ", getActionType()=" + getActionType() + ", getType()=" + getType() + ", getUsers()="
				+ getUsers() + ", getOfferingId()=" + getOfferingId() + ", getActPrice()=" + getActPrice() + "]";
	}
	
}
