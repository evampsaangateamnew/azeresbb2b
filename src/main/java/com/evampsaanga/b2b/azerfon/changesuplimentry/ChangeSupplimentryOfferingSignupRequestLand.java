package com.evampsaanga.b2b.azerfon.changesuplimentry;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.amqimplementationsesb.LogsType;
import com.evampsaanga.b2b.azerfon.hlrweb.getquerybalanceRequest.HLRBalanceServices;
import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.ThirdPartyNames;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Decrypter;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.evampsaanga.b2b.developer.utils.SOAPLoggingHandler;
import com.evampsaanga.b2b.gethomepage.Balance;
import com.huawei.bss.soaif._interface.common.ReqHeader;
import com.huawei.bss.soaif._interface.ussdgateway.USSDGWChngSOReqMsg;
import com.huawei.bss.soaif._interface.ussdgateway.USSDGWChngSORspMsg;
import com.huawei.crm.basetype.ens.OfferingInfo;
import com.huawei.crm.basetype.ens.OfferingKey;
import com.huawei.crm.basetype.ens.OrderInfo;
import com.huawei.crm.basetype.ens.OrderItemInfo;
import com.huawei.crm.basetype.ens.OrderItemValue;
import com.huawei.crm.basetype.ens.OrderItems;
import com.huawei.crm.basetype.ens.SubscriberInfo;
import com.huawei.crm.service.ens.SubmitOrderRequest;
import com.huawei.crm.service.ens.SubmitOrderResponse;
import com.huawei.crm.service.ens.SubmitRequestBody;
import com.ngbss.evampsaanga.services.ThirdPartyCall;
import com.ngbss.evampsaanga.services.ThirdPartyRequestHeader;

@Path("/azerfon")
public class ChangeSupplimentryOfferingSignupRequestLand {
	public static final Logger logger = Logger.getLogger("azerfon-esb");

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response Get(@Header("credentials") String credential, @Header("Content-Type") String contentType,
			@Body() String requestBody) {
		Response resp = new Response();
//		resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
//		resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
//		return resp;
		SubmitOrderResponse response = new SubmitOrderResponse();
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.CHANGE_SUPPLIMENTRY_OFFERING_TRANSACTION_NAME_SIGNUP);
		logs.setThirdPartyName(ThirdPartyNames.CHANGE_SUPPLIMENTRY_OFFERING);
		logs.setTableType(LogsType.ChangeSupplimentryOffering);
		try {
			logger.info("Request Landed on ChangeSupplimentryOfferingRequestLand Land:" + requestBody);
			Request cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, Request.class);

				if (cclient != null) {
//					if(cclient.getIsB2B()!=null && cclient.getIsB2B().equals("true"))
//						logs.setTransactionName(Transactions.CHANGE_SUPPLIMENTRY_OFFERING_TRANSACTION_NAME_SIGNUP_B2B);
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
					logs.setLang(cclient.getLang());
					logs.setActivatedOfferId(cclient.getOfferingId());
				}

			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					int actionType = -1;
					try {
						actionType = Integer.parseInt(cclient.getActionType());
						if (actionType != 1 && actionType != 3) {
							resp.setReturnCode(ResponseCodes.MISSING_PARAMETER_CODE);
							resp.setReturnMsg(ResponseCodes.MISSING_PARAMETER_DES);
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} catch (Exception ex) {
						resp.setReturnCode(ResponseCodes.MISSING_PARAMETER_CODE);
						resp.setReturnMsg(ResponseCodes.MISSING_PARAMETER_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
					USSDGWChngSORspMsg respone = null;
					
					if(!cclient.getOfferingId().equals(null))
					{
						
						
						response = changeSupplementaryOffers(cclient.getActionType(), cclient.getOfferingId(), cclient.getmsisdn());
						
						
					}
					else
					{
						resp.setReturnCode(ResponseCodes.MISSING_PARAMETER_CODE);
						resp.setReturnMsg(ResponseCodes.MISSING_PARAMETER_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
			
					if (response.getResponseHeader().getRetCode().equals("0")) 
					{
						resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
						resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					} else {
						resp.setReturnCode(response.getResponseHeader().getRetCode());
						resp.setReturnMsg(response.getResponseHeader().getRetMsg());
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				} else {
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(resp.getReturnCode());
		logs.setResponseDescription(resp.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}


	// funtion for Phase 2
	public Response changeSupplementaryV2(RequestLandBulk request) {
		Response resp = new Response();
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.CHANGE_SUPPLIMENTRY_OFFERING_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.CHANGE_SUPPLIMENTRY_OFFERING);
		logs.setTableType(LogsType.ChangeSupplimentryOffering);
		try 
		{
			String trnsactionName = Transactions.BASE_MAGENTO_TRANSACTION_NAME + " " + Transactions.LOGIN_TRANSACTION_NAME;
			String token = Helper.retrieveToken(trnsactionName, Helper.getValueFromJSON(Helper.ObjectToJson(request), "msisdn"));
			logger.info("MSISDN:" + request.getmsisdn()
					+ " Request Landed on ChangeSupplimentryOfferingRequestLand BUlk:" + request.toString());

			try {
				if (request != null && !request.getmsisdn().equals("")) {
					logs.setIp(request.getiP());
					logs.setChannel(request.getChannel());
					logs.setMsisdn(request.getmsisdn());
					logs.setLang(request.getLang());
					logs.setActivatedOfferId(request.getOfferingId());
				}

			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (request != null) {
				logs.setIp(request.getiP());
				logs.setChannel(request.getChannel());
				logs.setMsisdn(request.getmsisdn());

				if (!request.getmsisdn().equals("")) {
					int actionType = -1;
					try {
						actionType = Integer.parseInt(request.getActionType());
						if (actionType != 1 && actionType != 3) {
							resp.setReturnCode(ResponseCodes.MISSING_PARAMETER_CODE);
							resp.setReturnMsg(ResponseCodes.MISSING_PARAMETER_DES);
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} catch (Exception ex) {
						resp.setReturnCode(ResponseCodes.MISSING_PARAMETER_CODE);
						resp.setReturnMsg(ResponseCodes.MISSING_PARAMETER_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
					try {
						HLRBalanceServices service = new HLRBalanceServices();
						if (request.getGroupType().equalsIgnoreCase("PaybySubs")) {
							USSDGWChngSORspMsg respone = ussdChangeSuplimentryOfferings(actionType, request.getmsisdn(),
									request.getOfferingId());
							if (respone.getRspHeader().getReturnCode().equals("0000")) {
								resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
								resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
								logs.setResponseCode(resp.getReturnCode());
								logs.setResponseDescription(resp.getReturnMsg());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
								return resp;
							} else {
								resp.setReturnCode(respone.getRspHeader().getReturnCode());
								resp.setReturnMsg(respone.getRspHeader().getReturnMsg());
								logs.setResponseCode(resp.getReturnCode());
								logs.setResponseDescription(resp.getReturnMsg());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
								return resp;
							}
						} else if(request.getGroupType().equalsIgnoreCase("PartPay")) {
							Balance responseFromBakcell = service.getBalance(request.getmsisdn(), "postpaid",null,token);
							Helper.logInfoMessageV2(request.getmsisdn() + " - Balance is" +  responseFromBakcell.getPostpaid().getAvailableBalanceIndividualValue());
							Helper.logInfoMessageV2(request.getmsisdn() + " - Balance in double is " +Double.parseDouble(responseFromBakcell.getPostpaid().getAvailableBalanceIndividualValue()));
							Helper.logInfoMessageV2(request.getmsisdn() + " - Balance is" +  responseFromBakcell.getPostpaid().getAvailableBalanceCorporateValue());
							Helper.logInfoMessageV2(request.getmsisdn() + " - Balance in double is " +Double.parseDouble(responseFromBakcell.getPostpaid().getAvailableBalanceCorporateValue()));
							Helper.logInfoMessageV2(request.getmsisdn() + " - Activation Price from request is: " + request.getActPrice());
							if (responseFromBakcell.getReturnCode().equals("0")) {
								if (Double.parseDouble(responseFromBakcell.getPostpaid()
										.getAvailableBalanceIndividualValue()) + Double.parseDouble(responseFromBakcell.getPostpaid()
										.getAvailableBalanceCorporateValue()) >= Double
												.parseDouble(request.getActPrice())) {
									USSDGWChngSORspMsg respone = ussdChangeSuplimentryOfferings(actionType,
											request.getmsisdn(), request.getOfferingId());
									if (respone.getRspHeader().getReturnCode().equals("0000")) {
										resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
										resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
										logs.setResponseCode(resp.getReturnCode());
										logs.setResponseDescription(resp.getReturnMsg());
										logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
										logs.updateLog(logs);
										return resp;
									} else {
										resp.setReturnCode(respone.getRspHeader().getReturnCode());
										resp.setReturnMsg(respone.getRspHeader().getReturnMsg());
										logs.setResponseCode(resp.getReturnCode());
										logs.setResponseDescription(resp.getReturnMsg());
										logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
										logs.updateLog(logs);
										return resp;
									}
								} else {
									resp.setReturnCode(ResponseCodes.UNSUCCESS_CODE);
									resp.setReturnMsg(ResponseCodes.UNSUCCESS_DESC_CS);
									logs.setResponseCode(resp.getReturnCode());
									logs.setResponseDescription(resp.getReturnMsg());
									logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
									logs.updateLog(logs);
									return resp;
								}

							}
						}
						else {
							Balance responseFromBakcell = service.getBalance(request.getmsisdn(), "postpaid",null,token);
							Helper.logInfoMessageV2(request.getmsisdn() + " - Balance is" +  responseFromBakcell.getPostpaid().getAvailableBalanceIndividualValue());
							Helper.logInfoMessageV2(request.getmsisdn() + " - Balance in double is " +Double.parseDouble(responseFromBakcell.getPostpaid().getAvailableBalanceIndividualValue()));
							Helper.logInfoMessageV2(request.getmsisdn() + " - Activation Price from request is: " + request.getActPrice());
							if (responseFromBakcell.getReturnCode().equals("0")) {
								if (Double.parseDouble(responseFromBakcell.getPostpaid()
										.getAvailableBalanceIndividualValue()) + Double.parseDouble(responseFromBakcell.getPostpaid()
										.getAvailableBalanceCorporateValue()) >= Double
												.parseDouble(request.getActPrice())) {
									USSDGWChngSORspMsg respone = ussdChangeSuplimentryOfferings(actionType,
											request.getmsisdn(), request.getOfferingId());
									if (respone.getRspHeader().getReturnCode().equals("0000")) {
										resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
										resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
										logs.setResponseCode(resp.getReturnCode());
										logs.setResponseDescription(resp.getReturnMsg());
										logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
										logs.updateLog(logs);
										return resp;
									} else {
										resp.setReturnCode(respone.getRspHeader().getReturnCode());
										resp.setReturnMsg(respone.getRspHeader().getReturnMsg());
										logs.setResponseCode(resp.getReturnCode());
										logs.setResponseDescription(resp.getReturnMsg());
										logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
										logs.updateLog(logs);
										return resp;
									}
								} else {
									resp.setReturnCode(ResponseCodes.UNSUCCESS_CODE);
									resp.setReturnMsg(ResponseCodes.UNSUCCESS_DESC_CS);
									logs.setResponseCode(resp.getReturnCode());
									logs.setResponseDescription(resp.getReturnMsg());
									logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
									logs.updateLog(logs);
									return resp;
								}

							}
						}
					} catch (Exception e) {
						logger.error(Helper.GetException(e));
						resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
						resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}

				} else {
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
			resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
			resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}

		return resp;
	}

	public USSDGWChngSORspMsg ussdChangeSuplimentryOfferings(int action, String msisdn, String offeringUnqiqueId) {
		USSDGWChngSOReqMsg req = new USSDGWChngSOReqMsg();
		req.setReqHeader(getRequestHeader());
		req.setServiceNumber(msisdn);
		req.setIsConfirmed(Constants.USSD_IS_CONFIRMED);
		USSDGWChngSOReqMsg.SupplementaryOfferingList list = new USSDGWChngSOReqMsg.SupplementaryOfferingList();
		list.setActionType(Integer.toString(action));
		USSDGWChngSOReqMsg.SupplementaryOfferingList.OfferingId offeringId = new USSDGWChngSOReqMsg.SupplementaryOfferingList.OfferingId();
		offeringId.setOfferingId(offeringUnqiqueId);
		list.setOfferingId(offeringId);
		req.getSupplementaryOfferingList().add(list);
//		USSDGWChngSORspMsg respone = USSDService.getInstance().ussdGatewayChangeSupplementaryOffering(req);
		USSDGWChngSORspMsg respone = ThirdPartyCall.getUssdGatewayChangeSupplementaryOffering(req);
		return respone;
	}

	public ReqHeader getRequestHeader() {
		ReqHeader reqh = new ReqHeader();
		try {
			reqh.setAccessUser(ConfigurationManager.getConfigurationFromCache("ussd.reqh.AccessUser").trim());
			reqh.setChannelId(ConfigurationManager.getConfigurationFromCache("ussd.reqh.ChannelId").trim());
			reqh.setAccessPwd(ConfigurationManager.getConfigurationFromCache("ussd.reqh.AccessPwd").trim());
			reqh.setTransactionId(Helper.generateTransactionID());
			return reqh;
		} catch (Exception e) {
			logger.error(Helper.GetException(e));
		}
		return reqh;
	}
	
	public static SubmitOrderResponse changeSupplementaryOffers(String actionType, String offeringId,String msisdn) {
		SubmitOrderRequest submitOrderRequestMsgReq = new SubmitOrderRequest();
		  SubmitRequestBody submitrequestBody = new SubmitRequestBody();
		  OrderInfo orderInfo = new OrderInfo();
		  orderInfo.setOrderType(Constants.ORDER_TYPE);
		  submitrequestBody.setOrder(orderInfo);
		  OrderItems orderItems = new OrderItems();
		  OrderItemValue orderItem = new OrderItemValue();
		  OrderItemInfo orderItemInfo = new OrderItemInfo();
		  orderItemInfo.setOrderItemType(Constants.ORDER_ITEM_TYPE);
		  orderItemInfo.setIsCustomerNotification(Constants.CUSTOMER_NOTIFICATION);
		  orderItemInfo.setIsPartnerNotification(Constants.PARAMETER_NOTIFICATION);
		  orderItem.setOrderItemInfo(orderItemInfo);
		  SubscriberInfo subscriberInfo = new SubscriberInfo();
		  subscriberInfo.setServiceNumber(msisdn);
		  
		  String offeringIDs[] = offeringId.split(",");
		  for(int i=0 ;i<offeringIDs.length;i++)
		  {
		   OfferingInfo offeringInfo = new OfferingInfo();
		   offeringInfo.setActionType(actionType);
		   OfferingKey offeringID = new OfferingKey();
		   offeringID.setOfferingId(offeringIDs[i]);
		   offeringInfo.setOfferingId(offeringID);
		   offeringInfo.setEffectMode(Constants.EFFECTIVE_MODE);
		   offeringInfo.setActiveMode(Constants.ACTION_MODE);
		   subscriberInfo.getSupplementaryOfferingList().getOfferingInfo().add(offeringInfo);
		  }
		    
		  orderItem.setSubscriber(subscriberInfo);
		  orderItems.getOrderItem().add(orderItem);
		  submitrequestBody.setOrderItems(orderItems);
		  submitOrderRequestMsgReq.setSubmitRequestBody(submitrequestBody);
//		  submitOrderRequestMsgReq.setRequestHeader(OrderHandleService.getReqHeaderForCallForwarding());
//		  return OrderHandleService.getInstance().submitOrder(submitOrderRequestMsgReq);
		  submitOrderRequestMsgReq.setRequestHeader(ThirdPartyRequestHeader.getReqHeaderForCallForwarding());
		  return ThirdPartyCall.submitOrder(submitOrderRequestMsgReq);
	}
}
