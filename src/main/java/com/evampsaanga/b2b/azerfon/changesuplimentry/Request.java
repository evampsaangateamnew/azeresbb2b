package com.evampsaanga.b2b.azerfon.changesuplimentry;

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;

public class Request extends BaseRequest {
	// 1 for add 3 for delete
	private String offeringId = "";

	/**
	 * @return the offeringId
	 */
	public String getOfferingId() {
		return offeringId;
	}

	/**
	 * @param offeringId
	 *            the offeringId to set
	 */
	public void setOfferingId(String offeringId) {
		this.offeringId = offeringId;
	}

	private String actionType = "";

	/**
	 * @return the actionType
	 */
	public String getActionType() {
		return actionType;
	}

	/**
	 * @param actionType
	 *            the actionType to set
	 */
	public void setActionType(String actionType) {
		this.actionType = actionType;
	}
}
