package com.evampsaanga.b2b.azerfon.changesuplimentry;

import java.io.IOException;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.amqimplementationsesb.LogsType;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.ThirdPartyNames;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Decrypter;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.evampsaanga.b2b.developer.utils.SOAPLoggingHandler;
import com.huawei.crm.basetype.ens.OfferingInfo;
import com.huawei.crm.basetype.ens.OfferingKey;
import com.huawei.crm.basetype.ens.OrderInfo;
import com.huawei.crm.basetype.ens.OrderItemInfo;
import com.huawei.crm.basetype.ens.OrderItemValue;
import com.huawei.crm.basetype.ens.OrderItems;
import com.huawei.crm.basetype.ens.SubscriberInfo;
import com.huawei.crm.service.ens.SubmitOrderRequest;
import com.huawei.crm.service.ens.SubmitOrderResponse;
import com.huawei.crm.service.ens.SubmitRequestBody;
import com.ngbss.evampsaanga.services.ThirdPartyCall;
import com.ngbss.evampsaanga.services.ThirdPartyRequestHeader;

@Path("/azerfon")
public class ChangeSupplimentryOfferingRequestLand {
	public static final Logger logger = Logger.getLogger("azerfon-esb");

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response Get(@Header("credentials") String credential, @Header("Content-Type") String contentType,
			@Body() String requestBody) {
		Response resp = new Response();
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.CHANGE_SUPPLIMENTRY_OFFERING_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.CHANGE_SUPPLIMENTRY_OFFERING);
		logs.setTableType(LogsType.ChangeSupplimentryOffering);

		String token = "";
		String TrnsactionName = Transactions.CHANGE_SUPPLIMENTRY_OFFERING_TRANSACTION_NAME;

		try {

			token = Helper.retrieveToken(TrnsactionName, Helper.getValueFromJSON(requestBody, "msisdn"));

			logger.info(token+"Request Landed on ChangeSupplimentryOfferingRequestLand Land:" + requestBody);
			Request cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, Request.class);

				if (cclient != null) {
					if (cclient.getIsB2B() != null && cclient.getIsB2B().equals("true"))
						logs.setTransactionName(Transactions.CHANGE_SUPPLIMENTRY_OFFERING_TRANSACTION_NAME_B2B);
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
					logs.setLang(cclient.getLang());
					logs.setActivatedOfferId(cclient.getOfferingId());
				}

			} catch (Exception ex) {
				logger.info(token+Helper.GetException(ex));
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.info(token+Helper.GetException(ex));
				}
				if (credentials == null) {
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					int actionType = -1;
					try {
						actionType = Integer.parseInt(cclient.getActionType());
						if (actionType != 1 && actionType != 3) {
							resp.setReturnCode(ResponseCodes.MISSING_PARAMETER_CODE);
							resp.setReturnMsg(ResponseCodes.MISSING_PARAMETER_DES);
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} catch (Exception ex) {
						resp.setReturnCode(ResponseCodes.MISSING_PARAMETER_CODE);
						resp.setReturnMsg(ResponseCodes.MISSING_PARAMETER_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
					SubmitOrderResponse respone = changeSupplimentaryOffers(cclient.getmsisdn(),
							cclient.getOfferingId(),cclient.getActionType(),token);
					if (respone.getResponseHeader().getRetCode().equals("0")) {

						
						resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
						resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						logger.info(token+"Response from ESB: "+Helper.ObjectToJson(resp));
						return resp;
					} else {
						resp.setReturnCode(respone.getResponseHeader().getRetCode());
						resp.setReturnMsg(respone.getResponseHeader().getRetMsg());
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				} else {
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.info(token+Helper.GetException(ex));
		}
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(resp.getReturnCode());
		logs.setResponseDescription(resp.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	
	private static  SubmitOrderResponse changeSupplimentaryOffers(String msisdn, String offeringid,String actionType,String token) {
		// TODO Auto-generated method stub
		logger.info(token + "-Request Received In changeSupplimentaryOffers Azerfon Call-" + "Data-"
				+ msisdn +": "+offeringid +" : " +actionType);
		SubmitOrderRequest submitOrderRequestMsgReq = new SubmitOrderRequest();
		SubmitRequestBody submitrequestBody = new SubmitRequestBody();
		OrderInfo orderInfo = new OrderInfo();
		orderInfo.setOrderType("CO025");
		submitrequestBody.setOrder(orderInfo);
		OrderItems orderItems = new OrderItems();
		OrderItemValue orderItem = new OrderItemValue();
		OrderItemInfo orderItemInfo = new OrderItemInfo();
		orderItemInfo.setOrderItemType("CO025");
//		orderItemInfo.setIsCustomerNotification("0");
//		orderItemInfo.setIsPartnerNotification("0");
		orderItem.setOrderItemInfo(orderItemInfo);
		SubscriberInfo subscriberInfo = new SubscriberInfo();
		subscriberInfo.setServiceNumber(msisdn);
		OfferingInfo offeringInfo = new OfferingInfo();
		offeringInfo.setActionType(actionType);
		OfferingKey offeringID = new OfferingKey();
		offeringID.setOfferingId(offeringid);
		offeringInfo.setOfferingId(offeringID);
//		offeringInfo.setEffectMode("0");
//		offeringInfo.setActiveMode("A");
//		OfferingExtParameterList extParmList = new OfferingExtParameterList();
//		OfferingExtParameterInfo offertingExtparamInfo = new OfferingExtParameterInfo();
//		offertingExtparamInfo.setParamName("509703");
//		offertingExtparamInfo.setParamValue("776480535");
//		extParmList.getParameterInfo().add(offertingExtparamInfo);
//		offeringInfo.setExtParamList(extParmList);
		subscriberInfo.getSupplementaryOfferingList().getOfferingInfo().add(offeringInfo);
		orderItem.setSubscriber(subscriberInfo);
		orderItems.getOrderItem().add(orderItem);
		submitrequestBody.setOrderItems(orderItems);
		submitOrderRequestMsgReq.setSubmitRequestBody(submitrequestBody);
//		submitOrderRequestMsgReq.setRequestHeader(OrderHandleService.getReqHeaderForChangeTariff());
//		SubmitOrderResponse response = OrderHandleService.getInstance().submitOrder(submitOrderRequestMsgReq);
		submitOrderRequestMsgReq.setRequestHeader(ThirdPartyRequestHeader.getReqHeaderForChangeTariff());
		SubmitOrderResponse response = ThirdPartyCall.submitOrder(submitOrderRequestMsgReq);
		
		try {
			logger.info(token + "-Response from changeSupplimentaryOffers Azerfon Call-" + Helper.ObjectToJson(response));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.info(token+Helper.GetException(e));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.info(token+Helper.GetException(e));
		}
		return response;
	}
	

}
