package com.evampsaanga.b2b.azerfon.ussdgwquerybalance;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bss.soaif._interface.ussdgateway.QueryBalanceResponse;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "USSDGWQueryBalanceResponse", propOrder = { "responseBody" })
@XmlRootElement(name = "USSDGWQueryBalanceResponse")
public class USSDGWQueryBalanceResponse extends com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse {
	@XmlElement(name = "responseBody", required = true)
	private QueryBalanceResponse responseBody = null;

	public USSDGWQueryBalanceResponse(QueryBalanceResponse res) {
		super();
		this.responseBody = res;
	}

	public USSDGWQueryBalanceResponse() {
		super();
	}

	public QueryBalanceResponse getResponseBody() {
		return responseBody;
	}

	public void setResponseBody(QueryBalanceResponse responseBody) {
		this.responseBody = responseBody;
	}
}
