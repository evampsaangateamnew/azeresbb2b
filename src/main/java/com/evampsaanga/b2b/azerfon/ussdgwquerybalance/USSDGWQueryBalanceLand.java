package com.evampsaanga.b2b.azerfon.ussdgwquerybalance;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.NoSuchPaddingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.ws.WebServiceException;

import org.apache.camel.Header;

import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.developer.utils.Decrypter;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.evampsaanga.b2b.developer.utils.SOAPLoggingHandler;
import com.huawei.bss.soaif._interface.common.ReqHeader;
import com.huawei.bss.soaif._interface.ussdgateway.QueryBalanceIn;
import com.huawei.bss.soaif._interface.ussdgateway.QueryBalanceRequest;
import com.ngbss.evampsaanga.services.ThirdPartyCall;

@Path("/bakcell")
public class USSDGWQueryBalanceLand {
	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public USSDGWQueryBalanceResponse Get(@Header("credentials") String credential,
			USSDGWQueryBalanceRequestClient cclient) throws InvalidKeyException, NoSuchAlgorithmException,
			UnsupportedEncodingException, InvalidKeySpecException, NoSuchPaddingException {
		if (cclient != null) {
			{
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
					USSDGWQueryBalanceResponse resp = new USSDGWQueryBalanceResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
				}
				if (credentials == null) {
					USSDGWQueryBalanceResponse resp = new USSDGWQueryBalanceResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					return resp;
				}
				String verification = Helper.validateRequest(cclient);
				if (!verification.equals("")) {
					USSDGWQueryBalanceResponse resp = new USSDGWQueryBalanceResponse();
					resp.setReturnCode(ResponseCodes.ERROR_400);
					resp.setReturnMsg(verification);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					return callSoap(getRequestHeader(), cclient);
				} else {
					USSDGWQueryBalanceResponse resp = new USSDGWQueryBalanceResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					return resp;
				}
			}
		}
		return null;
	}

	public USSDGWQueryBalanceResponse callSoap(ReqHeader reqH, USSDGWQueryBalanceRequestClient cclient) {
		USSDGWQueryBalanceResponse resp = null;
		try {
			QueryBalanceRequest req = new QueryBalanceRequest();
			req.setReqHeader(reqH);
			QueryBalanceIn qBBody = new QueryBalanceIn();
			qBBody.setPrimaryIdentity(Constants.AZERI_COUNTRY_CODE + cclient.getmsisdn());
			req.setQueryBalanceBody(qBBody);
//			resp = new USSDGWQueryBalanceResponse(USSDService.getInstance().ussdGatewayQueryBalance(req));
			resp = new USSDGWQueryBalanceResponse(ThirdPartyCall.getUssdGatewayQueryBalance(req));
			resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
			resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
			return resp;
		} catch (Exception ex) {
			resp = new USSDGWQueryBalanceResponse();
			if (ex instanceof WebServiceException) {
				resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
				resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
			} else {
				resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
				resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
			}
		}
		return resp;
	}

	public ReqHeader getRequestHeader() {
		ReqHeader reqh = new ReqHeader();
		reqh = new ReqHeader();
		reqh.setAccessUser(ConfigurationManager.getConfigurationFromCache("ussd.reqh.AccessUser").trim());
		reqh.setChannelId(ConfigurationManager.getConfigurationFromCache("ussd.reqh.ChannelId").trim());
		reqh.setAccessPwd(ConfigurationManager.getConfigurationFromCache("ussd.reqh.AccessPwd").trim());
		reqh.setTransactionId(Helper.generateTransactionID());
		return reqh;
	}
}
