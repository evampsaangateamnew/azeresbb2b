package com.evampsaanga.b2b.azerfon.verifypassport;

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;

public class VerifyPassportRequest extends BaseRequest {
	String passPortNumber = "";

	/**
	 * @return the passPortNumber
	 */
	public String getPassPortNumber() {
		return passPortNumber;
	}

	/**
	 * @param passPortNumber
	 *            the passPortNumber to set
	 */
	public void setPassPortNumber(String passPortNumber) {
		this.passPortNumber = passPortNumber;
	}
}
