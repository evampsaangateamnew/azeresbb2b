package com.evampsaanga.b2b.azerfon.crmsubscriber;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.amqimplementationsesb.LogsType;
import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.ThirdPartyNames;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Decrypter;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.evampsaanga.b2b.developer.utils.SOAPLoggingHandler;
import com.huawei.crm.azerfon.bss.query.GetSubscriberResponse;
import com.huawei.crm.basetype.RequestHeader;

@Path("/bakcell/")
public class CrmSubscriberLand {
	public static final Logger logger = Logger.getLogger("azerfon-esb");

	public static RequestHeader getRequestHeaderForCRM() {
		RequestHeader reqhForCRM = new RequestHeader();
		reqhForCRM.setChannelId(ConfigurationManager.getConfigurationFromCache("crm.sub.ChannelId").trim());
		reqhForCRM.setTechnicalChannelId(ConfigurationManager.getConfigurationFromCache("crm.sub.TechnicalChannelId").trim());
		reqhForCRM.setAccessUser(ConfigurationManager.getConfigurationFromCache("crm.sub.AccessUser").trim());
		reqhForCRM.setTenantId(ConfigurationManager.getConfigurationFromCache("crm.sub.TenantId").trim());
		reqhForCRM.setAccessPwd(ConfigurationManager.getConfigurationFromCache("crm.sub.AccessPwd").trim());
		reqhForCRM.setTestFlag(ConfigurationManager.getConfigurationFromCache("crm.sub.TestFlag").trim());
		reqhForCRM.setLanguage(ConfigurationManager.getConfigurationFromCache("crm.sub.Language").trim());
		reqhForCRM.setTransactionId(Helper.generateTransactionID());
		return reqhForCRM;
	}

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public CrmSubscriberResponse Get(@Header("credentials") String credential,
			@Header("Content-Type") String contentType, @Body() String requestBody) {
		CrmSubscriberResponse resp = new CrmSubscriberResponse();
		Logs logs = new Logs();
		try {
			logs.setTransactionName(Transactions.CUSTOMER_DATA_TRANSACTION_NAME);
			logs.setThirdPartyName(ThirdPartyNames.CUSTOMER_DATA);
			logs.setTableType(LogsType.CustomerData);
			logger.info("Request Landed on CrmSubscriberResponse:" + requestBody);
			CrmSubscriberRequest cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, CrmSubscriberRequest.class);
				if (cclient != null) {
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
				}
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					try {
						GetSubscriberResponse respns = new com.evampsaanga.b2b.azerfon.getsubscriber.CRMSubscriberService()
								.GetSubscriberRequest(cclient.getmsisdn());
						if (respns.getResponseHeader().getRetCode().equals("0")) {
							resp = new CrmSubscriberResponse(respns.getGetSubscriberBody().getPIN1(),
									respns.getGetSubscriberBody().getPIN2(), respns.getGetSubscriberBody().getPUK1(),
									respns.getGetSubscriberBody().getPUK2(),
									respns.getGetSubscriberBody().getWrittenLanguage());
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
							logs.updateLog(logs);			return resp;
						}
						resp.setReturnCode(respns.getResponseHeader().getRetCode());
						resp.setReturnMsg(respns.getResponseHeader().getRetMsg());
						logs.updateLog(logs);		return resp;
					} catch (Exception ex) {
						logger.error(Helper.GetException(ex));
					}
				} else {
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(resp.getReturnCode());
		logs.setResponseDescription(resp.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());logs.updateLog(logs);
		return resp;
	}
}
