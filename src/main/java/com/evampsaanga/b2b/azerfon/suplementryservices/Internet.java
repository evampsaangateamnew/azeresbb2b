package com.evampsaanga.b2b.azerfon.suplementryservices;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Internet {
	@JsonProperty("offers")
	private InternetOffers[] offers;
	@JsonProperty("filters")
	private InternetFilters filters = null;
}