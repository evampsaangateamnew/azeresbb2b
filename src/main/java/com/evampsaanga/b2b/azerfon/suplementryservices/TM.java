package com.evampsaanga.b2b.azerfon.suplementryservices;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TM {
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("offers")
	private ArrayList<Offers> offers=null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("filters")
	private InternetFilters filters = null;
	
	@JsonProperty("offers")
	public ArrayList<Offers> getOffers() {
		return offers;
	}
	@JsonProperty("offers")
	public void setOffers(ArrayList<Offers> offers) {
		this.offers = offers;
	}

	
	
	
}