package com.evampsaanga.b2b.azerfon.suplementryservices;

public class Desktop {
	private String value;
	private String key;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
}