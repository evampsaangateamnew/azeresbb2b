package com.evampsaanga.b2b.azerfon.suplementryservices;

public class TextTitle {
	private String textWithTitleLabel;
	private String textWithTitleDescription;
	private String fragmentIcon;
	
	   public String getFragmentIcon() {
			return fragmentIcon;
		}

		public void setFragmentIcon(String fragmentIcon) {
			this.fragmentIcon = fragmentIcon;
		}


	public String getTextWithTitleLabel() {
		return textWithTitleLabel;
	}

	public void setTextWithTitleLabel(String textWithTitleLabel) {
		this.textWithTitleLabel = textWithTitleLabel;
	}

	public String getTextWithTitleDescription() {
		return textWithTitleDescription;
	}

	public void setTextWithTitleDescription(String textWithTitleDescription) {
		this.textWithTitleDescription = textWithTitleDescription;
	}

	@Override
	public String toString() {
		return "ClassPojo [textWithTitleLabel = " + textWithTitleLabel + ", textWithTitleDescription = "
				+ textWithTitleDescription + "]";
	}
}