package com.evampsaanga.b2b.azerfon.suplementryservices;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Campaign {
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("offers")
	private Offers[] offers;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("filters")
	private InternetFilters filters = null;

	public Offers[] getOffers() {
		return offers;
	}

	public void setOffers(Offers[] offers) {
		this.offers = offers;
	}
}