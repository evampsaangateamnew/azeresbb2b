package com.evampsaanga.b2b.azerfon.suplementryservices;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class InternetOffers {
	@JsonProperty("details")
	private InternetOffersDetails details = null;
	@JsonProperty("description")
	private InternetOffersDescription description = null;
	private Header header = null;

	public Header getHeader() {
		return header;
	}

	public void setHeader(Header header) {
		this.header = header;
	}
}