package com.evampsaanga.b2b.azerfon.suplementryservices;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.ALWAYS)
@JsonIgnoreProperties(ignoreUnknown = true)
public class InternetFilters {
	@JsonInclude(JsonInclude.Include.ALWAYS)
	private App[] app;
	@JsonInclude(JsonInclude.Include.ALWAYS)
	private Tab[] tab;
	@JsonInclude(JsonInclude.Include.ALWAYS)
	private Desktop[] desktop;

	public App[] getApp() {
		return app;
	}

	public void setApp(App[] app) {
		this.app = app;
	}

	public Tab[] getTab() {
		return tab;
	}

	public void setTab(Tab[] tab) {
		this.tab = tab;
	}

	public Desktop[] getDesktop() {
		return desktop;
	}

	public void setDesktop(Desktop[] desktop) {
		this.desktop = desktop;
	}
}