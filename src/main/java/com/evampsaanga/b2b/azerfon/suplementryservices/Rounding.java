package com.evampsaanga.b2b.azerfon.suplementryservices;

public class Rounding {
	private String internetRoundingLabel;
	private String destinationRoundingIcon;
	private String offnetRoundingLabel;
	private String destinationRoundingLabel;
	private String internetRoundingValue;
	private String onnetRoundingLabel;
	private String internationalRoundingLabel;
	private String onnetRoundingValue;
	private String destinationRoundingValue;
	private String descriptionRounding;
	private String internationalRoundingValue;
	private String offnetRoundingValue;
	private String fragmentIcon;
	
	   public String getFragmentIcon() {
			return fragmentIcon;
		}

		public void setFragmentIcon(String fragmentIcon) {
			this.fragmentIcon = fragmentIcon;
		}


	public String getInternetRoundingLabel() {
		return internetRoundingLabel;
	}

	public void setInternetRoundingLabel(String internetRoundingLabel) {
		this.internetRoundingLabel = internetRoundingLabel;
	}

	public String getDestinationRoundingIcon() {
		return destinationRoundingIcon;
	}

	public void setDestinationRoundingIcon(String destinationRoundingIcon) {
		this.destinationRoundingIcon = destinationRoundingIcon;
	}

	public String getOffnetRoundingLabel() {
		return offnetRoundingLabel;
	}

	public void setOffnetRoundingLabel(String offnetRoundingLabel) {
		this.offnetRoundingLabel = offnetRoundingLabel;
	}

	public String getDestinationRoundingLabel() {
		return destinationRoundingLabel;
	}

	public void setDestinationRoundingLabel(String destinationRoundingLabel) {
		this.destinationRoundingLabel = destinationRoundingLabel;
	}

	public String getInternetRoundingValue() {
		return internetRoundingValue;
	}

	public void setInternetRoundingValue(String internetRoundingValue) {
		this.internetRoundingValue = internetRoundingValue;
	}

	public String getOnnetRoundingLabel() {
		return onnetRoundingLabel;
	}

	public void setOnnetRoundingLabel(String onnetRoundingLabel) {
		this.onnetRoundingLabel = onnetRoundingLabel;
	}

	public String getInternationalRoundingLabel() {
		return internationalRoundingLabel;
	}

	public void setInternationalRoundingLabel(String internationalRoundingLabel) {
		this.internationalRoundingLabel = internationalRoundingLabel;
	}

	public String getOnnetRoundingValue() {
		return onnetRoundingValue;
	}

	public void setOnnetRoundingValue(String onnetRoundingValue) {
		this.onnetRoundingValue = onnetRoundingValue;
	}

	public String getDestinationRoundingValue() {
		return destinationRoundingValue;
	}

	public void setDestinationRoundingValue(String destinationRoundingValue) {
		this.destinationRoundingValue = destinationRoundingValue;
	}

	public String getDescriptionRounding() {
		return descriptionRounding;
	}

	public void setDescriptionRounding(String descriptionRounding) {
		this.descriptionRounding = descriptionRounding;
	}

	public String getInternationalRoundingValue() {
		return internationalRoundingValue;
	}

	public void setInternationalRoundingValue(String internationalRoundingValue) {
		this.internationalRoundingValue = internationalRoundingValue;
	}

	public String getOffnetRoundingValue() {
		return offnetRoundingValue;
	}

	public void setOffnetRoundingValue(String offnetRoundingValue) {
		this.offnetRoundingValue = offnetRoundingValue;
	}

	@Override
	public String toString() {
		return "ClassPojo [internetRoundingLabel = " + internetRoundingLabel + ", destinationRoundingIcon = "
				+ destinationRoundingIcon + ", offnetRoundingLabel = " + offnetRoundingLabel
				+ ", destinationRoundingLabel = " + destinationRoundingLabel + ", internetRoundingValue = "
				+ internetRoundingValue + ", onnetRoundingLabel = " + onnetRoundingLabel
				+ ", internationalRoundingLabel = " + internationalRoundingLabel + ", onnetRoundingValue = "
				+ onnetRoundingValue + ", destinationRoundingValue = " + destinationRoundingValue
				+ ", descriptionRounding = " + descriptionRounding + ", internationalRoundingValue = "
				+ internationalRoundingValue + ", offnetRoundingValue = " + offnetRoundingValue + "]";
	}
}