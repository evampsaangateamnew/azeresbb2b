package com.evampsaanga.b2b.azerfon.suplementryservices;

public class Date {
	private String dateDescription;
	private String fromDateLabel;
	private String toDateLabel;
	private String fromDateValue;
	private String toDateValue;
	private String fragmentIcon;
	
	   public String getFragmentIcon() {
			return fragmentIcon;
		}

		public void setFragmentIcon(String fragmentIcon) {
			this.fragmentIcon = fragmentIcon;
		}


	public String getDateDescription() {
		return dateDescription;
	}

	public void setDateDescription(String dateDescription) {
		this.dateDescription = dateDescription;
	}

	public String getFromDateLabel() {
		return fromDateLabel;
	}

	public void setFromDateLabel(String fromDateLabel) {
		this.fromDateLabel = fromDateLabel;
	}

	public String getToDateLabel() {
		return toDateLabel;
	}

	public void setToDateLabel(String toDateLabel) {
		this.toDateLabel = toDateLabel;
	}

	public String getFromDateValue() {
		return fromDateValue;
	}

	public void setFromDateValue(String fromDateValue) {
		this.fromDateValue = fromDateValue;
	}

	public String getToDateValue() {
		return toDateValue;
	}

	public void setToDateValue(String toDateValue) {
		this.toDateValue = toDateValue;
	}

	@Override
	public String toString() {
		return "ClassPojo [dateDescription = " + dateDescription + ", fromDateLabel = " + fromDateLabel
				+ ", toDateLabel = " + toDateLabel + ", fromDateValue = " + fromDateValue + ", toDateValue = "
				+ toDateValue + "]";
	}
}
