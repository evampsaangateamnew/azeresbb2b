package com.evampsaanga.b2b.azerfon.suplementryservices;

public class TitleSubtitle {
	private String subtitleAValue;
	private String subtitleELabel;
	private String subtitleEUnit;
	private String subtitleHvalue;
	private String titleLabel;
	private String subtitleBIcon;
	private String subtitleDDescription;
	private String subtitleGLabel;
	private String subtitleADescription;
	private String subtitleDUnit;
	private String subtitleBUnit;
	private String subtitleCUnit;
	private String subtitleFLabel;
	private String subtitleFvalue;
	private String subtitleDvalue;
	private String subtitleFDescription;
	private String subtitleCValue;
	private String subtitleEIcon;
	private String subtitleHLabel;
	private String subtitleCIcon;
	private String subtitleBValue;
	private String subtitleFIcon;
	private String subtitleEvalue;
	private String subtitleHIcon;
	private String subtitleAUnit;
	private String subtitleDLabel;
	private String subtitleCLabel;
	private String subtitleCDescription;
	private String subtitleGDescription;
	private String subtitleGUnit;
	private String subtitleBLabel;
	private String subtitleFUnit;
	private String subtitleBDescription;
	private String subtitleHUnit;
	private String subtitleAIcon;
	private String subtitleEDescription;
	private String subtitleALabel;
	private String subtitleGIcon;
	private String subtitleGvalue;
	private String subtitleHDescription;
	private String subtitleDIcon;
	private String fragmentIcon;
	
   public String getFragmentIcon() {
		return fragmentIcon;
	}

	public void setFragmentIcon(String fragmentIcon) {
		this.fragmentIcon = fragmentIcon;
	}


	public String getSubtitleAValue() {
		return subtitleAValue;
	}

	public void setSubtitleAValue(String subtitleAValue) {
		this.subtitleAValue = subtitleAValue;
	}

	public String getSubtitleELabel() {
		return subtitleELabel;
	}

	public void setSubtitleELabel(String subtitleELabel) {
		this.subtitleELabel = subtitleELabel;
	}

	public String getSubtitleEUnit() {
		return subtitleEUnit;
	}

	public void setSubtitleEUnit(String subtitleEUnit) {
		this.subtitleEUnit = subtitleEUnit;
	}

	public String getSubtitleHvalue() {
		return subtitleHvalue;
	}

	public void setSubtitleHvalue(String subtitleHvalue) {
		this.subtitleHvalue = subtitleHvalue;
	}

	public String getTitleLabel() {
		return titleLabel;
	}

	public void setTitleLabel(String titleLabel) {
		this.titleLabel = titleLabel;
	}

	public String getSubtitleBIcon() {
		return subtitleBIcon;
	}

	public void setSubtitleBIcon(String subtitleBIcon) {
		this.subtitleBIcon = subtitleBIcon;
	}

	public String getSubtitleDDescription() {
		return subtitleDDescription;
	}

	public void setSubtitleDDescription(String subtitleDDescription) {
		this.subtitleDDescription = subtitleDDescription;
	}

	public String getSubtitleGLabel() {
		return subtitleGLabel;
	}

	public void setSubtitleGLabel(String subtitleGLabel) {
		this.subtitleGLabel = subtitleGLabel;
	}

	public String getSubtitleADescription() {
		return subtitleADescription;
	}

	public void setSubtitleADescription(String subtitleADescription) {
		this.subtitleADescription = subtitleADescription;
	}

	public String getSubtitleDUnit() {
		return subtitleDUnit;
	}

	public void setSubtitleDUnit(String subtitleDUnit) {
		this.subtitleDUnit = subtitleDUnit;
	}

	public String getSubtitleBUnit() {
		return subtitleBUnit;
	}

	public void setSubtitleBUnit(String subtitleBUnit) {
		this.subtitleBUnit = subtitleBUnit;
	}

	public String getSubtitleCUnit() {
		return subtitleCUnit;
	}

	public void setSubtitleCUnit(String subtitleCUnit) {
		this.subtitleCUnit = subtitleCUnit;
	}

	public String getSubtitleFLabel() {
		return subtitleFLabel;
	}

	public void setSubtitleFLabel(String subtitleFLabel) {
		this.subtitleFLabel = subtitleFLabel;
	}

	public String getSubtitleFvalue() {
		return subtitleFvalue;
	}

	public void setSubtitleFvalue(String subtitleFvalue) {
		this.subtitleFvalue = subtitleFvalue;
	}

	public String getSubtitleDvalue() {
		return subtitleDvalue;
	}

	public void setSubtitleDvalue(String subtitleDvalue) {
		this.subtitleDvalue = subtitleDvalue;
	}

	public String getSubtitleFDescription() {
		return subtitleFDescription;
	}

	public void setSubtitleFDescription(String subtitleFDescription) {
		this.subtitleFDescription = subtitleFDescription;
	}

	public String getSubtitleCValue() {
		return subtitleCValue;
	}

	public void setSubtitleCValue(String subtitleCValue) {
		this.subtitleCValue = subtitleCValue;
	}

	public String getSubtitleEIcon() {
		return subtitleEIcon;
	}

	public void setSubtitleEIcon(String subtitleEIcon) {
		this.subtitleEIcon = subtitleEIcon;
	}

	public String getSubtitleHLabel() {
		return subtitleHLabel;
	}

	public void setSubtitleHLabel(String subtitleHLabel) {
		this.subtitleHLabel = subtitleHLabel;
	}

	public String getSubtitleCIcon() {
		return subtitleCIcon;
	}

	public void setSubtitleCIcon(String subtitleCIcon) {
		this.subtitleCIcon = subtitleCIcon;
	}

	public String getSubtitleBValue() {
		return subtitleBValue;
	}

	public void setSubtitleBValue(String subtitleBValue) {
		this.subtitleBValue = subtitleBValue;
	}

	public String getSubtitleFIcon() {
		return subtitleFIcon;
	}

	public void setSubtitleFIcon(String subtitleFIcon) {
		this.subtitleFIcon = subtitleFIcon;
	}

	public String getSubtitleEvalue() {
		return subtitleEvalue;
	}

	public void setSubtitleEvalue(String subtitleEvalue) {
		this.subtitleEvalue = subtitleEvalue;
	}

	public String getSubtitleHIcon() {
		return subtitleHIcon;
	}

	public void setSubtitleHIcon(String subtitleHIcon) {
		this.subtitleHIcon = subtitleHIcon;
	}

	public String getSubtitleAUnit() {
		return subtitleAUnit;
	}

	public void setSubtitleAUnit(String subtitleAUnit) {
		this.subtitleAUnit = subtitleAUnit;
	}

	public String getSubtitleDLabel() {
		return subtitleDLabel;
	}

	public void setSubtitleDLabel(String subtitleDLabel) {
		this.subtitleDLabel = subtitleDLabel;
	}

	public String getSubtitleCLabel() {
		return subtitleCLabel;
	}

	public void setSubtitleCLabel(String subtitleCLabel) {
		this.subtitleCLabel = subtitleCLabel;
	}

	public String getSubtitleCDescription() {
		return subtitleCDescription;
	}

	public void setSubtitleCDescription(String subtitleCDescription) {
		this.subtitleCDescription = subtitleCDescription;
	}

	public String getSubtitleGDescription() {
		return subtitleGDescription;
	}

	public void setSubtitleGDescription(String subtitleGDescription) {
		this.subtitleGDescription = subtitleGDescription;
	}

	public String getSubtitleGUnit() {
		return subtitleGUnit;
	}

	public void setSubtitleGUnit(String subtitleGUnit) {
		this.subtitleGUnit = subtitleGUnit;
	}

	public String getSubtitleBLabel() {
		return subtitleBLabel;
	}

	public void setSubtitleBLabel(String subtitleBLabel) {
		this.subtitleBLabel = subtitleBLabel;
	}

	public String getSubtitleFUnit() {
		return subtitleFUnit;
	}

	public void setSubtitleFUnit(String subtitleFUnit) {
		this.subtitleFUnit = subtitleFUnit;
	}

	public String getSubtitleBDescription() {
		return subtitleBDescription;
	}

	public void setSubtitleBDescription(String subtitleBDescription) {
		this.subtitleBDescription = subtitleBDescription;
	}

	public String getSubtitleHUnit() {
		return subtitleHUnit;
	}

	public void setSubtitleHUnit(String subtitleHUnit) {
		this.subtitleHUnit = subtitleHUnit;
	}

	public String getSubtitleAIcon() {
		return subtitleAIcon;
	}

	public void setSubtitleAIcon(String subtitleAIcon) {
		this.subtitleAIcon = subtitleAIcon;
	}

	public String getSubtitleEDescription() {
		return subtitleEDescription;
	}

	public void setSubtitleEDescription(String subtitleEDescription) {
		this.subtitleEDescription = subtitleEDescription;
	}

	public String getSubtitleALabel() {
		return subtitleALabel;
	}

	public void setSubtitleALabel(String subtitleALabel) {
		this.subtitleALabel = subtitleALabel;
	}

	public String getSubtitleGIcon() {
		return subtitleGIcon;
	}

	public void setSubtitleGIcon(String subtitleGIcon) {
		this.subtitleGIcon = subtitleGIcon;
	}

	public String getSubtitleGvalue() {
		return subtitleGvalue;
	}

	public void setSubtitleGvalue(String subtitleGvalue) {
		this.subtitleGvalue = subtitleGvalue;
	}

	public String getSubtitleHDescription() {
		return subtitleHDescription;
	}

	public void setSubtitleHDescription(String subtitleHDescription) {
		this.subtitleHDescription = subtitleHDescription;
	}

	public String getSubtitleDIcon() {
		return subtitleDIcon;
	}

	public void setSubtitleDIcon(String subtitleDIcon) {
		this.subtitleDIcon = subtitleDIcon;
	}

	@Override
	public String toString() {
		return "ClassPojo [subtitleAValue = " + subtitleAValue + ", subtitleELabel = " + subtitleELabel
				+ ", subtitleEUnit = " + subtitleEUnit + ", subtitleHvalue = " + subtitleHvalue + ", titleLabel = "
				+ titleLabel + ", subtitleBIcon = " + subtitleBIcon + ", subtitleDDescription = " + subtitleDDescription
				+ ", subtitleGLabel = " + subtitleGLabel + ", subtitleADescription = " + subtitleADescription
				+ ", subtitleDUnit = " + subtitleDUnit + ", subtitleBUnit = " + subtitleBUnit + ", subtitleCUnit = "
				+ subtitleCUnit + ", subtitleFLabel = " + subtitleFLabel + ", subtitleFvalue = " + subtitleFvalue
				+ ", subtitleDvalue = " + subtitleDvalue + ", subtitleFDescription = " + subtitleFDescription
				+ ", subtitleCValue = " + subtitleCValue + ", subtitleEIcon = " + subtitleEIcon + ", subtitleHLabel = "
				+ subtitleHLabel + ", subtitleCIcon = " + subtitleCIcon + ", subtitleBValue = " + subtitleBValue
				+ ", subtitleFIcon = " + subtitleFIcon + ", subtitleEvalue = " + subtitleEvalue + ", subtitleHIcon = "
				+ subtitleHIcon + ", subtitleAUnit = " + subtitleAUnit + ", subtitleDLabel = " + subtitleDLabel
				+ ", subtitleCLabel = " + subtitleCLabel + ", subtitleCDescription = " + subtitleCDescription
				+ ", subtitleGDescription = " + subtitleGDescription + ", subtitleGUnit = " + subtitleGUnit
				+ ", subtitleBLabel = " + subtitleBLabel + ", subtitleFUnit = " + subtitleFUnit
				+ ", subtitleBDescription = " + subtitleBDescription + ", subtitleHUnit = " + subtitleHUnit
				+ ", subtitleAIcon = " + subtitleAIcon + ", subtitleEDescription = " + subtitleEDescription
				+ ", subtitleALabel = " + subtitleALabel + ", subtitleGIcon = " + subtitleGIcon + ", subtitleGvalue = "
				+ subtitleGvalue + ", subtitleHDescription = " + subtitleHDescription + ", subtitleDIcon = "
				+ subtitleDIcon + "]";
	}
}
