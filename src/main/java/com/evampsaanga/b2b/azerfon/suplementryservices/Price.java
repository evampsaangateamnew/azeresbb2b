package com.evampsaanga.b2b.azerfon.suplementryservices;

public class Price {
	private String offnetPriceLabel;
	private String callPriceLabel;
	private String descriptionPrice;
	private String offnetPriceValue;
	private String priceValueD;
	private String onnetPriceLabel;
	private String callPriceIcon;
	private String priceLabelC;
	private String priceLabelD;
	private String priceValueC;
	private String onnetPriceValue;
	private String callPriceValue;
	private String offersCurrency;
	private String fragmentIcon;
	
	   public String getFragmentIcon() {
			return fragmentIcon;
		}

		public void setFragmentIcon(String fragmentIcon) {
			this.fragmentIcon = fragmentIcon;
		}


	public String getOffersCurrency() {
		return offersCurrency;
	}

	public void setOffersCurrency(String offersCurrency) {
		this.offersCurrency = offersCurrency;
	}

	public String getOffnetPriceLabel() {
		return offnetPriceLabel;
	}

	public void setOffnetPriceLabel(String offnetPriceLabel) {
		this.offnetPriceLabel = offnetPriceLabel;
	}

	public String getCallPriceLabel() {
		return callPriceLabel;
	}

	public void setCallPriceLabel(String callPriceLabel) {
		this.callPriceLabel = callPriceLabel;
	}

	public String getDescriptionPrice() {
		return descriptionPrice;
	}

	public void setDescriptionPrice(String descriptionPrice) {
		this.descriptionPrice = descriptionPrice;
	}

	public String getOffnetPriceValue() {
		return offnetPriceValue;
	}

	public void setOffnetPriceValue(String offnetPriceValue) {
		this.offnetPriceValue = offnetPriceValue;
	}

	public String getPriceValueD() {
		return priceValueD;
	}

	public void setPriceValueD(String priceValueD) {
		this.priceValueD = priceValueD;
	}

	public String getOnnetPriceLabel() {
		return onnetPriceLabel;
	}

	public void setOnnetPriceLabel(String onnetPriceLabel) {
		this.onnetPriceLabel = onnetPriceLabel;
	}

	public String getCallPriceIcon() {
		return callPriceIcon;
	}

	public void setCallPriceIcon(String callPriceIcon) {
		this.callPriceIcon = callPriceIcon;
	}

	public String getPriceLabelC() {
		return priceLabelC;
	}

	public void setPriceLabelC(String priceLabelC) {
		this.priceLabelC = priceLabelC;
	}

	public String getPriceLabelD() {
		return priceLabelD;
	}

	public void setPriceLabelD(String priceLabelD) {
		this.priceLabelD = priceLabelD;
	}

	public String getPriceValueC() {
		return priceValueC;
	}

	public void setPriceValueC(String priceValueC) {
		this.priceValueC = priceValueC;
	}

	public String getOnnetPriceValue() {
		return onnetPriceValue;
	}

	public void setOnnetPriceValue(String onnetPriceValue) {
		this.onnetPriceValue = onnetPriceValue;
	}

	public String getCallPriceValue() {
		return callPriceValue;
	}

	public void setCallPriceValue(String callPriceValue) {
		this.callPriceValue = callPriceValue;
	}
}
