package com.evampsaanga.b2b.azerfon.suplementryservices;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_EMPTY)
public class Data {
	@JsonProperty("Campaign")
	private Campaign Campaign = null;
	@JsonProperty("Call")
	private Call Call = null;
	@JsonProperty("TM")
	private TM TM = null;
	@JsonProperty("roaming")
	private Roaming roaming = null;
	@JsonProperty("SMS")
	private SMS SMS = null;
	@JsonProperty("Hybrid")
	private Hybrid Hybrid = null;
	@JsonProperty("Internet")
	private Internet Internet = null;
	
	@JsonProperty("Campaign")
	public Campaign getCampaign() {
		return Campaign;
	}
	@JsonProperty("Campaign")
	public void setCampaign(Campaign campaign) {
		Campaign = campaign;
	}
	@JsonProperty("Call")
	public Call getCall() {
		return Call;
	}
	@JsonProperty("Call")
	public void setCall(Call call) {
		Call = call;
	}
	@JsonProperty("TM")
	public TM getTM() {
		return TM;
	}
	@JsonProperty("TM")
	public void setTM(TM tM) {
		TM = tM;
	}
	@JsonProperty("roaming")
	public Roaming getRoaming() {
		return roaming;
	}
	@JsonProperty("roaming")
	public void setRoaming(Roaming roaming) {
		this.roaming = roaming;
	}
	@JsonProperty("SMS")
	public SMS getSMS() {
		return SMS;
	}
	@JsonProperty("SMS")
	public void setSMS(SMS sMS) {
		SMS = sMS;
	}
	@JsonProperty("Hybrid")
	public Hybrid getHybrid() {
		return Hybrid;
	}
	@JsonProperty("Hybrid")
	public void setHybrid(Hybrid hybrid) {
		Hybrid = hybrid;
	}
	@JsonProperty("Internet")
	public Internet getInternet() {
		return Internet;
	}
	@JsonProperty("Internet")
	public void setInternet(Internet internet) {
		Internet = internet;
	}
	
	
}