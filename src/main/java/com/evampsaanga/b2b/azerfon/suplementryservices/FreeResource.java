package com.evampsaanga.b2b.azerfon.suplementryservices;

public class FreeResource {
	private String freeResourceValue;
	private String onnetFreeResourceValue;
	private String freeResourceLabel;
	private String onnetFreeResourceLabel;
	private String descriptionFreeResource;
	private String fragmentIcon;
	
	   public String getFragmentIcon() {
			return fragmentIcon;
		}

		public void setFragmentIcon(String fragmentIcon) {
			this.fragmentIcon = fragmentIcon;
		}


	public String getFreeResourceValue() {
		return freeResourceValue;
	}

	public void setFreeResourceValue(String freeResourceValue) {
		this.freeResourceValue = freeResourceValue;
	}

	public String getOnnetFreeResourceValue() {
		return onnetFreeResourceValue;
	}

	public void setOnnetFreeResourceValue(String onnetFreeResourceValue) {
		this.onnetFreeResourceValue = onnetFreeResourceValue;
	}

	public String getFreeResourceLabel() {
		return freeResourceLabel;
	}

	public void setFreeResourceLabel(String freeResourceLabel) {
		this.freeResourceLabel = freeResourceLabel;
	}

	public String getOnnetFreeResourceLabel() {
		return onnetFreeResourceLabel;
	}

	public void setOnnetFreeResourceLabel(String onnetFreeResourceLabel) {
		this.onnetFreeResourceLabel = onnetFreeResourceLabel;
	}

	public String getDescriptionFreeResource() {
		return descriptionFreeResource;
	}

	public void setDescriptionFreeResource(String descriptionFreeResource) {
		this.descriptionFreeResource = descriptionFreeResource;
	}
}
