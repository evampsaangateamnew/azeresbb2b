package com.evampsaanga.b2b.azerfon.suplementryservices;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class InternetOffersDetails {
	private TitleSubtitle titleSubtitle;
	private FreeResource freeResource;
	private TextTitle textTitle;
	private Time time;
	private Price price;
	private Rounding rounding;
	private Date date;
	@JsonProperty("roaming")
	private Roamingsub roaming;

	public TitleSubtitle getTitleSubtitle() {
		return titleSubtitle;
	}

	public void setTitleSubtitle(TitleSubtitle titleSubtitle) {
		this.titleSubtitle = titleSubtitle;
	}

	public FreeResource getFreeResource() {
		return freeResource;
	}

	public void setFreeResource(FreeResource freeResource) {
		this.freeResource = freeResource;
	}

	public TextTitle getTextTitle() {
		return textTitle;
	}

	public void setTextTitle(TextTitle textTitle) {
		this.textTitle = textTitle;
	}

	public Time getTime() {
		return time;
	}

	public void setTime(Time time) {
		this.time = time;
	}

	public Price getPrice() {
		return price;
	}

	public void setPrice(Price price) {
		this.price = price;
	}

	public Rounding getRounding() {
		return rounding;
	}

	public void setRounding(Rounding rounding) {
		this.rounding = rounding;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
}