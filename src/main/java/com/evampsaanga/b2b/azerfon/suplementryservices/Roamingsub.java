package com.evampsaanga.b2b.azerfon.suplementryservices;

public class Roamingsub {
	private String descriptionPosition;
	private String description;
	private Country[] country;
	private String fragmentIcon;
	
	   public String getFragmentIcon() {
			return fragmentIcon;
		}

		public void setFragmentIcon(String fragmentIcon) {
			this.fragmentIcon = fragmentIcon;
		}


	public String getDescriptionPosition() {
		return descriptionPosition;
	}

	public void setDescriptionPosition(String descriptionPosition) {
		this.descriptionPosition = descriptionPosition;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Country[] getCountry() {
		return country;
	}

	public void setCountry(Country[] country) {
		this.country = country;
	}
}
