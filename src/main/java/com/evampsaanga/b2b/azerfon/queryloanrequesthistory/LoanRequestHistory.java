package com.evampsaanga.b2b.azerfon.queryloanrequesthistory;

public class LoanRequestHistory {
	private String loanID = "";
	private String dateTime = "";
	private String amount = "";
	private String status = "";
	private String paid = "";
	private String remaining = "";

	public LoanRequestHistory(String loanID, String dateTime, String amount, String status, String paid,
			String remaining) {
		super();
		this.loanID = loanID;
		this.dateTime = dateTime;
		this.amount = amount;
		this.status = status;
		this.paid = paid;
		this.remaining = remaining;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the paid
	 */
	public String getPaid() {
		return paid;
	}

	/**
	 * @param paid
	 *            the paid to set
	 */
	public void setPaid(String paid) {
		this.paid = paid;
	}

	/**
	 * @return the remaining
	 */
	public String getRemaining() {
		return remaining;
	}

	/**
	 * @param remaining
	 *            the remaining to set
	 */
	public void setRemaining(String remaining) {
		this.remaining = remaining;
	}

	/**
	 * @return the loanID
	 */
	public String getLoanID() {
		return loanID;
	}

	/**
	 * @param loanID
	 *            the loanID to set
	 */
	public void setLoanID(String loanID) {
		this.loanID = loanID;
	}

	/**
	 * @return the dateTime
	 */
	public String getDateTime() {
		return dateTime;
	}

	/**
	 * @param dateTime
	 *            the dateTime to set
	 */
	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	/**
	 * @return the amount
	 */
	public String getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            the amount to set
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}
}
