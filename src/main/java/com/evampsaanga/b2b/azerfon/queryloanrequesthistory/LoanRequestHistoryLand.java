package com.evampsaanga.b2b.azerfon.queryloanrequesthistory;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.amqimplementationsesb.LogsType;
import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.ThirdPartyNames;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Decrypter;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.evampsaanga.b2b.developer.utils.SOAPLoggingHandler;
import com.huawei.bme.cbsinterface.arservices.QueryLoanLogRequest;
import com.huawei.bme.cbsinterface.arservices.QueryLoanLogRequestMsg;
import com.huawei.bme.cbsinterface.arservices.QueryLoanLogResult;
import com.huawei.bme.cbsinterface.arservices.QueryLoanLogResult.LoanLogDetail;
import com.huawei.bme.cbsinterface.arservices.QueryLoanLogResultMsg;
import com.huawei.bme.cbsinterface.cbscommon.OperatorInfo;
import com.huawei.bme.cbsinterface.cbscommon.OwnershipInfo;
import com.huawei.bme.cbsinterface.cbscommon.SecurityInfo;
import com.huawei.cbs.ar.wsservice.arcommon.SubAccessCode;

@Path("/bakcell")
public class LoanRequestHistoryLand {
	public static final Logger logger = Logger.getLogger("azerfon-esb");

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LoanRequestHistoryClientResponse Get(@Header("credentials") String credential,
			@Header("Content-Type") String contentType, @Body() String requestBody) {
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.LOAN_REQUEST_HISTORY_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.LOAN_REQUEST_HISTORY);
		logs.setTableType(LogsType.LoanRequestHistory);
		try {
			logger.info("Request Landed on LoanRequestHistoryClientRequestLand:" + requestBody);
			LoanRequestHistoryClientRequest cclient = new LoanRequestHistoryClientRequest();
			try {
				cclient = Helper.JsonToObject(requestBody, LoanRequestHistoryClientRequest.class);
				if (cclient != null) {
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
					logs.setLang(cclient.getLang());	}
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				LoanRequestHistoryClientResponse resp = new LoanRequestHistoryClientResponse();
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(ResponseCodes.ERROR_400_CODE);
				logs.setResponseDescription(ResponseCodes.ERROR_400);
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			try {
				LoanRequestHistoryClientResponse resp = new LoanRequestHistoryClientResponse();
				if (cclient != null) {
					if (!cclient.getStartDate().isEmpty()) {
						if (!Helper.isValidFormat("yyyy-mm-dd", cclient.getEndDate())) {
							resp.setReturnMsg("end date is not in proper format");
							logs.setResponseDescription("end date is not in proper format");
							logs.updateLog(logs);
							return resp;
						}
					} else {
						resp.setReturnMsg("end date is not in proper format");
						logs.setResponseDescription("end date is not in proper format");
						logs.updateLog(logs);
						return resp;
					}
				} else {
					resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_400);
					logs.setResponseCode(ResponseCodes.ERROR_400_CODE);
					logs.setResponseDescription(ResponseCodes.ERROR_400);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} catch (Exception ex) {
				LoanRequestHistoryClientResponse resp = new LoanRequestHistoryClientResponse();
				resp.setReturnCode(ResponseCodes.MISSING_PARAMETER_CODE);
				resp.setReturnMsg("end date was improper");
				logs.setResponseDescription("end date was improper");
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					LoanRequestHistoryClientResponse resp = new LoanRequestHistoryClientResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(ResponseCodes.ERROR_401_CODE);
					logs.setResponseDescription(ResponseCodes.ERROR_401);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						LoanRequestHistoryClientResponse res = new LoanRequestHistoryClientResponse();
						res.setReturnCode(ResponseCodes.ERROR_400);
						res.setReturnMsg(verification);
						logs.setResponseCode(ResponseCodes.ERROR_400_CODE);
						logs.setResponseDescription(verification);
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return res;
					}
				} else {
					LoanRequestHistoryClientResponse resp = new LoanRequestHistoryClientResponse();
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode((ResponseCodes.ERROR_MSISDN_CODE));
					logs.setResponseDescription(ResponseCodes.ERROR_MSISDN);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					LoanRequestHistoryClientResponse resp = new LoanRequestHistoryClientResponse();
					QueryLoanLogResultMsg loanResponse = null;
					ArrayList<LoanRequestHistory> requestHistoryList = new ArrayList<LoanRequestHistory>();
					try {
						loanResponse = queryLoanLog(cclient.getmsisdn(), cclient.getStartDate(), cclient.getEndDate());
						if (loanResponse.getResultHeader().getResultCode().equals("0")) {
							Date endDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
									.parse(cclient.getEndDate() + " 23:59:59");
							Date startDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
									.parse(cclient.getStartDate() + " 00:00:00");
							QueryLoanLogResult loanLogResult = loanResponse.getQueryLoanLogResult();
							for (LoanLogDetail loanLogsSummary : loanLogResult.getLoanLogDetail()) {
								String operationDate = loanLogsSummary.getInitLoanDate();
								try {
									operationDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
											.format(new SimpleDateFormat("yyyyMMddHHmmss")
													.parse(loanLogsSummary.getInitLoanDate()));
								} catch (Exception e) {
									logger.error(Helper.GetException(e));
								}
								Date dateFormatted = null;
								try {
									dateFormatted = new SimpleDateFormat("yyyyMMddHHmmss")
											.parse(loanLogsSummary.getInitLoanDate());
								} catch (Exception e) {
									logger.error(Helper.GetException(e));
								}
								if (dateFormatted != null)
									if (dateFormatted.before(endDate) && dateFormatted.after(startDate)) {
										String status = ConfigurationManager
												.getConfigurationFromCache(ConfigurationManager.MAPPING_LOANLOG_STATUS
														+ loanLogsSummary.getLoanStatus());
										if (loanLogsSummary.getLoanStatus()
												.equalsIgnoreCase(Constants.LOAN_STATUS_OPEN)) {
											LoanRequestHistory loanRequestHistory = new LoanRequestHistory(
													loanLogsSummary.getLoanID(),
													new SimpleDateFormat(Constants.SQL_DATE_FORMAT_History).format(
															new SimpleDateFormat(Constants.SQL_DATE_FORMAT_required)
																	.parse(operationDate)),
													Helper.getBakcellMoney(loanLogsSummary.getInitLoanAMT()), status,
													Helper.getBakcellMoney(loanLogsSummary.getPaidAMT()),
													Helper.getBakcellMoney(loanLogsSummary.getRepaymentAMT()
															- loanLogsSummary.getPaidAMT()));
											requestHistoryList.add(loanRequestHistory);
										}
										if (loanLogsSummary.getLoanStatus()
												.equalsIgnoreCase(Constants.LOAN_STATUS_CLOSED)) {
											LoanRequestHistory loanRequestHistory = new LoanRequestHistory(
													loanLogsSummary.getLoanID(),
													new SimpleDateFormat(Constants.SQL_DATE_FORMAT_History).format(
															new SimpleDateFormat(Constants.SQL_DATE_FORMAT_required)
																	.parse(operationDate)),
													Helper.getBakcellMoney(loanLogsSummary.getInitLoanAMT()), status,
													Helper.getBakcellMoney(loanLogsSummary.getPaidAMT()),
													Helper.getBakcellMoney(loanLogsSummary.getRemainingAMT()));
											requestHistoryList.add(loanRequestHistory);
										}
									}
							}
							resp.setLoanRequest(requestHistoryList);
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
							logs.setResponseDescription(ResponseCodes.SUCESS_DES_200);
							logs.setResponseCode(ResponseCodes.SUCESS_CODE_200);
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						} else {
							resp.setReturnCode(loanResponse.getResultHeader().getResultCode());
							resp.setReturnMsg(loanResponse.getResultHeader().getResultDesc());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} catch (Exception ex) {
						logger.error(Helper.GetException(ex));
					}
					logs.updateLog(logs);
					return resp;
				} else {
					LoanRequestHistoryClientResponse resp = new LoanRequestHistoryClientResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseDescription(ResponseCodes.ERROR_401);
					logs.setResponseCode(ResponseCodes.ERROR_401_CODE);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		LoanRequestHistoryClientResponse resp = new LoanRequestHistoryClientResponse();
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseDescription(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	public QueryLoanLogResultMsg queryLoanLog(String msisdn, String startDate, String endDate) {
		QueryLoanLogRequestMsg msg = new QueryLoanLogRequestMsg();
		QueryLoanLogRequest qLLR = new QueryLoanLogRequest();
		SubAccessCode subAC = new SubAccessCode();
		subAC.setPrimaryIdentity(msisdn);
		qLLR.setSubAccessCode(subAC);
		qLLR.setBeginRowNum(Constants.BEGIN_ROW_NUM);
		qLLR.setFetchRowNum(Constants.FETCH_ROW_NUM);
		qLLR.setTotalRowNum(Constants.TOTAL_ROWNUM);
		msg.setRequestHeader(getLoanLogRequestHeader());
		msg.setQueryLoanLogRequest(qLLR);
	//	return CBSARService.getInstance().queryLoanLog(msg);
		QueryLoanLogResultMsg resp=new QueryLoanLogResultMsg();
		return resp;
	}

	public com.huawei.bme.cbsinterface.cbscommon.RequestHeader getLoanLogRequestHeader() {
		com.huawei.bme.cbsinterface.cbscommon.RequestHeader loanRequestHeader = new com.huawei.bme.cbsinterface.cbscommon.RequestHeader();
		loanRequestHeader.setVersion(Constants.LL_VERSION);
		loanRequestHeader.setBusinessCode(Constants.LL_BUSINESS_CODE);
		OwnershipInfo ownershipinf = new OwnershipInfo();
		ownershipinf.setBEID(Constants.LL_BEID);
		ownershipinf.setBRID(Constants.LL_BRID);
		loanRequestHeader.setOwnershipInfo(ownershipinf);
		SecurityInfo value = new SecurityInfo();
		value.setLoginSystemCode(ConfigurationManager.getConfigurationFromCache("cbs.username"));
		value.setPassword(ConfigurationManager.getConfigurationFromCache("cbs.password"));
		loanRequestHeader.setAccessSecurity(value);
		OperatorInfo opInfo = new OperatorInfo();
		opInfo.setChannelID(Constants.LL_CHANNEL_ID);
		opInfo.setOperatorID(Constants.LL_OPERATOR_ID);
		loanRequestHeader.setOperatorInfo(opInfo);
		loanRequestHeader.setMessageSeq(Helper.generateTransactionID());
		return loanRequestHeader;
	}
}
