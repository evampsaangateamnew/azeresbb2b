package com.evampsaanga.b2b.azerfon.getcdrssummary;

import java.util.ArrayList;

public class CDRSummary {
	private ArrayList<CDRSummaryDetails> records;
	private String totalUsage;
	private String totalCharge;

	public ArrayList<CDRSummaryDetails> getRecords() {
		return records;
	}

	public void setRecords(ArrayList<CDRSummaryDetails> records) {
		this.records = records;
	}

	public String getTotalUsage() {
		return totalUsage;
	}

	public void setTotalUsage(String totalUsage) {
		this.totalUsage = totalUsage;
	}

	public String getTotalCharge() {
		return totalCharge;
	}

	public void setTotalCharge(String totalCharge) {
		this.totalCharge = totalCharge;
	}
}
