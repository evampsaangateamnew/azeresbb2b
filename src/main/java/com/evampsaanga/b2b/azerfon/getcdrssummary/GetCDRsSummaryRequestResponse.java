package com.evampsaanga.b2b.azerfon.getcdrssummary;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetCDRsSummaryRequestResponse")
@XmlRootElement(name = "GetCDRsSummaryRequestResponse")
public class GetCDRsSummaryRequestResponse extends BaseResponse {
	private CDRSummaryResponseBody summaryResponseBody;

	public CDRSummaryResponseBody getSummaryResponseBody() {
		return summaryResponseBody;
	}

	public void setSummaryResponseBody(CDRSummaryResponseBody summaryResponseBody) {
		this.summaryResponseBody = summaryResponseBody;
	}
}