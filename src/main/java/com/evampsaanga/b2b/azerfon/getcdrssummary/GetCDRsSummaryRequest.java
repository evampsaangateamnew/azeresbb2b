package com.evampsaanga.b2b.azerfon.getcdrssummary;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "GetCDRsSummaryRequest")
public class GetCDRsSummaryRequest extends com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest {
	@XmlElement(name = "startDate", required = true)
	String startDate = "";
	@XmlElement(name = "endDate", required = true)
	String endDate = "";
	@XmlElement(name = "accountId", required = true)
	String accountId = "";
	@XmlElement(name = "customerId", required = true)
	String customerId = "";

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	/**
	 * @return the startDate
	 */
	public String getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate
	 *            the startDate to set
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the endDate
	 */
	public String getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate
	 *            the endDate to set
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public GetCDRsSummaryRequest() {
	}
}
