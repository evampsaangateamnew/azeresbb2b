package com.evampsaanga.b2b.azerfon.getcdrssummary;

public class CDRSummaryResponseBody {
	private CDRSummary voiceRecords;
	private CDRSummary smsRecords;
	private CDRSummary dataRecords;
	private CDRSummary otherRecords;

	public CDRSummary getVoiceRecords() {
		return voiceRecords;
	}

	public void setVoiceRecords(CDRSummary voiceRecords) {
		this.voiceRecords = voiceRecords;
	}

	public CDRSummary getSmsRecords() {
		return smsRecords;
	}

	public void setSmsRecords(CDRSummary smsRecords) {
		this.smsRecords = smsRecords;
	}

	public CDRSummary getDataRecords() {
		return dataRecords;
	}

	public void setDataRecords(CDRSummary dataRecords) {
		this.dataRecords = dataRecords;
	}

	public CDRSummary getOtherRecords() {
		return otherRecords;
	}

	public void setOtherRecords(CDRSummary otherRecords) {
		this.otherRecords = otherRecords;
	}
}
