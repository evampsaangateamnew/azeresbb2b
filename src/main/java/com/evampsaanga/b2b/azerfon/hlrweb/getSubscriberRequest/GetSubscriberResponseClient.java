package com.evampsaanga.b2b.azerfon.hlrweb.getSubscriberRequest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;
import com.huawei.bss.soaif._interface.hlrwebservice.GetSubscriberResponse;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetSubscriberResponseClient", propOrder = { "responseBody" })
@XmlRootElement(name = "GetSubscriberResponseClient")
public class GetSubscriberResponseClient extends BaseResponse {
	@XmlElement(name = "responseBody", required = true)
	private GetSubscriberResponse responseBody = new GetSubscriberResponse();

	public GetSubscriberResponseClient(GetSubscriberResponse res) {
		super();
		this.responseBody = res;
	}

	public GetSubscriberResponseClient() {
		super();
		this.responseBody = new GetSubscriberResponse();
	}

	public GetSubscriberResponse getResponseBody() {
		return responseBody;
	}

	public void setResponseBody(GetSubscriberResponse responseBody) {
		this.responseBody = responseBody;
	}
}
