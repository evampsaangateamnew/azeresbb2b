package com.evampsaanga.b2b.azerfon.hlrweb.getquerybalanceRequest;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.ws.WebServiceException;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.amqimplementationsesb.LogsType;
import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.ThirdPartyNames;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Decrypter;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.evampsaanga.b2b.gethomepage.Balance;
import com.huawei.bss.soaif._interface.common.ReqHeader;
@Path("/bakcell")
public class GetQueryBalanceLand {

	public static final Logger logger = Logger.getLogger("azerfon-esb");

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetQueryBalanceResponseClient Get(@Header("credentials") String credential, @Body() String requestBody) {
		logger.info("Requset on getBalance HLR:" + requestBody);
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.HLR_QUERY_BALANCE_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.HLR_QUERY_BALANCE);
		logs.setTableType(LogsType.HlrQueryBalance);
		
		GetQueryBalanceResponseClient response = new GetQueryBalanceResponseClient();
		try {
			String trnsactionName = Transactions.HLR_QUERY_BALANCE_TRANSACTION_NAME + " " + Transactions.HLR_QUERY_BALANCE_TRANSACTION_NAME;
			String token = Helper.retrieveToken(trnsactionName, Helper.getValueFromJSON(requestBody, "msisdn"));
			GetQueryBalanceRequestClient cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, GetQueryBalanceRequestClient.class);
				if (cclient != null) {
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
				}
			} catch (Exception ex) {
				response.setReturnCode(ResponseCodes.ERROR_400_CODE);
				response.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(response.getReturnCode());
				logs.setResponseDescription(response.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return response;
			}
			if (cclient != null) {
				String credentials = Decrypter.getInstance().decrypt(credential);
				if (credentials == null) {
					response = new GetQueryBalanceResponseClient();
					response.setReturnCode(ResponseCodes.ERROR_401_CODE);
					response.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(response.getReturnCode());
					logs.setResponseDescription(response.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return response;
				}
				String verification = Helper.validateRequest(cclient);
				if (!verification.equals("")) {
					response = new GetQueryBalanceResponseClient();
					response.setReturnCode(ResponseCodes.ERROR_400);
					response.setReturnMsg(verification);
					logs.setResponseCode(response.getReturnCode());
					logs.setResponseDescription(response.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return response;
				}
				try {
					HLRBalanceServices service = new HLRBalanceServices();
					Balance balance = service.getBalance(cclient.getmsisdn(), cclient.getCustomerType(),null,token);
					if (balance.getReturnCode().equals("0")) {
						response.setBalance(balance);
						response.setReturnCode(ResponseCodes.SUCESS_CODE_200);
						response.setReturnMsg(ResponseCodes.SUCESS_DES_200);
						logs.setResponseCode(response.getReturnCode());
						logs.setResponseDescription(response.getReturnMsg());
						logs.setUserType(cclient.getCustomerType());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					} else {
						response.setReturnCode(balance.getReturnCode());
						response.setReturnMsg(balance.getReturnMsg());
						logs.setResponseCode(response.getReturnCode());
						logs.setResponseDescription(response.getReturnMsg());
						logs.setUserType(cclient.getCustomerType());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					}
					logs.updateLog(logs);
					return response;
				} catch (Exception ee) {
					response = new GetQueryBalanceResponseClient();
					if (ee instanceof WebServiceException) {
						response.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
						response.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
						logs.setResponseCode(response.getReturnCode());
						logs.setResponseDescription(response.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					}
				}
			} 
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		logs.updateLog(logs);
		return response;
	}

	public ReqHeader getReqHeaderForQueryBalanceHLR() {
		ReqHeader reqHForQBalance = new ReqHeader();
			reqHForQBalance.setChannelId(ConfigurationManager.getConfigurationFromCache("hlrweb.reqh.ChannelId").trim());
			reqHForQBalance.setAccessUser(ConfigurationManager.getConfigurationFromCache("hlrweb.reqh.AccessUser").trim());
			reqHForQBalance.setAccessPwd(ConfigurationManager.getConfigurationFromCache("hlrweb.reqh.AccessPwd").trim());
			reqHForQBalance.setMIDWAREChannelID(ConfigurationManager.getConfigurationFromCache("hlrweb.reqh.MIDWAREChannelID").trim());
			reqHForQBalance.setMIDWAREAccessPwd(ConfigurationManager.getConfigurationFromCache("hlrweb.reqh.MIDWAREAccessPwd").trim());
			reqHForQBalance.setMIDWAREAccessUser(ConfigurationManager.getConfigurationFromCache("hlrweb.reqh.MIDWAREAccessUser").trim());
			reqHForQBalance.setTransactionId(Helper.generateTransactionID());
		return reqHForQBalance;
	}
	/*public static QueryBalanceResultMsg queryBalanceResponseArServices(String msisdn)
	{
		com.huawei.bme.cbsinterface.arservices.QueryBalanceRequestMsg queryBalanceRequestMsg  = new QueryBalanceRequestMsg();
		queryBalanceRequestMsg.setRequestHeader(CBSARService.getRequestPaymentRequestHeader());
		QueryBalanceRequest queryBalanceRequest = new QueryBalanceRequest();
		queryBalanceRequest.setBalanceType("");
		
		
		SubAccessCode subAccessCode = new SubAccessCode();
		subAccessCode.setPrimaryIdentity(msisdn);
//		subAccessCode.setSubscriberKey("");
		
		
		QueryObj queryObj = new QueryObj();
		queryObj.setSubAccessCode(subAccessCode);
		queryBalanceRequest.setQueryObj(queryObj);
		
		
		queryBalanceRequestMsg.setQueryBalanceRequest(queryBalanceRequest);
		
		com.huawei.bme.cbsinterface.arservices.QueryBalanceResultMsg queryBalanceArServices=CBSARService.getInstance().queryBalance(queryBalanceRequestMsg);
	     return queryBalanceArServices;
	}*/
}
