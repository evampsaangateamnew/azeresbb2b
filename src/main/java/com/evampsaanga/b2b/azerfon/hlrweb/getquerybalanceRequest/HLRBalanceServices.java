package com.evampsaanga.b2b.azerfon.hlrweb.getquerybalanceRequest;

import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

import org.apache.log4j.Logger;

import com.huawei.bme.cbsinterface.arservices.QueryBalanceResult.AcctList;
import com.huawei.bme.cbsinterface.arservices.QueryBalanceResult.AcctList.AccountCredit;
import com.huawei.bme.cbsinterface.arservices.QueryBalanceResult.AcctList.OutStandingList;
import com.huawei.bme.cbsinterface.arservices.QueryBalanceResult.AcctList.OutStandingList.OutStandingDetail;
import com.huawei.bme.cbsinterface.arservices.QueryBalanceResult.AcctList.PaymentLimitUsage;
import com.evampsaanga.b2b.azerfon.thirdpartycall.AzerfonThirdPartyCalls;
import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.evampsaanga.b2b.gethomepage.Balance;
import com.evampsaanga.b2b.gethomepage.BounusWallet;
import com.evampsaanga.b2b.gethomepage.CountryWideWallet;
import com.evampsaanga.b2b.gethomepage.MainWallet;
import com.evampsaanga.b2b.gethomepage.Postpaid;
import com.evampsaanga.b2b.gethomepage.Prepaid;
import com.huawei.bme.cbsinterface.arservices.QueryBalanceResultMsg;

public class HLRBalanceServices {
	public static final Logger logger = Logger.getLogger("azerfon-esb");

	private static Balance processBalanceResponse(String msisdn, String userType, List<AcctList> balanceResponse) {
		Balance finalBalanceResponse = new Balance();
		Postpaid postPaidBalanceData = new Postpaid();
		if (userType.equals(Constants.POSTPAID)) {
			for (AcctList acctList : balanceResponse) {
				
				long mainBalance=0L;
				String mainBalanceLabel;
				long creditLimit=0L;
				String creditLabel;
				long totalPayments=0L;
				long outStandingDebt=0L;
				String outStandingDebtLabel;
				logger.info(msisdn+"CHECKING PAYMENT LIMIT USAGE IS NON NULL AND GREATER THAN ZERO");
				if(acctList.getPaymentLimitUsage()!=null && acctList.getPaymentLimitUsage().size()>0 )
				{
					logger.info(msisdn+"PAYMENT LIMIT USAGE TRUE"+acctList.getPaymentLimitUsage().size());
					for (PaymentLimitUsage paymentLimitUSage : acctList.getPaymentLimitUsage()) 
					{
						logger.info(msisdn+"CHECKING AMOUNT TO BE 0"+paymentLimitUSage.getAmount());
						if(paymentLimitUSage.getAmount()!=0)
						{
							logger.info(msisdn+"PAYMENT LMIT AMOUNT IS ="+paymentLimitUSage.getAmount());
							creditLimit=paymentLimitUSage.getAmount();
							for (AccountCredit acountCredit : acctList.getAccountCredit()) 
							{
								
								logger.info(msisdn+"MAIN BALANCE"+acountCredit.getTotalRemainAmount());
								
								mainBalance=acountCredit.getTotalRemainAmount();
							
							}
							
						}
						else
						{
							logger.info(msisdn+"SETTING ACCOUNT CREDIT VALUE"+paymentLimitUSage.getAmount());
							for (AccountCredit acountCredit : acctList.getAccountCredit()) 
							{
								
								logger.info(msisdn+"CREDIT LIMIT "+acountCredit.getTotalCreditAmount());
								creditLimit=acountCredit.getTotalCreditAmount();
								mainBalance=acountCredit.getTotalRemainAmount();
							
							}
						}
						
					
					}
//					for (AccountCredit acountCredit : acctList.getAccountCredit()) {
//						
//						mainBalance=acountCredit.getTotalRemainAmount();
//					
//					}
				}
				else
				{
					for (AccountCredit acountCredit : acctList.getAccountCredit()) {
						creditLimit=acountCredit.getTotalCreditAmount();
						mainBalance=acountCredit.getTotalRemainAmount();
					
					}
				}
				
				for (OutStandingList OutindRAmount : acctList.getOutStandingList()) {
				
					for(OutStandingDetail outStdDebtDetails : OutindRAmount.getOutStandingDetail())
					{
						outStandingDebt = outStdDebtDetails.getOutStandingAmount();
					}
					
				}
				List<com.huawei.cbs.ar.wsservice.arcommon.AcctBalance> balanceResults = acctList.getBalanceResult();
				for (com.huawei.cbs.ar.wsservice.arcommon.AcctBalance acctBalance : balanceResults) {
					totalPayments=acctBalance.getTotalAmount();
				}
					postPaidBalanceData=new Postpaid(mainBalance+"",creditLimit+"",totalPayments+"",outStandingDebt+"");
				}
			
			NumberFormat formatter = new DecimalFormat("#0.00");
			formatter.setRoundingMode(RoundingMode.FLOOR);

			
			postPaidBalanceData.setBalanceIndividualValue(Helper.getBakcellMoneyCeilingMode(
					Long.parseLong(postPaidBalanceData.getBalanceIndividualValue())));
			
			postPaidBalanceData.setCurrentCreditLimit(
					Helper.getBakcellMoneyCeilingMode(Long.parseLong(postPaidBalanceData.getCurrentCreditLimit())));
			
			postPaidBalanceData.setTotalPayments(Helper
					.getBakcellMoneyCeilingMode(Long.parseLong(postPaidBalanceData.getTotalPayments())));
			
			
			
			
			if (Double.parseDouble(postPaidBalanceData.getOutstandingIndividualDept()) > 0.00) {
				postPaidBalanceData.setOutstandingIndividualDept("-" + Helper.getBakcellMoneyCeilingMode(
						Long.parseLong(postPaidBalanceData.getOutstandingIndividualDept())));
			} else {
				postPaidBalanceData.setOutstandingIndividualDept(Helper.getBakcellMoneyCeilingMode(
						Long.parseLong(postPaidBalanceData.getOutstandingIndividualDept())));
			}
			
			finalBalanceResponse.setPostpaid(postPaidBalanceData);
		} else if (userType.equals(Constants.PREPAID)) {
			Prepaid prepaidBalanceData = new Prepaid();
			String lowerLimit = ConfigurationManager
					.getConfigurationFromCache(ConfigurationManager.MAPPING_BALANCE_LOWER_LIMIT);
			BounusWallet bounusWallet = new BounusWallet();
			long bonusAmount=0L;
			for (AcctList acctList : balanceResponse) {
				List<com.huawei.cbs.ar.wsservice.arcommon.AcctBalance> balanceResults = acctList.getBalanceResult();
				for (com.huawei.cbs.ar.wsservice.arcommon.AcctBalance acctBalance : balanceResults) {
					try {
						logger.info("AccountType Azerfon----------------  " + acctBalance.getBalanceType());
						if (!ConfigurationManager.getContainsValueByKey(
								ConfigurationManager.MAPPING_BALANCE_MAIN + acctBalance.getBalanceType())) {
							continue;
						}
						String array[] = ConfigurationManager
								.getConfigurationFromCache(
										ConfigurationManager.MAPPING_BALANCE_MAIN + acctBalance.getBalanceType())
								.split(",");
						String name = array[0];
						NumberFormat df = new DecimalFormat("#0.00");
						df.setRoundingMode(RoundingMode.FLOOR);
						String currencyId=acctBalance.getCurrencyID().toString();
						logger.info("AccountType Azerfon----------------  " + name);
						if (name.equals(Constants.BONUS_WALLET))
						{
							logger.info("-----------------Bonus Balance----------------  ");
							logger.info("name ----------------  "+name);
							logger.info("Currncy----------------  "+ConfigurationManager.getConfigurationFromCache(currencyId));
							logger.info("amount ----------------  "+"" + df.format((acctBalance.getTotalAmount())/ Constants.MONEY_DIVIDEND));
							logger.info("Time----------------  "+acctBalance.getBalanceDetail().get(0).getEffectiveTime());
							logger.info("Expire time----------------  "+acctBalance.getBalanceDetail().get(0).getExpireTime());
							logger.info("-----------------Bonus Balance----------------  ");
							bonusAmount = bonusAmount+acctBalance.getTotalAmount();
							bounusWallet.setEffectiveTime(acctBalance.getBalanceDetail().get(0).getEffectiveTime());
							bounusWallet.setExpireTime(acctBalance.getBalanceDetail().get(0).getExpireTime());
							bounusWallet.setCurrency(currencyId);
//							prepaidBalanceData.setBounusWallet(new BounusWallet(
//									name,
//									ConfigurationManager.getConfigurationFromCache(currencyId),
//									"" + df.format((acctBalance.getTotalAmount())/ Constants.MONEY_DIVIDEND),
//									acctBalance.getBalanceDetail().get(0).getEffectiveTime(),
//									acctBalance.getBalanceDetail().get(0).getExpireTime(), 
//									lowerLimit));
						}
						else if (name.equals(Constants.COUNTRY_WIDE))
							prepaidBalanceData.setCountryWideWallet(new CountryWideWallet(name,
									ConfigurationManager.getConfigurationFromCache(currencyId),
									"" + df.format((acctBalance.getTotalAmount())/ Constants.MONEY_DIVIDEND),
									acctBalance.getBalanceDetail().get(0).getEffectiveTime(),
									acctBalance.getBalanceDetail().get(0).getExpireTime(), lowerLimit));
						else if (name.equals(Constants.MAIN_WALLET))
							prepaidBalanceData.setMainWallet(new MainWallet(name,
									ConfigurationManager.getConfigurationFromCache(acctBalance.getCurrencyID().toString()),
									Helper.getBakcellMoney((acctBalance.getTotalAmount())),
									acctBalance.getBalanceDetail().get(0).getEffectiveTime(),
									acctBalance.getBalanceDetail().get(0).getExpireTime(), lowerLimit));
					} catch (Exception ex) {
						logger.error(Helper.GetException(ex));
					}
				}
			}
			////adding all the bonus values for dashboard
			
			double a = (double)bonusAmount/ (double)Constants.MONEY_DIVIDEND;
			
//		    double roundOff = Math.round(a*100)/100;

		    double roundOff = (double) Math.round(a * 100) / 100;
		    
			bounusWallet.setAmount("" +roundOff);
			prepaidBalanceData.setBounusWallet(bounusWallet);
			finalBalanceResponse.setPrepaid(prepaidBalanceData);
		}
		return finalBalanceResponse;
	}

	/*public boolean isCorporate(String msisdn) {
		QueryBalanceRequest qBR = new QueryBalanceRequest();
		qBR.setReqHeader(getReqHeaderForQueryBalanceHLR());
		QueryBalanceIn qBI = new QueryBalanceIn();
		qBI.setPrimaryIdentity(msisdn);
		qBR.setQueryBalanceBody(qBI);
		QueryBalanceResponse res = HLRWebService.getInstance().queryBalance(qBR);
		if (res.getRspHeader().getReturnCode().equals("0")) {
			try {
				for (AcctList iterable_element : res.getQueryBalanceBody()) {
					if (!iterable_element.getCorPayRelaList().isEmpty()) {
						return true;
					}
				}
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
			}
		}
		return false;
	}*/
	
	
/*	public QueryBalanceResponse getQueryBalanceResponse(String msisdn) {
		QueryBalanceRequest qBR = new QueryBalanceRequest();
		qBR.setReqHeader(getReqHeaderForQueryBalanceHLR());
		QueryBalanceIn qBI = new QueryBalanceIn();
		qBI.setPrimaryIdentity(msisdn);
		qBR.setQueryBalanceBody(qBI);
		QueryBalanceResponse res = HLRWebService.getInstance().queryBalance(qBR);
		if (res.getRspHeader().getReturnCode().equals("0")) {
			try {
				for (AcctList iterable_element : res.getQueryBalanceBody()) {
					if (!iterable_element.getCorPayRelaList().isEmpty()) {
						return true;
					}
				}
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
			}
		}
		return false;
		return res;
		
		
	}*/
	

	public Balance getBalance(String msisdn, String userType,QueryBalanceResultMsg balanceResponse,String token) throws IOException, Exception {
		logger.info("MSISDN IS:" + msisdn + " User type is: " + userType);
		if(balanceResponse==null)
		{
			QueryBalanceResultMsg res=AzerfonThirdPartyCalls.queryBalanceResponseArServices(msisdn,token);
			balanceResponse = res;
		}
		QueryBalanceResultMsg res = balanceResponse;
		Balance finalBalance = new Balance();
		if (res.getResultHeader().getResultCode().equals("0")) {
			finalBalance = processBalanceResponse(msisdn, userType, res.getQueryBalanceResult().getAcctList());
		}
		finalBalance.setReturnCode(res.getResultHeader().getResultCode());
		finalBalance.setReturnMsg(res.getResultHeader().getResultDesc());
		return finalBalance;
	}
	
	
	
	public static void main(String arg[])
	{
		
		double a = (double)1172004/ (double)Constants.MONEY_DIVIDEND;
		System.out.println(a);
//	    double roundOff = Math.round(a*100)/100;

	    double roundOff = (double) Math.round(a * 100) / 100;
	    String value=""+roundOff;
	    System.out.println(value);
	}



}
