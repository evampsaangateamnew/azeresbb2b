package com.evampsaanga.b2b.azerfon.hlrweb.getSubscriberRequest;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import javax.crypto.NoSuchPaddingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.ws.WebServiceException;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.developer.utils.Decrypter;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.huawei.bss.soaif._interface.common.ReqHeader;
import com.huawei.bss.soaif._interface.hlrwebservice.GetSubscriberIn;
import com.huawei.bss.soaif._interface.hlrwebservice.GetSubscriberRequest;
import com.huawei.bss.soaif._interface.hlrwebservice.HLRWebService;

@Path("/bakcell")
public class GetSubscriberLand {
	public static final Logger logger = Logger.getLogger("azerfon-esb");

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetSubscriberResponseClient Get(@Header("credentials") String credential, GetSubscriberRequestClient cclient)
			throws InvalidKeyException, NoSuchAlgorithmException, UnsupportedEncodingException, InvalidKeySpecException,
			NoSuchPaddingException {
		String credentials = null;
		if (cclient != null) {
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
			}
			if (credentials == null) {
				GetSubscriberResponseClient response = new GetSubscriberResponseClient();
				response.setReturnCode(ResponseCodes.ERROR_401_CODE);
				response.setReturnMsg(ResponseCodes.ERROR_401);
				return response;
			}
			String verification = Helper.validateRequest(cclient);
			if (!verification.equals("")) {
				GetSubscriberResponseClient response = new GetSubscriberResponseClient();
				response.setReturnCode(ResponseCodes.ERROR_400);
				response.setReturnMsg(verification);
				return response;
			}
			if (!credentials.equals(Constants.CREDENTIALS)) {
				GetSubscriberResponseClient resp = new GetSubscriberResponseClient();
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				return resp;
			}
			GetSubscriberResponseClient response = new GetSubscriberResponseClient();
			GetSubscriberRequest sub = new GetSubscriberRequest();
			try {
				sub.setReqHeader(getReqHeader());
				GetSubscriberIn subBody = new GetSubscriberIn();
				subBody.setMSISDN(cclient.getmsisdn());
				sub.setGetSubscriberBody(subBody);
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
			}
			try {
				response = new GetSubscriberResponseClient(HLRWebService.getInstance().getSubscriber(sub));
				response.setReturnCode("0");
				response.setReturnMsg("");
			} catch (Exception ee) {
				logger.error(Helper.GetException(ee));
				if (ee instanceof WebServiceException) {
					response.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
					response.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
				} else {
					response.setReturnCode(ResponseCodes.SUCESS_CODE_200);
					response.setReturnMsg(ResponseCodes.SUCESS_DES_200);
				}
			}
			return response;
		}
		return null;
	}

	public com.huawei.bss.soaif._interface.common.ReqHeader getReqHeader() {
		ReqHeader reqHHLR = new ReqHeader();
		reqHHLR = new ReqHeader();
		reqHHLR.setChannelId(ConfigurationManager.getConfigurationFromCache("hlrweb.reqh.ChannelId").trim());
		reqHHLR.setAccessUser(ConfigurationManager.getConfigurationFromCache("hlrweb.reqh.AccessUser").trim());
		reqHHLR.setAccessPwd(ConfigurationManager.getConfigurationFromCache("hlrweb.reqh.AccessPwd").trim());
		reqHHLR.setMIDWAREChannelID(ConfigurationManager.getConfigurationFromCache("hlrweb.reqh.MIDWAREChannelID").trim());
		reqHHLR.setMIDWAREAccessPwd(ConfigurationManager.getConfigurationFromCache("hlrweb.reqh.MIDWAREAccessPwd").trim());
		reqHHLR.setMIDWAREAccessUser(ConfigurationManager.getConfigurationFromCache("hlrweb.reqh.MIDWAREAccessUser").trim());
		reqHHLR.setTransactionId(Helper.generateTransactionID());
		return reqHHLR;
	}
}
