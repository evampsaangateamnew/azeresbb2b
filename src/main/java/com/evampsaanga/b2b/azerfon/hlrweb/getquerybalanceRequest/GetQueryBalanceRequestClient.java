package com.evampsaanga.b2b.azerfon.hlrweb.getquerybalanceRequest;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.ALWAYS)
public class GetQueryBalanceRequestClient extends com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest {
	public GetQueryBalanceRequestClient() {
		super();
	}

	@JsonProperty("customerType")
	private String customerType = "";

	/**
	 * @return the customerType
	 */
	public String getCustomerType() {
		return customerType;
	}

	/**
	 * @param customerType
	 *            the customerType to set
	 */
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
}
