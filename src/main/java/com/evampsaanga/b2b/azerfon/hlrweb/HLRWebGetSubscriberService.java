package com.evampsaanga.b2b.azerfon.hlrweb;

import org.apache.log4j.Logger;

import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.huawei.bss.soaif._interface.common.ReqHeader;
import com.huawei.bss.soaif._interface.hlrwebservice.GetSubscriberIn;
import com.huawei.bss.soaif._interface.hlrwebservice.GetSubscriberRequest;
import com.huawei.bss.soaif._interface.hlrwebservice.GetSubscriberResponse;
import com.huawei.bss.soaif._interface.hlrwebservice.HLRWebService;

public class HLRWebGetSubscriberService {
	public static final Logger logger = Logger.getLogger("azerfon-esb");

	public GetSubscriberResponse getSubscriber(String msisdn) {
		GetSubscriberRequest sub = new GetSubscriberRequest();
		try {
			sub.setReqHeader(getReqHeader());
			GetSubscriberIn subBody = new GetSubscriberIn();
			subBody.setMSISDN(msisdn);
			sub.setGetSubscriberBody(subBody);
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		GetSubscriberResponse obj = HLRWebService.getInstance().getSubscriber(sub);
		return obj;
	}

	private com.huawei.bss.soaif._interface.common.ReqHeader getReqHeader() {
		ReqHeader reqHHLR = null;
		reqHHLR = new ReqHeader();
		reqHHLR.setChannelId(ConfigurationManager.getConfigurationFromCache("hlrweb.reqh.ChannelId").trim());
		reqHHLR.setAccessUser(ConfigurationManager.getConfigurationFromCache("hlrweb.reqh.AccessUser").trim());
		reqHHLR.setAccessPwd(ConfigurationManager.getConfigurationFromCache("hlrweb.reqh.AccessPwd").trim());
		reqHHLR.setMIDWAREChannelID(ConfigurationManager.getConfigurationFromCache("hlrweb.reqh.MIDWAREChannelID").trim());
		reqHHLR.setMIDWAREAccessPwd(ConfigurationManager.getConfigurationFromCache("hlrweb.reqh.MIDWAREAccessPwd").trim());
		reqHHLR.setMIDWAREAccessUser(ConfigurationManager.getConfigurationFromCache("hlrweb.reqh.MIDWAREAccessUser").trim());
		reqHHLR.setTransactionId(Helper.generateTransactionID());
		return reqHHLR;
	}
}
