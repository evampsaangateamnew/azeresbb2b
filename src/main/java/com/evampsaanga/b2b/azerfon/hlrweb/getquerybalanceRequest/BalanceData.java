package com.evampsaanga.b2b.azerfon.hlrweb.getquerybalanceRequest;

public class BalanceData {
	private String balanceExpiry = "";
	private String balanceName = "";
	private String amount = "";
	private String currency = "";
	private String enable = "";
	private String lowerLimit = "";
	private String effectiveTime = "";

	public BalanceData(String balanceExpiry, String balanceName, String amount, String currency, String enable) {
		super();
		this.balanceExpiry = balanceExpiry;
		this.balanceName = balanceName;
		this.amount = amount;
		this.currency = currency;
		this.enable = enable;
	}

	public BalanceData(String balanceExpiry, String balanceName, String amount, String currency, String enable,
			String lowerLimit, String effectiveTime) {
		super();
		this.balanceExpiry = balanceExpiry;
		this.balanceName = balanceName;
		this.amount = amount;
		this.currency = currency;
		this.enable = enable;
		this.lowerLimit = lowerLimit;
		this.effectiveTime = effectiveTime;
	}

	/**
	 * @return the lowerLimit
	 */
	public String getLowerLimit() {
		return lowerLimit;
	}

	/**
	 * @param lowerLimit
	 *            the lowerLimit to set
	 */
	public void setLowerLimit(String lowerLimit) {
		this.lowerLimit = lowerLimit;
	}

	/**
	 * @return the effectiveTime
	 */
	public String getEffectiveTime() {
		return effectiveTime;
	}

	/**
	 * @param effectiveTime
	 *            the effectiveTime to set
	 */
	public void setEffectiveTime(String effectiveTime) {
		this.effectiveTime = effectiveTime;
	}

	/**
	 * @return the balanceExpiry
	 */
	public String getBalanceExpiry() {
		return balanceExpiry;
	}

	/**
	 * @param balanceExpiry
	 *            the balanceExpiry to set
	 */
	public void setBalanceExpiry(String balanceExpiry) {
		this.balanceExpiry = balanceExpiry;
	}

	/**
	 * @return the balanceName
	 */
	public String getBalanceName() {
		return balanceName;
	}

	/**
	 * @param balanceName
	 *            the balanceName to set
	 */
	public void setBalanceName(String balanceName) {
		this.balanceName = balanceName;
	}

	/**
	 * @return the amount
	 */
	public String getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            the amount to set
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency
	 *            the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return the enable
	 */
	public String getEnable() {
		return enable;
	}

	/**
	 * @param enable
	 *            the enable to set
	 */
	public void setEnable(String enable) {
		this.enable = enable;
	}
}
