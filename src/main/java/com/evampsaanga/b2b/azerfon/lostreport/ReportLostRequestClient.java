package com.evampsaanga.b2b.azerfon.lostreport;

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;

public class ReportLostRequestClient extends BaseRequest {
	String reasonCode = "";
	String operationType = "";
	String virtualCorpCode="";

	String type="";
	
	
	public String getVirtualCorpCode() {
		return virtualCorpCode;
	}

	public void setVirtualCorpCode(String virtualCorpCode) {
		this.virtualCorpCode = virtualCorpCode;
	}

	public String getReasonCode() {
		return reasonCode;
	}

	public void setReasonCode(String reasonCode) {
		this.reasonCode = reasonCode;
	}

	public String getOperationType() {
		return operationType;
	}

	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}

	@Override
	public String toString() {
		return "ReportLostRequestClient [reasonCode=" + reasonCode + ", operationType=" + operationType + "]";
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	
}
