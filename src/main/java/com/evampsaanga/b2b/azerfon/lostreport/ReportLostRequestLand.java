package com.evampsaanga.b2b.azerfon.lostreport;

import java.math.RoundingMode;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.azerfon.appserver.refreshappservercache.BuildCacheRequestLand;
import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.azerfon.db.DBFactory;
import com.evampsaanga.b2b.azerfon.getUsersV2.UsersGroupData;
import com.evampsaanga.b2b.azerfon.getUsersV2.UsersGroupRequest;
import com.evampsaanga.b2b.azerfon.grouppermission.Datum;
import com.evampsaanga.b2b.azerfon.grouppermission.GroupPermissionMagentoResponse;
import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Decrypter;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.evampsaanga.b2b.developer.utils.SOAPLoggingHandler;
import com.huawei.bme.cbsinterface.arservices.PaymentResult.OutStandingList.OutStandingDetail;
import com.huawei.bme.cbsinterface.arservices.QueryBalanceRequest;
import com.huawei.bme.cbsinterface.arservices.QueryBalanceRequestMsg;
import com.huawei.bme.cbsinterface.arservices.QueryBalanceResult.AcctList.OutStandingList;
import com.huawei.bme.cbsinterface.arservices.QueryBalanceResultMsg;
import com.huawei.bme.cbsinterface.arservices.QueryBalanceRequest.QueryObj;
import com.huawei.bme.cbsinterface.arservices.QueryBalanceRequest.QueryObj.AcctAccessCode;
import com.huawei.crm.basetype.ens.OrderInfo;
import com.huawei.crm.basetype.ens.OrderItemInfo;
import com.huawei.crm.basetype.ens.OrderItemValue;
import com.huawei.crm.basetype.ens.OrderItems;
import com.huawei.crm.basetype.ens.SubscriberInfo;
import com.huawei.crm.service.ens.SubmitOrderRequest;
import com.huawei.crm.service.ens.SubmitOrderResponse;
import com.huawei.crm.service.ens.SubmitRequestBody;
import com.ngbss.evampsaanga.services.ThirdPartyCall;
import com.ngbss.evampsaanga.services.ThirdPartyRequestHeader;

@Path("/azerfon")
public class ReportLostRequestLand {
	public static final Logger logger = Logger.getLogger("azerfon-esb");

	@POST
	@Path("/getV2")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ReportLostResponseClient Get(@Header("credentials") String credential,
			@Header("Content-Type") String contentType, @Body() String requestBody) {
		ReportLostResponseClient resp = new ReportLostResponseClient();
		Logs logs = new Logs();
		try {
			String token;
			logger.info("Request Landed on Lost Report:" + requestBody);
			ReportLostRequestClient cclient = null;
			try {
				String TrnsactionName = Transactions.REPORT_LOST_SIM_TRANSACTION_NAME + " "
						+ Transactions.LOGIN_TRANSACTION_NAME;
				token = Helper.retrieveToken(TrnsactionName, Helper.getValueFromJSON(requestBody, "msisdn"));
				cclient = Helper.JsonToObject(requestBody, ReportLostRequestClient.class);
				// logger.info("<<<<<<<<<<< Request packet >>>>>>>>>>>" +
				// cclient.toString());
				if (cclient != null) {
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
					logs.setLang(cclient.getLang());
				}
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						resp.setReturnCode(ResponseCodes.ERROR_400);
						resp.setReturnMsg(verification);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				} else {
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					if (cclient.getReasonCode().equals("1")) {

						// //TESTING PURPOSE
						// OutStandingList e =new OutStandingList();
						// e.setBillCycleBeginTime("test");
						// resultMsg.getQueryBalanceResult().getAcctList().get(0).getOutStandingList().add(e);
						//
						// com.huawei.bme.cbsinterface.arservices.QueryBalanceResult.AcctList.OutStandingList.OutStandingDetail
						// a = new
						// com.huawei.bme.cbsinterface.arservices.QueryBalanceResult.AcctList.OutStandingList.OutStandingDetail()
						// ;
						// a.setOutStandingAmount(0);
						//
						// com.huawei.bme.cbsinterface.arservices.QueryBalanceResult.AcctList.OutStandingList.OutStandingDetail
						// b = new
						// com.huawei.bme.cbsinterface.arservices.QueryBalanceResult.AcctList.OutStandingList.OutStandingDetail()
						// ;
						// b.setOutStandingAmount(7);
						//
						// resultMsg.getQueryBalanceResult().getAcctList().get(0).getOutStandingList().get(0).getOutStandingDetail().add(a);
						// resultMsg.getQueryBalanceResult().getAcctList().get(0).getOutStandingList().get(0).getOutStandingDetail().add(b);
						//

						logger.info("Calling groupDataCall Method");
						GroupPermissionMagentoResponse restrictionResponse = Helper.groupDataCall(cclient.getmsisdn(),
								token, logger, null);
						int count = 0;
						if (restrictionResponse != null) {
							if (restrictionResponse.getData().isEmpty()) {
								// IF it is empty it means no restrictions
								count++;
							}

							else {
								for (Datum item : restrictionResponse.getData()) {
									if (item.getSuspendNumber()!=null && item.getSuspendNumber().equalsIgnoreCase("0"))
										count++;
									break;
								}
							}

							logger.info(token + " Count of Suspend Restriction is ::::" + count);
							if (count > 0) {

								logger.info(token + " User is allowed to Suspend  ::::" );
								logger.info(token + "Calling checkUserGroup ");
								String groupName = checkUserGroup(cclient);

								if (groupName.equalsIgnoreCase("PayBySubs"))

								{
									logger.info("Calling Query Balance");
									QueryBalanceRequestMsg queryBalanceRequestMsg = new QueryBalanceRequestMsg();
									QueryBalanceRequest BalanceRequest = new QueryBalanceRequest();
									QueryObj QueryObj = new QueryObj();
									AcctAccessCode accessCode = new AcctAccessCode();
									logger.info("#################################################3");
									logger.info("Testing:" + cclient.getmsisdn());
									logger.info("#################################################3");
									accessCode.setPrimaryIdentity(cclient.getmsisdn());
									QueryObj.setAcctAccessCode(accessCode);
									BalanceRequest.setQueryObj(QueryObj);
									queryBalanceRequestMsg.setQueryBalanceRequest(BalanceRequest);
									queryBalanceRequestMsg
											.setRequestHeader(ThirdPartyRequestHeader.getRequestPaymentRequestHeader());

									QueryBalanceResultMsg resultMsg = ThirdPartyCall
											.getQueryBalance(queryBalanceRequestMsg);

									Long remainingAmount = resultMsg.getQueryBalanceResult().getAcctList().get(0)
											.getAccountCredit().get(0).getTotalRemainAmount();
									NumberFormat df = new DecimalFormat("#0.00");
									df.setRoundingMode(RoundingMode.FLOOR);
									String remainingAmountstr = df
											.format(remainingAmount.longValue() / Constants.MONEY_DIVIDEND);
									double remainingAmountInt = Double.parseDouble(remainingAmountstr);

									logger.info(token + ResponseCodes.REPORT_LOST_SIM_NEGATIVE_BALANCE_MSG
											+ remainingAmountInt);

									logger.info(token + "Testing:::::: ");

									if (remainingAmountInt >= 0) {
										boolean isOutStandingBalanceCheck = false;
										if (resultMsg.getQueryBalanceResult().getAcctList() != null
												&& !resultMsg.getQueryBalanceResult().getAcctList().isEmpty()
												&& resultMsg.getQueryBalanceResult().getAcctList().get(0)
														.getOutStandingList() != null
												&& !resultMsg.getQueryBalanceResult().getAcctList().get(0)
														.getOutStandingList().isEmpty()
												&& resultMsg.getQueryBalanceResult().getAcctList().get(0)
														.getOutStandingList().get(0).getOutStandingDetail() != null
												&& !resultMsg.getQueryBalanceResult().getAcctList().get(0)
														.getOutStandingList().get(0).getOutStandingDetail().isEmpty()) {
											logger.info(
													"Entering in IF block because outstanding detail list not empty");
											for (int i = 0; i < resultMsg.getQueryBalanceResult().getAcctList().get(0)
													.getOutStandingList().get(0).getOutStandingDetail().size(); i++) {

												if (resultMsg.getQueryBalanceResult().getAcctList().get(0)
														.getOutStandingList().get(0).getOutStandingDetail().get(i)
														.getOutStandingAmount() > 0) {
													logger.info("Breaking Loop because of outstanding debt value ::"
															+ resultMsg.getQueryBalanceResult().getAcctList().get(0)
																	.getOutStandingList().get(0).getOutStandingDetail()
																	.get(i).getOutStandingAmount()
															+ " on index::" + i);
													isOutStandingBalanceCheck = true;
													break;
												}
											}
										}

										if (isOutStandingBalanceCheck) {
											resp.setReturnCode(ResponseCodes.REPORT_LOST_SIM_OUTSTANDING_BALANCE_CODE);
											resp.setReturnMsg(ResponseCodes.REPORT_LOST_SIM_OUTSTANDING_BALANCE_MSG);
											logs.setResponseCode(resp.getReturnCode());
											logs.setResponseDescription(resp.getReturnMsg());
											logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
											logs.updateLog(logs);
											return resp;
										} else {
											logger.info(token + " Calling Submit Order Third Party Api with msisdn"
													+ cclient.getmsisdn());
											SubmitOrderResponse response = reportlost(cclient.getmsisdn(), "SC999",
													cclient.getOperationType());
											if (response.getResponseHeader().getRetCode().equals("0")) {
												resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
												resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
											} else {
												resp.setReturnCode(response.getResponseHeader().getRetCode());
												resp.setReturnMsg(response.getResponseHeader().getRetMsg());
											}
										}
									}

									else {
										logger.info(token + ResponseCodes.REPORT_LOST_SIM_NEGATIVE_BALANCE_MSG
												+ remainingAmountInt);
										resp.setReturnCode(ResponseCodes.REPORT_LOST_SIM_NEGATIVE_BALANCE_CODE);
										resp.setReturnMsg(ResponseCodes.REPORT_LOST_SIM_NEGATIVE_BALANCE_MSG);
									}

								}

								else if (groupName.equalsIgnoreCase("PartPay")) {
									logger.info(token + " Calling Submit Order Third Party Api with msisdn"
											+ cclient.getmsisdn());
									SubmitOrderResponse response = reportlost(cclient.getmsisdn(), "SC999",
											cclient.getOperationType());
									if (response.getResponseHeader().getRetCode().equals("0")) {
										resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
										resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
									} else {
										resp.setReturnCode(response.getResponseHeader().getRetCode());
										resp.setReturnMsg(response.getResponseHeader().getRetMsg());
									}
								}

								else {
									resp.setReturnCode(ResponseCodes.REPORT_LOST_SIM_GROUP_NAME_CODE);
									resp.setReturnMsg(ResponseCodes.REPORT_LOST_SIM_GROUP_NAME_CODE_MSG);
								}

							}

							else {
								resp.setReturnCode(ResponseCodes.REPORT_LOST_SIM_RESTRICTION_CODE);
								resp.setReturnMsg(ResponseCodes.REPORT_LOST_SIM_RESTRICTION_MSG);
							}

						}

						else {
							resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
							resp.setReturnMsg(ResponseCodes.THIRD_PARTY_FAILURE);
						}

					}

					else {
						resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
						resp.setReturnMsg(ResponseCodes.REPORT_LOST_SIM_INVALID_CODE_MSG);
					}
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				} else {
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(resp.getReturnCode());
		logs.setResponseDescription(resp.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	public SubmitOrderResponse reportlost(String msisdn, String reasoncode, String OperationType) {
		SubmitOrderRequest submitOrderRequestMsgReq = new SubmitOrderRequest();
		// submitOrderRequestMsgReq.setRequestHeader(OrderHandleService.getReqHeaderForReportLost());
		submitOrderRequestMsgReq.setRequestHeader(ThirdPartyRequestHeader.getReqHeaderForReportLost());
		submitOrderRequestMsgReq.getRequestHeader().setAccessPwd("Abc1234%");
		SubmitRequestBody submitRequestBody = new SubmitRequestBody();
		OrderInfo order = new OrderInfo();
		order.setOrderType(Constants.RL_ORDER_ITEM);
		submitRequestBody.setOrder(order);
		OrderItems orderItems = new OrderItems();
		OrderItemValue OrderItemValue = new OrderItemValue();
		OrderItemInfo ordertieminfo = new OrderItemInfo();
		ordertieminfo.setOrderItemType(Constants.RL_ORDER_ITEM_TYPE);
		ordertieminfo.setReasonCode(Constants.RL_REASON_CODE);
		ordertieminfo.setReasonType(Constants.RL_REASON_TYPE);
		OrderItemValue.setOrderItemInfo(ordertieminfo);
		SubscriberInfo subscriber = new SubscriberInfo();
		subscriber.setServiceNumber(msisdn);
		subscriber.setOperateType(Constants.RL_OPERATOR_TYPE);
		OrderItemValue.setSubscriber(subscriber);
		orderItems.getOrderItem().add(OrderItemValue);
		submitRequestBody.setOrderItems(orderItems);
		submitOrderRequestMsgReq.setSubmitRequestBody(submitRequestBody);
		// SubmitOrderResponse response =
		// OrderHandleService.getInstance().submitOrder(submitOrderRequestMsgReq);
		SubmitOrderResponse response = ThirdPartyCall.submitOrder(submitOrderRequestMsgReq);
		return response;
	}

	public String checkUserGroup(ReportLostRequestClient cclient) {

		String TrnsactionName = Transactions.REPORT_LOST_SIM_TRANSACTION_NAME + " "
				+ Transactions.LOGIN_TRANSACTION_NAME;
		String token = Helper.retrieveToken(TrnsactionName, cclient.getmsisdn());

		logger.info(token + "Landed in checkUserGroup Method ");
		String groupname = "";
		if (BuildCacheRequestLand.usersCache.containsKey(cclient.getVirtualCorpCode() + "." + cclient.getLang())) {
			Helper.logInfoMessageV2(
					token + "User with virtualCorpCode " + cclient.getVirtualCorpCode() + " is found in Cache");
			// return BuildCacheRequestLand.usersCache.get(virtualCorpCode + "."
			// + usersGroupRequest.getLang());

			List<UsersGroupData> list = BuildCacheRequestLand.usersCache
					.get(cclient.getVirtualCorpCode() + "." + cclient.getLang());

			Helper.logInfoMessageV2(token + "List size against virtual corp code" + cclient.getVirtualCorpCode()
					+ "is :::" + list.size());

			for (UsersGroupData item : list) {
				if (cclient.getmsisdn().equalsIgnoreCase(item.getMsisdn())) {
					groupname = item.getGroupName();
					break;
				}
			}

		}

		else

		{
			Helper.logInfoMessageV2(token + "checkUsersFromDb:  with virtualCorpCode: " + cclient.getVirtualCorpCode());
			List<UsersGroupData> list = getUsersGroupDataDB(cclient, token);
			Helper.logInfoMessageV2(token + "List size against virtual corp code" + cclient.getVirtualCorpCode()
					+ "is :::" + list.size());
			BuildCacheRequestLand.usersCache.put(cclient.getVirtualCorpCode() + "." + cclient.getLang(), list);

			for (UsersGroupData item : list) {
				if (cclient.getmsisdn().equalsIgnoreCase(item.getMsisdn())) {
					groupname = item.getGroupName();
					break;
				}
			}
		}
		Helper.logInfoMessageV2(token + "Returning GroupName ::::" + groupname);
		return groupname;
	}

	private List<UsersGroupData> getUsersGroupDataDB(ReportLostRequestClient usersGroupRequest, String token) {
		List<UsersGroupData> usersData = new ArrayList<UsersGroupData>();
		// MagentoServices services = new MagentoServices();
		// printing DB Data
		// services.printDB();

		try {

			// int
			// limitRecords=Integer.parseInt(usersGroupRequest.getLimitUsers());
			// Helper.logInfoMessageV2(token+ "Limit record Value :
			// "+limitRecords);
			// int limits=
			// (Integer.parseInt(usersGroupRequest.getPageNum())-1)*limitRecords;
			// Helper.logInfoMessageV2(token+ "Limit alculated record Value :
			// "+limits);

			String sqlQuery = "SELECT * FROM user_data WHERE VIRTUAL_CORP_CODE='"
					+ usersGroupRequest.getVirtualCorpCode() + "'";
			Helper.logInfoMessageV2(token + " --   SQL Query @@GetusersGroupData is :" + sqlQuery);
			ResultSet rs = DBFactory.getDbConnection().prepareStatement(sqlQuery).executeQuery();
			/*
			 * ResultSetMetaData metadata = rs.getMetaData(); int columnCount =
			 * metadata.getColumnCount(); for (int i = 1; i <= columnCount; i++)
			 * { Helper.logInfoMessage(metadata.getColumnName(i) + ", "); }
			 */

			String row = "";
			while (rs.next()) {
				Helper.logInfoMessageV2(token + " SQL Query Result :" + row);
				UsersGroupData users = new UsersGroupData();
				users.setMsisdn(rs.getString("MSISDN"));
				users.setTariffPlan(rs.getString("TARIFF_PLAN"));
				users.setCrmSubId(rs.getString("CRM_SUB_ID"));
				users.setCrmCustId(rs.getString("CRM_CUST_ID"));
				users.setCrmCustCode(rs.getString("CUST_CODE"));
				users.setVirtualCorpCode(rs.getString("VIRTUAL_CORP_CODE"));
				users.setCrmCorpCustId(rs.getString("CRM_CORP_CUST_ID"));
				users.setCugGroupCode(rs.getString("CUG_GROUP_CODE"));
				users.setGroupCode(rs.getString("GROUP_CODE"));
				users.setCrmAccountId(rs.getString("CRM_ACCT_ID"));
				users.setAccCode(rs.getString("ACCT_CODE"));
				users.setCrmAccIdPaid(rs.getString("CRM_ACCT_ID_PAID"));
				users.setTariffNameDisplay(ConfigurationManager.getConfigurationFromCache(
						"tariff.name." + rs.getString("TARIFF_PLAN") + "." + usersGroupRequest.getLang()));

				users.setGroupName(
						groupName(users.getVirtualCorpCode(), users.getGroupCode(), usersGroupRequest.getType()));
				String disp_name = ConfigurationManager
						.getConfigurationFromCache(ConfigurationManager.MAPPING_GROUPIDTRANS
								+ usersGroupRequest.getLang() + "." + users.getGroupName());
				users.setGroupNameDisplay(disp_name);
				// logger.info("@@@@@@@@@@@@@@@@ users data :" + users);
				usersData.add(users);
			}
			rs.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			Helper.logInfoMessageV2(token + "SQLException", Helper.GetException(e));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Helper.logInfoMessageV2(token + "Exception", Helper.GetException(e));
		}
		return usersData;
	}

	public String groupName(String virtualCorpCode, String groupCode, String type) {
		String groupName = "";

		if (type.equalsIgnoreCase("data")) {
			if (virtualCorpCode != null && !virtualCorpCode.isEmpty() && !virtualCorpCode.equalsIgnoreCase("")
					&& groupCode != null && !groupCode.isEmpty() && !groupCode.equalsIgnoreCase("")) {
				groupName = "PartPay";
			}
		} else if (type.equalsIgnoreCase("voice")) {
			if (virtualCorpCode != null && !virtualCorpCode.isEmpty() && !virtualCorpCode.equalsIgnoreCase("")
					&& groupCode != null && !groupCode.isEmpty() && !groupCode.equalsIgnoreCase("")) {
				groupName = "PartPay";
			} else if (virtualCorpCode != null && !virtualCorpCode.isEmpty() && !virtualCorpCode.equalsIgnoreCase("")
					&& (groupCode == null || groupCode.isEmpty() && groupCode.equalsIgnoreCase(""))) {
				groupName = "PayBySubs";
			}
		}
		return groupName;

	}

}
