package com.evampsaanga.b2b.azerfon.callForwardService;

import com.evampsaanga.b2b.configs.Constants;
import com.huawei.crm.basetype.ens.OfferingExtParameterInfo;
import com.huawei.crm.basetype.ens.OfferingExtParameterList;
import com.huawei.crm.basetype.ens.OfferingInfo;
import com.huawei.crm.basetype.ens.OfferingKey;
import com.huawei.crm.basetype.ens.OrderInfo;
import com.huawei.crm.basetype.ens.OrderItemInfo;
import com.huawei.crm.basetype.ens.OrderItemValue;
import com.huawei.crm.basetype.ens.OrderItems;
import com.huawei.crm.basetype.ens.SubscriberInfo;
import com.huawei.crm.service.ens.SubmitOrderRequest;
import com.huawei.crm.service.ens.SubmitOrderResponse;
import com.huawei.crm.service.ens.SubmitRequestBody;
import com.ngbss.evampsaanga.services.ThirdPartyCall;
import com.ngbss.evampsaanga.services.ThirdPartyRequestHeader;

/**
 * Class makes request to SubmitOrder API
 * 
 * @author EvampSaanga
 *
 */
public class CallForwardService {
    /**
     * Method accepts parameters and sends request to SubmitOrderAPI
     * 
     * @param actionType
     * @param offeringId
     * @param msisdn
     * @param forwardNumber
     * @return
     */
    public SubmitOrderResponse callForwardServiceTOMSISDN(String actionType, String offeringId, String msisdn,
            String forwardNumber) {
        com.huawei.crm.service.ens.SubmitOrderRequest submitOrderRequestMsgReq = new SubmitOrderRequest();
        SubmitRequestBody submitrequestBody = new SubmitRequestBody();
        OrderInfo orderInfo = new OrderInfo();
        orderInfo.setOrderType(Constants.ORDER_TYPE);
        submitrequestBody.setOrder(orderInfo);
        OrderItems orderItems = new OrderItems();
        OrderItemValue orderItem = new OrderItemValue();
        OrderItemInfo orderItemInfo = new OrderItemInfo();
        orderItemInfo.setOrderItemType(Constants.ORDER_ITEM_TYPE);
        orderItemInfo.setIsCustomerNotification(Constants.CUSTOMER_NOTIFICATION);
        orderItemInfo.setIsPartnerNotification(Constants.PARAMETER_NOTIFICATION);
        orderItem.setOrderItemInfo(orderItemInfo);
        SubscriberInfo subscriberInfo = new SubscriberInfo();
        subscriberInfo.setServiceNumber(msisdn);
        OfferingInfo offeringInfo = new OfferingInfo();
        offeringInfo.setActionType(actionType);
        OfferingKey offeringID = new OfferingKey();
        offeringID.setOfferingId(offeringId);
        offeringInfo.setOfferingId(offeringID);
        offeringInfo.setEffectMode(Constants.EFFECTIVE_MODE);
        offeringInfo.setActiveMode(Constants.ACTION_MODE);
        OfferingExtParameterList extParmList = new OfferingExtParameterList();
        OfferingExtParameterInfo offertingExtparamInfo = new OfferingExtParameterInfo();
        offertingExtparamInfo.setParamName(Constants.EXTERNAL_PARAMETER_INFO_PARAM_NAME);
        offertingExtparamInfo.setParamValue(forwardNumber);
        extParmList.getParameterInfo().add(offertingExtparamInfo);
        offeringInfo.setExtParamList(extParmList);
        subscriberInfo.getSupplementaryOfferingList().getOfferingInfo().add(offeringInfo);
        orderItem.setSubscriber(subscriberInfo);
        orderItems.getOrderItem().add(orderItem);
        submitrequestBody.setOrderItems(orderItems);
        submitOrderRequestMsgReq.setSubmitRequestBody(submitrequestBody);
//        submitOrderRequestMsgReq.setRequestHeader(OrderHandleService.getReqHeaderForCallForwarding());
        submitOrderRequestMsgReq.setRequestHeader(ThirdPartyRequestHeader.getReqHeaderForCallForwarding());
//        return OrderHandleService.getInstance().submitOrder(submitOrderRequestMsgReq);
        return ThirdPartyCall.submitOrder(submitOrderRequestMsgReq);
    }
 
    /**
     * Method submits request to SUbmitOrderAPI for I am Back and I called you
     * simultanously
     * 
     * @param actionType
     * @param offeringId
     * @param offeringId1
     * @param msisdn
     * @return
     */
    public SubmitOrderResponse callForwardServiceTOIVR(String actionType, String offeringId, String offeringId1,String offeringId2,
            String msisdn) {
        SubmitOrderRequest submitOrderRequestMsgReq = new SubmitOrderRequest();
        SubmitRequestBody submitrequestBody = new SubmitRequestBody();
        OrderInfo orderInfo = new OrderInfo();
        orderInfo.setOrderType(Constants.ORDER_TYPE);
        submitrequestBody.setOrder(orderInfo);
        OrderItems orderItems = new OrderItems();
        OrderItemValue orderItem = new OrderItemValue();
        OrderItemInfo orderItemInfo = new OrderItemInfo();
        orderItemInfo.setOrderItemType(Constants.ORDER_ITEM_TYPE);
        orderItemInfo.setIsCustomerNotification(Constants.CUSTOMER_NOTIFICATION);
        orderItemInfo.setIsPartnerNotification(Constants.PARAMETER_NOTIFICATION);
        orderItem.setOrderItemInfo(orderItemInfo);
        SubscriberInfo subscriberInfo = new SubscriberInfo();
        subscriberInfo.setServiceNumber(msisdn);
        OfferingInfo offeringInfo = new OfferingInfo();
        offeringInfo.setActionType(actionType);
        OfferingKey offeringID = new OfferingKey();
        offeringID.setOfferingId(offeringId);
        offeringInfo.setOfferingId(offeringID);
        offeringInfo.setEffectMode(Constants.EFFECTIVE_MODE);
        offeringInfo.setActiveMode(Constants.ACTION_MODE);
        subscriberInfo.getSupplementaryOfferingList().getOfferingInfo().add(offeringInfo);
        if (offeringId1 != null && offeringId1.length() > 0) {
            OfferingInfo offeringInfo1 = new OfferingInfo();
            offeringInfo1.setActionType(actionType);
            OfferingKey offeringID1 = new OfferingKey();
            offeringID1.setOfferingId(offeringId1);
            offeringInfo1.setOfferingId(offeringID1);
            offeringInfo1.setEffectMode(Constants.EFFECTIVE_MODE);
            offeringInfo1.setActiveMode(Constants.ACTION_MODE);
            subscriberInfo.getSupplementaryOfferingList().getOfferingInfo().add(offeringInfo1);
        }
        
        if (offeringId2 != null && offeringId2.length() > 0) {
            OfferingInfo offeringInfo2 = new OfferingInfo();
            offeringInfo2.setActionType(actionType);
            OfferingKey offeringID2 = new OfferingKey();
            offeringID2.setOfferingId(offeringId2);
            offeringInfo2.setOfferingId(offeringID2);
            offeringInfo2.setEffectMode(Constants.EFFECTIVE_MODE);
            offeringInfo2.setActiveMode(Constants.ACTION_MODE);
            subscriberInfo.getSupplementaryOfferingList().getOfferingInfo().add(offeringInfo2);
        }
        
        orderItem.setSubscriber(subscriberInfo);
        orderItems.getOrderItem().add(orderItem);
        submitrequestBody.setOrderItems(orderItems);
        submitOrderRequestMsgReq.setSubmitRequestBody(submitrequestBody);
//        submitOrderRequestMsgReq.setRequestHeader(OrderHandleService.getReqHeaderForCallForwarding());
        submitOrderRequestMsgReq.setRequestHeader(ThirdPartyRequestHeader.getReqHeaderForCallForwarding());
        submitOrderRequestMsgReq.getRequestHeader().setAccessPwd("Abc1234%");
//        return OrderHandleService.getInstance().submitOrder(submitOrderRequestMsgReq);
        return ThirdPartyCall.submitOrder(submitOrderRequestMsgReq);
    }
     
    public SubmitOrderResponse callForwardServiceTOIVRIamBusyAdded(String actionType, String offeringId, String offeringId1, String OfferingIDBusy,
            String msisdn) {
        SubmitOrderRequest submitOrderRequestMsgReq = new SubmitOrderRequest();
        SubmitRequestBody submitrequestBody = new SubmitRequestBody();
        OrderInfo orderInfo = new OrderInfo();
        orderInfo.setOrderType(Constants.ORDER_TYPE);
        submitrequestBody.setOrder(orderInfo);
        OrderItems orderItems = new OrderItems();
        OrderItemValue orderItem = new OrderItemValue();
        OrderItemInfo orderItemInfo = new OrderItemInfo();
        orderItemInfo.setOrderItemType(Constants.ORDER_ITEM_TYPE);
        orderItemInfo.setIsCustomerNotification(Constants.CUSTOMER_NOTIFICATION);
        orderItemInfo.setIsPartnerNotification(Constants.PARAMETER_NOTIFICATION);
        orderItem.setOrderItemInfo(orderItemInfo);
        SubscriberInfo subscriberInfo = new SubscriberInfo();
        subscriberInfo.setServiceNumber(msisdn);
        OfferingInfo offeringInfo = new OfferingInfo();
        offeringInfo.setActionType(actionType);
        OfferingKey offeringID = new OfferingKey();
        offeringID.setOfferingId(offeringId);
        offeringInfo.setOfferingId(offeringID);
        offeringInfo.setEffectMode(Constants.EFFECTIVE_MODE);
        offeringInfo.setActiveMode(Constants.ACTION_MODE);
        subscriberInfo.getSupplementaryOfferingList().getOfferingInfo().add(offeringInfo);
        if (offeringId1 != null && offeringId1.length() > 0) {
            OfferingInfo offeringInfo1 = new OfferingInfo();
            offeringInfo1.setActionType(actionType);
            OfferingKey offeringID1 = new OfferingKey();
            offeringID1.setOfferingId(offeringId1);
            offeringInfo1.setOfferingId(offeringID1);
            offeringInfo1.setEffectMode(Constants.EFFECTIVE_MODE);
            offeringInfo1.setActiveMode(Constants.ACTION_MODE);
            subscriberInfo.getSupplementaryOfferingList().getOfferingInfo().add(offeringInfo1);
        }
         
        if (OfferingIDBusy != null && OfferingIDBusy.length() > 0) {
            OfferingInfo offeringInfo_busy = new OfferingInfo();
            offeringInfo_busy.setActionType(actionType);
            OfferingKey offeringIDBusyObj = new OfferingKey();
            offeringIDBusyObj.setOfferingId(OfferingIDBusy);
            offeringInfo_busy.setOfferingId(offeringIDBusyObj);
            offeringInfo_busy.setEffectMode(Constants.EFFECTIVE_MODE);
            offeringInfo_busy.setActiveMode(Constants.ACTION_MODE);
            subscriberInfo.getSupplementaryOfferingList().getOfferingInfo().add(offeringInfo_busy);
        }
         
        orderItem.setSubscriber(subscriberInfo);
        orderItems.getOrderItem().add(orderItem);
        submitrequestBody.setOrderItems(orderItems);
        submitOrderRequestMsgReq.setSubmitRequestBody(submitrequestBody);
//        submitOrderRequestMsgReq.setRequestHeader(OrderHandleService.getReqHeaderForCallForwarding());
        submitOrderRequestMsgReq.setRequestHeader(ThirdPartyRequestHeader.getReqHeaderForCallForwarding());
//        return OrderHandleService.getInstance().submitOrder(submitOrderRequestMsgReq);
        return ThirdPartyCall.submitOrder(submitOrderRequestMsgReq);
    }
}