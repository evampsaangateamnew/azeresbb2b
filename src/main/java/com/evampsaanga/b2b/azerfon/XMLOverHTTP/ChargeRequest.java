package com.evampsaanga.b2b.azerfon.XMLOverHTTP;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.evampsaanga.b2b.configs.Constants;

@XmlRootElement(name = "ChargeRequest")
public class ChargeRequest {
	private String vaspName = "LOAN_CIN";
	private String tariffId = "Tariff915";
	private String serviceID = "11";
	private String transactId = "";
	private String ContentId = "Loan_request_CIN";
	private String vaspId = "100001";
	private String mSISDN = "";

	@XmlElement(name = "VaspName")
	public String getVaspName() {
		return vaspName;
	}

	public void setVaspName(String vaspName) {
		this.vaspName = vaspName;
	}

	@XmlElement(name = "TariffId")
	public String getTariffId() {
		return tariffId;
	}

	public void setTariffId(String tariffId) {
		this.tariffId = tariffId;
	}

	@XmlElement(name = "ServiceID")
	public String getServiceID() {
		return serviceID;
	}

	public void setServiceID(String serviceID) {
		this.serviceID = serviceID;
	}

	@XmlElement(name = "TransactId")
	public String getTransactId() {
		return transactId;
	}

	public void setTransactId(String transactId) {
		this.transactId = transactId;
	}

	@XmlElement(name = "ContentId")
	public String getContentId() {
		return ContentId;
	}

	public void setContentId(String contentId) {
		this.ContentId = contentId;
	}

	@XmlElement(name = "VaspId")
	public String getVaspId() {
		return vaspId;
	}

	public void setVaspId(String vaspId) {
		this.vaspId = vaspId;
	}

	@XmlElement(name = "MSISDN")
	public String getmSISDN() {
		return mSISDN;
	}

	public void setmSISDN(String mSISDN) {
		if (mSISDN != null && !mSISDN.startsWith(Constants.AZERI_COUNTRY_CODE))
			mSISDN = Constants.AZERI_COUNTRY_CODE + mSISDN;
		this.mSISDN = mSISDN;
	}
}