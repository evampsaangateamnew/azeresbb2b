package com.evampsaanga.b2b.azerfon.XMLOverHTTP;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import org.apache.log4j.Logger;

public class RestClient {
	public static final Logger logger = Logger.getLogger("azerfon-esb");

	public String getResponseFromESB(String url, String postData) {
		String line = "";
		String esbResponse = "";
		try {
			logger.debug("Connecting..." + url);
			URL url_1 = new URL(url);
			logger.debug("Connected...");
			HttpURLConnection conn = (HttpURLConnection) url_1.openConnection();
			logger.debug("Connection Opened...");
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/xml");
			logger.debug("Sending Data...");
			OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
			writer.write(postData);
			writer.flush();
			logger.debug("Data Sent...");
			BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			while ((line = reader.readLine()) != null) {
				esbResponse = esbResponse + line;
			}
			writer.close();
			reader.close();
			conn.disconnect();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return esbResponse;
	}
}