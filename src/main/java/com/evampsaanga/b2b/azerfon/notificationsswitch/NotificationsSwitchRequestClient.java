package com.evampsaanga.b2b.azerfon.notificationsswitch;

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;

public class NotificationsSwitchRequestClient extends BaseRequest {
	String enable = "";

	public String getEnable() {
		return enable;
	}

	public void setEnable(String enable) {
		this.enable = enable;
	}
}
