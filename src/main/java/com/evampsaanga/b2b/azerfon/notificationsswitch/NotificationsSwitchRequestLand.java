package com.evampsaanga.b2b.azerfon.notificationsswitch;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.azerfon.db.DBFactory;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.developer.utils.Decrypter;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.evampsaanga.b2b.developer.utils.SOAPLoggingHandler;

@Path("/azerfon")
public class NotificationsSwitchRequestLand {
	public static final Logger logger = Logger.getLogger("azerfon-esb");
	Logs logs = new Logs();

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public NotificationsSwitchResponseClient Get(@Header("credentials") String credential,
			@Header("Content-Type") String contentType, @Body() String requestBody) {
		try {
			logger.info("Request Landed on NotificationsSwitchRequestLand:" + requestBody);
			NotificationsSwitchRequestClient cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, NotificationsSwitchRequestClient.class);
				if (cclient != null) {
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
				}
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				NotificationsSwitchResponseClient resp = new NotificationsSwitchResponseClient();
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				return resp;
			}
			if (cclient != null) {

				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					NotificationsSwitchResponseClient resp = new NotificationsSwitchResponseClient();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						NotificationsSwitchResponseClient res = new NotificationsSwitchResponseClient();
						res.setReturnCode(ResponseCodes.ERROR_400);
						res.setReturnMsg(verification);
						logs.setResponseCode(res.getReturnCode());
						logs.setResponseDescription(res.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						return res;
					}
				} else {
					NotificationsSwitchResponseClient resp = new NotificationsSwitchResponseClient();
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					return resp;
				}
				if (credentials.equals(Constants.CREDENTIALS)) {
					NotificationsSwitchResponseClient resp = new NotificationsSwitchResponseClient();
					resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
					resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
					return getResponse(cclient.getmsisdn(), cclient.getEnable(), logs);
				} else {
					NotificationsSwitchResponseClient resp = new NotificationsSwitchResponseClient();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		NotificationsSwitchResponseClient resp = new NotificationsSwitchResponseClient();
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(resp.getReturnCode());
		logs.setResponseDescription(resp.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		return resp;
	}

	public NotificationsSwitchResponseClient getResponse(String msisdn, String isenablestr, Logs logs)
			throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
		String sql1 = "UPDATE FCM_IDs SET is_enable=? where msisdn=? ";
		Connection myConnection = DBFactory.getAppConnection();
		try {
			PreparedStatement statement1 = myConnection.prepareStatement(sql1);
			try {
				int isenable = Integer.parseInt(isenablestr);
				statement1.setInt(1, isenable);
				statement1.setString(2, msisdn);
				statement1.executeUpdate();
				statement1.close();
			} catch (Exception e) {
				logger.error("Update History Exception  " + Helper.GetException(e));
			}
			NotificationsSwitchResponseClient rep11 = new NotificationsSwitchResponseClient();
			rep11.setReturnCode(ResponseCodes.SUCESS_CODE_200);
			rep11.setReturnMsg(ResponseCodes.SUCESS_DES_200);
			logs.setResponseCode(rep11.getReturnCode());
			logs.setResponseDescription(rep11.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			return rep11;
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		NotificationsSwitchResponseClient resp = new NotificationsSwitchResponseClient();
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(resp.getReturnCode());
		logs.setResponseDescription(resp.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		return resp;
	}
}
