package com.evampsaanga.b2b.azerfon.internetsettings;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.amqimplementationsesb.LogsType;
import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.ThirdPartyNames;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Decrypter;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.ngbss.evampsaanga.services.ThirdPartyCall;
import com.nsn.devicemanagement.wsdl.common._2.DmMap;
import com.nsn.devicemanagement.wsdl.common._2.DmMap.HashTable;
import com.nsn.devicemanagement.wsdl.provisioning._2.DMException;

@Path("/azerfon/")
public class InternetSettingsRequestLand {
	public static final Logger logger = Logger.getLogger("azerfon-esb");

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public InternetSettingsResponse Get(@Header("credentials") String credential,
			@Header("Content-Type") String contentType, @Body() String requestBody) {
		InternetSettingsResponse resp = new InternetSettingsResponse();
		Logs logs = new Logs();

		logs.setTransactionName(Transactions.SEND_INTERNET_SETTINGS);
		logs.setThirdPartyName(ThirdPartyNames.SEND_INTERNET_SETTINGS);
		logs.setTableType(LogsType.GetInternetSettings);
		try {
			logger.info("Request Landed on InternetSettingsRequestLand:" + requestBody);
			InternetSettingsRequest cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, InternetSettingsRequest.class);
				if (cclient != null) {
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
					logs.setLang(cclient.getLang());
				}
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
						resp.setReturnMsg(verification);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				} else {
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					try {
						DmMap specialProperties = new DmMap();
						specialProperties.setHashTable(new HashTable());
						String msisdn = cclient.getmsisdn();
						if (msisdn != null && !msisdn.startsWith(Constants.AZERI_COUNTRY_CODE))
							msisdn = Constants.AZERI_COUNTRY_CODE + msisdn;
//						int response = SADM.getInstance().submitProvisionRequest("+" + msisdn, null,Constants.SCENARIO_NAME, specialProperties);
						int response = ThirdPartyCall.submitProvisionRequest("+" + msisdn, null,Constants.SCENARIO_NAME, specialProperties);
						if (response == 0) {
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
						} else {
							resp.setReturnCode("" + response);
							resp.setReturnMsg(
									ConfigurationManager.getConfigurationFromCache("sadm.error." + response + ""));
						}
						return resp;
					} catch (DMException e) {
						logger.error(Helper.GetException(e));
					}
				} else {
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(resp.getReturnCode());
		logs.setResponseDescription(resp.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}
}
