package com.evampsaanga.b2b.azerfon.verifycdrsotp;

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;

public class VerifyCDRsOTPRequestV2 extends BaseRequest{
	private String userName;
	private String otp;
	private String requestPlatform;

	

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public String getRequestPlatform() {
		return requestPlatform;
	}

	public void setRequestPlatform(String requestPlatform) {
		this.requestPlatform = requestPlatform;
	}

}
