package com.evampsaanga.b2b.azerfon.limitpaymentrelation;

import java.io.IOException;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.amqimplementationsesb.LogsType;
import com.evampsaanga.b2b.azerfon.db.DBFactory;
import com.evampsaanga.b2b.azerfon.getsubscriber.CRMSubscriberService;
import com.evampsaanga.b2b.azerfon.hlrweb.getquerybalanceRequest.HLRBalanceServices;
import com.evampsaanga.b2b.azerfon.loan.getcollections.Collection;
import com.evampsaanga.b2b.azerfon.thirdpartycall.AzerfonThirdPartyCalls;
import com.evampsaanga.b2b.azerfon.ussdgwquerysubinforeqmsg.USSDGWQuerySubinfoLand;
import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.ThirdPartyNames;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Decrypter;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.evampsaanga.b2b.developer.utils.SOAPLoggingHandler;
import com.evampsaanga.b2b.gethomepage.Balance;
import com.evampsaanga.b2b.magento.tariffdetailsv2.TariffDetailsMagentoResponse;
import com.huawei.bme.cbsinterface.arservices.QueryBalanceRequest;
import com.huawei.bme.cbsinterface.arservices.QueryBalanceRequestMsg;
import com.huawei.bme.cbsinterface.arservices.QueryBalanceResultMsg;
import com.huawei.bme.cbsinterface.arservices.QueryBalanceRequest.QueryObj;
import com.huawei.bme.cbsinterface.arservices.QueryBalanceRequest.QueryObj.AcctAccessCode;
import com.huawei.crm.azerfon.bss.query.GetSubscriberResponse;
import com.ngbss.evampsaanga.services.ThirdPartyCall;
import com.ngbss.evampsaanga.services.ThirdPartyRequestHeader;

@Path("/azerfon/")
public class LimitPaymentRelationRequestLand {
	public static final Logger logger = Logger.getLogger("azerfon-esb");

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LimitPaymentRelationResponse Get(@Header("credentials") String credential,
			@Header("Content-Type") String contentType, @Body() String requestBody) {
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.CHANGE_LIMIT_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.CHANGE_TARRIF);
		logs.setTableType(LogsType.ChangeTarrif);

		LimitPaymentRelationRequestClient cclient = null;
		LimitPaymentRelationResponse resp = new LimitPaymentRelationResponse();
		try {
			logger.info("Request Landed on LimitPaymentRelationRequestLand:" + requestBody);
			String token = Helper.retrieveToken(Transactions.CHANGE_LIMIT_TRANSACTION_NAME,
					Helper.getValueFromJSON(requestBody, "msisdn"));

			try {
				cclient = Helper.JsonToObject(requestBody, LimitPaymentRelationRequestClient.class);
				if (cclient != null) {
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
					logs.setLang(cclient.getLang());

				}
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				}

				if (credentials == null) {

					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					// Comment because of libarary issue
					/*
					 * GetSubscriberResponse response = new
					 * com.evampsaanga.azerfon.getsubscriber.
					 * CRMSubscriberService()
					 * .GetSubscriberRequest(cclient.getMsisdnuser());
					 */
					// response.getGetSubscriberBody().getPrimaryOffering().getOfferingId().getOfferingId().toString()
					// response.getGetSubscriberBody().getSubscriberType()
					
					
					GetSubscriberResponse getSubscriberResponse = new CRMSubscriberService()
							.GetSubscriberRequest(cclient.getMsisdnuser());
					
					if(getSubscriberResponse.getResponseHeader().getRetCode().equalsIgnoreCase("0"))
					{
					
					
					if (getSubscriberResponse.getGetSubscriberBody() != null) {
						logger.info("---------------Subscriber Info-------------");
						logger.info(Helper.ObjectToJson(getSubscriberResponse));

						

						final String primaryOfferingId = getSubscriberResponse.getGetSubscriberBody()
								.getPrimaryOffering().getOfferingId().getOfferingId();
						
						cclient.setOfferingId(primaryOfferingId);
					}
					

					if (cclient.getType().equalsIgnoreCase("Individual")) {
						JSONArray jArray = new JSONArray();
						jArray.put(cclient.getOfferingId());
						JSONObject request = new JSONObject();
						request.put("offeringId", jArray);
						request.put("lang", cclient.getLang());
						request.put("msisdn", cclient.getmsisdn());
						request.put("subscriberType", "Prepaid");
						logger.info("msisdn " + cclient.getmsisdn() + request.toString());

						logger.info("CAlling Magento ");

						TariffDetailsMagentoResponse dataTariffDetailsbulk = AzerfonThirdPartyCalls
								.tariffDetailsV2BulkResponseChangeLimit(request.toString());

						logger.info("Returned From Magento Call ");
						// SubmitOrderResponse response =
						// changetarrifresponse(cclient.getmsisdn(),
						// cclient.getOfferingId());

						String destinationtariffMrcValue = "";

						int minimumLimit = 0;

						if (dataTariffDetailsbulk.getResultCode()
								.equals(ResponseCodes.TARIFF_DETAILS_VERSION_2_SUCCESSFUL)) {
							logger.info("Entering in First If ");
							// response.getGetSubscriberBody().getPrimaryOffering().getOfferingId().getOfferingId()
							logger.info("CAlling Helper.COmpare Method ");

							logger.info("dataTariffDetailsbulk response" + dataTariffDetailsbulk.getData());
							int indexForOffering = Helper.compare(cclient.getOfferingId(),
									dataTariffDetailsbulk.getData());
							logger.info("returned from Helper.COmpare With Index " + indexForOffering);
							if (indexForOffering != -1) {
								destinationtariffMrcValue = dataTariffDetailsbulk.getData().get(indexForOffering)
										.getHeader().getMrcValue();
								QueryBalanceRequestMsg queryBalanceRequestMsg = new QueryBalanceRequestMsg();
								QueryBalanceRequest BalanceRequest = new QueryBalanceRequest();
								QueryObj QueryObj = new QueryObj();
								AcctAccessCode accessCode = new AcctAccessCode();
								logger.info("#################################################3");
								logger.info("Testing:" + cclient.getMsisdnuser());
								logger.info("#################################################3");
								accessCode.setPrimaryIdentity(cclient.getMsisdnuser());
								QueryObj.setAcctAccessCode(accessCode);
								BalanceRequest.setQueryObj(QueryObj);
								queryBalanceRequestMsg.setQueryBalanceRequest(BalanceRequest);
								queryBalanceRequestMsg
										.setRequestHeader(ThirdPartyRequestHeader.getRequestPaymentRequestHeader());

								QueryBalanceResultMsg resultMsg = ThirdPartyCall
										.getQueryBalance(queryBalanceRequestMsg);
								
								Long remainingAmount = resultMsg.getQueryBalanceResult().getAcctList().get(0)
										.getAccountCredit().get(0).getTotalUsageAmount();
								NumberFormat df = new DecimalFormat("#0.00");
								df.setRoundingMode(RoundingMode.FLOOR);
								String remainingAmountstr = df
										.format(remainingAmount.longValue() / Constants.MONEY_DIVIDEND);
								double d = Double.parseDouble(remainingAmountstr);
								int remainingAmountInt = (int) d;
								int mrcValueInt = Integer.parseInt(destinationtariffMrcValue);
								minimumLimit = Math.max(remainingAmountInt, mrcValueInt);

								
								
								String maxlimit = getMaxFromDb(
										dataTariffDetailsbulk.getData().get(0).getHeader().getMrcValue());

								ArrayList<String> list = getAllLimits(cclient.getGroupType(), maxlimit,
										Integer.toString(minimumLimit), cclient.getType());

								LimitPaymentRelationData data = new LimitPaymentRelationData();
								long currentLimit = 0;
								if(cclient.getGroupType().equalsIgnoreCase("PayBySubs"))
								{
									logger.info(token + "----- User is PayBySubs----:");
									if(resultMsg.getQueryBalanceResult().getAcctList()!=null)
									{
										if(resultMsg.getQueryBalanceResult().getAcctList().get(0).getAccountCredit()!=null)
										{
											currentLimit = resultMsg.getQueryBalanceResult().getAcctList().get(0).getAccountCredit().get(0).getTotalCreditAmount();
											currentLimit = currentLimit / Constants.MONEY_DIVIDEND;
											logger.info(token + "----- The CUrrent limit of user is ----:"+currentLimit);
										}
										
									}
								}
								
								else if(cclient.getGroupType().equalsIgnoreCase("PartPay"))
									
								{
									
									if(resultMsg.getQueryBalanceResult().getAcctList()!=null)
									{
										if(resultMsg.getQueryBalanceResult().getAcctList().get(0).getPaymentLimitUsage()!=null)
										{
											currentLimit = resultMsg.getQueryBalanceResult().getAcctList().get(0).getPaymentLimitUsage().get(0).getAmount();
											currentLimit = currentLimit / Constants.MONEY_DIVIDEND;
										}
										
										logger.info(token + "----- The CUrrent limit of user is ----:"+currentLimit);
										
									}
								}
								
								
								
								
								data.setLimitList(list);
								// data.setInitialLimit(Integer.toString(minimumLimit));
								logger.info("Setting Initial Limit from list  " + list.get(0));
								if (list != null && list.size() > 0)
									data.setInitialLimit(list.get(0));
								data.setMaxLimit(maxlimit);
								data.setLimitLabel(ConfigurationManager
										.getConfigurationFromCache("changelimit.label." + cclient.getLang()));
								data.setCurrentLimit(Long.toString(currentLimit));
								
								
								if(Integer.parseInt(maxlimit)<Integer.parseInt(data.getCurrentLimit())){
									data.getLimitList().add(data.getCurrentLimit());
									data.setMaxLimit(data.getCurrentLimit());
								}
								
								
								if(Integer.parseInt(data.getInitialLimit()) > Integer.parseInt(data.getCurrentLimit()))
								{
									ArrayList<String> temp = data.getLimitList();
									
									data.setLimitList(null);
									
									ArrayList<String> newList = new ArrayList<String>();
									newList.add(data.getCurrentLimit());
									
									newList.addAll(temp);
									
									data.setLimitList(newList);
									data.setInitialLimit(data.getCurrentLimit());
									
									
								}
								
								
								resp.setData(data);
								resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
								resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
							} else {
								resp.setData(null);
								resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
								resp.setReturnMsg(ResponseCodes.CHANGE_LIMIT_MSG);
							}

						}

						else {

							resp.setReturnCode(dataTariffDetailsbulk.getResultCode());
							resp.setReturnMsg(dataTariffDetailsbulk.getMsg());
							logs.setResponseCode(dataTariffDetailsbulk.getResultCode());
							logs.setResponseDescription(dataTariffDetailsbulk.getMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());

						}
					} else {

						String minimumLimit = "";

						String maximumLimit = "";

						ArrayList<String> list = getAllLimits(cclient.getGroupType(), minimumLimit, maximumLimit,
								cclient.getType());
						minimumLimit = list.get(0);
						maximumLimit = list.get(list.size() - 1);

						LimitPaymentRelationData data = new LimitPaymentRelationData();

						data.setLimitList(list);
						data.setInitialLimit(minimumLimit);
						data.setMaxLimit(maximumLimit);
						data.setLimitLabel(ConfigurationManager
								.getConfigurationFromCache("changelimit.label." + cclient.getLang()));

						resp.setData(data);
						resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
						resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
					}
					
					}
					
					else
					{
						LimitPaymentRelationData data = new LimitPaymentRelationData();
						data.setLimitList(null);
						data.setInitialLimit(null);
						data.setMaxLimit(null);
						data.setLimitLabel(null);
						resp.setData(data);
						resp.setReturnCode(getSubscriberResponse.getResponseHeader().getRetCode());
						resp.setReturnMsg(ConfigurationManager
								.getConfigurationFromCache("ngbss.response.code.1211000433." + cclient.getLang()));
					}

				}

			}
			logs.updateLog(logs);

		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
			resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
			resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
		}

		try {
			logger.info("ResponseBeforeReturn: " + Helper.ObjectToJson(resp));
		} catch (IOException e) {
			logger.error(Helper.GetException(e));
			resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
			resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(Helper.GetException(e));
			resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
			resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
		}
		return resp;
	}

	public ArrayList<String> getAllLimits(String groupType, String max, String min, String type) {

		logger.info("Landed in getAllLimits with groupType :" + groupType);
		if (type.equalsIgnoreCase("Individual")) {
			ArrayList<String> listStr = new ArrayList<String>();

			try {

				String query = "select name from configuration_items where `name`>=" + min + " && `name`<=" + max
						+ " and `key` LIKE '%changelimit.credit.limit%' order by cast(`name` as unsigned) ASC";
				logger.info("Landed in getAllLimits Method with Query " + query);
				ResultSet rs = DBFactory.getMagentoDBConnection().prepareStatement(query).executeQuery();
				while (rs.next()) {
					listStr.add(rs.getString("name"));
				}

			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return listStr;
		} else {
			ArrayList<String> listStr = new ArrayList<>();
			try {

				String query = "select name ,CONVERT(SUBSTRING_INDEX(name,'-',-1),UNSIGNED INTEGER) AS num from configuration_items where `key` LIKE '%changelimit.credit.limit%' ORDER BY num asc";

				logger.info("Landed in getAllLimits Method with Query " + query);
				ResultSet rs = DBFactory.getMagentoDBConnection().prepareStatement(query).executeQuery();
				while (rs.next()) {
					listStr.add(rs.getString("name"));
				}

			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return listStr;
		}

	}

	public static int conversionStrtoInt(String valueinput)

	{
		if (valueinput == null || valueinput.equalsIgnoreCase("0") || valueinput.isEmpty()) {
			return 0;
		}
		int finalValue = 0;
		if (valueinput.contains(".")) {
			double value = Double.parseDouble(valueinput);
			finalValue = (int) value;

		} else {
			finalValue = Integer.parseInt(valueinput);

		}

		return finalValue;
	}

	public static String getMaxFromDb(String mrc) {
		logger.info("Landed in getMaxFromDb Method with mrc " + mrc);

		int mrcValue = Integer.parseInt(mrc);
		mrcValue += 30;

		try {

			String query = "select name from configuration_items where `name`>=" + mrcValue
					+ " and `key` LIKE '%changelimit.credit.limit%' LIMIT 1";
			logger.info("Landed in getMaxFromDb Method with Query " + query);
			ResultSet rs = DBFactory.getMagentoDBConnection().prepareStatement(query).executeQuery();
			if (rs.next() != false) {
				String max = rs.getString("name");
				logger.info("REturning Max" + max);
				return max;
			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static void main(String[] args) {
		/*
		 * String a="165.00"; double d = Double.parseDouble(a); int
		 * remainingAmountInt =(int) (d);
		 * System.out.println(remainingAmountInt);
		 */

		ArrayList<String> list = new ArrayList<String>();
		list.add("0");
		list.add("25000");
		list.add("7");
		list.add("12");
		list.add("14");

		ArrayList<Integer> listInt = new ArrayList<Integer>();
		for (String str : list) {
			listInt.add(Integer.parseInt(str));
		}

		Collections.sort(listInt);

		ArrayList<String> liststr = new ArrayList<String>();
		liststr.add(listInt.toString());
		for (int i = 0; i < liststr.size(); i++) {
			System.out.println(liststr.get(i));
		}

	}
}
