package com.evampsaanga.b2b.azerfon.limitpaymentrelation;

import java.util.ArrayList;

import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;

public class LimitPaymentRelationResponse extends BaseResponse {

	private LimitPaymentRelationData data= new LimitPaymentRelationData();

	public LimitPaymentRelationData getData() {
		return data;
	}

	public void setData(LimitPaymentRelationData data) {
		this.data = data;
	}
}
