package com.evampsaanga.b2b.azerfon.limitpaymentrelation;

import java.util.ArrayList;

public class LimitPaymentRelationData {
	private ArrayList<String> limitList;
	private String initialLimit;
	private String maxLimit;
	private String limitLabel;
	private String currentLimit;

	public String getLimitLabel() {
		return limitLabel;
	}

	public void setLimitLabel(String limitLabel) {
		this.limitLabel = limitLabel;
	}

	public ArrayList<String> getLimitList() {
		return limitList;
	}

	public void setLimitList(ArrayList<String> limitList) {
		this.limitList = limitList;
	}

	public String getInitialLimit() {
		return initialLimit;
	}

	public void setInitialLimit(String initialLimit) {
		this.initialLimit = initialLimit;
	}

	public String getMaxLimit() {
		return maxLimit;
	}

	public void setMaxLimit(String maxLimit) {
		this.maxLimit = maxLimit;
	}

	public String getCurrentLimit() {
		return currentLimit;
	}

	public void setCurrentLimit(String currentLimit) {
		this.currentLimit = currentLimit;
	}


}
