package com.evampsaanga.b2b.azerfon.limitpaymentrelation;

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;

public class LimitPaymentRelationRequestClient extends BaseRequest {
	private String offeringId="";
	private String groupType="";
	private String companyStandardValue="";
	private String msisdnuser="";
	private String type;
	
	
	
	public String getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(String offeringId) {
		this.offeringId = offeringId;
	}


	
   public String getMsisdnuser() {
		return msisdnuser;
	}

	public void setMsisdnuser(String msisdnuser) {
		this.msisdnuser = msisdnuser;
	}

public String getCompanyStandardValue() {
		return companyStandardValue;
	}

	public void setCompanyStandardValue(String companyStandardValue) {
		this.companyStandardValue = companyStandardValue;
	}

	public String getGroupType() {
		return groupType;
	}

	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}



}
