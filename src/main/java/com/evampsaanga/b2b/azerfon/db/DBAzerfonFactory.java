package com.evampsaanga.b2b.azerfon.db;

import java.sql.Connection;
import org.apache.log4j.Logger;

import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.developer.utils.Helper;

public class DBAzerfonFactory {
	public static final Logger logger = Logger.getLogger("azerfon-esb");
	static {
		new oracle.jdbc.OracleDriver();
		new oracle.jdbc.driver.OracleDriver();
	}

	public static Connection getConnection() {
		String struserid = ConfigurationManager.getDBProperties("ods.user");
		String strcredentialpass = ConfigurationManager.getDBProperties("ods.password");
		String dbURL = ConfigurationManager.getDBProperties("ods.url");
		logger.info("Db USEr :"+struserid);
		logger.info("Db password :"+strcredentialpass);
		logger.info("Db URL :"+dbURL);
		try {
			return java.sql.DriverManager.getConnection(dbURL, struserid, strcredentialpass);
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		return null;
	}
}
