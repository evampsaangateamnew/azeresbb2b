/**
 * 
 */
package com.evampsaanga.b2b.azerfon.nartv.getsubscriptions;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

import com.evampsaanga.b2b.azerfon.thirdpartycall.AzerfonThirdPartyCalls;

/**
 * @author Rafae Saleem
 *
 */
public class GetPlans {

	public static final Logger logger = Logger.getLogger("azerfon-esb");

	public static List<Subscriptions> getPlans(String token, String key, String lang, String offeringId, String planId,
			String state, String startDate, String endDate)
			throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {

		key = key.trim();
		List<Subscriptions> subscriptionsList = new ArrayList<Subscriptions>();

		if (!key.trim().isEmpty() && AppCache.hashmapForPlans.containsKey(key)) {

			logger.info(token + "Message against key:(" + key + ") returned from cache.");

			subscriptionsList = AppCache.hashmapForPlans.get(key);

		} else {

			logger.info(token + "Message against key:(" + key + ") not found in cache. Request Sent To DB");

			AzerfonThirdPartyCalls.getSubscriptions(token, lang, offeringId, planId, state, startDate, endDate);

			subscriptionsList = AppCache.hashmapForPlans.get(key);
		}

		return subscriptionsList;
	}
}
