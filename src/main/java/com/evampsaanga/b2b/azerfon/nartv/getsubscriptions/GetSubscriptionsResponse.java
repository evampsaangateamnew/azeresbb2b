package com.evampsaanga.b2b.azerfon.nartv.getsubscriptions;

import java.util.List;

import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;

public class GetSubscriptionsResponse extends BaseResponse {

	private List<Subscriptions> subscriptions;
	private String subscriberId;

	public List<Subscriptions> getSubscriptions() {
		return subscriptions;
	}

	public void setSubscriptions(List<Subscriptions> subscriptions) {
		this.subscriptions = subscriptions;
	}

	public String getSubscriberId() {
		return subscriberId;
	}

	public void setSubscriberId(String subscriberId) {
		this.subscriberId = subscriberId;
	}

	@Override
	public String toString() {
		return "GetSubscriptionsResponse [subscriptions=" + subscriptions + ", subscriberId=" + subscriberId + "]";
	}

}