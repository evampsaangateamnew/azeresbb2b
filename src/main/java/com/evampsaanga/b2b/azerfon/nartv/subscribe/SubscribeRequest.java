package com.evampsaanga.b2b.azerfon.nartv.subscribe;

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;

public class SubscribeRequest extends BaseRequest {

	private String grantType;
	private String clientId;
	private String username;
	private String password;
	private String subscriberId;
	private String planId;

	public String getGrantType() {
		return grantType;
	}

	public void setGrantType(String grantType) {
		this.grantType = grantType;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSubscriberId() {
		return subscriberId;
	}

	public void setSubscriberId(String subscriberId) {
		this.subscriberId = subscriberId;
	}

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	@Override
	public String toString() {
		return "SubscribeRequest [grantType=" + grantType + ", clientId=" + clientId + ", username=" + username
				+ ", password=" + password + ", subscriberId=" + subscriberId + ", planId=" + planId + "]";
	}

}
