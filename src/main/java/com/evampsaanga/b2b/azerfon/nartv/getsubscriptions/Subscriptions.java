package com.evampsaanga.b2b.azerfon.nartv.getsubscriptions;

public class Subscriptions {

	private String title;
	private String price;
	private String description;
	private String status;
	private String planId;
	private String startDate;
	private String endDate;
	private String labelToRenew;
	private String labelRenewalDate;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getLabelToRenew() {
		return labelToRenew;
	}

	public void setLabelToRenew(String labelToRenew) {
		this.labelToRenew = labelToRenew;
	}

	public String getLabelRenewalDate() {
		return labelRenewalDate;
	}

	public void setLabelRenewalDate(String labelRenewalDate) {
		this.labelRenewalDate = labelRenewalDate;
	}

	@Override
	public String toString() {
		return "Subscriptions [title=" + title + ", price=" + price + ", description=" + description + ", status="
				+ status + ", planId=" + planId + ", startDate=" + startDate + ", endDate=" + endDate
				+ ", labelToRenew=" + labelToRenew + ", labelRenewalDate=" + labelRenewalDate + "]";
	}

}
