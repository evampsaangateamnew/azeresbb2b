package com.evampsaanga.b2b.azerfon.nartv.getsubscriptions;

import java.io.IOException;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.azerfon.thirdpartycall.AzerfonThirdPartyCalls;
import com.evampsaanga.b2b.azerfon.validator.RequestValidator;
import com.evampsaanga.b2b.azerfon.validator.ResponseValidator;
import com.evampsaanga.b2b.azerfon.validator.ValidatorService;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Helper;

@Path("/azerfon/")
public class GetSubscriptionsController {

	public static final Logger logger = Logger.getLogger("azerfon-esb");

	@POST
	@Path("/getsubscriptions")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetSubscriptionsResponse Get(@Header("credentials") String credential, @Body() String requestBody) {

		Logs logs = new Logs();

		try {

			// ------------------------- Setting Transaction Name ---------------------- //

			GetSubscriptionsResponse getManagerTokenResponse = new GetSubscriptionsResponse();
			GetSubscriptionsRequest cclient = null;

			String transactionName = Transactions.NAR_TV_GET_SUBSCRIPTIONS;
			String token = Helper.retrieveToken(transactionName, Helper.getValueFromJSON(requestBody, "msisdn"));

			Constants.logger.info(token + "-Request Landed In-" + transactionName);
			Constants.logger.info(token + "-Request Data-" + requestBody);

			// ----------------------------- Validating Text --------------------------- //

			cclient = Helper.JsonToObject(requestBody, GetSubscriptionsRequest.class);

			ValidatorService validatorService = new ValidatorService();
			ResponseValidator responseValidator = new ResponseValidator();
			RequestValidator requestValidator = new RequestValidator();

			requestValidator.setChannel(cclient.getChannel());
			requestValidator.setiP(cclient.getiP());
			requestValidator.setIsB2B(cclient.getIsB2B());
			requestValidator.setLang(cclient.getLang());
			requestValidator.setMsisdn(cclient.getmsisdn());
			responseValidator = validatorService.processValidation(requestValidator, credential, token);

			if (responseValidator.getResponseCode().equals("00")) {

				GetSubscriptionsRequest getManagerTokenRequest = new GetSubscriptionsRequest();

				requestBody = Helper.addParamsToJSONObject(requestBody, "grantType", "password");
				requestBody = Helper.addParamsToJSONObject(requestBody, "clientId", "nar@master");
				requestBody = Helper.addParamsToJSONObject(requestBody, "username", "nar_it@sotal.tv");
				requestBody = Helper.addParamsToJSONObject(requestBody, "password", "vxGNUuqH");

				getManagerTokenRequest = Helper.JsonToObject(requestBody, GetSubscriptionsRequest.class);

				getManagerTokenResponse = AzerfonThirdPartyCalls.narTvSubscriptions(token, transactionName,
						getManagerTokenRequest, getManagerTokenResponse);

			} else {

				getManagerTokenResponse.setReturnCode(responseValidator.getResponseCode());
				getManagerTokenResponse.setReturnMsg(responseValidator.getResponseDescription());
				logs.setResponseCode(getManagerTokenResponse.getReturnCode());
				logs.setResponseDescription(getManagerTokenResponse.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);

			}

			Constants.logger.info(token + "-Response Returned From-" + transactionName + "-"
					+ Helper.ObjectToJson(getManagerTokenResponse));

			return getManagerTokenResponse;

		} catch (Exception ex) {

			Constants.logger.info(Helper.GetException(ex));
			GetSubscriptionsResponse resp = new GetSubscriptionsResponse();
			resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
			resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			try {
				Constants.logger.info(
						"" + "-Response Returned From-" + "NAR TV GET SUBSCRIPTIONS" + "-" + Helper.ObjectToJson(resp));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				logger.info(Helper.GetException(e));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.info(Helper.GetException(e));
			}
			return resp;
		}
	}
}
