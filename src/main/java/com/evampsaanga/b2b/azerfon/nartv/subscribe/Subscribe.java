package com.evampsaanga.b2b.azerfon.nartv.subscribe;

public class Subscribe {

	private String title;
	private String price;
	private String description;
	private String status;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Subscriptions [title=" + title + ", price=" + price + ", description=" + description + ", status="
				+ status + "]";
	}

}
