package com.evampsaanga.b2b.azerfon.nartv.getsubscriptions;

public class GetSubscriptionsThirdPartyRequest {

	private String grant_type;
	private String client_id;
	private String username;
	private String password;

	public String getGrant_type() {
		return grant_type;
	}

	public void setGrant_type(String grant_type) {
		this.grant_type = grant_type;
	}

	public String getClient_id() {
		return client_id;
	}

	public void setClient_id(String client_id) {
		this.client_id = client_id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "grant_type=" + grant_type + "&client_id=" + client_id + "&username=" + username + "&password="
				+ password;
	}

}
