package com.evampsaanga.b2b.azerfon.nartv.getsubscriptions;

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;

public class GetSubscriptionsRequest extends BaseRequest {

	private String grantType;
	private String clientId;
	private String username;
	private String password;
	private String offeringId;

	public String getGrantType() {
		return grantType;
	}

	public void setGrantType(String grantType) {
		this.grantType = grantType;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(String offeringId) {
		this.offeringId = offeringId;
	}

	@Override
	public String toString() {
		return "GetSubscriptionsRequest [grantType=" + grantType + ", clientId=" + clientId + ", username=" + username
				+ ", password=" + password + ", offeringId=" + offeringId + "]";
	}

}
