package com.evampsaanga.b2b.azerfon.sendemail;

import java.io.IOException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.amqimplementationsesb.LogsType;
import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.ThirdPartyNames;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Decrypter;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.evampsaanga.b2b.developer.utils.SOAPLoggingHandler;
import com.saanga.magento.apiclient.RestClient;

@Path("/azerfon")
public class SendEmailRequestLand {
	
	
	public static final Logger logger = Logger.getLogger("azerfon-esb");

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public SendEmailResponse Get(@Header("credentials") String credential,
			@Header("Content-Type") String contentType, @Body() String requestBody) {
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.SEND_EMAIL);
		logs.setThirdPartyName(ThirdPartyNames.SENDEMAIL);
		logs.setTableType(LogsType.SendEmail);
		String token = "";
		String TrnsactionName = Transactions.SEND_EMAIL;

		try {

			token = Helper.retrieveToken(TrnsactionName, Helper.getValueFromJSON(requestBody, "msisdn"));

		
			logger.info(token+"Request Landed on SendEmailRequestLand:" + requestBody);
			SendEmailRequest cclient = new SendEmailRequest();
			try {
				cclient = Helper.JsonToObject(requestBody, SendEmailRequest.class);
				if (cclient != null) {
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
				}
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				SendEmailResponse resp = new SendEmailResponse();
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		
			
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					SendEmailResponse resp = new SendEmailResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						SendEmailResponse res = new SendEmailResponse();
						res.setReturnCode(ResponseCodes.ERROR_400);
						res.setReturnMsg(verification);
						logs.setResponseCode(res.getReturnCode());
						logs.setResponseDescription(res.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return res;
					}
				} else {
					SendEmailResponse resp = new SendEmailResponse();
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					String lang = "2002";
					if(cclient.getLang().equalsIgnoreCase("3"))
						lang = "2002";
					if(cclient.getLang().equalsIgnoreCase("2"))
						lang = "2052";
					if(cclient.getLang().equalsIgnoreCase("4"))
						lang = "2060";
					return getResponse(cclient.getText(),cclient.getSubject(), cclient.getFrom(),cclient.getTo(),logger,cclient,token);
				} else {
					SendEmailResponse resp = new SendEmailResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		SendEmailResponse resp = new SendEmailResponse();
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(resp.getReturnCode());
		logs.setResponseDescription(resp.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}
	public SendEmailResponse getResponse(String text, String subject, String from,String to ,Logger logger,SendEmailRequest cclient,String token)
	{
		SendEmailResponse resp = new SendEmailResponse();
		
		logger.info(token+"NOW LANDED IN getResponseMethod");
		//start of sendEmail
		
		/*final String username = "Business.Services@bakcell.com";
		final String password = "B2525akcell++";*/
		
		final String username=ConfigurationManager.getConfigurationFromCache("email.smtp.username");
		final String password=ConfigurationManager.getConfigurationFromCache("email.smtp.password");
	
		logger.info(token+"USERNAME VALUE CONFIG "+username);
		logger.info(token+"PASSWORD VALUE CONFIG "+password);
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("lang", cclient.getLang());
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			logger.info(token+Helper.GetException(e1));
		}
		try {
			if(cclient.getFrom()==null || cclient.getFrom().isEmpty())
			{
			 from = ConfigurationManager.getConfigurationFromCache("email.from");
			logger.info(token+"FROM VALUE CONFIG "+from);
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			logger.info(token+Helper.GetException(e1));
		}
		
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.host", ConfigurationManager.getConfigurationFromCache("email.mail.smtp.host"));
		props.put("mail.smtp.port", "25");
		logger.info(token+"HOST VALUE CONFIG "+ ConfigurationManager.getConfigurationFromCache("email.mail.smtp.host"));
		Session session = Session.getInstance(props,
		  new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		  });

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(to));
			message.setSubject(subject);
			
			//message.setText(text);
		
		//	message.setContent(text, "text/html");
			
			
			Multipart mp = new MimeMultipart();
			MimeBodyPart htmlPart = new MimeBodyPart();
			htmlPart.setContent(text, "text/html; charset=UTF-8");
			mp.addBodyPart(htmlPart);
			message.setContent(mp);
			
			
			Transport.send(message);

			System.out.println("Done");
			logger.info(token+"RESULT IN TRY EMAIL SEND SUCCESSFULLY");

		} catch (MessagingException e) {
			logger.info(token+Helper.GetException(e));
		}
	
		
		//end of sendEmail
		
		
		resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
		resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
		try {
			logger.info(token+"Response FROM ESB " +  Helper.ObjectToJson(resp));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.info(token+Helper.GetException(e));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.info(token+Helper.GetException(e));
		}
		return resp;
    }

}
