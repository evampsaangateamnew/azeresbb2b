package com.evampsaanga.b2b.azerfon.sendemail;

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;

public class SendEmailRequest extends BaseRequest {
	String text="";
	String from="";
	String subject="";
	String to="";

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}
	
	

}
