package com.evampsaanga.b2b.azerfon.getsubscriber;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetSubscriberRequestClient", propOrder = { "requestBody" })
@XmlRootElement(name = "GetSubscriberRequestClient")
public class GetSubscriberRequestClient extends com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest {
	@XmlElement(name = "requestBody", required = true)
	String requestBody = "";

	/*
	 * @XmlElement(name = "requestBody", required = true) GetSubscriberRequest
	 * requestBody = null;
	 * 
	 * public GetSubscriberRequest getRequestBody() { return requestBody; }
	 * 
	 * public void setRequestBody(GetSubscriberRequest requestBody) {
	 * this.requestBody = requestBody; }
	 */
	public String getRequestBody() {
		return requestBody;
	}

	public void setRequestBody(String requestBody) {
		this.requestBody = requestBody;
	}
}
