package com.evampsaanga.b2b.azerfon.getsubscriber;

public class CoreServicesCategoryItem {

	private Long id;
	private String name;
	private String description;
	private Long categoryId;
	private String price;
	private String validity;
	private String offeringId;
	private String status = "Inactive";
	private String storeId;
	private Integer sortOrder;
	private String forwardNumber;
	private Integer renewable;
	private String validityLabel;
	private String progressDateLabel;
	private String progressTitle;
	private String freeFor;
	private String effectiveDate;
	private String expireDate;
	private String visibleFor;

	@Override
	public String toString() {
		return "CoreServicesCategoryItem [id=" + id + ", name=" + name + ", description=" + description
				+ ", categoryId=" + categoryId + ", price=" + price + ", validity=" + validity + ", offeringId="
				+ offeringId + ", status=" + status + ", storeId=" + storeId + ", sortOrder=" + sortOrder
				+ ", forwardNumber=" + forwardNumber + ", renewable=" + renewable + ", validityLabel=" + validityLabel
				+ ", progressDateLabel=" + progressDateLabel + ", progressTitle=" + progressTitle + ", freeFor="
				+ freeFor + ", effectiveDate=" + effectiveDate + ", expireDate=" + expireDate + ", visibleFor="
				+ visibleFor + "]";
	}

	public CoreServicesCategoryItem(Long id, String name, String description, Long categoryId, String price,
			String validity, String offeringId, String storeId, Integer sortOrder, String forwardNumber,
			Integer renewable, String freeFor, String visibleFor) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.categoryId = categoryId;
		this.price = price;
		this.validity = validity;
		this.offeringId = offeringId;
		this.storeId = storeId;
		this.sortOrder = sortOrder;
		this.forwardNumber = forwardNumber;
		this.renewable = renewable;
		this.freeFor = freeFor;
		this.visibleFor = visibleFor;

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getValidity() {
		return validity;
	}

	public void setValidity(String validity) {
		this.validity = validity;
	}

	public String getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(String offeringId) {
		this.offeringId = offeringId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStoreId() {
		return storeId;
	}

	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}

	public Integer getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getForwardNumber() {
		return forwardNumber;
	}

	public void setForwardNumber(String forwardNumber) {
		this.forwardNumber = forwardNumber;
	}

	public Integer getRenewable() {
		return renewable;
	}

	public void setRenewable(Integer renewable) {
		this.renewable = renewable;
	}

	public String getFreeFor() {
		return freeFor;
	}

	public void setFreeFor(String freeFor) {
		this.freeFor = freeFor;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(String expireDate) {
		this.expireDate = expireDate;
	}

	public String getValidityLabel() {
		return validityLabel;
	}

	public void setValidityLabel(String validityLabel) {
		this.validityLabel = validityLabel;
	}

	public String getProgressDateLabel() {
		return progressDateLabel;
	}

	public void setProgressDateLabel(String progressDateLabel) {
		this.progressDateLabel = progressDateLabel;
	}

	public String getProgressTitle() {
		return progressTitle;
	}

	public void setProgressTitle(String progressTitle) {
		this.progressTitle = progressTitle;
	}

	public String getVisibleFor() {
		return visibleFor;
	}

	public void setVisibleFor(String visibleFor) {
		this.visibleFor = visibleFor;
	}

}
