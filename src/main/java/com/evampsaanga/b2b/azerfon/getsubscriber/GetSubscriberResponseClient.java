package com.evampsaanga.b2b.azerfon.getsubscriber;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;
import com.huawei.crm.azerfon.bss.query.GetSubscriberResponse;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetSubscriberResponseClient", propOrder = { "responseBody", "subscriberResponse" })
@XmlRootElement(name = "GetSubscriberResponseClient")
public class GetSubscriberResponseClient extends BaseResponse {
	@XmlElement(name = "subscriberResponse", required = true)
	GetSubscriberResponse subscriberResponse = new GetSubscriberResponse();

	public GetSubscriberResponseClient() {
		super();
	}

	public GetSubscriberResponse getSubscriberResponse() {
		return subscriberResponse;
	}

	public void setSubscriberResponse(GetSubscriberResponse subscriberResponse) {
		this.subscriberResponse = subscriberResponse;
	}
}
