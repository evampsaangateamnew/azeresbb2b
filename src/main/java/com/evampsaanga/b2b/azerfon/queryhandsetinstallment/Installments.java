package com.evampsaanga.b2b.azerfon.queryhandsetinstallment;

public class Installments {
	private String TotalAmount = "";
	private String RemainingAmount = "";
	private String TotalPeriods = "";
	private String RemainingPeriods = "";
	private String BeginDate = "";
	private String EndDate = "";
	private String InstallmentStatus = "";

	public Installments(String totalAmount, String remainingAmount, String totalPeriods, String remainingPeriods,
			String beginDate, String endDate, String installmentStatus) {
		super();
		TotalAmount = totalAmount;
		RemainingAmount = remainingAmount;
		TotalPeriods = totalPeriods;
		RemainingPeriods = remainingPeriods;
		BeginDate = beginDate;
		EndDate = endDate;
		InstallmentStatus = installmentStatus;
	}

	/**
	 * @return the totalAmount
	 */
	public String getTotalAmount() {
		return TotalAmount;
	}

	/**
	 * @param totalAmount
	 *            the totalAmount to set
	 */
	public void setTotalAmount(String totalAmount) {
		TotalAmount = totalAmount;
	}

	/**
	 * @return the remainingAmount
	 */
	public String getRemainingAmount() {
		return RemainingAmount;
	}

	/**
	 * @param remainingAmount
	 *            the remainingAmount to set
	 */
	public void setRemainingAmount(String remainingAmount) {
		RemainingAmount = remainingAmount;
	}

	/**
	 * @return the totalPeriods
	 */
	public String getTotalPeriods() {
		return TotalPeriods;
	}

	/**
	 * @param totalPeriods
	 *            the totalPeriods to set
	 */
	public void setTotalPeriods(String totalPeriods) {
		TotalPeriods = totalPeriods;
	}

	/**
	 * @return the remainingPeriods
	 */
	public String getRemainingPeriods() {
		return RemainingPeriods;
	}

	/**
	 * @param remainingPeriods
	 *            the remainingPeriods to set
	 */
	public void setRemainingPeriods(String remainingPeriods) {
		RemainingPeriods = remainingPeriods;
	}

	/**
	 * @return the beginDate
	 */
	public String getBeginDate() {
		return BeginDate;
	}

	/**
	 * @param beginDate
	 *            the beginDate to set
	 */
	public void setBeginDate(String beginDate) {
		BeginDate = beginDate;
	}

	/**
	 * @return the endDate
	 */
	public String getEndDate() {
		return EndDate;
	}

	/**
	 * @param endDate
	 *            the endDate to set
	 */
	public void setEndDate(String endDate) {
		EndDate = endDate;
	}

	/**
	 * @return the installmentStatus
	 */
	public String getInstallmentStatus() {
		return InstallmentStatus;
	}

	/**
	 * @param installmentStatus
	 *            the installmentStatus to set
	 */
	public void setInstallmentStatus(String installmentStatus) {
		InstallmentStatus = installmentStatus;
	}
}
