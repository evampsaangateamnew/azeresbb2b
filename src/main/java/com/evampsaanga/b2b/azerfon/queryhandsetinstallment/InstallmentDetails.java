package com.evampsaanga.b2b.azerfon.queryhandsetinstallment;

import java.util.ArrayList;

public class InstallmentDetails {
	private String price = "";
	private String count = "";
	private String purchaseDate = "";
	private ArrayList<Installments> installments = new ArrayList<Installments>();

	public InstallmentDetails(String price, String count, String purchaseDate, ArrayList<Installments> installments) {
		super();
		this.price = price;
		this.count = count;
		this.purchaseDate = purchaseDate;
		this.installments = installments;
	}

	/**
	 * @return the price
	 */
	public String getPrice() {
		return price;
	}

	/**
	 * @param price
	 *            the price to set
	 */
	public void setPrice(String price) {
		this.price = price;
	}

	/**
	 * @return the count
	 */
	public String getCount() {
		return count;
	}

	/**
	 * @param count
	 *            the count to set
	 */
	public void setCount(String count) {
		this.count = count;
	}

	/**
	 * @return the purchaseDate
	 */
	public String getPurchaseDate() {
		return purchaseDate;
	}

	/**
	 * @param purchaseDate
	 *            the purchaseDate to set
	 */
	public void setPurchaseDate(String purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	/**
	 * @return the installments
	 */
	public ArrayList<Installments> getInstallments() {
		return installments;
	}

	/**
	 * @param installments
	 *            the installments to set
	 */
	public void setInstallments(ArrayList<Installments> installments) {
		this.installments = installments;
	}
}
