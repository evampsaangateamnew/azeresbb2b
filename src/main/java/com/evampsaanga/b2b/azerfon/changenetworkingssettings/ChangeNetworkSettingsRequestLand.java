package com.evampsaanga.b2b.azerfon.changenetworkingssettings;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.amqimplementationsesb.LogsType;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.ThirdPartyNames;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Decrypter;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.evampsaanga.b2b.developer.utils.SOAPLoggingHandler;
import com.huawei.crm.basetype.ens.ExtProductList;
import com.huawei.crm.basetype.ens.OrderInfo;
import com.huawei.crm.basetype.ens.OrderItemInfo;
import com.huawei.crm.basetype.ens.OrderItemValue;
import com.huawei.crm.basetype.ens.OrderItems;
import com.huawei.crm.basetype.ens.ProductInfo;
import com.huawei.crm.basetype.ens.SubscriberInfo;
import com.huawei.crm.service.ens.SubmitOrderRequest;
import com.huawei.crm.service.ens.SubmitOrderResponse;
import com.huawei.crm.service.ens.SubmitRequestBody;
import com.ngbss.evampsaanga.services.ThirdPartyCall;
import com.ngbss.evampsaanga.services.ThirdPartyRequestHeader;


@Path("/bakcell/")
public class ChangeNetworkSettingsRequestLand {
	public static final Logger logger = Logger.getLogger("azerfon-esb");

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ChangeNetworkSettingsResponse Get(@Header("credentials") String credential,
			@Header("Content-Type") String contentType, @Body() String requestBody) {
		ChangeNetworkSettingsResponse resp = new ChangeNetworkSettingsResponse();
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.CHANGE_NETWORK_SETTINGS);
		logs.setThirdPartyName(ThirdPartyNames.BAKCELL_ORDER_HANDLER);
		logs.setTableType(LogsType.ChangeNetworkSettings);
		try {
			logger.info("Request Landed on ChangeNetworkSettingsRequestLand:" + requestBody);
			ChangeNetworkSettingsRequest cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, ChangeNetworkSettingsRequest.class);
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					/// Call ChangeLanguage here
					SubmitOrderResponse response = getResponse(cclient);
					logger.info("response Change Network Settings "+ Helper.ObjectToJson(response));
					logger.info("responseCODE Change Network Settings "+ Helper.ObjectToJson(response.getResponseHeader().getRetCode()));
					if (response.getResponseHeader().getRetCode().equals("0")) {
						resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
						resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);	return resp;
					} else {
						resp.setReturnCode(response.getResponseHeader().getRetCode());
						resp.setReturnMsg(response.getResponseHeader().getRetMsg());
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);			return resp;
					}
				} else {
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(resp.getReturnCode());
		logs.setResponseDescription(resp.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	public static SubmitOrderResponse getResponse(ChangeNetworkSettingsRequest cclient) {
		SubmitOrderRequest submitOrderRequestMsgReq = new SubmitOrderRequest();
//		submitOrderRequestMsgReq.setRequestHeader(OrderHandleService.getReqHeaderForChangeNetworkSetting());
		submitOrderRequestMsgReq.setRequestHeader(ThirdPartyRequestHeader.getReqHeaderForChangeNetworkSetting());
		SubmitRequestBody submitRequestBody = new SubmitRequestBody();
		OrderInfo orderInfo = new OrderInfo();
		orderInfo.setOrderType(Constants.NETWORK_SETTINGS_ORDER_ITEM_TYPE);
		submitRequestBody.setOrder(orderInfo);
		
		
		OrderItems orderItems = new OrderItems();
		
		OrderItemValue orderItemValue = new OrderItemValue();
		
		
		OrderItemInfo orderItemInfo = new OrderItemInfo();
		orderItemInfo.setOrderItemType(Constants.NETWORK_SETTINGS_ORDER_ITEM_TYPE);
		orderItemValue.setOrderItemInfo(orderItemInfo);
		
		
		SubscriberInfo subscriber = new SubscriberInfo();
		subscriber.setServiceNumber(cclient.getmsisdn());
		/*subscriber.setLanguage(cclient.getLanguage());
		subscriber.setWrittenLanguage(cclient.getLanguage())*/;
		
		
		ProductInfo productnfo=new ProductInfo();
		productnfo.setProductId(cclient.getProductId());
		productnfo.setSelectFlag(cclient.getSelectFlag());
		
	
		ExtProductList extprodList=new ExtProductList();
		extprodList.getProductInfo().add(productnfo);
		subscriber.setNetworkSettingList(extprodList);
		
		orderItemValue.setSubscriber(subscriber);
		orderItems.getOrderItem().add(orderItemValue);
		submitRequestBody.setOrderItems(orderItems);
		submitOrderRequestMsgReq.setSubmitRequestBody(submitRequestBody);
//		return OrderHandleService.getInstance().submitOrder(submitOrderRequestMsgReq);
		return ThirdPartyCall.submitOrder(submitOrderRequestMsgReq);
	
	}
}