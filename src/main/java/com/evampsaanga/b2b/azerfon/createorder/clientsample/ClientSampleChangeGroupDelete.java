package com.evampsaanga.b2b.azerfon.createorder.clientsample;

import com.huawei.bss.soaif._interface.common.createorder.GroupSubscriberInfo;
import com.huawei.bss.soaif._interface.common.createorder.MemberSubscriberInfo;
import com.huawei.bss.soaif._interface.orderservice.createorder.CreateOrderReqMsg;
import com.huawei.bss.soaif._interface.orderservice.createorder.CreateOrderReqMsg.Order;

public class ClientSampleChangeGroupDelete {

	public static void main(String[] args) {
	        System.out.println("***********************");
	        System.out.println("Create Web Service Client...");
	        CreateOrderReqMsg createOrderReqMsg = new CreateOrderReqMsg();
	        createOrderReqMsg.setReqHeader(CreateOrderService.getCreateOderHeader());
	        Order order = new Order();
	        order.setOrderType("CG012");
			createOrderReqMsg.setOrder(order );
			
			GroupSubscriberInfo groupSubscriberInfo = new GroupSubscriberInfo();
			groupSubscriberInfo.setGroupId(1010003262919L);
			
			MemberSubscriberInfo memberSubscriberInfo = new MemberSubscriberInfo();
			memberSubscriberInfo.setMemberServiceNumber("555956002");
			
			createOrderReqMsg.setGroupSubscriber(groupSubscriberInfo);
			createOrderReqMsg.setGroupMember(memberSubscriberInfo);
			
			CreateOrderService.getInstance().createOrder(createOrderReqMsg);
	
	        System.out.println("***********************");
	        System.out.println("Call Over!");
	}
}
