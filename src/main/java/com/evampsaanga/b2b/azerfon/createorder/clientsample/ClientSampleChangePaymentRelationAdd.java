package com.evampsaanga.b2b.azerfon.createorder.clientsample;

import com.huawei.bss.soaif._interface.common.createorder.PaymentPlanInfo;
import com.huawei.bss.soaif._interface.common.createorder.PaymentRelation;
import com.huawei.bss.soaif._interface.common.createorder.PaymentRelation.PaymentLimit;
import com.huawei.bss.soaif._interface.orderservice.createorder.CreateOrderReqMsg;
import com.huawei.bss.soaif._interface.orderservice.createorder.CreateOrderReqMsg.Order;

public class ClientSampleChangePaymentRelationAdd {

	public static void main(String[] args) {
	        System.out.println("***********************");
	        System.out.println("Create Web Service Client...");
	        CreateOrderReqMsg createOrderReqMsg = new CreateOrderReqMsg();
	        createOrderReqMsg.setReqHeader(CreateOrderService.getCreateOderHeader());
	        Order order = new Order();
	        order.setOrderType("CO076");
			createOrderReqMsg.setOrder(order );
			PaymentPlanInfo paymentPlanInfo = new PaymentPlanInfo();
			paymentPlanInfo.setAccountID(1010003024611L);
			paymentPlanInfo.setServiceNumber("555956002");
			
			PaymentRelation paymentRelation = new PaymentRelation();
			paymentRelation.setActionType("1");
			paymentRelation.getServiceType().add("-1");
			PaymentLimit paymentLimit = new PaymentLimit();
			paymentLimit.setLimitMeasureUnit("101");
			paymentLimit.setLimitPattern("1");
			paymentLimit.setLimitUnit("1");
			paymentLimit.setLimitValue("15000000");
			paymentRelation.setPaymentLimit(paymentLimit );
			
			paymentPlanInfo.getPaymentRelationList().add(paymentRelation);
			createOrderReqMsg.setPaymentPlanList(paymentPlanInfo);
			
			CreateOrderService.getInstance().createOrder(createOrderReqMsg);
	
	        System.out.println("***********************");
	        System.out.println("Call Over!");
	}
}
