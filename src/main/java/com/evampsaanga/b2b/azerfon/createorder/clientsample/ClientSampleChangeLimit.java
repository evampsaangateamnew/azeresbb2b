package com.evampsaanga.b2b.azerfon.createorder.clientsample;

import com.huawei.bss.soaif._interface.common.createorder.CreditLimitInfo;
import com.huawei.bss.soaif._interface.common.createorder.ManageCreditLimit;
import com.huawei.bss.soaif._interface.orderservice.createorder.CreateOrderReqMsg;
import com.huawei.bss.soaif._interface.orderservice.createorder.CreateOrderReqMsg.Order;

public class ClientSampleChangeLimit {

	public static void main(String[] args) {
	        System.out.println("***********************");
	        System.out.println("Create Web Service Client...");
	        CreateOrderReqMsg createOrderReqMsg = new CreateOrderReqMsg();
	        createOrderReqMsg.setReqHeader(CreateOrderService.getCreateOderHeader());
	        Order order = new Order();
	        order.setOrderType("CO089");
			createOrderReqMsg.setOrder(order );
			
			ManageCreditLimit manageCreditLimit = new ManageCreditLimit();
			manageCreditLimit.setAccountId("1010002894359");
			
			CreditLimitInfo creditLimitInfo = new CreditLimitInfo();
			creditLimitInfo.setCreditLimitType("All");
			creditLimitInfo.setLimitClass("I");
			creditLimitInfo.setLimitValue(100000000);
			creditLimitInfo.setActionType("3");
			manageCreditLimit.setCreditLimitInfoList(creditLimitInfo);
	
			createOrderReqMsg.setManageCreditLimit(manageCreditLimit);
			CreateOrderService.getInstance().createOrder(createOrderReqMsg);
			
	        System.out.println("***********************");
	        System.out.println("Call Over!");
	}
}
