package com.evampsaanga.b2b.azerfon.getcoreservicesbulkV2;

import javax.xml.bind.annotation.XmlElement;

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;
/**
 * Request Container
 * @author Aqeel Abbas
 *
 */
public class GetCoreServicesRequest extends BaseRequest {
	
	@XmlElement(name = "userType", required = true)
	private String userType;
	@XmlElement(name = "brand", required = true)
	private String brand;
	
	@XmlElement(name = "accountType")
	private String accountType;
	
	@XmlElement(name = "groupType")
	private String groupType;
	
	@XmlElement(name = "isFrom")
	private String isFrom;
	
	
	public String getIsFrom() {
		return isFrom;
	}
	public void setIsFrom(String isFrom) {
		this.isFrom = isFrom;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getAccountType() {
		return accountType;
	}
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	public String getGroupType() {
		return groupType;
	}
	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}
	
	
	
}
