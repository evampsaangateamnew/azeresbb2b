package com.evampsaanga.b2b.azerfon.getcoreservicesbulkV2;

import java.util.ArrayList;

import com.evampsaanga.b2b.azerfon.getcoreservices.CoreServices;
/**
 * Response Container For Core Services
 * @author Aqeel Abbas
 *
 */
public class Data {
	private ArrayList<CoreServices> coreServices = new ArrayList<CoreServices>();

	/**
	 * @return the coreServices
	 */
	public ArrayList<CoreServices> getCoreServices() {
		return coreServices;
	}

	/**
	 * @param coreServices
	 *            the coreServices to set
	 */
	/*public void setCoreServices(ArrayList<CoreServices> coreServices) {
		this.coreServices = coreServices;
	}*/

	public void setCoreServices(ArrayList<CoreServices> coreServices2) {
		this.coreServices = coreServices2;
		
	}
}
