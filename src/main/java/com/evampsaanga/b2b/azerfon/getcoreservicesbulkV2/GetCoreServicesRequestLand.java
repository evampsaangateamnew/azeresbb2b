package com.evampsaanga.b2b.azerfon.getcoreservicesbulkV2;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.amqimplementationsesb.LogsType;
import com.evampsaanga.b2b.azerfon.getsubscriber.CRMSubscriberService;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.ThirdPartyNames;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Decrypter;
import com.evampsaanga.b2b.developer.utils.Helper;

/**
 * Service Class To Get Core Services
 * 
 * @author Aqeel Abbas
 *
 */
@Path("/bakcell")
public class GetCoreServicesRequestLand {
	public static final Logger logger = Logger.getLogger("azerfon-esb");
	/**
	 * 
	 * @param credential
	 * @param requestBody
	 * @return Array List Containing Core Services
	 */
	@POST
	@Path("/getbulk")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetCoreServicesResponse GetBulk(@Header("credentials") String credential, @Body() String requestBody) {
		GetCoreServicesResponse resp = new GetCoreServicesResponse();
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.GET_BULK_CORE_SERVICES_TRANSACTION_NAME_B2B);
		logs.setThirdPartyName(ThirdPartyNames.GET_CORE_SERVICES);
		logs.setTableType(LogsType.GetCoreServices);
		try {
			Helper.logInfoMessageV2("Request Landed on Get Core services List V2 With Data: " + requestBody);
			String credentials = null;
			GetCoreServicesRequest cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, GetCoreServicesRequest.class);
			} catch (Exception ex1) {
				logger.error(Helper.GetException(ex1));
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				if(cclient.getIsB2B()!=null && cclient.getIsB2B().equals("true"))
				{
					logs.setTransactionName(Transactions.GET_BULK_CORE_SERVICES_TRANSACTION_NAME_B2B);
				}
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					Helper.logInfoMessageV2(cclient.getmsisdn() + " - Credentials Verification Failed");
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				String verification = Helper.validateRequest(cclient);
				if (!verification.equals("")) {
					Helper.logInfoMessageV2(cclient.getmsisdn() + " - Request Verification Failed");
					resp.setReturnCode(ResponseCodes.ERROR_400);
					resp.setReturnMsg(verification);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					CRMSubscriberService crmSub = new CRMSubscriberService();
					resp.getData().setCoreServices(crmSub.getCoreServicesV2(cclient.getLang()));
					resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
					resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				} else {
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
				resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
			resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
			resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
	}
}
