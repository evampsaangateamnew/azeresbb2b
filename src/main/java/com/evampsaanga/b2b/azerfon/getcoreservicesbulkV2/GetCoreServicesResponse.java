package com.evampsaanga.b2b.azerfon.getcoreservicesbulkV2;

import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;
/**
 * Response Container For Core Services in bulk for Phase 2
 * @author EvampSaanga
 *
 */
public class GetCoreServicesResponse extends BaseResponse {
	private Data data = new Data();

	/**
	 * @return the data
	 */
	public Data getData() {
		return data;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	public void setData(Data data) {
		this.data = data;
	}
}
