package com.evampsaanga.b2b.azerfon.notificationscount;

import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;

public class NotificationCountResponseClient extends BaseResponse {
	Data data = new Data();

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}
}
