package com.evampsaanga.b2b.azerfon.notificationscount;



import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.amqimplementationsesb.LogsType;
import com.evampsaanga.b2b.azerfon.db.DBFactory;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.ThirdPartyNames;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Decrypter;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.evampsaanga.b2b.developer.utils.SOAPLoggingHandler;

@Path("/azerfon")
public class NotificationCountRequestLand {
	public static final Logger logger = Logger.getLogger("azerfon-esb");
	Logs logs = new Logs();

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public NotificationCountResponseClient Get(@Header("credentials") String credential,
			@Header("Content-Type") String contentType, @Body() String requestBody) {
		
		
		logs.setTransactionName(Transactions.NOTIFICATIONS_COUNT_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.GET_NOTIFICATION_COUNT);
		logs.setTableType(LogsType.GetNofificationsCount);
		try {
			logger.info("Request Landed on NotificationCountRequestLand:" + requestBody);
			NotificationCountRequestClient cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, NotificationCountRequestClient.class);
				if (cclient != null) {
					if(cclient.getIsB2B()!=null && cclient.getIsB2B().equals("true"))
						logs.setTransactionName(Transactions.NOTIFICATIONS_COUNT_TRANSACTION_NAME_B2B);
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
				}
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				NotificationCountResponseClient resp = new NotificationCountResponseClient();
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					NotificationCountResponseClient resp = new NotificationCountResponseClient();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						NotificationCountResponseClient res = new NotificationCountResponseClient();
						res.setReturnCode(ResponseCodes.ERROR_400);
						res.setReturnMsg(verification);
						logs.setResponseCode(res.getReturnCode());
						logs.setResponseDescription(res.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return res;
					}
				} else {
					NotificationCountResponseClient resp = new NotificationCountResponseClient();
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					
					//To do * Save log here in case of success
					return getResponse(cclient.getmsisdn(), cclient.getLang(), logs);
				} else {
					NotificationCountResponseClient resp = new NotificationCountResponseClient();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		NotificationCountResponseClient resp = new NotificationCountResponseClient();
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(resp.getReturnCode());
		logs.setResponseDescription(resp.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	public NotificationCountResponseClient getResponse(String msisdn, String lang, Logs logs)
			throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
		try {
			logger.info("NCA: Query for count");
			String sql = "Select count(*) as count from History where msisdn=? AND read_status=?";
			String dateString = "";
			ResultSet resultSet = null;
			logger.info("NCA: Getting connection");
			try {
				Connection myConnection = DBFactory.getAppConnection();
				PreparedStatement statement = myConnection.prepareStatement(sql);
				statement.setString(1, msisdn);
				statement.setInt(2, 1);
				logger.info("NCA: Executing Query");
				resultSet = statement.executeQuery();
				if (resultSet != null) {
					if (resultSet.next()) {
						dateString = resultSet.getString("count") + "";
					}
				}
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
			} finally {
				if (resultSet != null) {
					resultSet.close();
				}
			}
			Data data = new Data();
			data.setCount(dateString);
			data.setNotificationstatus("1");
			NotificationCountResponseClient rep11 = new NotificationCountResponseClient();
			rep11.setData(data);
			rep11.setReturnCode(ResponseCodes.SUCESS_CODE_200);
			rep11.setReturnMsg(ResponseCodes.SUCESS_DES_200);
			return rep11;
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		NotificationCountResponseClient resp = new NotificationCountResponseClient();
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(resp.getReturnCode());
		logs.setResponseDescription(resp.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		return resp;
	}
}
