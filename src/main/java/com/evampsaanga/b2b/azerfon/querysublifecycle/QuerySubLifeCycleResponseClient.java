package com.evampsaanga.b2b.azerfon.querysublifecycle;

import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;

public class QuerySubLifeCycleResponseClient extends BaseResponse {
	public Data data = new Data();

	public Data getData() {
		return data;
	}

	public void setData(Data object) {
		this.data = object;
	}
}
