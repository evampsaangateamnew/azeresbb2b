package com.evampsaanga.b2b.azerfon.querysublifecycle;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.developer.utils.Decrypter;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.evampsaanga.b2b.developer.utils.SOAPLoggingHandler;
import com.huawei.bme.cbsinterface.bccommon.SubAccessCode;
import com.huawei.bme.cbsinterface.bcservices.QuerySubLifeCycleRequest;
import com.huawei.bme.cbsinterface.bcservices.QuerySubLifeCycleRequestMsg;
import com.huawei.bme.cbsinterface.bcservices.QuerySubLifeCycleResultMsg;
import com.ngbss.evampsaanga.services.ThirdPartyCall;
import com.ngbss.evampsaanga.services.ThirdPartyRequestHeader;

@Path("/bakcell")
public class QuerySubLifeCycleRequestLand {
	public static final Logger logger = Logger.getLogger("azerfon-esb");

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public QuerySubLifeCycleResponseClient Get(@Header("credentials") String credential,
			@Header("Content-Type") String contentType, @Body() String requestBody) {
		QuerySubLifeCycleResponseClient resp = new QuerySubLifeCycleResponseClient();
		Logs logs = new Logs();
		try {
			logger.info("Request Landed Life sub cycle:" + requestBody);
			QuerySubLifeCycleRequestClient cclient = new QuerySubLifeCycleRequestClient();
			try {
				cclient = Helper.JsonToObject(requestBody, QuerySubLifeCycleRequestClient.class);
				if (cclient != null) {
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());logs.setLang(cclient.getLang());
				}
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			 logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				 logs.updateLog(logs);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						resp.setReturnCode(ResponseCodes.ERROR_400);
						resp.setReturnMsg(verification);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					 logs.updateLog(logs);
						return resp;
					}
				} else {
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					 logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					try {
						QuerySubLifeCycleResultMsg response = getsublife(cclient.getmsisdn());
						if (response.getResultHeader().getResultCode().equals("0")) {
							Data object = new Data();
							object.setResult(response.getQuerySubLifeCycleResult());
							resp.setData(object);
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							 logs.updateLog(logs);	return resp;
						} else {
							resp.setReturnCode(response.getResultHeader().getResultCode());
							resp.setReturnMsg(response.getResultHeader().getResultDesc());
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							// logs.updateLog(logs);
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							 logs.updateLog(logs);	return resp;
						}
					} catch (Exception e) {
						logger.error(Helper.GetException(e));
					}
				} else {
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(resp.getReturnCode());
		logs.setResponseDescription(resp.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
	 logs.updateLog(logs);
		return resp;
	}

	public QuerySubLifeCycleResultMsg getsublife(String msisdn) {
		QuerySubLifeCycleRequestMsg requestMsg = new QuerySubLifeCycleRequestMsg();
		requestMsg.setRequestHeader(ThirdPartyRequestHeader.getCBserviceRequestHeader());
//		requestMsg.setRequestHeader(BcService.getRequestHeader());
		QuerySubLifeCycleRequest lifeC = new QuerySubLifeCycleRequest();
		SubAccessCode subACode = new SubAccessCode();
		subACode.setPrimaryIdentity(msisdn);
		lifeC.setSubAccessCode(subACode);
		requestMsg.setQuerySubLifeCycleRequest(lifeC);
		QuerySubLifeCycleResultMsg result = ThirdPartyCall.getQuerySubLifeCycle(requestMsg);
//		QuerySubLifeCycleResultMsg result = BcService.getInstance().querySubLifeCycle(requestMsg);
		return result;
	}
}
