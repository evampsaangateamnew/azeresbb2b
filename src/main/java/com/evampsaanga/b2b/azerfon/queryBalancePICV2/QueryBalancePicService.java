package com.evampsaanga.b2b.azerfon.queryBalancePICV2;
/**
 * @author Aqeel Abbas
 * Queries the balance of pic
 */

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.amqimplementationsesb.LogsType;
import com.evampsaanga.b2b.authorization.AuthorizationAndAuthentication;
import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.ThirdPartyNames;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.huawei.bme.cbsinterface.arservices.QueryBalanceRequest;
import com.huawei.bme.cbsinterface.arservices.QueryBalanceRequest.QueryObj;
import com.huawei.bme.cbsinterface.arservices.QueryBalanceRequest.QueryObj.AcctAccessCode;
import com.huawei.bme.cbsinterface.arservices.QueryBalanceRequestMsg;
import com.huawei.bme.cbsinterface.arservices.QueryBalanceResult.AcctList;
import com.huawei.bme.cbsinterface.arservices.QueryBalanceResult.AcctList.AccountCredit;
import com.huawei.bme.cbsinterface.arservices.QueryBalanceResult.AcctList.AccountCredit.CreditAmountInfo;
import com.huawei.bme.cbsinterface.arservices.QueryBalanceResult.AcctList.OutStandingList;
import com.huawei.bme.cbsinterface.arservices.QueryBalanceResult.AcctList.OutStandingList.OutStandingDetail;
import com.huawei.bme.cbsinterface.arservices.QueryBalanceResultMsg;
import com.huawei.cbs.ar.wsservice.arcommon.AcctBalance;
import com.huawei.cbs.ar.wsservice.arcommon.AcctBalance.BalanceDetail;
import com.ngbss.evampsaanga.services.ThirdPartyCall;
import com.ngbss.evampsaanga.services.ThirdPartyRequestHeader;

@Path("/bakcell")
public class QueryBalancePicService {
	public static final Logger logger = Logger.getLogger("azerfon-esb");

	/**
	 * Generates Balance for PIC 
	 * @param requestBody
	 * @param credential
	 * @return Returns balace of PIC 
	 * @throws IOException
	 * @throws Exception
	 */
	@POST
	@Path("/querybalancepic")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public QueryBalancePicResponse queryBalancePic(@Body String requestBody, @Header("credentials") String credential)
			throws IOException, Exception {

		// Below is declaration block for variables to be used
		// Logs object to store values which are to be inserted in database for
		// reporting
		Logs logs = new Logs();
		QueryBalancePicResponse queryBalancePicResponse = new QueryBalancePicResponse();
		QueryBalancePicRequest queryBalancePicRequest = new QueryBalancePicRequest();
		QueryBalancePicResponseData queryBalancePicResponseData = new QueryBalancePicResponseData();
		Long accountCredit = new Long(0L);
		Long remainingAmount = new Long(0L);
		Long OutStandingAmount = new Long(0L);
		Long unbilledBalance = new Long(0L);
		Long balanceCredit = new Long(0L);
		Long totalUsageAmount = new Long(0L);
		Long usageDetails=new Long(0L);
		String typeBalance="";
		 Long m2Mbalance=new Long(0L);
		 String freResources="false";
		// Generic information about logs is being set to logs object
		logs.setRequestDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.setTransactionName(Transactions.QUERY_BALANCE_PIC);
		logs.setThirdPartyName(ThirdPartyNames.LOGIN);
		logs.setTableType(LogsType.CustomerData);
		// Authenticating the request using credentials string received in
		// header
		boolean authenticationresult = AuthorizationAndAuthentication.authenticateAndAuthorizeCredentials(credential);

		// if request is not authenticated adding unauthorized response
		// codes to response and logs object
		if (!authenticationresult)
			prepareErrorResponse(queryBalancePicResponse, logs, ResponseCodes.ERROR_401_CODE, ResponseCodes.ERROR_401);

		try {
			queryBalancePicRequest = Helper.JsonToObject(requestBody, QueryBalancePicRequest.class);
			// Logging incoming request to log file

			// logger.info(queryBalancePicRequest.getmsisdn() + " - Query
			// queryBalancePic start");
			Helper.logInfoMessageV2(queryBalancePicRequest.getmsisdn()
					+ " - Request lanaded on queryBalancePic with data as :" + requestBody);
		} catch (Exception ex) {
			// block catches the exception from mapper and sets the 400 bad
			// request response code
			logger.error(Helper.GetException(ex));
			prepareErrorResponse(queryBalancePicResponse, logs, ResponseCodes.ERROR_400_CODE, ResponseCodes.ERROR_400);
		}
		if (queryBalancePicRequest != null && authenticationresult) {
			Helper.logInfoMessageV2("Authentication successfull");
			QueryBalanceRequestMsg queryBalanceRequestMsg = new QueryBalanceRequestMsg();
			QueryBalanceRequest BalanceRequest = new QueryBalanceRequest();
			QueryObj QueryObj = new QueryObj();
			AcctAccessCode accessCode = new AcctAccessCode();
			logger.info("#################################################3");
			logger.info("Testing:"+queryBalancePicRequest.getAccountCode());
			logger.info("#################################################3");
			accessCode.setAccountCode(queryBalancePicRequest.getAccountCode());
			QueryObj.setAcctAccessCode(accessCode);
			BalanceRequest.setQueryObj(QueryObj);
			queryBalanceRequestMsg.setQueryBalanceRequest(BalanceRequest);
//			queryBalanceRequestMsg.setRequestHeader(CBSARService.getRequestPaymentRequestHeader());
			queryBalanceRequestMsg.setRequestHeader(ThirdPartyRequestHeader.getRequestPaymentRequestHeader());
			// reponse from api
//			QueryBalanceResultMsg resultMsg = CBSARService.getInstance().queryBalance(queryBalanceRequestMsg);
			QueryBalanceResultMsg resultMsg = ThirdPartyCall.getQueryBalance(queryBalanceRequestMsg);
			/*Helper.logInfoMessageV2(
					queryBalancePicRequest.getmsisdn() + "- Response from CBSAR: " + Helper.ObjectToJson(resultMsg));*/
			try {
				List<AcctList> resultMsgData = new ArrayList<>();
				if(resultMsg.getQueryBalanceResult() !=null && resultMsg.getQueryBalanceResult().getAcctList() !=null){
					resultMsgData = resultMsg.getQueryBalanceResult().getAcctList();		
				}
				if (resultMsgData != null) {
					for (AcctList item : resultMsgData) {
						// getting total account credit and total remaining
						// credit	
						//
						List<AcctBalance> balanceResult=item.getBalanceResult();
						for (AcctBalance balance : balanceResult) {
							//unbilledBalance += credits.getTotalUsageAmount();
							//TODO check if the list can have multiple records in the amount info  CreditAmountInfo
							 typeBalance=balance.getBalanceType();
						
							 if
								 (ConfigurationManager.getConfigurationFromCache("home.page.M2MBalance."+typeBalance).equals(Constants.M2MBalance)) 
							 {
								 freResources="true";
								
								List<BalanceDetail> balanceDetails=balance.getBalanceDetail();
								for (BalanceDetail details : balanceDetails)
								{
									usageDetails=usageDetails+details.getInitialAmount();
									m2Mbalance=m2Mbalance+details.getAmount();
								}
							 }
							
						}
						///

						List<AccountCredit> accountCredits = item.getAccountCredit();
						
						for (AccountCredit credits : accountCredits) {
							//unbilledBalance += credits.getTotalUsageAmount();
							//TODO check if the list can have multiple records in the amount info  CreditAmountInfo
							for (CreditAmountInfo acctinfo : credits.getCreditAmountInfo()) {
								accountCredit += acctinfo.getAmount();
							}
							//accountCredit += credits.getCreditAmountInfo().get(0).getAmount();
							remainingAmount += credits.getTotalRemainAmount();
							balanceCredit += credits.getTotalCreditAmount();
							totalUsageAmount=credits.getTotalUsageAmount();
						}
						// total outstanding amount
						List<OutStandingList> outStandingAmount = item.getOutStandingList();
						for (OutStandingList outstdList : outStandingAmount) {
							List<OutStandingDetail> outStandingDetails = outstdList.getOutStandingDetail();
							for (OutStandingDetail outStandingDetail : outStandingDetails) {
								OutStandingAmount += outStandingDetail.getOutStandingAmount();
							}
						}
					}
					// unbilledBalance calculation
				//	unbilledBalance =  (balanceCredit - remainingAmount - OutStandingAmount);
					unbilledBalance =  (totalUsageAmount - OutStandingAmount);
					NumberFormat df = new DecimalFormat("#0.00");
					df.setRoundingMode(RoundingMode.FLOOR);
					String remainingAmountstr = df.format(remainingAmount.doubleValue() / Constants.MONEY_DIVIDEND);
					String accountCreditstr = df.format(accountCredit.doubleValue() / Constants.MONEY_DIVIDEND);
					String unbilledBalancestr = df.format(unbilledBalance.doubleValue() / Constants.MONEY_DIVIDEND);
					String OutStandingAmountstr = df.format(OutStandingAmount.doubleValue() / Constants.MONEY_DIVIDEND);
					String m2mBalanceStr=df.format(m2Mbalance.doubleValue() / Constants.MONEY_DIVIDEND);
					String usageDetaildBalance=df.format(usageDetails.doubleValue() / Constants.MONEY_DIVIDEND);
							
					if(!unbilledBalancestr.equals("0.00"))
						unbilledBalancestr = "-" + unbilledBalancestr;
					if(!OutStandingAmountstr.equals("0.00"))
						OutStandingAmountstr = "-" + OutStandingAmountstr;
					
					String balCredit = df.format(balanceCredit.doubleValue() / Constants.MONEY_DIVIDEND);
					//unbilledBalance = accountCredit - remainingAmount - OutStandingAmount;
					// setting response
					queryBalancePicResponseData.setAvailable_credit(((remainingAmountstr)));
					queryBalancePicResponseData.setTotal_limit((accountCreditstr));
					queryBalancePicResponseData.setUnbilled_balance((unbilledBalancestr));
					queryBalancePicResponseData.setOutstanding_debt((OutStandingAmountstr));
					queryBalancePicResponseData.setBalanceCredit((balCredit));
					queryBalancePicResponseData.setBalanceM2m(m2mBalanceStr);
					queryBalancePicResponseData.setBalanceM2mInitial(usageDetaildBalance);
					queryBalancePicResponseData.setAvailableBalanceLabel(ConfigurationManager
							.getConfigurationFromCache("home.page.availableBalance.label."+queryBalancePicRequest.getLang()));
					queryBalancePicResponseData.setTotalLimitLabel(ConfigurationManager
							.getConfigurationFromCache("home.page.totalLimit.label."+queryBalancePicRequest.getLang()));
					queryBalancePicResponseData.setUnbilledBalanceLabel(ConfigurationManager
							.getConfigurationFromCache("home.page.unbilledBalance.label."+queryBalancePicRequest.getLang()));
					queryBalancePicResponseData.setOutstandingDebtLabel(ConfigurationManager
							.getConfigurationFromCache("home.page.outstandingDebt.label."+queryBalancePicRequest.getLang()));
					queryBalancePicResponse.setReturnCode(ResponseCodes.SUCESS_CODE_200);
					queryBalancePicResponse.setReturnMsg(ResponseCodes.SUCESS_DES_200);
					queryBalancePicResponse.setQueryBalancePicResponseData(queryBalancePicResponseData);
					logs.setResponseCode(queryBalancePicResponse.getReturnCode());
					logs.setResponseDescription(queryBalancePicResponse.getReturnMsg());
					logs.updateLog(logs);
				}
			} catch (Exception e) {
				logger.error("ERROR:", e);
				queryBalancePicResponse.setReturnCode(ResponseCodes.ERROR_400_CODE);
				queryBalancePicResponse.setReturnMsg(ResponseCodes.ERROR_400);
				queryBalancePicResponse.setQueryBalancePicResponseData(null);

			}

		}
		Helper.logInfoMessageV2(queryBalancePicRequest.getmsisdn() +" - Query queryBalancePic End");
		Helper.logInfoMessageV2("Response To Be dispatched " + Helper.ObjectToJson(queryBalancePicResponse));
		return queryBalancePicResponse;
	}

	/**
	 * Prepares the error respose and update logs queue
	 * @param queryBalancePicResponse
	 * @param logs
	 * @param returnCode
	 * @param returnMessage
	 */
	public void prepareErrorResponse(QueryBalancePicResponse queryBalancePicResponse, Logs logs, String returnCode,
			String returnMessage) {
		queryBalancePicResponse.setReturnCode(returnCode);
		queryBalancePicResponse.setReturnMsg(returnMessage);
		logs.setResponseCode(queryBalancePicResponse.getReturnCode());
		logs.setResponseDescription(queryBalancePicResponse.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);

	}
	public static void main(String[] args) {
		NumberFormat df = new DecimalFormat("#0.00");
		df.setRoundingMode(RoundingMode.FLOOR);
		
		BigDecimal aString =new BigDecimal( df.format((30000000/Constants.MONEY_DIVIDEND)));
		System.out.println(aString + "<>" );
		
	}
}
