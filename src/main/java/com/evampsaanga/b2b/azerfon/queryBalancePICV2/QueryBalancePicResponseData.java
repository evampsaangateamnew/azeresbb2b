package com.evampsaanga.b2b.azerfon.queryBalancePICV2;

public class QueryBalancePicResponseData {
	private String total_limit;
	private String available_credit;
	private String outstanding_debt;
	private String unbilled_balance;
	private String balanceCredit;
	private String balanceM2m;
	private String balanceM2mInitial;
	private String isFreeResources;
	private String availableBalanceLabel;
	private String totalLimitLabel;
	private String unbilledBalanceLabel;
	private String outstandingDebtLabel;

	public String getBalanceM2mInitial() {
		return balanceM2mInitial;
	}

	public void setBalanceM2mInitial(String balanceM2mInitial) {
		this.balanceM2mInitial = balanceM2mInitial;
	}

	public String getTotalLimitLabel() {
		return totalLimitLabel;
	}

	public void setTotalLimitLabel(String totalLimitLabel) {
		this.totalLimitLabel = totalLimitLabel;
	}

	public String getUnbilledBalanceLabel() {
		return unbilledBalanceLabel;
	}

	public void setUnbilledBalanceLabel(String unbilledBalanceLabel) {
		this.unbilledBalanceLabel = unbilledBalanceLabel;
	}

	public String getOutstandingDebtLabel() {
		return outstandingDebtLabel;
	}

	public void setOutstandingDebtLabel(String outstandingDebt) {
		this.outstandingDebtLabel = outstandingDebt;
	}

	public String getAvailableBalanceLabel() {
		return availableBalanceLabel;
	}

	public void setAvailableBalanceLabel(String availableBalanceLabel) {
		this.availableBalanceLabel = availableBalanceLabel;
	}

	public String getBalanceM2m() {
		return balanceM2m;
	}

	public void setBalanceM2m(String balanceM2m) {
		this.balanceM2m = balanceM2m;
	}

	public String getIsFreeResources() {
		return isFreeResources;
	}

	public void setIsFreeResources(String isFreeResources) {
		this.isFreeResources = isFreeResources;
	}

	public String getTotal_limit() {
		return total_limit;
	}

	public void setTotal_limit(String total_limit) {
		this.total_limit = total_limit;
	}

	public String getAvailable_credit() {
		return available_credit;
	}

	public void setAvailable_credit(String available_credit) {
		this.available_credit = available_credit;
	}

	public String getOutstanding_debt() {
		return outstanding_debt;
	}

	public void setOutstanding_debt(String outstanding_debt) {
		this.outstanding_debt = outstanding_debt;
	}

	public String getUnbilled_balance() {
		return unbilled_balance;
	}

	public void setUnbilled_balance(String unbilled_balance) {
		this.unbilled_balance = unbilled_balance;
	}

	public String getBalanceCredit() {
		return balanceCredit;
	}

	public void setBalanceCredit(String balanceCredit) {
		this.balanceCredit = balanceCredit;
	}

}
