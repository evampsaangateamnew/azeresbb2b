package com.evampsaanga.b2b.azerfon.queryBalancePICV2;

import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;

public class QueryBalancePicResponse extends BaseResponse{
	QueryBalancePicResponseData queryBalancePicResponseData;

	public QueryBalancePicResponseData getQueryBalancePicResponseData() {
		return queryBalancePicResponseData;
	}

	public void setQueryBalancePicResponseData(QueryBalancePicResponseData queryBalancePicResponseData) {
		this.queryBalancePicResponseData = queryBalancePicResponseData;
	}
	
}
