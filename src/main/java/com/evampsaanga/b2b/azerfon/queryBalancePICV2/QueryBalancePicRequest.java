package com.evampsaanga.b2b.azerfon.queryBalancePICV2;

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class QueryBalancePicRequest extends BaseRequest{
	
	private String customerID;
	private String type;
	private String pageNum;
	private String virtualCorpCode;
	private String accountCode;
	private String limitUsers;
	private String searchUsers;
	private String groupType;

	public String getPageNum() {
		return pageNum;
	}

	public void setPageNum(String pageNum) {
		this.pageNum = pageNum;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCustomerID() {
		return customerID;
	}

	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}

	public String getVirtualCorpCode() {
		return virtualCorpCode;
	}

	public void setVirtualCorpCode(String virtualCorpCode) {
		this.virtualCorpCode = virtualCorpCode;
	}

	public String getAccountCode() {
		return accountCode;
	}

	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}

	public String getLimitUsers() {
		return limitUsers;
	}

	public void setLimitUsers(String limitUsers) {
		this.limitUsers = limitUsers;
	}

	public String getSearchUsers() {
		return searchUsers;
	}

	public void setSearchUsers(String searchUsers) {
		this.searchUsers = searchUsers;
	}

	public String getGroupType() {
		return groupType;
	}

	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}

}
