package com.evampsaanga.b2b.azerfon.callforwarding;

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;

public class CallForwardRequest extends BaseRequest {
	private String actionType = "";
	private String number = "";
	private String offeringId = "";

	private String userType;
	private String brand;
	private String accountType;
	private String groupType;
	
	
	/**
	 * @return the actionType
	 */
	public String getActionType() {
		return actionType;
	}

	/**
	 * @param actionType
	 *            the actionType to set
	 */
	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	/**
	 * @return the number
	 */
	public String getNumber() {
		return number;
	}

	/**
	 * @param number
	 *            the number to set
	 */
	public void setNumber(String number) {
		this.number = number;
	}

	/**
	 * @return the offeringId
	 */
	public String getOfferingId() {
		return offeringId;
	}

	/**
	 * @param offeringId
	 *            the offeringId to set
	 */
	public void setOfferingId(String offeringId) {
		this.offeringId = offeringId;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getGroupType() {
		return groupType;
	}

	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}
	
}
