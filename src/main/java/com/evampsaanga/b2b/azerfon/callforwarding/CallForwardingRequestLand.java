package com.evampsaanga.b2b.azerfon.callforwarding;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.amqimplementationsesb.LogsType;
import com.evampsaanga.b2b.azerfon.callForwardService.CallForwardService;
import com.evampsaanga.b2b.azerfon.coreservices.CoreServicesIndividualLandV2;
import com.evampsaanga.b2b.azerfon.getcoreservices.GetCoreServicesRequest;
import com.evampsaanga.b2b.azerfon.getcoreservices.GetCoreServicesRequestLand;
import com.evampsaanga.b2b.azerfon.getcoreservices.GetCoreServicesResponse;
import com.evampsaanga.b2b.azerfon.getcustomerrequest.GetCustomerDataLand;
import com.evampsaanga.b2b.azerfon.getcustomerrequest.GetCustomerRequestClient;
import com.evampsaanga.b2b.azerfon.getcustomerrequest.GetCustomerRequestResponse;
import com.evampsaanga.b2b.azerfon.getcustomerrequest.OfferingInfo;
import com.evampsaanga.b2b.azerfon.getsubscriber.CoreServicesCategoryItem;
import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.ThirdPartyNames;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Decrypter;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.evampsaanga.b2b.developer.utils.SOAPLoggingHandler;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.huawei.crm.service.ens.SubmitOrderResponse;

@Path("/azerfon")
public class CallForwardingRequestLand {
	public static final Logger logger = Logger.getLogger("azerfon-esb");

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public CallForwardResponse Get(@Header("credentials") String credential, @Header("Content-Type") String contentType,
			@Body() String requestBody) {
		CallForwardResponse resp = new CallForwardResponse();
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.CALL_FORWARDING_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.CALL_FORWARDING);
		logs.setTableType(LogsType.CallForwarding);
		String TrnsactionName = Transactions.CALL_FORWARDING_TRANSACTION_NAME;
		String token = "";
		try {
			token = Helper.retrieveToken(TrnsactionName, Helper.getValueFromJSON(requestBody, "msisdn"));
			logger.info(token + " Request on CallForwardingRequestLand Land:" + requestBody);
			CallForwardRequest cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, CallForwardRequest.class);
			} catch (Exception ex) {
				logger.info(token + Helper.GetException(ex));
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {

					logger.info(token + "Upper if 401" + Helper.ObjectToJson(resp));
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						resp.setReturnCode(ResponseCodes.ERROR_400);
						resp.setReturnMsg(verification);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				} else {
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					if (cclient.getOfferingId().equals("")
							|| (!cclient.getActionType().equals("1") && !cclient.getActionType().equals("3"))) {
						resp.setReturnCode(ResponseCodes.ERROR_400);
						resp.setReturnMsg("Action or offeringId invalid");
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
					CallForwardService callService = new CallForwardService();
					if (cclient.getNumber().equals("")) {
						// Send to IVR
						SubmitOrderResponse response = null;
						logger.info(token + "IN thi check if  cclient.getNumber().equals()");
						if (cclient.getOfferingId().equalsIgnoreCase(Constants.I_CALLED_YOU_OFFERING_ID)) {
							if (cclient.getActionType().equalsIgnoreCase("1")) {

								response = callService.callForwardServiceTOIVR(cclient.getActionType(),
										Constants.I_CALLED_YOU_OFFERING_ID, Constants.I_AM_BACK_OFFERING_ID, null,
										cclient.getmsisdn());
							} else if (cclient.getActionType().equalsIgnoreCase("3")) {
								/*
								 * if (!checkIfIamBackActivated(cclient).
								 * equalsIgnoreCase("Inactive")) response =
								 * callService.callForwardServiceTOIVR(cclient.
								 * getActionType(), cclient.getOfferingId(),
								 * Constants.I_AM_BACK_OFFERING_ID,
								 * cclient.getmsisdn()); else {
								 */
								response = callService.callForwardServiceTOIVR(cclient.getActionType(),
										cclient.getOfferingId(), null, null, cclient.getmsisdn());
								// }
							}
						} else
							response = callService.callForwardServiceTOIVR(cclient.getActionType(),
									cclient.getOfferingId(), null, null, cclient.getmsisdn());
						if (response != null && response.getResponseHeader().getRetCode().equals("0")) {
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						} else if (null != response) {
							resp.setReturnMsg(response.getResponseHeader().getRetMsg());
							resp.setReturnCode(response.getResponseHeader().getRetCode());
						} else {
							resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
							resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
						}
					} else {
						// send to number
						SubmitOrderResponse response = null;
						if (cclient.getNumber().startsWith("0")) {
							resp.setReturnMsg(ResponseCodes.CALL_FORWARD_TO_INVALID_NUMBER_DES);
							resp.setReturnCode(ResponseCodes.CALL_FORWARD_TO_INVALID_NUMBER_CODE);
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
						if (!cclient.getmsisdn().equalsIgnoreCase(cclient.getNumber())) {

							if (cclient.getActionType().equals("1")
									&& cclient.getOfferingId().equals(Constants.I_CALLED_YOU_OFFERING_ID)) {
								String forwardnumberuo = "994" + cclient.getNumber();
								cclient.setNumber(forwardnumberuo);

							}
							response = callService.callForwardServiceTOMSISDN(cclient.getActionType(),
									cclient.getOfferingId(), cclient.getmsisdn(), cclient.getNumber());
						} else {
							resp.setReturnMsg(ResponseCodes.CALL_FORWARD_TO_SAME_NUMBER_DES);
							resp.setReturnCode(ResponseCodes.CALL_FORWARD_TO_SAME_NUMBER_CODE);
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
						if (response.getResponseHeader().getRetCode().equals("0")) {
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						} else {
							resp.setReturnMsg(response.getResponseHeader().getRetMsg());
							resp.setReturnCode(response.getResponseHeader().getRetCode());
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						}
					}
					logs.updateLog(logs);
					return resp;
				} else {

					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					logger.info(token + "Outer else 401" + Helper.ObjectToJson(resp));
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.info(token + Helper.GetException(ex));
		}
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(resp.getReturnCode());
		logs.setResponseDescription(resp.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	// for pocessing core services in bulk

	@SuppressWarnings({ "null", "unused" })
	public CallForwardResponse processCoreServices(ProcessCoreServicesRequestV2 cclient, String token) {
		CallForwardResponse resp = new CallForwardResponse();
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.CALL_FORWARDING_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.CALL_FORWARDING);
		logs.setTableType(LogsType.CallForwarding);
		logger.info("Request Landed on Process Core Services Bulk Land new:");
		// boolean
		// value=flagTocheckNetworkSettingsOfferingIds(cclient.getOfferingId());
		try {
			if (cclient != null) {
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						resp.setReturnCode(ResponseCodes.ERROR_400);
						resp.setReturnMsg(verification);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					} else {
						if (cclient.getOfferingId().equals("")
								|| (!cclient.getActionType().equals("1") && !cclient.getActionType().equals("3"))) {
							resp.setReturnCode(ResponseCodes.ERROR_400);
							resp.setReturnMsg("Action or offeringId invalid");
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
						// Start of Adding ChangeNetworkSettings

						// else if
						// (flagTocheckNetworkSettingsOfferingIds(cclient.getOfferingId()))
						// {
						//
						// logger.info("IN NETWOrk Settings IFELSE " +
						// cclient.getOfferingId());
						// ChangeNetworkSettingsRequest cclient1 = new
						// ChangeNetworkSettingsRequest();
						// cclient1.setProductId(cclient.getOfferingId());
						// // For Change NetwrokSettings Close: 1 , Open: 2,
						// // and in request FROM user we are getting 1 for
						// // Activate, 3 for deactivate
						// // to call thirdParty API
						// if (cclient.getActionType().equals("1")) {
						// cclient1.setSelectFlag("2");
						// }
						// if (cclient.getActionType().equals("3")) {
						// cclient1.setSelectFlag("1");
						// }
						// logger.info("FLAG NETWOrk Settings IFELSE " +
						// cclient1.getSelectFlag());
						// cclient1.setProductId(cclient.getOfferingId());
						// cclient1.setMsisdn(cclient.getmsisdn());
						// SubmitOrderResponse responsechangeNetworkSettings =
						// com.evampsaanga.azerfon.changenetworkingssettings.ChangeNetworkSettingsRequestLand
						// .getResponse(cclient1);
						// if
						// (responsechangeNetworkSettings.getResponseHeader().getRetCode().equals("0"))
						// {
						// resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
						// resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
						// logs.setResponseCode(resp.getReturnCode());
						// logs.setResponseDescription(resp.getReturnMsg());
						// logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						// logs.updateLog(logs);
						// return resp;
						// } else {
						// resp.setReturnCode(responsechangeNetworkSettings.getResponseHeader().getRetCode());
						// resp.setReturnMsg(responsechangeNetworkSettings.getResponseHeader().getRetMsg());
						// logs.setResponseCode(resp.getReturnCode());
						// logs.setResponseDescription(resp.getReturnMsg());
						// logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						// logs.updateLog(logs);
						// return resp;
						// }
						//
						// }

						// End OF Adding ChangeNetworkSettings
						else {
							CallForwardService callService = new CallForwardService();
							SubmitOrderResponse response = null;
							if (cclient.getNumber() == null) {
								cclient.setNumber("");
							}
							if (cclient.getNumber().equals("")) {

								// Send to IVR

								CoreServicesIndividualLandV2 coreServicesLandV2 = new CoreServicesIndividualLandV2();
								GetCoreServicesResponse coreServicesResponseList = new GetCoreServicesResponse();
								GetCoreServicesRequest requestCoreServiceList = new GetCoreServicesRequest();
								requestCoreServiceList.setLang(cclient.getLang());
								requestCoreServiceList.setiP(cclient.getiP());
								requestCoreServiceList.setIsB2B("1");
								requestCoreServiceList.setChannel(cclient.getChannel());
								requestCoreServiceList.setMsisdn(cclient.getmsisdn());
								requestCoreServiceList.setSelectedMsisdn(cclient.getmsisdn());
								requestCoreServiceList.setUserType(cclient.getPicType());

								String jsonRequest = Helper.ObjectToJson(requestCoreServiceList);
								logger.info("Calling GET CORE SERVICES");
								coreServicesResponseList = coreServicesLandV2.Get(Constants.CREDENTIALS, jsonRequest);
								
								logger.info("CORE SERVICES LIST SIZE IS:::"+coreServicesResponseList.getData().getCoreServices().size());
								logger.info("RESPONSE LIST OF CORE SERVICES  "+new ObjectMapper().writeValueAsString(coreServicesResponseList));
								

								if (cclient.getOfferingId()
										.equalsIgnoreCase(Constants.CORE_SERVICES_ROAMING_INTERNATIONAL_NATIONAL)) {
									// Action Type = 1 is for activation.
									if (cclient.getActionType().equalsIgnoreCase("1")) {

										CoreServicesCategoryItem item = coreServicesResponseList.getData()
												.getCoreServices().stream()
												.flatMap(a -> a.getCoreServicesList().stream())
												.filter(b -> b.getOfferingId()
														.equalsIgnoreCase(Constants.CORE_SERVICES_INTERNATIONAL_ACCESS)
														&& b.getStatus().equalsIgnoreCase("inactive"))
												.findFirst().orElse(null);

										if (item != null) {
											response = callService.callForwardServiceTOIVR(cclient.getActionType(),
													Constants.CORE_SERVICES_ROAMING_INTERNATIONAL_NATIONAL,
													Constants.CORE_SERVICES_EXTENDED_ROAMING,
													Constants.CORE_SERVICES_INTERNATIONAL_ACCESS, cclient.getmsisdn());
										}

										else {
											response = callService.callForwardServiceTOIVR(cclient.getActionType(),
													Constants.CORE_SERVICES_ROAMING_INTERNATIONAL_NATIONAL,
													Constants.CORE_SERVICES_EXTENDED_ROAMING, null,
													cclient.getmsisdn());
										}

									} else if (cclient.getActionType().equalsIgnoreCase("3")) {

										response = callService.callForwardServiceTOIVR(cclient.getActionType(),
												Constants.CORE_SERVICES_ROAMING_INTERNATIONAL_NATIONAL,
												Constants.CORE_SERVICES_EXTENDED_ROAMING, null, cclient.getmsisdn());

										// if (response != null &&
										// response.getResponseHeader().getRetCode().equals("0"))
										// {
										// logger.info("Response From NGBSSS For
										// CORE_SERVICES_INTERNATIONAL_NATIONAL
										// & CORE_SERVICES_EXTENDED_ROAMING : "
										// + Helper.ObjectToJson(response));
										// response =
										// callService.callForwardServiceTOIVR(cclient.getActionType(),
										// Constants.CORE_SERVICES_INTERNATIONAL_ACCESS,
										// null, cclient.getmsisdn());
										// }
									}

								}

								else if (cclient.getOfferingId()
										.equalsIgnoreCase(Constants.CORE_SERVICES_INTERNATIONAL_ACCESS)) {

									if (cclient.getActionType().equalsIgnoreCase("1")) {
										response = callService.callForwardServiceTOIVR(cclient.getActionType(),
												Constants.CORE_SERVICES_INTERNATIONAL_ACCESS, null, null,
												cclient.getmsisdn());
										// if (response != null &&
										// response.getResponseHeader().getRetCode().equals("0"))
										// {
										// logger.info("Response From NGBSSS For
										// CORE_SERVICES_INTERNATIONAL_NATIONAL
										// & CORE_SERVICES_EXTENDED_ROAMING : "
										// + Helper.ObjectToJson(response));
										// response =
										// callService.callForwardServiceTOIVR(cclient.getActionType(),
										// Constants.CORE_SERVICES_INTERNATIONAL_ACCESS,
										// null, cclient.getmsisdn());
										// }

									} else if (cclient.getActionType().equalsIgnoreCase("3")) {

										// CoreServicesCategoryItem item =
										// coreServicesResponseList.getData()
										// .getCoreServices().stream()
										// .flatMap(a ->
										// a.getCoreServicesList().stream())
										// .filter(b ->
										// b.getOfferingId().equalsIgnoreCase(
										// Constants.CORE_SERVICES_ROAMING_INTERNATIONAL_NATIONAL)
										// &&
										// b.getStatus().equalsIgnoreCase("active"))
										// .findFirst().orElse(null);

										String currentState=null;

										for (int i = 0; i < coreServicesResponseList.getData().getCoreServices()
												.size(); i++) {
											for (int j = 0; j < coreServicesResponseList.getData().getCoreServices()
													.get(i).getCoreServicesList().size(); j++) {
												if (coreServicesResponseList.getData().getCoreServices().get(i)
														.getCoreServicesList().get(j).getOfferingId().equalsIgnoreCase(
																Constants.CORE_SERVICES_ROAMING_INTERNATIONAL_NATIONAL)
														&& coreServicesResponseList.getData().getCoreServices().get(i)
																.getCoreServicesList().get(j).getStatus()
																.equalsIgnoreCase("active")) {
													currentState = "active";
													break;
												}
											}
											
											if(currentState!=null && currentState.equalsIgnoreCase("active"))
												break;
										}
										
										
										logger.info("The Current state of CORE_SERVICES_ROAMING_INTERNATIONAL_NATIONAL before deactive is " +currentState);

										if (currentState!=null && currentState.equalsIgnoreCase("active")) {
											response = callService.callForwardServiceTOIVR(cclient.getActionType(),
													Constants.CORE_SERVICES_ROAMING_INTERNATIONAL_NATIONAL,
													Constants.CORE_SERVICES_EXTENDED_ROAMING,
													Constants.CORE_SERVICES_INTERNATIONAL_ACCESS, cclient.getmsisdn());

										}

										else {
											response = callService.callForwardServiceTOIVR(cclient.getActionType(),
													Constants.CORE_SERVICES_INTERNATIONAL_ACCESS, null, null,
													cclient.getmsisdn());
										}

										// if (response != null &&
										// response.getResponseHeader().getRetCode().equals("0"))
										// {
										// logger.info("Response From NGBSSS For
										// CORE_SERVICES_INTERNATIONAL_NATIONAL
										// & CORE_SERVICES_EXTENDED_ROAMING : "
										// + Helper.ObjectToJson(response));
										// response =
										// callService.callForwardServiceTOIVR(cclient.getActionType(),
										// Constants.CORE_SERVICES_INTERNATIONAL_ACCESS,
										// null, cclient.getmsisdn());
										// }
									}
								}

								else {
									response = callService.callForwardServiceTOIVR(cclient.getActionType(),
											cclient.getOfferingId(), null, null, cclient.getmsisdn());
								}

								if (response != null && response.getResponseHeader().getRetCode().equals("0")) {
									logger.info("Response From NGBSSS: " + Helper.ObjectToJson(response));
									resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
									resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
									logs.setResponseCode(resp.getReturnCode());
									logs.setResponseDescription(resp.getReturnMsg());
									logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
									logs.updateLog(logs);
									return resp;
								} else if (null != response) {
									resp.setReturnMsg(response.getResponseHeader().getRetMsg());
									resp.setReturnCode(response.getResponseHeader().getRetCode());
								} else {
									logger.info("<<<<<<< RESP >>>>>>>" + response.getResponseHeader().getRetMsg());
									resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
									resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
								}
							} else {
								// send to number
								response = null;
								if (!cclient.getmsisdn().equalsIgnoreCase(cclient.getNumber())) {
									response = callService.callForwardServiceTOMSISDN(cclient.getActionType(),
											cclient.getOfferingId(), cclient.getmsisdn(), cclient.getNumber());
									if (response.getResponseHeader().getRetCode().equals("0")) {
										resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
										resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
										logs.setResponseCode(resp.getReturnCode());
										logs.setResponseDescription(resp.getReturnMsg());
										logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
										logs.updateLog(logs);
										return resp;
									} else if (null != response) {
										resp.setReturnMsg(response.getResponseHeader().getRetMsg());
										resp.setReturnCode(response.getResponseHeader().getRetCode());
										logs.setResponseCode(resp.getReturnCode());
										logs.setResponseDescription(resp.getReturnMsg());
										logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
										logs.updateLog(logs);
										return resp;
									} else {
										logger.info("<<<<<<< RESP >>>>>>>" + response.getResponseHeader().getRetMsg());
										resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
										resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
										logs.setResponseCode(resp.getReturnCode());
										logs.setResponseDescription(resp.getReturnMsg());
										logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
										logs.updateLog(logs);
										return resp;
									}
								} else {
									resp.setReturnMsg(ResponseCodes.CALL_FORWARD_TO_SAME_NUMBER_DES);
									resp.setReturnCode(ResponseCodes.CALL_FORWARD_TO_SAME_NUMBER_CODE);
									logs.setResponseCode(resp.getReturnCode());
									logs.setResponseDescription(resp.getReturnMsg());
									logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
									logs.updateLog(logs);
									return resp;
								}
							}
						}
					}
				} else {
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}

			} else {
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		} catch (

		Exception ex) {
			logger.error(Helper.GetException(ex));
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_400);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		return resp;
	}

	private String checkIfIamBackActivated(CallForwardRequest cclient) {
		String flag = "Inactive";
		GetCoreServicesRequestLand getCoreServicesRequestLand = new GetCoreServicesRequestLand();
		GetCoreServicesRequest getCoreServicesRequest = new GetCoreServicesRequest();
		getCoreServicesRequest.setChannel(cclient.getChannel());
		getCoreServicesRequest.setiP(cclient.getiP());
		getCoreServicesRequest.setLang(cclient.getLang());
		getCoreServicesRequest.setMsisdn(cclient.getmsisdn());
		getCoreServicesRequest.setAccountType(cclient.getAccountType());
		getCoreServicesRequest.setUserType(cclient.getUserType());
		getCoreServicesRequest.setBrand(cclient.getBrand());
		getCoreServicesRequest.setGroupType(cclient.getGroupType());
		ObjectMapper mapper = new ObjectMapper();
		String jsonInString = "";
		try {
			jsonInString = mapper.writeValueAsString(getCoreServicesRequest);
			GetCoreServicesResponse getCoreServicesResponse = getCoreServicesRequestLand
					.Get(Constants.CREDENTIALSUNCODED, jsonInString);
			if (getCoreServicesResponse.getReturnCode().equalsIgnoreCase("200")) {
				for (int i = 0; i < getCoreServicesResponse.getData().getCoreServices().size(); i++) {
					for (int j = 0; j < getCoreServicesResponse.getData().getCoreServices().get(i).getCoreServicesList()
							.size(); j++)
						if (getCoreServicesResponse.getData().getCoreServices().get(i).getCoreServicesList().get(j)
								.getOfferingId().equalsIgnoreCase(Constants.I_AM_BACK_OFFERING_ID)) {
							logger.debug(
									"getCoreServicesResponse.getData().getCoreServices().get(i).getCoreServicesList().get(j).getStatus()----  >>>> "
											+ getCoreServicesResponse.getData().getCoreServices().get(i)
													.getCoreServicesList().get(j).getStatus());
							return getCoreServicesResponse.getData().getCoreServices().get(i).getCoreServicesList()
									.get(j).getStatus();
						}
				}
			}
		} catch (JsonProcessingException e) {
			logger.error(Helper.GetException(e));
		}
		logger.debug("I have flag value----->>>> " + flag);
		return flag;
	}

	private ArrayList<String> checkIfIamBackActivatedV2(ProcessCoreServicesRequestV2 cclient, Logs logs, String token) {

		ArrayList<String> offerigIDs = new ArrayList<String>();

		GetCustomerRequestResponse resp = new GetCustomerRequestResponse();
		GetCustomerDataLand getcustomer = new GetCustomerDataLand();

		GetCustomerRequestClient cclientcustomer = new GetCustomerRequestClient();
		cclientcustomer.setMsisdn(cclient.getmsisdn());
		resp = getcustomer.RequestSoap(cclientcustomer, logs, token);

		/*
		 * com.huawei.crm.query.GetSubscriberResponse respns = new
		 * com.evampsaanga.azerfon.getsubscriber.CRMSubscriberService()
		 * .GetSubscriberRequest(cclient.getmsisdn()); List<GetSubOfferingInfo>
		 * getSubOfferingInfo =
		 * respns.getGetSubscriberBody().getSupplementaryOfferingList()
		 * .getGetSubOfferingInfo();
		 * 
		 * for (int i = 0; i < getSubOfferingInfo.size(); i++) {
		 * if(getSubOfferingInfo.get(i).getOfferingId().equals(Constants.
		 * I_AM_BUSY)); { logger.info("------NEW BUSY_OFFERING_ID----- "+
		 * getSubOfferingInfo.get(i).getOfferingId()); }
		 * if(getSubOfferingInfo.get(i).getOfferingId().equals(Constants.
		 * I_AM_BACK_OFFERING_ID)) {
		 * logger.info("------NEW I AM BACK OFFERING_ID ----- "
		 * +getSubOfferingInfo.get(i).getOfferingId()); } }
		 */

		List<OfferingInfo> supplementrylist = resp.getCustomerData().getSupplementaryOfferingList();
		for (int i = 0; i < supplementrylist.size(); i++) {
			logger.debug("Offring ID >>>> " + supplementrylist.get(i).getOfferingId() + " : "
					+ supplementrylist.get(i).getStatus());
			if (supplementrylist.get(i).getOfferingId().equals(Constants.I_AM_BACK_OFFERING_ID)) {
				if (supplementrylist.get(i).getStatus().equalsIgnoreCase("Active")) {

					offerigIDs.add(Constants.I_AM_BACK_OFFERING_ID);
					// return flag;
				}

			}
			if (supplementrylist.get(i).getOfferingId().equals(Constants.I_AM_BUSY)) {
				if (supplementrylist.get(i).getStatus().equalsIgnoreCase("Active")) {

					offerigIDs.add(Constants.I_AM_BUSY);

				}
			}
		}
		logger.debug("I have Size List Offering IDs----->>>> " + offerigIDs.size() + " " + offerigIDs);
		return offerigIDs;
	}

	static boolean flagTocheckNetworkSettingsOfferingIds(String offeringID) {

		String offeringIDs = ConfigurationManager.getConfigurationFromCache("network.settings.offeringIds");
		logger.info(" nextWorkOfferingIds FROM DB " + offeringIDs);
		List<String> items = Arrays.asList(offeringIDs.split(","));
		if (items.contains(offeringID)) {
			logger.info("TRUE nextWorkOfferingId found");
			return true;
		} else {
			logger.info("FALSE nextWorkOfferingId NOT found");
			return false;
		}

	}

}