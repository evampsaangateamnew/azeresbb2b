package com.evampsaanga.b2b.azerfon.callforwarding;

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;
/**
 * Request Containter For Call Forward Phase 2
 * @author Aqeel Abbas
 *
 */
public class CallForwardRequestV2 extends BaseRequest {
	private String offeringId;
	private String actionType;
	private String type;
	private String users;
	private String actPrice;
	private String number;
	private String orderKey; 
	
	public String getOrderKey() {
		return orderKey;
	}
	public void setOrderKey(String orderKey) {
		this.orderKey = orderKey;
	}
	public String getActPrice() {
		return actPrice;
	}

	public void setActPrice(String actPrice) {
		this.actPrice = actPrice;
	}

	public String getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(String offeringId) {
		this.offeringId = offeringId;
	}

	public String getUsers() {
		return users;
	}
	public void setUsers(String users) {
		this.users = users;
	}
	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

}
