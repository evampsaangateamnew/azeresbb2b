package com.evampsaanga.b2b.azerfon.status.freesms;

import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;

public class GetSMSResponse extends BaseResponse {
	private SMSstatus status;

	/**
	 * @return the status
	 */
	public SMSstatus getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(SMSstatus status) {
		this.status = status;
	}
}
