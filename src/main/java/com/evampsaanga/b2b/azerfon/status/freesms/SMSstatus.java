package com.evampsaanga.b2b.azerfon.status.freesms;

public class SMSstatus {
	private int onNetSMS = 0;
	private int offNetSMS = 0;

	/**
	 * @return the smsSent
	 */
	public SMSstatus(int onNetSMS, int offNetSMS) {
		super();
		this.onNetSMS = onNetSMS;
		this.offNetSMS = offNetSMS;
	}

	/**
	 * @return the onNetSMS
	 */
	public int getOnNetSMS() {
		return onNetSMS;
	}

	/**
	 * @param onNetSMS
	 *            the onNetSMS to set
	 */
	public void setOnNetSMS(int onNetSMS) {
		this.onNetSMS = onNetSMS;
	}

	/**
	 * @return the offNetSMS
	 */
	public int getOffNetSMS() {
		return offNetSMS;
	}

	/**
	 * @param offNetSMS
	 *            the offNetSMS to set
	 */
	public void setOffNetSMS(int offNetSMS) {
		this.offNetSMS = offNetSMS;
	}
}
