package com.evampsaanga.b2b.azerfon.status.freesms;



import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.amqimplementationsesb.LogsType;
import com.evampsaanga.b2b.azerfon.db.DBFactory;
import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.ThirdPartyNames;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Decrypter;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.evampsaanga.b2b.developer.utils.SOAPLoggingHandler;

@Path("/azerfon")
public class GetSMSRequestLand {
	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetSMSResponse Get(@Header("credentials") String credential, @Body() String requestBody) {
		GetSMSRequest cclient = null;Logs logs = new Logs();
		logs.setTransactionName(Transactions.SEND_SMS_STATUS);
		logs.setThirdPartyName(ThirdPartyNames.FREE_SMS_STATUS);
		logs.setTableType(LogsType.AppFaq);
		GetSMSResponse resp = new GetSMSResponse();
		try {
			cclient = Helper.JsonToObject(requestBody, GetSMSRequest.class);
			if (cclient != null) {
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());logs.setLang(cclient.getLang());
			}	} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_400);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);	return resp;
		}
		try {
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);	return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						resp.setReturnCode(ResponseCodes.ERROR_400);
						resp.setReturnMsg(verification);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);	return resp;
					}
				} else {
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);return resp;
				}
				if (credentials.equals(Constants.CREDENTIALS)) {
					try (PreparedStatement stmt = DBFactory.getDbConnection()
							.prepareStatement("select onnet,offnet from "
									+ ConfigurationManager.getDBProperties("db.table.freesms") + " where msisdn="
									+ "'" + cclient.getmsisdn() + "'");
							ResultSet rs = stmt.executeQuery();) {
						int onnet = -1;
						int offnet = -1;
						try {
							if (rs.next()) {
								offnet = Integer.parseInt(rs.getString("offnet"));
								onnet = Integer.parseInt(rs.getString("onnet"));
								resp.setStatus(new com.evampsaanga.b2b.azerfon.status.freesms.SMSstatus(onnet, offnet));
								resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
								resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
							} else {
								resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
								resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
								int freesmsconfigureOnnet = Integer
										.parseInt(ConfigurationManager.getDBProperties("onnet.freesms.configure"));
								int freesmsconfigureOffnet = Integer.parseInt(
										ConfigurationManager.getDBProperties("offnet.freesms.configure"));
								resp.setStatus(new com.evampsaanga.b2b.azerfon.status.freesms.SMSstatus(
										freesmsconfigureOnnet, freesmsconfigureOffnet));
							}
							
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						} catch (Exception ex) {
							logger.error(Helper.GetException(ex));
						}
					} catch (Exception ex) {
						logger.error(Helper.GetException(ex));
					}
				} else {
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);	return resp;
				}
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
			resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
			resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
		}
		resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
		resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
		logs.setResponseCode(resp.getReturnCode());
		logs.setResponseDescription(resp.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);	return resp;
	}

	public static final Logger logger = Logger.getLogger("azerfon-esb");
}
