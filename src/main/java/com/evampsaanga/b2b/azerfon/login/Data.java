package com.evampsaanga.b2b.azerfon.login;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "entity_id", "website_id", "email", "group_id", "increment_id", "store_id", "created_at",
		"updated_at", "is_active", "disable_auto_group_change", "created_in", "prefix", "firstname", "middlename",
		"lastname", "suffix", "dob", "password_hash", "rp_token", "rp_token_created_at", "default_billing",
		"default_shipping", "taxvat", "confirmation", "gender", "failures_num", "first_failure", "lock_expires",
		"msisdn", "account_id" })
public class Data {
	@JsonProperty("entity_id")
	private String entityId;
	@JsonProperty("website_id")
	private String websiteId;
	@JsonProperty("email")
	private String email;
	@JsonProperty("group_id")
	private String groupId;
	@JsonProperty("increment_id")
	private Object incrementId;
	@JsonProperty("store_id")
	private String storeId;
	@JsonProperty("created_at")
	private String createdAt;
	@JsonProperty("updated_at")
	private String updatedAt;
	@JsonProperty("is_active")
	private String isActive;
	@JsonProperty("disable_auto_group_change")
	private String disableAutoGroupChange;
	@JsonProperty("created_in")
	private String createdIn;
	@JsonProperty("prefix")
	private Object prefix;
	@JsonProperty("firstname")
	private String firstname;
	@JsonProperty("middlename")
	private String middlename;
	@JsonProperty("lastname")
	private String lastname;
	@JsonProperty("suffix")
	private Object suffix;
	@JsonProperty("dob")
	private String dob;
	@JsonProperty("password_hash")
	private String passwordHash;
	@JsonProperty("rp_token")
	private Object rpToken;
	@JsonProperty("rp_token_created_at")
	private Object rpTokenCreatedAt;
	@JsonProperty("default_billing")
	private Object defaultBilling;
	@JsonProperty("default_shipping")
	private Object defaultShipping;
	@JsonProperty("taxvat")
	private Object taxvat;
	@JsonProperty("confirmation")
	private Object confirmation;
	@JsonProperty("gender")
	private Object gender;
	@JsonProperty("failures_num")
	private String failuresNum;
	@JsonProperty("first_failure")
	private Object firstFailure;
	@JsonProperty("lock_expires")
	private Object lockExpires;
	@JsonProperty("msisdn")
	private String msisdn;
	@JsonProperty("account_id")
	private String accountId;
	@JsonProperty("rateus_android")
	private String rateus_android;
	@JsonProperty("rateus_ios")
	private String rateus_ios;

	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("entity_id")
	public String getEntityId() {
		return entityId;
	}

	@JsonProperty("entity_id")
	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	@JsonProperty("website_id")
	public String getWebsiteId() {
		return websiteId;
	}

	@JsonProperty("website_id")
	public void setWebsiteId(String websiteId) {
		this.websiteId = websiteId;
	}

	@JsonProperty("email")
	public String getEmail() {
		return email;
	}

	@JsonProperty("email")
	public void setEmail(String email) {
		this.email = email;
	}

	@JsonProperty("group_id")
	public String getGroupId() {
		return groupId;
	}

	@JsonProperty("group_id")
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	@JsonProperty("increment_id")
	public Object getIncrementId() {
		return incrementId;
	}

	@JsonProperty("increment_id")
	public void setIncrementId(Object incrementId) {
		this.incrementId = incrementId;
	}

	@JsonProperty("store_id")
	public String getStoreId() {
		return storeId;
	}

	@JsonProperty("store_id")
	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}

	@JsonProperty("created_at")
	public String getCreatedAt() {
		return createdAt;
	}

	@JsonProperty("created_at")
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	@JsonProperty("updated_at")
	public String getUpdatedAt() {
		return updatedAt;
	}

	@JsonProperty("updated_at")
	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	@JsonProperty("is_active")
	public String getIsActive() {
		return isActive;
	}

	@JsonProperty("is_active")
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	@JsonProperty("disable_auto_group_change")
	public String getDisableAutoGroupChange() {
		return disableAutoGroupChange;
	}

	@JsonProperty("disable_auto_group_change")
	public void setDisableAutoGroupChange(String disableAutoGroupChange) {
		this.disableAutoGroupChange = disableAutoGroupChange;
	}

	@JsonProperty("created_in")
	public String getCreatedIn() {
		return createdIn;
	}

	@JsonProperty("created_in")
	public void setCreatedIn(String createdIn) {
		this.createdIn = createdIn;
	}

	@JsonProperty("prefix")
	public Object getPrefix() {
		return prefix;
	}

	@JsonProperty("prefix")
	public void setPrefix(Object prefix) {
		this.prefix = prefix;
	}

	@JsonProperty("firstname")
	public String getFirstname() {
		return firstname;
	}

	@JsonProperty("firstname")
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	@JsonProperty("middlename")
	public String getMiddlename() {
		return middlename;
	}

	@JsonProperty("middlename")
	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}

	@JsonProperty("lastname")
	public String getLastname() {
		return lastname;
	}

	@JsonProperty("lastname")
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	@JsonProperty("suffix")
	public Object getSuffix() {
		return suffix;
	}

	@JsonProperty("suffix")
	public void setSuffix(Object suffix) {
		this.suffix = suffix;
	}

	@JsonProperty("dob")
	public String getDob() {
		return dob;
	}

	@JsonProperty("dob")
	public void setDob(String dob) {
		this.dob = dob;
	}

	@JsonProperty("password_hash")
	public String getPasswordHash() {
		return passwordHash;
	}

	@JsonProperty("password_hash")
	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}

	@JsonProperty("rp_token")
	public Object getRpToken() {
		return rpToken;
	}

	@JsonProperty("rp_token")
	public void setRpToken(Object rpToken) {
		this.rpToken = rpToken;
	}

	@JsonProperty("rp_token_created_at")
	public Object getRpTokenCreatedAt() {
		return rpTokenCreatedAt;
	}

	@JsonProperty("rp_token_created_at")
	public void setRpTokenCreatedAt(Object rpTokenCreatedAt) {
		this.rpTokenCreatedAt = rpTokenCreatedAt;
	}

	@JsonProperty("default_billing")
	public Object getDefaultBilling() {
		return defaultBilling;
	}

	@JsonProperty("default_billing")
	public void setDefaultBilling(Object defaultBilling) {
		this.defaultBilling = defaultBilling;
	}

	@JsonProperty("default_shipping")
	public Object getDefaultShipping() {
		return defaultShipping;
	}

	@JsonProperty("default_shipping")
	public void setDefaultShipping(Object defaultShipping) {
		this.defaultShipping = defaultShipping;
	}

	@JsonProperty("taxvat")
	public Object getTaxvat() {
		return taxvat;
	}

	@JsonProperty("taxvat")
	public void setTaxvat(Object taxvat) {
		this.taxvat = taxvat;
	}

	@JsonProperty("confirmation")
	public Object getConfirmation() {
		return confirmation;
	}

	@JsonProperty("confirmation")
	public void setConfirmation(Object confirmation) {
		this.confirmation = confirmation;
	}

	@JsonProperty("gender")
	public Object getGender() {
		return gender;
	}

	@JsonProperty("gender")
	public void setGender(Object gender) {
		this.gender = gender;
	}

	@JsonProperty("failures_num")
	public String getFailuresNum() {
		return failuresNum;
	}

	@JsonProperty("failures_num")
	public void setFailuresNum(String failuresNum) {
		this.failuresNum = failuresNum;
	}

	@JsonProperty("first_failure")
	public Object getFirstFailure() {
		return firstFailure;
	}

	@JsonProperty("first_failure")
	public void setFirstFailure(Object firstFailure) {
		this.firstFailure = firstFailure;
	}

	@JsonProperty("lock_expires")
	public Object getLockExpires() {
		return lockExpires;
	}

	@JsonProperty("lock_expires")
	public void setLockExpires(Object lockExpires) {
		this.lockExpires = lockExpires;
	}

	@JsonProperty("msisdn")
	public String getMsisdn() {
		return msisdn;
	}

	@JsonProperty("msisdn")
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	@JsonProperty("account_id")
	public String getAccountId() {
		return accountId;
	}

	@JsonProperty("account_id")
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	public String getRateus_android() {
		return rateus_android;
	}

	public void setRateus_android(String rateus_android) {
		this.rateus_android = rateus_android;
	}

	public String getRateus_ios() {
		return rateus_ios;
	}

	public void setRateus_ios(String rateus_ios) {
		this.rateus_ios = rateus_ios;
	}
}
