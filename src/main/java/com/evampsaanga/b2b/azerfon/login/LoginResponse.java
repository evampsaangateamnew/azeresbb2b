package com.evampsaanga.b2b.azerfon.login;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.evampsaanga.b2b.azerfon.getcustomerrequest.CustomerData;
import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "SignUpRequest")
public class LoginResponse extends BaseResponse {
	private com.evampsaanga.b2b.azerfon.getcustomerrequest.CustomerData customerData = new CustomerData();

	/**
	 * @return the customerData
	 */
	public com.evampsaanga.b2b.azerfon.getcustomerrequest.CustomerData getCustomerData() {
		return customerData;
	}

	/**
	 * @param customerData
	 *            the customerData to set
	 */
	public void setCustomerData(com.evampsaanga.b2b.azerfon.getcustomerrequest.CustomerData customerData) {
		this.customerData = customerData;
	}
}
