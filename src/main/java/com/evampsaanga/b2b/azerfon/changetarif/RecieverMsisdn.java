
package com.evampsaanga.b2b.azerfon.changetarif;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "tariff ids",
    "msisdn",
    "grouptype"
})
public class RecieverMsisdn {

    @JsonProperty("tariff ids")
    private String tariffIds;
    @JsonProperty("msisdn")
    private String msisdn;
    @JsonProperty("grouptype")
    private String grouptype;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("tariff ids")
    public String getTariffIds() {
        return tariffIds;
    }

    @JsonProperty("tariff ids")
    public void setTariffIds(String tariffIds) {
        this.tariffIds = tariffIds;
    }

    @JsonProperty("msisdn")
    public String getMsisdn() {
        return msisdn;
    }

    @JsonProperty("msisdn")
    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    @JsonProperty("grouptype")
    public String getGrouptype() {
        return grouptype;
    }

    @JsonProperty("grouptype")
    public void setGrouptype(String grouptype) {
        this.grouptype = grouptype;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
