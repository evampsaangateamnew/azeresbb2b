package com.evampsaanga.b2b.azerfon.changetarif;

import java.io.IOException;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.amqimplementationsesb.LogsType;
import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.ThirdPartyNames;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Decrypter;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.evampsaanga.b2b.developer.utils.SOAPLoggingHandler;
import com.huawei.crm.basetype.ens.OfferingInfo;
import com.huawei.crm.basetype.ens.OfferingKey;
import com.huawei.crm.basetype.ens.OrderInfo;
import com.huawei.crm.basetype.ens.OrderItemInfo;
import com.huawei.crm.basetype.ens.OrderItemValue;
import com.huawei.crm.basetype.ens.OrderItems;
import com.huawei.crm.basetype.ens.SubscriberInfo;
import com.huawei.crm.service.ens.SubmitOrderRequest;
import com.huawei.crm.service.ens.SubmitOrderResponse;
import com.huawei.crm.service.ens.SubmitRequestBody;
import com.ngbss.evampsaanga.services.ThirdPartyCall;
import com.ngbss.evampsaanga.services.ThirdPartyRequestHeader;


@Path("/azerfon/")
public class ChangeTarrifRequestLand {
	public static final Logger logger = Logger.getLogger("azerfon-esb");

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ChangeTarrifResponseClient Get(@Header("credentials") String credential,
			@Header("Content-Type") String contentType, @Body() String requestBody) {
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.CHANGE_TARRIF_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.CHANGE_TARRIF);
		logs.setTableType(LogsType.ChangeTarrif);

		String token = "";
		String TrnsactionName = Transactions.CHANGE_TARRIF_TRANSACTION_NAME;

		try {

			token = Helper.retrieveToken(TrnsactionName, Helper.getValueFromJSON(requestBody, "msisdn"));

			logger.info(token+"Request Landed on Change Tarrif:" + requestBody);
			ChangeTarrifRequestClient cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, ChangeTarrifRequestClient.class);
				if (cclient != null) {
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
					logs.setLang(cclient.getLang());
					logs.setTariffId(cclient.getOfferingId());

				}
			} catch (Exception ex) {
				logger.info(token+Helper.GetException(ex));
				ChangeTarrifResponseClient resp = new ChangeTarrifResponseClient();
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.info(token+Helper.GetException(ex));
				}
				if (credentials == null) {
					ChangeTarrifResponseClient resp = new ChangeTarrifResponseClient();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					ChangeTarrifResponseClient resp = new ChangeTarrifResponseClient();
					/**
					 * HOT Fix for some tariffs which have different offering id for subscription
					 * and renewal.
					 */
					if (cclient.getActionType() != null
							&& cclient.getActionType().equalsIgnoreCase(Constants.TARIFF_CHANGE_ACTION_RENEWAL)) {
						cclient.setOfferingId(ConfigurationManager
								.getConfigurationFromCache("tariff.renewal.offering.id." + cclient.getOfferingId()));
					}
					/**
					 * Hot Fix closed.
					 */

					SubmitOrderResponse response = changetarrifresponse(cclient.getmsisdn(), cclient.getOfferingId(),token);
					if (response.getResponseHeader().getRetCode().equals("0")) {
						resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
						resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					} else {
						resp.setReturnCode(response.getResponseHeader().getRetCode());
						resp.setReturnMsg(response.getResponseHeader().getRetMsg());
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					}
					logs.updateLog(logs);
					return resp;
				} else {
					ChangeTarrifResponseClient resp = new ChangeTarrifResponseClient();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.info(token+Helper.GetException(ex));
		}
		ChangeTarrifResponseClient resp = new ChangeTarrifResponseClient();
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(resp.getReturnCode());
		logs.setResponseDescription(resp.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	public SubmitOrderResponse changetarrifresponse(String msisdn, String offeringid,String token) {
		logger.info(token + "-Request Received In changetarrifresponse Method Azerfon Call-" + "Data-"
				+ msisdn +": "+offeringid );
		SubmitOrderRequest submitOrderRequestMsgReq = new SubmitOrderRequest();
//		submitOrderRequestMsgReq.setRequestHeader(OrderHandleService.getReqHeaderForChangeTariff());
		submitOrderRequestMsgReq.setRequestHeader(ThirdPartyRequestHeader.getReqHeaderForChangeTariff());
		SubmitRequestBody submitRequestBody = new SubmitRequestBody();
		OrderInfo oInfo = new OrderInfo();
		oInfo.setOrderType(Constants.CHANGE_TARIFF_ORDER_TYPE);
		OrderItemInfo oitemInfo = new OrderItemInfo();
		oitemInfo.setOrderItemType(Constants.CHANGE_TARIFF_ORDER_ITEM_TYPE);
		oitemInfo.setIsCustomerNotification(Constants.CHANGE_TARIFF_CUSTOMER_NOTIFICATION);
		oitemInfo.setIsPartnerNotification(Constants.CHANGE_TARIFF_PARAMETER_NOTIFICATION);
		submitRequestBody.setOrder(oInfo);
		OrderItems OrderItems = new OrderItems();
		OrderItemValue OrderItemValue = new OrderItemValue();
		OrderItemValue.setOrderItemInfo(oitemInfo);
		SubscriberInfo subscriber = new SubscriberInfo();
		subscriber.setServiceNumber(msisdn);
		subscriber.setPrimaryOfferingInfo(new OfferingInfo());
		subscriber.getPrimaryOfferingInfo().setActionType(Constants.CHANGE_TARIFF_ACTION_TYPE);
		OfferingKey offeringId = new OfferingKey();
		offeringId.setOfferingId(offeringid);
		subscriber.getPrimaryOfferingInfo().setActiveMode(Constants.CHANGE_TARIFF_ACTION_MODE);
		subscriber.getPrimaryOfferingInfo().setEffectMode(Constants.CHANGE_TARIFF_EFFECTIVE_MODE);
		subscriber.getPrimaryOfferingInfo().setOfferingId(offeringId);
		OrderItemValue.setSubscriber(subscriber);
		OrderItems.getOrderItem().add(OrderItemValue);
		submitRequestBody.setOrderItems(OrderItems);
		submitOrderRequestMsgReq.setSubmitRequestBody(submitRequestBody);
//		SubmitOrderResponse response = OrderHandleService.getInstance().submitOrder(submitOrderRequestMsgReq);
		SubmitOrderResponse response = ThirdPartyCall.submitOrder(submitOrderRequestMsgReq);
		try {
			logger.info(token + "-Response from changetarrifresponse Azerfon Call-" + Helper.ObjectToJson(response));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.info(token+Helper.GetException(e));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.info(token+Helper.GetException(e));
		}
	return response;
	}
}
