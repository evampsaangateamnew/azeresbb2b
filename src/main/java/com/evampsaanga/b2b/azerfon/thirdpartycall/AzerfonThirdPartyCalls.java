package com.evampsaanga.b2b.azerfon.thirdpartycall;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.evampsaanga.b2b.azerfon.db.DBFactory;
import com.evampsaanga.b2b.azerfon.getcustomerrequest.GetCustomerDataLand;
import com.evampsaanga.b2b.azerfon.loan.getcollections.Collection;
import com.evampsaanga.b2b.azerfon.loan.getcollections.GetCollectionsRequest;
import com.evampsaanga.b2b.azerfon.loan.getcollections.GetCollectionsResponse;
import com.evampsaanga.b2b.azerfon.loan.getdebtinfo.GetDebtInfoRequest;
import com.evampsaanga.b2b.azerfon.loan.getdebtinfo.GetDebtInfoResponse;
import com.evampsaanga.b2b.azerfon.loan.getloan.GetLoanRequest;
import com.evampsaanga.b2b.azerfon.loan.getloan.GetLoanResponse;
import com.evampsaanga.b2b.azerfon.loan.getloanpaymenthistory.GetLoanPaymentHistoryRequest;
import com.evampsaanga.b2b.azerfon.loan.getloanpaymenthistory.GetLoanPaymentHistoryResponse;
import com.evampsaanga.b2b.azerfon.loan.getloanrequesthistory.GetLoanRequestHistoryRequest;
import com.evampsaanga.b2b.azerfon.loan.getloanrequesthistory.GetLoanRequestHistoryResponse;
import com.evampsaanga.b2b.azerfon.loan.getprovisions.GetProvisionsRequest;
import com.evampsaanga.b2b.azerfon.loan.getprovisions.GetProvisionsResponse;
import com.evampsaanga.b2b.azerfon.loan.getprovisions.Provisions;
import com.evampsaanga.b2b.azerfon.loan.getrequestfeechargings.GetRequestFeeChargingsRequest;
import com.evampsaanga.b2b.azerfon.loan.getrequestfeechargings.GetRequestFeeChargingsResponse;
import com.evampsaanga.b2b.azerfon.loan.getsubscriberhistory.GetSubscriberHistoryRequest;
import com.evampsaanga.b2b.azerfon.loan.getsubscriberhistory.GetSubscriberHistoryResponse;
import com.evampsaanga.b2b.azerfon.loan.getsubscriberhistory.LoanPayment;
import com.evampsaanga.b2b.azerfon.loan.getsubscriberhistory.LoanRequest;
import com.evampsaanga.b2b.azerfon.loan.providenewcredit.ProvideNewCreditRequest;
import com.evampsaanga.b2b.azerfon.loan.providenewcredit.ProvideNewCreditResponse;
import com.evampsaanga.b2b.azerfon.nartv.getsubscriptions.AppCache;
import com.evampsaanga.b2b.azerfon.nartv.getsubscriptions.GetPlans;
import com.evampsaanga.b2b.azerfon.nartv.getsubscriptions.GetSubscriptionsRequest;
import com.evampsaanga.b2b.azerfon.nartv.getsubscriptions.GetSubscriptionsResponse;
import com.evampsaanga.b2b.azerfon.nartv.getsubscriptions.GetSubscriptionsThirdPartyRequest;
import com.evampsaanga.b2b.azerfon.nartv.getsubscriptions.Subscriptions;
import com.evampsaanga.b2b.azerfon.nartv.subscribe.SubscribeRequest;
import com.evampsaanga.b2b.azerfon.nartv.subscribe.SubscribeResponse;
import com.evampsaanga.b2b.azerfon.nartv.subscribe.SubscribeThirdPartyRequest;
import com.evampsaanga.b2b.azerfon.requestmoney.RequestMoneyRequest;
import com.evampsaanga.b2b.azerfon.requestmoney.RequestMoneyResponse;
import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.evampsaanga.b2b.magento.tariffdetailsv2.TariffDetailsMagentoResponse;
import com.huawei.bme.cbsinterface.arservices.AdjustmentRequest;
import com.huawei.bme.cbsinterface.arservices.AdjustmentRequest.AdjustmentObj;
import com.huawei.bme.cbsinterface.arservices.AdjustmentRequest.FreeUnitAdjustmentInfo;
import com.huawei.bme.cbsinterface.arservices.AdjustmentRequestMsg;
import com.huawei.bme.cbsinterface.arservices.AdjustmentResultMsg;
import com.huawei.bme.cbsinterface.arservices.PaymentRequest;
import com.huawei.bme.cbsinterface.arservices.PaymentRequest.PaymentInfo;
import com.huawei.bme.cbsinterface.arservices.PaymentRequest.PaymentObj;
import com.huawei.bme.cbsinterface.arservices.PaymentRequest.PaymentObj.AcctAccessCode;
import com.huawei.bme.cbsinterface.arservices.PaymentRequestMsg;
import com.huawei.bme.cbsinterface.arservices.PaymentResultMsg;
import com.huawei.bme.cbsinterface.arservices.QueryBalanceRequest;
import com.huawei.bme.cbsinterface.arservices.QueryBalanceRequestMsg;
import com.huawei.bme.cbsinterface.arservices.QueryBalanceResultMsg;
import com.huawei.bme.cbsinterface.arservices.RechargeRequest;
import com.huawei.bme.cbsinterface.arservices.RechargeRequest.RechargeInfo;
import com.huawei.bme.cbsinterface.arservices.RechargeRequest.RechargeInfo.CardPayment;
import com.huawei.bme.cbsinterface.arservices.RechargeRequest.RechargeObj;
import com.huawei.bme.cbsinterface.arservices.RechargeRequestMsg;
import com.huawei.bme.cbsinterface.arservices.RechargeResultMsg;
import com.huawei.bme.cbsinterface.bbcommon.SubAccessCode;
import com.huawei.bme.cbsinterface.bbcommon.SubGroupAccessCode;
import com.huawei.bme.cbsinterface.bbservices.QueryFreeUnitRequest;
import com.huawei.bme.cbsinterface.bbservices.QueryFreeUnitRequest.QueryObj;
import com.huawei.bme.cbsinterface.bbservices.QueryFreeUnitRequestMsg;
import com.huawei.bme.cbsinterface.bbservices.QueryFreeUnitResultMsg;
import com.huawei.bme.cbsinterface.bcservices.QuerySubLifeCycleRequest;
import com.huawei.bme.cbsinterface.bcservices.QuerySubLifeCycleRequestMsg;
import com.huawei.bme.cbsinterface.bcservices.QuerySubLifeCycleResultMsg;
import com.huawei.bme.cbsinterface.cbscommon.OperatorInfo;
import com.huawei.bme.cbsinterface.cbscommon.OwnershipInfo;
import com.huawei.bme.cbsinterface.cbscommon.SecurityInfo;
import com.huawei.bss.soaif._interface.subscriberservice.QueryHandsetInstallmentReqMsg;
import com.huawei.bss.soaif._interface.subscriberservice.QueryHandsetInstallmentRspMsg;
import com.huawei.bss.soaif.chargesubscriber._interface.common.ObjectAccessInfo;
import com.huawei.bss.soaif.chargesubscriber._interface.rechargeservice.ChargeForSubscriberReqMsg;
import com.huawei.bss.soaif.chargesubscriber._interface.rechargeservice.ChargeForSubscriberRspMsg;
import com.huawei.cbs.ar.wsservice.arcommon.BalanceAdjustment;
import com.huawei.cbs.ar.wsservice.arcommon.SimpleProperty;
import com.huawei.crm.azerfon.bss.query.GetSubscriberResponse;
import com.huawei.crm.basetype.ens.AccountEntity;
import com.huawei.crm.basetype.ens.ExtParameterInfo;
import com.huawei.crm.basetype.ens.ExtParameterList;
import com.huawei.crm.basetype.ens.OfferingInfo;
import com.huawei.crm.basetype.ens.OfferingKey;
import com.huawei.crm.basetype.ens.OrderInfo;
import com.huawei.crm.basetype.ens.OrderItemInfo;
import com.huawei.crm.basetype.ens.OrderItemValue;
import com.huawei.crm.basetype.ens.OrderItems;
import com.huawei.crm.basetype.ens.RequestHeader;
import com.huawei.crm.basetype.ens.SubscriberInfo;
import com.huawei.crm.query.GetCustomerIn;
import com.huawei.crm.query.GetCustomerRequest;
import com.huawei.crm.query.GetCustomerResponse;
import com.huawei.crm.query.GetGroupDataIn;
import com.huawei.crm.query.GetGroupRequest;
import com.huawei.crm.query.GetGroupResponse;
import com.huawei.crm.service.ens.SubmitOrderRequest;
import com.huawei.crm.service.ens.SubmitOrderResponse;
import com.huawei.crm.service.ens.SubmitRequestBody;
import com.ngbss.evampsaanga.services.BcService;
import com.ngbss.evampsaanga.services.ThirdPartyCall;
import com.ngbss.evampsaanga.services.ThirdPartyRequestHeader;
import com.saanga.magento.apiclient.RestClient;

public class AzerfonThirdPartyCalls {
	public static final Logger logger = Logger.getLogger("azerfon-esb");

	public static SubscribeResponse narTvSubscribe(String token, String transactionName,
			SubscribeRequest subscribeRequest, SubscribeResponse subscribeResponse) throws Exception {

		logger.info(token + "-Request Received In " + transactionName + " Third Party-" + "Data-"
				+ subscribeRequest.toString());

		SubscribeThirdPartyRequest subscribeThirdPartyRequest = new SubscribeThirdPartyRequest();

		subscribeThirdPartyRequest.setClient_id(subscribeRequest.getClientId());
		subscribeThirdPartyRequest.setGrant_type(subscribeRequest.getGrantType());
		subscribeThirdPartyRequest.setUsername(subscribeRequest.getUsername());
		subscribeThirdPartyRequest.setPassword(subscribeRequest.getPassword());

		String jsonRequest = subscribeThirdPartyRequest.toString();

		String url = "https://discovery-ams.sotalcloud.com/sdp/v2/authorize";

		logger.info(token + "-Request Call To Third Party");
		logger.info(token + "-Request Path-" + url);
		logger.info(token + "-Request Packet-" + jsonRequest);

		String response = RestClient.sendCallToAzerfonNarTvManagerToken(token, url, jsonRequest);

		// String response =
		// "{"data\":\"eyJhbGciOiJIUzI1NiIsIn55cCI6IkpXVCJ9.eyJpZCI6IlhXWWlQM2JoNURUbGVXTVNBS3Vkc2xYeWk4Q2N5eVpOIn0.iH7aJVLpl7kqudIAytI_CFzjQ-HZj5OAd0LkMIoeMns\"}";

		logger.info(token + "-Response Received From Third Party-" + response);

		if (response != null && !response.isEmpty()) {

			if (Helper.getValueFromJSON(response, "data") != null
					&& !Helper.getValueFromJSON(response, "data").isEmpty()) {

				String managerToken = Helper.getValueFromJSON(response, "data");

				url = "https://discovery-ams.sotalcloud.com/sdp/v2/billing-actions";

				String data = "{" + "\"type\": \"create-subscription\"," + "\"subscriber\": {" + "\"id\":"
						+ subscribeRequest.getSubscriberId() + "}," + "\"plan\": {" + "\"id\":"
						+ subscribeRequest.getPlanId() + "}," + "\"gateway\": {" + "\"id\": 18" + "}" + "}";

				logger.info(token + "-Request Call To Third Party");
				logger.info(token + "-Request Path-" + url);
				logger.info(token + "-Request Packet-" + data);

				// response = RestClient.sendCallToAzerfonNarTvSubscribe(token,
				// url, data,
				// managerToken);
				response = "{\"data\":{\"id\":4562,\"type\":\"create-subscription\",\"create_time\":\"2018-09-03T11:59:49Z\",\"update_time\":\"2018-09-03T11:59:53Z\",\"state\":\"success\",\"token\":\"FqA9BhbZsT54p9JpGMwwj8y5CVstw1wj\"}}";

				logger.info(token + "-Response Received From Third Party-" + response);

				if (response != null && !response.isEmpty()) {

					if (Helper.getValueFromJSON(response, "data") != null
							&& !Helper.getValueFromJSON(response, "data").isEmpty()) {

						String subscribeData = Helper.getValueFromJSON(response, "data");

						if (Helper.getValueFromJSON(subscribeData, "state")
								.equalsIgnoreCase(Constants.NAR_TV_SUBSCRIBE_STATE_SUCCESS)) {

							subscribeResponse.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							subscribeResponse.setReturnMsg(ResponseCodes.SUCESS_DES_200);

						} else {

							subscribeResponse
									.setReturnCode(ResponseCodes.NAR_TV_THIRD_PARTY_RESPONSE_STATE_NOT_SUCCESS_CODE);
							subscribeResponse
									.setReturnMsg(ResponseCodes.NAR_TV_THIRD_PARTY_RESPONSE_STATE_NOT_SUCCESS_DESC);

						}

					} else {

						subscribeResponse.setReturnCode(ResponseCodes.NARTV_THIRDPARY_RESPONSE_DATA_NULL_CODE);
						subscribeResponse.setReturnMsg(ResponseCodes.NARTV_THIRDPARY_RESPONSE_DATA_NULL_CODE);
					}

				} else {

					subscribeResponse.setReturnCode(ResponseCodes.NARTV_THIRDPARY_RESPONSE_NULL_CODE);
					subscribeResponse.setReturnMsg(ResponseCodes.NARTV_THIRDPARY_RESPONSE_NULL_DESC);
				}

			} else {

				subscribeResponse.setReturnCode(ResponseCodes.NARTV_THIRDPARY_RESPONSE_DATA_NULL_CODE);
				subscribeResponse.setReturnMsg(ResponseCodes.NARTV_THIRDPARY_RESPONSE_DATA_NULL_DESC);
			}

		} else {

			subscribeResponse.setReturnCode(ResponseCodes.NARTV_THIRDPARY_RESPONSE_NULL_CODE);
			subscribeResponse.setReturnMsg(ResponseCodes.NARTV_THIRDPARY_RESPONSE_NULL_DESC);
		}

		return subscribeResponse;
	}

	public static GetSubscriptionsResponse narTvSubscriptions(String token, String transactionName,
			GetSubscriptionsRequest getManagerTokenRequest, GetSubscriptionsResponse getManagerTokenResponse)
			throws Exception {

		logger.info(token + "-Request Received In " + transactionName + " Third Party-" + "Data-"
				+ getManagerTokenRequest.toString());

		GetSubscriptionsThirdPartyRequest getSubscriptionsThirdPartyRequest = new GetSubscriptionsThirdPartyRequest();

		getSubscriptionsThirdPartyRequest.setClient_id(getManagerTokenRequest.getClientId());
		getSubscriptionsThirdPartyRequest.setGrant_type(getManagerTokenRequest.getGrantType());
		getSubscriptionsThirdPartyRequest.setUsername(getManagerTokenRequest.getUsername());
		getSubscriptionsThirdPartyRequest.setPassword(getManagerTokenRequest.getPassword());

		String jsonRequest = getSubscriptionsThirdPartyRequest.toString();

		String url = "https://discovery-ams.sotalcloud.com/sdp/v2/authorize";

		logger.info(token + "-Request Call To Third Party");
		logger.info(token + "-Request Path-" + url);
		logger.info(token + "-Request Packet-" + jsonRequest);

		String response = RestClient.sendCallToAzerfonNarTvManagerToken(token, url, jsonRequest);

		// String response =
		// "{"data\":\"eyJhbGciOiJIUzI1NiIsIn55cCI6IkpXVCJ9.eyJpZCI6IlhXWWlQM2JoNURUbGVXTVNBS3Vkc2xYeWk4Q2N5eVpOIn0.iH7aJVLpl7kqudIAytI_CFzjQ-HZj5OAd0LkMIoeMns\"}";

		logger.info(token + "-Response Received From Third Party-" + response);

		if (response != null && !response.isEmpty()) {

			if (Helper.getValueFromJSON(response, "data") != null
					&& !Helper.getValueFromJSON(response, "data").isEmpty()) {

				String data = Helper.getValueFromJSON(response, "data");

				url = "https://discovery-ams.sotalcloud.com/sdp/v2/subscribers?ext_id[eq]="
						+ Constants.SUBSCRIBER_ID_PREFIX + getManagerTokenRequest.getmsisdn();

				logger.info(token + "-Request Call To Third Party");
				logger.info(token + "-Request Path-" + url);

				response = RestClient.sendCallToAzerfonNarTvSubscriptions(token, url, data);

				logger.info(token + "-Response Received From Third Party-" + response);

				if (response != null && !response.isEmpty()) {

					if (Helper.getValueFromJSON(response, "data") != null
							&& !Helper.getValueFromJSON(response, "data").isEmpty()) {

						JSONObject jsonObjectId = new JSONObject(response);
						JSONArray dataArrayId = jsonObjectId.getJSONArray("data");

						if (jsonObjectId.getJSONArray("data").length() > 0) {

							JSONObject dataObjectId = dataArrayId.getJSONObject(0);
							String id = dataObjectId.getString("id");

							logger.info(token + "-Subscriber Id-" + id);

							url = "https://discovery-ams.sotalcloud.com/sdp/v2/subscriptions?subscriber.id[eq]=" + id;

							logger.info(token + "-Request Call To Third Party");
							logger.info(token + "-Request Path-" + url);

							response = RestClient.sendCallToAzerfonNarTvSubscriptions(token, url, data);

							logger.info(token + "-Response Received From Third Party-" + response);

							if (response != null && !response.isEmpty()) {

								if (Helper.getValueFromJSON(response, "data") != null
										&& !Helper.getValueFromJSON(response, "data").isEmpty()) {

									JSONObject jsonObjectStatus = new JSONObject(response);

									JSONArray dataArrayStatus = jsonObjectStatus.getJSONArray("data");

									if (jsonObjectStatus.getJSONArray("data").length() > 0) {

										String planId = "";
										String state = "";
										String startDate = "";
										String endDate = "";

										for (int i = 0; i < jsonObjectStatus.getJSONArray("data").length(); i++) {

											JSONObject dataObjectStatus = dataArrayStatus.getJSONObject(i);

											if (dataObjectStatus.getString("state")
													.equalsIgnoreCase(Constants.NAR_TV_SUBSCRIBER_STATE)) {

												planId = dataObjectStatus.getString("id");
												state = dataObjectStatus.getString("state");
												startDate = dataObjectStatus.getString("period_start_time");
												endDate = dataObjectStatus.getString("period_end_time");

											}
										}

										logger.info(token + "-Plan Id-" + planId);
										logger.info(token + "-State-" + state);
										logger.info(token + "-Start Date-" + startDate);
										logger.info(token + "-End Date-" + endDate);

										if (planId != null && !planId.isEmpty()
												&& state.equalsIgnoreCase(Constants.NAR_TV_SUBSCRIBER_STATE)) {

											List<Subscriptions> subscriptionsList = new ArrayList<Subscriptions>();

											subscriptionsList = GetPlans.getPlans(token,
													"nar.tv." + getManagerTokenRequest.getLang() + "."
															+ getManagerTokenRequest.getOfferingId(),
													getManagerTokenRequest.getLang(),
													getManagerTokenRequest.getOfferingId(), planId, state, startDate,
													endDate);

											// subscriptionsList =
											// getSubscriptions(token,
											// getManagerTokenRequest.getLang(),
											// getManagerTokenRequest.getOfferingId(),
											// planId, state);

											if (subscriptionsList != null && !subscriptionsList.isEmpty()) {

												getManagerTokenResponse.setSubscriptions(subscriptionsList);
												getManagerTokenResponse.setSubscriberId(id);

												getManagerTokenResponse.setReturnCode(ResponseCodes.SUCESS_CODE_200);
												getManagerTokenResponse.setReturnMsg(ResponseCodes.SUCESS_DES_200);

											} else {

												getManagerTokenResponse.setReturnCode(
														ResponseCodes.NAR_TV_THIRD_PARTY_RESPONSE_NO_ACTIVE_PLAN_CODE);
												getManagerTokenResponse.setReturnMsg(
														ResponseCodes.NAR_TV_THIRD_PARTY_RESPONSE_NO_ACTIVE_PLAN_DESC);

											}

										} else {

											getManagerTokenResponse.setReturnCode(
													ResponseCodes.NAR_TV_THIRD_PARTY_RESPONSE_NO_ACTIVE_PLAN_CODE);
											getManagerTokenResponse.setReturnMsg(
													ResponseCodes.NAR_TV_THIRD_PARTY_RESPONSE_NO_ACTIVE_PLAN_DESC);

										}

									} else {

										getManagerTokenResponse.setReturnCode(
												ResponseCodes.NARTV_THIRDPARY_RESPONSE_DATA_NULL_SUBSCRIPTIONS_CODE);
										getManagerTokenResponse.setReturnMsg(
												ResponseCodes.NARTV_THIRDPARY_RESPONSE_DATA_NULL_SUBSCRIPTIONS_DESC);
									}

								} else {

									getManagerTokenResponse.setReturnCode(
											ResponseCodes.NARTV_THIRDPARY_RESPONSE_DATA_NULL_SUBSCRIPTIONS_CODE);
									getManagerTokenResponse.setReturnMsg(
											ResponseCodes.NARTV_THIRDPARY_RESPONSE_DATA_NULL_SUBSCRIPTIONS_DESC);
								}

							} else {

								getManagerTokenResponse.setReturnCode(ResponseCodes.NARTV_THIRDPARY_RESPONSE_NULL_CODE);
								getManagerTokenResponse.setReturnMsg(ResponseCodes.NARTV_THIRDPARY_RESPONSE_NULL_DESC);
							}

						} else {

							getManagerTokenResponse
									.setReturnCode(ResponseCodes.NARTV_THIRDPARY_RESPONSE_DATA_NULL_SUBSCRIBER_CODE);
							getManagerTokenResponse
									.setReturnMsg(ResponseCodes.NARTV_THIRDPARY_RESPONSE_DATA_NULL_SUBSCRIBER_DESC);
						}

					} else {

						getManagerTokenResponse
								.setReturnCode(ResponseCodes.NARTV_THIRDPARY_RESPONSE_DATA_NULL_SUBSCRIBER_CODE);
						getManagerTokenResponse
								.setReturnMsg(ResponseCodes.NARTV_THIRDPARY_RESPONSE_DATA_NULL_SUBSCRIBER_DESC);
					}

				} else {

					getManagerTokenResponse.setReturnCode(ResponseCodes.NARTV_THIRDPARY_RESPONSE_NULL_CODE);
					getManagerTokenResponse.setReturnMsg(ResponseCodes.NARTV_THIRDPARY_RESPONSE_NULL_DESC);
				}

			} else {

				getManagerTokenResponse.setReturnCode(ResponseCodes.NARTV_THIRDPARY_RESPONSE_DATA_NULL_CODE);
				getManagerTokenResponse.setReturnMsg(ResponseCodes.NARTV_THIRDPARY_RESPONSE_DATA_NULL_DESC);
			}

		} else {

			getManagerTokenResponse.setReturnCode(ResponseCodes.NARTV_THIRDPARY_RESPONSE_NULL_CODE);
			getManagerTokenResponse.setReturnMsg(ResponseCodes.NARTV_THIRDPARY_RESPONSE_NULL_DESC);
		}

		return getManagerTokenResponse;
	}

	public static List<Subscriptions> getSubscriptions(String token, String lang, String offeringId, String planId,
			String state, String startDate, String endDate)
			throws ClassNotFoundException, InstantiationException, IllegalAccessException {

		logger.info(token + "Reading Plans From Database Started...");

		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Subscriptions> getSubscriptionsResponseList = new ArrayList<Subscriptions>();
		try {
			String query = "SELECT * from nartv WHERE store_id=? and status=1 and FIND_IN_SET(?,allowed_for)";
			preparedStatement = DBFactory.getMagentoDBConnection().prepareStatement(query);

			preparedStatement.setString(1, lang);

			preparedStatement.setString(2, offeringId);

			logger.info(token + "-query-" + query);
			logger.info(token + "-query-storeId-" + lang);
			logger.info(token + "-query-offeringId-" + offeringId);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {

				Subscriptions subscriptions = new Subscriptions();

				if (planId.equalsIgnoreCase(resultSet.getString("narId"))
						&& state.equalsIgnoreCase(Constants.NAR_TV_SUBSCRIBER_STATE)) {

					subscriptions.setStatus(Constants.NAR_TV_SUBSCRIBER_STATUS_TRUE);
					subscriptions.setStartDate(Helper.parseDateDynamically(token, startDate, "yyyy-MM-dd HH:mm:ss"));
					subscriptions.setEndDate(Helper.parseDateDynamically(token, endDate, "yyyy-MM-dd HH:mm:ss"));
					subscriptions.setLabelToRenew(
							ConfigurationManager.getConfigurationFromCache("azerfon.nar.tv.to.renew." + lang));
					subscriptions.setLabelRenewalDate(
							ConfigurationManager.getConfigurationFromCache("azerfon.nar.tv.renewal.date." + lang));

				} else {

					subscriptions.setStatus(Constants.NAR_TV_SUBSCRIBER_STATUS_FALSE);
					subscriptions.setStartDate("");
					subscriptions.setEndDate("");
					subscriptions.setLabelToRenew("");
					subscriptions.setLabelRenewalDate("");
				}

				subscriptions.setTitle(resultSet.getString("plan_name"));
				subscriptions.setPrice(resultSet.getString("price"));
				subscriptions.setDescription(resultSet.getString("description"));
				subscriptions.setPlanId(resultSet.getString("narId"));

				getSubscriptionsResponseList.add(subscriptions);

			}

			AppCache.hashmapForPlans.put("nar.tv." + lang + "." + offeringId, getSubscriptionsResponseList);
			logger.info(token + "-Reading Plans From Database Ended.");
			logger.info(token + "-Subscriptions List-" + getSubscriptionsResponseList.toString());

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			logger.info(Helper.GetException(e));
		} finally {
			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					logger.info(Helper.GetException(e));
				}
			}
			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					logger.info(Helper.GetException(e));
				}
			}
		}

		return getSubscriptionsResponseList;
	}

	public static QueryFreeUnitResultMsg queryfreeUnitResponseThirdParty(String token, String msisdn, Logger logger) {

		Constants.logger.info(token + "******************* Query Free unit ********************");
		Constants.logger.info(
				token + "Request reached in AzerfonThirdPartyCalls Class, freeUnitResponseThirdParty Method" + msisdn);

		QueryFreeUnitRequestMsg msg = new QueryFreeUnitRequestMsg();
		QueryFreeUnitRequest qFUR = new QueryFreeUnitRequest();
		QueryObj qO = new QueryObj();
		SubAccessCode sAC = new SubAccessCode();
		sAC.setPrimaryIdentity(msisdn);
		qO.setSubAccessCode(sAC);
		qFUR.setQueryObj(qO);
		msg.setQueryFreeUnitRequest(qFUR);
		// msg.setRequestHeader(CBSBBService.getRequestHeader());
		msg.setRequestHeader(ThirdPartyRequestHeader.getRequestHeader());
		QueryFreeUnitResultMsg response = null;

		logger.info(token + " PrimaryIdentity: " + sAC.getPrimaryIdentity());

		try {
			Constants.logger.info(token + " freeUnitRequestThirdParty: " + Helper.ObjectToJson(msg));
			// response = CBSBBService.getInstance().queryFreeUnit(msg);
			response = ThirdPartyCall.getQueryFreeUnit(msg);
			Constants.logger.info(token + " freeUnitResponseThirdParty: " + Helper.ObjectToJson(response));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.info(Helper.GetException(e));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.info(Helper.GetException(e));
		}

		return response;
	}

	public static QueryFreeUnitResultMsg queryfreeUnitResponseb2BThirdParty(String token, String groupCode,
			Logger logger) {

		Constants.logger.info(token + "******************* Query Free unit B2B ********************");
		Constants.logger.info(
				token + "Request reached in AzerfonThirdPartyCalls Class, queryfreeUnitResponseb2BThirdParty Method"
						+ groupCode);

		QueryFreeUnitRequestMsg msg = new QueryFreeUnitRequestMsg();
		QueryFreeUnitRequest qFUR = new QueryFreeUnitRequest();
		QueryObj qO = new QueryObj();

		SubGroupAccessCode groupAcccesSub = new SubGroupAccessCode();
		groupAcccesSub.setSubGroupCode(groupCode);

		qO.setSubGroupAccessCode(groupAcccesSub);

		qFUR.setQueryObj(qO);
		msg.setQueryFreeUnitRequest(qFUR);
		// msg.setRequestHeader(CBSBBService.getRequestHeader());
		msg.setRequestHeader(ThirdPartyRequestHeader.getRequestHeader());
		QueryFreeUnitResultMsg response = null;

		logger.info(token + " groupAccessCode: " + groupAcccesSub.getSubGroupCode());

		try {
			Constants.logger.info(token + " freeUnitRequestThirdParty B2B : " + Helper.ObjectToJson(msg));
			// response = CBSBBService.getInstance().queryFreeUnit(msg);
			response = ThirdPartyCall.getQueryFreeUnit(msg);
			Constants.logger.info(token + " freeUnitResponseThirdPartyB2B : " + Helper.ObjectToJson(response));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.info(Helper.GetException(e));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.info(Helper.GetException(e));
		}

		return response;
	}

	public static QueryBalanceResultMsg queryBalanceResponseArServices(String msisdn, String token)
			throws IOException, Exception {

		Constants.logger
				.info(token + "*************************  Query Balance ***************************************");
		Constants.logger.info(token + "MSISDN:" + msisdn);

		com.huawei.bme.cbsinterface.arservices.QueryBalanceRequestMsg queryBalanceRequestMsg = new QueryBalanceRequestMsg();
		// queryBalanceRequestMsg.setRequestHeader(CBSARService.getRequestPaymentRequestHeader());
		queryBalanceRequestMsg.setRequestHeader(ThirdPartyRequestHeader.getRequestPaymentRequestHeader());
		QueryBalanceRequest queryBalanceRequest = new QueryBalanceRequest();
		queryBalanceRequest.setBalanceType("");

		com.huawei.cbs.ar.wsservice.arcommon.SubAccessCode subAccessCode = new com.huawei.cbs.ar.wsservice.arcommon.SubAccessCode();
		subAccessCode.setPrimaryIdentity(msisdn);
		// subAccessCode.setSubscriberKey("");

		com.huawei.bme.cbsinterface.arservices.QueryBalanceRequest.QueryObj queryObj = new com.huawei.bme.cbsinterface.arservices.QueryBalanceRequest.QueryObj();
		queryObj.setSubAccessCode(subAccessCode);
		queryBalanceRequest.setQueryObj(queryObj);

		Constants.logger.info(token + " QueryBalanceRequest: " + Helper.ObjectToJson(queryBalanceRequestMsg));

		queryBalanceRequestMsg.setQueryBalanceRequest(queryBalanceRequest);

		// com.huawei.bme.cbsinterface.arservices.QueryBalanceResultMsg
		// queryBalanceArServices = CBSARService.getInstance()
		// .queryBalance(queryBalanceRequestMsg);
		com.huawei.bme.cbsinterface.arservices.QueryBalanceResultMsg queryBalanceArServices = ThirdPartyCall
				.getQueryBalance(queryBalanceRequestMsg);

		Constants.logger.info(token + " QueryBalanceResponse: " + Helper.ObjectToJson(queryBalanceArServices));

		return queryBalanceArServices;
	}

	public static QuerySubLifeCycleResultMsg querySubLifeCycle(String msisdn, String token)
			throws IOException, Exception {

		Constants.logger.info(
				token + "*************************  QuerySubLifeCycle Balance ***************************************");
		Constants.logger.info(token + "MSISDN:" + msisdn);
		Constants.logger.info("");

		QuerySubLifeCycleRequestMsg requestMsg = new QuerySubLifeCycleRequestMsg();
		// requestMsg.setRequestHeader(BcService.getRequestHeader());
		requestMsg.setRequestHeader(ThirdPartyRequestHeader.getCBserviceRequestHeader());
		QuerySubLifeCycleRequest lifeC = new QuerySubLifeCycleRequest();
		com.huawei.bme.cbsinterface.bccommon.SubAccessCode subACode = new com.huawei.bme.cbsinterface.bccommon.SubAccessCode();
		subACode.setPrimaryIdentity(msisdn);
		lifeC.setSubAccessCode(subACode);
		requestMsg.setQuerySubLifeCycleRequest(lifeC);

		Constants.logger.info(token + "Request:" + Helper.ObjectToJson(requestMsg));
		Constants.logger.info("");

		// QuerySubLifeCycleResultMsg result =
		// BcService.getInstance().querySubLifeCycle(requestMsg);
		QuerySubLifeCycleResultMsg result = ThirdPartyCall.getQuerySubLifeCycle(requestMsg);

		Constants.logger.info(token + " QuerySubLifeCycle Response: " + Helper.ObjectToJson(result));
		Constants.logger.info("");

		return result;
	}

	public static QueryHandsetInstallmentRspMsg queryHandSetInstallments(String msisdn, String token)
			throws IOException, Exception {

		Constants.logger.info(token
				+ "*******************START of ThirdPArty Azerfon  Query HandsetInstallement Method ******************* ");

		com.huawei.bss.soaif._interface.common.ReqHeader reqHHLR = new com.huawei.bss.soaif._interface.common.ReqHeader();
		reqHHLR.setChannelId(getRequestHeaderForCRM().getChannelId());
		reqHHLR.setAccessUser(getRequestHeaderForCRM().getAccessUser());
		reqHHLR.setAccessPwd(getRequestHeaderForCRM().getAccessPwd());
		reqHHLR.setTransactionId(Helper.generateTransactionID());
		reqHHLR.setTenantId(getRequestHeaderForCRM().getTenantId());
		reqHHLR.setOperatorId(getRequestHeaderForCRM().getAccessUser());
		QueryHandsetInstallmentReqMsg reqMsg = new QueryHandsetInstallmentReqMsg();
		reqMsg.setServiceNumber(msisdn);
		reqMsg.setReqHeader(reqHHLR);

		Constants.logger.info("");
		Constants.logger.info(token + "Request:" + Helper.ObjectToJson(reqMsg));
		Constants.logger.info("");

		// QueryHandsetInstallmentRspMsg queryHandsetInstallmentRspMsg =
		// SubscriberService.getInstance().queryHandsetInstallment(reqMsg);
		QueryHandsetInstallmentRspMsg queryHandsetInstallmentRspMsg = ThirdPartyCall.setQueryHandsetInstallment(reqMsg);

		Constants.logger.info(token + "Response:" + Helper.ObjectToJson(queryHandsetInstallmentRspMsg));
		Constants.logger.info("");
		Constants.logger.info(token
				+ "*******************END of ThirdPArty Azerfon  Query HandsetInstallement Method******************* ");

		return queryHandsetInstallmentRspMsg;

	}

	public static PaymentResultMsg QueryPostPaidPayment(String msisdn, String cardpinnumber, Logger logger,
			String token) {

		Constants.logger.info(token
				+ "*************************AzerfonThirdPartyCalls  QueryPostPaidPayment Method***************************************");
		logger.info(token + "Requesr Received in QueryPostPaidPayment");

		PaymentRequestMsg paymentRequestMsg = new PaymentRequestMsg();
		// paymentRequestMsg.setRequestHeader(CBSARService.getRequestPaymentRequestHeader());
		paymentRequestMsg.setRequestHeader(ThirdPartyRequestHeader.getRequestPaymentRequestHeader());
		PaymentRequest paymentRequest = new PaymentRequest();
		paymentRequest.setPaymentChannelID(Constants.TOPUP_POSTPAID_PAYMENT_CHANNELID);
		paymentRequest.setOpType(Constants.TOPUP_POSTPAID_OPERATION_TYPE);
		PaymentObj paymentObject = new PaymentObj();
		AcctAccessCode acctAccessCode = new AcctAccessCode();
		acctAccessCode.setPrimaryIdentity(msisdn);
		acctAccessCode.setPayType(Constants.TOPUP_POSTPAID_PAY_TYPE);
		paymentObject.setAcctAccessCode(acctAccessCode);
		paymentRequest.setPaymentObj(paymentObject);
		PaymentInfo paymentInfo = new PaymentInfo();
		com.huawei.bme.cbsinterface.arservices.PaymentRequest.PaymentInfo.CardPayment cardPayment = new com.huawei.bme.cbsinterface.arservices.PaymentRequest.PaymentInfo.CardPayment();
		PaymentResultMsg response = new PaymentResultMsg();

		try {
			logger.info(token + "CardNumber in third Party: " + cardpinnumber);
			// cardPayment.setCardPinNumber(TopUpEncrypterService.encrypt_data(cardpinnumber));
			cardPayment.setCardPinNumber(cardpinnumber);

			paymentInfo.setCardPayment(cardPayment);
			paymentRequest.setPaymentInfo(paymentInfo);
			paymentRequestMsg.setPaymentRequest(paymentRequest);
			// response = CBSARService.getInstance().payment(paymentRequestMsg);
			response = ThirdPartyCall.getPayment(paymentRequestMsg);
			logger.info(token + "Response Azerfon QueryPostPaidPayment" + Helper.ObjectToJson(response));
			return response;
		} catch (Exception ex) {
			logger.info(token + Helper.GetException(ex));
		}

		return response;
	}

	public static RechargeResultMsg QueryPrePaidPayment(String msisdn, String cardpinnumber, Logger logger,
			String token) {

		Constants.logger.info(token
				+ "*************************AzerfonThirdPartyCalls  QueryPrePaidPayment Method***************************************");

		RechargeRequestMsg rechargeRequestMsg = new RechargeRequestMsg();
		RechargeResultMsg rechargeResponse = new RechargeResultMsg();

		RechargeRequest requestMsgRecharge = new RechargeRequest();
		com.huawei.bme.cbsinterface.arservices.RechargeRequest.RechargeObj.AcctAccessCode acctAcessCode = new com.huawei.bme.cbsinterface.arservices.RechargeRequest.RechargeObj.AcctAccessCode();
		acctAcessCode.setPrimaryIdentity(msisdn);
		RechargeObj rechargeObject = new RechargeObj();
		rechargeObject.setAcctAccessCode(acctAcessCode);
		requestMsgRecharge.setRechargeObj(rechargeObject);
		RechargeInfo rechargeInfo = new RechargeInfo();
		CardPayment cardPayment = new CardPayment();

		try {
			// cardPayment.setCardPinNumber(TopUpEncrypterService.encrypt_data(cardpinnumber));
			logger.info(token + "CardNumber in ThirdParty :" + cardpinnumber);
			cardPayment.setCardPinNumber(cardpinnumber);
			rechargeInfo.setCardPayment(cardPayment);
			requestMsgRecharge.setRechargeInfo(rechargeInfo);
			rechargeRequestMsg.setRechargeRequest(requestMsgRecharge);
			// rechargeRequestMsg.setRequestHeader(CBSARService.getRequestPaymentRequestHeader());
			rechargeRequestMsg.setRequestHeader(ThirdPartyRequestHeader.getRequestPaymentRequestHeader());
			// rechargeResponse =
			// CBSARService.getInstance().recharge(rechargeRequestMsg);
			rechargeResponse = ThirdPartyCall.getRecharge(rechargeRequestMsg);
			logger.info(token + "Response Azerfon ESB: " + Helper.ObjectToJson(rechargeResponse));
		} catch (Exception ex) {
			logger.info(token + Helper.GetException(ex));
		}

		return rechargeResponse;
	}

	public static GetProvisionsResponse loanGetProvisions(String token, String transactionName,
			GetProvisionsRequest getProvisionsRequest, GetProvisionsResponse getProvisionsResponse) throws Exception {

		logger.info(token + "-Request Received In " + transactionName + " Third Party-" + "Data-"
				+ getProvisionsRequest.toString());

		// String startDate =
		// Helper.dateFormattorOnlyDate(getProvisionsRequest.getReportStartDate(),
		// token);

		String endDate = Helper.dateFormattorOnlyDate(getProvisionsRequest.getReportEndDate(), token);

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date start = format.parse(getProvisionsRequest.getReportStartDate());
		Date end = format.parse(getProvisionsRequest.getReportEndDate());

		List<Date> datesInRange = new ArrayList<>();
		Calendar startCalendar = new GregorianCalendar();
		startCalendar.setTime(start);
		Calendar endCalendar = new GregorianCalendar();
		endCalendar.setTime(end);

		if (startCalendar.equals(endCalendar)) {
			Date result = startCalendar.getTime();
			datesInRange.add(result);
			startCalendar.add(Calendar.DATE, 30);
		} else {
			while (startCalendar.before(endCalendar)) {
				Date result = startCalendar.getTime();
				datesInRange.add(result);
				startCalendar.add(Calendar.DATE, 30);
			}
		}

		logger.info(token + "-Dates-" + datesInRange.toString());

		int size = datesInRange.size();
		String url = "";
		List<Provisions> provisionsList = new ArrayList<Provisions>();
		int flag = 1;

		logger.info(token + "-Size-" + size);

		for (int i = 0; i < size; i++) {

			if (i == size - 1) {

				url = "GetProvisions?userName="
						+ ConfigurationManager.getConfigurationFromCache("azerfon.loan.username") + "&password="
						+ ConfigurationManager.getConfigurationFromCache("azerfon.loan.password") + "&subscriberId="
						+ Constants.SUBSCRIBER_ID_PREFIX + getProvisionsRequest.getmsisdn() + "&reportStartDate="
						+ Helper.dateFormattorOnlyDate(format.format(datesInRange.get(size - 1)), token)
						+ "&reportEndDate=" + endDate;

			} else {

				url = "GetProvisions?userName="
						+ ConfigurationManager.getConfigurationFromCache("azerfon.loan.username") + "&password="
						+ ConfigurationManager.getConfigurationFromCache("azerfon.loan.password") + "&subscriberId="
						+ Constants.SUBSCRIBER_ID_PREFIX + getProvisionsRequest.getmsisdn() + "&reportStartDate="
						+ Helper.dateFormattorOnlyDate(format.format(datesInRange.get(i)), token) + "&reportEndDate="
						+ Helper.dateFormattorOnlyDate(format.format(datesInRange.get(i + 1)), token);

			}

			logger.info(token + "-Request Call To Third Party");
			logger.info(token + "-Request Path-" + url);

			String response = RestClient.sendCallToAzerfonLoan(token, url);

			if (response != null && !response.isEmpty()) {

				if (Helper.getValueFromJSON(response, "statusCode")
						.equalsIgnoreCase(ResponseCodes.THIRD_PARTY_LOAN_STATUS_CODE_OK)) {

					JSONObject jsonObject = new JSONObject(response);
					JSONArray provisionsJsonArray = jsonObject.getJSONArray("provisions");

					for (int j = 0; j < provisionsJsonArray.length(); j++) {

						JSONObject jsonObjectcollection = provisionsJsonArray.getJSONObject(j);
						Provisions provisions = new Provisions();

						provisions.setDebtId(jsonObjectcollection.getInt("debtId"));
						provisions.setSubscriberId(jsonObjectcollection.getString("subscriberId"));
						provisions.setInitialAmount(jsonObjectcollection.getInt("initialAmount"));
						provisions.setServiceFee(jsonObjectcollection.getDouble("serviceFee"));
						provisions.setCurrentAmount(jsonObjectcollection.getDouble("currentAmount"));
						provisions.setCreditDate(Helper.parseDateDynamically(token,
								jsonObjectcollection.getString("creditDate"), "dd/MM/YY HH:mm:ss"));
						provisions.setSmsNotificationContent(jsonObjectcollection.getString("smsNotificationContent"));

						provisionsList.add(provisions);
					}

				} else if (Helper.getValueFromJSON(response, "statusCode")
						.equalsIgnoreCase(ResponseCodes.THIRD_PARTY_LOAN_STATUS_CODE_NO_HISTORY)) {

					getProvisionsResponse.setReturnCode(ResponseCodes.SUCESS_CODE_200);
					getProvisionsResponse.setReturnMsg(ResponseCodes.NO_HISTORY_DESCRIPTION);
					flag = 0;
					break;

				} else if (Helper.getValueFromJSON(response, "statusCode")
						.equalsIgnoreCase(ResponseCodes.THIRD_PARTY_LOAN_STATUS_CODE_INVALID_SUBSCRIBER_ID)) {

					getProvisionsResponse.setReturnCode(ResponseCodes.INVALID_SUBSCRIBER_ID_CODE);
					getProvisionsResponse.setReturnMsg(ResponseCodes.INVALID_SUBSCRIBER_ID_DESCRIPTION);
					flag = 0;
					break;

				} else if (Helper.getValueFromJSON(response, "statusCode")
						.equalsIgnoreCase(ResponseCodes.THIRD_PARTY_LOAN_STATUS_CODE_DATE_INTERVAL_OUT_OF_RANGE)) {

					getProvisionsResponse.setReturnCode(ResponseCodes.DATE_INTERVAL_OUT_OF_RANGE_CODE);
					getProvisionsResponse.setReturnMsg(ResponseCodes.DATE_INTERVAL_OUT_OF_RANGE_DESCRIPTION);
					flag = 0;
					break;

				} else {

					getProvisionsResponse.setReturnCode(ResponseCodes.GENERIC_ERROR_CODE);
					getProvisionsResponse.setReturnMsg(ResponseCodes.GENERIC_ERROR_DES);
					flag = 0;
					break;
				}

			} else {

				getProvisionsResponse.setReturnCode(ResponseCodes.GENERIC_ERROR_CODE);
				getProvisionsResponse.setReturnMsg(ResponseCodes.GENERIC_ERROR_DES);
				flag = 0;
				break;
			}
		}

		logger.info(token + "-Flag-" + flag);

		if (flag == 1) {

			String temp = "";

			for (int i = 0; i < provisionsList.size(); i++) {

				for (int j = i + 1; j < size; j++) {

					if (Helper.getDateFromString(provisionsList.get(j).getCreditDate(), "dd/MM/YY HH:mm:ss").after(
							Helper.getDateFromString(provisionsList.get(i).getCreditDate(), "dd/MM/YY HH:mm:ss"))) {

						temp = provisionsList.get(i).getCreditDate();
						provisionsList.get(i).setCreditDate(provisionsList.get(j).getCreditDate());
						provisionsList.get(j).setCreditDate(temp);
					}
				}
			}

			List<Provisions> provisionsListFinal = new ArrayList<Provisions>();

			for (int a = provisionsList.size() - 1; a >= 0; a--) {

				provisionsListFinal.add(provisionsList.get(a));
			}

			getProvisionsResponse.setProvisions(provisionsListFinal);
			getProvisionsResponse.setReturnCode(ResponseCodes.SUCESS_CODE_200);
			getProvisionsResponse.setReturnMsg(ResponseCodes.SUCESS_DES_200);
		}

		return getProvisionsResponse;
	}

	public static GetCollectionsResponse loanGetCollections(String token, String transactionName,
			GetCollectionsRequest getCollectionsRequest, GetCollectionsResponse getCollectionsResponse)
			throws Exception {

		logger.info(token + "-Request Received In " + transactionName + " Third Party-" + "Data-"
				+ getCollectionsRequest.toString());

		// String startDate =
		// Helper.dateFormattorOnlyDate(getCollectionsRequest.getReportStartDate(),
		// token);

		String endDate = Helper.dateFormattorOnlyDate(getCollectionsRequest.getReportEndDate(), token);

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date start = format.parse(getCollectionsRequest.getReportStartDate());
		Date end = format.parse(getCollectionsRequest.getReportEndDate());

		List<Date> datesInRange = new ArrayList<>();
		Calendar startCalendar = new GregorianCalendar();
		startCalendar.setTime(start);
		Calendar endCalendar = new GregorianCalendar();
		endCalendar.setTime(end);

		if (startCalendar.equals(endCalendar)) {

			Date result = startCalendar.getTime();
			datesInRange.add(result);
			startCalendar.add(Calendar.DATE, 30);

		} else {

			while (startCalendar.before(endCalendar)) {
				Date result = startCalendar.getTime();
				datesInRange.add(result);
				startCalendar.add(Calendar.DATE, 30);
			}
		}

		logger.info(token + "-Dates-" + datesInRange.toString());

		int size = datesInRange.size();
		String url = "";
		List<Collection> collectionsList = new ArrayList<Collection>();
		int flag = 1;

		logger.info(token + "-Size-" + size);

		for (int i = 0; i < size; i++) {

			if (i == size - 1) {

				url = "GetCollections?userName="
						+ ConfigurationManager.getConfigurationFromCache("azerfon.loan.username") + "&password="
						+ ConfigurationManager.getConfigurationFromCache("azerfon.loan.password") + "&subscriberId="
						+ Constants.SUBSCRIBER_ID_PREFIX + getCollectionsRequest.getmsisdn() + "&reportStartDate="
						+ Helper.dateFormattorOnlyDate(format.format(datesInRange.get(size - 1)), token)
						+ "&reportEndDate=" + endDate;

			} else {

				url = "GetCollections?userName="
						+ ConfigurationManager.getConfigurationFromCache("azerfon.loan.username") + "&password="
						+ ConfigurationManager.getConfigurationFromCache("azerfon.loan.password") + "&subscriberId="
						+ Constants.SUBSCRIBER_ID_PREFIX + getCollectionsRequest.getmsisdn() + "&reportStartDate="
						+ Helper.dateFormattorOnlyDate(format.format(datesInRange.get(i)), token) + "&reportEndDate="
						+ Helper.dateFormattorOnlyDate(format.format(datesInRange.get(i + 1)), token);

			}

			logger.info(token + "-Request Call To Third Party");
			logger.info(token + "-Request Path-" + url);

			String response = RestClient.sendCallToAzerfonLoan(token, url);

			logger.info(token + "-Response Received From Third Party-" + response);

			if (response != null && !response.isEmpty()) {

				if (Helper.getValueFromJSON(response, "statusCode")
						.equalsIgnoreCase(ResponseCodes.THIRD_PARTY_LOAN_STATUS_CODE_OK)) {

					JSONObject jsonObject = new JSONObject(response);
					JSONArray collectionsJsonArray = jsonObject.getJSONArray("collections");

					for (int j = 0; j < collectionsJsonArray.length(); j++) {

						JSONObject jsonObjectcollection = collectionsJsonArray.getJSONObject(j);
						Collection collection = new Collection();
						collection.setDebtId(jsonObjectcollection.getInt("debtId"));
						collection.setLastChargingDate(Helper.parseDateDynamically(token,
								jsonObjectcollection.getString("lastChargingDate"), "dd/MM/YY HH:mm:ss"));
						collection.setAmountToCharge(jsonObjectcollection.getDouble("amountToCharge"));
						logger.info("TEST PAYMENT HISTORY:" + jsonObjectcollection.getDouble("chargedAmount"));
						logger.info("TEST PAYMENT HISTORY:"
								+ Helper.getBakcellMoneyDouble(jsonObjectcollection.getDouble("chargedAmount")));
						collection.setChargedAmount(Helper
								.getBakcellMoneyDouble(jsonObjectcollection.getDouble("chargedAmount")).toString());
						collection.setCollectionOrigin(jsonObjectcollection.getString("collectionOrigin"));
						collection.setAmountPriorCharging(jsonObjectcollection.getDouble("amountPriorCharging"));
						collection.setCustomerBalance(jsonObjectcollection.getDouble("customerBalance"));
						collection.setSmsNotificationContent(jsonObjectcollection.getString("smsNotificationContent"));

						collectionsList.add(collection);
					}

				} else if (Helper.getValueFromJSON(response, "statusCode")
						.equalsIgnoreCase(ResponseCodes.THIRD_PARTY_LOAN_STATUS_CODE_NO_HISTORY)) {

					getCollectionsResponse.setReturnCode(ResponseCodes.SUCESS_CODE_200);
					getCollectionsResponse.setReturnMsg(ResponseCodes.NO_HISTORY_DESCRIPTION);
					flag = 0;
					break;

				} else if (Helper.getValueFromJSON(response, "statusCode")
						.equalsIgnoreCase(ResponseCodes.THIRD_PARTY_LOAN_STATUS_CODE_INVALID_SUBSCRIBER_ID)) {

					getCollectionsResponse.setReturnCode(ResponseCodes.INVALID_SUBSCRIBER_ID_CODE);
					getCollectionsResponse.setReturnMsg(ResponseCodes.INVALID_SUBSCRIBER_ID_DESCRIPTION);
					flag = 0;
					break;

				} else if (Helper.getValueFromJSON(response, "statusCode")
						.equalsIgnoreCase(ResponseCodes.THIRD_PARTY_LOAN_STATUS_CODE_DATE_INTERVAL_OUT_OF_RANGE)) {

					getCollectionsResponse.setReturnCode(ResponseCodes.DATE_INTERVAL_OUT_OF_RANGE_CODE);
					getCollectionsResponse.setReturnMsg(ResponseCodes.DATE_INTERVAL_OUT_OF_RANGE_DESCRIPTION);
					flag = 0;
					break;

				} else {

					getCollectionsResponse.setReturnCode(ResponseCodes.GENERIC_ERROR_CODE);
					getCollectionsResponse.setReturnMsg(ResponseCodes.GENERIC_ERROR_DES);
					flag = 0;
					break;
				}

			} else {

				getCollectionsResponse.setReturnCode(ResponseCodes.GENERIC_ERROR_CODE);
				getCollectionsResponse.setReturnMsg(ResponseCodes.GENERIC_ERROR_DES);
				flag = 0;
				break;
			}
		}

		logger.info(token + "-Flag-" + flag);

		if (flag == 1) {

			String temp = "";

			for (int i = 0; i < collectionsList.size(); i++) {

				for (int j = i + 1; j < size; j++) {

					if (Helper.getDateFromString(collectionsList.get(j).getLastChargingDate(), "dd/MM/YY HH:mm:ss")
							.after(Helper.getDateFromString(collectionsList.get(i).getLastChargingDate(),
									"dd/MM/YY HH:mm:ss"))) {

						temp = collectionsList.get(i).getLastChargingDate();
						collectionsList.get(i).setLastChargingDate(collectionsList.get(j).getLastChargingDate());
						collectionsList.get(j).setLastChargingDate(temp);
					}
				}
			}

			List<Collection> collectionsListFinal = new ArrayList<Collection>();

			for (int a = collectionsList.size() - 1; a >= 0; a--) {

				collectionsListFinal.add(collectionsList.get(a));
			}

			getCollectionsResponse.setCollections(collectionsListFinal);
			getCollectionsResponse.setReturnCode(ResponseCodes.SUCESS_CODE_200);
			getCollectionsResponse.setReturnMsg(ResponseCodes.SUCESS_DES_200);
		}

		return getCollectionsResponse;
	}

	public static GetRequestFeeChargingsResponse loanGetRequestFeeChargings(String token, String transactionName,
			GetRequestFeeChargingsRequest getRequestFeeChargingsRequest,
			GetRequestFeeChargingsResponse getRequestFeeChargingsResponse) throws Exception {

		logger.info(token + "-Request Received In " + transactionName + " Third Party-" + "Data-"
				+ getRequestFeeChargingsRequest.toString());

		String startDate = Helper.dateFormattorOnlyDate(getRequestFeeChargingsRequest.getReportStartDate(), token);
		String endDate = Helper.dateFormattorOnlyDate(getRequestFeeChargingsRequest.getReportEndDate(), token);

		String url = "GetRequestFeeChargings?userName="
				+ ConfigurationManager.getConfigurationFromCache("azerfon.loan.username") + "&password="
				+ ConfigurationManager.getConfigurationFromCache("azerfon.loan.password") + "&subscriberId="
				+ Constants.SUBSCRIBER_ID_PREFIX + getRequestFeeChargingsRequest.getmsisdn() + "&reportStartDate="
				+ startDate + "&reportEndDate=" + endDate;

		logger.info(token + "-Request Call To Third Party");
		logger.info(token + "-Request Path-" + url);

		String response = RestClient.sendCallToAzerfonLoan(token, url);

		if (response != null && !response.isEmpty()) {

			if (Helper.getValueFromJSON(response, "statusCode")
					.equalsIgnoreCase(ResponseCodes.THIRD_PARTY_LOAN_STATUS_CODE_OK)) {

				getRequestFeeChargingsResponse = Helper.JsonToObject(response, GetRequestFeeChargingsResponse.class);

				getRequestFeeChargingsResponse.setReturnCode(ResponseCodes.SUCESS_CODE_200);
				getRequestFeeChargingsResponse.setReturnMsg(ResponseCodes.SUCESS_DES_200);

			} else {

				getRequestFeeChargingsResponse.setReturnCode(ResponseCodes.GENERIC_ERROR_CODE);
				getRequestFeeChargingsResponse.setReturnMsg(ResponseCodes.GENERIC_ERROR_DES);
			}

		} else {

			getRequestFeeChargingsResponse.setReturnCode(ResponseCodes.GENERIC_ERROR_CODE);
			getRequestFeeChargingsResponse.setReturnMsg(ResponseCodes.GENERIC_ERROR_DES);
		}

		logger.info(token + "-Response Received From Third Party-" + response);

		return getRequestFeeChargingsResponse;
	}

	public static ProvideNewCreditResponse loanProvideNewCredit(String token, String transactionName,
			ProvideNewCreditRequest provideNewCreditRequest, ProvideNewCreditResponse provideNewCreditResponse)
			throws Exception {

		logger.info(token + "-Request Received In " + transactionName + " Third Party-" + "Data-"
				+ provideNewCreditRequest.toString());

		String url = "ProvideCredit?userName=" + ConfigurationManager.getConfigurationFromCache("azerfon.loan.username")
				+ "&password=" + ConfigurationManager.getConfigurationFromCache("azerfon.loan.password")
				+ "&subscriberId=" + Constants.SUBSCRIBER_ID_PREFIX + provideNewCreditRequest.getmsisdn()
				+ "&creditType=CBSmart";

		logger.info(token + "-Request Call To Third Party");
		logger.info(token + "-Request Path-" + url);

		String response = RestClient.sendCallToAzerfonLoan(token, url);

		if (response != null && !response.isEmpty()) {

			if (Helper.getValueFromJSON(response, "statusCode")
					.equalsIgnoreCase(ResponseCodes.THIRD_PARTY_LOAN_STATUS_CODE_OK)) {

				provideNewCreditResponse = Helper.JsonToObject(response, ProvideNewCreditResponse.class);

				provideNewCreditResponse.setReturnCode(ResponseCodes.SUCESS_CODE_200);
				provideNewCreditResponse.setReturnMsg(ResponseCodes.SUCESS_DES_200);

			} else {

				provideNewCreditResponse.setReturnCode(ResponseCodes.GENERIC_ERROR_CODE);
				provideNewCreditResponse.setReturnMsg(ResponseCodes.GENERIC_ERROR_DES);
			}

		} else {

			provideNewCreditResponse.setReturnCode(ResponseCodes.GENERIC_ERROR_CODE);
			provideNewCreditResponse.setReturnMsg(ResponseCodes.GENERIC_ERROR_DES);
		}

		logger.info(token + "-Response Received From Third Party-" + response);

		return provideNewCreditResponse;
	}

	public static GetDebtInfoResponse loanGetDebitInfo(String token, String transactionName,
			GetDebtInfoRequest getDebtInfoRequest, GetDebtInfoResponse getDebtInfoResponse) throws Exception {

		logger.info(token + "-Request Received In  loanGetDebitInfo" + transactionName + " AzerfonThird Party-"
				+ "Data-" + getDebtInfoRequest.toString());

		String url = "GetDebtInfo?userName=" + ConfigurationManager.getConfigurationFromCache("azerfon.loan.username")
				+ "&password=" + ConfigurationManager.getConfigurationFromCache("azerfon.loan.password")
				+ "&subscriberId=" + Constants.SUBSCRIBER_ID_PREFIX + getDebtInfoRequest.getmsisdn();

		logger.info(token + "-Request Call To loanGetDebitInfo Third Party");
		logger.info(token + "-Request Path loanGetDebitInfo : " + url);

		String response = RestClient.sendCallToAzerfonLoan(token, url);

		if (response != null && !response.isEmpty()) {

			if (Helper.getValueFromJSON(response, "statusCode")
					.equalsIgnoreCase(ResponseCodes.THIRD_PARTY_LOAN_STATUS_CODE_OK)) {

				getDebtInfoResponse = Helper.JsonToObject(response, GetDebtInfoResponse.class);

				getDebtInfoResponse.setReturnCode(ResponseCodes.SUCESS_CODE_200);
				getDebtInfoResponse.setReturnMsg(ResponseCodes.SUCESS_DES_200);

			} else {

				getDebtInfoResponse.setReturnCode(ResponseCodes.GENERIC_ERROR_CODE);
				getDebtInfoResponse.setReturnMsg(ResponseCodes.GENERIC_ERROR_DES);
			}

		} else {

			getDebtInfoResponse.setReturnCode(ResponseCodes.GENERIC_ERROR_CODE);
			getDebtInfoResponse.setReturnMsg(ResponseCodes.GENERIC_ERROR_DES);
		}

		logger.info(token + "-Response Received From Third Party-" + response);

		return getDebtInfoResponse;
	}

	public static GetLoanPaymentHistoryResponse loanPaymentHistory(String token, String transactionName,
			GetLoanPaymentHistoryRequest getLoanPaymentHistoryRequest,
			GetLoanPaymentHistoryResponse getLoanPaymentHistoryResponse) throws Exception {

		logger.info(token + "-Request Received In " + transactionName + " Third Party-" + "Data-"
				+ getLoanPaymentHistoryRequest.toString());

		/******************
		 * Commented For Now Until APIs Are Finalized From Client
		 ******************/

		// String url = "GetDebtInfo?userName=" +
		// ConfigurationManager.getConfigurationFromCache("azerfon.loan.username")
		// + "&password=" +
		// ConfigurationManager.getConfigurationFromCache("azerfon.loan.password")
		// + "&subscriberId=" + Constants.SUBSCRIBER_ID_PREFIX +
		// getLoanPaymentHistoryRequest.getmsisdn();
		//
		// logger.info(token + "-Request Call To Third Party");
		// logger.info(token + "-Request Path-" + url);
		//
		// String response = RestClient.sendCallToAzerfonLoan(token, url);

		/******************
		 * Commented For Now Until APIs Are Finalized From Client
		 ******************/

		String response = "{\"returnMsg\":\"Sucessfull\",\"statusCode\":\"OK\",\"loanPayment\":[{\"loanID\":\"10206800000487336920170817141243\",\"dateTime\":\"2017-09-08 15:04:12\",\"amount\":\"1.0\"},{\"loanID\":\"10206800000487336920170905143351\",\"dateTime\":\"2017-09-08 15:04:12\",\"amount\":\"2.0\"},{\"loanID\":\"10206800000487336920170905143323\",\"dateTime\":\"2017-09-08 15:04:12\",\"amount\":\"1.0\"},{\"loanID\":\"10206800000487336920170817154729\",\"dateTime\":\"2017-09-08 15:04:12\",\"amount\":\"1.0\"},{\"loanID\":\"10206800000487336920170817141233\",\"dateTime\":\"2017-09-07 03:12:43\",\"amount\":\"0.031097\"},{\"loanID\":\"10206800000487336920170817141243\",\"dateTime\":\"2017-09-07 03:12:43\",\"amount\":\"0.0\"},{\"loanID\":\"10206800000487336920170817141233\",\"dateTime\":\"2017-09-07 03:04:23\",\"amount\":\"0.968903\"},{\"loanID\":\"10206800000487336920170817141011\",\"dateTime\":\"2017-09-07 03:02:48\",\"amount\":\"0.831097\"},{\"loanID\":\"10206800000487336920170817141233\",\"dateTime\":\"2017-09-07 03:02:48\",\"amount\":\"0.0\"},{\"loanID\":\"10206800000487336920170817141011\",\"dateTime\":\"2017-09-06 17:38:54\",\"amount\":\"1.0E-4\"},{\"loanID\":\"10206800000487336920170817141011\",\"dateTime\":\"2017-09-06 17:37:04\",\"amount\":\"1.0E-4\"},{\"loanID\":\"10206800000487336920170817141011\",\"dateTime\":\"2017-09-06 17:35:26\",\"amount\":\"1.0E-4\"},{\"loanID\":\"10206800000487336920170817141011\",\"dateTime\":\"2017-09-06 17:33:24\",\"amount\":\"1.0E-4\"},{\"loanID\":\"10206800000487336920170817141011\",\"dateTime\":\"2017-09-06 17:32:52\",\"amount\":\"1.0E-4\"},{\"loanID\":\"10206800000487336920170817141011\",\"dateTime\":\"2017-09-06 17:23:11\",\"amount\":\"1.0E-4\"},{\"loanID\":\"10206800000487336920170817141011\",\"dateTime\":\"2017-09-06 14:07:09\",\"amount\":\"1.0E-4\"},{\"loanID\":\"10206800000487336920170817140803\",\"dateTime\":\"2017-09-01 22:36:21\",\"amount\":\"0.631797\"},{\"loanID\":\"10206800000487336920170817141011\",\"dateTime\":\"2017-09-01 22:36:21\",\"amount\":\"0.168203\"},{\"loanID\":\"10206800000487336920170817140639\",\"dateTime\":\"2017-09-01 22:29:14\",\"amount\":\"1.0\"},{\"loanID\":\"10206800000487336920170817140803\",\"dateTime\":\"2017-09-01 22:29:14\",\"amount\":\"0.368203\"},{\"loanID\":\"10206800000487336920170817135619\",\"dateTime\":\"2017-09-01 21:54:29\",\"amount\":\"1.0\"},{\"loanID\":\"10206800000487336920170817140639\",\"dateTime\":\"2017-09-01 21:54:29\",\"amount\":\"0.0\"}]}";

		if (response != null && !response.isEmpty()) {

			if (Helper.getValueFromJSON(response, "statusCode")
					.equalsIgnoreCase(ResponseCodes.THIRD_PARTY_LOAN_STATUS_CODE_OK)) {

				getLoanPaymentHistoryResponse = Helper.JsonToObject(response, GetLoanPaymentHistoryResponse.class);

				getLoanPaymentHistoryResponse.setReturnCode(ResponseCodes.SUCESS_CODE_200);
				getLoanPaymentHistoryResponse.setReturnMsg(ResponseCodes.SUCESS_DES_200);

			} else {

				getLoanPaymentHistoryResponse.setReturnCode(ResponseCodes.GENERIC_ERROR_CODE);
				getLoanPaymentHistoryResponse.setReturnMsg(ResponseCodes.GENERIC_ERROR_DES);
			}

		} else {

			getLoanPaymentHistoryResponse.setReturnCode(ResponseCodes.GENERIC_ERROR_CODE);
			getLoanPaymentHistoryResponse.setReturnMsg(ResponseCodes.GENERIC_ERROR_DES);
		}

		logger.info(token + "-Response Received From Third Party-" + response);

		return getLoanPaymentHistoryResponse;
	}

	public static GetLoanRequestHistoryResponse loanRequestHistory(String token, String transactionName,
			GetLoanRequestHistoryRequest getLoanRequestHistoryRequest,
			GetLoanRequestHistoryResponse getLoanRequestHistoryResponse) throws Exception {

		logger.info(token + "-Request Received In " + transactionName + " Third Party-" + "Data-"
				+ getLoanRequestHistoryRequest.toString());

		/******************
		 * Commented For Now Until APIs Are Finalized From Client
		 ******************/

		// String url = "GetDebtInfo?userName=" +
		// ConfigurationManager.getConfigurationFromCache("azerfon.loan.username")
		// + "&password=" +
		// ConfigurationManager.getConfigurationFromCache("azerfon.loan.password")
		// + "&subscriberId=" + Constants.SUBSCRIBER_ID_PREFIX +
		// getLoanRequestHistoryRequest.getmsisdn();
		//
		// logger.info(token + "-Request Call To Third Party");
		// logger.info(token + "-Request Path-" + url);
		//
		// String response = RestClient.sendCallToAzerfonLoan(token, url);

		/******************
		 * Commented For Now Until APIs Are Finalized From Client
		 ******************/

		String response = "{\"returnMsg\":\"Sucessfull\",\"statusCode\":\"OK\",\"loanRequest\":[{\"loanID\":\"10057800000487526920170907132704\",\"dateTime\":\"2016-12-09 00:07:13\",\"amount\":\"2\",\"status\":\"Open\",\"paid\":\"0\",\"remaining\":\"2\"},{\"loanID\":\"10057800000487526920170907131611\",\"dateTime\":\"2016-12-09 00:07:13\",\"amount\":\"2\",\"status\":\"Open\",\"paid\":\"0\",\"remaining\":\"2\"},{\"loanID\":\"10057800000487526920170907131607\",\"dateTime\":\"2016-12-09 00:07:13\",\"amount\":\"2\",\"status\":\"Open\",\"paid\":\"0\",\"remaining\":\"2\"}]}";

		if (response != null && !response.isEmpty()) {

			if (Helper.getValueFromJSON(response, "statusCode")
					.equalsIgnoreCase(ResponseCodes.THIRD_PARTY_LOAN_STATUS_CODE_OK)) {

				getLoanRequestHistoryResponse = Helper.JsonToObject(response, GetLoanRequestHistoryResponse.class);

				getLoanRequestHistoryResponse.setReturnCode(ResponseCodes.SUCESS_CODE_200);
				getLoanRequestHistoryResponse.setReturnMsg(ResponseCodes.SUCESS_DES_200);

			} else {

				getLoanRequestHistoryResponse.setReturnCode(ResponseCodes.GENERIC_ERROR_CODE);
				getLoanRequestHistoryResponse.setReturnMsg(ResponseCodes.GENERIC_ERROR_DES);
			}

		} else {

			getLoanRequestHistoryResponse.setReturnCode(ResponseCodes.GENERIC_ERROR_CODE);
			getLoanRequestHistoryResponse.setReturnMsg(ResponseCodes.GENERIC_ERROR_DES);
		}

		logger.info(token + "-Response Received From Third Party-" + response);

		return getLoanRequestHistoryResponse;
	}

	public static GetLoanResponse getLoan(String token, String transactionName, GetLoanRequest getLoanRequest,
			GetLoanResponse getLoanResponse) throws Exception {

		logger.info(token + "-Request Received In " + transactionName + " Third Party-" + "Data-"
				+ getLoanRequest.toString());

		String url = "";
		String response = "";

		if (getLoanRequest.getFriendMsisdn() != null && !getLoanRequest.getFriendMsisdn().isEmpty()) {

			GetSubscriberResponse respns = new com.evampsaanga.b2b.azerfon.getsubscriber.CRMSubscriberService()
					.GetSubscriberRequest(getLoanRequest.getFriendMsisdn());

			if (respns.getResponseHeader().getRetCode().equals("0")) {

				url = "ProvideNewAForBCredit?userName="
						+ ConfigurationManager.getConfigurationFromCache("azerfon.loan.username") + "&password="
						+ ConfigurationManager.getConfigurationFromCache("azerfon.loan.password") + "&subscriberId="
						+ Constants.SUBSCRIBER_ID_PREFIX + getLoanRequest.getmsisdn()
						+ "&creditType=CBSmart&subscriberBId=" + Constants.SUBSCRIBER_ID_PREFIX
						+ getLoanRequest.getFriendMsisdn();
				response = RestClient.sendCallToAzerfonLoan(token, url);

			} else {

				response = "-1";
			}

		} else {

			url = "ProvideNewCredit?userName=" + ConfigurationManager.getConfigurationFromCache("azerfon.loan.username")
					+ "&password=" + ConfigurationManager.getConfigurationFromCache("azerfon.loan.password")
					+ "&subscriberId=" + Constants.SUBSCRIBER_ID_PREFIX + getLoanRequest.getmsisdn()
					+ "&creditType=CBSmart";
			response = RestClient.sendCallToAzerfonLoan(token, url);
			if (response != null && !response.isEmpty() && !response.equals("-1")) {
				/**
				 * skipped on 05-08-2020. Now onwards, Simberella will handle
				 * charging at their end.
				 */
				// AzerfonThirdPartyCalls.responseChargecustomer(token,
				// getLoanRequest.getmsisdn(), "500328");
			}
		}

		// logger.info(token + "-Request Call To Third Party");
		// logger.info(token + "-Request Path-" + url);

		if (response != null && !response.isEmpty() && !response.equals("-1")) {

			if (Helper.getValueFromJSON(response, "statusCode")
					.equalsIgnoreCase(ResponseCodes.THIRD_PARTY_LOAN_STATUS_CODE_OK)) {

				getLoanResponse = Helper.JsonToObject(response, GetLoanResponse.class);

				getLoanResponse.setReturnCode(ResponseCodes.SUCESS_CODE_200);
				getLoanResponse.setReturnMsg(ResponseCodes.SUCESS_DES_200);

			} else {

				getLoanResponse.setReturnCode(ResponseCodes.GENERIC_ERROR_CODE);
				getLoanResponse.setReturnMsg(ResponseCodes.GENERIC_ERROR_DES);
			}

		} else if ("-1".equals(response)) {
			getLoanResponse.setReturnCode(ResponseCodes.SUBSCRIBER_NOT_FOUND_CODE);
			getLoanResponse.setReturnMsg(ResponseCodes.SUBSCRIBER_NOT_FOUND_DESC);
		}

		else {

			getLoanResponse.setReturnCode(ResponseCodes.GENERIC_ERROR_CODE);
			getLoanResponse.setReturnMsg(ResponseCodes.GENERIC_ERROR_DES);
		}
		logger.info(token + "-Response Received From Third Party-" + Helper.ObjectToJson(getLoanResponse));
		logger.info(token + "-Response Received From Third Party-" + response);

		return getLoanResponse;
	}

	public static GetSubscriberHistoryResponse loanGetSubscriberHistory(String token, String transactionName,
			GetSubscriberHistoryRequest getSubscriberHistoryRequest,
			GetSubscriberHistoryResponse getSubscriberHistoryResponse) throws Exception {

		logger.info(token + "-Request Received In " + transactionName + " Third Party-" + "Data-"
				+ getSubscriberHistoryRequest.toString());

		String startDate = Helper.dateFormattorOnlyDate(getSubscriberHistoryRequest.getStartDate(), token);
		String endDate = Helper.dateFormattorOnlyDate(getSubscriberHistoryRequest.getEndDate(), token);

		String url = "GetSubscriberHistory?userName="
				+ ConfigurationManager.getConfigurationFromCache("azerfon.loan.username") + "&password="
				+ ConfigurationManager.getConfigurationFromCache("azerfon.loan.password") + "&subscriberId="
				+ Constants.SUBSCRIBER_ID_PREFIX + "708333419" + "&reportStartDate=" + startDate + "&reportEndDate="
				+ endDate;

		logger.info(token + "-Request Call To Third Party");
		logger.info(token + "-Request Path-" + url);

		String response = RestClient.sendCallToAzerfonLoan(token, url);

		if (response != null && !response.isEmpty()) {

			logger.info(token + "response Is not Null ot NOT EMPY");

			if (Helper.getValueFromJSON(response, "statusCode")
					.equalsIgnoreCase(ResponseCodes.THIRD_PARTY_LOAN_STATUS_CODE_OK)) {

				JSONObject jsonResponse = new JSONObject(response);

				if (getSubscriberHistoryRequest.getUseCase().equalsIgnoreCase(Constants.USE_CASE_REQUEST_HISTORY)) {

					JSONArray provisions = jsonResponse.getJSONArray("provisions");
					List<LoanRequest> provisionsFinalArray = new ArrayList<LoanRequest>();

					for (int i = 0; i < provisions.length(); i++) {

						JSONObject objectProvisions = provisions.getJSONObject(i);
						LoanRequest loanRequest = new LoanRequest();

						loanRequest.setLoanID(Integer.toString(objectProvisions.getInt("debtId")));
						loanRequest.setRemaining(Double.toString(objectProvisions.getDouble("currentAmount")));
						loanRequest.setDateTime(objectProvisions.getString("creditDate"));
						loanRequest.setPaid(Double.toString(
								(objectProvisions.getDouble("initialAmount") + objectProvisions.getDouble("serviceFee"))
										- objectProvisions.getDouble("currentAmount")));
						loanRequest.setStatus(objectProvisions.getDouble("currentAmount") <= 0 ? Constants.STATUS_PAID
								: Constants.STATUS_IN_PROGRESS);
						loanRequest.setAmount(Double.toString(objectProvisions.getDouble("currentAmount")));

						provisionsFinalArray.add(loanRequest);
					}

					getSubscriberHistoryResponse.setLoanRequest(provisionsFinalArray);

				} else if (getSubscriberHistoryRequest.getUseCase()
						.equalsIgnoreCase(Constants.USE_CASE_PAYMENT_HISTORY)) {

					JSONArray collections = jsonResponse.getJSONArray("collections");
					List<LoanPayment> collectionsFinalArray = new ArrayList<LoanPayment>();

					for (int i = 0; i < collections.length(); i++) {

						JSONObject objectCollections = collections.getJSONObject(i);
						LoanPayment loanPayment = new LoanPayment();

						loanPayment.setLoanID(Integer.toString(objectCollections.getInt("debtId")));
						loanPayment.setDateTime(objectCollections.getString("lastChargingDate"));
						loanPayment.setAmount(Double.toString(objectCollections.getDouble("chargedAmount")));

						collectionsFinalArray.add(loanPayment);
					}

					getSubscriberHistoryResponse.setLoanPayment(collectionsFinalArray);
				}

				getSubscriberHistoryResponse.setReturnCode(ResponseCodes.SUCESS_CODE_200);
				getSubscriberHistoryResponse.setReturnMsg(ResponseCodes.SUCESS_DES_200);

			} else if (Helper.getValueFromJSON(response, "statusCode")
					.equalsIgnoreCase(ResponseCodes.THIRD_PARTY_LOAN_STATUS_CODE_NO_HISTORY)) {

				getSubscriberHistoryResponse.setReturnCode(ResponseCodes.SUCESS_CODE_200);
				getSubscriberHistoryResponse.setReturnMsg(ResponseCodes.NO_HISTORY_DESCRIPTION);

			} else if (Helper.getValueFromJSON(response, "statusCode")
					.equalsIgnoreCase(ResponseCodes.THIRD_PARTY_LOAN_STATUS_CODE_INVALID_SUBSCRIBER_ID)) {

				getSubscriberHistoryResponse.setReturnCode(ResponseCodes.INVALID_SUBSCRIBER_ID_CODE);
				getSubscriberHistoryResponse.setReturnMsg(ResponseCodes.INVALID_SUBSCRIBER_ID_DESCRIPTION);

			} else if (Helper.getValueFromJSON(response, "statusCode")
					.equalsIgnoreCase(ResponseCodes.THIRD_PARTY_LOAN_STATUS_CODE_DATE_INTERVAL_OUT_OF_RANGE)) {

				getSubscriberHistoryResponse.setReturnCode(ResponseCodes.DATE_INTERVAL_OUT_OF_RANGE_CODE);
				getSubscriberHistoryResponse.setReturnMsg(ResponseCodes.DATE_INTERVAL_OUT_OF_RANGE_DESCRIPTION);

			} else {

				getSubscriberHistoryResponse.setReturnCode(ResponseCodes.GENERIC_ERROR_CODE);
				getSubscriberHistoryResponse.setReturnMsg(ResponseCodes.GENERIC_ERROR_DES);
			}

		} else {

			logger.info(token + "-Response Received From Third Party Null OR Empty-" + response);
			getSubscriberHistoryResponse.setReturnCode(ResponseCodes.GENERIC_ERROR_CODE);
			getSubscriberHistoryResponse.setReturnMsg(ResponseCodes.GENERIC_ERROR_DES);
		}

		logger.info(token + "-Response Received From Third Party-" + response);

		return getSubscriberHistoryResponse;
	}

	public static RequestMoneyResponse requestMoney(String token, String transactionName,
			RequestMoneyRequest requestMoneyRequest, RequestMoneyResponse requestMoneyResponse) throws Exception {

		logger.info(token + "-Request Received In " + transactionName + " Third Party-" + "Data-"
				+ requestMoneyRequest.toString());

		if (requestMoneyRequest.getFriendMsisdn() != null && !requestMoneyRequest.getFriendMsisdn().isEmpty()) {

		} else {

			try {

				int flag = DBFactory.insertMessageIntoEsmg(requestMoneyRequest.getmsisdn(),
						ConfigurationManager.getConfigurationFromCache(
								"azerfon.loan.request.money.182." + requestMoneyRequest.getLang()),
						requestMoneyRequest.getLang(), "", null, token);

				if (flag == 1) {

					requestMoneyResponse.setReturnCode(ResponseCodes.SUCESS_CODE_200);
					requestMoneyResponse.setReturnMsg(ResponseCodes.SUCESS_DES_200);

				} else {

					requestMoneyResponse.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
					requestMoneyResponse.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
				}

			} catch (Exception ex) {

				logger.error(Helper.GetException(ex));
			}

		}

		logger.info(token + "-Response Received From Third Party-" + requestMoneyResponse);

		return requestMoneyResponse;
	}

	/*
	 * private static void changeSupplimentaryOffers() {
	 * 
	 * SubmitOrderRequest submitOrderRequestMsgReq = new SubmitOrderRequest();
	 * SubmitRequestBody submitrequestBody = new SubmitRequestBody(); OrderInfo
	 * orderInfo = new OrderInfo(); orderInfo.setOrderType("CO025");
	 * submitrequestBody.setOrder(orderInfo); OrderItems orderItems = new
	 * OrderItems(); OrderItemValue orderItem = new OrderItemValue();
	 * OrderItemInfo orderItemInfo = new OrderItemInfo();
	 * orderItemInfo.setOrderItemType("CO025");
	 * orderItem.setOrderItemInfo(orderItemInfo); SubscriberInfo subscriberInfo
	 * = new SubscriberInfo(); subscriberInfo.setServiceNumber("776480535");
	 * OfferingInfo offeringInfo = new OfferingInfo();
	 * offeringInfo.setActionType("1"); OfferingKey offeringID = new
	 * OfferingKey(); offeringID.setOfferingId("1566931668");
	 * offeringInfo.setOfferingId(offeringID);
	 * subscriberInfo.getSupplementaryOfferingList().getOfferingInfo().add(
	 * offeringInfo); orderItem.setSubscriber(subscriberInfo);
	 * orderItems.getOrderItem().add(orderItem);
	 * submitrequestBody.setOrderItems(orderItems);
	 * submitOrderRequestMsgReq.setSubmitRequestBody(submitrequestBody);
	 * submitOrderRequestMsgReq.setRequestHeader(OrderHandleService.
	 * getReqHeaderForChangeSupplementary());
	 * OrderHandleService.getInstance().submitOrder(submitOrderRequestMsgReq); }
	 */

	public static GetGroupResponse orderQueryGroupData(String msisdn, String token) {

		logger.info(token
				+ "*******************START of ThirdPArty Azerfon orderQueryGroupData Method ******************* ");
		GetGroupResponse resp = new GetGroupResponse();

		try {
			GetGroupRequest getGroupDataRequestMsgReq = new GetGroupRequest();
			// getGroupDataRequestMsgReq.setRequestHeader(CRMServices.getReqHeaderForOrderQueryGroup());
			getGroupDataRequestMsgReq.setRequestHeader(ThirdPartyRequestHeader.getReqHeaderForOrderQueryGroup());
			GetGroupDataIn getGroupDataIn = new GetGroupDataIn();
			// getGroupDataIn.setCustomerId(getCustomerResponse.getGetCustomerBody().getCustomerId());
			getGroupDataIn.setServiceNumber(msisdn);
			getGroupDataRequestMsgReq.setGetGroupBody(getGroupDataIn);
			// resp =
			// CRMServices.getInstance().getGroupData(getGroupDataRequestMsgReq);
			resp = ThirdPartyCall.getGroupData(getGroupDataRequestMsgReq);
			logger.info(token + "Response From Azerfon GetGroupResponse: " + Helper.ObjectToJson(resp));
		} catch (Exception e) {
			logger.info(token + Helper.GetException(e));
		}

		logger.info(
				token + "*******************END of ThirdPArty Azerfon orderQueryGroupData Method ******************* ");
		return resp;
	}

	public static AdjustmentResultMsg adjustmentResponse(String token, Long debitValues, Long creditValues,
			String debitUnitType, String creditUnitType, String msisdn, String expiryDate) {
		logger.info(token + "Request Landed in in AzerfonThirdPartyCalls,  method adjustmentResponse");

		logger.info(token + "ExpiryDate Before :" + expiryDate);
		expiryDate = Helper.generatingDateTimeBeforeFourHours(expiryDate, token);
		logger.info(token + "ExpiryDate After :" + expiryDate);
		AdjustmentRequestMsg adjustmentRequestMsg = new AdjustmentRequestMsg();
		AdjustmentRequest adjustmentRequest = new AdjustmentRequest();

		com.huawei.bme.cbsinterface.arservices.AdjustmentRequest.AdjustmentObj adjustmentObj = new AdjustmentObj();
		com.huawei.cbs.ar.wsservice.arcommon.SubAccessCode subAccessCode = new com.huawei.cbs.ar.wsservice.arcommon.SubAccessCode();
		subAccessCode.setPrimaryIdentity(msisdn);
		// adjustmentObj.setAcctAccessCode(value);
		// adjustmentObj.setCustAccessCode(value);
		adjustmentObj.setSubAccessCode(subAccessCode);
		// adjustmentObj.setSubGroupAccessCode(value);
		// adjustmentRequest.set

		// List<BalanceAdjustment> adjustmentInfo = new ArrayList<>();
		// BalanceAdjustment balanceAdjustment = new BalanceAdjustment();
		// balanceAdjustment.setAdjustmentAmt(value); // adjustment
		// amount->Optional
		// balanceAdjustment.setAdjustmentType(value); // adjustment
		// type->->Optional
		// balanceAdjustment.setBalanceID(value); // balance ID->Optional
		// balanceAdjustment.setBalanceType(value); // balance type->->Optional
		// balanceAdjustment.setCurrencyID(value); // currencyID->Optional
		// balanceAdjustment.setEffectiveTime(value); // effectiveTime->Optional
		// balanceAdjustment.setExpireTime(value); // expireTime->Optional
		//
		//
		// adjustmentInfo.add(balanceAdjustment);
		// adjustmentRequest.setAdjustmentInfo(adjustmentInfo);
		adjustmentRequest.setAdjustmentObj(adjustmentObj);

		List<FreeUnitAdjustmentInfo> listOffreeUnitAdjustmentInfo = new ArrayList<>();

		FreeUnitAdjustmentInfo freeUnitAdjustmentInfo = new FreeUnitAdjustmentInfo();
		freeUnitAdjustmentInfo.setAdjustmentAmt(debitValues); // seconds/kbs
		freeUnitAdjustmentInfo.setAdjustmentType("2"); // 1:CR & 2:DR for Credit
														// Expiry-time and
														// expiration-time is
														// needed.but not for
														// Debit
		freeUnitAdjustmentInfo.setEffectiveTime(Helper
				.generatingDateTimeBeforeFourHours(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()), token)); // effective
		// time
		// ->Optional
		// freeUnitAdjustmentInfo.setFreeUnitInstanceID(value);//free unit
		// instance ID ->Optional
		// freeUnitAdjustmentInfo.setFreeUnitType("C_Nar_Garden_All_net_minutes");
		// //free unit type ID ->Optional

		freeUnitAdjustmentInfo.setFreeUnitType(debitUnitType); // free unit type
																// ID ->Optional
		freeUnitAdjustmentInfo.setExpireTime(expiryDate); // expire time which

		// START OF DEBIT

		FreeUnitAdjustmentInfo freeUnitAdjustmentInfoDeb = new FreeUnitAdjustmentInfo();
		freeUnitAdjustmentInfoDeb.setAdjustmentAmt(creditValues); // seconds/kbs
		freeUnitAdjustmentInfoDeb.setAdjustmentType("1"); // 1:CR & 2:DR for
															// Credit
															// Expiry-time and
															// expiration-time
															// is
															// needed.but not
															// for Debit
		freeUnitAdjustmentInfoDeb.setEffectiveTime(Helper
				.generatingDateTimeBeforeFourHours(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()), token)); // effective
		// time
		// //
		// ->Optional
		// freeUnitAdjustmentInfo.setFreeUnitInstanceID(value);//free unit
		// instance ID ->Optional
		// freeUnitAdjustmentInfo.setFreeUnitType("C_Nar_Garden_All_net_minutes");
		// //free unit type ID ->Optional
		freeUnitAdjustmentInfoDeb.setFreeUnitType(creditUnitType); // free unit
																	// type ID
																	// ->Optional
		freeUnitAdjustmentInfoDeb.setExpireTime(expiryDate); // expire time
																// which
		// END OF DEBIT

		listOffreeUnitAdjustmentInfo.add(freeUnitAdjustmentInfo);
		listOffreeUnitAdjustmentInfo.add(freeUnitAdjustmentInfoDeb);

		adjustmentRequest.setFreeUnitAdjustmentInfo(listOffreeUnitAdjustmentInfo);
		adjustmentRequest.setAdjustmentSerialNo(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
		adjustmentRequest.setOpType("1");
		adjustmentRequest.setAdjustmentReasonCode("FUAREQ1");

		List<SimpleProperty> additionalProperty = new ArrayList<>();
		SimpleProperty simpleProperty = new SimpleProperty();
		simpleProperty.setCode("Remark");
		simpleProperty.setValue("01:1234|02:878sf|03:Test of Free MBs Add API");
		additionalProperty.add(simpleProperty);
		SimpleProperty simpleProperty2 = new SimpleProperty();
		simpleProperty2.setCode("SPID");
		simpleProperty2.setValue("E-Care");

		adjustmentRequest.setAdditionalProperty(additionalProperty);
		adjustmentRequestMsg.setAdjustmentRequest(adjustmentRequest);
		// adjustmentRequestMsg.setRequestHeader(CBSARService.getRequestPaymentRequestHeader());
		adjustmentRequestMsg.setRequestHeader(ThirdPartyRequestHeader.getRequestPaymentRequestHeader());
		// AdjustmentResultMsg resp =
		// CBSARService.getInstance().adjustment(adjustmentRequestMsg);
		AdjustmentResultMsg resp = ThirdPartyCall.getAdjustment(adjustmentRequestMsg);

		try {
			logger.info(token + "Response adjustment " + Helper.ObjectToJson(resp));
		} catch (IOException e) {
			logger.info(token + Helper.GetException(e));

		} catch (Exception e) {
			logger.info(token + Helper.GetException(e));
		}

		return resp;
	}

	public static AdjustmentResultMsg simSwapadjustmentResponse(String token, String transactionId, String msisdn) {

		logger.info(token + "Request Landed in  AzerfonThirdPartyCalls,  method adjustmentResponse with params:::"
				+ msisdn + "and" + transactionId);

		AdjustmentRequestMsg adjustmentRequestMsg = new AdjustmentRequestMsg();
		AdjustmentRequest adjustmentRequest = new AdjustmentRequest();

		com.huawei.bme.cbsinterface.arservices.AdjustmentRequest.AdjustmentObj adjustmentObj = new AdjustmentObj();

		com.huawei.bme.cbsinterface.arservices.AdjustmentRequest.AdjustmentObj.AcctAccessCode value = new com.huawei.bme.cbsinterface.arservices.AdjustmentRequest.AdjustmentObj.AcctAccessCode();
		value.setPrimaryIdentity(msisdn);
		value.setPayType("2");
		adjustmentObj.setAcctAccessCode(value);

		adjustmentRequest.setAdjustmentObj(adjustmentObj);
		adjustmentRequest.setAdjustmentSerialNo(transactionId);
		adjustmentRequest.setOpType("1");
		List<BalanceAdjustment> adjustmentInfo = new ArrayList<>();
		BalanceAdjustment balanceAdjustment = new BalanceAdjustment();
		balanceAdjustment.setAdjustmentType("1");
		balanceAdjustment.setAdjustmentAmt(300000L);
		adjustmentInfo.add(balanceAdjustment);

		adjustmentRequest.setAdjustmentInfo(adjustmentInfo);

		adjustmentRequestMsg.setAdjustmentRequest(adjustmentRequest);
		com.huawei.bme.cbsinterface.cbscommon.RequestHeader reqHeader = new com.huawei.bme.cbsinterface.cbscommon.RequestHeader();

		reqHeader = getRequestHeaderForSimSwap(reqHeader);
		adjustmentRequestMsg.setRequestHeader(reqHeader);
		AdjustmentResultMsg resp = ThirdPartyCall.getAdjustment(adjustmentRequestMsg);

		try {
			logger.info(token + "Response adjustment " + Helper.ObjectToJson(resp));
		} catch (IOException e) {
			logger.info(token + Helper.GetException(e));

		} catch (Exception e) {
			logger.info(token + Helper.GetException(e));
		}

		return resp;
	}

	// Headers for Sim Swap Adjustment

	public static com.huawei.bme.cbsinterface.cbscommon.RequestHeader getRequestHeaderForSimSwap(
			com.huawei.bme.cbsinterface.cbscommon.RequestHeader reqH) {

		reqH.setVersion("1.2");
		reqH.setBusinessCode(Constants.TOPUP_POSTPAID_BUSINESS_CODE);
		reqH.setMessageSeq(BcService.generateTransactionID());
		OwnershipInfo ownerShipInfo = new OwnershipInfo();
		ownerShipInfo.setBEID(Constants.TOPUP_POSTPAID_BEID);

		reqH.setOwnershipInfo(ownerShipInfo);
		SecurityInfo accesSecurityInfo = new SecurityInfo();
		accesSecurityInfo.setLoginSystemCode("ecare");
		accesSecurityInfo.setPassword("Abs1234%");
		reqH.setAccessSecurity(accesSecurityInfo);
		OperatorInfo operatorInfo = new OperatorInfo();
		operatorInfo.setOperatorID("205");
		operatorInfo.setChannelID("3");
		reqH.setOperatorInfo(operatorInfo);
		// reqH.setAccessMode(Constants.TOPUP_POSTPAID_ACCESS_MODE);
		// reqH.setMsgLanguageCode(Constants.TOPUP_POSTPAID_LANGUAGE_CODE);
		// TimeFormat timeFormat = new TimeFormat();
		// timeFormat.setTimeType(Constants.TOPUP_POSTPAID_TIME_TYPE);
		// timeFormat.setTimeZoneID(Constants.TOPUP_POSTPAID_TIME_ZONEID);
		// reqH.setTimeFormat(timeFormat);
		return reqH;
	}

	public static SubmitOrderResponse changeSupplimentaryOffers(String msisdn, String offeringId, String actionType) {
		// TODO Auto-generated method stub

		SubmitOrderRequest submitOrderRequestMsgReq = new SubmitOrderRequest();
		SubmitRequestBody submitrequestBody = new SubmitRequestBody();
		submitOrderRequestMsgReq.setRequestHeader(getReqHeaderForChangeLanguage());
		OrderInfo orderInfo = new OrderInfo();
		orderInfo.setOrderType("CO025");
		submitrequestBody.setOrder(orderInfo);
		OrderItems orderItems = new OrderItems();
		OrderItemValue orderItem = new OrderItemValue();
		OrderItemInfo orderItemInfo = new OrderItemInfo();
		orderItemInfo.setOrderItemType("CO025");
		// orderItemInfo.setIsCustomerNotification("0");
		// orderItemInfo.setIsPartnerNotification("0");
		orderItem.setOrderItemInfo(orderItemInfo);
		SubscriberInfo subscriberInfo = new SubscriberInfo();
		subscriberInfo.setServiceNumber(msisdn);
		OfferingInfo offeringInfo = new OfferingInfo();
		offeringInfo.setActionType(actionType);
		OfferingKey offeringID = new OfferingKey();
		offeringID.setOfferingId(offeringId);
		offeringInfo.setOfferingId(offeringID);
		// offeringInfo.setEffectMode("0");
		// offeringInfo.setActiveMode("A");
		// OfferingExtParameterList extParmList = new
		// OfferingExtParameterList();
		// OfferingExtParameterInfo offertingExtparamInfo = new
		// OfferingExtParameterInfo();
		// offertingExtparamInfo.setParamName("509703");
		// offertingExtparamInfo.setParamValue("776480535");
		// extParmList.getParameterInfo().add(offertingExtparamInfo);
		// offeringInfo.setExtParamList(extParmList);
		subscriberInfo.getSupplementaryOfferingList().getOfferingInfo().add(offeringInfo);
		orderItem.setSubscriber(subscriberInfo);
		orderItems.getOrderItem().add(orderItem);
		submitrequestBody.setOrderItems(orderItems);
		submitOrderRequestMsgReq.setSubmitRequestBody(submitrequestBody);

		// com.huawei.crm.service.ens.SubmitOrderResponse response =
		// OrderHandleService.getInstance().submitOrder(submitOrderRequestMsgReq);
		com.huawei.crm.service.ens.SubmitOrderResponse response = ThirdPartyCall.submitOrder(submitOrderRequestMsgReq);
		return response;
	}

	public static GetCustomerResponse GetcustomerDataAzerfon(String msisdn, Logger logger, String token) {

		GetCustomerRequest cDR = new GetCustomerRequest();
		cDR.setRequestHeader(GetCustomerDataLand.getRequestHeader());
		GetCustomerIn gCI = new GetCustomerIn();
		gCI.setServiceNumber(msisdn);
		cDR.setGetCustomerBody(gCI);

		// GetCustomerResponse sR =
		// CRMServices.getInstance().getCustomerData(cDR);
		GetCustomerResponse sR = ThirdPartyCall.getCustomerData(cDR);
		return sR;
	}

	public static RequestHeader getRequestHeaderForCRM() {
		RequestHeader reqhForCRM = new RequestHeader();
		reqhForCRM.setChannelId(ConfigurationManager.getConfigurationFromCache("crm.sub.ChannelId").trim());
		reqhForCRM.setTechnicalChannelId(
				ConfigurationManager.getConfigurationFromCache("crm.sub.TechnicalChannelId").trim());
		reqhForCRM.setAccessUser(ConfigurationManager.getConfigurationFromCache("crm.sub.AccessUser").trim());
		reqhForCRM.setTenantId(ConfigurationManager.getConfigurationFromCache("crm.sub.TenantId").trim());
		reqhForCRM.setAccessPwd(ConfigurationManager.getConfigurationFromCache("crm.sub.AccessPwd").trim());
		reqhForCRM.setTestFlag(ConfigurationManager.getConfigurationFromCache("crm.sub.TestFlag").trim());
		reqhForCRM.setTransactionId(Helper.generateTransactionID());
		reqhForCRM.setLanguage(ConfigurationManager.getConfigurationFromCache("crm.sub.Language").trim());
		return reqhForCRM;
	}

	public static com.huawei.crm.basetype.RequestHeader getReqHeaderForGETNetworkSetting() {
		com.huawei.crm.basetype.RequestHeader reqH = new com.huawei.crm.basetype.RequestHeader();
		reqH.setVersion("1");
		reqH.setChannelId("3");
		reqH.setTechnicalChannelId("53");
		reqH.setAccessUser("ecare");
		reqH.setAccessPwd("Abc1234%");
		reqH.setTransactionId(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
		reqH.setTenantId("101");
		reqH.setTestFlag("0");
		reqH.setLanguage("2002");

		return reqH;
	}

	// CharegeCustomer
	public static ChargeForSubscriberRspMsg responseChargecustomer(String token, String msisdn, String contentId) {
		logger.info(token + "Request Lanaded in Azerfon ThirdParty responseChargecustomer " + msisdn);
		ChargeForSubscriberReqMsg request = new ChargeForSubscriberReqMsg();
		// request.setReqHeader(RechargeService.getRechargeRequestHeader());
		request.setReqHeader(ThirdPartyRequestHeader.getRechargeRequestHeader());
		ObjectAccessInfo accessInfo = new ObjectAccessInfo();
		accessInfo.setObjectId(msisdn);
		accessInfo.setObjectIdType("4");
		request.setAccessInfo(accessInfo);
		request.setContentId(contentId);// 405661 For Exchange 500328 for loan
		request.setDescription("Charge Subscriber");
		request.setQuantity(1);
		// ChargeForSubscriberRspMsg response =
		// RechargeService.getInstance().chargeForSubscriber(request);
		ChargeForSubscriberRspMsg response = ThirdPartyCall.getChargeForSubscriber(request);
		try {
			logger.info(token + "ResponseChargeSubscriber " + Helper.ObjectToJson(response));
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(token + Helper.GetException(e));
		}
		return response;
	}

	public static TariffDetailsMagentoResponse tariffDetailsV2BulkResponse(String request) {
		com.evampsaanga.b2b.magento.tariffdetailsv2.TariffDetailsMagentoResponse data = new com.evampsaanga.b2b.magento.tariffdetailsv2.TariffDetailsMagentoResponse();
		try {

			String token = Helper.retrieveToken(Transactions.CHANGE_TARRIF_TRANSACTION_NAME,
					Helper.getValueFromJSON(request, "msisdn"));
			String response = RestClient.SendCallToMagento(token,
					ConfigurationManager.getConfigurationFromCache("magento.app.tariffV2"), request.toString());

			data = Helper.JsonToObject(response,
					com.evampsaanga.b2b.magento.tariffdetailsv2.TariffDetailsMagentoResponse.class);

			logger.info("TariffResponse FROM magento >>:" + response);

		} catch (IOException e) {
			logger.error(Helper.GetException(e));
			e.printStackTrace();
		} catch (Exception e) {
			logger.error(Helper.GetException(e));
			e.printStackTrace();
		}

		return data;
	}

	public static TariffDetailsMagentoResponse tariffDetailsV2BulkResponseChangeLimit(String request) {
		com.evampsaanga.b2b.magento.tariffdetailsv2.TariffDetailsMagentoResponse data = new com.evampsaanga.b2b.magento.tariffdetailsv2.TariffDetailsMagentoResponse();
		try {

			String token = Helper.retrieveToken(Transactions.CHANGE_TARRIF_TRANSACTION_NAME,
					Helper.getValueFromJSON(request, "msisdn"));
			String response = RestClient.SendCallToMagento(token,
					ConfigurationManager.getConfigurationFromCache("magento.app.tariffV2.change.limit"),
					request.toString());

			data = Helper.JsonToObject(response,
					com.evampsaanga.b2b.magento.tariffdetailsv2.TariffDetailsMagentoResponse.class);

			logger.info("TariffResponse FROM magento >>:" + response);

		} catch (IOException e) {
			logger.error(Helper.GetException(e));
			e.printStackTrace();
		} catch (Exception e) {
			logger.error(Helper.GetException(e));
			e.printStackTrace();
		}

		return data;
	}

	public static SubmitOrderResponse changeLimit(String msisdn, String accountId, String creditlimit,
			String parameterValue) throws Exception {

		logger.info("LANDED IN THIRD PARTY CHANGE LIMIT METHOD");
		// TODO Auto-generated method stub
		SubmitOrderRequest submitOrderRequestMsgReq = new SubmitOrderRequest();
		submitOrderRequestMsgReq.setRequestHeader(getReqHeaderForChangeLanguage());
		SubmitRequestBody submitRequestBody = new SubmitRequestBody();
		OrderInfo order = new OrderInfo();
		order.setOrderType("CO027"); // simswap->CO016

		submitRequestBody.setOrder(order);
		OrderItems orderItems = new OrderItems();
		OrderItemValue OrderItemValue = new OrderItemValue();
		OrderItemInfo ordertieminfo = new OrderItemInfo();
		ordertieminfo.setOrderItemType("CO027");
		OrderItemValue.setOrderItemInfo(ordertieminfo);
		SubscriberInfo subscriber = new SubscriberInfo();
		subscriber.setServiceNumber(msisdn); // 776480535
		// AccountList accountList = new AccountList();
		// AccountInfo accountInfo = new AccountInfo();

		AccountEntity accountEntity = new AccountEntity();
		logger.info("ACCOUNT ID IS::::::::" + accountId);
		accountEntity.setAccountId(Long.parseLong(accountId));// 11000005415028L
		if (!creditlimit.isEmpty())
			accountEntity.setCreditLimit(Long.parseLong(creditlimit)); // B2B
																		// uncomment
																		// only
																		// in
																		// case
																		// of
																		// PayBySubs
		ExtParameterList extParameterList = new ExtParameterList();
		ExtParameterInfo extParameterInfo = new ExtParameterInfo();
		extParameterInfo.setParamName("reliability");
		extParameterInfo.setParamValue(parameterValue);
		extParameterList.getParameterInfo().add(extParameterInfo);
		accountEntity.setExtParamList(extParameterList);

		// OfferingExtParameterList offeringExtParameterList = new
		// OfferingExtParameterList();
		// OfferingExtParameterInfo offeringExtParameterInfo = new
		// OfferingExtParameterInfo();

		// OrderItemValue.setSubscriber(subscriber);
		OrderItemValue.setAccount(accountEntity);

		orderItems.getOrderItem().add(OrderItemValue);
		submitRequestBody.setOrderItems(orderItems);
		submitOrderRequestMsgReq.setSubmitRequestBody(submitRequestBody);
		logger.info("Change limit Request::::" + Helper.ObjectToJson(submitOrderRequestMsgReq));
		return ThirdPartyCall.submitOrder(submitOrderRequestMsgReq);
	}

	public static SubmitOrderResponse changetariff(String msisdn, String offeringid, String token) {
		logger.info(token + "-Request Received In changetarrifresponse Method Azerfon B2B Call-" + "Data-" + msisdn
				+ ": " + offeringid);
		SubmitOrderRequest submitOrderRequestMsgReq = new SubmitOrderRequest();
		submitOrderRequestMsgReq.setRequestHeader(getReqHeaderForChangeLanguage());
		SubmitRequestBody submitRequestBody = new SubmitRequestBody();
		OrderInfo oInfo = new OrderInfo();
		oInfo.setOrderType(Constants.CHANGE_TARIFF_ORDER_TYPE);
		OrderItemInfo oitemInfo = new OrderItemInfo();
		oitemInfo.setOrderItemType(Constants.CHANGE_TARIFF_ORDER_ITEM_TYPE);
		oitemInfo.setIsCustomerNotification(Constants.CHANGE_TARIFF_CUSTOMER_NOTIFICATION);
		oitemInfo.setIsPartnerNotification(Constants.CHANGE_TARIFF_PARAMETER_NOTIFICATION);
		submitRequestBody.setOrder(oInfo);
		OrderItems OrderItems = new OrderItems();
		OrderItemValue OrderItemValue = new OrderItemValue();
		OrderItemValue.setOrderItemInfo(oitemInfo);
		SubscriberInfo subscriber = new SubscriberInfo();
		subscriber.setServiceNumber(msisdn);
		subscriber.setPrimaryOfferingInfo(new OfferingInfo());
		subscriber.getPrimaryOfferingInfo().setActionType(Constants.CHANGE_TARIFF_ACTION_TYPE);
		OfferingKey offeringId = new OfferingKey();
		offeringId.setOfferingId(offeringid);
		subscriber.getPrimaryOfferingInfo().setActiveMode(Constants.CHANGE_TARIFF_ACTION_MODE);
		subscriber.getPrimaryOfferingInfo().setEffectMode(Constants.CHANGE_TARIFF_EFFECTIVE_MODE);
		subscriber.getPrimaryOfferingInfo().setOfferingId(offeringId);
		OrderItemValue.setSubscriber(subscriber);
		OrderItems.getOrderItem().add(OrderItemValue);
		submitRequestBody.setOrderItems(OrderItems);
		submitOrderRequestMsgReq.setSubmitRequestBody(submitRequestBody);
		SubmitOrderResponse response = ThirdPartyCall.submitOrder(submitOrderRequestMsgReq);
		try {
			logger.info(
					token + "-Response from changetarrifresponse Azerfon B2B Call-" + Helper.ObjectToJson(response));
		} catch (IOException e) {
			logger.info(token + Helper.GetException(e));
		} catch (Exception e) {
			logger.info(token + Helper.GetException(e));
		}
		return response;
	}

	public static RequestHeader getReqHeaderForChangeLanguage() {
		RequestHeader reqH = new RequestHeader();
		reqH.setVersion("1");
		reqH.setChannelId("3");
		reqH.setTechnicalChannelId("53");
		reqH.setAccessUser("ecare");
		reqH.setAccessPwd("Abc1234%");
		reqH.setTransactionId(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + System.currentTimeMillis());
		reqH.setTenantId("101");
		return reqH;
	}
}
