package com.evampsaanga.b2b.azerfon.actionhistory;

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;
import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * Request Packet for action history
 * @author Aqeel Abbas
 *
 */

public class ActionHistoryRequest extends BaseRequest {
	private String startDate;
	private String endDate;
	@JsonProperty(value ="orderKey",required = false)
	private String orderKey;

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getOrderKey() {
		return orderKey;
	}

	public void setOrderKey(String orderKey) {
		this.orderKey = orderKey;
	}
}
