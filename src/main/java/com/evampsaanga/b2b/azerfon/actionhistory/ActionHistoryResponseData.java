package com.evampsaanga.b2b.azerfon.actionhistory;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Response Data Container For Action History
 * 
 * @author Aqeel Abbas
 *
 */

@JsonPropertyOrder({ "orderId", "date", "orderType", "orderKey", "orderStatus", "totalCount", "success", "pending",
		"failed", "cancelled" })

public class ActionHistoryResponseData {
	private String orderId;
	private String date;
	private String orderType;
	private String orderKey;
	private String orderStatus;
	private String totalCount = "0";
	private String success = "0";
	private String pending = "0";
	private String failed = "0";
	private String cancelled = "0";
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	public String getOrderKey() {
		return orderKey;
	}
	public void setOrderKey(String orderKey) {
		this.orderKey = orderKey;
	}
	public String getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	public String getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(String totalCount) {
		this.totalCount = totalCount;
	}
	public String getSuccess() {
		return success;
	}
	public void setSuccess(String success) {
		this.success = success;
	}
	public String getPending() {
		return pending;
	}
	public void setPending(String pending) {
		this.pending = pending;
	}
	public String getFailed() {
		return failed;
	}
	public void setFailed(String failed) {
		this.failed = failed;
	}
	public String getCancelled() {
		return cancelled;
	}
	public void setCancelled(String cancelled) {
		this.cancelled = cancelled;
	}



}
