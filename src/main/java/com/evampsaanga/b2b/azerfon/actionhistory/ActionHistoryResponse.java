package com.evampsaanga.b2b.azerfon.actionhistory;

import java.util.ArrayList;

import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;

/**
 * Response Container For Action History
 * @author Aqeel Abbas
 *
 */
public class ActionHistoryResponse extends BaseResponse{
	private ArrayList<ActionHistoryResponseData> orderList;

	public ArrayList<ActionHistoryResponseData> getOrderList() {
		return orderList;
	}

	public void setOrderList(ArrayList<ActionHistoryResponseData> orderList) {
		this.orderList = orderList;
	}

}
