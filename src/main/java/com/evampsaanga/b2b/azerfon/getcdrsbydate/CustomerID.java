package com.evampsaanga.b2b.azerfon.getcdrsbydate;

public class CustomerID {
	private String cbscustomerId;
	private String crmcustomerId;

	public String getCbscustomerId() {
		return cbscustomerId;
	}

	public void setCbscustomerId(String cbscustomerId) {
		this.cbscustomerId = cbscustomerId;
	}

	public String getCrmcustomerId() {
		return crmcustomerId;
	}

	public void setCrmcustomerId(String crmcustomerId) {
		this.crmcustomerId = crmcustomerId;
	}
}
