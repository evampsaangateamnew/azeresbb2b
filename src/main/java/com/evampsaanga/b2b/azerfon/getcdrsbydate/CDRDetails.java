package com.evampsaanga.b2b.azerfon.getcdrsbydate;

public class CDRDetails {
	String number = "";
	String startDateTime = "";
	String endDateTime = "";
	String service = "";
	String period = "";
	String destination = "";
	String usage = "";
	String chargedAmount = "";
	String zone = "";
	String type = "";

	public CDRDetails() {
	}

	public CDRDetails(String number, String startDateTime, String endDateTime, String service, String period,
			String destination, String usage, String chargedAmount, String type, String zone) {
		super();
		this.number = number;
		this.startDateTime = startDateTime;
		this.endDateTime = endDateTime;
		this.service = service;
		this.period = period;
		this.destination = destination;
		this.usage = usage;
		this.chargedAmount = chargedAmount;
		this.type = type;
		this.zone = zone;
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	/**
	 * @return the number
	 */
	public String getNumber() {
		return number;
	}

	/**
	 * @param number
	 *            the number to set
	 */
	public void setNumber(String number) {
		this.number = number;
	}

	/**
	 * @return the startDateTime
	 */
	public String getStartDateTime() {
		return startDateTime;
	}

	/**
	 * @param startDateTime
	 *            the startDateTime to set
	 */
	public void setStartDateTime(String startDateTime) {
		this.startDateTime = startDateTime;
	}

	/**
	 * @return the endDateTime
	 */
	public String getEndDateTime() {
		return endDateTime;
	}

	/**
	 * @param endDateTime
	 *            the endDateTime to set
	 */
	public void setEndDateTime(String endDateTime) {
		this.endDateTime = endDateTime;
	}

	/**
	 * @return the service
	 */
	public String getService() {
		return service;
	}

	/**
	 * @param service
	 *            the service to set
	 */
	public void setService(String service) {
		this.service = service;
	}

	/**
	 * @return the period
	 */
	public String getPeriod() {
		return period;
	}

	/**
	 * @param period
	 *            the period to set
	 */
	public void setPeriod(String period) {
		this.period = period;
	}

	/**
	 * @return the destination
	 */
	public String getDestination() {
		return destination;
	}

	/**
	 * @param destination
	 *            the destination to set
	 */
	public void setDestination(String destination) {
		this.destination = destination;
	}

	/**
	 * @return the usage
	 */
	public String getUsage() {
		return usage;
	}

	/**
	 * @param usage
	 *            the usage to set
	 */
	public void setUsage(String usage) {
		this.usage = usage;
	}

	/**
	 * @return the chargedAmount
	 */
	public String getChargedAmount() {
		return chargedAmount;
	}

	/**
	 * @param chargedAmount
	 *            the chargedAmount to set
	 */
	public void setChargedAmount(String chargedAmount) {
		this.chargedAmount = chargedAmount;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
}
