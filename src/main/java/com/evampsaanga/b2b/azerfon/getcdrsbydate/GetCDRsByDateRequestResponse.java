package com.evampsaanga.b2b.azerfon.getcdrsbydate;

import java.util.ArrayList;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetCDRsByDateRequestResponse")
@XmlRootElement(name = "GetCDRsByDateRequestResponse")
public class GetCDRsByDateRequestResponse extends BaseResponse {
	ArrayList<CDRDetails> records = new ArrayList<>();

	/**
	 * @return the records
	 */
	public ArrayList<CDRDetails> getRecords() {
		return records;
	}

	/**
	 * @param records
	 *            the records to set
	 */
	public void setRecords(ArrayList<CDRDetails> records) {
		this.records = records;
	}

	public GetCDRsByDateRequestResponse() {
		super();
	}
}
