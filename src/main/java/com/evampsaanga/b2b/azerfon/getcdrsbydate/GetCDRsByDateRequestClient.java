package com.evampsaanga.b2b.azerfon.getcdrsbydate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "GetCDRsByDateRequestClient")
public class GetCDRsByDateRequestClient extends com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest {
	@XmlElement(name = "pin", required = true)
	String pin = "";
	@XmlElement(name = "accountId", required = true)
	String accountId = "";
	@XmlElement(name = "customerId", required = true)
	String customerId = "";

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	/**
	 * @return the accountId
	 */
	public String getAccountId() {
		return accountId;
	}

	/**
	 * @param accountId
	 *            the accountId to set
	 */
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	/**
	 * @return the pin
	 */
	public String getPin() {
		return pin;
	}

	/**
	 * @param pin
	 *            the pin to set
	 */
	public void setPin(String pin) {
		this.pin = pin;
	}

	@XmlElement(name = "startDate", required = true)
	String startDate = "";
	@XmlElement(name = "endDate", required = true)
	String endDate = "";

	/**
	 * @return the startDate
	 */
	public String getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate
	 *            the startDate to set
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the endDate
	 */
	public String getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate
	 *            the endDate to set
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public GetCDRsByDateRequestClient() {
	}
}
