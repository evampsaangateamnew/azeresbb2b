package com.evampsaanga.b2b.azerfon.usagedetailspdf;

import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;

public class UsageDetailsPdfResponse extends BaseResponse {
	
	private String pdfPath;

	public String getPdfPath() {
		return pdfPath;
	}

	public void setPdfPath(String pdfPath) {
		this.pdfPath = pdfPath;
	}
	
}
