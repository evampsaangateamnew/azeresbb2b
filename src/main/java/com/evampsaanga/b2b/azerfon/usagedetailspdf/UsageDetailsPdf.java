package com.evampsaanga.b2b.azerfon.usagedetailspdf;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;

import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.amqimplementationsesb.LogsType;
import com.evampsaanga.b2b.azerfon.getcdrsbydate.GetCDRsByDateLand;
import com.evampsaanga.b2b.azerfon.getcdrsbydate.GetCDRsByDateRequestResponse;
import com.evampsaanga.b2b.azerfon.msisdninvoice.MSISDNInvoiceRequest;
import com.evampsaanga.b2b.azerfon.msisdninvoice.MSISDNSummary;
import com.evampsaanga.b2b.azerfon.msisdninvoice.MSISDNSummaryResponse;
import com.evampsaanga.b2b.azerfon.msisdninvoice.SummaryData;
import com.evampsaanga.b2b.azerfon.pdf.Pdf;
import com.evampsaanga.b2b.azerfon.pdf.PdfToEmailLand;
import com.evampsaanga.b2b.azerfon.sendemail.SendEmailResponse;
import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.ThirdPartyNames;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Decrypter;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.evampsaanga.b2b.developer.utils.SOAPLoggingHandler;
import com.google.gson.JsonObject;

@Path("/azerfon")
public class UsageDetailsPdf {
	public static final Logger loggerV2 = Logger.getLogger("azerfon-esb");
	public static String modeulName = "USAGE DETAILS PDF";
	
	
	@POST
	@Path("/getusagedetailspdf")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UsageDetailsPdfResponse getUsageDetailsPdf(@Body String requestBody, @Header("credentials") String credential ,@Header("Content-Type") String contentType) {

		Logs logs = new Logs();
		logs.setTransactionName(Transactions.USAGE_DETAILS_PDF);
		logs.setThirdPartyName(ThirdPartyNames.ODS);
		logs.setTableType(LogsType.UsageDetailsPdf);
		UsageDetailsPdfRequest cclient = null;
		UsageDetailsPdfResponse resp = new UsageDetailsPdfResponse();

		String token;

		try {
			cclient = Helper.JsonToObject(requestBody, UsageDetailsPdfRequest.class);
			token = Helper.retrieveToken(Transactions.USAGE_DETAILS_PDF, Helper.getValueFromJSON(requestBody, "msisdn"));
			logMessage("REQUEST: " + Helper.ObjectToJson(cclient));
			if (cclient != null) {
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				logs.setLang(cclient.getLang());
			}
		} catch (Exception ex) {
			loggerV2.info(Helper.GetException(ex));
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_401);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		if (cclient != null) {
			String credentials = null;
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
			} catch (Exception ex) {
				SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials == null) {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}

			if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				try {

					if (cclient.getIsSendMail().equalsIgnoreCase("true")) { // if send mail is true
						
						loggerV2.info(token+"Entering in if Because Request is for Send Mail");

						if (!cclient.getMailTo().isEmpty()) { // check if email is given
							
							loggerV2.info(token+"EMAIL IS:::"+cclient.getMailTo());

							String path = ConfigurationManager
									.getConfigurationFromCache("invoice.details.pdfLocalPath");
//							String datetime = Helper.getPdfTimeStamp();
							String filename = cclient.getmsisdn() + "_usagehistory"
									+ Constants.PDF_EXT;
							String pdfPath = path + filename;
							
							loggerV2.info(token+"Local PDF Path is "+pdfPath);

							

							com.evampsaanga.b2b.azerfon.getcdrsbydate.GetCDRsByDateLand getCDRsByDateLand = new GetCDRsByDateLand();
							
							JSONObject requestUsage = new JSONObject(requestBody);
							requestUsage.remove("isDownloadPdf");
							requestUsage.remove("isSendMail");
							requestUsage.remove("mailTo");
							
							loggerV2.info(token+"Calling Usage Details Api with request "+requestUsage.toString());
							
							GetCDRsByDateRequestResponse cdrByDateResponse	= getCDRsByDateLand.Get(credential, contentType, requestUsage.toString());
							com.evampsaanga.b2b.azerfon.pdf.Pdf pdf = new Pdf();
							
							pdf.generateUsageHistoryPdf(cclient.getmsisdn(), cdrByDateResponse,cclient.getLang());
							
							SendEmailResponse emailResponse = new PdfToEmailLand().getResponse(
									ConfigurationManager.getConfigurationFromCache("invoice.details.usagedetails.label.emailsubject."+cclient.getLang()), cclient.getMailTo(), pdfPath, filename, loggerV2, token);

							resp.setReturnCode(emailResponse.getReturnCode());
							resp.setReturnMsg(emailResponse.getReturnMsg());
							return resp;

						} else { // if email is not given,this will return error reponse

							resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
							resp.setReturnMsg("Email not Given");
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} else if(cclient.getIsDownloadPdf().equalsIgnoreCase("true")) {

						com.evampsaanga.b2b.azerfon.getcdrsbydate.GetCDRsByDateLand getCDRsByDateLand =new GetCDRsByDateLand();
						
						JSONObject requestUsage = new JSONObject(requestBody);
						requestUsage.remove("isDownloadPdf");
						requestUsage.remove("isSendMail");
						requestUsage.remove("mailTo");
						
						GetCDRsByDateRequestResponse cdrByDateResponse	= getCDRsByDateLand.Get(credential, contentType, requestUsage.toString());
						
						 com.evampsaanga.b2b.azerfon.pdf.Pdf pdf = new Pdf();
						String pdfPath = pdf.generateUsageHistoryPdf(cclient.getmsisdn(), cdrByDateResponse,cclient.getLang());
						
						
						resp.setPdfPath(pdfPath);
						resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
						resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
						///////////////////////////////////////////////////////////////////////////
						logMessage("RESPONSE: " + Helper.ObjectToJson(resp));

						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;

					}
				} catch (Exception ex) {
					loggerV2.info(Helper.GetException(ex));
					resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
					resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		}
//		logs.updateLog(logs);
		loggerV2.info("returning Empty Pdf Path");
		return resp;
	}
	
	private void logMessage(String message) {
		loggerV2.info(modeulName + ": " + message);
	}
	
}
