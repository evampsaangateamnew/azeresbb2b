package com.evampsaanga.b2b.azerfon.usagedetailspdf;

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;

public class UsageDetailsPdfRequest extends BaseRequest {
	
	String startDate;
	String endDate;
	String accountId;
	String customerId;
	String isDownloadPdf;
	String isSendMail;
	String mailTo;
	
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getIsDownloadPdf() {
		return isDownloadPdf;
	}
	public void setIsDownloadPdf(String isDownloadPdf) {
		this.isDownloadPdf = isDownloadPdf;
	}
	public String getIsSendMail() {
		return isSendMail;
	}
	public void setIsSendMail(String isSendMail) {
		this.isSendMail = isSendMail;
	}
	public String getMailTo() {
		return mailTo;
	}
	public void setMailTo(String mailTo) {
		this.mailTo = mailTo;
	}
	

}
