package com.evampsaanga.b2b.azerfon.rateusV2;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Response Data Container For Rate Us Api
 * 
 * @author EvampSaanga
 *
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class RateUsResponseData {
	private String resultCode;
	private String msg;
	private String execTime;

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getExecTime() {
		return execTime;
	}

	public void setExecTime(String execTime) {
		this.execTime = execTime;
	}

}
