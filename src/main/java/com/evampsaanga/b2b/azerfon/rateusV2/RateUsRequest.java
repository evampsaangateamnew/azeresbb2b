package com.evampsaanga.b2b.azerfon.rateusV2;

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;

/**
 * Request Container
 * 
 * @author Aqeel Abbas
 *
 */
public class RateUsRequest extends BaseRequest {

	private String entityId;

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

}
