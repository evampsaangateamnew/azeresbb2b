package com.evampsaanga.b2b.azerfon.rateusV2;

import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;
/**
 * Response Container
 * @author Aqeel Abbas
 *
 */
public class RateUsResponse extends BaseResponse {
	RateUsResponseData data;

	public RateUsResponseData getData() {
		return data;
	}

	public void setData(RateUsResponseData data) {
		this.data = data;
	}

}
