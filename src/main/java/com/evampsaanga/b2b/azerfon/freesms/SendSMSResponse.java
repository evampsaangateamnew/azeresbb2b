package com.evampsaanga.b2b.azerfon.freesms;

import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;

public class SendSMSResponse extends BaseResponse {
	private SMSstatus status;

	/**
	 * @return the status
	 */
	public SMSstatus getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(SMSstatus status) {
		this.status = status;
	}
}
