package com.evampsaanga.b2b.azerfon.getvalues.exchangeservice;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;

import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.amqimplementationsesb.LogsType;
import com.evampsaanga.b2b.azerfon.db.DBFactory;
import com.evampsaanga.b2b.azerfon.exchangeservice.ExchangeServiceRequestClient;
import com.evampsaanga.b2b.azerfon.exchangeservice.ExchangeServiceResponse;
import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.ThirdPartyNames;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Decrypter;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.evampsaanga.b2b.developer.utils.SOAPLoggingHandler;
import com.mysql.fabric.xmlrpc.base.Array;

import antlr.collections.List;

@Path("/azerfon")
public class ExchangeServicesValuesRequestLand {
	public static final Logger logger = Logger.getLogger("azerfon-esb");

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ExchangeServicesValuesResponse Get(@Header("credentials") String credential,
			@Header("Content-Type") String contentType, @Body() String requestBody) {
		Logs logs = new Logs();
		ExchangeServicesValuesResponse resp = new ExchangeServicesValuesResponse();

		String token = "";
		String TrnsactionName = Transactions.EXCHANGE_SERVICE_GETVALUES_TRANSACTION_NAME;

		try {

			token = Helper.retrieveToken(TrnsactionName, Helper.getValueFromJSON(requestBody, "msisdn"));

			logger.info(token + "Request Landed on ExchangeServicesValuesRequestLand:" + requestBody);
			logs.setRequestDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.setTransactionName(Transactions.EXCHANGE_SERVICE_GETVALUES_TRANSACTION_NAME);
			logs.setThirdPartyName(ThirdPartyNames.EXCHANGE_SERVICE_GETVALUES);
			logs.setTableType(LogsType.ExchangeServicesValues);

			ExchangeServicesValuesRequestClient cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, ExchangeServicesValuesRequestClient.class);
				if (cclient != null) {
					if (cclient.getIsB2B() != null && cclient.getIsB2B().equals("true")) {
						logs.setTransactionName(Transactions.EXCHANGE_SERVICE_TRANSACTION_NAME);
					}
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());

					logger.info(token + "TransactionName: " + logs.getTransactionName());
				}
			} catch (Exception ex) {
				logger.info(token + Helper.GetException(ex));
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					logger.info(token + "Unable to decrypt credentials");
					SOAPLoggingHandler.logger.info(token + Helper.GetException(ex));
				}
				if (credentials == null) {
					logger.info(token + "<<<<<<<< <<<<< credentials = null >>>>> >>>>>>>>");
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						logger.info(token + "<<<<<<<< <<<<< verification = null >>>>> >>>>>>>>");
						ExchangeServicesValuesResponse res = new ExchangeServicesValuesResponse();
						res.setReturnCode(ResponseCodes.ERROR_400_CODE);
						res.setReturnMsg(verification);
						logs.setResponseCode(res.getReturnCode());
						logs.setResponseDescription(res.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return res;
					}
				} else {
					logger.info(token + "<<<<<<<< <<<<< ERROR_MSISDN_CODE >>>>> >>>>>>>>");
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
			
					ExchangeServicesValuesResponse resp1=reloadPropertiesCache(token, cclient);
					logs.setResponseCode(resp1.getReturnCode());
					logs.setResponseDescription(resp1.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					ExchangeServicesValuesResponse Response= reloadPropertiesCache(token, cclient);
					logger.info(token + "Response Returned From ESB:  :"+Helper.ObjectToJson(resp));
					
					return resp1;
				} else {
					logger.info(token + "<<<<<<<< <<<<< ERROR_401_CODE >>>>> >>>>>>>>");
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.info(token + "Error", ex);
			logger.info(token + Helper.GetException(ex));
		}
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(resp.getReturnCode());
		logs.setResponseDescription(resp.getReturnMsg());
		logs.updateLog(logs);
		return resp;
	}

	

	public static ExchangeServicesValuesResponse reloadPropertiesCache(String token, ExchangeServicesValuesRequestClient cclient) {
		
		logger.info(token+"START OF reloadPropertiesCache Metohd with ");
		HashMap<String, ArrayList<ExchangeServicesValuesResponse>> cacheExchangeServieMapping=new HashMap<>();
		ExchangeServicesValuesResponse resp = new ExchangeServicesValuesResponse();
		ArrayList<DataVoiceValues> dataArray=new ArrayList<>();
		ArrayList<DataVoiceValues> voiceArray=new ArrayList<>();
			PreparedStatement preparedStatement = null;
			ResultSet resultSet = null;
			try {
				String getAllConfiguration = "select * from exchangeservices_mappings";
				preparedStatement = DBFactory.getDbConnection().prepareStatement(getAllConfiguration);
				resultSet = preparedStatement.executeQuery();
				while (resultSet.next()){
					DataVoiceValues dataVoice=new DataVoiceValues();
					dataVoice.setVoice(resultSet.getString("voice"));
					dataVoice.setData(resultSet.getString("data"));
					dataVoice.setDataUnit("MB");
					if(cclient.getLang().equals("2"))
					{
						dataVoice.setVoiceUnit("Мин");
					}
					else if(cclient.getLang().equals("3"))
					{
						dataVoice.setVoiceUnit("Min");
					}
					else
					{
						dataVoice.setVoiceUnit("Dəq");
					}
				
				voiceArray.add(dataVoice);
				dataArray.add(dataVoice);
			      
				}
				
				String fullPrepaidOfferingIds=ConfigurationManager.getConfigurationFromCache("exchange.service.prepaid.fullofferingIds").trim();
				String [] items = fullPrepaidOfferingIds.split(",");
				java.util.List<String> listOfferingIDs = Arrays.asList(items);
				
				logger.info(token+"OfferingId In request :"+cclient.getOfferingId());
				logger.info(token+"OfferingIdS Array In request :"+listOfferingIDs);
				if(listOfferingIDs.contains(cclient.getOfferingId()))
				{
					
					logger.info(token+"Contains OfferingId :"+cclient.getOfferingId());
					resp.setIsServiceEnabled("true");
			    }
				else
				{
					logger.info(token+"NOT Contains OfferingId :"+cclient.getOfferingId());
					resp.setIsServiceEnabled("false");
				}
				resp.setDatavalues(dataArray);
				resp.setVoicevalues(voiceArray);
				resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
				resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
		
				logger.info(token+" :Response ExchabangeServieValues: "+Helper.ObjectToJson(resp));
			
			} catch (Exception e) {
				logger.info(token+Helper.GetException(e));
				resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
				resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
			} finally {
				try {
					if (resultSet != null)
						resultSet.close();
					if (preparedStatement != null)
						preparedStatement.close();
				} catch (Exception e) {
					logger.info(token+Helper.GetException(e));
					resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
					resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
				}
			}
			logger.info(token+"END OF reloadPropertiesCache Metohd with ");
		
			return resp;
		
	}
	public static void main(String[] args) {
		int a=-10;
		int b=(-1)*(a);
		System.out.println(b);
		
	}
}
