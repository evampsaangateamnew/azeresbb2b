package com.evampsaanga.b2b.azerfon.getvalues.exchangeservice;

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;

public class ExchangeServicesValuesRequestClient extends BaseRequest {
	private String offeringId="";
	private String offeringName="";

	public String getOfferingName() {
		return offeringName;
	}

	public void setOfferingName(String offeringName) {
		this.offeringName = offeringName;
	}

	public String getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(String offeringId) {
		this.offeringId = offeringId;
	}
	

	

}
