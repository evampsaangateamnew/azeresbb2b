package com.evampsaanga.b2b.azerfon.getvalues.exchangeservicecount;

import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;

public class ExchangeServiceCountResponse extends BaseResponse {
	private String count;
	

	public String getCount() {
		return count;
	}

	public void setCount(String count) {
		this.count = count;
	}

	

}
