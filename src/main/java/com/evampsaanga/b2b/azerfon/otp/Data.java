
package com.evampsaanga.b2b.azerfon.otp;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "smsSignUpEn",
    "smsSignUpRu",
    "smsSignUpAz",
    "smsForgotPasswordEn",
    "smsForgotPasswordRu",
    "smsForgotPasswordAz",
    "smsUsageEn",
    "smsUsageRu",
    "smsUsageAz",
    "smsMoneyTransferEn",
    "smsMoneyTransferRU",
    "smsMoneyTransferAZ",
    "isPromoEnabled",
    "promoMsgEN",
    "promoMsgRU",
    "promoMsgAZ",
    "offeringIds",
    "validPinTime",
    "maxAttemptsAllowed"
})
public class Data {

    @JsonProperty("smsSignUpEn")
    private String smsSignUpEn;
    @JsonProperty("smsSignUpRu")
    private String smsSignUpRu;
    @JsonProperty("smsSignUpAz")
    private String smsSignUpAz;
    @JsonProperty("smsForgotPasswordEn")
    private String smsForgotPasswordEn;
    @JsonProperty("smsForgotPasswordRu")
    private String smsForgotPasswordRu;
    @JsonProperty("smsForgotPasswordAz")
    private String smsForgotPasswordAz;
    @JsonProperty("smsUsageEn")
    private String smsUsageEn;
    @JsonProperty("smsUsageRu")
    private String smsUsageRu;
    @JsonProperty("smsUsageAz")
    private String smsUsageAz;
    @JsonProperty("smsMoneyTransferEn")
    private String smsMoneyTransferEn;
    @JsonProperty("smsMoneyTransferRU")
    private String smsMoneyTransferRU;
    @JsonProperty("smsMoneyTransferAZ")
    private String smsMoneyTransferAZ;
    @JsonProperty("isPromoEnabled")
    private String isPromoEnabled;
    @JsonProperty("promoMsgEN")
    private String promoMsgEN;
    @JsonProperty("promoMsgRU")
    private String promoMsgRU;
    @JsonProperty("promoMsgAZ")
    private String promoMsgAZ;
    @JsonProperty("offeringIds")
    private String offeringIds;
    @JsonProperty("validPinTime")
    private Integer validPinTime;
    @JsonProperty("maxAttemptsAllowed")
    private String maxAttemptsAllowed;

    @JsonProperty("smsSignUpEn")
    public String getSmsSignUpEn() {
        return smsSignUpEn;
    }

    @JsonProperty("smsSignUpEn")
    public void setSmsSignUpEn(String smsSignUpEn) {
        this.smsSignUpEn = smsSignUpEn;
    }

    @JsonProperty("smsSignUpRu")
    public String getSmsSignUpRu() {
        return smsSignUpRu;
    }

    @JsonProperty("smsSignUpRu")
    public void setSmsSignUpRu(String smsSignUpRu) {
        this.smsSignUpRu = smsSignUpRu;
    }

    @JsonProperty("smsSignUpAz")
    public String getSmsSignUpAz() {
        return smsSignUpAz;
    }

    @JsonProperty("smsSignUpAz")
    public void setSmsSignUpAz(String smsSignUpAz) {
        this.smsSignUpAz = smsSignUpAz;
    }

    @JsonProperty("smsForgotPasswordEn")
    public String getSmsForgotPasswordEn() {
        return smsForgotPasswordEn;
    }

    @JsonProperty("smsForgotPasswordEn")
    public void setSmsForgotPasswordEn(String smsForgotPasswordEn) {
        this.smsForgotPasswordEn = smsForgotPasswordEn;
    }

    @JsonProperty("smsForgotPasswordRu")
    public String getSmsForgotPasswordRu() {
        return smsForgotPasswordRu;
    }

    @JsonProperty("smsForgotPasswordRu")
    public void setSmsForgotPasswordRu(String smsForgotPasswordRu) {
        this.smsForgotPasswordRu = smsForgotPasswordRu;
    }

    @JsonProperty("smsForgotPasswordAz")
    public String getSmsForgotPasswordAz() {
        return smsForgotPasswordAz;
    }

    @JsonProperty("smsForgotPasswordAz")
    public void setSmsForgotPasswordAz(String smsForgotPasswordAz) {
        this.smsForgotPasswordAz = smsForgotPasswordAz;
    }

    @JsonProperty("smsUsageEn")
    public String getSmsUsageEn() {
        return smsUsageEn;
    }

    @JsonProperty("smsUsageEn")
    public void setSmsUsageEn(String smsUsageEn) {
        this.smsUsageEn = smsUsageEn;
    }

    @JsonProperty("smsUsageRu")
    public String getSmsUsageRu() {
        return smsUsageRu;
    }

    @JsonProperty("smsUsageRu")
    public void setSmsUsageRu(String smsUsageRu) {
        this.smsUsageRu = smsUsageRu;
    }

    @JsonProperty("smsUsageAz")
    public String getSmsUsageAz() {
        return smsUsageAz;
    }

    @JsonProperty("smsUsageAz")
    public void setSmsUsageAz(String smsUsageAz) {
        this.smsUsageAz = smsUsageAz;
    }

    @JsonProperty("smsMoneyTransferEn")
    public String getSmsMoneyTransferEn() {
        return smsMoneyTransferEn;
    }

    @JsonProperty("smsMoneyTransferEn")
    public void setSmsMoneyTransferEn(String smsMoneyTransferEn) {
        this.smsMoneyTransferEn = smsMoneyTransferEn;
    }

    @JsonProperty("smsMoneyTransferRU")
    public String getSmsMoneyTransferRU() {
        return smsMoneyTransferRU;
    }

    @JsonProperty("smsMoneyTransferRU")
    public void setSmsMoneyTransferRU(String smsMoneyTransferRU) {
        this.smsMoneyTransferRU = smsMoneyTransferRU;
    }

    @JsonProperty("smsMoneyTransferAZ")
    public String getSmsMoneyTransferAZ() {
        return smsMoneyTransferAZ;
    }

    @JsonProperty("smsMoneyTransferAZ")
    public void setSmsMoneyTransferAZ(String smsMoneyTransferAZ) {
        this.smsMoneyTransferAZ = smsMoneyTransferAZ;
    }

    @JsonProperty("isPromoEnabled")
    public String getIsPromoEnabled() {
        return isPromoEnabled;
    }

    @JsonProperty("isPromoEnabled")
    public void setIsPromoEnabled(String isPromoEnabled) {
        this.isPromoEnabled = isPromoEnabled;
    }

    @JsonProperty("promoMsgEN")
    public String getPromoMsgEN() {
        return promoMsgEN;
    }

    @JsonProperty("promoMsgEN")
    public void setPromoMsgEN(String promoMsgEN) {
        this.promoMsgEN = promoMsgEN;
    }

    @JsonProperty("promoMsgRU")
    public String getPromoMsgRU() {
        return promoMsgRU;
    }

    @JsonProperty("promoMsgRU")
    public void setPromoMsgRU(String promoMsgRU) {
        this.promoMsgRU = promoMsgRU;
    }

    @JsonProperty("promoMsgAZ")
    public String getPromoMsgAZ() {
        return promoMsgAZ;
    }

    @JsonProperty("promoMsgAZ")
    public void setPromoMsgAZ(String promoMsgAZ) {
        this.promoMsgAZ = promoMsgAZ;
    }

    @JsonProperty("offeringIds")
    public String getOfferingIds() {
        return offeringIds;
    }

    @JsonProperty("offeringIds")
    public void setOfferingIds(String offeringIds) {
        this.offeringIds = offeringIds;
    }

    @JsonProperty("validPinTime")
    public Integer getValidPinTime() {
        return validPinTime;
    }

    @JsonProperty("validPinTime")
    public void setValidPinTime(Integer validPinTime) {
        this.validPinTime = validPinTime;
    }

    @JsonProperty("maxAttemptsAllowed")
    public String getMaxAttemptsAllowed() {
        return maxAttemptsAllowed;
    }

    @JsonProperty("maxAttemptsAllowed")
    public void setMaxAttemptsAllowed(String maxAttemptsAllowed) {
        this.maxAttemptsAllowed = maxAttemptsAllowed;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("smsSignUpEn", smsSignUpEn).append("smsSignUpRu", smsSignUpRu).append("smsSignUpAz", smsSignUpAz).append("smsForgotPasswordEn", smsForgotPasswordEn).append("smsForgotPasswordRu", smsForgotPasswordRu).append("smsForgotPasswordAz", smsForgotPasswordAz).append("smsUsageEn", smsUsageEn).append("smsUsageRu", smsUsageRu).append("smsUsageAz", smsUsageAz).append("smsMoneyTransferEn", smsMoneyTransferEn).append("smsMoneyTransferRU", smsMoneyTransferRU).append("smsMoneyTransferAZ", smsMoneyTransferAZ).append("isPromoEnabled", isPromoEnabled).append("promoMsgEN", promoMsgEN).append("promoMsgRU", promoMsgRU).append("promoMsgAZ", promoMsgAZ).append("offeringIds", offeringIds).append("validPinTime", validPinTime).append("maxAttemptsAllowed", maxAttemptsAllowed).toString();
    }

}
