package com.evampsaanga.b2b.azerfon.otp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;

import com.evampsaanga.b2b.authorization.AuthorizationAndAuthentication;
import com.evampsaanga.b2b.azerfon.db.DBFactory;
import com.evampsaanga.b2b.azerfon.getsubscriber.CoreServicesCategory;
import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;
import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.developer.utils.Helper;

@Path("/bakcell")
public class OtpRequestLand {

	public static final Logger logger = Logger.getLogger("azerfon-esb");
	
	@POST
	@Path("/setotp")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BaseResponse setOtp(@Header("credentials") String credential,@Header("Content-Type") String contentType, @Body() String requestBody)
	{
		
		
		
		BaseResponse baseResponse = new BaseResponse();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try 
		{
			logger.info("OTP:setotp request landed "+requestBody);
			JSONObject jsonObject = new JSONObject(requestBody);
			///check count
			
			Connection con = DBFactory.getMagentoDBConnection();
			String query = "";
			
		
			query = "SELECT login_id,created_at,count FROM login_attempts WHERE msisdn='"+jsonObject.getString("msisdn")+"' AND created_at > date_sub(now(), interval 5 minute) ORDER BY login_id ASC LIMIT 1";
			pstmt = con.prepareStatement(query); // create a statement
			
			logger.info("OTP:<<<<<<<< Query >>>>>>>>" + pstmt.toString());
			
			rs = pstmt.executeQuery();
			// extract data from the ResultSet
			if(rs.next())
			{
				logger.info("OTP:Record found in DB");
				int count = Integer.parseInt(rs.getString("count"));
				int loginId = Integer.parseInt(rs.getString("login_id"));
				if(count<=Integer.parseInt("2000"))
				{
					//insert
					logger.info("OTP:Otp value is less than 2000");
					logger.info("OTP:Updating record in DB");
					int insertCount = 0;
					String updateQuery = "UPDATE login_attempts set count=count+1 where login_id="+loginId+"";
					logger.info("OTP:update query "+updateQuery);
					pstmt = con.prepareStatement(updateQuery); 
					int res = pstmt.executeUpdate();
					if(res > 0)
					{
						logger.info("OTP:Count is updated successfully");
						//count updated
						baseResponse.setReturnCode("00");
						baseResponse.setReturnCode("Success");
						return baseResponse;
						
					}
					else
					{
						logger.info("OTP:Count updation failed");
						//count updated
						baseResponse.setReturnCode("102");
						baseResponse.setReturnCode("Count updated failed");
						return baseResponse;
					}
				}
				else
				{
					logger.info("OTP:otp limit is exceeded from 2000");
				
					baseResponse.setReturnCode("100");
					baseResponse.setReturnCode("Count is greater than 5");
					return baseResponse;
				}
				
			}
			else
			{
				//insert
				logger.info("OTP:Insert otp into DB");
				int insertCount = 0;
				String insertQuery = "INSERT INTO login_attempts (`msisdn`,`count`, `created_at`) VALUES ('"+jsonObject.getString("msisdn")+"', '"+insertCount+"',now())";
				pstmt = con.prepareStatement(insertQuery);
				logger.info("OTP: insert "+pstmt.toString());
				insertCount = pstmt.executeUpdate();
				if(insertCount > 0)
				{
					//count added
					logger.info("OTP:Record inserted successfully");
					baseResponse.setReturnCode("00");
					baseResponse.setReturnCode("Record inserted successfully");
					
				}
				else
				{
					logger.info("OTP:Insertion failed");
					baseResponse.setReturnCode("101");
					baseResponse.setReturnCode("Insertion failed");
					
				}
			}
			rs.close();
			return baseResponse;
		} catch (Exception e) {
			logger.error("EXCEPTION:", e);
			baseResponse.setReturnCode("500");
			baseResponse.setReturnCode(e.getMessage());
			return baseResponse;
		}
		finally {
			if(pstmt != null)
				try {
					pstmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			if(rs != null)
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		
	}
	
	
	
}
