
package com.evampsaanga.b2b.azerfon.otp;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "data",
    "resultCode",
    "msg",
    "execTime"
})
public class MessageTemplateResponse {

    @JsonProperty("data")
    private Data data;
    @JsonProperty("resultCode")
    private String resultCode;
    @JsonProperty("msg")
    private String msg;
    @JsonProperty("execTime")
    private Float execTime;

    @JsonProperty("data")
    public Data getData() {
        return data;
    }

    @JsonProperty("data")
    public void setData(Data data) {
        this.data = data;
    }

    @JsonProperty("resultCode")
    public String getResultCode() {
        return resultCode;
    }

    @JsonProperty("resultCode")
    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    @JsonProperty("msg")
    public String getMsg() {
        return msg;
    }

    @JsonProperty("msg")
    public void setMsg(String msg) {
        this.msg = msg;
    }

    @JsonProperty("execTime")
    public Float getExecTime() {
        return execTime;
    }

    @JsonProperty("execTime")
    public void setExecTime(Float execTime) {
        this.execTime = execTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("data", data).append("resultCode", resultCode).append("msg", msg).append("execTime", execTime).toString();
    }

}
