package com.evampsaanga.b2b.azerfon.signupverifyotp;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "SignUpRequest")
public class SignUpRequest extends BaseRequest {
	@XmlElement(name = "cause", required = true)
	private String cause = "";
	
	@XmlElement(name = "pin", required = true)
	private String pin = "";

	/**
	 * @return the cause
	 */
	public String getCause() {
		return cause;
	}

	/**
	 * @param cause
	 *            the cause to set
	 */
	public void setCause(String cause) {
		this.cause = cause;
	}



	/**
	 * @return the pin
	 */
	public String getPin() {
		return pin;
	}

	/**
	 * @param pin
	 *            the pin to set
	 */
	public void setPin(String pin) {
		this.pin = pin;
	}
}
