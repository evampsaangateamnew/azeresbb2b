package com.evampsaanga.b2b.azerfon.external.services;

import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;

public class LoginExternalResponse extends BaseResponse{

    private String entityId;

    public String getEntityId() {
	return entityId;
    }

    public void setEntityId(String entityId) {
	this.entityId = entityId;
    }
    
}
