package com.evampsaanga.b2b.azerfon.external.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.evampsaanga.azerfon.appserver.refreshappservercache.BuildCacheRequestLand;
import com.evampsaanga.azerfon.appserver.refreshappservercache.CustomerModelCache;
import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.amqimplementationsesb.LogsType;
import com.evampsaanga.b2b.azerfon.getcustomerrequest.GetCustomerDataLand;
import com.evampsaanga.b2b.azerfon.getcustomerrequest.GetCustomerRequestClient;
import com.evampsaanga.b2b.azerfon.getcustomerrequest.GetCustomerRequestResponse;
import com.evampsaanga.b2b.azerfon.login.LoginRequest;
import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.ThirdPartyNames;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Decrypter;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.saanga.magento.apiclient.RestClient;

@Path("/login")
public class LoginECareRequestLand {

    public static final Logger logger = Logger.getLogger("azerfon-esb");
    
    @POST
	@Path("/ecare")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LoginExternalResponse GetLogInECare(@Header("credentials") String credential, @Body() String requestBody) {
		logger.info("Login  Customer Request From Ecare" + requestBody);
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.LOGIN_EXTERNAL_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.LOGIN);
		logs.setTableType(LogsType.Login);
		LoginRequest cclient = null;
		LoginExternalResponse resp = new LoginExternalResponse();
		String token = "";
		String TrnsactionName = Transactions.BASE_MAGENTO_TRANSACTION_NAME + " " + Transactions.LOGIN_EXTERNAL_TRANSACTION_NAME;

		try {

			token = Helper.retrieveToken(TrnsactionName, Helper.getValueFromJSON(requestBody, "msisdn"));

			logger.info(token + "-Request Data-" + requestBody);
			cclient = Helper.JsonToObject(requestBody, LoginRequest.class);
			logs.setIp(cclient.getiP());
			logs.setChannel(cclient.getChannel());
			logs.setMsisdn(cclient.getmsisdn());
			logs.setLang(cclient.getLang());
		} catch (Exception ex) {
			logger.info(token+Helper.GetException(ex));
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_400);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		{
			String credentials = null;
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
			} catch (Exception ex) {
				logger.info(token+Helper.GetException(ex));
			}
			if (credentials == null) {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
				String verification = Helper.validateRequest(cclient);
				if (!verification.equals("")) {
					resp.setReturnCode(ResponseCodes.ERROR_400);
					resp.setReturnMsg(verification);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				try {
					GetCustomerDataLand getCustomer = new GetCustomerDataLand();
					GetCustomerRequestClient getCustomerRequestClient = new GetCustomerRequestClient();
					getCustomerRequestClient.setChannel(cclient.getChannel());
					getCustomerRequestClient.setiP(cclient.getiP());
					getCustomerRequestClient.setLang(cclient.getLang());
					getCustomerRequestClient.setMsisdn(cclient.getmsisdn());
					ObjectMapper mapper = new ObjectMapper();
					String jsonInString = mapper.writeValueAsString(getCustomerRequestClient);
					GetCustomerRequestResponse responseGetCustomer = getCustomer.Get(Constants.CREDENTIALSUNCODED, "",
							jsonInString);
					if (responseGetCustomer.getReturnCode().equals(ResponseCodes.SUCESS_CODE_200)) {
						if (responseGetCustomer.getCustomerData().getStatus().equals("Active")
								|| responseGetCustomer.getCustomerData().getStatus().equals("Block 1 Way")
								|| responseGetCustomer.getCustomerData().getStatus().equals("Block 2 Way")) {
						} else {
							resp.setReturnCode("551");
							resp.setReturnMsg("The number is in " + responseGetCustomer.getCustomerData().getStatus()
									+ " state, you are not allowed to login");
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} else {
						resp.setReturnCode(responseGetCustomer.getReturnCode());
						resp.setReturnMsg(responseGetCustomer.getReturnMsg());
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
					if (responseGetCustomer.getReturnCode().equals(ResponseCodes.SUCESS_CODE_200)) {
						JSONObject jsonObject = new JSONObject();
						jsonObject.put("msisdn", cclient.getmsisdn());
						jsonObject.put("password", cclient.getPassword());
						jsonObject.put("accountId", responseGetCustomer.getCustomerData().getAccountId());
						jsonObject.put("customerId", responseGetCustomer.getCustomerData().getCustomerId());
						logger.info(token+"LOGIN_ECARE:accountId" + responseGetCustomer.getCustomerData().getAccountId());
						logger.info(token+"LOGIN_ECARE:Request to magento for log in" + jsonObject);
						//commented on 

						try {
							boolean checkDb = false;
							if (!BuildCacheRequestLand.customerCache.containsKey(cclient.getmsisdn()) ) 
							{
								checkDb =Helper.checkMsisdnExistInDB(cclient.getmsisdn());
							}
							else
							{
								checkDb = true;
							}
							
							
							if (checkDb ) 
							{


								CustomerModelCache customerModelCache = BuildCacheRequestLand.customerCache
										.get(cclient.getmsisdn());
								
								
								
								int result = Long.compare(Long.valueOf(BuildCacheRequestLand.customerCache.get(cclient.getmsisdn()).getCustomerId()), Long.valueOf(responseGetCustomer.getCustomerData().getCustomerId()));
								if(result==0)
								{
									logger.info(token+"LOGIN_ECARE:"+cclient.getmsisdn()+"Customer id is matched");
								//------------- checkLoginAttempts checks the login attempts from custom php API ------------//
									if(checkLoginAttempts(cclient.getmsisdn(),token))
									{
										logger.info(token+"LOGIN_ECARE:"+cclient.getmsisdn()+" check login attempt in if check true ");
										if (Helper.checkSHApass(cclient.getPassword(),cclient.getmsisdn()).equals(customerModelCache.getPassword_hash())) 
										{
											
											
											logger.info(token+"LOGIN_ECARE:"+cclient.getmsisdn()+" checkSSHPASS compare passwords ");
											logger.info(token+"LOGIN_ECARE:"+cclient.getmsisdn()+"Email coming" + customerModelCache.getEmail());
		
												
											logger.info(token+"LOGIN_ECARE:"+cclient.getmsisdn()+"EmailFrom Cache :" + customerModelCache.getEmail());
		
											
											resetLoginAttempt(cclient.getmsisdn(),token);
											
											//adding
											logger.info("LOGIN_ECARE:Email coming" + customerModelCache.getEmail());
									        resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
									        resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
									        resp.setEntityId(customerModelCache.getEntity_id());
									        logger.info("LOGIN_ECARE:Data Sent To App " + resp.toString());
									        logs.setResponseCode(resp.getReturnCode());
									        logs.setResponseDescription(resp.getReturnMsg());
									        logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
									        logs.updateLog(logs);
									        return resp;
											
											
											
										} else {
											logger.info("LOGIN_ECARE:"+cclient.getmsisdn()+" "+ResponseCodes.PASSWORD_MATCH_FAILED_DESCRIPTION);
											resp.setReturnCode(ResponseCodes.PASSWORD_MATCH_FAILED_CODE);
											resp.setReturnMsg(ResponseCodes.PASSWORD_MATCH_FAILED_DESCRIPTION);
											logs.setResponseCode(resp.getReturnCode());
											logs.setResponseDescription(resp.getReturnMsg());
											logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
											logs.updateLog(logs);
											return resp;
										}
									}
									else
									{
										logger.info("LOGIN_ECARE:"+cclient.getmsisdn()+" login attempt failed");
										resp.setReturnCode(ResponseCodes.LOGIN_ATTEMPT_FAILED);
										resp.setReturnMsg(ResponseCodes.LOGIN_ATTEMPT_FAILED_DESCRIPTION);
										logs.setResponseCode(resp.getReturnCode());
										logs.setResponseDescription(resp.getReturnMsg());
										logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
										logs.updateLog(logs);
										return resp;
									}
								}
								else
								{
									//user not exisit
									logger.info("LOGIN_ECARE:"+cclient.getmsisdn()+" Deleting Customer due to ownership change");
									
									deleteCustomer(cclient.getmsisdn(),token);
									BuildCacheRequestLand.customerCache.remove(cclient.getmsisdn());
									
									resp.setReturnCode(ResponseCodes.USER_DOESNOT_EXIST_CODE);
									resp.setReturnMsg(ResponseCodes.USER_DOESNOT_EXIST_DESC);
									logs.setResponseCode(resp.getReturnCode());
									logs.setResponseDescription(resp.getReturnMsg());
									logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
									logs.updateLog(logs);
									return resp;
								}
									
							}

							else {
								logger.info("LOGIN_ECARE:"+cclient.getmsisdn()+"-"+ResponseCodes.USER_NOT_FOUND_DESCRIPTION);
								resp.setReturnCode(ResponseCodes.USER_NOT_FOUND_CODE);
								resp.setReturnMsg(ResponseCodes.USER_NOT_FOUND_DESCRIPTION);
								logs.setResponseCode(resp.getReturnCode());
								logs.setResponseDescription(resp.getReturnMsg());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
								return resp;
							}
							
//								resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
//								resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
//								resp.setEntityId(data.getData().getEntityId());
//								logger.info("Data Sent To App " + resp.toString());
//								logs.setResponseCode(resp.getReturnCode());
//								logs.setResponseDescription(resp.getReturnMsg());
//								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
//								logs.updateLog(logs);
//								return resp;
//							} else {
//								resp.setReturnCode(data.getResultCode());
//								resp.setReturnMsg(data.getMsg());
//								logs.setResponseCode(resp.getReturnCode());
//								logs.setResponseDescription(resp.getReturnMsg());
//								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
//								logs.updateLog(logs);
//								return resp;
//							}
						} catch (Exception ex) {
							logger.error(Helper.GetException(ex));
							resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
							resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} else {
						resp.setReturnCode(responseGetCustomer.getReturnCode());
						resp.setReturnMsg(responseGetCustomer.getReturnMsg());
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				} catch (JSONException ex) {
					logger.error(Helper.GetException(ex));
					resp.setReturnCode(ResponseCodes.MAGENTO_SERVER_ERROR_CODE);
					resp.setReturnMsg(ResponseCodes.MAGENTO_SERVER_ERROR_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				} catch (Exception ex) {
					logger.error(Helper.GetException(ex));
					resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
					resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.updateLog(logs);
				return resp;
			}
		}
	}
    
    
    private boolean checkLoginAttempts(String getmsisdn,String token) throws Exception {
		//check login attempts
		
		logger.info(token+"LOGIN_ECARE:"+getmsisdn+" check login attempt");
		
		JSONObject loginAttemptRequest = new JSONObject();
		loginAttemptRequest.put("msisdn", getmsisdn);
		loginAttemptRequest.put("type", "validateLoginAttempts");
		logger.info(token+"LOGIN_ECARE:"+getmsisdn+" check login attempt request "+loginAttemptRequest.toString());
		String loginAttemp = RestClient.SendCallToMagento(token,
				ConfigurationManager.getConfigurationFromCache("magento.app.login.validateLoginAttempts"),
				loginAttemptRequest.toString());
		logger.info(token+"LOGIN_ECARE:"+getmsisdn+" check login attempt response"+loginAttemp);
		JSONObject loginAttemmptResponse = new JSONObject(loginAttemp);
		if(loginAttemmptResponse.getString("resultCode").equalsIgnoreCase("200"))
		{
			logger.info(token+"LOGIN_ECARE:"+getmsisdn+" returning true"+loginAttemmptResponse.getString("resultCode"));
			return true;
		}
		else
		{
			logger.info(token+"LOGIN_ECARE:"+getmsisdn+" returning false");
			return false;
		}
	}
    
    private boolean resetLoginAttempt(String getmsisdn,String token) throws Exception {
		// TODO Auto-generated method stub
		logger.info(token+"LOGIN_ECARE:"+getmsisdn+" reset login attempt");
		
		JSONObject loginAttemptRequest = new JSONObject();
		loginAttemptRequest.put("msisdn", getmsisdn);
		loginAttemptRequest.put("type", "resetLoginAttempts");
		logger.info(token+"LOGIN_ECARE:"+getmsisdn+" check login attempt request "+loginAttemptRequest.toString());
		String loginAttemp = RestClient.SendCallToMagento(token,
				ConfigurationManager.getConfigurationFromCache("magento.app.login.validateLoginAttempts"),
				loginAttemptRequest.toString());
		logger.info(token+"LOGIN_ECARE:"+getmsisdn+" check login  reset attempt response"+loginAttemp);
		JSONObject loginAttemmptResponse = new JSONObject(loginAttemp);
		if(loginAttemmptResponse.getString("resultCode").equalsIgnoreCase("200"))
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}
    
    private boolean deleteCustomer(String getmsisdn,String token) throws Exception {
		// TODO Auto-generated method stub
		try
		{
			logger.info(token+"SIGNUPSENDOTP-"+getmsisdn+":Deleting customer "+getmsisdn);
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("msisdn", getmsisdn);
			logger.info(token+"SIGNUPSENDOTP-"+getmsisdn+":Deleting customer request "+jsonObject.toString());
			String response = RestClient.SendCallToMagento(token,
					ConfigurationManager.getConfigurationFromCache("magento.app.signupflow.deletecustomer"), //delete
					jsonObject.toString());
			logger.info(token+"SIGNUPSENDOTP-"+getmsisdn+":Deleting customer response "+response);
			JSONObject responseJSON = new JSONObject(response);
			if(responseJSON.getString("resultCode").equals(Constants.MAGENTO_SUCESS_CODE_DELETE_API))
			{
				return true;
			}
			else
				return false;
		}
		catch (Exception e) 
		{
			// TODO: handle exception
			logger.info(token+Helper.GetException(e));
			return false;
		}
		
	}
    
}
