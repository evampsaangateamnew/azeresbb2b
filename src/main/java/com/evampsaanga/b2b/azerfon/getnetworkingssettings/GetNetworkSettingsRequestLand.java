package com.evampsaanga.b2b.azerfon.getnetworkingssettings;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.amqimplementationsesb.LogsType;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.ThirdPartyNames;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Decrypter;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.evampsaanga.b2b.developer.utils.SOAPLoggingHandler;
import com.huawei.crm.query.GetNetworkSettingDataIn;
import com.huawei.crm.query.GetNetworkSettingDataRequest;
import com.huawei.crm.query.GetNetworkSettingDataResponse;
import com.ngbss.evampsaanga.services.ThirdPartyCall;
import com.ngbss.evampsaanga.services.ThirdPartyRequestHeader;


@Path("/bakcell/")
public class GetNetworkSettingsRequestLand {
	public static final Logger logger = Logger.getLogger("azerfon-esb");

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetNetworkSettingsResponse Get(@Header("credentials") String credential,
			@Header("Content-Type") String contentType, @Body() String requestBody) {
		GetNetworkSettingsResponse resp = new GetNetworkSettingsResponse();
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.CHANGE_NETWORK_SETTINGS);
		logs.setThirdPartyName(ThirdPartyNames.BAKCELL_ORDER_HANDLER);
		logs.setTableType(LogsType.ChangeNetworkSettings);
		try {
			logger.info("Request Landed on GetNetworkSettingsRequestLand:" + requestBody);
			GetNetworkSettingsRequest cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, GetNetworkSettingsRequest.class);
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					/// Call ChangeLanguage here
					GetNetworkSettingDataResponse response = getResponse(cclient);
					logger.info("response GET Network Settings "+ Helper.ObjectToJson(response));
					logger.info("responseCODE GET Network Settings "+ Helper.ObjectToJson(response.getResponseHeader().getRetCode()));
					if (response.getResponseHeader().getRetCode().equals("0")) {
						resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
						resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
						resp.setGetNetworkSettingDataBody(response.getGetNetworkSettingDataBody());
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);	return resp;
					} else {
						resp.setReturnCode(response.getResponseHeader().getRetCode());
						resp.setReturnMsg(response.getResponseHeader().getRetMsg());
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);			return resp;
					}
				} else {
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(resp.getReturnCode());
		logs.setResponseDescription(resp.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	private GetNetworkSettingDataResponse getResponse(GetNetworkSettingsRequest cclient) {
		
		
		GetNetworkSettingDataRequest getNetworkSettingDataRequestMsgReq = new GetNetworkSettingDataRequest();
		GetNetworkSettingDataIn getNetworkSettingDataIn = new GetNetworkSettingDataIn();
		getNetworkSettingDataIn.setServiceNumber(cclient.getmsisdn());
		
		
//		getNetworkSettingDataRequestMsgReq.setRequestHeader(com.evampsaanga.services.CRMServices.getReqHeaderForGETNetworkSetting());
		getNetworkSettingDataRequestMsgReq.setRequestHeader(ThirdPartyRequestHeader.getReqHeaderForGETNetworkSetting());
		getNetworkSettingDataRequestMsgReq.setGetNetworkSettingDataBody(getNetworkSettingDataIn );
//		return CRMServices.getInstance().getNetworkSettingData(getNetworkSettingDataRequestMsgReq );
		return ThirdPartyCall.getNetworkSettingData(getNetworkSettingDataRequestMsgReq );
		
		
		
	}
}