package com.evampsaanga.b2b.azerfon.getnetworkingssettings;

import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;

public class GetNetworkSettingsResponse extends BaseResponse {
	com.huawei.crm.query.GetNetworkSettingDataList getNetworkSettingDataBody=null;

	public com.huawei.crm.query.GetNetworkSettingDataList getGetNetworkSettingDataBody() {
		return getNetworkSettingDataBody;
	}

	public void setGetNetworkSettingDataBody(com.huawei.crm.query.GetNetworkSettingDataList getNetworkSettingDataBody) {
		this.getNetworkSettingDataBody = getNetworkSettingDataBody;
	}
	

}
