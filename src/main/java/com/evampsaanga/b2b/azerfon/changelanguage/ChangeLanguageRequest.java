package com.evampsaanga.b2b.azerfon.changelanguage;

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;

public class ChangeLanguageRequest extends BaseRequest {
	private String language = "";

	/**
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * @param language
	 *            the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}
}
