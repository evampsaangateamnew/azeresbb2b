/**
 * 
 */
package com.evampsaanga.b2b.azerfon.notifications;

/**
 * @author Abdul Saboor
 *
 */
public class Notification {
	private String datetime;
	private String message;
	private String icon;
	private String actionType;
	private String actionID;
	private String btnTxt;
	private String timeElapsed = "";

	/**
	 * @return the timeElapsed
	 */
	public String getTimeElapsed() {
		return timeElapsed;
	}

	/**
	 * @param timeElapsed
	 *            the timeElapsed to set
	 */
	public void setTimeElapsed(String timeElapsed) {
		this.timeElapsed = timeElapsed;
	}

	public String getDatetime() {
		return datetime;
	}

	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public String getActionID() {
		return actionID;
	}

	public void setActionID(String actionID) {
		this.actionID = actionID;
	}

	public String getBtnTxt() {
		return btnTxt;
	}

	public void setBtnTxt(String btnTxt) {
		this.btnTxt = btnTxt;
	}
}
