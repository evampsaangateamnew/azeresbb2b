package com.evampsaanga.b2b.azerfon.notifications;

import java.util.ArrayList;
import java.util.List;

import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;

public class NotificationResponseClient extends BaseResponse {
	List<Notification> notificationsList = new ArrayList<>();

	public List<Notification> getNotificationsList() {
		return notificationsList;
	}

	public void setNotificationsList(List<Notification> notificationsList) {
		this.notificationsList = notificationsList;
	}
}
