package com.evampsaanga.b2b.azerfon.loanpaymenthistory;

public class LoanPaymentHistory {
	private String loanID = "";
	private String dateTime = "";
	private String amount = "";

	public LoanPaymentHistory(String loanID, String dateTime, String amount) {
		super();
		this.loanID = loanID;
		this.dateTime = dateTime;
		this.amount = amount;
	}

	/**
	 * @return the loanID
	 */
	public String getLoanID() {
		return loanID;
	}

	/**
	 * @param loanID
	 *            the loanID to set
	 */
	public void setLoanID(String loanID) {
		this.loanID = loanID;
	}

	/**
	 * @return the dateTime
	 */
	public String getDateTime() {
		return dateTime;
	}

	/**
	 * @param dateTime
	 *            the dateTime to set
	 */
	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	/**
	 * @return the amount
	 */
	public String getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            the amount to set
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}
}
