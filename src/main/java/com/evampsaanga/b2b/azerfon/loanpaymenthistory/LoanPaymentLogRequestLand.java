package com.evampsaanga.b2b.azerfon.loanpaymenthistory;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.amqimplementationsesb.LogsType;
import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.ThirdPartyNames;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Decrypter;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.evampsaanga.b2b.developer.utils.SOAPLoggingHandler;
import com.huawei.bme.cbsinterface.arservices.QueryLoanLogRequest;
import com.huawei.bme.cbsinterface.arservices.QueryLoanLogRequestMsg;
import com.huawei.bme.cbsinterface.arservices.QueryLoanLogResult;
import com.huawei.bme.cbsinterface.arservices.QueryLoanLogResult.RepaymentLogDetail;
import com.huawei.bme.cbsinterface.arservices.QueryLoanLogResultMsg;
import com.huawei.bme.cbsinterface.cbscommon.OperatorInfo;
import com.huawei.bme.cbsinterface.cbscommon.OwnershipInfo;
import com.huawei.bme.cbsinterface.cbscommon.SecurityInfo;
import com.huawei.cbs.ar.wsservice.arcommon.SubAccessCode;

@Path("/bakcell")
public class LoanPaymentLogRequestLand {
	public static final Logger logger = Logger.getLogger("azerfon-esb");

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LoanLogResponse Get(@Header("credentials") String credential, @Header("Content-Type") String contentType,
			@Body() String requestBody) {
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.LOAN_PAYMENT_HISTORY_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.LOAN_PAYMENT_HISTORY);
		logs.setTableType(LogsType.LoanPaymentHistory);
		try {
			logger.info("Request Landed on Loan payment History Land:" + requestBody);
			LoanLogRequest cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, LoanLogRequest.class);
				if (cclient != null) {
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
					logs.setLang(cclient.getLang());}
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				LoanLogResponse resp = new LoanLogResponse();
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(ResponseCodes.ERROR_400_CODE);
				logs.setResponseDescription(ResponseCodes.ERROR_400);
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					LoanLogResponse resp = new LoanLogResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(ResponseCodes.ERROR_401_CODE);
					logs.setResponseDescription(ResponseCodes.ERROR_401);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						LoanLogResponse res = new LoanLogResponse();
						res.setReturnCode(ResponseCodes.ERROR_400);
						res.setReturnMsg(verification);
						logs.setResponseCode(ResponseCodes.ERROR_400);
						logs.setResponseDescription(verification);
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return res;
					}
				} else {
					LoanLogResponse resp = new LoanLogResponse();
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode((ResponseCodes.ERROR_MSISDN_CODE));
					logs.setResponseDescription(ResponseCodes.ERROR_MSISDN);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					try {
						boolean result = Helper.isValidFormat("yyyy-mm-dd", cclient.getStartDate());
						if (!(result)) {
							LoanLogResponse resp = new LoanLogResponse();
							resp.setReturnCode(ResponseCodes.MISSING_PARAMETER_CODE);
							resp.setReturnMsg(ResponseCodes.MISSING_PARAMETER_DES);
							logs.setResponseDescription(ResponseCodes.MISSING_PARAMETER_DES);
							logs.updateLog(logs);
							return resp;
						}
					} catch (Exception ex) {
						LoanLogResponse resp = new LoanLogResponse();
						resp.setReturnCode(ResponseCodes.MISSING_PARAMETER_CODE);
						resp.setReturnMsg(ResponseCodes.MISSING_PARAMETER_DES);
						logs.setResponseDescription(ResponseCodes.MISSING_PARAMETER_DES);
						logs.updateLog(logs);
						return resp;
					}
					try {
						boolean result = Helper.isValidFormat("yyyy-mm-dd", cclient.getEndDate());
						if (!(result)) {
							LoanLogResponse resp = new LoanLogResponse();
							resp.setReturnMsg(ResponseCodes.MISSING_PARAMETER_DES);
							logs.setResponseDescription(ResponseCodes.MISSING_PARAMETER_DES);
							logs.updateLog(logs);
							return resp;
						}
					} catch (Exception ex) {
						LoanLogResponse resp = new LoanLogResponse();
						resp.setReturnCode(ResponseCodes.MISSING_PARAMETER_CODE);
						resp.setReturnMsg(ResponseCodes.MISSING_PARAMETER_DES);
						logs.setResponseDescription(ResponseCodes.MISSING_PARAMETER_DES);
						logs.updateLog(logs);
						return resp;
					}
					LoanLogResponse resp = new LoanLogResponse();
					Date endDateFormatted = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
							.parse(cclient.getEndDate() + " 23:59:59");
					Date starDateFormated = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
							.parse(cclient.getStartDate() + " 00:00:00");
					QueryLoanLogResultMsg loanResponse = null;
					try {
						loanResponse = queryLoanLog(cclient.getmsisdn());
						if (loanResponse.getResultHeader().getResultCode().equals("0")) {
							QueryLoanLogResult loanLogResult = loanResponse.getQueryLoanLogResult();
							for (RepaymentLogDetail loanLogRepayment : loanLogResult.getRepaymentLogDetail()) {
								try {
									String operationDate = loanLogRepayment.getOperDate();
									Date dateFormatted = null;
									try {
										dateFormatted = new SimpleDateFormat("yyyyMMddHHmmss").parse(operationDate);
										operationDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
												.format(dateFormatted);
										dateFormatted = new SimpleDateFormat("yyyy-MM-dd")
												.parse(new SimpleDateFormat("yyyy-MM-dd").format(dateFormatted));
									} catch (Exception e) {
										logger.error(Helper.GetException(e));
									}
									if (dateFormatted != null)
										if ((dateFormatted.before(endDateFormatted)
												|| dateFormatted.equals(endDateFormatted))
												&& (dateFormatted.after(starDateFormated)
														|| dateFormatted.equals(starDateFormated))) {
											resp.getLoanPayment().add(new LoanPaymentHistory(
													loanLogRepayment.getLoanID(),
													new SimpleDateFormat(Constants.SQL_DATE_FORMAT_History).format(
															new SimpleDateFormat(Constants.SQL_DATE_FORMAT_required)
																	.parse(operationDate)),
													((double) (loanLogRepayment.getRepayAMT()
															+ loanLogRepayment.getRepayPoundage())
															/ Constants.MONEY_DIVIDEND) + ""));
										}
								} catch (Exception ex) {
									logger.error(Helper.GetException(ex));
								}
							}
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
							logs.setResponseDescription(ResponseCodes.SUCESS_DES_200);
							logs.setResponseCode(ResponseCodes.SUCESS_CODE_200);
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} catch (Exception ex) {
						logger.error(Helper.GetException(ex));
					}logs.setResponseDescription(ResponseCodes.ERROR_401);
					logs.setResponseCode(ResponseCodes.ERROR_401_CODE);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				} else {
					LoanLogResponse resp = new LoanLogResponse();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseDescription(ResponseCodes.ERROR_401);
					logs.setResponseCode(ResponseCodes.ERROR_401_CODE);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		LoanLogResponse resp = new LoanLogResponse();
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseDescription(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	public QueryLoanLogResultMsg queryLoanLog(String msisdn) {
		QueryLoanLogRequestMsg msg = new QueryLoanLogRequestMsg();
		QueryLoanLogRequest qLLR = new QueryLoanLogRequest();
		SubAccessCode subAC = new SubAccessCode();
		subAC.setPrimaryIdentity(msisdn);
		qLLR.setSubAccessCode(subAC);
		qLLR.setBeginRowNum(Constants.BEGIN_ROW_NUM);
		qLLR.setFetchRowNum(Constants.FETCH_ROW_NUM);
		qLLR.setTotalRowNum(Constants.TOTAL_ROWNUM);
		msg.setRequestHeader(getLoanLogRequestHeader());
		msg.setQueryLoanLogRequest(qLLR);
		//return CBSARService.getInstance().queryLoanLog(msg);
		QueryLoanLogResultMsg resp=new QueryLoanLogResultMsg();
		return resp;
	}

	public com.huawei.bme.cbsinterface.cbscommon.RequestHeader getLoanLogRequestHeader() {
		com.huawei.bme.cbsinterface.cbscommon.RequestHeader loanRequestHeader = new com.huawei.bme.cbsinterface.cbscommon.RequestHeader();
		loanRequestHeader.setVersion(Constants.LL_VERSION);
		loanRequestHeader.setBusinessCode(Constants.LL_BUSINESS_CODE);
		OwnershipInfo ownershipinf = new OwnershipInfo();
		ownershipinf.setBEID(Constants.LL_BEID);
		ownershipinf.setBRID(Constants.LL_BRID);
		loanRequestHeader.setOwnershipInfo(ownershipinf);
		SecurityInfo value = new SecurityInfo();
		value.setLoginSystemCode(ConfigurationManager.getConfigurationFromCache("cbs.username"));
		value.setPassword(ConfigurationManager.getConfigurationFromCache("cbs.password"));
		loanRequestHeader.setAccessSecurity(value);
		OperatorInfo opInfo = new OperatorInfo();
		opInfo.setChannelID(Constants.LL_CHANNEL_ID);
		opInfo.setOperatorID(Constants.LL_OPERATOR_ID);
		loanRequestHeader.setOperatorInfo(opInfo);
		loanRequestHeader.setMessageSeq(Helper.generateTransactionID());
		return loanRequestHeader;
	}
}
