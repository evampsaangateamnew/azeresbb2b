package com.evampsaanga.b2b.azerfon.getappmenu;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"offeringName"
})
public class GetAppMenuRequest extends BaseRequest {

@JsonProperty("offeringName")
private String offeringName;

@JsonProperty("offeringName")
public String getOfferingName() {
return offeringName;
}

@JsonProperty("offeringName")
public void setOfferingName(String offeringName) {
this.offeringName = offeringName;
}

@Override
public String toString() {
return new ToStringBuilder(this).append("offeringName", offeringName).toString();
}

}
