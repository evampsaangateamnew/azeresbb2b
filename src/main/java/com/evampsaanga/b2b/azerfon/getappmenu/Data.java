
package com.evampsaanga.b2b.azerfon.getappmenu;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "MenuEN",
    "MenuAZ",
    "MenuRU"
})
public class Data {

    @JsonProperty("MenuEN")
    private MenuEN menuEN;
    @JsonProperty("MenuAZ")
    private MenuAZ menuAZ;
    @JsonProperty("MenuRU")
    private MenuRU menuRU;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("MenuEN")
    public MenuEN getMenuEN() {
        return menuEN;
    }

    @JsonProperty("MenuEN")
    public void setMenuEN(MenuEN menuEN) {
        this.menuEN = menuEN;
    }

    @JsonProperty("MenuAZ")
    public MenuAZ getMenuAZ() {
        return menuAZ;
    }

    @JsonProperty("MenuAZ")
    public void setMenuAZ(MenuAZ menuAZ) {
        this.menuAZ = menuAZ;
    }

    @JsonProperty("MenuRU")
    public MenuRU getMenuRU() {
        return menuRU;
    }

    @JsonProperty("MenuRU")
    public void setMenuRU(MenuRU menuRU) {
        this.menuRU = menuRU;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
