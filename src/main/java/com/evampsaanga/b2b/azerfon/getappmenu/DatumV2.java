package com.evampsaanga.b2b.azerfon.getappmenu;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

public class DatumV2 {
	ArrayList<MenuHorizontal> menuHorizontal;
	ArrayList<MenuVertical> menuVertical;
	
	public ArrayList<MenuHorizontal> getMenuHorizontal() {
		return menuHorizontal;
	}
	public void setMenuHorizontal(ArrayList<MenuHorizontal> menuHorizontal) {
		this.menuHorizontal = menuHorizontal;
	}
	public ArrayList<MenuVertical> getMenuVertical() {
		return menuVertical;
	}
	public void setMenuVertical(ArrayList<MenuVertical> menuVertical) {
		this.menuVertical = menuVertical;
	}
}
