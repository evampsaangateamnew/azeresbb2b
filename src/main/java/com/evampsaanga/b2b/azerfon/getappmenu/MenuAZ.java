
package com.evampsaanga.b2b.azerfon.getappmenu;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "menuHorizontal",
    "menuVertical"
})
public class MenuAZ {

    @JsonProperty("menuHorizontal")
    private List<MenuHorizontal_> menuHorizontal = null;
    @JsonProperty("menuVertical")
    private List<MenuVertical_> menuVertical = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("menuHorizontal")
    public List<MenuHorizontal_> getMenuHorizontal() {
        return menuHorizontal;
    }

    @JsonProperty("menuHorizontal")
    public void setMenuHorizontal(List<MenuHorizontal_> menuHorizontal) {
        this.menuHorizontal = menuHorizontal;
    }

    @JsonProperty("menuVertical")
    public List<MenuVertical_> getMenuVertical() {
        return menuVertical;
    }

    @JsonProperty("menuVertical")
    public void setMenuVertical(List<MenuVertical_> menuVertical) {
        this.menuVertical = menuVertical;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
