package com.evampsaanga.b2b.azerfon.getappmenu;

import java.util.ArrayList;
import java.util.List;

import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;
import com.fasterxml.jackson.annotation.JsonProperty;

public class GetAppMenuResponse extends BaseResponse {
	@JsonProperty("data")
	private List<Datum> data = new ArrayList<Datum>();

	/**
	 * @return the data
	 */
	public List<Datum> getData() {
		return data;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	public void setData(List<Datum> data) {
		this.data = data;
	}
}
