package com.evampsaanga.b2b.azerfon.sendotp;

import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;

public class SendOTPResponse extends BaseResponse {
	private String responseMsg;
	private String channel;

	public String getResponseMsg() {
		return responseMsg;
	}

	public void setResponseMsg(String responseMsg) {
		this.responseMsg = responseMsg;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

}
