package com.evampsaanga.b2b.azerfon.sendotp;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "SignUpRequest")
public class SendOTPRequest extends BaseRequest {
	private String userName;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Override
	public String toString() {
		return "SendOTPRequest [userName=" + userName + "]";
	}
	
}
