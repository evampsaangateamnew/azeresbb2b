package com.evampsaanga.b2b.azerfon.getnetworksettings;

public class NetworkService {
	private String status = "";
	private String productId = "";
	private String serviceName = "";

	public NetworkService(String status, String productId, String serviceName) {
		super();
		this.status = status;
		this.productId = productId;
		this.serviceName = serviceName;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the productId
	 */
	public String getProductId() {
		return productId;
	}

	/**
	 * @param productId
	 *            the productId to set
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}

	/**
	 * @return the serviceName
	 */
	public String getServiceName() {
		return serviceName;
	}

	/**
	 * @param serviceName
	 *            the serviceName to set
	 */
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
}
