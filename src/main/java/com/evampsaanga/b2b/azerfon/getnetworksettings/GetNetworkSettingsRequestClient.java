package com.evampsaanga.b2b.azerfon.getnetworksettings;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetNetworkSettingsRequestClient", propOrder = { "requestBody" })
@XmlRootElement(name = "GetNetworkSettingsRequestClient")
public class GetNetworkSettingsRequestClient extends com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest {
	// @XmlElement(name = "networkRequestBody", required = true)
	// GetNetworkSettingDataRequest networkRequestBody = null;
	// @XmlElement(name = "requestBody", required = true)
	// String requestBody="";
	/*
	 * public GetNetworkSettingDataRequest getNetworkRequestBody() { return
	 * networkRequestBody; } public void
	 * setNetworkRequestBody(GetNetworkSettingDataRequest networkRequestBody) {
	 * this.networkRequestBody = networkRequestBody; }
	 */
}
