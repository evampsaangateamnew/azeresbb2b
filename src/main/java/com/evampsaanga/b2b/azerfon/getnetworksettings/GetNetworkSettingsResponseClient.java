package com.evampsaanga.b2b.azerfon.getnetworksettings;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetNetworkSettingsResponseClient", propOrder = { "responseBody" })
@XmlRootElement(name = "GetSubscriberResponseClient")
public class GetNetworkSettingsResponseClient extends BaseResponse {
	private List<NetworkService> list = new ArrayList<NetworkService>();

	/**
	 * @return the list
	 */
	public List<NetworkService> getList() {
		return list;
	}

	/**
	 * @param list
	 *            the list to set
	 */
	public void setList(List<NetworkService> list) {
		this.list = list;
	}
}
