package com.evampsaanga.b2b.azerfon.magento.mysubs;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "callPriceIcon", "callPriceLabel", "callPriceValue", "onnetPriceLabel", "onnetPriceValue",
		"offnetPriceLabel", "offnetPriceValue", "descriptionPrice" })
public class Price {
	@JsonProperty("callPriceIcon")
	private String callPriceIcon;
	@JsonProperty("callPriceLabel")
	private String callPriceLabel;
	@JsonProperty("callPriceValue")
	private String callPriceValue;
	@JsonProperty("onnetPriceLabel")
	private String onnetPriceLabel;
	@JsonProperty("onnetPriceValue")
	private String onnetPriceValue;
	@JsonProperty("offnetPriceLabel")
	private String offnetPriceLabel;
	@JsonProperty("offnetPriceValue")
	private String offnetPriceValue;
	@JsonProperty("descriptionPrice")
	private String descriptionPrice;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("callPriceIcon")
	public String getCallPriceIcon() {
		return callPriceIcon;
	}

	@JsonProperty("callPriceIcon")
	public void setCallPriceIcon(String callPriceIcon) {
		this.callPriceIcon = callPriceIcon;
	}

	@JsonProperty("callPriceLabel")
	public String getCallPriceLabel() {
		return callPriceLabel;
	}

	@JsonProperty("callPriceLabel")
	public void setCallPriceLabel(String callPriceLabel) {
		this.callPriceLabel = callPriceLabel;
	}

	@JsonProperty("callPriceValue")
	public String getCallPriceValue() {
		return callPriceValue;
	}

	@JsonProperty("callPriceValue")
	public void setCallPriceValue(String callPriceValue) {
		this.callPriceValue = callPriceValue;
	}

	@JsonProperty("onnetPriceLabel")
	public String getOnnetPriceLabel() {
		return onnetPriceLabel;
	}

	@JsonProperty("onnetPriceLabel")
	public void setOnnetPriceLabel(String onnetPriceLabel) {
		this.onnetPriceLabel = onnetPriceLabel;
	}

	@JsonProperty("onnetPriceValue")
	public String getOnnetPriceValue() {
		return onnetPriceValue;
	}

	@JsonProperty("onnetPriceValue")
	public void setOnnetPriceValue(String onnetPriceValue) {
		this.onnetPriceValue = onnetPriceValue;
	}

	@JsonProperty("offnetPriceLabel")
	public String getOffnetPriceLabel() {
		return offnetPriceLabel;
	}

	@JsonProperty("offnetPriceLabel")
	public void setOffnetPriceLabel(String offnetPriceLabel) {
		this.offnetPriceLabel = offnetPriceLabel;
	}

	@JsonProperty("offnetPriceValue")
	public String getOffnetPriceValue() {
		return offnetPriceValue;
	}

	@JsonProperty("offnetPriceValue")
	public void setOffnetPriceValue(String offnetPriceValue) {
		this.offnetPriceValue = offnetPriceValue;
	}

	@JsonProperty("descriptionPrice")
	public String getDescriptionPrice() {
		return descriptionPrice;
	}

	@JsonProperty("descriptionPrice")
	public void setDescriptionPrice(String descriptionPrice) {
		this.descriptionPrice = descriptionPrice;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}
}
