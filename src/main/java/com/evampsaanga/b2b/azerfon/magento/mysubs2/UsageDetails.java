package com.evampsaanga.b2b.azerfon.magento.mysubs2;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "offerName", "totalUnits", "remainingUnits", "unitName", "initialDate", "expiryDate", "status",
		"amount", "currency", "offeringId" })
public class UsageDetails {
	@JsonProperty("offerName")
	private String offerName;
	@JsonProperty("totalUnits")
	private String totalUnits;
	@JsonProperty("remainingUnits")
	private String remainingUnits;
	@JsonProperty("unitName")
	private String unitName;
	@JsonProperty("initialDate")
	private String initialDate;
	@JsonProperty("expiryDate")
	private String expiryDate;
	@JsonProperty("status")
	private String status;
	@JsonProperty("amount")
	private String amount;
	@JsonProperty("currency")
	private String currency;
	@JsonProperty("offeringId")
	private String offeringId;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("offerName")
	public String getOfferName() {
		return offerName;
	}

	@JsonProperty("offerName")
	public void setOfferName(String offerName) {
		this.offerName = offerName;
	}

	@JsonProperty("totalUnits")
	public String getTotalUnits() {
		return totalUnits;
	}

	@JsonProperty("totalUnits")
	public void setTotalUnits(String totalUnits) {
		this.totalUnits = totalUnits;
	}

	@JsonProperty("remainingUnits")
	public String getRemainingUnits() {
		return remainingUnits;
	}

	@JsonProperty("remainingUnits")
	public void setRemainingUnits(String remainingUnits) {
		this.remainingUnits = remainingUnits;
	}

	@JsonProperty("unitName")
	public String getUnitName() {
		return unitName;
	}

	@JsonProperty("unitName")
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	@JsonProperty("initialDate")
	public String getInitialDate() {
		return initialDate;
	}

	@JsonProperty("initialDate")
	public void setInitialDate(String initialDate) {
		this.initialDate = initialDate;
	}

	@JsonProperty("expiryDate")
	public String getExpiryDate() {
		return expiryDate;
	}

	@JsonProperty("expiryDate")
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	@JsonProperty("status")
	public String getStatus() {
		return status;
	}

	@JsonProperty("status")
	public void setStatus(String status) {
		this.status = status;
	}

	@JsonProperty("amount")
	public String getAmount() {
		return amount;
	}

	@JsonProperty("amount")
	public void setAmount(String amount) {
		this.amount = amount;
	}

	@JsonProperty("currency")
	public String getCurrency() {
		return currency;
	}

	@JsonProperty("currency")
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	@JsonProperty("offeringId")
	public String getOfferingId() {
		return offeringId;
	}

	@JsonProperty("offeringId")
	public void setOfferingId(String offeringId) {
		this.offeringId = offeringId;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}
}
