package com.evampsaanga.b2b.azerfon.magento.mysubs;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "header", "usageDetails", "details" })
public class Offer {
	@JsonProperty("header")
	private Header header;
	@JsonProperty("usageDetails")
	private UsageDetails usageDetails;
	@JsonProperty("details")
	private Details details;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("header")
	public Header getHeader() {
		return header;
	}

	@JsonProperty("header")
	public void setHeader(Header header) {
		this.header = header;
	}

	@JsonProperty("usageDetails")
	public UsageDetails getUsageDetails() {
		return usageDetails;
	}

	@JsonProperty("usageDetails")
	public void setUsageDetails(UsageDetails usageDetails) {
		this.usageDetails = usageDetails;
	}

	@JsonProperty("details")
	public Details getDetails() {
		return details;
	}

	@JsonProperty("details")
	public void setDetails(Details details) {
		this.details = details;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}
}
