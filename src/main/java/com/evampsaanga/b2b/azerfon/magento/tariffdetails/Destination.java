
package com.evampsaanga.b2b.azerfon.magento.tariffdetails;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "destinationIcon",
    "destinationLabel",
    "destinationValue",
    "destinationOnnetLabel",
    "destinationOnnetValue",
    "destinationOffnetLabel",
    "destinationOffnetValue",
    "destinationInternationalLabel",
    "destinationInternationalValue",
    "destinationInternetLabel",
    "destinationInternetValue",
    "shortDescription"
})
public class Destination {

    @JsonProperty("destinationIcon")
    private String destinationIcon;
    @JsonProperty("destinationLabel")
    private String destinationLabel;
    @JsonProperty("destinationValue")
    private String destinationValue;
    @JsonProperty("destinationOnnetLabel")
    private String destinationOnnetLabel;
    @JsonProperty("destinationOnnetValue")
    private String destinationOnnetValue;
    @JsonProperty("destinationOffnetLabel")
    private String destinationOffnetLabel;
    @JsonProperty("destinationOffnetValue")
    private String destinationOffnetValue;
    @JsonProperty("destinationInternationalLabel")
    private String destinationInternationalLabel;
    @JsonProperty("destinationInternationalValue")
    private String destinationInternationalValue;
    @JsonProperty("destinationInternetLabel")
    private String destinationInternetLabel;
    @JsonProperty("destinationInternetValue")
    private String destinationInternetValue;
    @JsonProperty("shortDescription")
    private String shortDescription;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("destinationIcon")
    public String getDestinationIcon() {
        return destinationIcon;
    }

    @JsonProperty("destinationIcon")
    public void setDestinationIcon(String destinationIcon) {
        this.destinationIcon = destinationIcon;
    }

    @JsonProperty("destinationLabel")
    public String getDestinationLabel() {
        return destinationLabel;
    }

    @JsonProperty("destinationLabel")
    public void setDestinationLabel(String destinationLabel) {
        this.destinationLabel = destinationLabel;
    }

    @JsonProperty("destinationValue")
    public String getDestinationValue() {
        return destinationValue;
    }

    @JsonProperty("destinationValue")
    public void setDestinationValue(String destinationValue) {
        this.destinationValue = destinationValue;
    }

    @JsonProperty("destinationOnnetLabel")
    public String getDestinationOnnetLabel() {
        return destinationOnnetLabel;
    }

    @JsonProperty("destinationOnnetLabel")
    public void setDestinationOnnetLabel(String destinationOnnetLabel) {
        this.destinationOnnetLabel = destinationOnnetLabel;
    }

    @JsonProperty("destinationOnnetValue")
    public String getDestinationOnnetValue() {
        return destinationOnnetValue;
    }

    @JsonProperty("destinationOnnetValue")
    public void setDestinationOnnetValue(String destinationOnnetValue) {
        this.destinationOnnetValue = destinationOnnetValue;
    }

    @JsonProperty("destinationOffnetLabel")
    public String getDestinationOffnetLabel() {
        return destinationOffnetLabel;
    }

    @JsonProperty("destinationOffnetLabel")
    public void setDestinationOffnetLabel(String destinationOffnetLabel) {
        this.destinationOffnetLabel = destinationOffnetLabel;
    }

    @JsonProperty("destinationOffnetValue")
    public String getDestinationOffnetValue() {
        return destinationOffnetValue;
    }

    @JsonProperty("destinationOffnetValue")
    public void setDestinationOffnetValue(String destinationOffnetValue) {
        this.destinationOffnetValue = destinationOffnetValue;
    }

    @JsonProperty("destinationInternationalLabel")
    public String getDestinationInternationalLabel() {
        return destinationInternationalLabel;
    }

    @JsonProperty("destinationInternationalLabel")
    public void setDestinationInternationalLabel(String destinationInternationalLabel) {
        this.destinationInternationalLabel = destinationInternationalLabel;
    }

    @JsonProperty("destinationInternationalValue")
    public String getDestinationInternationalValue() {
        return destinationInternationalValue;
    }

    @JsonProperty("destinationInternationalValue")
    public void setDestinationInternationalValue(String destinationInternationalValue) {
        this.destinationInternationalValue = destinationInternationalValue;
    }

    @JsonProperty("destinationInternetLabel")
    public String getDestinationInternetLabel() {
        return destinationInternetLabel;
    }

    @JsonProperty("destinationInternetLabel")
    public void setDestinationInternetLabel(String destinationInternetLabel) {
        this.destinationInternetLabel = destinationInternetLabel;
    }

    @JsonProperty("destinationInternetValue")
    public String getDestinationInternetValue() {
        return destinationInternetValue;
    }

    @JsonProperty("destinationInternetValue")
    public void setDestinationInternetValue(String destinationInternetValue) {
        this.destinationInternetValue = destinationInternetValue;
    }

    @JsonProperty("shortDescription")
    public String getShortDescription() {
        return shortDescription;
    }

    @JsonProperty("shortDescription")
    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
