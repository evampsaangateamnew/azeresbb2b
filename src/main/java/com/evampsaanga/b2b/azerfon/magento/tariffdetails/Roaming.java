
package com.evampsaanga.b2b.azerfon.magento.tariffdetails;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "country",
    "descriptionPosition",
    "description",
    "roamingIcon"
})
public class Roaming {

    @JsonProperty("country")
    private List<Object> country = null;
    @JsonProperty("descriptionPosition")
    private String descriptionPosition;
    @JsonProperty("description")
    private String description;
    @JsonProperty("roamingIcon")
    private String roamingIcon;
   
   

	@JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("country")
    public List<Object> getCountry() {
        return country;
    }

    @JsonProperty("country")
    public void setCountry(List<Object> country) {
        this.country = country;
    }

    @JsonProperty("descriptionPosition")
    public String getDescriptionPosition() {
        return descriptionPosition;
    }

    @JsonProperty("descriptionPosition")
    public void setDescriptionPosition(String descriptionPosition) {
        this.descriptionPosition = descriptionPosition;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("roamingIcon")
    public String getRoamingIcon() {
		return roamingIcon;
	}
    @JsonProperty("roamingIcon")
	public void setRoamingIcon(String roamingIcon) {
		this.roamingIcon = roamingIcon;
	}
    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
