
package com.evampsaanga.b2b.azerfon.magento.tariffdetails;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "tariffDescription",
    "sortOrder",
    "id",
    "offeringName",
    "offeringId",
    "tagIcon",
    "tag",
    "currency",
    "name",
    "mrcLabel",
    "mrcValue",
    "Call",
    "Internet",
    "SMS",
    "countryWide",
    "Whatsapp",
    "bonusSix",
    "bonusSeven",
    "bonusEight",
    "bonusNine",
    "bonusTen",
    "bonusLabel"
})
public class Header {

    @JsonProperty("tariffDescription")
    private String tariffDescription;
    @JsonProperty("sortOrder")
    private String sortOrder;
    @JsonProperty("id")
    private String id;
    @JsonProperty("offeringName")
    private String offeringName;
    @JsonProperty("offeringId")
    private String offeringId;
    @JsonProperty("tagIcon")
    private String tagIcon;
    @JsonProperty("tag")
    private String tag;
    @JsonProperty("currency")
    private String currency;
    @JsonProperty("name")
    private String name;
    @JsonProperty("mrcLabel")
    private String mrcLabel;
    @JsonProperty("mrcValue")
    private String mrcValue;
    @JsonProperty("Call")
    private Call call;
    @JsonProperty("Internet")
    private Internet internet;
    @JsonProperty("SMS")
    private SMS sMS;
    @JsonProperty("countryWide")
    private CountryWide countryWide;
    @JsonProperty("Whatsapp")
    private Whatsapp whatsapp;
    @JsonProperty("bonusSix")
    private BonusSix bonusSix;
    @JsonProperty("bonusSeven")
    private BonusSeven bonusSeven;
    @JsonProperty("bonusEight")
    private BonusEight bonusEight;
    @JsonProperty("bonusNine")
    private BonusNine bonusNine;
    @JsonProperty("bonusTen")
    private BonusTen bonusTen;
    @JsonProperty("bonusLabel")
    private String bonusLabel;
    
    @JsonProperty("bonusLabel")
    public String getBonusLabel() {
		return bonusLabel;
	}
    @JsonProperty("bonusLabel")
	public void setBonusLabel(String bonusLabel) {
		this.bonusLabel = bonusLabel;
	}

	@JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("tariffDescription")
    public String getTariffDescription() {
        return tariffDescription;
    }

    @JsonProperty("tariffDescription")
    public void setTariffDescription(String tariffDescription) {
        this.tariffDescription = tariffDescription;
    }

    @JsonProperty("sortOrder")
    public String getSortOrder() {
        return sortOrder;
    }

    @JsonProperty("sortOrder")
    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("offeringName")
    public String getOfferingName() {
        return offeringName;
    }

    @JsonProperty("offeringName")
    public void setOfferingName(String offeringName) {
        this.offeringName = offeringName;
    }

    @JsonProperty("offeringId")
    public String getOfferingId() {
        return offeringId;
    }

    @JsonProperty("offeringId")
    public void setOfferingId(String offeringId) {
        this.offeringId = offeringId;
    }

    @JsonProperty("tagIcon")
    public String getTagIcon() {
        return tagIcon;
    }

    @JsonProperty("tagIcon")
    public void setTagIcon(String tagIcon) {
        this.tagIcon = tagIcon;
    }

    @JsonProperty("tag")
    public String getTag() {
        return tag;
    }

    @JsonProperty("tag")
    public void setTag(String tag) {
        this.tag = tag;
    }

    @JsonProperty("currency")
    public String getCurrency() {
        return currency;
    }

    @JsonProperty("currency")
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("mrcLabel")
    public String getMrcLabel() {
        return mrcLabel;
    }

    @JsonProperty("mrcLabel")
    public void setMrcLabel(String mrcLabel) {
        this.mrcLabel = mrcLabel;
    }

    @JsonProperty("mrcValue")
    public String getMrcValue() {
        return mrcValue;
    }

    @JsonProperty("mrcValue")
    public void setMrcValue(String mrcValue) {
        this.mrcValue = mrcValue;
    }

    @JsonProperty("Call")
    public Call getCall() {
        return call;
    }

    @JsonProperty("Call")
    public void setCall(Call call) {
        this.call = call;
    }

    @JsonProperty("Internet")
    public Internet getInternet() {
        return internet;
    }

    @JsonProperty("Internet")
    public void setInternet(Internet internet) {
        this.internet = internet;
    }

    @JsonProperty("SMS")
    public SMS getSMS() {
        return sMS;
    }

    @JsonProperty("SMS")
    public void setSMS(SMS sMS) {
        this.sMS = sMS;
    }

    @JsonProperty("countryWide")
    public CountryWide getCountryWide() {
        return countryWide;
    }

    @JsonProperty("countryWide")
    public void setCountryWide(CountryWide countryWide) {
        this.countryWide = countryWide;
    }

    @JsonProperty("Whatsapp")
    public Whatsapp getWhatsapp() {
        return whatsapp;
    }

    @JsonProperty("Whatsapp")
    public void setWhatsapp(Whatsapp whatsapp) {
        this.whatsapp = whatsapp;
    }

    @JsonProperty("bonusSix")
    public BonusSix getBonusSix() {
        return bonusSix;
    }

    @JsonProperty("bonusSix")
    public void setBonusSix(BonusSix bonusSix) {
        this.bonusSix = bonusSix;
    }

    @JsonProperty("bonusSeven")
    public BonusSeven getBonusSeven() {
        return bonusSeven;
    }

    @JsonProperty("bonusSeven")
    public void setBonusSeven(BonusSeven bonusSeven) {
        this.bonusSeven = bonusSeven;
    }

    @JsonProperty("bonusEight")
    public BonusEight getBonusEight() {
        return bonusEight;
    }

    @JsonProperty("bonusEight")
    public void setBonusEight(BonusEight bonusEight) {
        this.bonusEight = bonusEight;
    }

    @JsonProperty("bonusNine")
    public BonusNine getBonusNine() {
        return bonusNine;
    }

    @JsonProperty("bonusNine")
    public void setBonusNine(BonusNine bonusNine) {
        this.bonusNine = bonusNine;
    }

    @JsonProperty("bonusTen")
    public BonusTen getBonusTen() {
        return bonusTen;
    }

    @JsonProperty("bonusTen")
    public void setBonusTen(BonusTen bonusTen) {
        this.bonusTen = bonusTen;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
