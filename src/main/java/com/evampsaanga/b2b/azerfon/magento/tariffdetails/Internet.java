
package com.evampsaanga.b2b.azerfon.magento.tariffdetails;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "internetIcon",
    "internetLabel",
    "internetValue",
    "internetMetrics",
 
})
public class Internet {

    @JsonProperty("internetIcon")
    private String internetIcon;
    @JsonProperty("internetLabel")
    private String internetLabel;
    @JsonProperty("internetValue")
    private String internetValue;
    @JsonProperty("internetMetrics")
    private String internetMetrics;
  

	@JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("internetIcon")
    public String getInternetIcon() {
        return internetIcon;
    }

    @JsonProperty("internetIcon")
    public void setInternetIcon(String internetIcon) {
        this.internetIcon = internetIcon;
    }

    @JsonProperty("internetLabel")
    public String getInternetLabel() {
        return internetLabel;
    }

    @JsonProperty("internetLabel")
    public void setInternetLabel(String internetLabel) {
        this.internetLabel = internetLabel;
    }

    @JsonProperty("internetValue")
    public String getInternetValue() {
        return internetValue;
    }

    @JsonProperty("internetValue")
    public void setInternetValue(String internetValue) {
        this.internetValue = internetValue;
    }

    @JsonProperty("internetMetrics")
    public String getInternetMetrics() {
        return internetMetrics;
    }

    @JsonProperty("internetMetrics")
    public void setInternetMetrics(String internetMetrics) {
        this.internetMetrics = internetMetrics;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
