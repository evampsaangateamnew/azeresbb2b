
package com.evampsaanga.b2b.azerfon.magento.tariffdetails;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "smsIcon",
    "smsLabel",
    "smsValue",
    "smsCountryWideLabel",
    "smsCountryWideValue",
    "smsInternationalLabel",
    "smsInternationalValue"
})
public class SMS_ {

    @JsonProperty("smsIcon")
    private String smsIcon;
    @JsonProperty("smsLabel")
    private String smsLabel;
    @JsonProperty("smsValue")
    private String smsValue;
    @JsonProperty("smsCountryWideLabel")
    private String smsCountryWideLabel;
    @JsonProperty("smsCountryWideValue")
    private String smsCountryWideValue;
    @JsonProperty("smsInternationalLabel")
    private String smsInternationalLabel;
    @JsonProperty("smsInternationalValue")
    private String smsInternationalValue;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("smsIcon")
    public String getSmsIcon() {
        return smsIcon;
    }

    @JsonProperty("smsIcon")
    public void setSmsIcon(String smsIcon) {
        this.smsIcon = smsIcon;
    }

    @JsonProperty("smsLabel")
    public String getSmsLabel() {
        return smsLabel;
    }

    @JsonProperty("smsLabel")
    public void setSmsLabel(String smsLabel) {
        this.smsLabel = smsLabel;
    }

    @JsonProperty("smsValue")
    public String getSmsValue() {
        return smsValue;
    }

    @JsonProperty("smsValue")
    public void setSmsValue(String smsValue) {
        this.smsValue = smsValue;
    }

    @JsonProperty("smsCountryWideLabel")
    public String getSmsCountryWideLabel() {
        return smsCountryWideLabel;
    }

    @JsonProperty("smsCountryWideLabel")
    public void setSmsCountryWideLabel(String smsCountryWideLabel) {
        this.smsCountryWideLabel = smsCountryWideLabel;
    }

    @JsonProperty("smsCountryWideValue")
    public String getSmsCountryWideValue() {
        return smsCountryWideValue;
    }

    @JsonProperty("smsCountryWideValue")
    public void setSmsCountryWideValue(String smsCountryWideValue) {
        this.smsCountryWideValue = smsCountryWideValue;
    }

    @JsonProperty("smsInternationalLabel")
    public String getSmsInternationalLabel() {
        return smsInternationalLabel;
    }

    @JsonProperty("smsInternationalLabel")
    public void setSmsInternationalLabel(String smsInternationalLabel) {
        this.smsInternationalLabel = smsInternationalLabel;
    }

    @JsonProperty("smsInternationalValue")
    public String getSmsInternationalValue() {
        return smsInternationalValue;
    }

    @JsonProperty("smsInternationalValue")
    public void setSmsInternationalValue(String smsInternationalValue) {
        this.smsInternationalValue = smsInternationalValue;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
