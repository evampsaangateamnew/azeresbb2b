
package com.evampsaanga.b2b.azerfon.magento.tariffdetails;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "header",
    "packagePrices",
    "details",
    "subscribable"
})
public class Item {

    @JsonProperty("header")
    private Header header;
    @JsonProperty("packagePrices")
    private PackagePrices packagePrices;
    @JsonProperty("details")
    private Details details;
    @JsonProperty("subscribable")
    private String subscribable;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("header")
    public Header getHeader() {
        return header;
    }

    @JsonProperty("header")
    public void setHeader(Header header) {
        this.header = header;
    }

    @JsonProperty("packagePrices")
    public PackagePrices getPackagePrices() {
        return packagePrices;
    }

    @JsonProperty("packagePrices")
    public void setPackagePrices(PackagePrices packagePrices) {
        this.packagePrices = packagePrices;
    }

    @JsonProperty("details")
    public Details getDetails() {
        return details;
    }

    @JsonProperty("details")
    public void setDetails(Details details) {
        this.details = details;
    }

    @JsonProperty("subscribable")
    public String getSubscribable() {
        return subscribable;
    }

    @JsonProperty("subscribable")
    public void setSubscribable(String subscribable) {
        this.subscribable = subscribable;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
