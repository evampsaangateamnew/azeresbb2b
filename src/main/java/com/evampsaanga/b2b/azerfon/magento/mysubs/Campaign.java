package com.evampsaanga.b2b.azerfon.magento.mysubs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "offers" })
public class Campaign {
	@JsonProperty("offers")
	private List<com.evampsaanga.b2b.azerfon.magento.mysubs2.Offer> offers = new ArrayList<com.evampsaanga.b2b.azerfon.magento.mysubs2.Offer>();
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("offers")
	public List<com.evampsaanga.b2b.azerfon.magento.mysubs2.Offer> getOffers() {
		return offers;
	}

	@JsonProperty("offers")
	public void setOffers(List<com.evampsaanga.b2b.azerfon.magento.mysubs2.Offer> offers) {
		this.offers = offers;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}
}
