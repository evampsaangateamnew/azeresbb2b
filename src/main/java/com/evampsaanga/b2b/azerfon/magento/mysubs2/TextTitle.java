package com.evampsaanga.b2b.azerfon.magento.mysubs2;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "textWithTitleLabel", "textWithTitleDescription" })
public class TextTitle {
	@JsonProperty("textWithTitleLabel")
	private String textWithTitleLabel;
	@JsonProperty("textWithTitleDescription")
	private String textWithTitleDescription;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("textWithTitleLabel")
	public String getTextWithTitleLabel() {
		return textWithTitleLabel;
	}

	@JsonProperty("textWithTitleLabel")
	public void setTextWithTitleLabel(String textWithTitleLabel) {
		this.textWithTitleLabel = textWithTitleLabel;
	}

	@JsonProperty("textWithTitleDescription")
	public String getTextWithTitleDescription() {
		return textWithTitleDescription;
	}

	@JsonProperty("textWithTitleDescription")
	public void setTextWithTitleDescription(String textWithTitleDescription) {
		this.textWithTitleDescription = textWithTitleDescription;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}
}
