package com.evampsaanga.b2b.azerfon.magento.mysubs2;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "destinationRoundingIcon", "destinationRoundingLabel", "destinationRoundingValue",
		"onnetRoundingLabel", "onnetRoundingValue", "offnetRoundingLabel", "offnetRoundingValue",
		"internationalRoundingLabel", "internationalRoundingValue", "internetRoundingLabel", "internetRoundingValue",
		"descriptionRounding" })
public class Rounding {
	@JsonProperty("destinationRoundingIcon")
	private String destinationRoundingIcon;
	@JsonProperty("destinationRoundingLabel")
	private String destinationRoundingLabel;
	@JsonProperty("destinationRoundingValue")
	private String destinationRoundingValue;
	@JsonProperty("onnetRoundingLabel")
	private String onnetRoundingLabel;
	@JsonProperty("onnetRoundingValue")
	private String onnetRoundingValue;
	@JsonProperty("offnetRoundingLabel")
	private String offnetRoundingLabel;
	@JsonProperty("offnetRoundingValue")
	private String offnetRoundingValue;
	@JsonProperty("internationalRoundingLabel")
	private String internationalRoundingLabel;
	@JsonProperty("internationalRoundingValue")
	private String internationalRoundingValue;
	@JsonProperty("internetRoundingLabel")
	private String internetRoundingLabel;
	@JsonProperty("internetRoundingValue")
	private String internetRoundingValue;
	@JsonProperty("descriptionRounding")
	private String descriptionRounding;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("destinationRoundingIcon")
	public String getDestinationRoundingIcon() {
		return destinationRoundingIcon;
	}

	@JsonProperty("destinationRoundingIcon")
	public void setDestinationRoundingIcon(String destinationRoundingIcon) {
		this.destinationRoundingIcon = destinationRoundingIcon;
	}

	@JsonProperty("destinationRoundingLabel")
	public String getDestinationRoundingLabel() {
		return destinationRoundingLabel;
	}

	@JsonProperty("destinationRoundingLabel")
	public void setDestinationRoundingLabel(String destinationRoundingLabel) {
		this.destinationRoundingLabel = destinationRoundingLabel;
	}

	@JsonProperty("destinationRoundingValue")
	public String getDestinationRoundingValue() {
		return destinationRoundingValue;
	}

	@JsonProperty("destinationRoundingValue")
	public void setDestinationRoundingValue(String destinationRoundingValue) {
		this.destinationRoundingValue = destinationRoundingValue;
	}

	@JsonProperty("onnetRoundingLabel")
	public String getOnnetRoundingLabel() {
		return onnetRoundingLabel;
	}

	@JsonProperty("onnetRoundingLabel")
	public void setOnnetRoundingLabel(String onnetRoundingLabel) {
		this.onnetRoundingLabel = onnetRoundingLabel;
	}

	@JsonProperty("onnetRoundingValue")
	public String getOnnetRoundingValue() {
		return onnetRoundingValue;
	}

	@JsonProperty("onnetRoundingValue")
	public void setOnnetRoundingValue(String onnetRoundingValue) {
		this.onnetRoundingValue = onnetRoundingValue;
	}

	@JsonProperty("offnetRoundingLabel")
	public String getOffnetRoundingLabel() {
		return offnetRoundingLabel;
	}

	@JsonProperty("offnetRoundingLabel")
	public void setOffnetRoundingLabel(String offnetRoundingLabel) {
		this.offnetRoundingLabel = offnetRoundingLabel;
	}

	@JsonProperty("offnetRoundingValue")
	public String getOffnetRoundingValue() {
		return offnetRoundingValue;
	}

	@JsonProperty("offnetRoundingValue")
	public void setOffnetRoundingValue(String offnetRoundingValue) {
		this.offnetRoundingValue = offnetRoundingValue;
	}

	@JsonProperty("internationalRoundingLabel")
	public String getInternationalRoundingLabel() {
		return internationalRoundingLabel;
	}

	@JsonProperty("internationalRoundingLabel")
	public void setInternationalRoundingLabel(String internationalRoundingLabel) {
		this.internationalRoundingLabel = internationalRoundingLabel;
	}

	@JsonProperty("internationalRoundingValue")
	public String getInternationalRoundingValue() {
		return internationalRoundingValue;
	}

	@JsonProperty("internationalRoundingValue")
	public void setInternationalRoundingValue(String internationalRoundingValue) {
		this.internationalRoundingValue = internationalRoundingValue;
	}

	@JsonProperty("internetRoundingLabel")
	public String getInternetRoundingLabel() {
		return internetRoundingLabel;
	}

	@JsonProperty("internetRoundingLabel")
	public void setInternetRoundingLabel(String internetRoundingLabel) {
		this.internetRoundingLabel = internetRoundingLabel;
	}

	@JsonProperty("internetRoundingValue")
	public String getInternetRoundingValue() {
		return internetRoundingValue;
	}

	@JsonProperty("internetRoundingValue")
	public void setInternetRoundingValue(String internetRoundingValue) {
		this.internetRoundingValue = internetRoundingValue;
	}

	@JsonProperty("descriptionRounding")
	public String getDescriptionRounding() {
		return descriptionRounding;
	}

	@JsonProperty("descriptionRounding")
	public void setDescriptionRounding(String descriptionRounding) {
		this.descriptionRounding = descriptionRounding;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}
}
