package com.evampsaanga.b2b.azerfon.magento.mysubs2;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "type", "tag", "name", "offerGroupNameLabel", "offerGroupNameValue" })
public class Header {
	@JsonProperty("id")
	private String id;
	@JsonProperty("type")
	private String type;
	@JsonProperty("tag")
	private String tag;
	@JsonProperty("name")
	private String name;
	@JsonProperty("offerGroupNameLabel")
	private String offerGroupNameLabel;
	@JsonProperty("offerGroupNameValue")
	private String offerGroupNameValue;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("type")
	public String getType() {
		return type;
	}

	@JsonProperty("type")
	public void setType(String type) {
		this.type = type;
	}

	@JsonProperty("tag")
	public String getTag() {
		return tag;
	}

	@JsonProperty("tag")
	public void setTag(String tag) {
		this.tag = tag;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("offerGroupNameLabel")
	public String getOfferGroupNameLabel() {
		return offerGroupNameLabel;
	}

	@JsonProperty("offerGroupNameLabel")
	public void setOfferGroupNameLabel(String offerGroupNameLabel) {
		this.offerGroupNameLabel = offerGroupNameLabel;
	}

	@JsonProperty("offerGroupNameValue")
	public String getOfferGroupNameValue() {
		return offerGroupNameValue;
	}

	@JsonProperty("offerGroupNameValue")
	public void setOfferGroupNameValue(String offerGroupNameValue) {
		this.offerGroupNameValue = offerGroupNameValue;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}
}
