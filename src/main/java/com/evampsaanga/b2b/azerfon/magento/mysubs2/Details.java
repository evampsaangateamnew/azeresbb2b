package com.evampsaanga.b2b.azerfon.magento.mysubs2;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "titleSubtitle", "price", "rounding", "textTitle", "date", "time", "freeResource" })
public class Details {
	@JsonProperty("titleSubtitle")
	private TitleSubtitle titleSubtitle;
	@JsonProperty("price")
	private Price price;
	@JsonProperty("rounding")
	private Rounding rounding;
	@JsonProperty("textTitle")
	private TextTitle textTitle;
	@JsonProperty("date")
	private Date date;
	@JsonProperty("time")
	private Time time;
	@JsonProperty("freeResource")
	private FreeResource freeResource;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("titleSubtitle")
	public TitleSubtitle getTitleSubtitle() {
		return titleSubtitle;
	}

	@JsonProperty("titleSubtitle")
	public void setTitleSubtitle(TitleSubtitle titleSubtitle) {
		this.titleSubtitle = titleSubtitle;
	}

	@JsonProperty("price")
	public Price getPrice() {
		return price;
	}

	@JsonProperty("price")
	public void setPrice(Price price) {
		this.price = price;
	}

	@JsonProperty("rounding")
	public Rounding getRounding() {
		return rounding;
	}

	@JsonProperty("rounding")
	public void setRounding(Rounding rounding) {
		this.rounding = rounding;
	}

	@JsonProperty("textTitle")
	public TextTitle getTextTitle() {
		return textTitle;
	}

	@JsonProperty("textTitle")
	public void setTextTitle(TextTitle textTitle) {
		this.textTitle = textTitle;
	}

	@JsonProperty("date")
	public Date getDate() {
		return date;
	}

	@JsonProperty("date")
	public void setDate(Date date) {
		this.date = date;
	}

	@JsonProperty("time")
	public Time getTime() {
		return time;
	}

	@JsonProperty("time")
	public void setTime(Time time) {
		this.time = time;
	}

	@JsonProperty("freeResource")
	public FreeResource getFreeResource() {
		return freeResource;
	}

	@JsonProperty("freeResource")
	public void setFreeResource(FreeResource freeResource) {
		this.freeResource = freeResource;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}
}
