
package com.evampsaanga.b2b.azerfon.magento.tariffdetails;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "freeResourceLabel",
    "freeResourceValue",
    "onnetFreeResourceLabel",
    "onnetFreeResourceValue",
    "descriptionFreeResource",
    "freeResourceIcon"
})
public class FreeResource {

    @JsonProperty("freeResourceLabel")
    private String freeResourceLabel;
    @JsonProperty("freeResourceValue")
    private String freeResourceValue;
    @JsonProperty("onnetFreeResourceLabel")
    private String onnetFreeResourceLabel;
    @JsonProperty("onnetFreeResourceValue")
    private String onnetFreeResourceValue;
    @JsonProperty("descriptionFreeResource")
    private String descriptionFreeResource;
    @JsonProperty("freeResourceIcon")
    private String freeResourceIcon;


	@JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("freeResourceLabel")
    public String getFreeResourceLabel() {
        return freeResourceLabel;
    }

    @JsonProperty("freeResourceLabel")
    public void setFreeResourceLabel(String freeResourceLabel) {
        this.freeResourceLabel = freeResourceLabel;
    }

    @JsonProperty("freeResourceValue")
    public String getFreeResourceValue() {
        return freeResourceValue;
    }

    @JsonProperty("freeResourceValue")
    public void setFreeResourceValue(String freeResourceValue) {
        this.freeResourceValue = freeResourceValue;
    }

    @JsonProperty("onnetFreeResourceLabel")
    public String getOnnetFreeResourceLabel() {
        return onnetFreeResourceLabel;
    }

    @JsonProperty("onnetFreeResourceLabel")
    public void setOnnetFreeResourceLabel(String onnetFreeResourceLabel) {
        this.onnetFreeResourceLabel = onnetFreeResourceLabel;
    }

    @JsonProperty("onnetFreeResourceValue")
    public String getOnnetFreeResourceValue() {
        return onnetFreeResourceValue;
    }

    @JsonProperty("onnetFreeResourceValue")
    public void setOnnetFreeResourceValue(String onnetFreeResourceValue) {
        this.onnetFreeResourceValue = onnetFreeResourceValue;
    }

    @JsonProperty("descriptionFreeResource")
    public String getDescriptionFreeResource() {
        return descriptionFreeResource;
    }

    @JsonProperty("descriptionFreeResource")
    public void setDescriptionFreeResource(String descriptionFreeResource) {
        this.descriptionFreeResource = descriptionFreeResource;
    }

    @JsonProperty("freeResourceIcon")
    public String getFreeResourceIcon() {
		return freeResourceIcon;
	}
    @JsonProperty("freeResourceIcon")
	public void setFreeResourceIcon(String freeResourceIcon) {
		this.freeResourceIcon = freeResourceIcon;
	}
    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
