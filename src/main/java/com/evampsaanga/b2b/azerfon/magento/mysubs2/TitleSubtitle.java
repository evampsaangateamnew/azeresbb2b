package com.evampsaanga.b2b.azerfon.magento.mysubs2;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "titleLabel", "subtitleAIcon", "subtitleALabel", "subtitleAValue", "subtitleADescription",
		"subtitleBIcon", "subtitleBLabel", "subtitleBValue", "subtitleBDescription", "subtitleCIcon", "subtitleCLabel",
		"subtitleCValue", "subtitleCDescription", "subtitleDIcon", "subtitleDLabel", "subtitleDvalue",
		"subtitleDDescription" })
public class TitleSubtitle {
	@JsonProperty("titleLabel")
	private String titleLabel;
	@JsonProperty("subtitleAIcon")
	private String subtitleAIcon;
	@JsonProperty("subtitleALabel")
	private String subtitleALabel;
	@JsonProperty("subtitleAValue")
	private String subtitleAValue;
	@JsonProperty("subtitleADescription")
	private String subtitleADescription;
	@JsonProperty("subtitleBIcon")
	private String subtitleBIcon;
	@JsonProperty("subtitleBLabel")
	private String subtitleBLabel;
	@JsonProperty("subtitleBValue")
	private String subtitleBValue;
	@JsonProperty("subtitleBDescription")
	private String subtitleBDescription;
	@JsonProperty("subtitleCIcon")
	private String subtitleCIcon;
	@JsonProperty("subtitleCLabel")
	private String subtitleCLabel;
	@JsonProperty("subtitleCValue")
	private String subtitleCValue;
	@JsonProperty("subtitleCDescription")
	private String subtitleCDescription;
	@JsonProperty("subtitleDIcon")
	private String subtitleDIcon;
	@JsonProperty("subtitleDLabel")
	private String subtitleDLabel;
	@JsonProperty("subtitleDvalue")
	private String subtitleDvalue;
	@JsonProperty("subtitleDDescription")
	private String subtitleDDescription;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("titleLabel")
	public String getTitleLabel() {
		return titleLabel;
	}

	@JsonProperty("titleLabel")
	public void setTitleLabel(String titleLabel) {
		this.titleLabel = titleLabel;
	}

	@JsonProperty("subtitleAIcon")
	public String getSubtitleAIcon() {
		return subtitleAIcon;
	}

	@JsonProperty("subtitleAIcon")
	public void setSubtitleAIcon(String subtitleAIcon) {
		this.subtitleAIcon = subtitleAIcon;
	}

	@JsonProperty("subtitleALabel")
	public String getSubtitleALabel() {
		return subtitleALabel;
	}

	@JsonProperty("subtitleALabel")
	public void setSubtitleALabel(String subtitleALabel) {
		this.subtitleALabel = subtitleALabel;
	}

	@JsonProperty("subtitleAValue")
	public String getSubtitleAValue() {
		return subtitleAValue;
	}

	@JsonProperty("subtitleAValue")
	public void setSubtitleAValue(String subtitleAValue) {
		this.subtitleAValue = subtitleAValue;
	}

	@JsonProperty("subtitleADescription")
	public String getSubtitleADescription() {
		return subtitleADescription;
	}

	@JsonProperty("subtitleADescription")
	public void setSubtitleADescription(String subtitleADescription) {
		this.subtitleADescription = subtitleADescription;
	}

	@JsonProperty("subtitleBIcon")
	public String getSubtitleBIcon() {
		return subtitleBIcon;
	}

	@JsonProperty("subtitleBIcon")
	public void setSubtitleBIcon(String subtitleBIcon) {
		this.subtitleBIcon = subtitleBIcon;
	}

	@JsonProperty("subtitleBLabel")
	public String getSubtitleBLabel() {
		return subtitleBLabel;
	}

	@JsonProperty("subtitleBLabel")
	public void setSubtitleBLabel(String subtitleBLabel) {
		this.subtitleBLabel = subtitleBLabel;
	}

	@JsonProperty("subtitleBValue")
	public String getSubtitleBValue() {
		return subtitleBValue;
	}

	@JsonProperty("subtitleBValue")
	public void setSubtitleBValue(String subtitleBValue) {
		this.subtitleBValue = subtitleBValue;
	}

	@JsonProperty("subtitleBDescription")
	public String getSubtitleBDescription() {
		return subtitleBDescription;
	}

	@JsonProperty("subtitleBDescription")
	public void setSubtitleBDescription(String subtitleBDescription) {
		this.subtitleBDescription = subtitleBDescription;
	}

	@JsonProperty("subtitleCIcon")
	public String getSubtitleCIcon() {
		return subtitleCIcon;
	}

	@JsonProperty("subtitleCIcon")
	public void setSubtitleCIcon(String subtitleCIcon) {
		this.subtitleCIcon = subtitleCIcon;
	}

	@JsonProperty("subtitleCLabel")
	public String getSubtitleCLabel() {
		return subtitleCLabel;
	}

	@JsonProperty("subtitleCLabel")
	public void setSubtitleCLabel(String subtitleCLabel) {
		this.subtitleCLabel = subtitleCLabel;
	}

	@JsonProperty("subtitleCValue")
	public String getSubtitleCValue() {
		return subtitleCValue;
	}

	@JsonProperty("subtitleCValue")
	public void setSubtitleCValue(String subtitleCValue) {
		this.subtitleCValue = subtitleCValue;
	}

	@JsonProperty("subtitleCDescription")
	public String getSubtitleCDescription() {
		return subtitleCDescription;
	}

	@JsonProperty("subtitleCDescription")
	public void setSubtitleCDescription(String subtitleCDescription) {
		this.subtitleCDescription = subtitleCDescription;
	}

	@JsonProperty("subtitleDIcon")
	public String getSubtitleDIcon() {
		return subtitleDIcon;
	}

	@JsonProperty("subtitleDIcon")
	public void setSubtitleDIcon(String subtitleDIcon) {
		this.subtitleDIcon = subtitleDIcon;
	}

	@JsonProperty("subtitleDLabel")
	public String getSubtitleDLabel() {
		return subtitleDLabel;
	}

	@JsonProperty("subtitleDLabel")
	public void setSubtitleDLabel(String subtitleDLabel) {
		this.subtitleDLabel = subtitleDLabel;
	}

	@JsonProperty("subtitleDvalue")
	public String getSubtitleDvalue() {
		return subtitleDvalue;
	}

	@JsonProperty("subtitleDvalue")
	public void setSubtitleDvalue(String subtitleDvalue) {
		this.subtitleDvalue = subtitleDvalue;
	}

	@JsonProperty("subtitleDDescription")
	public String getSubtitleDDescription() {
		return subtitleDDescription;
	}

	@JsonProperty("subtitleDDescription")
	public void setSubtitleDDescription(String subtitleDDescription) {
		this.subtitleDDescription = subtitleDDescription;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}
}
