package com.evampsaanga.b2b.azerfon.topup;

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;

public class TopupRequestClient extends BaseRequest {
	String customerType = "";
	String cardPinNumber = "";
	String topupnum = "";

	public String getTopupnum() {
		return topupnum;
	}

	public void setTopupnum(String topupnum) {
		this.topupnum = topupnum;
	}

	public String getCardPinNumber() {
		return cardPinNumber;
	}

	public void setCardPinNumber(String cardPinNumber) {
		this.cardPinNumber = cardPinNumber;
	}

	/**
	 * @return the customerType
	 */
	public String getCustomerType() {
		return customerType;
	}

	/**
	 * @param customerType
	 *            the customerType to set
	 */
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
}
