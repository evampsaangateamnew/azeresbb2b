package com.evampsaanga.b2b.azerfon.manipulatefnf;

import java.util.ArrayList;

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;

public class ManipulatefnfClientRequest extends BaseRequest {

	private String actionType = "";
	private String primaryOfferingId = "";
	private String offeringId = "";
	private String fnfNumber = "";
	private ArrayList<String> deleteFnf;
	private String newMsisdn = "";
	private String isFirstTime = "";

	public String getIsFirstTime() {
		return isFirstTime;
	}

	public void setIsFirstTime(String isFirstTime) {
		this.isFirstTime = isFirstTime;
	}

	public String getNewMsisdn() {
		return newMsisdn;
	}

	public void setNewMsisdn(String newMsisdn) {
		this.newMsisdn = newMsisdn;
	}

	public ArrayList<String> getDeleteFnf() {
		return deleteFnf;
	}

	public void setDeleteFnf(ArrayList<String> deleteFnf) {
		this.deleteFnf = deleteFnf;
	}

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public String getPrimaryOfferingId() {
		return primaryOfferingId;
	}

	public void setPrimaryOfferingId(String primaryOfferingId) {
		this.primaryOfferingId = primaryOfferingId;
	}

	public String getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(String offeringId) {
		this.offeringId = offeringId;
	}

	public String getFnfNumber() {
		return fnfNumber;
	}

	public void setFnfNumber(String fnfNumber) {
		this.fnfNumber = fnfNumber;
	}

}
