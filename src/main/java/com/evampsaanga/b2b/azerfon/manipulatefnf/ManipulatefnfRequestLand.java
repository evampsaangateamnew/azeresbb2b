package com.evampsaanga.b2b.azerfon.manipulatefnf;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.amqimplementationsesb.LogsType;
import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.ThirdPartyNames;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Decrypter;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.evampsaanga.b2b.developer.utils.SOAPLoggingHandler;
import com.huawei.crm.basetype.ens.ExtProductList;
import com.huawei.crm.basetype.ens.OfferingExtParameterInfo;
import com.huawei.crm.basetype.ens.OfferingExtParameterList;
import com.huawei.crm.basetype.ens.OfferingInfo;
import com.huawei.crm.basetype.ens.OfferingKey;
import com.huawei.crm.basetype.ens.OfferingList;
import com.huawei.crm.basetype.ens.OrderInfo;
import com.huawei.crm.basetype.ens.OrderItemInfo;
import com.huawei.crm.basetype.ens.OrderItemValue;
import com.huawei.crm.basetype.ens.OrderItems;
import com.huawei.crm.basetype.ens.ProductInfo;
import com.huawei.crm.basetype.ens.RequestHeader;
import com.huawei.crm.basetype.ens.SubscriberInfo;
import com.huawei.crm.service.ens.SubmitOrderRequest;
import com.huawei.crm.service.ens.SubmitOrderResponse;
import com.huawei.crm.service.ens.SubmitRequestBody;
import com.ngbss.evampsaanga.services.ThirdPartyCall;
import com.ngbss.evampsaanga.services.ThirdPartyRequestHeader;

@Path("/azerfon")
public class ManipulatefnfRequestLand {
	public static final Logger logger = Logger.getLogger("azerfon-esb");

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ManipulatefnfClientResponse Get(@Header("credentials") String credential,
			@Header("Content-Type") String contentType, @Body() String requestBody) {

		ManipulatefnfClientResponse resp = new ManipulatefnfClientResponse();
		Logs logs = new Logs();
		try {

			String transactionName = Transactions.MANIPULATE_FNF_TRANSACTION_NAME;
			String token = Helper.retrieveToken(transactionName, Helper.getValueFromJSON(requestBody, "msisdn"));

			Constants.logger.info(token + "-Request Landed In-" + transactionName);
			Constants.logger.info(token + "-Request Data-" + requestBody);

			logs.setTransactionName(Transactions.MANIPULATE_FNF_TRANSACTION_NAME);
			logs.setThirdPartyName(ThirdPartyNames.MANIPULATE_FNF);
			logs.setTableType(LogsType.ManipulateFnF);

			ManipulatefnfClientRequest cclient = new ManipulatefnfClientRequest();

			try {

				cclient = Helper.JsonToObject(requestBody, ManipulatefnfClientRequest.class);
				cclient.setPrimaryOfferingId(getFNFSupplementary(cclient.getOfferingId()));

				if (cclient != null) {

					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
					logs.setLang(cclient.getLang());
				}

			} catch (Exception ex) {

				logger.error(Helper.GetException(ex));
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}

			if (cclient != null) {

				String credentials = null;

				try {

					credentials = Decrypter.getInstance().decrypt(credential);

				} catch (Exception ex) {

					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				}

				if (credentials == null) {

					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}

				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {

					if (!cclient.getFnfNumber().equals("")) {

						cclient.setFnfNumber(Constants.AZERI_COUNTRY_CODE + cclient.getFnfNumber());
					}

					if (cclient.getNewMsisdn() != null && !cclient.getNewMsisdn().equals("")
							&& !cclient.getFnfNumber().equals("")) {

						com.huawei.crm.service.ens.SubmitOrderResponse response = updatemanipulateFNF(token,
								cclient.getFnfNumber(), cclient.getNewMsisdn(), cclient.getmsisdn(),
								cclient.getPrimaryOfferingId());

						if (response.getResponseHeader().getRetCode().equals("0")) {

							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);

						} else {

							resp.setReturnCode(response.getResponseHeader().getRetCode());
							resp.setReturnMsg(response.getResponseHeader().getRetMsg());
						}
					}

					if (cclient.getNewMsisdn().equals("") && cclient.getActionType().equals("")
							&& !cclient.getFnfNumber().equals("")) {

						if (cclient.getIsFirstTime().equals("true")) {
							com.huawei.crm.service.ens.SubmitOrderResponse response = addmanipulateFNFfirstTime(token,
									cclient.getFnfNumber(), cclient.getmsisdn(), cclient.getPrimaryOfferingId());

							if (response.getResponseHeader().getRetCode().equals("0")) {

								resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
								resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);

							} else {

								resp.setReturnCode(response.getResponseHeader().getRetCode());
								resp.setReturnMsg(response.getResponseHeader().getRetMsg());
							}
						} else {

							com.huawei.crm.service.ens.SubmitOrderResponse response = addmanipulateFNF(token,
									cclient.getFnfNumber(), cclient.getmsisdn(), cclient.getPrimaryOfferingId());

							if (response.getResponseHeader().getRetCode().equals("0")) {

								resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
								resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);

							} else {

								resp.setReturnCode(response.getResponseHeader().getRetCode());
								resp.setReturnMsg(response.getResponseHeader().getRetMsg());
							}
						}

					}

//					if (!cclient.getActionType().equals("")) {
//						String fnfnumber = cclient.getActionType().substring(0, 3);
//						if (!fnfnumber.equals(Constants.AZERI_COUNTRY_CODE)) {
//							cclient.setActionType(Constants.AZERI_COUNTRY_CODE + cclient.getActionType());
//						}
//					}

					if (!cclient.getActionType().isEmpty() && cclient.getFnfNumber().isEmpty()) 
					{
						
//						com.huawei.crm.service.SubmitOrderResponse response = deletemanipulateFNF(
//								cclient.getActionType(), cclient.getmsisdn(), cclient.getPrimaryOfferingId(),cclient.getDeleteFnf());
						SubmitOrderResponse response = changeSupplimentaryOffers(cclient.getmsisdn(), "3");
						if (response.getResponseHeader().getRetCode().equals("0")) 
						{
							
								resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
								resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
							
							
						} else {

							resp.setReturnCode(response.getResponseHeader().getRetCode());
							resp.setReturnMsg(response.getResponseHeader().getRetMsg());
						}
					}

					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);

					Constants.logger.info(
							token + "-Response Returned From-" + transactionName + "-" + Helper.ObjectToJson(resp));

					return resp;

				} else {

					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {

			logger.error(Helper.GetException(ex));
		}

		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(resp.getReturnCode());
		logs.setResponseDescription(resp.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	public SubmitOrderResponse addmanipulateFNF(String token, String fnfnumber,
			String msisdn, String supOfferingId) {

		logger.info(token + "In addmanipulateFNF Function");

		SubmitOrderRequest submitOrderRequestMsgReq = new SubmitOrderRequest();
//		submitOrderRequestMsgReq.setRequestHeader(OrderHandleService.getReqHeaderForManipulateFnF());
		submitOrderRequestMsgReq.setRequestHeader(ThirdPartyRequestHeader.getReqHeaderForManipulateFnF());
		OfferingInfo offeringInfo = new OfferingInfo();
		offeringInfo.setActionType(Constants.FNF_ACTION_TYPE);
		SubmitRequestBody submitRequestBody = new SubmitRequestBody();
		OrderInfo oInfo = new OrderInfo();
		oInfo.setOrderType(Constants.FNF_ORDER_ITEM);
		OrderItemInfo oitemInfo = new OrderItemInfo();
		oitemInfo.setOrderItemType(Constants.FNF_ORDER_ITEM_TYPE);
		submitRequestBody.setOrder(oInfo);
		OrderItems OrderItems = new OrderItems();
		OrderItemValue OrderItemValue = new OrderItemValue();
		OrderItemValue.setOrderItemInfo(oitemInfo);
		SubscriberInfo subscriber = new SubscriberInfo();
		subscriber.setServiceNumber(msisdn);
		OfferingKey offeringId = new OfferingKey();
		offeringId.setOfferingId("566577110");
		offeringInfo.setOfferingId(offeringId);
		ProductInfo productInfo = new ProductInfo();
		productInfo.setProductId(Constants.FNF_PODUCT_ID);
		productInfo.setSelectFlag(Constants.FNF_SELECT_FLAG);
		OfferingExtParameterInfo offeringExtPaInfo = new OfferingExtParameterInfo();
		offeringExtPaInfo.setParamName(Constants.FNF_INFO);
		offeringExtPaInfo.setParamValue(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
		OfferingExtParameterList offeringExtParamList = new OfferingExtParameterList();
		OfferingExtParameterInfo param1 = new OfferingExtParameterInfo();
		param1.setParamName(Constants.FNF_SERIAL_NUMBER);
		param1.setParamValue(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
		offeringExtParamList.getParameterInfo().add(param1);
		OfferingExtParameterInfo param2 = new OfferingExtParameterInfo();
		param2.setParamName(Constants.FNF_NUMBER);
		param2.setParamValue(fnfnumber);
		offeringExtParamList.getParameterInfo().add(param2);
		offeringExtPaInfo.setExtParamList(offeringExtParamList);
		productInfo.getExtParamList().getParameterInfo().add(offeringExtPaInfo);
		offeringInfo.getProductList().getProductInfo().add(productInfo);
		subscriber.getSupplementaryOfferingList().getOfferingInfo().add(offeringInfo);
		OrderItemValue.setSubscriber(subscriber);
		OrderItems.getOrderItem().add(OrderItemValue);
		submitRequestBody.setOrderItems(OrderItems);
		submitOrderRequestMsgReq.setSubmitRequestBody(submitRequestBody);
//		com.huawei.crm.service.SubmitOrderResponse response = OrderHandleService.getInstance().submitOrder(submitOrderRequestMsgReq);
		SubmitOrderResponse response = ThirdPartyCall.submitOrder(submitOrderRequestMsgReq);
		return response;
	}

	public com.huawei.crm.service.ens.SubmitOrderResponse addmanipulateFNFfirstTime(String token, String fnfnumber,
			String msisdn, String supOfferingId) {

		logger.info(token + "In addmanipulateFNFfirstTime Function");

		SubmitOrderRequest submitOrderRequestMsgReq = new SubmitOrderRequest();
//		submitOrderRequestMsgReq.setRequestHeader(OrderHandleService.getReqHeaderForManipulateFnF());
		submitOrderRequestMsgReq.setRequestHeader(ThirdPartyRequestHeader.getReqHeaderForManipulateFnF());

		SubmitRequestBody submitRequestBody = new SubmitRequestBody();

		OrderInfo orderInfo = new OrderInfo();
		orderInfo.setOrderType("AZC10");
		submitRequestBody.setOrder(orderInfo);
		OrderItems orderItems = new OrderItems();
		OrderItemValue orderItemValue = new OrderItemValue();

		OrderItemInfo orderItemInfo = new OrderItemInfo();
		orderItemInfo.setOrderItemType("CO025");
		orderItemValue.setOrderItemInfo(orderItemInfo);
		SubscriberInfo subscriberInfo = new SubscriberInfo();
		subscriberInfo.setServiceNumber(msisdn);
		OfferingList offeringList = new OfferingList();
		OfferingInfo offeringInfo = new OfferingInfo();
		offeringInfo.setActionType("1");
		OfferingKey offeringKey = new OfferingKey();
		offeringKey.setOfferingId("566577110");
		offeringInfo.setOfferingId(offeringKey);
		offeringList.getOfferingInfo().add(offeringInfo);
		subscriberInfo.setSupplementaryOfferingList(offeringList);
		orderItemValue.setSubscriber(subscriberInfo);
		orderItems.getOrderItem().add(orderItemValue);

		OrderItemValue orderItemValue2 = new OrderItemValue();
		OrderItemInfo orderItemInfo2 = new OrderItemInfo();
		orderItemInfo2.setOrderItemType("CO075");

		orderItemValue2.setOrderItemInfo(orderItemInfo2);
		SubscriberInfo subscriberInfo2 = new SubscriberInfo();
		subscriberInfo2.setServiceNumber(msisdn);
		OfferingList offeringList2 = new OfferingList();
		OfferingInfo offeringInfo2 = new OfferingInfo();
		offeringInfo2.setActionType("2");
		OfferingKey offeringKey2 = new OfferingKey();
		offeringKey2.setOfferingId("566577110");
		offeringInfo2.setOfferingId(offeringKey2);
		ExtProductList extProductList = new ExtProductList();
		ProductInfo productInfo = new ProductInfo();
		productInfo.setProductId("1023");
		productInfo.setSelectFlag("1");
		OfferingExtParameterInfo offeringExtParameterInfo = new OfferingExtParameterInfo();
		offeringExtParameterInfo.setParamName("C_FNINFO");
//		offeringExtParameterInfo.setParamCode("C_FNINFO");
		offeringExtParameterInfo.setParamValue(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
		OfferingExtParameterList innerOfferingExtParameterList = new OfferingExtParameterList();
		OfferingExtParameterInfo innerOfferingExtParameterInfo1 = new OfferingExtParameterInfo();
		innerOfferingExtParameterInfo1.setParamName("C_FN_SERIAL_NO");
		innerOfferingExtParameterInfo1.setParamValue(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
		OfferingExtParameterInfo innerOfferingExtParameterInfo2 = new OfferingExtParameterInfo();
		innerOfferingExtParameterInfo2.setParamName("C_FN_NUMBER");
		innerOfferingExtParameterInfo2.setParamValue(fnfnumber);
		innerOfferingExtParameterList.getParameterInfo().add(innerOfferingExtParameterInfo1);
		innerOfferingExtParameterList.getParameterInfo().add(innerOfferingExtParameterInfo2);
		offeringExtParameterInfo.setExtParamList(innerOfferingExtParameterList);
		productInfo.getExtParamList().getParameterInfo().add(offeringExtParameterInfo);
		extProductList.getProductInfo().add(productInfo);
		offeringInfo2.setProductList(extProductList);
		offeringList2.getOfferingInfo().add(offeringInfo2);
		subscriberInfo2.setSupplementaryOfferingList(offeringList2);
		orderItemValue2.setSubscriber(subscriberInfo2);
		orderItems.getOrderItem().add(orderItemValue2);
		submitRequestBody.setOrderItems(orderItems);
		submitOrderRequestMsgReq.setSubmitRequestBody(submitRequestBody);
//		com.huawei.crm.service.SubmitOrderResponse response = OrderHandleService.getInstance()
//				.submitOrder(submitOrderRequestMsgReq);
		com.huawei.crm.service.ens.SubmitOrderResponse response = ThirdPartyCall.submitOrder(submitOrderRequestMsgReq);
		return response;
	}

//	private static void deleteFnF() 
//	{
//		// TODO Auto-generated method stub
//		System.out.println("***********************");
//        System.out.println("Create Web Service Client...");
//        System.out.println("***********************");
//        System.out.println("  Delete Fnf  ");
//        System.out.println("***********************");
//		
//        SubmitOrderRequest submitOrderRequestMsgReq = new SubmitOrderRequest();
//		submitOrderRequestMsgReq.setRequestHeader(SubmitOrderFactory.getReqHeaderForManipulateFnF());
//		OfferingInfo offeringInfo = new OfferingInfo();
//		offeringInfo.setActionType("2");
//		SubmitRequestBody submitRequestBody = new SubmitRequestBody();
//		OrderInfo oInfo = new OrderInfo();
//		oInfo.setOrderType("CO075");
//		OrderItemInfo oitemInfo = new OrderItemInfo();
//		oitemInfo.setOrderItemType("CO075");
//		submitRequestBody.setOrder(oInfo);
//		OrderItems OrderItems = new OrderItems();
//		OrderItemValue OrderItemValue = new OrderItemValue();
//		OrderItemValue.setOrderItemInfo(oitemInfo);
//		SubscriberInfo subscriber = new SubscriberInfo();
//		subscriber.setServiceNumber("776480587");
//		OfferingKey offeringId = new OfferingKey();
//		offeringId.setOfferingId("566577110");
//		offeringInfo.setOfferingId(offeringId);
//		ProductInfo productInfo = new ProductInfo();
//		productInfo.setProductId("1023");
//		productInfo.setSelectFlag("1");
//		OfferingExtParameterInfo offeringExtPaInfo = new OfferingExtParameterInfo();
//		offeringExtPaInfo.setParamName("C_FNINFO");
//		offeringExtPaInfo.setParamValue(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
//		OfferingExtParameterList offeringExtParamList = new OfferingExtParameterList();
//		OfferingExtParameterInfo param1 = new OfferingExtParameterInfo();
//		param1.setParamName("C_FN_SERIAL_NO");
//		param1.setParamValue(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
//		offeringExtParamList.getParameterInfo().add(param1);
//		OfferingExtParameterInfo param2 = new OfferingExtParameterInfo();
//		param2.setParamName("C_FN_NUMBER");
//		param2.setParamOldValue("994776480590");
//		offeringExtParamList.getParameterInfo().add(param2);
//		OfferingExtParameterInfo param3 = new OfferingExtParameterInfo();
//		param2.setParamName("C_FN_NUMBER");
//		param2.setParamOldValue("994776480544");
//		offeringExtParamList.getParameterInfo().add(param3);
//		offeringExtPaInfo.setExtParamList(offeringExtParamList);
//		productInfo.getExtParamList().getParameterInfo().add(offeringExtPaInfo);
//		offeringInfo.getProductList().getProductInfo().add(productInfo);
//		subscriber.getSupplementaryOfferingList().getOfferingInfo().add(offeringInfo);
//		OrderItemValue.setSubscriber(subscriber);
//		OrderItems.getOrderItem().add(OrderItemValue);
//		submitRequestBody.setOrderItems(OrderItems);
//		submitOrderRequestMsgReq.setSubmitRequestBody(submitRequestBody);
//		SubmitOrderResponse response = SubmitOrderFactory.getInstance().submitOrder(submitOrderRequestMsgReq);
//        
//        System.out.println("***********************");
//        
//		
//	}
//	

	public com.huawei.crm.service.ens.SubmitOrderResponse deletemanipulateFNF(String action, String msisdn,
			String supOfferingId, ArrayList<String> deleteFnf)
	{
		logger.info("Delete All FNF");

		SubmitOrderRequest submitOrderRequestMsgReq = new SubmitOrderRequest();
//		submitOrderRequestMsgReq.setRequestHeader(OrderHandleService.getReqHeaderForManipulateFnF());
		submitOrderRequestMsgReq.setRequestHeader(ThirdPartyRequestHeader.getReqHeaderForManipulateFnF());
		OfferingInfo offeringInfo = new OfferingInfo();
		offeringInfo.setActionType(Constants.FNF_ACTION_TYPE);
		SubmitRequestBody submitRequestBody = new SubmitRequestBody();
		OrderInfo oInfo = new OrderInfo();
		oInfo.setOrderType(Constants.FNF_ORDER_ITEM);
		OrderItemInfo oitemInfo = new OrderItemInfo();
		oitemInfo.setOrderItemType(Constants.FNF_ORDER_ITEM_TYPE);
		submitRequestBody.setOrder(oInfo);
		OrderItems OrderItems = new OrderItems();
		OrderItemValue OrderItemValue = new OrderItemValue();
		OrderItemValue.setOrderItemInfo(oitemInfo);
		SubscriberInfo subscriber = new SubscriberInfo();
		subscriber.setServiceNumber(msisdn);
		OfferingKey offeringId = new OfferingKey();
		offeringId.setOfferingId(supOfferingId);
		offeringInfo.setOfferingId(offeringId);
		ProductInfo productInfo = new ProductInfo();
		productInfo.setProductId(Constants.FNF_PODUCT_ID);
		productInfo.setSelectFlag(Constants.FNF_SELECT_FLAG);
		OfferingExtParameterInfo offeringExtPaInfo = new OfferingExtParameterInfo();
		offeringExtPaInfo.setParamName(Constants.FNF_INFO);
		offeringExtPaInfo.setParamValue(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
		OfferingExtParameterList offeringExtParamList = new OfferingExtParameterList();
		OfferingExtParameterInfo param1 = new OfferingExtParameterInfo();
		param1.setParamName(Constants.FNF_SERIAL_NUMBER);
		param1.setParamValue(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
		offeringExtParamList.getParameterInfo().add(param1);

		for (int i = 0; i < deleteFnf.size(); i++) {

			OfferingExtParameterInfo param2 = new OfferingExtParameterInfo();
			param2.setParamName(Constants.FNF_NUMBER);
			param2.setParamOldValue(Constants.AZERI_COUNTRY_CODE + deleteFnf.get(i).toString());
			offeringExtParamList.getParameterInfo().add(param2);
		}

		offeringExtPaInfo.setExtParamList(offeringExtParamList);
		productInfo.getExtParamList().getParameterInfo().add(offeringExtPaInfo);
		offeringInfo.getProductList().getProductInfo().add(productInfo);
		subscriber.getSupplementaryOfferingList().getOfferingInfo().add(offeringInfo);
		OrderItemValue.setSubscriber(subscriber);
		OrderItems.getOrderItem().add(OrderItemValue);
		submitRequestBody.setOrderItems(OrderItems);
		submitOrderRequestMsgReq.setSubmitRequestBody(submitRequestBody);
//		com.huawei.crm.service.SubmitOrderResponse response = OrderHandleService.getInstance()
//				.submitOrder(submitOrderRequestMsgReq);
		com.huawei.crm.service.ens.SubmitOrderResponse response = ThirdPartyCall.submitOrder(submitOrderRequestMsgReq);
		return response;
	}

	/**
	 * Method updates already added FNF (not used in this version)
	 * 
	 * @param action
	 * @param fnfnumber
	 * @param msidn
	 * @param string
	 * @return
	 */
	public com.huawei.crm.service.ens.SubmitOrderResponse updatemanipulateFNF(String token, String action,
			String fnfnumber, String msidn, String primaryOfferingId) {

		logger.info(token + "In updatemanipulateFNF Function");

		RequestHeader reqH = new RequestHeader();
		reqH.setVersion("1");
		reqH.setChannelId("3");
		reqH.setTechnicalChannelId("53");
		reqH.setAccessUser("ecare");
		reqH.setAccessPwd("Abc1234%");
		reqH.setTransactionId(Helper.generateTransactionID());
		reqH.setTenantId(ConfigurationManager.getConfigurationFromCache("crm.sub.TenantId").trim());
		SubmitOrderRequest submitOrderRequestMsgReq = new SubmitOrderRequest();
		submitOrderRequestMsgReq.setRequestHeader(reqH);
		OfferingInfo offeringInfo = new OfferingInfo();
		offeringInfo.setActionType("2");
		SubmitRequestBody submitRequestBody = new SubmitRequestBody();
		OrderInfo oInfo = new OrderInfo();
		oInfo.setOrderType("CO075");
		OrderItemInfo oitemInfo = new OrderItemInfo();
		oitemInfo.setOrderItemType("CO075");
		submitRequestBody.setOrder(oInfo);
		OrderItems OrderItems = new OrderItems();
		OrderItemValue OrderItemValue = new OrderItemValue();
		OrderItemValue.setOrderItemInfo(oitemInfo);
		SubscriberInfo subscriber = new SubscriberInfo();
		subscriber.setServiceNumber(msidn);
		OfferingKey offeringId = new OfferingKey();
		offeringId.setOfferingId("566577110");
		offeringInfo.setOfferingId(offeringId);
		ProductInfo productInfo = new ProductInfo();
		productInfo.setProductId("1023");
		productInfo.setSelectFlag("1");
		OfferingExtParameterInfo offeringExtPaInfo = new OfferingExtParameterInfo();
		offeringExtPaInfo.setParamName("C_FNINFO");
		offeringExtPaInfo.setParamValue("1525498661");
		OfferingExtParameterList offeringExtParamList = new OfferingExtParameterList();
		OfferingExtParameterInfo param1 = new OfferingExtParameterInfo();
		param1.setParamName("C_FN_SERIAL_NO");
		// param1.setParamValue(new
		// SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
		param1.setParamValue("1525498661");
		offeringExtParamList.getParameterInfo().add(param1);
		OfferingExtParameterInfo param2 = new OfferingExtParameterInfo();
		param2.setParamName("C_FN_NUMBER");
		param2.setParamValue(Constants.AZERI_COUNTRY_CODE + fnfnumber);
		param2.setParamOldValue(action);
		offeringExtParamList.getParameterInfo().add(param2);
		offeringExtPaInfo.setExtParamList(offeringExtParamList);
		productInfo.getExtParamList().getParameterInfo().add(offeringExtPaInfo);
		offeringInfo.getProductList().getProductInfo().add(productInfo);
		subscriber.getSupplementaryOfferingList().getOfferingInfo().add(offeringInfo);
		OrderItemValue.setSubscriber(subscriber);
		OrderItems.getOrderItem().add(OrderItemValue);
		submitRequestBody.setOrderItems(OrderItems);
		submitOrderRequestMsgReq.setSubmitRequestBody(submitRequestBody);
//		com.huawei.crm.service.SubmitOrderResponse response = OrderHandleService.getInstance()
//				.submitOrder(submitOrderRequestMsgReq);
		com.huawei.crm.service.ens.SubmitOrderResponse response = ThirdPartyCall.submitOrder(submitOrderRequestMsgReq);
		return response;
	}
	
	
	private static SubmitOrderResponse changeSupplimentaryOffers(String msisdn,String actionType) {
		// TODO Auto-generated method stub
		
		SubmitOrderRequest submitOrderRequestMsgReq = new SubmitOrderRequest();
		SubmitRequestBody submitrequestBody = new SubmitRequestBody();
		
		OrderInfo orderInfo = new OrderInfo();
		orderInfo.setOrderType("CO025");
		submitrequestBody.setOrder(orderInfo);
		OrderItems orderItems = new OrderItems();
		OrderItemValue orderItem = new OrderItemValue();
		OrderItemInfo orderItemInfo = new OrderItemInfo();
		orderItemInfo.setOrderItemType("CO025");
//		orderItemInfo.setIsCustomerNotification("0");
//		orderItemInfo.setIsPartnerNotification("0");
		orderItem.setOrderItemInfo(orderItemInfo);
		SubscriberInfo subscriberInfo = new SubscriberInfo();
		subscriberInfo.setServiceNumber(msisdn);
		OfferingInfo offeringInfo = new OfferingInfo();
		offeringInfo.setActionType(actionType);//1 for Add,2 for update,3 for delete
		OfferingKey offeringID = new OfferingKey();
		offeringID.setOfferingId("566577110");
		offeringInfo.setOfferingId(offeringID);
//		offeringInfo.setEffectMode("0");
//		offeringInfo.setActiveMode("A");
//		OfferingExtParameterList extParmList = new OfferingExtParameterList();
//		OfferingExtParameterInfo offertingExtparamInfo = new OfferingExtParameterInfo();
//		offertingExtparamInfo.setParamName("509703");
//		offertingExtparamInfo.setParamValue("776480535");
//		extParmList.getParameterInfo().add(offertingExtparamInfo);
//		offeringInfo.setExtParamList(extParmList);
		subscriberInfo.getSupplementaryOfferingList().getOfferingInfo().add(offeringInfo);
		orderItem.setSubscriber(subscriberInfo);
		orderItems.getOrderItem().add(orderItem);
		submitrequestBody.setOrderItems(orderItems);
		submitOrderRequestMsgReq.setSubmitRequestBody(submitrequestBody);
		
		
		
		RequestHeader reqH = new RequestHeader();
		reqH.setVersion("1");
		reqH.setChannelId("3");
		reqH.setTechnicalChannelId("53");
		reqH.setAccessUser("ecare");
		reqH.setAccessPwd("Abc1234%");
		reqH.setTransactionId(Helper.generateTransactionID());
		reqH.setTenantId(ConfigurationManager.getConfigurationFromCache("crm.sub.TenantId").trim());
		
		
		submitOrderRequestMsgReq.setRequestHeader(reqH);
		
		
		
//		com.huawei.crm.service.SubmitOrderResponse response = OrderHandleService.getInstance()
//				.submitOrder(submitOrderRequestMsgReq);
		com.huawei.crm.service.ens.SubmitOrderResponse response = ThirdPartyCall.submitOrder(submitOrderRequestMsgReq);
		return response;
	}

	public String getFNFSupplementary(String key) {
		String value = ConfigurationManager.getConfigurationFromCache("fnf.offering.id.limit." + key);
		if (value == null)
			value = "5";
		if (value != null && value.contains(","))
			return value.split(",")[1];
		return value;
	}
}
