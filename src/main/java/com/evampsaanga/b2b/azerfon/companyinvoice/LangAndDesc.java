package com.evampsaanga.b2b.azerfon.companyinvoice;

public class LangAndDesc {
	private String categoryId;
	private String langId;
	private String desc;

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getLangId() {
		return langId;
	}

	public void setLangId(String langId) {
		this.langId = langId;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	@Override
	public String toString() {
		return "LangAndDesc [categoryId=" + categoryId + ", langId=" + langId + ", desc=" + desc + "]";
	}

}
