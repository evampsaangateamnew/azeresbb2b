package com.evampsaanga.b2b.azerfon.companyinvoice;

import java.util.List;

public class CompanyInvoiceData {
	
	String label;
	String amount;
    private List<InvoiceModel> results = null;
    
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public List<InvoiceModel> getResults() {
		return results;
	}
	public void setResults(List<InvoiceModel> results) {
		this.results = results;
	}
}
