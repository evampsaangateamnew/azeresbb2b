package com.evampsaanga.b2b.azerfon.companyinvoice;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)

public class CompanyInvoiceResponse {

    @JsonProperty("returnCode")
    private String returnCode;
    @JsonProperty("returnMsg")
    private String returnMsg;
    @JsonProperty("data")
    private InvoiceData data;
    
    @JsonProperty("returnCode")
	public String getReturnCode() {
		return returnCode;
	}
    
    @JsonProperty("returnCode")
	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}
    
    @JsonProperty("returnMsg")
	public String getReturnMsg() {
		return returnMsg;
	}
    
    @JsonProperty("returnMsg")
	public void setReturnMsg(String returnMsg) {
		this.returnMsg = returnMsg;
	}

	public InvoiceData getData() {
		return data;
	}

	public void setData(InvoiceData data) {
		this.data = data;
	}	
}
