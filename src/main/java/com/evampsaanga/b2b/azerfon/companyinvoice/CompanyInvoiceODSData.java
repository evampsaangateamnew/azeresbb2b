package com.evampsaanga.b2b.azerfon.companyinvoice;

public class CompanyInvoiceODSData {
	private String categoryId;
	private String amount;

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "CompanyInvoiceODSData [categoryId=" + categoryId + ", amount=" + amount + "]";
	}

}
