/**
 * 
 */
package com.evampsaanga.b2b.azerfon.companyinvoice;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author HamzaFarooque
 *
 */
public class InvoiceData {
	
	@JsonProperty("exportLink")
	private String exportLink;
	
	@JsonProperty("invoiceData")
    private List<CompanyInvoiceData> invoiceData  = null;
	
	public String getExportLink() {
		return exportLink;
	}
	public void setExportLink(String exportLink) {
		this.exportLink = exportLink;
	}
	public List<CompanyInvoiceData> getInvoiceData() {
		return invoiceData;
	}
	public void setInvoiceData(List<CompanyInvoiceData> invoiceData) {
		this.invoiceData = invoiceData;
	}
	
}
