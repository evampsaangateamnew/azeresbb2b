package com.evampsaanga.b2b.azerfon.companyinvoice;

public class ODSData {
	private String categoryId;
	private String amount;

	private String tariffName;

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getTariffName() {
		return tariffName;
	}

	public void setTariffName(String tariffName) {
		this.tariffName = tariffName;
	}

	@Override
	public String toString() {
		return "ODSData [categoryId=" + categoryId + ", amount=" + amount + ", tariffName=" + tariffName + "]";
	}

}
