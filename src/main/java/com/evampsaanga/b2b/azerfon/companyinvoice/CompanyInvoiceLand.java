package com.evampsaanga.b2b.azerfon.companyinvoice;

import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.amqimplementationsesb.LogsType;
import com.evampsaanga.b2b.azerfon.db.DBAzerfonFactory;
import com.evampsaanga.b2b.azerfon.pdf.Pdf;
import com.evampsaanga.b2b.azerfon.pdf.PdfToEmailLand;
import com.evampsaanga.b2b.azerfon.sendemail.SendEmailResponse;
import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.ThirdPartyNames;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Decrypter;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.evampsaanga.b2b.developer.utils.SOAPLoggingHandler;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Path("/azerfon")
public class CompanyInvoiceLand {

	public static final Logger loggerV2 = Logger.getLogger("azerfon-esb");
	public static String modeulName = "COMPANY_INVOICE";

	@POST
	@Path("/getcompanyinvoice")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public CompanyInvoiceResponse getSummary(@Body String requestBody, @Header("credentials") String credential) {
		logMessage("***************************** Company Invoice Summary **********************************");
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.COMPANY_INVOICE);
		logs.setThirdPartyName(ThirdPartyNames.ODS);
		logs.setTableType(LogsType.CompanyInvoice);
		CompanyInvoiceRequest cclient = null;
		CompanyInvoiceResponse resp = new CompanyInvoiceResponse();

		String token = "";
		String TrnsactionName = Transactions.COMPANY_INVOICE;

		try {

			cclient = Helper.JsonToObject(requestBody, CompanyInvoiceRequest.class);
			token = Helper.retrieveToken(TrnsactionName, Helper.getValueFromJSON(requestBody, "msisdn"));
			Helper.logInfoMessageV2(token + "-Request Data-" + requestBody);

			logMessage(cclient.getmsisdn() + "-REQUEST: " + Helper.ObjectToJson(cclient));
			if (cclient != null) {
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				logs.setLang(cclient.getLang());
			}
		} catch (Exception ex) {
			loggerV2.info(Helper.GetException(ex));
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_401);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		if (cclient != null) {
			String credentials = null;
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
			} catch (Exception ex) {
				SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials == null) {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				try {

					if (cclient.getIsSendMail().equalsIgnoreCase("true")) { // if
																			// send
																			// mail
																			// is
																			// true

						if (!cclient.getMailTo().isEmpty()) { // check if email
																// is given

							String path = ConfigurationManager
									.getConfigurationFromCache("invoice.details.pdfLocalPath");
							String ext = Constants.PDF_EXT;
							// String datetime = Helper.getPdfTimeStamp();
							String filename = cclient.getmsisdn() + Constants.PDF_EXT;
							String pdfPath = path + filename;

							SendEmailResponse emailResponse = new PdfToEmailLand().getResponse(
									ConfigurationManager
									.getConfigurationFromCache("invoice.details.company.invoice.email.subject."+cclient.getLang()), cclient.getMailTo(), pdfPath, filename, loggerV2, token);
							resp.setReturnCode(emailResponse.getReturnCode());
							resp.setReturnMsg(emailResponse.getReturnMsg());

							return resp;

						} else { // if email is not given,this will return error
									// reponse

							resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
							resp.setReturnMsg(ConfigurationManager
									.getConfigurationFromCache("actionhistory.generic.failure."+cclient.getLang()));
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} else {

						logMessage(cclient.getmsisdn() + "=============Preparing Company Invoice =================");
						HashMap<String, LangAndDesc> listLangDesc = getCategoryWithLangAndDescFromODS(
								cclient.getmsisdn(), getLangMap(cclient.getLang()));
						HashMap<String, ODSData> hashmapODS_Data = getCompanyInvoiceFromODS(cclient, listLangDesc);

						InvoiceData data = new InvoiceData();
						List<CompanyInvoiceData> companyInvoiceData = new ArrayList<CompanyInvoiceData>();
						CompanyInvoiceData invoiceData = new CompanyInvoiceData();
						List<InvoiceModel> results = new ArrayList<InvoiceModel>();
						InvoiceModel invoiceModel = new InvoiceModel();
						if (listLangDesc.get(
								ConfigurationManager.getConfigurationFromCache("invoice.details.header.id.1")) != null
								&& hashmapODS_Data.get(ConfigurationManager
										.getConfigurationFromCache("invoice.details.header.id.1")) != null) {
							invoiceData.setLabel(listLangDesc
									.get(ConfigurationManager.getConfigurationFromCache("invoice.details.header.id.1"))
									.getDesc());
							invoiceData.setAmount(FormatValue(hashmapODS_Data
									.get(ConfigurationManager.getConfigurationFromCache("invoice.details.header.id.1"))
									.getAmount()));

						} else {

							invoiceData.setLabel(ConfigurationManager
									.getConfigurationFromCache("invoice.details.header.id.1." + cclient.getLang()));
							invoiceData.setAmount("0.00");

						}
						companyInvoiceData.add(invoiceData);
						invoiceData = new CompanyInvoiceData();
						if (listLangDesc.get(
								ConfigurationManager.getConfigurationFromCache("invoice.details.header.id.2")) != null
								&& hashmapODS_Data.get(ConfigurationManager
										.getConfigurationFromCache("invoice.details.header.id.2")) != null) {
							invoiceData.setLabel(listLangDesc
									.get(ConfigurationManager.getConfigurationFromCache("invoice.details.header.id.2"))
									.getDesc());
							invoiceData.setAmount(FormatValue(hashmapODS_Data
									.get(ConfigurationManager.getConfigurationFromCache("invoice.details.header.id.2"))
									.getAmount()));

						} else {

							invoiceData.setLabel(ConfigurationManager
									.getConfigurationFromCache("invoice.details.header.id.2." + cclient.getLang()));
							invoiceData.setAmount("0.00");

						}
						String[] arrSecondHeadingSplitup = ConfigurationManager
								.getConfigurationFromCache("invoice.details.header.id.2.splitup").split(",");
						for (int i = 0; i < arrSecondHeadingSplitup.length; i++) {
							if (listLangDesc.get(arrSecondHeadingSplitup[i]).getDesc() != null
									&& hashmapODS_Data.get(arrSecondHeadingSplitup[i]) != null) {
								invoiceModel = new InvoiceModel();
								invoiceModel.setLabel(listLangDesc.get(arrSecondHeadingSplitup[i]).getDesc());
								invoiceModel.setAmount(
										FormatValue(hashmapODS_Data.get(arrSecondHeadingSplitup[i]).getAmount()));
								results.add(invoiceModel);
							}

							else {
								invoiceModel = new InvoiceModel();
								invoiceModel.setLabel(
										ConfigurationManager.getConfigurationFromCache("invoice.details.header.id."
												+ arrSecondHeadingSplitup[i] + "." + cclient.getLang()));
								invoiceModel.setAmount("0.00");
								results.add(invoiceModel);
							}
						}
						invoiceData.setResults(results);
						companyInvoiceData.add(invoiceData);
						invoiceData = new CompanyInvoiceData();

						if (listLangDesc.get(
								ConfigurationManager.getConfigurationFromCache("invoice.details.header.id.3")) != null
								&& hashmapODS_Data.get(ConfigurationManager
										.getConfigurationFromCache("invoice.details.header.id.3")) != null) {
							invoiceData.setLabel(listLangDesc
									.get(ConfigurationManager.getConfigurationFromCache("invoice.details.header.id.3"))
									.getDesc());
							invoiceData.setAmount(FormatValue(hashmapODS_Data
									.get(ConfigurationManager.getConfigurationFromCache("invoice.details.header.id.3"))
									.getAmount()));

						} else {

							invoiceData.setLabel(ConfigurationManager
									.getConfigurationFromCache("invoice.details.header.id.3." + cclient.getLang()));
							loggerV2.info("List Size: " + results.size());
							invoiceData.setAmount(
									FormatValue((Double.parseDouble(results.get(results.size() - 1).getAmount()))
											+ (Double.parseDouble(results.get(results.size() - 2).getAmount())) + ""));

						}
						results = new ArrayList<InvoiceModel>();

						String[] arrThirdHeadingSplitup = ConfigurationManager
								.getConfigurationFromCache("invoice.details.header.id.3.splitup").split(",");
						for (int i = 0; i < arrThirdHeadingSplitup.length; i++) {
							if (listLangDesc.get(arrThirdHeadingSplitup[i]).getDesc() != null
									&& hashmapODS_Data.get(arrThirdHeadingSplitup[i]) != null) {
								invoiceModel = new InvoiceModel();
								invoiceModel.setLabel(listLangDesc.get(arrThirdHeadingSplitup[i]).getDesc());
								invoiceModel.setAmount(
										FormatValue(hashmapODS_Data.get(arrThirdHeadingSplitup[i]).getAmount()));
								results.add(invoiceModel);
							}

							else {
								invoiceModel = new InvoiceModel();
								invoiceModel.setLabel(
										ConfigurationManager.getConfigurationFromCache("invoice.details.header.id."
												+ arrThirdHeadingSplitup[i] + "." + cclient.getLang()));
								invoiceModel.setAmount("0.00");
								results.add(invoiceModel);
							}
						}

						invoiceData.setResults(results);
						companyInvoiceData.add(invoiceData);
						data.setInvoiceData(companyInvoiceData);
						resp.setData(data);

						String pdfPath = new Pdf().generateCompanyInvoicePdf(cclient.getmsisdn(), resp,
								cclient.getLang());
						loggerV2.info(cclient.getmsisdn() + "PDF Generated :" + pdfPath);

						resp.getData().setExportLink(pdfPath);

						loggerV2.info(cclient.getmsisdn()
								+ "=============================== datalist populated =======================");

						resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
						resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						// cclient.setBillMonth("-4");
						///////////////////////////////////////////////////////////////////////////
						logMessage(
								cclient.getmsisdn() + "============= Response to App Server and WEB =================");
						logMessage(cclient.getmsisdn() + "RESPONSE: " + Helper.ObjectToJson(resp));

					}
				} catch (Exception ex) {
					loggerV2.info(Helper.GetException(ex));
					resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
					resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		}
		return resp;
	}

	private String FormatValue(String amount) {
		NumberFormat df = new DecimalFormat("#0.00");
		df.setRoundingMode(RoundingMode.FLOOR);
		return df.format(Double.parseDouble(amount));
	}

	private HashMap<String, ODSData> getCompanyInvoiceFromODS(CompanyInvoiceRequest cclient,
			HashMap<String, LangAndDesc> listLangDesc) throws JsonProcessingException {

		logMessage(cclient.getmsisdn() + "-Get company invoice data from ODS.");
		Connection myConnection = DBAzerfonFactory.getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		HashMap<String, ODSData> hashMapODSData = new HashMap<String, ODSData>();
		try {
			String sql = "select CATEGORY, sum(AMT) from E_CARE_DEV.V_E_CARE_TOTAL_INVOICE t where ACCT_CODE =? and DATA_DAY = to_date(?,'yyyymmdd') group by  CATEGORY, ACCT_ID, ACCT_CODE, BILL_CYCLE_ID";
			preparedStatement = myConnection.prepareStatement(sql);
			preparedStatement.setString(1, cclient.getAcctCode());
			preparedStatement.setString(2, getBillingMonth(cclient.getBillMonth()));

			// preparedStatement.setString(1, "11000000640588");
			// preparedStatement.setString(2, "20200101");
			loggerV2.debug(cclient.getmsisdn() + "-Company Invoice Query-" + sql);
			loggerV2.debug(cclient.getmsisdn() + "-Company Invoice Query params| AccountCode-" + cclient.getAcctCode()
					+ "| Billing month" + getBillingMonth(cclient.getBillMonth()));
			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				loggerV2.debug(cclient.getmsisdn() + "-Company Invoice CAT ID-" + resultSet.getString(1));
				loggerV2.debug(cclient.getmsisdn() + "-Company Invoice Amount-" + resultSet.getString(2));
				ODSData odsData = new ODSData();
				odsData.setCategoryId(resultSet.getString(1));
				odsData.setAmount(resultSet.getString(2));
				// hashMapODSData.put(odsData.getCategoryId().replace(".", ""),
				// odsData);
				hashMapODSData.put(odsData.getCategoryId(), odsData);
			}
			preparedStatement.close();
			resultSet.close();
		} catch (Exception e) {
			loggerV2.info(cclient.getmsisdn() + Helper.GetException(e));
		} finally {
			try {
				if (resultSet != null)
					resultSet.close();
				if (preparedStatement != null)
					preparedStatement.close();
			} catch (Exception e) {
				loggerV2.info(Helper.GetException(e));
			}
		}
		loggerV2.info("ODS LANGUAGE DESC-" + new ObjectMapper().writeValueAsString(hashMapODSData));
		return hashMapODSData;
	}

	private HashMap<String, LangAndDesc> getCategoryWithLangAndDescFromODS(String msisdn, String lang) {
		logMessage(msisdn + "-Get Language Description and category ID in laguage-" + lang);

		HashMap<String, LangAndDesc> hashmap = new HashMap<>();
		Connection myConnection = DBAzerfonFactory.getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			String sql = "select ID,ID_LANG,DESCRIPTION from E_CARE_DEV.E_CARE_ID_DESCRIPTIONS t where COLUMN_ID=10 and ID_LANG=?";
			loggerV2.debug(sql);
			preparedStatement = myConnection.prepareStatement(sql);
			preparedStatement.setString(1, lang);
			loggerV2.debug(msisdn + "-ODS Compan Invoice Categories Query-" + sql);
			loggerV2.debug(msisdn + "-ODS Compan Invoice Categories  Query params| Language-" + lang);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				LangAndDesc langAndDesc = new LangAndDesc();
				langAndDesc.setCategoryId(resultSet.getString(1));
				langAndDesc.setLangId(resultSet.getString(2));
				langAndDesc.setDesc(resultSet.getString(3));
				hashmap.put(langAndDesc.getCategoryId(), langAndDesc);
				// hashmap.put(langAndDesc.getCategoryId().replace(".", ""),
				// langAndDesc);
			}
			preparedStatement.close();
			resultSet.close();
		} catch (Exception e) {
			loggerV2.info(msisdn + Helper.GetException(e));
		} finally {
			try {
				if (resultSet != null)
					resultSet.close();
				if (preparedStatement != null)
					preparedStatement.close();
			} catch (Exception e) {
				loggerV2.info(Helper.GetException(e));
			}
		}
		try {
			loggerV2.info("ODS LANGUAGE DESC-" + new ObjectMapper().writeValueAsString(hashmap));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			loggerV2.info(Helper.GetException(e));
		}
		return hashmap;
	}

	private String getLangMap(String lang) {
		if (lang.equalsIgnoreCase("2"))
			return "2052";
		else if (lang.equalsIgnoreCase("4"))
			return "2060";
		else if (lang.equalsIgnoreCase("3"))
			return "2002";
		else
			return "2002";
	}

	private String getBillingMonth(String billMonth) {
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		Calendar aCalendar = Calendar.getInstance();
		// add -1 month to current month

		aCalendar.add(Calendar.MONTH, Integer.parseInt(billMonth));
		// set DATE to 1, so first date of previous month
		aCalendar.set(Calendar.DATE, 1);
		return format.format(aCalendar.getTime());
	}

	private void logMessage(String message) {
		loggerV2.info(modeulName + ": " + message);
	}

}
