package com.evampsaanga.b2b.azerfon.ussdgwqueryfreeresource;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "USSDGWQueryFreeResourcesResponse", propOrder = { "responseBody" })
@XmlRootElement(name = "USSDGWQueryFreeResourcesResponse")
public class USSDGWQueryFreeResourcesResponse extends com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse {
}
