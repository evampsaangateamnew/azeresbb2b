package com.evampsaanga.b2b.azerfon.getDashboardData;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.amqimplementationsesb.LogsType;
import com.evampsaanga.b2b.authorization.AuthorizationAndAuthentication;
import com.evampsaanga.b2b.azerfon.getUsersV2.GetUsersGroupInfoService;
import com.evampsaanga.b2b.azerfon.getUsersV2.UsersGroupRequest;
import com.evampsaanga.b2b.azerfon.queryBalancePICV2.QueryBalancePicResponse;
import com.evampsaanga.b2b.azerfon.queryBalancePICV2.QueryBalancePicService;
import com.evampsaanga.b2b.azerfon.queryinvoiceV2.QueryInvoiceRequestData;
import com.evampsaanga.b2b.azerfon.queryinvoiceV2.QueryInvoiceResponse;
import com.evampsaanga.b2b.azerfon.queryinvoiceV2.QueryInvoiceResponseData;
import com.evampsaanga.b2b.azerfon.queryinvoiceV2.QueryInvoiceService;
import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.ThirdPartyNames;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.evampsaanga.b2b.magentoservices.MagentoServices;
import com.evampsaanga.b2b.models.LoginRequest;
import com.evampsaanga.b2b.models.LoginResponse;
import com.evampsaanga.b2b.validator.rules.ChannelNotEmpty;
import com.evampsaanga.b2b.validator.rules.IPNotEmpty;
import com.evampsaanga.b2b.validator.rules.LangNotEmpty;
import com.evampsaanga.b2b.validator.rules.MSISDNNotEmpty;
import com.evampsaanga.b2b.validator.rules.ValidationResult;

/**
 * 
 * @author Abdul Saboor Generates dashboard data for mobile app
 *
 */
@Path("/azerfon")
public class DashboardDataService {
	public static final Logger logger = Logger.getLogger("azerfon-esb");

	/**
	 * 
	 * @param requestBody
	 * @param credential
	 * @param isFromB2B
	 * @return dashboard information for mobile app
	 * @throws SQLException
	 * @throws InterruptedException
	 */
	@POST
	@Path("/getdashboardinfo")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DashboardDataResponse getDashboardData(@Body String requestBody, @Header("credentials") String credential,
			@Header("isFromB2B") String isFromB2B) throws SQLException, InterruptedException {
		// Logging incoming request to log file
		Helper.logInfoMessageV2("E&S -- Request Land Time " + new Date());
		Helper.logInfoMessageV2("Request lanaded on getdashboardinfo with data as :" + requestBody);

		String reqString = "";
		// Below is declaration block for variables to be used
		DashboardDataRequest dashboardDataRequest = new DashboardDataRequest();
		DashboardDataResponse dashboardDataResponse = new DashboardDataResponse();
		// Logs object to store values which are to be inserted in database for
		// reporting
		Logs logs = new Logs();
		// Generic information about logs is being set to logs object
		logs.setRequestDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.setTransactionName(Transactions.DASHBOARD);
		logs.setThirdPartyName(ThirdPartyNames.LOGIN);
		logs.setTableType(LogsType.CustomerData);

		// Authenticating the request using credentials string received in
		// header
		boolean authenticationresult = AuthorizationAndAuthentication.authenticateAndAuthorizeCredentials(credential);

		// if request is not authenticated adding unauthorized response
		// codes to response and logs object
		if (!authenticationresult)
			prepareErrorResponse(dashboardDataResponse, logs, ResponseCodes.ERROR_401_CODE, ResponseCodes.ERROR_401,
					"authenticateAndAuthorizeCredentials");

		String token = "";
		String TrnsactionName = Transactions.BASE_MAGENTO_TRANSACTION_NAME + " " + Transactions.LOGIN_TRANSACTION_NAME;

		try {
			token = Helper.retrieveToken(TrnsactionName, Helper.getValueFromJSON(requestBody, "msisdn"));

			logger.info(token + "-Request Data-" + requestBody);
			dashboardDataRequest = Helper.JsonToObject(requestBody, DashboardDataRequest.class);
		} catch (Exception ex) {
			// block catches the exception from mapper and sets the 400 bad
			// request response code
			logger.error(token + Helper.GetException(ex));
			prepareErrorResponse(dashboardDataResponse, logs, ResponseCodes.ERROR_400_CODE, ResponseCodes.ERROR_400,
					"JsonToObject");
		}
		if (authenticationresult && dashboardDataRequest != null) {
			// block sets the mandatory parameters to logs object which are
			// taken from request body
			logs.setIp(dashboardDataRequest.getiP());
			logs.setChannel(dashboardDataRequest.getChannel());
			logs.setMsisdn(dashboardDataRequest.getmsisdn());
			// validating request packet
			ValidationResult validationResult = validateRequest(dashboardDataRequest);
			if (validationResult.isValidationResult()) {
				logs.setResponseCode(dashboardDataResponse.getReturnCode());
				logs.setResponseDescription(dashboardDataResponse.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
			} else
				// if request parameters fails validation response is
				// prepared with validation error
				prepareErrorResponse(dashboardDataResponse, logs, validationResult.getValidationCode(),
						validationResult.getValidationMessage(), "validateRequest");
			// verifying login
			MagentoServices loginObj = new MagentoServices();
			// Login Response
			Helper.logInfoMessageV2(
					token + dashboardDataRequest.getmsisdn() + " - Request for user info: " + requestBody);
			LoginResponse loginResponse = new LoginResponse();
			LoginRequest loginRequest = new LoginRequest();
			try {
				loginRequest = Helper.JsonToObject(requestBody, LoginRequest.class);
			} catch (Exception ex) {
				logger.error("Exception:", ex);
				prepareErrorResponse(dashboardDataResponse, logs, ResponseCodes.ERROR_400_CODE, ResponseCodes.ERROR_400,
						"JsonTOObject Conversion");
			}
			try {
				/// login and get predefined call
				loginResponse = loginObj.getUserAuthenticated(loginRequest, isFromB2B, token);
				Helper.logInfoMessageV2(token + "E&S -- DASHBOARD Login Response Time " + new Date());
				if (!ResponseCodes.SUCESS_CODE_200.equals(loginResponse.getReturnCode())) {
					Helper.logInfoMessageV2(token + dashboardDataRequest.getmsisdn()
							+ " - Error while getting response from Login: " + loginResponse.getReturnCode());
					loginObj.prepareErrorResponse(loginResponse, logs, ResponseCodes.ERROR_400_CODE,
							ResponseCodes.ERROR_400);
					dashboardDataResponse.setLoginData(loginResponse.getLoginData());
				} else {
					// We are recieving Virtual Corp Code in Account id
					// temperorly
					String virtualCorpCode = loginResponse.getLoginData().getVpc();
					GetUsersGroupInfoService usersGroupInfoService = new GetUsersGroupInfoService();

					// Setting request values for user group data
					UsersGroupRequest usersGroupRequest = new UsersGroupRequest();
					usersGroupRequest.setChannel(dashboardDataRequest.getChannel());
					usersGroupRequest.setVirtualCorpCode(virtualCorpCode);
					usersGroupRequest.setiP(dashboardDataRequest.getiP());
					usersGroupRequest.setLang(dashboardDataRequest.getLang());
					usersGroupRequest.setMsisdn(dashboardDataRequest.getmsisdn());
					// reqString = Helper.ObjectToJson(usersGroupRequest);
					// JSONObject jobj = new JSONObject(reqString);
					// jobj.remove("acctCode");
					// reqString = jobj.toString();
					// omitting user groups from the dashboard response
					/*
					 * Helper.logInfoMessageV2(dashboardDataRequest.getmsisdn()
					 * + " - Request Packet To get user info: " + reqString);
					 * UsersGroupResponse usersGroupResponse =
					 * usersGroupInfoService.getUsersGroupData(reqString,
					 * credential); Helper.
					 * logInfoMessageV2("E&S -- DASHBOARD user group Response Time "
					 * + new Date()); if
					 * (!"200".equals(usersGroupResponse.getReturnCode())) {
					 * Helper.logInfoMessageV2(dashboardDataRequest.getmsisdn()
					 * +
					 * " -Error while getting response from user group info service: "
					 * + usersGroupResponse.getReturnCode());
					 * usersGroupInfoService.prepareErrorResponse(
					 * usersGroupResponse, logs, ResponseCodes.ERROR_400_CODE,
					 * "Unable to Fetch Users Group Information");
					 * dashboardDataResponse.setGroupData(usersGroupResponse.
					 * getGroupData());
					 * dashboardDataResponse.setUsers(usersGroupResponse.
					 * getUsers()); }
					 */

					Helper.logInfoMessageV2(token + dashboardDataRequest.getmsisdn()
							+ " - Request Packet To get user count: " + reqString);
					String userCount = usersGroupInfoService.getUsersCount(loginResponse.getLoginData().getVpc(),
							loginResponse.getLoginData().getGroupCode(), loginResponse.getLoginData().getGroupType());
					Helper.logInfoMessageV2(
							token + "E&S -- DASHBOARD user count Response Time " + userCount + " " + new Date());
					dashboardDataResponse.setUserCount(userCount);

					// *********************
					// changing as customer id is not working
					// In Account Code we are recieving Virtual Corp COde
					usersGroupRequest.setVirtualCorpCode(loginResponse.getLoginData().getVpc());
					usersGroupRequest.setAccountCode(loginResponse.getLoginData().getAccountCode());
					reqString = Helper.ObjectToJson(usersGroupRequest);

					// jobj = new JSONObject(reqString);
					// jobj.remove("acctCode");
					// reqString = jobj.toString();
					// TODO
					// *********************
					Helper.logInfoMessageV2(token + dashboardDataRequest.getmsisdn()
							+ " -Request to Fetch Balance Person Incharge Information " + reqString);

					QueryBalancePicService balancePicService = new QueryBalancePicService();
					QueryBalancePicResponse balancePicResponse = balancePicService.queryBalancePic(reqString,
							credential);
					Helper.logInfoMessageV2(token + "E&S -- DASHBOARD PIC Balance Response Time " + new Date());
					if (!"200".equals(balancePicResponse.getReturnCode())) {
						Helper.logInfoMessageV2(dashboardDataRequest.getmsisdn()
								+ " -Error while getting response from query balance pic service: "
								+ balancePicResponse.getReturnCode());

						balancePicService.prepareErrorResponse(balancePicResponse, logs, ResponseCodes.ERROR_400_CODE,
								"Unable to Fetch Balance Person Incharge Information");
					}

					Calendar aCalendar = Calendar.getInstance();
					// add -1 month to current month
					aCalendar.add(Calendar.MONTH, -1);
					// set DATE to 1, so first date of previous month
					aCalendar.set(Calendar.DATE, 1);

					Date firstDateOfPreviousMonth = aCalendar.getTime();
					SimpleDateFormat f = new SimpleDateFormat("yyyyMMddHHmmss");
					f.format(firstDateOfPreviousMonth);
					// set actual maximum date of previous month
					aCalendar.set(Calendar.DATE, aCalendar.getActualMaximum(Calendar.DAY_OF_MONTH));
					// read it
					Date lastDateOfPreviousMonth = aCalendar.getTime();
					f.format(lastDateOfPreviousMonth);

					// setting request vaalues for query invoice
					QueryInvoiceRequestData requestData = new QueryInvoiceRequestData();
					requestData.setChannel(dashboardDataRequest.getChannel());
					// changing it to customer account code from customer
					// id(customerId)
					// TODO
					requestData.setCustomerID(loginResponse.getLoginData().getAccountCode());
					// *************
					requestData.setiP(dashboardDataRequest.getiP());
					requestData.setLang(dashboardDataRequest.getLang());
					requestData.setMsisdn(dashboardDataRequest.getmsisdn());
					// testing
					requestData.setStartTime(f.format(firstDateOfPreviousMonth).toString());
					requestData.setEndTime(f.format(lastDateOfPreviousMonth).toString());
					/*
					 * requestData.setStartTime("2016123000000");
					 * requestData.setEndTime("2017123000000");
					 */
					// *****************
					reqString = Helper.ObjectToJson(requestData);

					Helper.logInfoMessageV2(
							token + dashboardDataRequest.getmsisdn() + " - Reuqest to get invoice info: " + reqString);
					// Helper.logInfoMessage("Requrest String =" + reqString);

					// START OF Free Unit
					FreeResourcesDashboad freeunitresp = new FreeResourcesDashboad();
					M2mBalance m2mBalance = new M2mBalance();
					m2mBalance.setBalanceM2mLabel(ConfigurationManager
							.getConfigurationFromCache("home.page.m2mbalance.label." + dashboardDataRequest.getLang()));
					Helper.logInfoMessageV2(token + " - BalanceBefore If check: "
							+ balancePicResponse.getQueryBalancePicResponseData().getBalanceM2m());
					if (balancePicResponse.getQueryBalancePicResponseData().getBalanceM2m() != null
							&& !balancePicResponse.getQueryBalancePicResponseData().getBalanceM2m().isEmpty()
							&& !balancePicResponse.getQueryBalancePicResponseData().getBalanceM2m()
									.equalsIgnoreCase("0.00")) {
						Helper.logInfoMessageV2(token + "E&S -- DASHBOAD m2M balance :"
								+ balancePicResponse.getQueryBalancePicResponseData().getBalanceM2m());
						Helper.logInfoMessageV2(token + "E&S -- DASHBOAD m2M initial balance :"
								+ balancePicResponse.getQueryBalancePicResponseData().getBalanceM2mInitial());
						m2mBalance.setBalanceM2m(balancePicResponse.getQueryBalancePicResponseData().getBalanceM2m());
						m2mBalance.setBalanceM2mInitial(
								balancePicResponse.getQueryBalancePicResponseData().getBalanceM2mInitial());

						if (loginResponse.getLoginData().getGroupType().equalsIgnoreCase("data")) {
							freeunitresp.setM2mBalancce(m2mBalance);
						} else {
							Helper.logInfoMessageV2("Setting M2M Balance Null Because Pic Type is Voice");
							freeunitresp.setM2mBalancce(null);
						}

						Helper.logInfoMessageV2(token + "Balance in FreeResourrce: "
								+ Helper.ObjectToJson(freeunitresp.getM2mBalancce()));
						freeunitresp = DashboardFreeUnits.processUnityTypesHomeInCaseOfNull(token,
								loginResponse.getLoginData().getGroupCode(), logger, freeunitresp,
								dashboardDataRequest);
					} else {
						freeunitresp = DashboardFreeUnits.processUnityTypesDashboardB2b(token,
								loginResponse.getLoginData().getGroupCode(), logger, freeunitresp,
								dashboardDataRequest);
						freeunitresp.setM2mBalancce(m2mBalance);
					}

					dashboardDataResponse.setFreeResourceDashboard(freeunitresp);

					// END of Free Units

					QueryInvoiceService invoiceService = new QueryInvoiceService();
					QueryInvoiceResponse invoiceResponse = new QueryInvoiceResponse();
					try {
						invoiceResponse = invoiceService.queryInvoice(reqString, credential);
						Helper.logInfoMessageV2(token + "E&S -- DASHBOARD Query Invoice Response Time " + new Date());
						if (!"200".equals(invoiceResponse.getReturnCode())) {
							Helper.logInfoMessageV2(dashboardDataRequest.getmsisdn()
									+ " - Error while getting response from query invoice service invoice: "
									+ invoiceResponse.getReturnCode());

							prepareErrorResponse(dashboardDataResponse, logs, ResponseCodes.ERROR_400_CODE,
									ResponseCodes.ERROR_400, "queryInvoice");
							invoiceService.prepareErrorResponse(invoiceResponse, logs, ResponseCodes.ERROR_400_CODE,
									ResponseCodes.ERROR_400);
						}
					} catch (Exception ex) {
						Helper.logInfoMessageV2(token + "Error", Helper.GetException(ex));
						prepareErrorResponse(dashboardDataResponse, logs, ResponseCodes.ERROR_400_CODE,
								ResponseCodes.ERROR_400, "queryInvoice");
						invoiceService.prepareErrorResponse(invoiceResponse, logs, ResponseCodes.ERROR_400_CODE,
								ResponseCodes.ERROR_400);
					} finally {
						try {
							if (invoiceResponse.getQueryInvoiceResponseData() != null
									&& !invoiceResponse.getQueryInvoiceResponseData().isEmpty()) {
								this.modifyInvoiceResponse(invoiceResponse.getQueryInvoiceResponseData(),dashboardDataRequest.getLang());
							} else {
								List<QueryInvoiceResponseData> dataList = new ArrayList<QueryInvoiceResponseData>();
								QueryInvoiceResponseData data = new QueryInvoiceResponseData("0", "0", "0", "0", "0",
										"0", "0", "0", "0", "0", "0", "0","0");
								data.setTotalAmountToBePaidLabel(ConfigurationManager.getConfigurationFromCache(
										"home.page.totalAmountToBePaid.label." + dashboardDataRequest.getLang()));
								dataList.add(data);
								data.setInvoiceLabel(ConfigurationManager.getConfigurationFromCache(
										"home.page.invoice.label." + dashboardDataRequest.getLang()));
								data.setStatus("C");
								data.setDaysDisp(ConfigurationManager.getConfigurationFromCache(
										"home.page.invoice.days." + dashboardDataRequest.getLang()));
								invoiceResponse.setQueryInvoiceResponseData(dataList);
							}
							if (dashboardDataRequest.getChannel().equalsIgnoreCase("web")) {
								Helper.logInfoMessageV2(
										dashboardDataRequest.getmsisdn() + " - Setting DAshboard Response for web ");
								dashboardDataResponse.setReturnCode("200");
								dashboardDataResponse.setReturnMsg("Successful");
								dashboardDataResponse.setUserCount(userCount);
								dashboardDataResponse.setQueryBalancePicResponseData(
										balancePicResponse.getQueryBalancePicResponseData());
								dashboardDataResponse
										.setQueryInvoiceResponseData(invoiceResponse.getQueryInvoiceResponseData());
							} else {
								dashboardDataResponse.setReturnCode("200");
								dashboardDataResponse.setReturnMsg("Successful");
								dashboardDataResponse.setLoginData(loginResponse.getLoginData());
								Helper.logInfoMessageV2(dashboardDataRequest.getmsisdn() + " - Setting predefined");
								dashboardDataResponse.setPredefinedData(loginResponse.getPredefinedData());
								// dashboardDataResponse.setGroupData(usersGroupResponse.getGroupData());
								// dashboardDataResponse.setUsers(usersGroupResponse.getUsers());
								dashboardDataResponse.setUserCount(userCount);
								dashboardDataResponse.setQueryBalancePicResponseData(
										balancePicResponse.getQueryBalancePicResponseData());
								dashboardDataResponse
										.setQueryInvoiceResponseData(invoiceResponse.getQueryInvoiceResponseData());
							}
						} catch (Exception e) {
							Helper.logInfoMessageV2(token + "Error", Helper.GetException(e));
						}
					}

				}
			} catch (Exception e) {
				Helper.logInfoMessageV2(token + "Error", Helper.GetException(e));
				Helper.logInfoMessageV2(
						dashboardDataRequest.getmsisdn() + " - Error while getting response from Login");
				loginObj.prepareErrorResponse(loginResponse, logs, ResponseCodes.ERROR_400_CODE,
						ResponseCodes.ERROR_400);
				dashboardDataResponse.setLoginData(loginResponse.getLoginData());
			}
		}

		try {
			Helper.logInfoMessageV2(token + "EEB Dashboard Response: " + Helper.ObjectToJson(dashboardDataResponse));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dashboardDataResponse;

	}

	private void modifyInvoiceResponse(List<QueryInvoiceResponseData> queryInvoiceResponseData,String lang) {

		SimpleDateFormat destinationDateFormat = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat sourceDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String oldDueDate;
		String oldInvoiceDate;
		if (!queryInvoiceResponseData.isEmpty()) {
			for (int i = 0; i < queryInvoiceResponseData.size(); i++) {
				try {
					QueryInvoiceResponseData queryInvoiceItemData = new QueryInvoiceResponseData();
					queryInvoiceItemData = queryInvoiceResponseData.get(i);

					oldInvoiceDate = queryInvoiceItemData.getInvoiceDateDisp();
					oldDueDate = queryInvoiceItemData.getDueDateDisp();

					logger.info("Old InvoiceDateDisp = " + oldInvoiceDate);
					logger.info("Old DueDateDisp = " + oldDueDate);

					queryInvoiceItemData
							.setInvoiceDateDisp(ConfigurationManager.getConfigurationFromCache(
									new StringBuilder("labels.issuedate.").append(lang).toString()) + ": "
									+destinationDateFormat.format(sourceDateFormat.parse(oldInvoiceDate)));

					queryInvoiceItemData
							.setDueDateDisp(ConfigurationManager
									.getConfigurationFromCache(new StringBuilder("labels.duedate.").append(lang).toString())
									+ ": " +destinationDateFormat.format(sourceDateFormat.parse(oldDueDate)));

					logger.info("New InvoiceDateDisp = " + queryInvoiceItemData.getInvoiceDateDisp());
					logger.info("New DueDateDisp = " + queryInvoiceItemData.getDueDateDisp());
					queryInvoiceResponseData.set(i, queryInvoiceItemData);
				} catch (ParseException e) {
					logger.info(Helper.GetException(e));
					logger.info("------------Error parsing Date------------");
				}
			}

		} else {
			logger.info("---------------------queryInvoiceResponseDataList is Empty------------------------- ");
		}

	}

	/**
	 * 
	 * @param dashboardDataRequest
	 * @return validation result
	 */
	private ValidationResult validateRequest(DashboardDataRequest dashboardDataRequest) {
		ValidationResult validationResult = new ValidationResult();
		validationResult.setValidationResult(true);
		// Validating all the fields against non empty rule
		validationResult = new MSISDNNotEmpty().validateObject(dashboardDataRequest.getmsisdn());
		validationResult = new ChannelNotEmpty().validateObject(dashboardDataRequest.getChannel());
		validationResult = new IPNotEmpty().validateObject(dashboardDataRequest.getiP());
		validationResult = new LangNotEmpty().validateObject(dashboardDataRequest.getLang());
		return validationResult;
	}

	/**
	 * prepares error response and updates logs queue
	 * 
	 * @param dashboardDataResponse
	 * @param logs
	 * @param returnCode
	 * @param returnMessage
	 * @param api
	 */
	private void prepareErrorResponse(DashboardDataResponse dashboardDataResponse, Logs logs, String returnCode,
			String returnMessage, String api) {
		dashboardDataResponse.setReturnCode(returnCode);
		dashboardDataResponse.setReturnMsg(returnMessage + " " + api);
		logs.setResponseCode(dashboardDataResponse.getReturnCode());
		logs.setResponseDescription(dashboardDataResponse.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
	}

	public static void main(String[] args) {
		String input = "Geeks for saboor";

		// getBytes() method to convert string
		// into bytes[].

		char[] charArray = input.toCharArray();
		System.out.println("chara" + charArray[0]);

		HashMap<Character, Integer> charMap = new HashMap();
		int count = 0;
		for (int i = 0; i < charArray.length; i++) {
			if (charMap.containsKey(charArray[i])) {
				charMap.put(charArray[i], charMap.get(charArray[i]) + 1);
			} else {
				charMap.put(charArray[i], count + 1);
			}
		}
		for (char c : charMap.keySet()) {
			if (charMap.get(c) > 1) {
				System.out.println("Duplicate : " + c + " totlaCount :" + charMap.get(c));
			}
		}

		String[] arr = input.split(" ");
		System.out.println(arr[2]);
		System.out.println("lenth: " + arr.length);
		for (int i = arr.length - 1; i >= 0; i--) {
			System.out.println(arr[i]);
		}

		System.out.println("----START OF CHARCATER REVERSE----");

		char[] charNewArray = input.toCharArray();

		for (int i = input.length() - 1; i >= 0; i--) {
			System.out.println("" + charNewArray[i]);
		}
		System.out.println("----END OF CHARCATER REVERSE----");
	}
}
