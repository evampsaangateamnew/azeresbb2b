package com.evampsaanga.b2b.azerfon.getDashboardData;

import java.util.ArrayList;
import java.util.List;

import com.evampsaanga.b2b.gethomepage.FreeResource;
import com.fasterxml.jackson.annotation.JsonProperty;

public class FreeResourcesDashboad {

	private M2mBalance m2mBalancce;
	public M2mBalance getM2mBalancce() {
		return m2mBalancce;
	}
	public void setM2mBalancce(M2mBalance m2mBalancce) {
		this.m2mBalancce = m2mBalancce;
	}
	public List<FreeResource> getFreeResources() {
		return freeResources;
	}
	public void setFreeResources(List<FreeResource> freeResources) {
		this.freeResources = freeResources;
	}

	@JsonProperty("freeResources")
	private List<FreeResource> freeResources = new ArrayList<FreeResource>();

}
