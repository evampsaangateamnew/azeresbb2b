package com.evampsaanga.b2b.azerfon.getDashboardData;

import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

import com.evampsaanga.b2b.azerfon.getUsersV2.UsersGroupData;
import com.evampsaanga.b2b.azerfon.getUsersV2.UsersGroupResponseData;
import com.evampsaanga.b2b.azerfon.queryBalancePICV2.QueryBalancePicResponseData;
import com.evampsaanga.b2b.azerfon.queryinvoiceV2.QueryInvoiceResponseData;
import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;

@XmlRootElement
public class DashboardDataResponseWeb extends BaseResponse {
	private HashMap<String, UsersGroupResponseData> groupData;
	private Map<String, UsersGroupData> users;
	private QueryBalancePicResponseData queryBalancePicResponseData;
	private String userCount;
	private QueryInvoiceResponseData queryInvoiceResponseData;

	
	public HashMap<String, UsersGroupResponseData> getGroupData() {
		return groupData;
	}

	public void setGroupData(HashMap<String, UsersGroupResponseData> groupData) {
		this.groupData = groupData;
	}

	public Map<String, UsersGroupData> getUsers() {
		return users;
	}

	public void setUsers(Map<String, UsersGroupData> users) {
		this.users = users;
	}

	public QueryBalancePicResponseData getQueryBalancePicResponseData() {
		return queryBalancePicResponseData;
	}

	public void setQueryBalancePicResponseData(QueryBalancePicResponseData queryBalancePicResponseData) {
		this.queryBalancePicResponseData = queryBalancePicResponseData;
	}

	public QueryInvoiceResponseData getQueryInvoiceResponseData() {
		return queryInvoiceResponseData;
	}

	public void setQueryInvoiceResponseData(QueryInvoiceResponseData queryInvoiceResponseData) {
		this.queryInvoiceResponseData = queryInvoiceResponseData;
	}

	public String getUserCount() {
		return userCount;
	}

	public void setUserCount(String userCount) {
		this.userCount = userCount;
	}

	

}
