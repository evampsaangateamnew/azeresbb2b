package com.evampsaanga.b2b.azerfon.getDashboardData;

public class M2mBalance {
	
	private  String balanceM2m="0";
	private String balanceM2mInitial="0";
	private String balanceM2mLabel;
	
	public String getBalanceM2mLabel() {
		return balanceM2mLabel;
	}
	public void setBalanceM2mLabel(String balanceM2mLabel) {
		this.balanceM2mLabel = balanceM2mLabel;
	}
	public String getBalanceM2m() {
		return balanceM2m;
	}
	public void setBalanceM2m(String balanceM2m) {
		this.balanceM2m = balanceM2m;
	}
	public String getBalanceM2mInitial() {
		return balanceM2mInitial;
	}
	public void setBalanceM2mInitial(String balanceM2mInitial) {
		this.balanceM2mInitial = balanceM2mInitial;
	}


}
