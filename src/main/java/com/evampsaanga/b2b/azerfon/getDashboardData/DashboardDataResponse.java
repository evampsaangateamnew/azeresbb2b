package com.evampsaanga.b2b.azerfon.getDashboardData;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

import com.evampsaanga.b2b.azerfon.getUsersV2.UsersGroupData;
import com.evampsaanga.b2b.azerfon.getUsersV2.UsersGroupResponseData;
import com.evampsaanga.b2b.azerfon.queryBalancePICV2.QueryBalancePicResponseData;
import com.evampsaanga.b2b.azerfon.queryinvoiceV2.QueryInvoiceResponseData;
import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;
import com.evampsaanga.b2b.magento.getpredefineddataV2.GetPredefinedDataResponseData;
import com.evampsaanga.b2b.models.LoginData;

@XmlRootElement
public class DashboardDataResponse extends BaseResponse {
	private LoginData loginData;
	private GetPredefinedDataResponseData predefinedData; 
	private HashMap<String, UsersGroupResponseData> groupData;
	private Map<String, UsersGroupData> users;
	private QueryBalancePicResponseData queryBalancePicResponseData;

	private List<QueryInvoiceResponseData> queryInvoiceResponseData;
	private FreeResourcesDashboad freeResourceDashboard;
	


	public FreeResourcesDashboad getFreeResourceDashboard() {
		return freeResourceDashboard;
	}

	public void setFreeResourceDashboard(FreeResourcesDashboad freeResourceDashboard) {
		this.freeResourceDashboard = freeResourceDashboard;
	}

	private String userCount;
	
	
	public GetPredefinedDataResponseData getPredefinedData() {
		return predefinedData;
	}

	public void setPredefinedData(GetPredefinedDataResponseData predefinedData) {
		this.predefinedData = predefinedData;
	}

	public LoginData getLoginData() {
		return loginData;
	}

	public void setLoginData(LoginData loginData) {
		this.loginData = loginData;
	}

	public HashMap<String, UsersGroupResponseData> getGroupData() {
		return groupData;
	}

	public void setGroupData(HashMap<String, UsersGroupResponseData> groupData) {
		this.groupData = groupData;
	}

	public Map<String, UsersGroupData> getUsers() {
		return users;
	}

	public void setUsers(Map<String, UsersGroupData> users) {
		this.users = users;
	}

	public QueryBalancePicResponseData getQueryBalancePicResponseData() {
		return queryBalancePicResponseData;
	}

	public void setQueryBalancePicResponseData(QueryBalancePicResponseData queryBalancePicResponseData) {
		this.queryBalancePicResponseData = queryBalancePicResponseData;
	}

	public List<QueryInvoiceResponseData> getQueryInvoiceResponseData() {
		return queryInvoiceResponseData;
	}

	public void setQueryInvoiceResponseData(List<QueryInvoiceResponseData> queryInvoiceResponseData) {
		this.queryInvoiceResponseData = queryInvoiceResponseData;
	}

	public String getUserCount() {
		return userCount;
	}

	public void setUserCount(String userCount) {
		this.userCount = userCount;
	}
	

}
