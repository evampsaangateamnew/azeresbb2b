package com.evampsaanga.b2b.azerfon.getDashboardData;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;

import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.amqimplementationsesb.LogsType;
import com.evampsaanga.b2b.authorization.AuthorizationAndAuthentication;
import com.evampsaanga.b2b.azerfon.getUsersV2.GetUsersGroupInfoService;
import com.evampsaanga.b2b.azerfon.getUsersV2.UsersGroupRequest;
import com.evampsaanga.b2b.azerfon.queryBalancePICV2.QueryBalancePicResponse;
import com.evampsaanga.b2b.azerfon.queryBalancePICV2.QueryBalancePicService;
import com.evampsaanga.b2b.azerfon.queryinvoiceV2.QueryInvoiceRequestData;
import com.evampsaanga.b2b.azerfon.queryinvoiceV2.QueryInvoiceResponse;
import com.evampsaanga.b2b.azerfon.queryinvoiceV2.QueryInvoiceService;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.ThirdPartyNames;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.evampsaanga.b2b.validator.rules.ChannelNotEmpty;
import com.evampsaanga.b2b.validator.rules.IPNotEmpty;
import com.evampsaanga.b2b.validator.rules.LangNotEmpty;
import com.evampsaanga.b2b.validator.rules.MSISDNNotEmpty;
import com.evampsaanga.b2b.validator.rules.ValidationResult;
/**
 * 
 * @author Aqeel Abbas
 * Returns dashboard response to web portal. This class is explicitly written for generating response for web portal
 *
 */
@Path("/bakcell")
public class DashboardWebService {
	public static final Logger logger = Logger.getLogger("azerfon-esb");
/**
 * 
 * @param requestBody
 * @param credential
 * @return dashboard information for web
 * @throws IOException
 * @throws Exception
 */
	@POST
	@Path("/getdashboardwebinfo")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DashboardDataResponseWeb getDashboardDataWeb(@Body String requestBody,
			@Header("credentials") String credential) throws IOException, Exception {
		// Logging incoming request to log file
		Helper.logInfoMessageV2("Request lanaded on getdashboardinfo for web with data as :" + requestBody);
		// Below is declaration block for variables to be used
		// Logs object to store values which are to be inserted in database for
		// reporting
		String reqString = requestBody;
		UsersGroupRequest usersGroupRequest = new UsersGroupRequest();
		DashboardDataResponseWeb dashboardDataResponse = new DashboardDataResponseWeb();
		Logs logs = new Logs();

		// Generic information about logs is being set to logs object
		logs.setRequestDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.setTransactionName(Transactions.DASHBOARD);
		logs.setThirdPartyName(ThirdPartyNames.LOGIN);
		logs.setTableType(LogsType.CustomerData);

		try {
			usersGroupRequest = Helper.JsonToObject(requestBody, UsersGroupRequest.class);
		} catch (Exception ex) {
			// block catches the exception from mapper and sets the 400 bad
			// request response code
			logger.error(Helper.GetException(ex));
			prepareErrorResponse(dashboardDataResponse, logs, ResponseCodes.ERROR_400_CODE, ResponseCodes.ERROR_400,
					"JsonToObject");
		}
		
		// Authenticating the request using credentials string received in
		// header
		boolean authenticationresult = AuthorizationAndAuthentication.authenticateAndAuthorizeCredentials(credential);

		// if request is not authenticated adding unauthorized response
		// codes to response and logs object
		if (!authenticationresult)
			prepareErrorResponse(dashboardDataResponse, logs, ResponseCodes.ERROR_401_CODE, ResponseCodes.ERROR_401,
					"authenticateAndAuthorizeCredentials");

		if (authenticationresult && usersGroupRequest != null) {
			// block sets the mandatory parameters to logs object which are
			// taken from request body
			logs.setIp(usersGroupRequest.getiP());
			logs.setChannel(usersGroupRequest.getChannel());
			logs.setMsisdn(usersGroupRequest.getmsisdn());
			//Validating request parameters
			ValidationResult validationResult = validateRequest(usersGroupRequest);

			if (validationResult.isValidationResult()) {
				Helper.logInfoMessageV2(usersGroupRequest.getmsisdn() + " - Request Parameters Validated Successfully");
				GetUsersGroupInfoService usersGroupInfoService = new GetUsersGroupInfoService();
				/*Helper.logInfoMessageV2(usersGroupRequest.getmsisdn() + " - Started to get user info");
				Helper.logInfoMessageV2(usersGroupRequest.getmsisdn() + " - Requrest String For User Group Data " + reqString);
				//discuss with shahid bhai if below end point is needed or not
				UsersGroupResponse usersGroupResponse = usersGroupInfoService.getUsersGroupData(reqString, credential);
				if (!"200".equals(usersGroupResponse.getReturnCode())) {
					Helper.logInfoMessageV2(usersGroupRequest.getmsisdn() + " - Error while getting response from user group info service: " + usersGroupResponse.getReturnCode());
					usersGroupInfoService.prepareErrorResponse(usersGroupResponse, logs, ResponseCodes.ERROR_400_CODE,
							ResponseCodes.ERROR_400);
					dashboardDataResponse.setGroupData(usersGroupResponse.getGroupData());
					dashboardDataResponse.setUsers(usersGroupResponse.getUsers());					
					logs.setResponseCode(dashboardDataResponse.getReturnCode());
					logs.setResponseDescription(dashboardDataResponse.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
				}*/
				
				Helper.logInfoMessageV2(usersGroupRequest.getmsisdn() + " - Request Packet To get user count: " + reqString);
				JSONObject jsonObject = new JSONObject(reqString);
				String group_code = jsonObject.getString("group_code");
				String group_type = jsonObject.getString("group_type");
				String userCount = usersGroupInfoService.getUsersCount(reqString,group_code,group_type);
				Helper.logInfoMessageV2("E&S -- DASHBOARD user count Response Time " + userCount + " " + new Date());
				dashboardDataResponse.setUserCount(userCount);
				// Getting Balance for PIC
				JSONObject jObject = new JSONObject(reqString);
				String acctCode = jObject.getString("acctCode");
				jObject.remove("customerID");
				jObject.remove("acctCode");
				jObject.put("customerID", acctCode);
				Helper.logInfoMessageV2(usersGroupRequest.getmsisdn() + " - Request For Balance Person Incharge Information: "+ jObject.toString());
				QueryBalancePicService balancePicService = new QueryBalancePicService();
				//TODO change it as user group data
				QueryBalancePicResponse balancePicResponse = balancePicService.queryBalancePic(jObject.toString(), credential);
				if (!"200".equals(balancePicResponse.getReturnCode())) {
					Helper.logInfoMessageV2(usersGroupRequest.getmsisdn() + " - Error while getting response from query balance pic service: " + balancePicResponse.getReturnCode());

					balancePicService.prepareErrorResponse(balancePicResponse, logs, ResponseCodes.ERROR_400_CODE, ResponseCodes.ERROR_400);
				}
				
				//Preparing to generating request for query invoice api
				Calendar aCalendar = Calendar.getInstance();
				// add -1 month to current month
				aCalendar.add(Calendar.MONTH, -1);
				// set DATE to 1, so first date of previous month
				aCalendar.set(Calendar.DATE, 1);
				Date firstDateOfPreviousMonth = aCalendar.getTime();
				SimpleDateFormat f = new SimpleDateFormat("yyyyMMddHHmmss");
				f.format(firstDateOfPreviousMonth);
				// set actual maximum date of previous month
				aCalendar.set(Calendar.DATE, aCalendar.getActualMaximum(Calendar.DAY_OF_MONTH));
				// read it
				Date lastDateOfPreviousMonth = aCalendar.getTime();
				f.format(lastDateOfPreviousMonth);

				// setting request vaalues for query invoice
				QueryInvoiceRequestData requestData = new QueryInvoiceRequestData();
				//generating request packet
				requestData.setChannel(usersGroupRequest.getChannel());				
				requestData.setCustomerID(acctCode);
				requestData.setiP(usersGroupRequest.getiP());
				requestData.setLang(usersGroupRequest.getLang());
				requestData.setMsisdn(usersGroupRequest.getmsisdn());
				requestData.setStartTime(f.format(firstDateOfPreviousMonth).toString());
				requestData.setEndTime(f.format(lastDateOfPreviousMonth).toString());
				
				reqString = Helper.ObjectToJson(requestData);
				Helper.logInfoMessageV2(usersGroupRequest.getmsisdn() + " - Request For Invoice info: " + reqString);
				QueryInvoiceService invoiceService = new QueryInvoiceService();
				QueryInvoiceResponse invoiceResponse = new QueryInvoiceResponse();
				try {
					invoiceResponse = invoiceService.queryInvoice(reqString, credential);
					if (!"200".equals(invoiceResponse.getReturnCode())) {
						Helper.logInfoMessageV2(usersGroupRequest.getmsisdn() + " - Error while getting response from query invoice service");

						prepareErrorResponse(dashboardDataResponse, logs, ResponseCodes.ERROR_400_CODE,
								"Unable to Fetch Invoice Information", "queryInvoice");
						invoiceService.prepareErrorResponse(invoiceResponse, logs, ResponseCodes.ERROR_400_CODE,
								"Unable to Fetch Invoice Information");
					}
				} catch (Exception ex) {
					logger.error("Error", ex);
					prepareErrorResponse(dashboardDataResponse, logs, ResponseCodes.ERROR_400_CODE,
							ResponseCodes.ERROR_400, "queryInvoice");
					invoiceService.prepareErrorResponse(invoiceResponse, logs, ResponseCodes.ERROR_400_CODE,
							ResponseCodes.ERROR_400);

				} finally {
					dashboardDataResponse.setReturnCode("200");
					dashboardDataResponse.setReturnMsg("Successful");
					//dashboardDataResponse.setGroupData(usersGroupResponse.getGroupData());
					//dashboardDataResponse.setUsers(usersGroupResponse.getUsers());
					dashboardDataResponse.setUserCount(userCount);
					dashboardDataResponse.setQueryBalancePicResponseData(balancePicResponse.getQueryBalancePicResponseData());
					if (invoiceResponse.getQueryInvoiceResponseData() == null) {
						dashboardDataResponse.setQueryInvoiceResponseData(null);
					} else {
						DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
						Date date = (Date) formatter
								.parse(invoiceResponse.getQueryInvoiceResponseData().get(0).getDueDate());
						SimpleDateFormat newFormat = new SimpleDateFormat("yyyy/MM/dd");
						String dueDate = newFormat.format(date);
						invoiceResponse.getQueryInvoiceResponseData().get(0).setDueDate(dueDate);
						date = (Date) formatter
								.parse(invoiceResponse.getQueryInvoiceResponseData().get(0).getInvoiceDate());
						String invoiceDate = newFormat.format(date);
						invoiceResponse.getQueryInvoiceResponseData().get(0).setInvoiceDate(invoiceDate);
						dashboardDataResponse
								.setQueryInvoiceResponseData(invoiceResponse.getQueryInvoiceResponseData().get(0));
					}
					logs.setResponseCode(dashboardDataResponse.getReturnCode());
					logs.setResponseDescription(dashboardDataResponse.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
				}

			}
		}
		return dashboardDataResponse;
	}
/**
 * 
 * @param usersGroupRequest
 * @return validation results
 */
	private ValidationResult validateRequest(UsersGroupRequest usersGroupRequest) {
		ValidationResult validationResult = new ValidationResult();
		validationResult.setValidationResult(true);
		// Validating all the fields against non empty rule
		validationResult = new MSISDNNotEmpty().validateObject(usersGroupRequest.getmsisdn());
		validationResult = new ChannelNotEmpty().validateObject(usersGroupRequest.getChannel());
		validationResult = new IPNotEmpty().validateObject(usersGroupRequest.getiP());
		validationResult = new LangNotEmpty().validateObject(usersGroupRequest.getLang());
		return validationResult;
	}

	/**
	 * 
	 * @param dashboardDataResponse
	 * @param logs
	 * @param returnCode
	 * @param returnMessage
	 * @param api
	 */
	private void prepareErrorResponse(DashboardDataResponseWeb dashboardDataResponse, Logs logs, String returnCode,
			String returnMessage, String api) {
		dashboardDataResponse.setReturnCode(returnCode);
		dashboardDataResponse.setReturnMsg(returnMessage + " " + api);
		logs.setResponseCode(dashboardDataResponse.getReturnCode());
		logs.setResponseDescription(dashboardDataResponse.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);

	}
}
