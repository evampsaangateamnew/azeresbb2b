package com.evampsaanga.b2b.azerfon.getDashboardData;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import com.evampsaanga.b2b.azerfon.thirdpartycall.AzerfonThirdPartyCalls;
import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.evampsaanga.b2b.gethomepage.FreeResource;
import com.evampsaanga.b2b.gethomepage.FreeResourcesTypes;
import com.huawei.bme.cbsinterface.bbservices.QueryFreeUnitResult;
import com.huawei.bme.cbsinterface.bbservices.QueryFreeUnitResultMsg;

public class DashboardFreeUnits {
	// public static final Logger logger = Logger.getLogger("azerfon-esb");
	public static FreeResourcesDashboad processUnityTypesDashboardB2b(String token, String groupAccessCode,
			Logger logger, FreeResourcesDashboad freeResources, DashboardDataRequest dashboardDataRequest)
			throws IOException, Exception {

		logger.info(token
				+ "*******START OF Request Landed in DashboardFreeUnits class,  Method processUnityTypesDashboardB2b****** ");
		QueryFreeUnitResultMsg res = AzerfonThirdPartyCalls.queryfreeUnitResponseb2BThirdParty(token, groupAccessCode,
				logger);
		if (res.getResultHeader().getResultCode().equalsIgnoreCase(Constants.NGBSS_QUERY_FREE_UNITS_SUCCESS)) {
			if (res.getQueryFreeUnitResult() != null) {
				List<QueryFreeUnitResult.FreeUnitItem> items = res.getQueryFreeUnitResult().getFreeUnitItem();
				HashMap<String, FreeResourcesTypes> resourceMapped = new HashMap<>();

				logger.info(token + "FreeResourcesFrom Azerfon" + Helper.ObjectToJson(items));
				for (Iterator<com.huawei.bme.cbsinterface.bbservices.QueryFreeUnitResult.FreeUnitItem> iterator = items
						.iterator(); iterator.hasNext();) {
					com.huawei.bme.cbsinterface.bbservices.QueryFreeUnitResult.FreeUnitItem freeUnitItem = iterator
							.next();
					try {
						if (freeUnitItem.getFreeUnitType() != null)

							logger.info(token + "UNIT TYPE CHECK :" + freeUnitItem.getFreeUnitType());
						if (!ConfigurationManager
								.getContainsValueByKey("free.resource.type." + freeUnitItem.getFreeUnitType()))
							logger.info(
									token + "A KEY of Query Resources doesnt exists:" + freeUnitItem.getFreeUnitType());
						else if (ConfigurationManager
								.getConfigurationFromCache("free.resource.type." + freeUnitItem.getFreeUnitType())
								.equals(Constants.DATA)) {
							long initialAmount = 0L;
							long remainingAmount = 0L;
							try {
								initialAmount = freeUnitItem.getTotalInitialAmount();
							} catch (Exception ex) {
								logger.info(token + Helper.GetException(ex));
							}
							try {
								remainingAmount = freeUnitItem.getTotalUnusedAmount();
							} catch (Exception ex) {
								logger.info(token + Helper.GetException(ex));
							}
							if (resourceMapped.containsKey(Constants.DATA)) {
								FreeResourcesTypes unit = resourceMapped.get(Constants.DATA);
								unit.setResourceInitialUnits(unit.getResourceInitialUnits() + initialAmount);
								unit.setResourceRemainingUnits(unit.getResourceRemainingUnits() + remainingAmount);
								resourceMapped.put(Constants.DATA, unit);
							} else {
								resourceMapped.put(Constants.DATA, new FreeResourcesTypes(Constants.DATA, initialAmount,
										remainingAmount, freeUnitItem.getMeasureUnitName()));
							}
						} else if (ConfigurationManager
								.getConfigurationFromCache("free.resource.type." + freeUnitItem.getFreeUnitType())
								.equals(Constants.SMS)) {
							long initialAmount = 0L;
							long remainingAmount = 0L;
							try {
								initialAmount = freeUnitItem.getTotalInitialAmount();
							} catch (Exception ex) {
							}
							try {
								remainingAmount = freeUnitItem.getTotalUnusedAmount();
							} catch (Exception ex) {
							}
							if (resourceMapped.containsKey(Constants.SMS)) {
								FreeResourcesTypes unit = resourceMapped.get(Constants.SMS);
								unit.setResourceInitialUnits(unit.getResourceInitialUnits() + initialAmount);
								unit.setResourceRemainingUnits(unit.getResourceRemainingUnits() + remainingAmount);
								resourceMapped.put(Constants.SMS, unit);
							} else {
								resourceMapped.put(Constants.SMS, new FreeResourcesTypes(Constants.SMS, initialAmount,
										remainingAmount, freeUnitItem.getMeasureUnitName()));
							}
						}
					} catch (Exception ex) {
						logger.info(token + Helper.GetException(ex));
					}
				}
				ArrayList<FreeResourcesTypes> list = new ArrayList<>();

				if (resourceMapped.get(Constants.DATA) != null) {
					FreeResourcesTypes datafreeresources = resourceMapped.get(Constants.DATA);
					String formatter = Helper.readableFileSize(datafreeresources.getResourceRemainingUnits());
					datafreeresources.setResourceUnitName(formatter.split(" ")[1]);
					datafreeresources.setRemainingFormatted(formatter.split(" ")[0]);
					list.add(datafreeresources);
				} else {
					FreeResourcesTypes datafreeresources = new FreeResourcesTypes();
					// String formatter =
					// readableFileSize(datafreeresources.getResourceRemainingUnits());
					datafreeresources.setResourceInitialUnits(0L);
					datafreeresources.setResourceRemainingUnits(0L);
					datafreeresources.setResourceUnitName("MB");
					datafreeresources.setRemainingFormatted("0");
					datafreeresources.setResourceType("DATA");
					list.add(datafreeresources);
				}
				if (resourceMapped.get(Constants.SMS) != null) {
					FreeResourcesTypes sms = resourceMapped.get(Constants.SMS);
					sms.setRemainingFormatted(sms.getResourceRemainingUnits() + "");
					sms.setResourceUnitName("SMS");
					list.add(sms);
				} else {
					FreeResourcesTypes sms = new FreeResourcesTypes();
					sms.setRemainingFormatted("0");
					sms.setResourceUnitName("SMS");
					sms.setResourceInitialUnits(0L);
					sms.setResourceRemainingUnits(0L);
					sms.setResourceType("SMS");
					list.add(sms);
				}

				// FreeResourcesDashboad freeResources=new
				// FreeResourcesDashboad();
				for (FreeResourcesTypes freeResourcesTypes : list) {

					freeResources.getFreeResources().add(new FreeResource(freeResourcesTypes.getRemainingFormatted(),
							ConfigurationManager.getConfigurationFromCache(
									"resourcesTitleLabel." + freeResourcesTypes.getResourceType().toLowerCase() + "."
											+ dashboardDataRequest.getLang()),
							freeResourcesTypes.getResourceType(), freeResourcesTypes.getResourceInitialUnits() + "",
							freeResourcesTypes.getResourceRemainingUnits() + "",
							freeResourcesTypes.getResourceUnitName(), ""));

				}
				logger.info(token + "*Response From  Method processUnityTypesDashboardB2b** : "
						+ Helper.ObjectToJson(freeResources));

				logger.info(token
						+ "*******END OF Request Landed in DashboardFreeUnits class,  Method processUnityTypesDashboardB2b****** ");
			}
			
			
			else
			{
				
				freeResources = getEmptyObjects(freeResources,dashboardDataRequest.getLang());
				
				
			}
			
		}
		
		else
		{
			
			freeResources = getEmptyObjects(freeResources,dashboardDataRequest.getLang());
		}
		
		
		return freeResources;

	}

	private static FreeResourcesDashboad getEmptyObjects(FreeResourcesDashboad freeResources, String lang) {
		
		ArrayList<FreeResourcesTypes> list = new ArrayList<>();

		FreeResourcesTypes datafreeresources = new FreeResourcesTypes();
		datafreeresources.setResourceInitialUnits(0L);
		datafreeresources.setResourceRemainingUnits(0L);
		datafreeresources.setResourceUnitName("MB");
		datafreeresources.setRemainingFormatted("0");
		datafreeresources.setResourceType("DATA");
		list.add(datafreeresources);

		FreeResourcesTypes sms = new FreeResourcesTypes();
		sms.setRemainingFormatted("0");
		sms.setResourceUnitName("SMS");
		sms.setResourceInitialUnits(0L);
		sms.setResourceRemainingUnits(0L);
		sms.setResourceType("SMS");
		list.add(sms);

		for (FreeResourcesTypes freeResourcesTypes : list) {

			freeResources.getFreeResources().add(new FreeResource(freeResourcesTypes.getRemainingFormatted(),
					ConfigurationManager.getConfigurationFromCache(
							"resourcesTitleLabel." + freeResourcesTypes.getResourceType().toLowerCase() + "."
									+ lang),
					freeResourcesTypes.getResourceType(), freeResourcesTypes.getResourceInitialUnits() + "",
					freeResourcesTypes.getResourceRemainingUnits() + "", freeResourcesTypes.getResourceUnitName(), ""));

		}

		return freeResources;
		
		
		
	}

	public static FreeResourcesDashboad processUnityTypesHomeInCaseOfNull(String token, String groupAccessCode,
			Logger logger, FreeResourcesDashboad freeResources, DashboardDataRequest dashboardDataRequest)
			throws IOException, Exception {

		ArrayList<FreeResourcesTypes> list = new ArrayList<>();

		FreeResourcesTypes datafreeresources = new FreeResourcesTypes();
		datafreeresources.setResourceInitialUnits(0L);
		datafreeresources.setResourceRemainingUnits(0L);
		datafreeresources.setResourceUnitName("MB");
		datafreeresources.setRemainingFormatted("0");
		datafreeresources.setResourceType("DATA");
		list.add(datafreeresources);

		FreeResourcesTypes sms = new FreeResourcesTypes();
		sms.setRemainingFormatted("0");
		sms.setResourceUnitName("SMS");
		sms.setResourceInitialUnits(0L);
		sms.setResourceRemainingUnits(0L);
		sms.setResourceType("SMS");
		list.add(sms);

		for (FreeResourcesTypes freeResourcesTypes : list) {

			freeResources.getFreeResources().add(new FreeResource(freeResourcesTypes.getRemainingFormatted(),
					ConfigurationManager.getConfigurationFromCache(
							"resourcesTitleLabel." + freeResourcesTypes.getResourceType().toLowerCase() + "."
									+ dashboardDataRequest.getLang()),
					freeResourcesTypes.getResourceType(), freeResourcesTypes.getResourceInitialUnits() + "",
					freeResourcesTypes.getResourceRemainingUnits() + "", freeResourcesTypes.getResourceUnitName(), ""));

		}

		return freeResources;
	}
}
