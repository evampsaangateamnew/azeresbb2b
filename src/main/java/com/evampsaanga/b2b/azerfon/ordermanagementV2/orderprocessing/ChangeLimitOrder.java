/**
 * 
 */
package com.evampsaanga.b2b.azerfon.ordermanagementV2.orderprocessing;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.evampsaanga.azerfon.appserver.refreshappservercache.BuildCacheRequestLand;
import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.azerfon.db.DBFactory;
import com.evampsaanga.b2b.azerfon.getUsersV2.UsersGroupData;
import com.evampsaanga.b2b.azerfon.getsubscriber.CRMSubscriberService;
import com.evampsaanga.b2b.azerfon.grouppermission.Datum;
import com.evampsaanga.b2b.azerfon.grouppermission.GroupPermissionMagentoResponse;
import com.evampsaanga.b2b.azerfon.limitpaymentrelation.LimitPaymentRelationRequestLand;
import com.evampsaanga.b2b.azerfon.queryinvoiceV2.QueryInvoiceRequestData;
import com.evampsaanga.b2b.azerfon.queryinvoiceV2.QueryInvoiceResponse;
import com.evampsaanga.b2b.azerfon.queryinvoiceV2.QueryInvoiceService;
import com.evampsaanga.b2b.azerfon.thirdpartycall.AzerfonThirdPartyCalls;
import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.ThirdPartyNames;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.evampsaanga.b2b.magento.tariffdetailsv2.TariffDetailsMagentoResponse;
import com.huawei.bme.cbsinterface.arservices.QueryBalanceRequest;
import com.huawei.bme.cbsinterface.arservices.QueryBalanceRequestMsg;
import com.huawei.bme.cbsinterface.arservices.QueryBalanceResultMsg;
import com.huawei.bme.cbsinterface.arservices.QueryBalanceRequest.QueryObj;
import com.huawei.bme.cbsinterface.arservices.QueryBalanceRequest.QueryObj.AcctAccessCode;
import com.huawei.crm.azerfon.bss.query.GetSubscriberResponse;
import com.huawei.crm.query.GetGroupDataInfo;
import com.huawei.crm.query.GetGroupResponse;
import com.huawei.crm.service.ens.SubmitOrderResponse;
import com.ngbss.evampsaanga.services.ThirdPartyCall;
import com.ngbss.evampsaanga.services.ThirdPartyRequestHeader;

/**
 * @author HamzaFarooque
 *
 */
public class ChangeLimitOrder implements Order {

	public static final Logger loggerOrderV2 = Logger.getLogger("azerfonlogs-order");

	@SuppressWarnings("unused")
	@Override
	public void processOrder(ArrayList<String> order_key, ArrayList<String> order_id) throws Exception {

		loggerOrderV2.info("In Change Limit Order ----------  Processing order");

		Logs logs = new Logs();
		int index = 0, amount;
		String TrnsactionName = Transactions.CHANGE_LIMIT_TRANSACTION_NAME;
		String token;
		java.sql.Connection conn = DBFactory.getDbConnection();
		
		logs.setTransactionName(TrnsactionName);
		logs.setThirdPartyName(ThirdPartyNames.CHANGE_LIMIT);

		GetGroupResponse getGroupResponse = new GetGroupResponse();
		List<GetGroupDataInfo> grpDataList = new ArrayList<>();

		StringBuffer stringBuffer = new StringBuffer();
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy_HHmmss_SSSS");
		String dateTime = format.format(new Date());
		String fileName = "OM_" + dateTime + "_" + order_id.get(index) + ".log";
		// should be configurable in Constants
		String grouptype, tariffid = "";
		String path = Constants.ORDER_MANAGEMENT_LOGS_FILEPATH;

		stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
		stringBuffer.append(System.getProperty("line.separator"));
		stringBuffer.append(Helper.getOMlogTimeStamp() + "***Change Limit ");
		stringBuffer.append(System.getProperty("line.separator"));
		stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
		stringBuffer.append(System.getProperty("line.separator"));

		try {
			// update the main table status
			OrderProcessing.updateOrderStatus(order_id.get(index), "P", conn, stringBuffer);
			// get the order attributes so that further processing can be done

			JSONObject jsonObject = getOrderAttributes(order_id.get(index), conn, stringBuffer);
			loggerOrderV2.info("Order Attributes: " + jsonObject);
			logFile(stringBuffer, " OrderAttributes " + jsonObject);

			// request packet to process

			JSONArray jArray = jsonObject.getJSONArray("users");
			jsonObject.remove("users");
			jsonObject.remove("type");
			jsonObject.remove("orderKey");

			for (int i = 0; i < jArray.length(); i++) {

				try {

					jsonObject.put("msisdn", jArray.getJSONObject(i).get("msisdn").toString());

					token = Helper.retrieveToken(TrnsactionName, jsonObject.getString("msisdn"));

					loggerOrderV2.info(token + "Json User Array" + jArray.toString());

					grouptype = jArray.getJSONObject(i).getString("groupType");
					
					logs.setIp(jsonObject.getString("iP"));
					logs.setChannel(jsonObject.getString("channel"));
					logs.setMsisdn(jsonObject.getString("msisdn"));
					logs.setLang(jsonObject.getString("lang"));
					
					// subscriber call
					logFile(stringBuffer, "Call to GetSubscriber");
					GetSubscriberResponse getSubscriberResponse = new CRMSubscriberService()
							.GetSubscriberRequest(jsonObject.getString("msisdn"));
					
					
					if (getSubscriberResponse.getGetSubscriberBody() != null) {
						loggerOrderV2.info("---------------Subscriber Info-------------");
						loggerOrderV2.info(Helper.ObjectToJson(getSubscriberResponse));

						logFile(stringBuffer, Helper.ObjectToJson(getSubscriberResponse));

						final String primaryOfferingId = getSubscriberResponse.getGetSubscriberBody()
								.getPrimaryOffering().getOfferingId().getOfferingId();

						tariffid = primaryOfferingId;

					loggerOrderV2.info(token + "Group Type:" + grouptype + " ----- Tariff ID :" + tariffid);

					loggerOrderV2.info(token + "msisdn is: " + jsonObject.getString("msisdn"));

					loggerOrderV2.info(token + "Order Attributes: " + jsonObject);

					stringBuffer.append(
							Helper.getOMlogTimeStamp() + " --------------- MSISDN : " + jsonObject.getString("msisdn"));

					GroupPermissionMagentoResponse magentoresponse = Helper
							.groupDataCall(jsonObject.getString("msisdn"), token, loggerOrderV2, stringBuffer);
					// group data processing
					if (magentoresponse != null) {

						loggerOrderV2.info(token + " - Result code From Magento " + magentoresponse.getResultCode());

						List<String> restrictions = new ArrayList<String>();

						// Check Restriction Magento Response is success
						if (magentoresponse.getResultCode() == 0) {
							// Permissions data processing
							if (magentoresponse.getData().isEmpty()) {
								// if data is empty then no Restriction
								restrictions = new ArrayList<String>();
							} else {
								// check all Restrictions
								String limit;
								for (Datum datum : magentoresponse.getData()) {
									limit = datum.getLimitRestrictions();
									loggerOrderV2.info(token + "----- Restriction elements ----:" + limit);
									if (datum.getLimitRestrictions()!=null && !datum.getLimitRestrictions().isEmpty()) {
										restrictions.addAll(Arrays.asList(limit.split(",")));
									}
								}
							}
							logFile(stringBuffer, "----- Restrictions List----:" + Helper.listToString(restrictions));
							loggerOrderV2
									.info(token + "----- Restrictions List----:" + Helper.listToString(restrictions));

							if (!tariffid.trim().isEmpty()) {
								JSONArray jArrayOffering = new JSONArray();
								jArrayOffering.put(tariffid);
								JSONObject request = new JSONObject();
								request.put("offeringId", jArrayOffering);
								request.put("lang", jsonObject.getString("lang").trim());
								request.put("msisdn", jsonObject.getString("msisdn"));

								loggerOrderV2.info(
										token + " Magento Request Packet for TariffDetails :" + request.toString());
								logFile(stringBuffer,
										" Magento Request Packet for TariffDetails :" + request.toString());

								loggerOrderV2.info(token + "CAlling Magento tariffDetailsV2 to get MRC");
								logFile(stringBuffer, "CAlling Magento tariffDetailsV2 to get MRC");

								TariffDetailsMagentoResponse dataTariffDetailsbulk = AzerfonThirdPartyCalls
										.tariffDetailsV2BulkResponseChangeLimit(request.toString());
								logFile(stringBuffer, "Response returned From Magento Call :"
										+ Helper.ObjectToJson(dataTariffDetailsbulk));
								loggerOrderV2.info(token + "Response returned From Magento Call :"
										+ Helper.ObjectToJson(dataTariffDetailsbulk));
								// SubmitOrderResponse response =
								// changetarrifresponse(cclient.getmsisdn(),
								// cclient.getOfferingId());

								String user_action_type = "null";
								String primaryTariffMrcValue = "";
								int mrcValue;
								if (dataTariffDetailsbulk.getResultCode()
										.equals(ResponseCodes.TARIFF_DETAILS_VERSION_2_SUCCESSFUL)
										&& !dataTariffDetailsbulk.getData().isEmpty()) {
									// response.getGetSubscriberBody().getPrimaryOffering().getOfferingId().getOfferingId()
									loggerOrderV2.info(token + " dataTariffDetailsbulk response"
											+ dataTariffDetailsbulk.getData());

									loggerOrderV2.info(token + " CAlling Helper.COmpare Method ");
									int indexForOffering = Helper.compare(tariffid, dataTariffDetailsbulk.getData());
									loggerOrderV2.info(
											token + "returned from Helper.COmpare With Index " + indexForOffering);
									if (indexForOffering != -1) {
										primaryTariffMrcValue = dataTariffDetailsbulk.getData().get(indexForOffering)
												.getHeader().getMrcValue();

										mrcValue = Integer.parseInt(primaryTariffMrcValue);
										amount = Integer.parseInt(jsonObject.getString("amount"));
										
										
										//Checking Current Limit of User
										
										loggerOrderV2.info(token + "----- Calling Balance Query for Current Limit of User----:");
										QueryBalanceRequestMsg queryBalanceRequestMsg = new QueryBalanceRequestMsg();
										QueryBalanceRequest BalanceRequest = new QueryBalanceRequest();
										QueryObj QueryObj = new QueryObj();
										AcctAccessCode accessCode = new AcctAccessCode();
										loggerOrderV2.info("#################################################3");
										loggerOrderV2.info("Testing:" + jsonObject.getString("msisdn"));
										loggerOrderV2.info("#################################################3");
										accessCode.setPrimaryIdentity(jsonObject.getString("msisdn"));
										QueryObj.setAcctAccessCode(accessCode);
										BalanceRequest.setQueryObj(QueryObj);
										queryBalanceRequestMsg.setQueryBalanceRequest(BalanceRequest);
										queryBalanceRequestMsg
												.setRequestHeader(ThirdPartyRequestHeader.getRequestPaymentRequestHeader());

										QueryBalanceResultMsg resultMsg = ThirdPartyCall.getQueryBalance(queryBalanceRequestMsg);
										
										Long remainingAmount = resultMsg.getQueryBalanceResult().getAcctList().get(0)
												.getAccountCredit().get(0).getTotalUsageAmount();
										NumberFormat df = new DecimalFormat("#0.00");
										df.setRoundingMode(RoundingMode.FLOOR);
										String remainingAmountstr = df
												.format(remainingAmount.longValue() / Constants.MONEY_DIVIDEND);
										double d = Double.parseDouble(remainingAmountstr);
										int remainingAmountInt = (int) d;
										int minimumLimit = Math.max(remainingAmountInt, mrcValue);
										String maximum =LimitPaymentRelationRequestLand.getMaxFromDb(mrcValue+"");
										int max = Integer.parseInt(maximum);
										
										loggerOrderV2.info(token + "----- The user min threshold is ----:"+minimumLimit);
										loggerOrderV2.info(token + "----- The user max threshold is  ----:"+max);
										loggerOrderV2.info(token + "----- The user is changing limit to  ----:"+amount);
										
										
										if(amount >= minimumLimit && amount <= max )
										{
										
										long currentLimit = 0;
										if(grouptype.equalsIgnoreCase("PayBySubs"))
										{
											loggerOrderV2.info(token + "----- User is PayBySubs----:");
											if(resultMsg.getQueryBalanceResult().getAcctList()!=null)
											{
												if(resultMsg.getQueryBalanceResult().getAcctList().get(0).getAccountCredit()!=null)
												{
													currentLimit = resultMsg.getQueryBalanceResult().getAcctList().get(0).getAccountCredit().get(0).getTotalCreditAmount();
													currentLimit = currentLimit / Constants.MONEY_DIVIDEND;
													loggerOrderV2.info(token + "----- The CUrrent limit of user is ----:"+currentLimit);
													loggerOrderV2.info(token + "----- The user is changing limit to  ----:"+amount);
												}
												
											}
										}
										
										else if(grouptype.equalsIgnoreCase("PartPay"))
											
										{
											
											if(resultMsg.getQueryBalanceResult().getAcctList()!=null)
											{
												if(resultMsg.getQueryBalanceResult().getAcctList().get(0).getPaymentLimitUsage()!=null)
												{
													currentLimit = resultMsg.getQueryBalanceResult().getAcctList().get(0).getPaymentLimitUsage().get(0).getAmount();
													currentLimit = currentLimit / Constants.MONEY_DIVIDEND;
												}
												
												loggerOrderV2.info(token + "----- The CUrrent limit of user is ----:"+currentLimit);
												loggerOrderV2.info(token + "----- The user is changing limit to  ----:"+amount);
												
											}
										}
										
										
										
										

										if ((int)currentLimit < amount) {
											user_action_type = "Upgrade";
										} else if ((int)currentLimit > amount) {
											user_action_type = "Downgrade";
										}
										loggerOrderV2.info(token + "User Action Type :: " + user_action_type);
										logFile(stringBuffer, "User Action Type :: " + user_action_type);

										String paramValue = ConfigurationManager
												.getConfigurationFromCache("changelimit.credit.limit." + amount);
										logFile(stringBuffer, "Param Value From Config" + paramValue);
										loggerOrderV2.info(token + "Param Value From Config :: " + paramValue);
										String vpc = jsonObject.getString("virtualCorpCode");
										String accountId = "";

										if (!restrictions.contains(user_action_type)) {
											// Check Subscriber Group Type
											if (grouptype.equalsIgnoreCase("PartPay")) {

												// get accountID from Cache
												loggerOrderV2.info(
														token + "Language variable :" + jsonObject.getString("lang"));
												if (BuildCacheRequestLand.usersCache
														.containsKey(vpc + "." + jsonObject.getString("lang"))) {
													Helper.logInfoMessageV2(
															"User with virtualCorpCode " + vpc + " is found in Cache");
													// return
													// BuildCacheRequestLand.usersCache.get(virtualCorpCode
													// + "."
													// +
													// usersGroupRequest.getLang());

													List<UsersGroupData> list = BuildCacheRequestLand.usersCache
															.get(vpc + "." + jsonObject.getString("lang"));
													for (UsersGroupData ugd : list) {
														if (ugd.getMsisdn().equals(jsonObject.getString("msisdn"))) {
															accountId = ugd.getCrmAccountId();
															break;
														}
													}
													loggerOrderV2
															.info(token + "Fetched AccountID from Cache :" + accountId);

													if (jsonObject.getString("companyLevel")
															.equalsIgnoreCase("Standard")) {

														// Query Invoice to
														// check last month bill
														QueryInvoiceResponse qir = this.getQueryInvoice(
																jsonObject.getString("accountCode"),
																jsonObject.getString("msisdn"),
																jsonObject.getString("lang"),
																jsonObject.getString("iP"),
																jsonObject.getString("channel"));

														loggerOrderV2.info(token + "Response from query invoice :: "
																+ Helper.ObjectToJson(qir));
														logFile(stringBuffer, "Response from query invoice :: "
																+ Helper.ObjectToJson(qir));

														if (qir.getQueryInvoiceResponseData().get(0).getStatus()
																.equals("C")) {
															// if only last
															// month bill
															// pending
															SubmitOrderResponse submitOrderResponse = AzerfonThirdPartyCalls
																	.changeLimit(jsonObject.getString("msisdn"),
																			accountId, "", paramValue);

															if (submitOrderResponse != null
																	&& submitOrderResponse.getResponseHeader()
																			.getRetCode().equalsIgnoreCase("0")) {

																loggerOrderV2.info(token
																		+ "--------limit changed and Reponse is : "
																		+ Helper.ObjectToJson(submitOrderResponse));
																logFile(stringBuffer,
																		"--------limit changed and Reponse is : "
																				+ Helper.ObjectToJson(
																						submitOrderResponse));

																OrderProcessing.updateOrderDetails(order_id.get(index),
																		jsonObject.getString("msisdn"), "C",
																		submitOrderResponse.getResponseHeader()
																				.getRetCode(),
																		Helper.getNgbssMessageFromResponseCode(
																				submitOrderResponse.getResponseHeader()
																						.getRetCode(),
																				submitOrderResponse.getResponseHeader()
																						.getRetMsg(),
																				jsonObject.getString("lang")),
																		conn, stringBuffer);
																logs.setResponseCode(submitOrderResponse
																		.getResponseHeader().getRetCode());
																logs.setResponseDescription(submitOrderResponse
																		.getResponseHeader().getRetMsg());
																logs.setResponseDateTime(
																		Helper.GenerateDateTimeToMsAccuracy());
																logs.updateLog(logs);
															} else {
																loggerOrderV2.info(token
																		+ "--------limit changed and Reponse is : "
																		+ Helper.ObjectToJson(submitOrderResponse));
																logFile(stringBuffer,
																		"--------limit changed and Reponse is : "
																				+ Helper.ObjectToJson(
																						submitOrderResponse));

																OrderProcessing.updateOrderDetails(order_id.get(index),
																		jsonObject.getString("msisdn"), "F",
																		submitOrderResponse.getResponseHeader()
																				.getRetCode(),
																		Helper.getNgbssMessageFromResponseCode(
																				submitOrderResponse.getResponseHeader()
																						.getRetCode(),
																				submitOrderResponse.getResponseHeader()
																						.getRetMsg(),
																				jsonObject.getString("lang")),
																		conn, stringBuffer);
																logs.setResponseCode(submitOrderResponse
																		.getResponseHeader().getRetCode());
																logs.setResponseDescription(submitOrderResponse
																		.getResponseHeader().getRetMsg());
																logs.setResponseDateTime(
																		Helper.GenerateDateTimeToMsAccuracy());
																logs.updateLog(logs);
															}

														} else { // if more than
																	// last
																	// month is
																	// pending

															loggerOrderV2
																	.info(token + "--------Bill is pending--------");
															logFile(stringBuffer, "--------Bill is pending--------");
															OrderProcessing.updateOrderDetails(order_id.get(index),
																	jsonObject.getString("msisdn"), "F",
																	ResponseCodes.ERROR_401_CODE,
																	"actionhistory.pending.bill.", conn, stringBuffer);
															logs.setResponseCode(ResponseCodes.ERROR_401_CODE);
															logs.setResponseDescription("Bill is pending");
															logs.setResponseDateTime(
																	Helper.GenerateDateTimeToMsAccuracy());
															logs.updateLog(logs);
														}

													}

													else {

														SubmitOrderResponse submitOrderResponse = AzerfonThirdPartyCalls
																.changeLimit(jsonObject.getString("msisdn"), accountId,
																		"", paramValue);

														if (submitOrderResponse != null
																&& submitOrderResponse.getResponseHeader().getRetCode()
																		.equalsIgnoreCase("0")) {

															loggerOrderV2.info(
																	token + "--------limit changed and Reponse is : "
																			+ Helper.ObjectToJson(submitOrderResponse));
															logFile(stringBuffer,
																	"--------limit changed and Reponse is : "
																			+ Helper.ObjectToJson(submitOrderResponse));

															OrderProcessing.updateOrderDetails(order_id.get(index),
																	jsonObject.getString("msisdn"), "C",
																	submitOrderResponse.getResponseHeader()
																			.getRetCode(),
																	Helper.getNgbssMessageFromResponseCode(
																			submitOrderResponse.getResponseHeader()
																					.getRetCode(),
																			submitOrderResponse.getResponseHeader()
																					.getRetMsg(),
																			jsonObject.getString("lang")),
																	conn, stringBuffer);
															logs.setResponseCode(submitOrderResponse.getResponseHeader()
																	.getRetCode());
															logs.setResponseDescription(submitOrderResponse
																	.getResponseHeader().getRetMsg());
															logs.setResponseDateTime(
																	Helper.GenerateDateTimeToMsAccuracy());
															logs.updateLog(logs);
														} else {
															loggerOrderV2.info(
																	token + "--------limit changed and Reponse is : "
																			+ Helper.ObjectToJson(submitOrderResponse));
															logFile(stringBuffer,
																	"--------limit changed and Reponse is : "
																			+ Helper.ObjectToJson(submitOrderResponse));

															OrderProcessing.updateOrderDetails(order_id.get(index),
																	jsonObject.getString("msisdn"), "F",
																	submitOrderResponse.getResponseHeader()
																			.getRetCode(),
																	Helper.getNgbssMessageFromResponseCode(
																			submitOrderResponse.getResponseHeader()
																					.getRetCode(),
																			submitOrderResponse.getResponseHeader()
																					.getRetMsg(),
																			jsonObject.getString("lang")),
																	conn, stringBuffer);
															logs.setResponseCode(submitOrderResponse.getResponseHeader()
																	.getRetCode());
															logs.setResponseDescription(submitOrderResponse
																	.getResponseHeader().getRetMsg());
															logs.setResponseDateTime(
																	Helper.GenerateDateTimeToMsAccuracy());
															logs.updateLog(logs);
														}
													}

												} else {
													loggerOrderV2.info(token + "virtualCorpCode not Found in Cache");
													logFile(stringBuffer, "virtualCorpCode not Found in Cache");
												}

											} else if (grouptype.equalsIgnoreCase("PayBySubs")) { // if
																									// paybysub

												if (BuildCacheRequestLand.usersCache
														.containsKey(vpc + "." + jsonObject.getString("lang"))) {
													Helper.logInfoMessageV2(
															"User with virtualCorpCode " + vpc + " is found in Cache");
													// return
													// BuildCacheRequestLand.usersCache.get(virtualCorpCode
													// + "."
													// +
													// usersGroupRequest.getLang());

													List<UsersGroupData> list = BuildCacheRequestLand.usersCache
															.get(vpc + "." + jsonObject.getString("lang"));
													for (UsersGroupData ugd : list) {
														if (ugd.getMsisdn().equals(jsonObject.getString("msisdn"))) {
															accountId = ugd.getCrmAccountId();
															break;
														}
													}
													loggerOrderV2
															.info(token + "Fetched AccountID from Cache :" + accountId);
												} else {
													loggerOrderV2.info(token + "virtualCorpCode not Found in Cache");
													logFile(stringBuffer, "virtualCorpCode not Found in Cache");
												}

												loggerOrderV2.info(token
														+ "--------PaybySub Case Submit Order Request Packet --------");
												loggerOrderV2.info(token + "--------PaybySub Case MSISDN =  --------"
														+ jsonObject.getString("msisdn"));
												loggerOrderV2.info(token + "--------PaybySub Case AMOUNT =  --------"
														+ jsonObject.getString("amount")+"00000");
												loggerOrderV2.info(token
														+ "--------PaybySub Case ACCOUNT_ID =  --------" + accountId);
												loggerOrderV2.info(token
														+ "--------PaybySub Case PARAM VALUE =  --------" + paramValue);
												SubmitOrderResponse submitOrderResponse = AzerfonThirdPartyCalls
														.changeLimit(jsonObject.getString("msisdn"), accountId,
																jsonObject.getString("amount")+"00000", paramValue);

												if (submitOrderResponse != null && submitOrderResponse
														.getResponseHeader().getRetCode().equalsIgnoreCase("0")) {

													loggerOrderV2.info(token + "--------limit changed and Reponse is : "
															+ Helper.ObjectToJson(submitOrderResponse));
													logFile(stringBuffer, "--------limit changed and Reponse is : "
															+ Helper.ObjectToJson(submitOrderResponse));

													OrderProcessing.updateOrderDetails(order_id.get(index),
															jsonObject.getString("msisdn"), "C",
															submitOrderResponse.getResponseHeader().getRetCode(),
															Helper.getNgbssMessageFromResponseCode(
																	submitOrderResponse.getResponseHeader()
																			.getRetCode(),
																	submitOrderResponse.getResponseHeader().getRetMsg(),
																	jsonObject.getString("lang")),
															conn, stringBuffer);
													logs.setResponseCode(
															submitOrderResponse.getResponseHeader().getRetCode());
													logs.setResponseDescription(
															submitOrderResponse.getResponseHeader().getRetMsg());
													logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
													logs.updateLog(logs);
												} else {
													loggerOrderV2.info(token + "--------limit changed and Reponse is : "
															+ Helper.ObjectToJson(submitOrderResponse));
													logFile(stringBuffer, "--------limit changed and Reponse is : "
															+ Helper.ObjectToJson(submitOrderResponse));

													OrderProcessing.updateOrderDetails(order_id.get(index),
															jsonObject.getString("msisdn"), "F",
															submitOrderResponse.getResponseHeader().getRetCode(),
															Helper.getNgbssMessageFromResponseCode(
																	submitOrderResponse.getResponseHeader()
																			.getRetCode(),
																	submitOrderResponse.getResponseHeader().getRetMsg(),
																	jsonObject.getString("lang")),
															conn, stringBuffer);
													logs.setResponseCode(
															submitOrderResponse.getResponseHeader().getRetCode());
													logs.setResponseDescription(
															submitOrderResponse.getResponseHeader().getRetMsg());
													logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
													logs.updateLog(logs);
												}
											}

										} else {
											loggerOrderV2
													.info(token + "--------User Does not have the permission--------");
											logFile(stringBuffer, "--------User Does not have the permission--------");
											OrderProcessing.updateOrderDetails(order_id.get(index),
													jsonObject.getString("msisdn"), "F", ResponseCodes.ERROR_401_CODE,
													"actionhistory.restricted.", conn, stringBuffer);
											logs.setResponseCode(ResponseCodes.ERROR_401_CODE);
											logs.setResponseDescription("User Does not have the permission");
											logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
											logs.updateLog(logs);
										}
										
										
										}
										else
										{
											loggerOrderV2
											.info(token + "--------Invalid Limit--------");
									logFile(stringBuffer, "--------Invalid Limit--------");
									OrderProcessing.updateOrderDetails(order_id.get(index),
											jsonObject.getString("msisdn"), "F", ResponseCodes.ERROR_401_CODE,
											"actionhistory.invalid.limit.", conn, stringBuffer);
									logs.setResponseCode(ResponseCodes.ERROR_401_CODE);
									logs.setResponseDescription("Invalid Limit");
									logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
									logs.updateLog(logs);
										}
										
									} else {// else of if MRC amtch value is -1
										loggerOrderV2.info(token
												+ "------------ MRC Value didnt matches or not found -------------");
										logFile(stringBuffer,
												"------------ MRC Value didnt matches or not found -------------");
										OrderProcessing.updateOrderDetails(order_id.get(index),
												jsonObject.getString("msisdn"), "F", ResponseCodes.ERROR_400,
												"actionhistory.generic.failure.", conn, stringBuffer);
										logs.setResponseCode(ResponseCodes.ERROR_400);
										logs.setResponseDescription("MRC Value didnt matches or not found");
										logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
										logs.updateLog(logs);
									} 
								
								
								} else {// else of Magento tariffDetailsV2
									loggerOrderV2.info(token + "Error from Magento tariffDetailsV2");
									logFile(stringBuffer, "Error from Magento tariffDetailsV2");
									OrderProcessing.updateOrderDetails(order_id.get(index),
											jsonObject.getString("msisdn"), "F",
											ResponseCodes.MAGENTO_SERVER_ERROR_CODE,
											"actionhistory.generic.failure.", conn, stringBuffer);
									logs.setResponseCode(dataTariffDetailsbulk.getResultCode());
									logs.setResponseDescription(dataTariffDetailsbulk.getMsg());
									logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
									logs.updateLog(logs);
								}
							} else { // else of If tariff Id is not given
								loggerOrderV2.info(token + "--------Tariff Id is not Given--------");
								logFile(stringBuffer, "--------Tariff Id is not Given--------");
								OrderProcessing.updateOrderDetails(order_id.get(index), jsonObject.getString("msisdn"),
										"F", ResponseCodes.ERROR_400_CODE,
										"actionhistory.generic.failure.", conn, stringBuffer);
								logs.setResponseCode(Integer.toString(magentoresponse.getResultCode()));
								logs.setResponseDescription(magentoresponse.getMsg());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
							}
						} else {
							// magento else started
							loggerOrderV2.info(token + "--------Error from magento Restriction service--------");
							logFile(stringBuffer, "--------Error from magento restriction service--------");
							OrderProcessing.updateOrderDetails(order_id.get(index), jsonObject.getString("msisdn"), "F",
									Integer.toString(magentoresponse.getResultCode()),"actionhistory.generic.failure.", conn,
									stringBuffer);
							logs.setResponseCode(Integer.toString(magentoresponse.getResultCode()));
							logs.setResponseDescription(magentoresponse.getMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
						} // magento else ended
					} else {
						// group data else started
						String responseMsg;
						if (getGroupResponse.getResponseHeader().getRetCode().equalsIgnoreCase("0")) {
							responseMsg = "No Group data found Against Msisdn ";
						} else {
							responseMsg = "Bad resquest for calling Group data ";
						}
						loggerOrderV2.info(token + "----------Group Data list against Msisdn is : "
								+ getGroupResponse.getGetGroupBody());
						loggerOrderV2.info(token + "----------" + responseMsg + "----------");

						logFile(stringBuffer, responseMsg);

						// Update Order Details to failed and add reason in DB
						OrderProcessing.updateOrderDetails(order_id.get(index), jsonObject.getString("msisdn"), "F",
								getGroupResponse.getResponseHeader().getRetCode(), Helper.getNgbssMessageFromResponseCode(
										getGroupResponse.getResponseHeader().getRetCode(),
										getGroupResponse.getResponseHeader().getRetMsg(),
										jsonObject.getString("lang")), conn, stringBuffer);
						logs.setResponseCode(getGroupResponse.getResponseHeader().getRetCode());
						logs.setResponseDescription(responseMsg);
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);

					} // group data else end
					
					}
					else
					{
						// get subscriber data else started

						loggerOrderV2.info("*************Error getting subscriber Info*************");
						logFile(stringBuffer, "Error getting Offring ID from Subscriber Info");

						// update single order detail
						OrderProcessing.updateOrderDetails(order_id.get(index), jsonObject.getString("msisdn"), "F",
								getSubscriberResponse.getResponseHeader().getRetCode(),
								Helper.getNgbssMessageFromResponseCode(
										getGroupResponse.getResponseHeader().getRetCode(),
										getGroupResponse.getResponseHeader().getRetMsg(),
										jsonObject.getString("lang")),
								conn, stringBuffer);

					}

				} catch (Exception e) {
					OrderProcessing.updateOrderDetails(order_id.get(index), jsonObject.getString("msisdn"), "F",
							ResponseCodes.INTERNAL_SERVER_ERROR_CODE,"actionhistory.generic.failure.", conn,
							stringBuffer);
					loggerOrderV2.info(Helper.GetException(e));
				}
			} // for loop end bracket

			// end of logic
			// updating purchase order
			OrderProcessing.updateOrderStatus(order_id.get(index), "C", conn, stringBuffer);
			// close files
			java.io.File file = new java.io.File(path + fileName);
			FileWriter fileWriter = new FileWriter(file);
			BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

			// write contents of StringBuffer to a file
			bufferedWriter.write(stringBuffer.toString());

			// flush the stream
			bufferedWriter.flush();

			// close the stream
			bufferedWriter.close();
			conn.close();

		} catch (Exception e) {
			//OrderProcessing.updateOrderStatus(order_id.get(index), "F", conn, stringBuffer);
			loggerOrderV2.error("Containing Previuos Status because control comes in catch block of order");
			loggerOrderV2.info(Helper.GetException(e));
			loggerOrderV2.error((Helper.GetException(e)));
		}
	}

	private QueryInvoiceResponse getQueryInvoice(String accountCode, String msisdn, String lang, String ip,
			String channel) throws Exception {

		QueryInvoiceRequestData requestData = new QueryInvoiceRequestData();

		Calendar aCalendar = Calendar.getInstance();
		// add -2 month to current month
		aCalendar.add(Calendar.MONTH, -2);
		// set DATE to 1, so first date of previous month
		aCalendar.set(Calendar.DATE, 1);

		Date firstDateOfPreviousMonth = aCalendar.getTime();
		SimpleDateFormat f = new SimpleDateFormat("yyyyMMddHHmmss");
		f.format(firstDateOfPreviousMonth);
		// set actual maximum date of previous month
		aCalendar.set(Calendar.DATE, aCalendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		// read it
		Date lastDateOfPreviousMonth = aCalendar.getTime();
		f.format(lastDateOfPreviousMonth);

		// setting request values for query invoice

		requestData.setChannel(channel);
		// changing it to customer account code from customer id(customerId)
		// TODO
		requestData.setCustomerID(accountCode);// loginResponse.getLoginData().getAccountCode()
		// *************
		requestData.setiP(ip);
		requestData.setLang(lang);
		requestData.setMsisdn(msisdn);
		// testing
		requestData.setStartTime(f.format(firstDateOfPreviousMonth).toString());
		requestData.setEndTime(f.format(lastDateOfPreviousMonth).toString());
		/*
		 * requestData.setStartTime("2016123000000");
		 * requestData.setEndTime("2017123000000");
		 */
		return new QueryInvoiceService().queryInvoiceCall(requestData, null);
	}

	private JSONObject getOrderAttributes(String order_id, Connection conn, StringBuffer stringBuffer) {

		stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
		stringBuffer.append(System.getProperty("line.separator"));
		stringBuffer.append(Helper.getOMlogTimeStamp() + "*** Select Data from Order Attributes  ");
		stringBuffer.append(System.getProperty("line.separator"));
		stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
		stringBuffer.append(System.getProperty("line.separator"));
		JSONObject jsonObject = new JSONObject();

		try {
			String query = "select key_value, value from order_attributes where order_id_fk =?";
			PreparedStatement preparedStmt = conn.prepareStatement(query);
			preparedStmt.setString(1, order_id);
			stringBuffer.append(Helper.getOMlogTimeStamp() + " Executing Query :" + preparedStmt.toString());
			ResultSet resultSet = preparedStmt.executeQuery();

			while (resultSet.next()) {
				jsonObject.put(resultSet.getString(1), (resultSet.getString(2)));
				stringBuffer
						.append(Helper.getOMlogTimeStamp() + resultSet.getString(1) + " : " + resultSet.getString(2));
			}

			logFile(stringBuffer, "*** Select Data from Order Details For Change Tariff  ");

			query = "select msisdn,group_type,tariff_ids,corpCrmCustName from order_details where order_id_fk =?";
			preparedStmt = conn.prepareStatement(query);
			preparedStmt.setString(1, order_id);
			stringBuffer.append(Helper.getOMlogTimeStamp() + "Executing Query : " + preparedStmt.toString());
			stringBuffer.append(System.getProperty("line.separator"));
			resultSet = preparedStmt.executeQuery();
			JSONArray jArray = new JSONArray();
			while (resultSet.next()) {
				JSONObject jobj = new JSONObject();
				jobj.put("msisdn", resultSet.getString(1));
				stringBuffer.append(Helper.getOMlogTimeStamp() + " MSISDN: " + resultSet.getString(1));
				stringBuffer.append(System.getProperty("line.separator"));
				jobj.put("groupType", resultSet.getString(2));
				stringBuffer.append(Helper.getOMlogTimeStamp() + " GROUP_TYPE: " + resultSet.getString(2));
				stringBuffer.append(System.getProperty("line.separator"));
				jobj.put("tariff_ids", resultSet.getString(3));
				stringBuffer.append(Helper.getOMlogTimeStamp() + " TARIFF_IDS: " + resultSet.getString(3));
				stringBuffer.append(System.getProperty("line.separator"));
				jobj.put("corpCrmCustName", resultSet.getString(4));
				stringBuffer.append(Helper.getOMlogTimeStamp() + " corpCrmCustName: " + resultSet.getString(4));
				stringBuffer.append(System.getProperty("line.separator"));
				jArray.put(jobj);
			}
			jsonObject.put("users", jArray);
			resultSet.close();
			preparedStmt.close();

		} catch (Exception e) {
			loggerOrderV2.debug(Helper.GetException(e));
		}
		return jsonObject;
	}

	StringBuffer logFile(StringBuffer stringBuffer, String msg) {
		stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
		stringBuffer.append(System.getProperty("line.separator"));
		stringBuffer.append(Helper.getOMlogTimeStamp() + msg);
		stringBuffer.append(System.getProperty("line.separator"));
		stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
		stringBuffer.append(System.getProperty("line.separator"));

		return stringBuffer;
	}
}
