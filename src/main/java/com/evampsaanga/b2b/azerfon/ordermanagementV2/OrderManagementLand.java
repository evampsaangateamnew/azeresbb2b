package com.evampsaanga.b2b.azerfon.ordermanagementV2;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.transaction.annotation.Transactional;

import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.amqimplementationsesb.LogsType;
import com.evampsaanga.b2b.azerfon.actionhistory.ActionHistoryRequest;
import com.evampsaanga.b2b.azerfon.actionhistory.ActionHistoryResponse;
import com.evampsaanga.b2b.azerfon.actionhistory.ActionHistoryResponseData;
import com.evampsaanga.b2b.azerfon.callforwarding.CallForwardRequestV2;
import com.evampsaanga.b2b.azerfon.changesuplimentry.RequestBulk;
import com.evampsaanga.b2b.azerfon.changesuplimentry.RequestBulkData;
import com.evampsaanga.b2b.azerfon.changetarif.ChangeTariffBulkRequest;
import com.evampsaanga.b2b.azerfon.changetarif.RecieverMsisdn;
import com.evampsaanga.b2b.azerfon.createorder.clientsample.CreateOrderService;
import com.evampsaanga.b2b.azerfon.db.DBFactory;
import com.evampsaanga.b2b.azerfon.ordermanagement.changepaymentrelation.BulkPaymentRelationMsisdns;
import com.evampsaanga.b2b.azerfon.ordermanagement.changepaymentrelation.ChangePaymentRelationBulkRequest;
import com.evampsaanga.b2b.azerfon.ordermanagement.dao.OrderManagementRequest;
import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.ThirdPartyNames;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Decrypter;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.evampsaanga.b2b.querypayment.BcService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.huawei.bme.cbsinterface.bccommon.SubAccessCode;
import com.huawei.bme.cbsinterface.bcservices.QueryPaymentRelationRequest;
import com.huawei.bme.cbsinterface.bcservices.QueryPaymentRelationRequest.PaymentObj;
import com.huawei.bme.cbsinterface.bcservices.QueryPaymentRelationRequestMsg;
import com.huawei.bme.cbsinterface.bcservices.QueryPaymentRelationResultMsg;
import com.huawei.bss.soaif._interface.common.createorder.PaymentPlanInfo;
import com.huawei.bss.soaif._interface.common.createorder.PaymentRelation;
import com.huawei.bss.soaif._interface.common.createorder.PaymentRelation.PaymentLimit;
import com.huawei.bss.soaif._interface.orderservice.createorder.CreateOrderReqMsg;
import com.huawei.bss.soaif._interface.orderservice.createorder.CreateOrderReqMsg.Order;
import com.huawei.bss.soaif._interface.orderservice.createorder.CreateOrderRspMsg;

/**
 * 
 * @author Hamza farooq Creates and handles bulk order operations
 */

@Path("/azerfon")
public class OrderManagementLand {
//	Process Order management logs in loggerOrderV2
//	public static final Logger loggerOrderV2 = Logger.getLogger("azerfonlogs-order");
	public static final Logger loggerV2 = Logger.getLogger("azerfon-esb");

	/**
	 * Insert the orders according to the type specified in the parameters
	 * 
	 * @param credential
	 * @param contentType
	 * @param requestBody
	 * @return Returns a message indicating if the insertion of the order is
	 *         successful or not
	 * @throws SQLException
	 * @throws Exception
	 */
	@POST
	@Path("/insertorders")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public OrderManagementResponse insertorders(@Header("credentials") String credential,
			@Header("Content-Type") String contentType, @Body() String requestBody) throws SQLException, Exception {
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.MANAGE_ORDER);
		logs.setThirdPartyName(ThirdPartyNames.MANAGE_ORDER);
		logs.setTableType(LogsType.ManageOrder);
		loggerV2.info("Request Landed on insertOrder: " + requestBody);
		OrderManagementResponse resp = new OrderManagementResponse();

		JSONObject jsonObject = null;
		OrderManagementRequest orderManagementRequest = null;
		try {
			jsonObject = new JSONObject(requestBody);
			orderManagementRequest = Helper.JsonToObject(requestBody, OrderManagementRequest.class);
		} catch (Exception e) {
			loggerV2.info(Helper.GetException(e));
		}

		loggerV2.info("Transaction Name " + GeneratedType(orderManagementRequest.getOrderKey()));
		switch (orderManagementRequest.getOrderKey()) {
		case Constants.RETRY_FAILED:

			loggerV2.info("=====================Retrying Order===========================");
			resp = retryFailedUnified(orderManagementRequest, credential, logs);
			loggerV2.info(jsonObject.getString("msisdn").toString() + " - Response is: " + resp.toString());

			logs.setTransactionName(GeneratedType(orderManagementRequest.getOrderKey()));
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			break;

		case Constants.CANCEL_PENDING:

			loggerV2.info("=====================Cancelling Order===========================");
			resp = cancelPendingUnified(orderManagementRequest, credential, logs);
			loggerV2.info(jsonObject.getString("msisdn").toString() + " - Response is: " + resp.toString());
			logs.setTransactionName(GeneratedType(orderManagementRequest.getOrderKey()));
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			break;

		default:

			loggerV2.info("=====================default case===========================");
			resp = insertOrderInDB(orderManagementRequest, credential, logs);
			loggerV2.info(jsonObject.getString("msisdn").toString() + " - Response is: " + resp.toString());
			logs.setTransactionName(GeneratedType(orderManagementRequest.getOrderKey()));
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);

		}
		return resp;
	}

	private OrderManagementResponse cancelPendingUnified(OrderManagementRequest orderManagementRequest,
			String credential, Logs logs) {
		String credentials = null;
		OrderManagementResponse resp = new OrderManagementResponse();
		try (java.sql.Connection conn = DBFactory.getDbConnection();) {
			credentials = Decrypter.getInstance().decrypt(credential);
			if (credentials == null) {
				loggerV2.info(orderManagementRequest.getmsisdn() + "  Credentials are Null");
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				resp.setResponseMsg("Credentials are not correct");
//				logs.setResponseCode(resp.getReturnCode());
//				logs.setResponseDescription(resp.getReturnMsg());
//				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
//				logs.updateLog(logs);
				return resp;
			} else if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				try {
					loggerV2.info(orderManagementRequest.getmsisdn() + " - Updating Order Details to Cancel");
					boolean falg = updateOrderDetailsToCancel(orderManagementRequest.getOrderId(), "U", conn,orderManagementRequest.getLang());
					if (falg) {
						loggerV2.info(orderManagementRequest.getmsisdn() + " - Updating Order Status");
						falg = updateOrderStatus(orderManagementRequest.getOrderId(), "C", conn, null);
						if (falg) {
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
							resp.setResponseMsg(ConfigurationManager
									.getConfigurationFromCache("actionhistory.order.retry." + orderManagementRequest.getLang()));
//							logs.setResponseCode(resp.getReturnCode());
//							logs.setResponseDescription(resp.getReturnMsg());
//							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
//							logs.updateLog(logs);
							return resp;
						} else {
							resp.setReturnCode(ResponseCodes.UNSUCCESS_CODE_CANCEL);
							resp.setReturnMsg(ResponseCodes.UNSUCCESS_CANCEL_DESC);
							resp.setResponseMsg(ConfigurationManager
									.getConfigurationFromCache("actionhistory.generic.failure." + orderManagementRequest.getLang()));
//							logs.setResponseCode(resp.getReturnCode());
//							logs.setResponseDescription(resp.getReturnMsg());
//							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
//							logs.updateLog(logs);
							return resp;
						}
					} else {
						loggerV2.info(orderManagementRequest.getmsisdn() + " - Order is Not in Pending Status");
						resp.setReturnCode(ResponseCodes.UNSUCCESS_CODE_CANCEL);
						resp.setReturnMsg(ResponseCodes.UNSUCCESS_CANCEL_DESC);
						resp.setResponseMsg(ConfigurationManager
								.getConfigurationFromCache("actionhistory.generic.failure." + orderManagementRequest.getLang()));
//						logs.setResponseCode(resp.getReturnCode());
//						logs.setResponseDescription(resp.getReturnMsg());
//						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
//						logs.updateLog(logs);
						return resp;
					}
				} catch (Exception e) {
					loggerV2.error(Helper.GetException(e));
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(e.getMessage());
					resp.setResponseMsg(e.getMessage());
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception e) {
			loggerV2.error(Helper.GetException(e));
			resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
			resp.setReturnMsg(e.getMessage());
			resp.setResponseMsg(e.getMessage());
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		return resp;
	}

	private OrderManagementResponse retryFailedUnified(OrderManagementRequest orderManagementRequest, String credential,
			Logs logs) {

		String credentials = null;
		OrderManagementResponse resp = new OrderManagementResponse();
		try (java.sql.Connection conn = DBFactory.getDbConnection();) {
			credentials = Decrypter.getInstance().decrypt(credential);
			if (credentials == null) {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				resp.setResponseMsg("Credentials are not correct");
//				logs.setResponseCode(resp.getReturnCode());
//				logs.setResponseDescription(resp.getReturnMsg());
//				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
//				logs.updateLog(logs);
				return resp;
			} else if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				try {
					boolean flag = true;
					flag = updateOrderStatusForRetry(orderManagementRequest.getOrderId(), "N", conn);
					loggerV2.info("Main Table Updated:" + flag);
					if (flag) {
						resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
						resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
						resp.setResponseMsg(ConfigurationManager
								.getConfigurationFromCache("actionhistory.order.retry." + orderManagementRequest.getLang()));
//						logs.setResponseCode(resp.getReturnCode());
//						logs.setResponseDescription(resp.getReturnMsg());
//						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
//						logs.updateLog(logs);
						return resp;
					} else {
						loggerV2.info("No Orders Found");
						resp.setReturnCode(ResponseCodes.UNSUCCESS_CODE_CANCEL);
						resp.setReturnMsg(ResponseCodes.UNSUCCESS_CANCEL_DESC);
						resp.setResponseMsg(ConfigurationManager
								.getConfigurationFromCache("actionhistory.generic.failure." + orderManagementRequest.getLang()));
//						logs.setResponseCode(resp.getReturnCode());
//						logs.setResponseDescription(resp.getReturnMsg());
//						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
//						logs.updateLog(logs);
						return resp;
					}
				} catch (Exception e) {
					loggerV2.error(Helper.GetException(e));
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(e.getMessage());
					resp.setResponseMsg(e.getMessage());
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception e) {
			loggerV2.error(Helper.GetException(e));
			resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
			resp.setReturnMsg(e.getMessage());
			resp.setResponseMsg(e.getMessage());
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		return resp;

	}

	private OrderManagementResponse insertOrderInDB(OrderManagementRequest orderManagementRequest, String credential,
			Logs logs) {

		loggerV2.info("Landed in insertOrderInDB Method ");
		String credentials = null;
		OrderManagementResponse resp = new OrderManagementResponse();
		java.sql.Connection conn = DBFactory.getDbConnection();
		loggerV2.info(" DB is Connected ");
		try {
			credentials = Decrypter.getInstance().decrypt(credential);
			if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {

				if (orderManagementRequest.getmsisdn().equals("")) {
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				// inserting data into purchase order table
				String uuid = insertPurchaseOrderUnified(orderManagementRequest, conn);
				// inserting data into order attributes
				// add order details into database
				addOrderDetailsUnified(orderManagementRequest, uuid, conn,
						GeneratedType(orderManagementRequest.getOrderKey()));
				addOrderttributesUnified(orderManagementRequest, uuid, conn);
				resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
				resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
				resp.setResponseMsg(ConfigurationManager
						.getConfigurationFromCache("actionhistory.order.retry." + orderManagementRequest.getLang()));
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		} catch (Exception e) {
			loggerV2.info(Helper.GetException(e));
			resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
			resp.setReturnMsg(e.getMessage());
			resp.setResponseMsg(e.getMessage());
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		return resp;

	}

	private String insertPurchaseOrderUnified(OrderManagementRequest orderManagementRequest, Connection conn)
			throws SQLException {
		
		loggerV2.info(" Landed in insertPurchaseOrderUnified Method ");
		String uuid = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());

		loggerV2.info("IP Address" + Helper.currentMachineIPAddress());

		String query = "insert into purchase_order (type, order_key,status,host,username,order_id,customer_id)"
				+ " values (?, ?, ?, ?, ?, ?,?)";

		// create the mysql insert preparedstatement
		PreparedStatement preparedStmt = conn.prepareStatement(query);
		preparedStmt.setString(1, GeneratedType(orderManagementRequest.getOrderKey()));
		preparedStmt.setString(2, orderManagementRequest.getOrderKey());
		preparedStmt.setString(3, "N");
		preparedStmt.setString(4, Helper.currentMachineIPAddress());
		preparedStmt.setString(5, orderManagementRequest.getmsisdn());
		preparedStmt.setString(6, uuid);
		preparedStmt.setString(7, orderManagementRequest.getCustomerId());
		loggerV2.info("Insert Purchase Order Query" + preparedStmt);
		preparedStmt.execute();
		preparedStmt.close();
		return uuid;
	}

	private void addOrderDetailsUnified(OrderManagementRequest orderManagementRequest, String uuid, Connection conn,
			String string) throws SQLException {
		String queryorderdetails = "insert into order_details (order_id_fk, msisdn,status,created_at,group_type,groupIdFrom,tariff_ids,corpCrmCustName) values ";
		for (int i = 0; i < orderManagementRequest.getRecieverMsisdn().size(); i++) {

			queryorderdetails += "(" + uuid + ",'"
					+ Helper.returnEmptyStringOnNull(orderManagementRequest.getRecieverMsisdn().get(i).getMsisdn())
					+ "','P','" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + "','"
					+ Helper.returnEmptyStringOnNull(orderManagementRequest.getRecieverMsisdn().get(i).getGroupType())
					+ "','"
					+ Helper.returnEmptyStringOnNull(orderManagementRequest.getRecieverMsisdn().get(i).getGroupIdFrom())
					+ "','"
					+ Helper.returnEmptyStringOnNull(orderManagementRequest.getRecieverMsisdn().get(i).getTariffIds())
					+ "','" + Helper.returnEmptyStringOnNull(
							orderManagementRequest.getRecieverMsisdn().get(i).getCorpCrmCustName())
					+ "'),";

		}
		if (queryorderdetails != null && queryorderdetails.length() > 0
				&& queryorderdetails.charAt(queryorderdetails.length() - 1) == ',')
			queryorderdetails = queryorderdetails.substring(0, queryorderdetails.length() - 1);
		loggerV2.debug("QUERY Order Details" + queryorderdetails);
		PreparedStatement preparedStmtOrderDetails = conn.prepareStatement(queryorderdetails);
		loggerV2.info("Order Details Query: " + preparedStmtOrderDetails.toString());
		preparedStmtOrderDetails.execute();
		preparedStmtOrderDetails.close();
	}

	@SuppressWarnings("unchecked")
	private void addOrderttributesUnified(OrderManagementRequest orderManagementRequest, String uuid, Connection conn)
			throws JSONException, IOException, Exception {
		JSONObject jsonObject = null;
		Iterator<String> iterator;

		jsonObject = new JSONObject(Helper.ObjectToJson(orderManagementRequest));

		if (jsonObject.has("recieverMsisdn"))
			jsonObject.remove("recieverMsisdn");
		jsonObject.put("type", GeneratedType(orderManagementRequest.getOrderKey()));
		iterator = jsonObject.keys();

		loggerV2.info("Adding attribute");
		loggerV2.info("Request Packet: " + Helper.ObjectToJson(orderManagementRequest));

		String query = "insert into order_attributes (order_id_fk,key_value,value,created_at) values(?,?,?,?)";
		PreparedStatement preparedStmt = conn.prepareStatement(query);
		String key = "";
		while (iterator.hasNext()) {
			key = iterator.next();
			preparedStmt.setString(1, uuid);
			preparedStmt.setString(2, key);
			preparedStmt.setString(3, jsonObject.getString(key).toString());
			preparedStmt.setString(4, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
			loggerV2.info("Adding Order Attributes Query" + preparedStmt.toString());
			preparedStmt.execute();
		}
	}

	/**
	 * Updates order_details table status against msisdn to cancel the order
	 * 
	 * @param orderId
	 * @param Stauts
	 * @param conn
	 * @return Returns a flag. True in case of success, false in case of failure
	 * @throws SQLException
	 */
	private boolean updateOrderDetailsToCancel(String orderId, String Status, Connection conn , String lang) throws SQLException {

		String query = "update order_details set status =?,`responsecode`='200',responsedescription=? WHERE status = 'P' AND order_id_fk=?";
		PreparedStatement preparedStatement = conn.prepareStatement(query);
		preparedStatement.setString(1, Status);
		preparedStatement.setString(2, ConfigurationManager
				.getConfigurationFromCache("actionhistory.order.cancel."+lang));
		preparedStatement.setString(3, orderId);

		loggerV2.info("Query: " + preparedStatement.toString());
		int count = preparedStatement.executeUpdate();
		loggerV2.info("Count Updated Query: " + count);

		if (count > 0) {
			return true;
		} else {
			return false;
		}

	}

	/**
	 * Helper Method to insert order details
	 * 
	 * @param jsonObject
	 * @param uuid
	 * @param conn
	 * @param type
	 * @throws IOException
	 * @throws Exception
	 */
	@Transactional(rollbackFor = Exception.class)
	private void addOrderDetails(JSONObject jsonObject, String uuid, Connection conn, String type)
			throws IOException, Exception {
		loggerV2.info("Type is : " + type);
		loggerV2.info("JSON is " + jsonObject);
		int length = 0;
		String[] str = null;
		// list to be used for core services
		List<String> userList = new ArrayList<>();
		// list to be used for change supplementary
		ArrayList<RequestBulkData> usersList = new ArrayList<>();

		List<RecieverMsisdn> usersListchangeTariff = new ArrayList<>();

		List<BulkPaymentRelationMsisdns> usersListChangePaymentRelation = new ArrayList<>();

		if (type.equals("BroadcastSMS")) {
			str = jsonObject.getString("recieverMsisdn").split(",");
			length = str.length;
		}
		if (type.equals("Process Core Services")) {
			CallForwardRequestV2 users = Helper.JsonToObject(jsonObject.toString(), CallForwardRequestV2.class);
			userList = Arrays.asList(users.getUsers().split(","));
			length = userList.size();
		}
		if (type.equals("Change Supplementary Offerings")) {
			RequestBulk users = Helper.JsonToObject(jsonObject.toString(), RequestBulk.class);
			usersList = users.getUsers();
			loggerV2.info("User List :" + new ObjectMapper().writeValueAsString(usersList));
			length = usersList.size();
		}
		if (type.equals("Retry Failed Order")) {
			length = 1;
		}

		if (type.equals("Change Tariff")) {
			ChangeTariffBulkRequest users = Helper.JsonToObject(jsonObject.toString(), ChangeTariffBulkRequest.class);
			usersListchangeTariff = users.getRecieverMsisdn();
			loggerV2.info("User List :" + new ObjectMapper().writeValueAsString(usersListchangeTariff));
			length = usersListchangeTariff.size();
		}
		if (type.equalsIgnoreCase(Constants.CHANGEL_PAYMENT_RELATION)) {
			ChangePaymentRelationBulkRequest users = Helper.JsonToObject(jsonObject.toString(),
					ChangePaymentRelationBulkRequest.class);
			usersListChangePaymentRelation = users.getPaymentsMsisdns();
			loggerV2.info("User List :" + new ObjectMapper().writeValueAsString(usersListChangePaymentRelation));
			length = usersListChangePaymentRelation.size();
		}

		String queryorderdetails = "insert into order_details (order_id_fk, msisdn,status,created_at,group_type,tariff_ids) values ";
		for (int i = 0; i < length; i++) {

			if (type.equals("BroadcastSMS")) {
				queryorderdetails += "(" + uuid + ",'" + str[i] + "','P','"
						+ new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + "','',''),";
			} else if (type.equals("Process Core Services")) {
				queryorderdetails += "(" + uuid + ",'" + userList.get(i) + "','P','"
						+ new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + "','',''),";
			} else if (type.equals("Change Supplementary Offerings")) {
				queryorderdetails += "(" + uuid + ",'" + usersList.get(i).getMsisdn() + "','P','"
						+ new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + "','"
						+ usersList.get(i).getGroupType() + "',''),";
			}

			else if (type.equalsIgnoreCase("Change Tariff")) {

				loggerV2.info("----- start -----");
				loggerV2.info("UserList: msisdnm : " + usersListchangeTariff.get(i).getMsisdn());
				loggerV2.info("UserList: tariffId : " + usersListchangeTariff.get(i).getTariffIds());
				loggerV2.info("UserList: groupTypr" + usersListchangeTariff.get(i).getGrouptype());
				loggerV2.info("UserList: uuid" + uuid);
				loggerV2.info("----- end -----");
				queryorderdetails += "(" + uuid + ",'" + usersListchangeTariff.get(i).getMsisdn() + "','P','"
						+ new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + "','"
						+ usersListchangeTariff.get(i).getGrouptype() + "','"
						+ usersListchangeTariff.get(i).getTariffIds() + "'),";
			} else if (type.equalsIgnoreCase(Constants.CHANGEL_PAYMENT_RELATION)) {

				loggerV2.info("----- start -----");
				loggerV2.info("chngePAyment UserList: msisdnm : " + usersListChangePaymentRelation.get(i).getMsisdn());
				loggerV2.info(
						"chngePAyment UserList: tariffId : " + usersListChangePaymentRelation.get(i).getTariffId());
				loggerV2.info(
						"chngePAyment UserList: groupTypr" + usersListChangePaymentRelation.get(i).getGroupType());
				loggerV2.info("chngePAyment UserList: uuid" + uuid);
				loggerV2.info("----- end -----");
				queryorderdetails += "(" + uuid + ",'" + usersListChangePaymentRelation.get(i).getMsisdn() + "','P','"
						+ new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + "','"
						+ usersListChangePaymentRelation.get(i).getGroupType() + "','"
						+ usersListChangePaymentRelation.get(i).getTariffId() + "'),";
			} else if (type.equals("Retry Failed Order")) {
				/** BLUNDER **/
			}
		}
		if (queryorderdetails != null && queryorderdetails.length() > 0
				&& queryorderdetails.charAt(queryorderdetails.length() - 1) == ',')
			queryorderdetails = queryorderdetails.substring(0, queryorderdetails.length() - 1);
		loggerV2.debug("QUERY Order Details" + queryorderdetails);
		PreparedStatement preparedStmtOrderDetails = conn.prepareStatement(queryorderdetails);
		preparedStmtOrderDetails.execute();
		preparedStmtOrderDetails.close();
	}

	/**
	 * helper method to insert data into main order table of order management
	 * 
	 * @param jsonObject
	 * @param conn
	 * @return
	 * @throws SQLException
	 * @throws JSONException
	 */
	@Transactional(rollbackFor = Exception.class)
	private String insertPurchaseOrder(JSONObject jsonObject, Connection conn) throws SQLException, JSONException {
		String uuid = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
		String orderKey = GeneratedKey(jsonObject.getString("type"));

		loggerV2.info("IP Address" + Helper.currentMachineIPAddress());

		String query = "insert into purchase_order (type, order_key,status,host,username,order_id)"
				+ " values (?, ?, ?, ?, ?, ?)";

		// create the mysql insert preparedstatement
		PreparedStatement preparedStmt = conn.prepareStatement(query);
		preparedStmt.setString(1, jsonObject.getString("type"));
		preparedStmt.setString(2, orderKey);
		preparedStmt.setString(3, "N");
		preparedStmt.setString(4, Helper.currentMachineIPAddress());
		preparedStmt.setString(5, jsonObject.getString("msisdn"));
		preparedStmt.setString(6, uuid);
		loggerV2.info("Insert Purchase Order Query" + preparedStmt);
		preparedStmt.execute();
		return uuid;
	}

	/**
	 * key generation
	 * 
	 * @param type
	 * @return Key Against the order type
	 */
	private String GeneratedKey(String type) {
		if (type.equals(Constants.BROADCAST_SMS_TYPE))
			return Constants.BROADCAST_SMS;
		else if (type.equals(Constants.CORE_SERVICES_TYPE))
			return Constants.CORE_SERVICES;
		else if (type.equals(Constants.CHANGE_SUPPLEMENTARY_TYPE))
			return Constants.CHANGE_SUPPLEMENTARY;
		else if (type.equals(Constants.RETRY_FAILED_TYPE))
			return Constants.RETRY_FAILED;
		else if (type.equals(Constants.CHANGEL_TARIFF_TYPE))
			return Constants.CHANGEL_TARIFF;
		else if (type.equals(Constants.CHANGEL_PAYMENT_RELATION))
			return Constants.CHANGEL_PAYMENT_RELATION_KEY;
		else if (type.equals(Constants.CHANGEL_GROUP_TYPE))
			return Constants.CHANGEL_GROUP;
		else if (type.equals(Constants.CHANGEL_CUG_TYPE))
			return Constants.CHANGEL_CUG;
		else if (type.equals(Constants.SIM_SWAP_TYPE))
			return Constants.SIM_SWAP;
		return "";

	}

	private String GeneratedType(String key) {
		if (key.equals(Constants.BROADCAST_SMS))
			return Constants.BROADCAST_SMS_TYPE;
		else if (key.equals(Constants.CORE_SERVICES))
			return Constants.CORE_SERVICES_TYPE;
		else if (key.equals(Constants.CHANGE_SUPPLEMENTARY))
			return Constants.CHANGE_SUPPLEMENTARY_TYPE;
		else if (key.equals(Constants.RETRY_FAILED))
			return Constants.RETRY_FAILED_TYPE;
		else if (key.equals(Constants.CHANGEL_TARIFF))
			return Constants.CHANGEL_TARIFF_TYPE;
		else if (key.equals(Constants.CHANGEL_PAYMENT_RELATION_KEY))
			return Constants.CHANGEL_PAYMENT_RELATION;
		else if (key.equals(Constants.CHANGEL_PAYMENT_RELATION_KEY))
			return Constants.CHANGEL_PAYMENT_RELATION;
		else if (key.equals(Constants.CHANGEL_GROUP))
			return Constants.CHANGEL_GROUP_TYPE;
		else if (key.equals(Constants.CHANGEL_CUG))
			return Constants.CHANGEL_CUG_TYPE;
		else if (key.equals(Constants.SIM_SWAP))
			return Constants.SIM_SWAP_TYPE;
		return "";

	}

	/**
	 * helper function to insert order request attributes
	 * 
	 * @param jsonObject
	 * @param uuid
	 * @param conn
	 * @throws SQLException
	 * @throws JSONException
	 */
	@SuppressWarnings("unchecked")
	@Transactional(rollbackFor = Exception.class)
	private void addOrderttributes(JSONObject jsonObject, String uuid, Connection conn)
			throws SQLException, JSONException {

		if (jsonObject.has("paymentsMsisdns")) {
			jsonObject.remove("paymentsMsisdns");
		}
		if (jsonObject.has("recieverMsisdn"))
			jsonObject.remove("recieverMsisdn");
		if (jsonObject.has("users"))
			jsonObject.remove("users");

		Iterator<String> iterator = jsonObject.keys();
		loggerV2.info("Adding attribute");
		String query = "insert into order_attributes (order_id_fk,key_value,value,created_at) values(?,?,?,?)";
		PreparedStatement preparedStmt = conn.prepareStatement(query);
		String key = "";
		while (iterator.hasNext()) {
			key = iterator.next();
			preparedStmt.setString(1, uuid);
			preparedStmt.setString(2, key);
			preparedStmt.setString(3, jsonObject.getString(key).toString());
			preparedStmt.setString(4, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
			loggerV2.info("Adding Order Attributes Query" + preparedStmt.toString());
			preparedStmt.execute();
		}

	}

	/**
	 * mian function to process orders. this function is executed via cron
	 * 
	 * @throws SQLException
	 * @throws Exception
	 */
//	@Transactional(rollbackFor = Exception.class)
//	public void processOrder() throws SQLException, Exception 
//	{
//		Logs logs = new Logs();
//		boolean resFlag = false;
//		
//		try 
//		{
//			java.sql.Connection conn = DBFactory.getDbConnection();
//			Map<String, ArrayList<String>> map = getOrderType(conn);
//			ArrayList<String> type = map.get("Type");
//			ArrayList<String> order_id = map.get("Order_Id");
//			for (int index = 0; index < type.size(); index++) {
//				
//				StringBuffer stringBuffer = new StringBuffer();
//				SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy_HHmmss_SSSS");
//				String dateTime = format.format(new Date());
//				String fileName = "OM_" + dateTime + "_" + order_id.get(index) + ".log";
//				// should be configurable in Constants
//				String path = Constants.ORDER_MANAGEMENT_FILEPATH;
//				// the following statement is used to log any messages
//
//				
//				stringBuffer.append(Helper.getOMlogTimeStamp()
//						+ "------------------------------------------------------------------");
//				stringBuffer.append(System.getProperty("line.separator"));
//				stringBuffer.append(Helper.getOMlogTimeStamp()
//						+ "                 Initialization of WorkOrder                      ");
//				stringBuffer.append(System.getProperty("line.separator"));
//				stringBuffer.append(Helper.getOMlogTimeStamp()
//						+ "------------------------------------------------------------------");
//				stringBuffer.append(System.getProperty("line.separator"));
//				
//
//				if (type.get(index).equals("BroadcastSMS")) {
//					
//					
//					
//					stringBuffer.append(Helper.getOMlogTimeStamp()
//							+ "******************************************************************");
//					stringBuffer.append(System.getProperty("line.separator"));
//					stringBuffer.append(Helper.getOMlogTimeStamp()
//							+ "                 Loading Broadcast sms process                    ");
//					stringBuffer.append(System.getProperty("line.separator"));
//					stringBuffer.append(Helper.getOMlogTimeStamp()
//							+ "******************************************************************");
//					stringBuffer.append(System.getProperty("line.separator"));
//
//					SendSMSRequest request = new SendSMSRequest();
//					try {
//						stringBuffer.append(
//								Helper.getOMlogTimeStamp() + " Updating status \"P\" of Order " + order_id.get(index));
//						stringBuffer.append(System.getProperty("line.separator"));
//
//						stringBuffer.append(Helper.getOMlogTimeStamp() + "  Order ID: " + order_id.get(index));
//						stringBuffer.append(System.getProperty("line.separator"));
//						// updating order status from N to P
//						updateOrderStatus(order_id.get(index), "P", conn, stringBuffer);
//
//						// selecting order attributes , logs are done in
//						// seperate file
//						JSONObject jsonObject = getOrderAttributes(order_id.get(index), conn, stringBuffer);
//						loggerV2.info("Order Attributes: " + jsonObject);
//						String[] idString = jsonObject.get("recieverMsisdn").toString().split(",");
//						jsonObject.remove("recieverMsisdn");
//						jsonObject.remove("type");
//						jsonObject.remove("orderKey");
//						String senderName = jsonObject.getString("senderName");
//						jsonObject.remove("senderName");
//						jsonObject.remove("msisdn");
//						jsonObject.put("msisdn", senderName);
//
//						
//						
//						stringBuffer.append(Helper.getOMlogTimeStamp()
//								+ "******************************************************************");
//						stringBuffer.append(System.getProperty("line.separator"));
//						stringBuffer.append(Helper.getOMlogTimeStamp()
//								+ " Fetch data from order details for processing Broadcast messages    ");
//						stringBuffer.append(System.getProperty("line.separator"));
//						stringBuffer.append(Helper.getOMlogTimeStamp()
//								+ "******************************************************************");
//						stringBuffer.append(System.getProperty("line.separator"));
//
//						for (int j = 0; j < idString.length; j++) {
//							jsonObject.put("recieverMsisdn", idString[j]);
//
//							// start of new logic
//							String getSuborderQuery = "select msisdn from order_details where order_id_fk='"
//									+ order_id.get(index) + "' and msisdn='" + jsonObject.getString("recieverMsisdn")
//									+ "' and (status='P')";
//							PreparedStatement preparedStatement = DBFactory.getDbConnection()
//									.prepareStatement(getSuborderQuery);
//							
//							stringBuffer.append(Helper.getOMlogTimeStamp() + "Execute Query : " + getSuborderQuery);
//							stringBuffer.append(System.getProperty("line.separator"));
//
//							ResultSet resultSet = preparedStatement.executeQuery();
//							if (resultSet.next()) {
//								request = Helper.JsonToObject(jsonObject.toString(), SendSMSRequest.class);
//								
//								request.setTextmsg(new String(Base64.decodeBase64(request.getTextmsg().getBytes())));
//
//								stringBuffer.append(Helper.getOMlogTimeStamp() + "Text Message : " + request.getTextmsg());
//								stringBuffer.append(System.getProperty("line.separator"));
//
//								loggerV2.info(request.getTextmsg());
//					
//								int upFlag = DBFactory.insertMessageIntoEsmg(request.getRecieverMsisdn(),
//										request.getTextmsg(), request.getLang(), request.getmsisdn(),
//										stringBuffer);
//								if (upFlag > 0) {
//									resFlag = true;
//									stringBuffer.append(Helper.getOMlogTimeStamp() + " Record inserted into Database");
//									stringBuffer.append(System.getProperty("line.separator"));
//
//								} else {
//									resFlag = false;
//									stringBuffer.append(Helper.getOMlogTimeStamp() + " Record inserted Failed");
//									stringBuffer.append(System.getProperty("line.separator"));
//
//								}
//								if (resFlag) {
//									stringBuffer.append(Helper.getOMlogTimeStamp() + " Update Order status to C ");
//									stringBuffer.append(System.getProperty("line.separator"));
//
//									updateOrderDetails(order_id.get(index), idString[j], "C",
//											ResponseCodes.SUCESS_CODE_200, ResponseCodes.SUCESS_DES_200, conn,
//											stringBuffer);
//								} else if (!resFlag) {
//									// updating order details
//									stringBuffer.append(Helper.getOMlogTimeStamp() + " Update Order status to F ");
//									stringBuffer.append(System.getProperty("line.separator"));
//									updateOrderDetails(order_id.get(index), idString[j], "F",
//											ResponseCodes.UNSUCCESS_CODE, ResponseCodes.UNSUCCESS_DESC + idString[j],
//											conn, stringBuffer);
//								}
//							} else {
//								stringBuffer.append(Helper.getOMlogTimeStamp() + "No record found ");
//								stringBuffer.append(System.getProperty("line.separator"));
//								stringBuffer.append(Helper.getOMlogTimeStamp() + "No record found ");
//								stringBuffer.append(System.getProperty("line.separator"));
//							}
//						} // end of new logic
//							// updating purchase order
//						updateOrderStatus(order_id.get(index), "C", conn, stringBuffer);
//					} catch (Exception e) {
//						loggerV2.error(Helper.GetException(e));
//						logs.setResponseCode(ResponseCodes.ERROR_400_CODE);
//						logs.setResponseDescription(e.getMessage());
//						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
//						logs.updateLog(logs);
//						return;
//					}
//				} else if (type.get(index).equals("Process Core Services")) {
//
//					stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
//					stringBuffer.append(System.getProperty("line.separator"));
//					stringBuffer.append(Helper.getOMlogTimeStamp() + "***Loading Process Core Services ");
//					stringBuffer.append(System.getProperty("line.separator"));
//					stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
//					stringBuffer.append(System.getProperty("line.separator"));
//
//					ProcessCoreServicesRequestV2 requestV2 = new ProcessCoreServicesRequestV2();
//					CallForwardingRequestLand coreServices = new CallForwardingRequestLand();
//					CallForwardResponse response = new CallForwardResponse();
//					// update the main table status
//					updateOrderStatus(order_id.get(index), "P", conn, stringBuffer);
//					// get the order attributes so that further processing can be done
//					try {
//						JSONObject jsonObject = getOrderAttributes(order_id.get(index), conn, stringBuffer);
//						loggerV2.info("Order Attributes: " + jsonObject);
//						// request packet to process
//						JSONArray jArray = jsonObject.getJSONArray("users");
//						jsonObject.remove("users");
//						jsonObject.remove("type");
//						jsonObject.remove("orderKey");
//
//						stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
//						stringBuffer.append(System.getProperty("line.separator"));
//						stringBuffer.append(Helper.getOMlogTimeStamp()
//								+ "***Fetch data from order details for Process Core Services ");
//						stringBuffer.append(System.getProperty("line.separator"));
//						stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
//						stringBuffer.append(System.getProperty("line.separator"));
//
//						for (int i = 0; i < jArray.length(); i++) {
//							jsonObject.put("msisdn", jArray.getJSONObject(i).get("msisdn").toString());
//							loggerV2.info("msisdn is: " + jsonObject.getString("msisdn"));
//							stringBuffer.append(Helper.getOMlogTimeStamp() + " MSISDN : " + jsonObject.getString("msisdn"));
//							String getSuborderQuery = "select msisdn from order_details where order_id_fk='"
//									+ order_id.get(index) + "' and msisdn='" + jsonObject.getString("msisdn")
//									+ "' and (status='P')";
//							PreparedStatement preparedStatement = DBFactory.getDbConnection()
//									.prepareStatement(getSuborderQuery);
//							stringBuffer.append(
//									Helper.getOMlogTimeStamp() + "Execute Query : " + preparedStatement.toString());
//							ResultSet resultSet = preparedStatement.executeQuery();
//							if (resultSet.next()) {
//
//								stringBuffer.append(Helper.getOMlogTimeStamp()
//										+ "************************************************");
//								stringBuffer.append(System.getProperty("line.separator"));
//								stringBuffer.append(
//										Helper.getOMlogTimeStamp() + "***Calling processCoreService on process order ");
//								stringBuffer.append(System.getProperty("line.separator"));
//
//								stringBuffer.append(Helper.getOMlogTimeStamp() + " REQUEST_PACKET " + jsonObject.toString());
//								stringBuffer.append(System.getProperty("line.separator"));
//								requestV2 = Helper.JsonToObject(jsonObject.toString(),
//										ProcessCoreServicesRequestV2.class);
//
//								response = coreServices.processCoreServices(requestV2);
//
//								stringBuffer.append(Helper.getOMlogTimeStamp() + " RESPONSE_PACKET "
//										+ Helper.ObjectToJson(response));
//								stringBuffer.append(System.getProperty("line.separator"));
//								stringBuffer.append(Helper.getOMlogTimeStamp()
//										+ "************************************************");
//								stringBuffer.append(System.getProperty("line.separator"));
//
//								if (response.getReturnCode().equals(ResponseCodes.SUCESS_CODE_200)) {
//									// updating order details
//									updateOrderDetails(order_id.get(index), jsonObject.getString("msisdn"), "C",
//											ResponseCodes.SUCESS_CODE_200, ResponseCodes.SUCESS_DES_200, conn,
//											stringBuffer);
//									// updating purchase order
//									updateOrderStatus(order_id.get(index), "C", conn, stringBuffer);
//								} else {
//									// updating order details
//									updateOrderDetails(order_id.get(index), jsonObject.getString("msisdn"), "F",
//											response.getReturnCode(),
//											response.getReturnMsg() + jsonObject.getString("msisdn"), conn,
//											stringBuffer);
//									// update to be put here in case of partial
//									// success
//									// TODOs
//								}
//							} else {
//
//								stringBuffer.append(Helper.getOMlogTimeStamp() + " No Record found");
//							}
//						}
//						// update the main table status
//						updateOrderStatus(order_id.get(index), "C", conn, stringBuffer);
//					} catch (Exception e) {
//						// updating purchase order
//						updateOrderStatus(order_id.get(index), "F", conn, stringBuffer);
//						loggerV2.error(Helper.GetException(e));
//					}
//
//				} else if (type.get(index).equals("Change Supplementary Offerings")) {
//
//					stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
//					stringBuffer.append(System.getProperty("line.separator"));
//					stringBuffer.append(Helper.getOMlogTimeStamp() + "***Change Supplementary Offerings ");
//					stringBuffer.append(System.getProperty("line.separator"));
//					stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
//					stringBuffer.append(System.getProperty("line.separator"));
//
//					RequestLandBulk requestLandBulk = new RequestLandBulk();
//
//					// update the main table status
//					updateOrderStatus(order_id.get(index), "P", conn, stringBuffer);
//					// get the order attributes so that further processing can
//					// be done
//					try {
//						JSONObject jsonObject = getOrderAttributes(order_id.get(index), conn, stringBuffer);
//						loggerV2.info("Order Attributes: " + jsonObject);
//						ChangeSupplimentryOfferingRequestLand CSOLand = new ChangeSupplimentryOfferingRequestLand();
//						// request packet to process
//						JSONArray jArray = jsonObject.getJSONArray("users");
//						jsonObject.remove("users");
//						jsonObject.remove("type");
//						jsonObject.remove("orderKey");
//
//						stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
//						stringBuffer.append(System.getProperty("line.separator"));
//						stringBuffer.append(Helper.getOMlogTimeStamp()
//								+ "***Fetch data from order details for Change Supplementary Offerings ");
//						stringBuffer.append(System.getProperty("line.separator"));
//						stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
//						stringBuffer.append(System.getProperty("line.separator"));
//						for (int i = 0; i < jArray.length(); i++) 
//						{
//							jsonObject.put("msisdn", jArray.getJSONObject(i).get("msisdn").toString());
//							jsonObject.put("groupType", jArray.getJSONObject(i).get("groupType").toString());
//							loggerV2.info("msisdn is: " + jsonObject.getString("msisdn"));
//
//							// start of new logic
//							String getSuborderQuery = "select msisdn from order_details where order_id_fk='"
//									+ order_id.get(index) + "' and msisdn='" + jsonObject.getString("msisdn")
//									+ "' and (status='P')";
//
//							stringBuffer.append(Helper.getOMlogTimeStamp() + "MSISDN");
//							stringBuffer.append(System.getProperty("line.separator"));
//							PreparedStatement preparedStatement = DBFactory.getDbConnection()
//									.prepareStatement(getSuborderQuery);
//							stringBuffer.append(Helper.getOMlogTimeStamp() + "Execute Query " + preparedStatement.toString());
//							stringBuffer.append(System.getProperty("line.separator"));
//							ResultSet resultSet = preparedStatement.executeQuery();
//
//							if (resultSet.next()) {
//
//								stringBuffer.append(Helper.getOMlogTimeStamp()
//										+ " REQUEST PACKET FOR CHANGE SUPPLIMENTARY OFFERS " + jsonObject.toString());
//								stringBuffer.append(System.getProperty("line.separator"));
//								requestLandBulk = Helper.JsonToObject(jsonObject.toString(), RequestLandBulk.class);
//								Response response = CSOLand.changeSupplementaryV2(requestLandBulk);
//								stringBuffer.append(
//										Helper.getOMlogTimeStamp() + " RESPONSE PACKET FOR CHANGE SUPPLIMENTARY OFFERS "
//												+ Helper.ObjectToJson(response));
//								stringBuffer.append(System.getProperty("line.separator"));
//								if (response.getReturnCode().equals(ResponseCodes.SUCESS_CODE_200)) {
//									// updating order details
//									updateOrderDetails(order_id.get(index), jsonObject.getString("msisdn"), "C",
//											ResponseCodes.SUCESS_CODE_200, ResponseCodes.SUCESS_DES_200, conn,
//											stringBuffer);
//									// updating purchase order
//									updateOrderStatus(order_id.get(index), "C", conn, stringBuffer);
//								} else {
//									// updating order details
//									updateOrderDetails(order_id.get(index), jsonObject.getString("msisdn"), "F",
//											response.getReturnCode(),
//											response.getReturnMsg() + jsonObject.getString("msisdn"), conn,
//											stringBuffer);
//									// update to be put here in case of partial
//									// success
//									// TODOs
//								}
//
//							} else {
//								stringBuffer.append(Helper.getOMlogTimeStamp() + " No Record Found");
//								stringBuffer.append(System.getProperty("line.separator"));
//								// break;
//							}
//						}
//						// update the main table status
//						updateOrderStatus(order_id.get(index), "C", conn, stringBuffer);
//					} catch (Exception e) {
//						// updating purchase order
//						updateOrderStatus(order_id.get(index), "F", conn, stringBuffer);
//						loggerV2.error(Helper.GetException(e));
//					}
//				} else if (type.get(index).equals("Change Tariff")) 
//				{
//					stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
//					stringBuffer.append(System.getProperty("line.separator"));
//					stringBuffer.append(Helper.getOMlogTimeStamp() + "***Change Tariff ");
//					stringBuffer.append(System.getProperty("line.separator"));
//					stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
//					stringBuffer.append(System.getProperty("line.separator"));
//
//					// update the main table status
//					updateOrderStatus(order_id.get(index), "P", conn, stringBuffer);
//					// get the order attributes so that further processing can
//					// be done
//					try 
//					{
//						JSONObject jsonObject = getOrderAttributes(order_id.get(index), conn, stringBuffer);
//						loggerV2.info("Order Attributes: " + jsonObject);
//						stringBuffer.append(Helper.getOMlogTimeStamp()+" OrderAttributes "+jsonObject);
//						stringBuffer.append(System.getProperty("line.separator"));
//						
//						// request packet to process
//
//						JSONArray jArray = jsonObject.getJSONArray("users");
//						jsonObject.remove("users");
//						jsonObject.remove("type");
//						jsonObject.remove("orderKey");
//						//************************************************//
//						//             Fetching Destination MRC
//						//************************************************//
//						// START OF destinationTariffID
//						String destinationtariffMrcValue = null;
//						TariffDetailsMagentoResponse tariffDetailsMagentoResponse = ConfigurationManager.getConfigurationFromTariffCache(Constants.TARIFF_DETAIL_ENGLISH);
//						if(tariffDetailsMagentoResponse.getResultCode().equals(ResponseCodes.TARIFF_DETAILS_VERSION_2_SUCCESSFUL))
//						{
//							
//								int destinationTariffIndex = Helper.compare(jsonObject.getString("destinationTariff"), tariffDetailsMagentoResponse.getData());
//								destinationtariffMrcValue = ConfigurationManager.getConfigurationFromTariffCache(Constants.TARIFF_DETAIL_ENGLISH).getData().get(destinationTariffIndex).getHeader().getMrcValue();
//								loggerV2.info("***Destination MRC fetched From Cache  ***" + destinationtariffMrcValue);
//								loggerV2.info("***Destination Tariff  ***" + jsonObject.getString("destinationTariff"));
//								stringBuffer.append(System.getProperty("line.separator"));
//								stringBuffer.append(Helper.getOMlogTimeStamp() + "***Fetched Destination MRC From Magento: "+destinationtariffMrcValue);
//								stringBuffer.append(System.getProperty("line.separator"));
//							
//						}
//						
//						
//						//************************************************//
//						//             Fetching User Group Data Cache
//						//************************************************//
//						
//						
//						loggerV2.info("SIZE FROM MAGENTO DESTINATION : " + tariffDetailsMagentoResponse.getData().size());
//
//						ArrayList<UsersGroupData> usersGroupData = new ArrayList<UsersGroupData>();
//						JSONObject getGroupRequest = new JSONObject();
//						getGroupRequest.put("lang", "3");
//						getGroupRequest.put("iP", "9.9.9.9");
//						getGroupRequest.put("channel", "web");
//						getGroupRequest.put("msisdn", jsonObject.getString("msisdn"));
//						getGroupRequest.put("isB2B", "true");
//						
//						getGroupRequest.put("customerID", jsonObject.getString("customerId"));
//						
//						UserGroupDataCache userCache = new UserGroupDataCache();
//						usersGroupData = (ArrayList<UsersGroupData>) userCache.checkUsersInCache(getGroupRequest.toString());
//						
//						//************************************************//
//						//             Fetching MRC value of MSISDN
//						//************************************************//
//						
//						String mrcValueofBulkTarifff = "";
//
//							loggerV2.info("***OutetArrayLength ***" + jArray.length());
//							for (int a = 0; a < jArray.length(); a++) 
//							{
//								
//								loggerV2.info("***MSISDN ***" + jArray.getJSONObject(a).get("msisdn").toString());
//								// call subscriber for NGBSS on msisdn of request
//								//************************************************//
//								//             Calling Subscriber information
//								//************************************************//
//								
//								stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
//								stringBuffer.append(System.getProperty("line.separator"));
//								stringBuffer.append(Helper.getOMlogTimeStamp() + "***Get Subscriber ");
//								stringBuffer.append(System.getProperty("line.separator"));
//								stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
//								stringBuffer.append(System.getProperty("line.separator"));
//								com.huawei.crm.query.GetSubscriberResponse response = new com.evampsaanga.bakcell.getsubscriber.CRMSubscriberService().GetSubscriberRequest(jArray.getJSONObject(a).get("msisdn").toString());
//								if (response.getResponseHeader().getRetCode().equals("0")) 
//								{
//									loggerV2.info("*** MSISDN CURRENT TariffID FROM SUBSCRIBER ***" + response.getGetSubscriberBody().getPrimaryOffering().getOfferingId().getOfferingId());
//									int indexForOffering = Helper.compare(response.getGetSubscriberBody().getPrimaryOffering().getOfferingId().getOfferingId(), tariffDetailsMagentoResponse.getData());
//									if(indexForOffering!=-1)
//									{
//										//check offering for MRC
//										mrcValueofBulkTarifff = tariffDetailsMagentoResponse.getData().get(indexForOffering).getHeader().getMrcValue();
//										stringBuffer.append(System.getProperty("line.separator"));
//										stringBuffer.append(Helper.getOMlogTimeStamp() + "*** Source MRC "+mrcValueofBulkTarifff);
//										stringBuffer.append(System.getProperty("line.separator"));
//										loggerV2.info("*** SOURCE MRC ***" + mrcValueofBulkTarifff);
//										
//										
//										//************************************************//
//										// Checking if Destination MRC is Greater than current MRC
//										//************************************************//
//										
//										
//										
//										if ((Double.parseDouble(destinationtariffMrcValue) > Double.parseDouble(mrcValueofBulkTarifff)) && !(jArray.getJSONObject(a).get("groupType").toString().equals("PaybySubs") || jArray.getJSONObject(a).get("groupType").toString().equals("PaybySub"))) 
//										{ 
//											//************************************************//
//											//             Start of Upgrade Case
//											//************************************************//
//											
//
//											stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
//											stringBuffer.append(System.getProperty("line.separator"));
//											stringBuffer.append(Helper.getOMlogTimeStamp() + "***UPGRADE ");
//											stringBuffer.append(System.getProperty("line.separator"));
//											stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
//											stringBuffer.append(System.getProperty("line.separator"));
//											loggerV2.info("*********************************************************");
//											loggerV2.info("*** START OF Upgrade CASE  ");
//											loggerV2.info("*********************************************************");
//
//											// upgrade
//											// calculation (balance
//											HLRBalanceServices service = new HLRBalanceServices();
//											Balance balance = service.getBalance(
//													jArray.getJSONObject(a).getString("msisdn").toString(), "postpaid",
//													null);
//											if (balance.getReturnCode().equals("0")) 
//											{
//
//												double mrcToBePassedChangePayment;
//												loggerV2.info("-Balance Value for CHECK : "
//														+ balance.getPostpaid().getCurrentCreditCorporateValue());
//												loggerV2.info(
//														"-Destination tariff Value : " + destinationtariffMrcValue);
//												
//												
//												//************************************************//
//												//checking if MRC is greater than current Payment Relation
//												//************************************************//
//												if (Double.parseDouble(destinationtariffMrcValue) > Double.parseDouble(
//														balance.getPostpaid().getCurrentCreditCorporateValue())) 
//												{
//													
//													
//													stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
//													stringBuffer.append(System.getProperty("line.separator"));
//													stringBuffer.append(Helper.getOMlogTimeStamp() + "***Destination MRC is greater than current credit corporate value ");
//													stringBuffer.append(System.getProperty("line.separator"));
//													stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
//													stringBuffer.append(System.getProperty("line.separator"));
//
//													loggerV2.info(
//															"*********************************************************");
//													loggerV2.info(
//															"*** START OF MRC > currentCreditCoorporate Value check  ");
//													loggerV2.info(
//															"*********************************************************");
//
//													//************************************************//
//													//             Upgrade Payment Relation to MRC + 10 
//													//************************************************//
//													mrcToBePassedChangePayment = Double
//															.parseDouble(destinationtariffMrcValue) + 10;
//													loggerV2.info("***MRCVALUE + 10  =>" + mrcToBePassedChangePayment);
//
//													loggerV2.info(
//															"*********************************************************");
//													loggerV2.info(
//															"*** END OF MRC > currentCreditCoorporate Value check  ");
//													loggerV2.info(
//															"*********************************************************");
//													loggerV2.info("-------START OF QUERY PAYMENT RELATION-----------");
//
//													stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
//													stringBuffer.append(System.getProperty("line.separator"));
//													stringBuffer.append(Helper.getOMlogTimeStamp() + "***Query Payment Relation call");
//													stringBuffer.append(System.getProperty("line.separator"));
//													stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
//													stringBuffer.append(System.getProperty("line.separator"));
//													
//													QueryPaymentRelationRequestMsg paymentRelationRequestMsg = new QueryPaymentRelationRequestMsg();
//
//													paymentRelationRequestMsg
//															.setRequestHeader(BcService.getRequestHeader());
//													QueryPaymentRelationRequest paymentRelationRequest = new QueryPaymentRelationRequest();
//
//													PaymentObj paymentObj = new PaymentObj();
//													SubAccessCode subAccessCode = new SubAccessCode();
//													subAccessCode.setPrimaryIdentity(
//															jArray.getJSONObject(a).getString("msisdn").toString());
//													paymentObj.setSubAccessCode(subAccessCode);
//													paymentRelationRequest.setPaymentObj(paymentObj);
//
//													paymentRelationRequestMsg
//															.setQueryPaymentRelationRequest(paymentRelationRequest);
//
//													QueryPaymentRelationResultMsg responseQPR = BcService.getInstance()
//															.queryPaymentRelation(paymentRelationRequestMsg);
//
//													stringBuffer.append(Helper.getOMlogTimeStamp() + "Request Packet :"+Helper.ObjectToJson(paymentRelationRequestMsg));
//													stringBuffer.append(System.getProperty("line.separator"));
//													stringBuffer.append(Helper.getOMlogTimeStamp() + "Response Packet :"+Helper.ObjectToJson(responseQPR));
//													stringBuffer.append(System.getProperty("line.separator"));
//													
//													loggerV2.info("RESPONSE QUERY PAYMET CODE :"
//															+ responseQPR.getResultHeader().getResultCode());
//													loggerV2.info("RESPONSE QUERY PAYMET MSG :"
//															+ responseQPR.getResultHeader().getResultDesc());
//													loggerV2.info("RESPONSE QUERY PAYMET MSG :" + Helper
//															.ObjectToJson(responseQPR.getQueryPaymentRelationResult()
//																	.getPaymentRelationList()));
//													
//													
//													stringBuffer.append(Helper.getOMlogTimeStamp() + "End of Query Payment Relation");
//													stringBuffer.append(System.getProperty("line.separator"));
//													loggerV2.info("-----END OF QUERY PAYMENT RELATION-----------");
//
//													//************************************************//
//													//        highest priority PayRelation 
//													//************************************************//
//													
//													PayRelation payRelationObject = Collections.max(
//															responseQPR.getQueryPaymentRelationResult()
//																	.getPaymentRelationList().getPayRelation(),
//															new PayRelationComparator());
//
//													loggerV2.info(" QUERY PAYMET Priority :"
//															+ payRelationObject.getPriority().toString());
//													loggerV2.info(
//															"QUERY PAYMET AccountKey :" + payRelationObject.getPayRelationKey());
//													/// **************************************************************************//
//
//													// END of QueryPAymentRelation
//
//													// JUST FOR TEST : Start of
//													// change PaymentRelation
//													String customerCrmAccountId=null;
//													for(int i=0;i<usersGroupData.size();i++)
//													{
//														if(usersGroupData.get(i).getMsisdn().equals(jArray.getJSONObject(a).get("msisdn").toString()))
//															customerCrmAccountId = usersGroupData.get(i).getCorp_crm_acct_id();
//													}
//													
//													CreateOrderRspMsg createOrderRspMsg = changePaymentRealtion(jArray.getJSONObject(a).get("groupType").toString(), jArray.getJSONObject(a).get("msisdn").toString(), mrcToBePassedChangePayment, payRelationObject.getPayRelationKey(),customerCrmAccountId);
//													
//													
//													//************************************************//
//													//call Change Tariff if change Payment relation is passed 
//													//************************************************//
//													if(createOrderRspMsg.getRspHeader().getReturnCode().equals("0000"))
//													{
//														changeTariff(jArray.getJSONObject(a).getString("msisdn").toString(),jsonObject.getString("destinationTariff"),stringBuffer,conn,order_id.get(index));
//													}
//													else
//													{
//														//Failed updation in DB
//														stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
//														stringBuffer.append(System.getProperty("line.separator"));
//														stringBuffer.append(Helper.getOMlogTimeStamp() + "*** Change Payment Relation is Failed ");
//														stringBuffer.append(System.getProperty("line.separator"));
//														stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
//														stringBuffer.append(System.getProperty("line.separator"));
//														updateOrderDetails(order_id.get(index),
//																jArray.getJSONObject(a).getString("msisdn").toString(), "F",
//																createOrderRspMsg.getRspHeader().getReturnCode(),
//																"Change Payment Relation::" + createOrderRspMsg.getRspHeader().getReturnMsg()
//																		+ jsonObject.getString("msisdn"),
//																conn, stringBuffer);
////														updateOrderStatus(order_id.get(index), "F", conn, stringBuffer);
//													}
//
//												} 
//												else // else for : mrc is less than current Payment Relation
//												{
//													
//													stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
//													stringBuffer.append(System.getProperty("line.separator"));
//													stringBuffer.append(Helper.getOMlogTimeStamp() + "***Destination MRC is less than or equal to current credit corporate value ");
//													stringBuffer.append(System.getProperty("line.separator"));
//													stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
//													stringBuffer.append(System.getProperty("line.separator"));
//
//													loggerV2.info(
//															"*********************************************************");
//													loggerV2.info(
//															"***Destination MRC is less than or equal to current credit corporate value ");
//													loggerV2.info(
//															"*********************************************************");
//
//													// creditCorporate value
//													// extracted from balance
//													Double availableCorporateValue = Math.abs(Double.parseDouble(balance.getPostpaid().getBalanceCorporateValue()));
//													loggerV2.info("*** AvailableCorporateValue *** "+ Math.abs(availableCorporateValue));
//													/*
//													 * int
//													 * currentPaymentRelation =
//													 * Integer.parseInt(
//													 * balance.getPostpaid().
//													 * getCurrentCreditCorporateValue
//													 * ());
//													 */
//
//													double currentPaymentRelation = Double.parseDouble(
//															balance.getPostpaid().getCurrentCreditCorporateValue());
//
//													loggerV2.info("*** CurrrentPaymentRelation *** "
//															+ currentPaymentRelation);
//													// local date variable for
//													// manipulation of date
//													LocalDate localDate = LocalDate.now();
//													int daysLeft = localDate.lengthOfMonth()
//															- localDate.getDayOfMonth();
//													
//													loggerV2.info("LocalDate: "+localDate.toString());
//													loggerV2.info("daysLeft :" + daysLeft);
//													// System.out.println(daysLeft);
//													int daysPast = localDate.lengthOfMonth() - daysLeft - 1;
//													loggerV2.info("daysPast :" + daysPast);
//													// System.out.println(daysPast);
//													loggerV2.info("CurrentMRC :" + mrcValueofBulkTarifff);
//													loggerV2.info("DestinationMrC :" + destinationtariffMrcValue);
//													
//													loggerV2.info("**************************************************");
//													loggerV2.info("availableCorporateValue: " + availableCorporateValue);
//													loggerV2.info("lengthOfMonth: " + localDate.lengthOfMonth());
//													loggerV2.info("Double.parseDouble(mrcValueofBulkTarifff)/localDate.lengthOfMonth(): " + (Double.parseDouble(mrcValueofBulkTarifff)/ localDate.lengthOfMonth()) * daysPast);
//													loggerV2.info("(Double.parseDouble(destinationtariffMrcValue)/ localDate.getDayOfMonth())* (daysLeft + 1)" + (Double.parseDouble(destinationtariffMrcValue)/ localDate.lengthOfMonth())* (daysLeft + 1));
//													
//													
//													
//													double formular = availableCorporateValue- (Double.parseDouble(mrcValueofBulkTarifff)/ localDate.lengthOfMonth() * daysPast)+ (Double.parseDouble(destinationtariffMrcValue)/ localDate.lengthOfMonth())* (daysLeft + 1);
//													loggerV2.info("calculatedValues: " + formular);
//													
//													loggerV2.info("**************************************************");
//													
//													//************************************************//
//													//checking with the formula value 
//													//************************************************//
//													if (formular > currentPaymentRelation) 
//													{
//														
//														
//														stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
//														stringBuffer.append(System.getProperty("line.separator"));
//														stringBuffer.append(Helper.getOMlogTimeStamp() + "***Formula passed for change Tarif with days and months");
//														stringBuffer.append(System.getProperty("line.separator"));
//														stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
//														stringBuffer.append(System.getProperty("line.separator"));
//														
//														
//														String mrcToBePassedChangePaymentint = Helper
//																.round((int)formular);
//
//														mrcToBePassedChangePayment = Double
//																.parseDouble(mrcToBePassedChangePaymentint);
//
//														loggerV2.info("*** MRCVALUE Formula Result =>"
//																+ mrcToBePassedChangePayment);
//														loggerV2.info("-------START OF QUERY PAYMENT RELATION-----------");
//
//														stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
//														stringBuffer.append(System.getProperty("line.separator"));
//														stringBuffer.append(Helper.getOMlogTimeStamp() + "***calling Query Payment Relation call");
//														stringBuffer.append(System.getProperty("line.separator"));
//														stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
//														stringBuffer.append(System.getProperty("line.separator"));
//														
//														QueryPaymentRelationRequestMsg paymentRelationRequestMsg = new QueryPaymentRelationRequestMsg();
//
//														paymentRelationRequestMsg
//																.setRequestHeader(BcService.getRequestHeader());
//														QueryPaymentRelationRequest paymentRelationRequest = new QueryPaymentRelationRequest();
//
//														PaymentObj paymentObj = new PaymentObj();
//														SubAccessCode subAccessCode = new SubAccessCode();
//														subAccessCode.setPrimaryIdentity(
//																jArray.getJSONObject(a).getString("msisdn").toString());
//														paymentObj.setSubAccessCode(subAccessCode);
//														paymentRelationRequest.setPaymentObj(paymentObj);
//
//														paymentRelationRequestMsg
//																.setQueryPaymentRelationRequest(paymentRelationRequest);
//
//														QueryPaymentRelationResultMsg responseQPR = BcService.getInstance()
//																.queryPaymentRelation(paymentRelationRequestMsg);
//
//														stringBuffer.append(Helper.getOMlogTimeStamp() + "Request Packet :"+Helper.ObjectToJson(paymentRelationRequestMsg));
//														stringBuffer.append(System.getProperty("line.separator"));
//														stringBuffer.append(Helper.getOMlogTimeStamp() + "Response Packet :"+Helper.ObjectToJson(responseQPR));
//														stringBuffer.append(System.getProperty("line.separator"));
//														
//														loggerV2.info("RESPONSE QUERY PAYMET CODE :"
//																+ responseQPR.getResultHeader().getResultCode());
//														loggerV2.info("RESPONSE QUERY PAYMET MSG :"
//																+ responseQPR.getResultHeader().getResultDesc());
//														loggerV2.info("RESPONSE QUERY PAYMET MSG :" + Helper
//																.ObjectToJson(responseQPR.getQueryPaymentRelationResult()
//																		.getPaymentRelationList()));
//														
//														
//														stringBuffer.append(Helper.getOMlogTimeStamp() + "End of Query Payment Relation");
//														stringBuffer.append(System.getProperty("line.separator"));
//														loggerV2.info("-----END OF QUERY PAYMENT RELATION-----------");
//
//														// *********************Get
//														// highest priority PayRelation
//														// ********************//
//														PayRelation payRelationObject = Collections.max(
//																responseQPR.getQueryPaymentRelationResult()
//																		.getPaymentRelationList().getPayRelation(),
//																new PayRelationComparator());
//
//														loggerV2.info("QUERY PAYMET Priority :"+ payRelationObject.getPriority().toString());
//														loggerV2.info("Key passed in Add payment Relation :" + jArray.getJSONObject(a).get("corpCrmCustName").toString());
//														/// **************************************************************************//
//
//														// END of QueryPAymentRelation
//
//														// JUST FOR TEST : Start of
//														// change PaymentRelation
//														String customerCrmAccountId=null;
//														for(int i=0;i<usersGroupData.size();i++)
//														{
//															if(usersGroupData.get(i).getMsisdn().equals(jArray.getJSONObject(a).get("msisdn").toString()))
//																customerCrmAccountId = usersGroupData.get(i).getCorp_crm_acct_id();
//														}
//														CreateOrderRspMsg createOrderRspMsg = changePaymentRealtion(jArray.getJSONObject(a).get("groupType").toString()
//																				,jArray.getJSONObject(a).get("msisdn").toString()
//																				,mrcToBePassedChangePayment
//																				,payRelationObject.getPayRelationKey()
//																				,customerCrmAccountId);
//														
//														
//														//************************************************//
//														//call Change Tariff if change Payment relation is passed 
//														//************************************************//
//														loggerV2.info(
//																"*********************************************************");
//														loggerV2.info(
//																"***Calling Change Tariff  ");
//														loggerV2.info(
//																"*********************************************************");
//														if(createOrderRspMsg.getRspHeader().getReturnCode().equals("0"))
//														{
//															changeTariff(jArray.getJSONObject(a).getString("msisdn").toString(),jsonObject.getString("destinationTariff"),stringBuffer,conn,order_id.get(index));
//															
//														}
//														else // if change payment relation failed
//														{
//															//Failed updation in DB
//															stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
//															stringBuffer.append(System.getProperty("line.separator"));
//															stringBuffer.append(Helper.getOMlogTimeStamp() + "*** Change Payment Relation is Failed ");
//															stringBuffer.append(System.getProperty("line.separator"));
//															stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
//															stringBuffer.append(System.getProperty("line.separator"));
//															updateOrderDetails(order_id.get(index),
//																	jArray.getJSONObject(a).getString("msisdn").toString(), "F",
//																	createOrderRspMsg.getRspHeader().getReturnCode(),
//																	"Change Payment Relation::" + createOrderRspMsg.getRspHeader().getReturnMsg()
//																			+ jsonObject.getString("msisdn"),
//																	conn, stringBuffer);
////															updateOrderStatus(order_id.get(index), "F", conn, stringBuffer);
//														}
//													} 
//													else
//													{
//														// a formula if failed,we have to upgrade destination MRc+10,and then we wil pass with updated values to the changePaymentRelation
//														
//														stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
//														stringBuffer.append(System.getProperty("line.separator"));
//														stringBuffer.append(Helper.getOMlogTimeStamp() + "***Formula is Failed ");
//														stringBuffer.append(System.getProperty("line.separator"));
//														stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
//														stringBuffer.append(System.getProperty("line.separator"));
//														
//														
//														loggerV2.info(
//																"*********************************************************");
//														loggerV2.info(
//																"***Calling Change Tariff  ");
//														loggerV2.info(
//																"*********************************************************");
//														//************************************************//
//														//call Change Tariff if change Payment relation is passed 
//														//************************************************//
//														changeTariff(jArray.getJSONObject(a).getString("msisdn").toString(),jsonObject.getString("destinationTariff"),stringBuffer,conn,order_id.get(index));
//													}	
//												}
//											} // END OF IF Balance -->
//												// successful
//											else { // START of else Balance -->
//													// balance resposne is not
//													// sucessfulll, update
//													// orderdetais for failure
//												
//												
//												stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
//												stringBuffer.append(System.getProperty("line.separator"));
//												stringBuffer.append(Helper.getOMlogTimeStamp() + "***Balance Failed ");
//												stringBuffer.append(System.getProperty("line.separator"));
//												stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
//												stringBuffer.append(System.getProperty("line.separator"));
//												updateOrderDetails(order_id.get(index),
//														jArray.getJSONObject(a).getString("msisdn").toString(), "F",
//														balance.getReturnCode(),
//														"BalancAPI::" + balance.getReturnMsg()
//																+ jsonObject.getString("msisdn"),
//														conn, stringBuffer);
//											} // END of else balance
//
//											stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
//											stringBuffer.append(System.getProperty("line.separator"));
//											stringBuffer.append(Helper.getOMlogTimeStamp() + "***END OF Upgrade CASE ");
//											stringBuffer.append(System.getProperty("line.separator"));
//											stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
//											stringBuffer.append(System.getProperty("line.separator"));
//											
//											loggerV2.info("*********************************************************");
//											loggerV2.info("*** END OF Upgrade CASE  ");
//											loggerV2.info("*********************************************************");
//
//										} // End OF IF Upgrade CHECK
//										else 
//										{ 
//											//************************************************//
//											// MRC is less than or equal to new Tariff 
//											//************************************************//
//											//************************************************//
//											// Downgrade Scenario 
//											//************************************************//
//											
//											stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
//											stringBuffer.append(System.getProperty("line.separator"));
//											stringBuffer.append(Helper.getOMlogTimeStamp() + "***Downgrade Case ");
//											stringBuffer.append(System.getProperty("line.separator"));
//											stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
//											stringBuffer.append(System.getProperty("line.separator"));
//											loggerV2.info("*********************************************************");
//											loggerV2.info("*** DOWNGRADE  ");
//											loggerV2.info("*********************************************************");
//
//											// checkPermission
//											String groupTypeValuePermissions = jArray.getJSONObject(a).get("groupType")
//													.toString() + "-Downgrade";
//											loggerV2.info("PERMISSION VALUE :" + groupTypeValuePermissions);
//											loggerV2.info("PERMISSION VALUES STRING :"
//													+ jsonObject.get("tariffPermissions").toString());
//											if ((jsonObject.get("tariffPermissions").toString()).contains(groupTypeValuePermissions)) { // START of IF Permissions CHECK
//
//												loggerV2.info("SUCCESS PERMISSION CHECK :");
//												// changeTariff
//												stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
//												stringBuffer.append(System.getProperty("line.separator"));
//												stringBuffer.append(Helper.getOMlogTimeStamp() + "***Calling Change Tariff ");
//												stringBuffer.append(System.getProperty("line.separator"));
//												stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
//												stringBuffer.append(System.getProperty("line.separator"));
//												
//												ChangeTarrifRequestLand changetariff = new ChangeTarrifRequestLand();
//												SubmitOrderResponse responseSubmitOrder = changetariff.changetarrifresponse(
//														jArray.getJSONObject(a).getString("msisdn").toString(),
//														jsonObject.getString("destinationTariff"));
//												if (responseSubmitOrder.getResponseHeader().getRetCode().equals("0")) {
//
//													loggerV2.info("SUCCESS RESPONSEcode in ChangeTariff :"
//															+ responseSubmitOrder.getResponseHeader().getRetCode());
//													updateOrderDetails(order_id.get(index),
//															jArray.getJSONObject(a).getString("msisdn").toString(), "C",
//															ResponseCodes.SUCESS_CODE_200, ResponseCodes.SUCESS_DES_200,
//															conn, stringBuffer);
//												} else {
//
//													loggerV2.info("FAILED RESPONSE CODE in ChangeTariff :"
//															+ responseSubmitOrder.getResponseHeader().getRetCode());
//													loggerV2.info("FAILED RESPONSE MEssage in ChangeTariff :"
//															+ responseSubmitOrder.getResponseHeader().getRetMsg());
//													updateOrderDetails(order_id.get(index),
//															jArray.getJSONObject(a).getString("msisdn").toString(), "F",
//															responseSubmitOrder.getResponseHeader().getRetCode(),
//															responseSubmitOrder.getResponseHeader().getRetMsg()
//																	+ jsonObject.getString("msisdn"),
//															conn, stringBuffer);
//													loggerV2.info("---CHANGE TARIFF UPDATED---");
//												} // END of chageTariff
//											} // END of IF , Permissions CHECK
//											else {
//
//												loggerV2.info("FAILED PERMISSION CHECK, NOT ALLOWED:");
//												updateOrderDetails(order_id.get(index),
//														jArray.getJSONObject(a).getString("msisdn").toString(), "F",
//														"5000",
//														"No Permission for downgrade to ths pic"
//																+ jsonObject.getString("msisdn"),
//														conn, stringBuffer);
//											}
//
//										}
//										
//										
//										
//									}
//									else
//									{
//										//update order as failed because offering is not found in Magento Response
//										updateOrderDetails(order_id.get(index),
//												jArray.getJSONObject(a).getString("msisdn").toString(), "F",
//												"5000",
//												"unable to get primary offering for fetching MRC"
//														+ jsonObject.getString("msisdn"),
//												conn, stringBuffer);
////										updateOrderStatus(order_id.get(index), "F", conn, stringBuffer);
//									}	
//								}
//								else
//								{
//									//update order is failed because subscriber is failed
//									updateOrderDetails(order_id.get(index),
//											jArray.getJSONObject(a).getString("msisdn").toString(), "F",
//											response.getResponseHeader().getRetCode(),
//											response.getResponseHeader().getRetMsg()+":"
//													+ jsonObject.getString("msisdn"),
//											conn, stringBuffer);
////									updateOrderStatus(order_id.get(index), "F", conn, stringBuffer);
//								}
//
//
//									}
//								}
//				 catch (Exception e) {
//					// updating purchase order
//					updateOrderStatus(order_id.get(index), "F", conn, stringBuffer);
//					loggerV2.error(Helper.GetException(e));
//				}
//					updateOrderStatus(order_id.get(index), "C", conn, stringBuffer);
//				
//				}
//				else if (type.get(index).equals(Constants.CHANGEL_PAYMENT_RELATION)) 
//				{
//
//				     stringBuffer
//				       .append(Helper.getOMlogTimeStamp() + "************************************************");
//				     stringBuffer.append(System.getProperty("line.separator"));
//				     stringBuffer.append(Helper.getOMlogTimeStamp() + "*** " + Constants.CHANGEL_PAYMENT_RELATION);
//				     stringBuffer.append(System.getProperty("line.separator"));
//				     stringBuffer
//				       .append(Helper.getOMlogTimeStamp() + "************************************************");
//				     stringBuffer.append(System.getProperty("line.separator"));
//
//				     // update the main table status
//				     updateOrderStatus(order_id.get(index), "P", conn, stringBuffer);
//				     // get the order attributes so that further processing can
//				     // be done
//				     try {
//				      JSONObject jsonObject = getOrderAttributes(order_id.get(index), conn, stringBuffer);
//				      loggerV2.info("Order Attributes: " + jsonObject);
//				      // request packet to process
//				      JSONArray jArray = jsonObject.getJSONArray("users");
//				      jsonObject.remove("users");
//				      jsonObject.remove("type");
//				      jsonObject.remove("orderKey");
//
//				      stringBuffer.append(
//				        Helper.getOMlogTimeStamp() + "************************************************");
//				      stringBuffer.append(System.getProperty("line.separator"));
//				      stringBuffer.append(Helper.getOMlogTimeStamp()
//				        + "***Fetch data from order details for Change Payment Relation");
//				      stringBuffer.append(System.getProperty("line.separator"));
//				      stringBuffer.append(
//				        Helper.getOMlogTimeStamp() + "************************************************");
//				      stringBuffer.append(System.getProperty("line.separator"));
//
//				      ChangePaymentRelationBulkProcess processchane = new ChangePaymentRelationBulkProcess();
//				      processchane.processOrder(jsonObject, jArray, stringBuffer,order_id.get(index),conn, loggerV2);
//
//				     } catch (Exception e) {
//				      // updating purchase order
//				      updateOrderStatus(order_id.get(index), "F", conn, stringBuffer);
//				      loggerV2.error(Helper.GetException(e));
//				     }
//				    }
//
//				    // End of Change PAyment Relation elseIF
//				else if(type.get(index).equals(Constants.CHANGEL_GROUP_TYPE))
//
//			    {
//			     loggerV2.info("CHANGE GROUP");
//			     
//			     // update the main table status
//			     updateOrderStatus(order_id.get(index), "P", conn, stringBuffer);
//			     // get the order attributes so that further processing can
//			     // be done
//			     
//			     JSONObject jsonObject = getOrderAttributes(order_id.get(index), conn, stringBuffer);
//			     
//			     ChangeGroup changeGroup = new ChangeGroup(jsonObject,stringBuffer,conn);
//			     stringBuffer = changeGroup.processOrder(order_id.get(index));
//			     loggerV2.info("Order Attributes: " +jsonObject);
//			     
//			     
//			    }
//
//				//close files
//				java.io.File file = new java.io.File(path+fileName);
//				FileWriter fileWriter = new FileWriter(file);
//				BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
//				//write contents of StringBuffer to a file
//				bufferedWriter.write(stringBuffer.toString());
//				
//				//flush the stream
//				bufferedWriter.flush();
//				
//				//close the stream
//				bufferedWriter.close();
//				conn.close();
//				
//			}
//		} catch (Exception e) {
//			
//			loggerV2.error(Helper.GetException(e));
//			logs.setResponseCode(ResponseCodes.ERROR_400_CODE);
//			logs.setResponseDescription(e.getMessage());
//			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
//			logs.updateLog(logs);
//			return;
//		}
//		
//	}

//	private void changeTariff(String msisdn,String destinationTariff,StringBuffer stringBuffer,Connection conn, String order) throws SQLException, InterruptedException {
//		//TODO Auto-generated method stub
//		stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
//		stringBuffer.append(System.getProperty("line.separator"));
//		stringBuffer.append(Helper.getOMlogTimeStamp() + "***Calling Change Tariff");
//		stringBuffer.append(System.getProperty("line.separator"));
//		stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
//		stringBuffer.append(System.getProperty("line.separator"));
//		
//		loggerV2.info("---Holding call sleep of 10_sec: " + System.currentTimeMillis());
//		Thread.sleep(10000);
//		ChangeTarrifRequestLand changetariff = new ChangeTarrifRequestLand();
//		SubmitOrderResponse responseSubmitOrder = changetariff.changetarrifresponse(
//				msisdn,
//				destinationTariff);
//
//		// START OF CHANGE TARIFF
//		if (responseSubmitOrder.getResponseHeader().getRetCode().equals("0")) 
//		{
//			loggerV2.info("SUCCESS RESPONSEcode in ChangeTariff :"
//					+ responseSubmitOrder.getResponseHeader().getRetCode());
//			updateOrderDetails(order,
//					msisdn,
//					"C", 
//					ResponseCodes.SUCESS_CODE_200,
//					ResponseCodes.SUCESS_DES_200, conn,
//					stringBuffer);
////			updateOrderStatus(order, "C", conn, stringBuffer);
//			
//		} // END of chageTariff IF ,
//			// Suceessful
//		else {// START of
//				// chageTariff ELSE
//
//			loggerV2.info("FAILED RESPONSEcode in ChangeTariff :"
//					+ responseSubmitOrder.getResponseHeader().getRetCode() + "-Msg-"
//					+ responseSubmitOrder.getResponseHeader().getRetMsg());
//			updateOrderDetails(order,
//					msisdn,
//					"F", responseSubmitOrder.getResponseHeader().getRetCode(),
//					"ChangeTariffAPI::"
//							+ responseSubmitOrder.getResponseHeader().getRetMsg()
//							+ msisdn,
//					conn, stringBuffer);
////			updateOrderStatus(order, "F", conn, stringBuffer);
//		} // END of chageTariff ELSE
//	}
//
//	/**
//	 * helper function to update order details
//	 * 
//	 * @param order_id
//	 * @param msisdn
//	 * @param status
//	 * @param responseCode
//	 * @param responseDesc
//	 * @param conn
//	 * @param stringBuffer
//	 * @return Returns true if the update is successful else false
//	 * @throws SQLException
//	 */
//	@Transactional(rollbackFor = Exception.class)
//	public static boolean updateOrderDetails(String order_id, String msisdn, String status, String responseCode,
//			String responseDesc, Connection conn, StringBuffer stringBuffer)
//			throws SQLException {
//		if(conn.isClosed())
//			conn = DBFactory.getDbConnection();
//		loggerV2.info("=======now IN updateOrderDetails Method=======");
//		
//		  stringBuffer.append(Helper.getOMlogTimeStamp() +"************************************************");
//		  stringBuffer.append(System.getProperty("line.separator"));
//		  stringBuffer.append(Helper.getOMlogTimeStamp() +"*** Update Order Details  ");
//		  stringBuffer.append(System.getProperty("line.separator"));
//		  stringBuffer.append(Helper.getOMlogTimeStamp() +"************************************************");
//		  stringBuffer.append(System.getProperty("line.separator"));
//		 
//		String query = "";
//		if (msisdn.equals("")) {
//			query = "update order_details set status = ?, responsecode=?, responsedescription=? where order_id_fk= ? and status='F'";
//		} else
//			query = "update order_details set status = ?, responsecode=?, responsedescription=? where order_id_fk= ? and msisdn=?";
//		// String
//		PreparedStatement preparedStmt = conn.prepareStatement(query);
//		preparedStmt = conn.prepareStatement(query);
//		preparedStmt.setString(1, status);
//		preparedStmt.setString(2, responseCode);
//		preparedStmt.setString(3, responseDesc);
//		preparedStmt.setString(4, order_id);
//		if (!msisdn.equals(""))
//			preparedStmt.setString(5, msisdn);
//		
//		  stringBuffer.append(Helper.getOMlogTimeStamp() +"Execute Query : " + preparedStmt.toString());
//		  stringBuffer.append(System.getProperty("line.separator"));
//		 
//		loggerV2.info("QUERY for update Order_purchase" + preparedStmt.toString());
//		preparedStmt.execute();
//		int count = preparedStmt.getUpdateCount();
//		loggerV2.info("Count In updateOrderDetails " + count);
//		if (count > 0) {
//			conn.close();
//			stringBuffer.append(Helper.getOMlogTimeStamp() + "Record updated : ");
//			stringBuffer.append(System.getProperty("line.separator"));
//			return true;
//		} else {
//			conn.close();
//			 stringBuffer.append(Helper.getOMlogTimeStamp() + "Record updation failed : ");
//			 stringBuffer.append(System.getProperty("line.separator"));
//			return false;
//		}
//	}
//
//	/**
//	 * Fetches order details from DB
//	 * 
//	 * @param order_id
//	 * @param conn
//	 * @param stringBuffer2
//	 * @return Json object containing order details
//	 * @throws IOException
//	 * @throws Exception
//	 */
//	@SuppressWarnings("resource")
//	private JSONObject getOrderAttributes(String order_id, Connection conn,
//			StringBuffer stringBuffer2) throws IOException, Exception {
//
//		stringBuffer2.append(Helper.getOMlogTimeStamp() + "************************************************");
//		stringBuffer2.append(System.getProperty("line.separator"));
//		stringBuffer2.append(Helper.getOMlogTimeStamp() + "*** Select Data from Order Attributes  ");
//		stringBuffer2.append(System.getProperty("line.separator"));
//		stringBuffer2.append(Helper.getOMlogTimeStamp() + "************************************************");
//		stringBuffer2.append(System.getProperty("line.separator"));
//
//		String query = "select key_value, value from order_attributes where order_id_fk =?";
//		PreparedStatement preparedStmt = conn.prepareStatement(query);
//		preparedStmt.setString(1, order_id);
//		stringBuffer2.append(Helper.getOMlogTimeStamp() + " Executing Query :" + preparedStmt.toString());
//		ResultSet resultSet = preparedStmt.executeQuery();
//		JSONObject jsonObject = new JSONObject();
//		while (resultSet.next()) {
//			jsonObject.put(resultSet.getString(1), (resultSet.getString(2)));
//			stringBuffer2.append(Helper.getOMlogTimeStamp() + resultSet.getString(1) + " : " + resultSet.getString(2));
//
//		}
//		if (jsonObject.getString("type").toString().equals("BroadcastSMS")) 
//		{
//
//			stringBuffer2.append(Helper.getOMlogTimeStamp() + "************************************************");
//			stringBuffer2.append(System.getProperty("line.separator"));
//			stringBuffer2.append(Helper.getOMlogTimeStamp() + "*** Select Data from Order Details For Broadcast SMS  ");
//			stringBuffer2.append(System.getProperty("line.separator"));
//			stringBuffer2.append(Helper.getOMlogTimeStamp() + "************************************************");
//			stringBuffer2.append(System.getProperty("line.separator"));
//
//			query = "select msisdn from order_details where order_id_fk =?";
//			preparedStmt = conn.prepareStatement(query);
//			preparedStmt.setString(1, order_id);
//			stringBuffer2.append(Helper.getOMlogTimeStamp() + "Executing Query : " + preparedStmt.toString());
//			stringBuffer2.append(System.getProperty("line.separator"));
//
//			resultSet = preparedStmt.executeQuery();
//			StringBuffer stringBuffer = new StringBuffer();
//			while (resultSet.next()) {
//
//				stringBuffer.append(resultSet.getString(1)).append(",");
//				stringBuffer2.append(Helper.getOMlogTimeStamp() + " MSISDN: " + resultSet.getString(1));
//
//			}
//			stringBuffer.replace(stringBuffer.length() - 1, stringBuffer.length(), "");
//			jsonObject.put("recieverMsisdn", stringBuffer.toString());
//
//		} else if (jsonObject.getString("type").toString().equals("Process Core Services")) {
//
//			stringBuffer2.append(Helper.getOMlogTimeStamp() + "************************************************");
//			stringBuffer2.append(System.getProperty("line.separator"));
//			stringBuffer2.append(Helper.getOMlogTimeStamp() + "*** Select Data from Order Details PCS  ");
//			stringBuffer2.append(System.getProperty("line.separator"));
//			stringBuffer2.append(Helper.getOMlogTimeStamp() + "************************************************");
//			stringBuffer2.append(System.getProperty("line.separator"));
//
//			query = "select msisdn from order_details where order_id_fk =?";
//			preparedStmt = conn.prepareStatement(query);
//			preparedStmt.setString(1, order_id);
//			stringBuffer2.append(Helper.getOMlogTimeStamp() + "Executing Query : " + preparedStmt.toString());
//			resultSet = preparedStmt.executeQuery();
//			JSONArray jArray = new JSONArray();
//			while (resultSet.next()) {
//				JSONObject jobj = new JSONObject();
//				jobj.put("msisdn", resultSet.getString(1));
//				stringBuffer2.append(Helper.getOMlogTimeStamp() + " MSISDN: " + resultSet.getString(1));
//				stringBuffer2.append(System.getProperty("line.separator"));
//				jArray.put(jobj);
//			}
//			jsonObject.put("users", jArray);
//		} else if (jsonObject.getString("type").toString().equals("Change Supplementary Offerings")) {
//
//			stringBuffer2.append(Helper.getOMlogTimeStamp() + "************************************************");
//			stringBuffer2.append(System.getProperty("line.separator"));
//			stringBuffer2.append(Helper.getOMlogTimeStamp() + "***Select Data from Order Details Change Supplemantry ");
//			stringBuffer2.append(System.getProperty("line.separator"));
//			stringBuffer2.append(Helper.getOMlogTimeStamp() + "************************************************");
//			stringBuffer2.append(System.getProperty("line.separator"));
//
//			query = "select msisdn,group_type from order_details where order_id_fk =?";
//			preparedStmt = conn.prepareStatement(query);
//			preparedStmt.setString(1, order_id);
//			stringBuffer2.append(Helper.getOMlogTimeStamp() + "Executing Query : " + preparedStmt.toString());
//			stringBuffer2.append(System.getProperty("line.separator"));
//			resultSet = preparedStmt.executeQuery();
//			JSONArray jArray = new JSONArray();
//			while (resultSet.next()) {
//				JSONObject jobj = new JSONObject();
//				jobj.put("msisdn", resultSet.getString(1));
//				stringBuffer2.append(Helper.getOMlogTimeStamp() + " MSISDN: " + resultSet.getString(1));
//				stringBuffer2.append(System.getProperty("line.separator"));
//				jobj.put("groupType", resultSet.getString(2));
//				stringBuffer2.append(Helper.getOMlogTimeStamp() + " GROUP_TYPE: " + resultSet.getString(2));
//				stringBuffer2.append(System.getProperty("line.separator"));
//				jArray.put(jobj);
//			}
//			jsonObject.put("users", jArray);
//		} else if (jsonObject.getString("type").toString().equalsIgnoreCase("Change Tariff") || jsonObject.getString("type").toString().equalsIgnoreCase(Constants.CHANGEL_PAYMENT_RELATION) ) 
//		{
//			query = "select msisdn,group_type,tariff_ids,corpCrmCustName from order_details where order_id_fk =?";
//			preparedStmt = conn.prepareStatement(query);
//			preparedStmt.setString(1, order_id);
//			stringBuffer2.append(Helper.getOMlogTimeStamp() + "Executing Query : " + preparedStmt.toString());
//			stringBuffer2.append(System.getProperty("line.separator"));
//			resultSet = preparedStmt.executeQuery();
//			JSONArray jArray = new JSONArray();
//			while (resultSet.next()) 
//			{
//				JSONObject jobj = new JSONObject();
//				jobj.put("msisdn", resultSet.getString(1));
//				stringBuffer2.append(Helper.getOMlogTimeStamp() + " MSISDN: " + resultSet.getString(1));
//				stringBuffer2.append(System.getProperty("line.separator"));
//				jobj.put("groupType", resultSet.getString(2));
//				stringBuffer2.append(Helper.getOMlogTimeStamp() + " GROUP_TYPE: " + resultSet.getString(2));
//				stringBuffer2.append(System.getProperty("line.separator"));
//				jobj.put("tariff_ids", resultSet.getString(3));
//				stringBuffer2.append(Helper.getOMlogTimeStamp() + " TARIFF_IDS: " + resultSet.getString(3));
//				stringBuffer2.append(System.getProperty("line.separator"));
//				jobj.put("corpCrmCustName", resultSet.getString(4));
//				stringBuffer2.append(Helper.getOMlogTimeStamp() + " corpCrmCustName: " + resultSet.getString(4));
//				stringBuffer2.append(System.getProperty("line.separator"));
//				jArray.put(jobj);
//			}
//			jsonObject.put("users", jArray);
//		}
//		 else if (jsonObject.getString("type").toString().equals("Change Group")) 
//		 {
//				query = "select msisdn,group_type,groupIdFrom,accountID,tariff_ids,corpCrmCustName from order_details where order_id_fk =?";
//				preparedStmt = conn.prepareStatement(query);
//				preparedStmt.setString(1, order_id);
//				stringBuffer2.append(Helper.getOMlogTimeStamp() + "Executing Query : " + preparedStmt.toString());
//				stringBuffer2.append(System.getProperty("line.separator"));
//				resultSet = preparedStmt.executeQuery();
//				JSONArray jArray = new JSONArray();
//				while (resultSet.next()) {
//					JSONObject jobj = new JSONObject();
//					jobj.put("msisdn", resultSet.getString(1));
//					stringBuffer2.append(Helper.getOMlogTimeStamp() + " MSISDN: " + resultSet.getString(1));
//					stringBuffer2.append(System.getProperty("line.separator"));
//					jobj.put("groupType", resultSet.getString(2));
//					stringBuffer2.append(Helper.getOMlogTimeStamp() + " GROUP_TYPE: " + resultSet.getString(2));
//					stringBuffer2.append(System.getProperty("line.separator"));
//					jobj.put("groupIdFrom", resultSet.getString(3));
//					stringBuffer2.append(Helper.getOMlogTimeStamp() + " GroupIDFrom: " + resultSet.getString(3));
//					stringBuffer2.append(System.getProperty("line.separator"));
//					
//					jobj.put("accountID", resultSet.getString(4));
//					stringBuffer2.append(Helper.getOMlogTimeStamp() + " accountID: " + resultSet.getString(4));
//					stringBuffer2.append(System.getProperty("line.separator"));
//					
//					jobj.put("tariffIds", resultSet.getString(5));
//					stringBuffer2.append(Helper.getOMlogTimeStamp() + " tariff_ids: " + resultSet.getString(5));
//					stringBuffer2.append(System.getProperty("line.separator"));
//					
//					
//					jobj.put("corpCrmCustName", resultSet.getString(6));
//					stringBuffer2.append(Helper.getOMlogTimeStamp() + " corpCrmCustName: " + resultSet.getString(6));
//					stringBuffer2.append(System.getProperty("line.separator"));
//					
//					jArray.put(jobj);
//				}
//				jsonObject.put("recieverMsisdn", jArray);
//				jsonObject.remove("type");
//			}
//		
//		loggerV2.info("attributes are: " + jsonObject);
//		resultSet.close();
//		preparedStmt.close();
//		return jsonObject;
//
//	}

	/**
	 * Fetches order type from DB
	 * 
	 * @param conn
	 * @return Returns a map containing array list of orders with types
	 * @throws SQLException
	 * @throws JSONException
	 */
	private Map<String, ArrayList<String>> getOrderType(Connection conn) throws SQLException, JSONException {
		String query = "select type,order_id from purchase_order where status =? AND host=?";
		PreparedStatement preparedStmt = conn.prepareStatement(query);
		preparedStmt.setString(1, "N");
		preparedStmt.setString(2, Helper.currentMachineIPAddress());
		ResultSet rSet = preparedStmt.executeQuery();
		Map<String, ArrayList<String>> map = new HashMap<>();
		ArrayList<String> type = new ArrayList<>();
		ArrayList<String> order_id = new ArrayList<>();
		while (rSet.next()) {
			type.add(rSet.getString("TYPE"));
			order_id.add(rSet.getString("ORDER_ID"));
			loggerV2.info("Pending Order Type is: " + rSet.getString("TYPE") + "  >>>>> <<<<< Order Id is: "
					+ rSet.getString("ORDER_ID"));
		}
		map.put("Type", type);
		map.put("Order_Id", order_id);
		rSet.close();
		preparedStmt.close();
		return map;
	}

	/**
	 * 
	 */
	@Transactional(rollbackFor = Exception.class)
	private boolean updateOrderStatusForRetry(String order_id, String status, Connection conn) throws SQLException {
		String query = "update purchase_order as po, order_details as od set po.`status`='N', po.retry_count=po.retry_count+1, od.`status`='P' where od.`status`='F' and po.order_id=? and od.order_id_fk=?";
		PreparedStatement preparedStmt = conn.prepareStatement(query);
		preparedStmt = conn.prepareStatement(query);
		preparedStmt.setString(1, order_id);
		preparedStmt.setString(2, order_id);
		loggerV2.info("QUERY for update Order_purchase" + preparedStmt);
		preparedStmt.execute();
		int count = preparedStmt.getUpdateCount();
		if (count > 0)
			return true;
		else
			return false;
	}

	/**
	 * helper function to update order status upon processing
	 * 
	 * @param order_id
	 * @param status
	 * @param conn
	 * @param stringBuffer
	 * @return Returns true if any failed orders found else false
	 * @throws SQLException
	 */
	@Transactional(rollbackFor = Exception.class)
	public static boolean updateOrderStatus(String order_id, String status, Connection conn, StringBuffer stringBuffer)
			throws SQLException {
		String query = "update purchase_order set status = ? where order_id= ?";
		if (conn.isClosed())
			conn = DBFactory.getDbConnection();
		PreparedStatement preparedStmt = conn.prepareStatement(query);
		preparedStmt = conn.prepareStatement(query);
		preparedStmt.setString(1, status);
		preparedStmt.setString(2, order_id);

		loggerV2.info("QUERY for update Order_purchase" + preparedStmt.toString());
		preparedStmt.execute();
		int count = preparedStmt.getUpdateCount();
		// check is added for order management,in case of process order logging
		// will not
		// work
		if (stringBuffer != null) {

			stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
			stringBuffer.append(System.getProperty("line.separator"));
			stringBuffer.append(Helper.getOMlogTimeStamp() + "*** Update Purchase Order Details  ");
			stringBuffer.append(System.getProperty("line.separator"));
			stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
			stringBuffer.append(System.getProperty("line.separator"));
			stringBuffer.append(Helper.getOMlogTimeStamp() + "Executing Query : " + preparedStmt.toString());
			stringBuffer.append(System.getProperty("line.separator"));

			if (count > 0) {
				stringBuffer.append(Helper.getOMlogTimeStamp() + "Record updated : ");
				stringBuffer.append(System.getProperty("line.separator"));
			} else {
				stringBuffer.append(Helper.getOMlogTimeStamp() + "Record updated : ");
				stringBuffer.append(System.getProperty("line.separator"));
			}
		}
		if (count > 0)
			return true;
		else
			return false;
	}

	/**
	 * Generates order history against pic
	 * 
	 * @param credential
	 * @param contentType
	 * @param requestBody
	 * @return Returns action history for bulk operations against Pic
	 */
	@POST
	@Path("/actionhistory")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Transactional(rollbackFor = Exception.class)
	public ActionHistoryResponse actionHistory(@Header("credentials") String credential,
			@Header("Content-Type") String contentType, @Body() String requestBody) {
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.ACTION_HISTORY_B2B);
		logs.setThirdPartyName(ThirdPartyNames.MANAGE_ORDER);
		logs.setTableType(LogsType.ActionHistory);
		ActionHistoryRequest cclient = new ActionHistoryRequest();
		ActionHistoryResponse resp = new ActionHistoryResponse();

		loggerV2.info("Request Landed on Action History: " + requestBody);
		try {
			java.sql.Connection conn = DBFactory.getDbConnection();
			cclient = Helper.JsonToObject(requestBody, ActionHistoryRequest.class);
			if (cclient != null) {
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				logs.setLang(cclient.getLang());
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
					if (credentials == null) {
						loggerV2.info(cclient.getmsisdn() + " - Credentials Not Verified");
						resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
						resp.setReturnMsg(ResponseCodes.ERROR_401);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					} else if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
						String verification = Helper.validateRequest(cclient);
						if (!verification.equals("")) {
							loggerV2.info(cclient.getmsisdn() + " - Verification of Request Failed");
							resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
							resp.setReturnMsg(verification);
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
						if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
							// getting the order id
							String query = "";
							if (cclient.getOrderKey() == null || cclient.getOrderKey().equals(""))
								query = "SELECT po.order_id, DATE_FORMAT(po.created_at,'%Y/%m/%d %H:%i:%s') as created_at, po.type, po.order_key, po. STATUS order_ststus,	 IFNULL(sum(od.completed), 0) completed, IFNULL(sum(od.failed), 0) failed, IFNULL(sum(od.new), 0) new, IFNULL(sum(od.user_canceled), 0) user_canceled, IFNULL(sum(od.pending), 0) pending, sum(od.total) FROM purchase_order po, ( SELECT order_id_fk, CASE WHEN STATUS LIKE '%C%' THEN counts END AS completed, CASE WHEN STATUS LIKE '%F%' THEN counts END AS failed, CASE WHEN STATUS LIKE '%N%' THEN counts END AS new, CASE WHEN STATUS LIKE '%U%' THEN counts END AS user_canceled, CASE WHEN STATUS LIKE '%P%' THEN counts END AS pending, sum(counts) total FROM ( SELECT order_id_fk, STATUS, count(*) counts FROM order_details GROUP BY order_id_fk, STATUS ) AS a GROUP BY order_id_fk, STATUS ) AS od WHERE po.order_id = od.order_id_fk AND po.username=? and date(created_at)>=? and date(created_at)<=? group by po.order_id, po.created_at, po.type, po.order_key, po. STATUS ORDER BY created_at DESC";
							else
								query = "SELECT po.order_id, DATE_FORMAT( po.created_at, '%Y/%m/%d %H:%i:%s') AS created_at, po.type, po.order_key, po.STATUS order_ststus, IFNULL( sum( od.completed ), 0 ) completed, IFNULL( sum( od.failed ), 0 ) failed, IFNULL( sum( od.new ), 0 ) new, IFNULL( sum( od.user_canceled ), 0 ) user_canceled, IFNULL( sum( od.pending ), 0 ) pending, sum( od.total ) FROM purchase_order po,( SELECT order_id_fk, CASE WHEN STATUS LIKE '%C%' THEN counts END AS completed, CASE WHEN STATUS LIKE '%F%' THEN counts END AS failed, CASE WHEN STATUS LIKE '%N%' THEN counts END AS new, CASE WHEN STATUS LIKE '%U%' THEN counts END AS user_canceled, CASE WHEN STATUS LIKE '%P%' THEN counts END AS pending, sum( counts ) total FROM ( SELECT order_id_fk, STATUS, count(*) counts FROM order_details GROUP BY order_id_fk, STATUS ) AS a GROUP BY order_id_fk, STATUS ) AS od WHERE po.order_id = od.order_id_fk AND po.username =? AND date( created_at )>=? AND date( created_at )<=? AND order_key =? GROUP BY po.order_id, po.created_at, po.type, po.order_key, po.STATUS ORDER BY created_at DESC";
							PreparedStatement preparedStmt = conn.prepareStatement(query);
							try {
								preparedStmt = conn.prepareStatement(query);
								preparedStmt.setString(1, cclient.getmsisdn());
								preparedStmt.setString(2, cclient.getStartDate());
								preparedStmt.setString(3, cclient.getEndDate());
								if (!(cclient.getOrderKey() == null))
									if (!cclient.getOrderKey().equals(""))
										preparedStmt.setString(4, cclient.getOrderKey());
								ResultSet rSet = preparedStmt.executeQuery();

								
								
								loggerV2.info("Query: " + preparedStmt);
								ArrayList<ActionHistoryResponseData> historylist = new ArrayList<ActionHistoryResponseData>();
								while (rSet.next()) {
									ActionHistoryResponseData resObj = new ActionHistoryResponseData();
									resObj.setOrderId(rSet.getString(1));
									resObj.setOrderStatus(rSet.getString(5));
									resObj.setOrderType(rSet.getString(3));
									
									Date date1 = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss").parse(rSet.getString(2));
									resObj.setDate(new SimpleDateFormat("dd/MM/yy HH:mm:ss").format(date1));
									resObj.setOrderKey(rSet.getString(4));
									resObj.setPending(rSet.getString(10));
									resObj.setFailed(rSet.getString(7));
									resObj.setTotalCount(rSet.getString(11));
									resObj.setCancelled(rSet.getString(9));
									resObj.setSuccess(rSet.getString(6));

									loggerV2.info("TYpeFromDB: " + rSet.getString(3));
									loggerV2.info("OrderStatus: " + rSet.getString(5));

									historylist.add(resObj);
								}
								rSet.close();
								preparedStmt.close();

								resp.setOrderList(historylist);
								resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
								resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
								logs.setResponseCode(resp.getReturnCode());
								logs.setResponseDescription(resp.getReturnMsg());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
								rSet.close();
								preparedStmt.close();
								return resp;

							} catch (Exception e) {
								loggerV2.error(Helper.GetException(e));
								resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
								resp.setReturnMsg(e.getMessage());
								logs.setResponseCode(resp.getReturnCode());
								logs.setResponseDescription(resp.getReturnMsg());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
								return resp;
							}

						}
					} else {
						loggerV2.error("Incorrect Msisdn");
						resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
						resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				} catch (Exception ex) {
					loggerV2.error(Helper.GetException(ex));
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}

		} catch (Exception e) {
			loggerV2.error(Helper.GetException(e));
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_401);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		return resp;

	}

	public static String orderStatus(String dbOrderStatus, String lang) {
		
		loggerV2.info("Landed in orderStatus method with status :::: "+dbOrderStatus+ " and lanuage is:::"+ lang  );
		String status = "";
		switch (dbOrderStatus) {
		case "C":
			status = ConfigurationManager
					.getConfigurationFromCache("actionhistory.order.completed."+lang);
			break;
		case "F":
			status = "Failed";
			break;
		case "N":
			status = "New";
			break;
		case "P":
			status = "Pending";
			break;

		default:
			status = "Unknown";
		}
		System.out.println("STATUS IN Function :" + status);
		return status;

	}

//	public static TariffDetailsMagentoResponse tariffDetailsV2BulkResponse(String request) {
//		  com.evampsaanga.magento.tariffdetailsv2.TariffDetailsMagentoResponse data = new com.evampsaanga.magento.tariffdetailsv2.TariffDetailsMagentoResponse();
//		  try {
//
//			  
//		   String response = RestClient.SendCallToMagento(
//		     ConfigurationManager.getConfigurationFromCache("magento.app.tariffV2"), request.toString());
//
//		   data = Helper.JsonToObject(response,
//		     com.evampsaanga.magento.tariffdetailsv2.TariffDetailsMagentoResponse.class);
//
//		   loggerV2.info("TariffResponse FROM magento >>:" + response);
//
//		  } catch (IOException e) {
//		   loggerV2.error(Helper.GetException(e));
//		   e.printStackTrace();
//		  } catch (Exception e) {
//		   loggerV2.error(Helper.GetException(e));
//		   e.printStackTrace();
//		  }
//
//		  return data;
//		 }
	public static QueryPaymentRelationResultMsg queryPaymentRelation(String msisdn, StringBuffer stringBuffer) {

		loggerV2.info("-----START queryPaymentRelation method----------" + msisdn);
		QueryPaymentRelationRequestMsg paymentRelationRequestMsg = new QueryPaymentRelationRequestMsg();

		paymentRelationRequestMsg.setRequestHeader(BcService.getRequestHeader());
		QueryPaymentRelationRequest paymentRelationRequest = new QueryPaymentRelationRequest();

		PaymentObj paymentObj = new PaymentObj();
		SubAccessCode subAccessCode = new SubAccessCode();
		subAccessCode.setPrimaryIdentity(msisdn);
		paymentObj.setSubAccessCode(subAccessCode);
		paymentRelationRequest.setPaymentObj(paymentObj);

		paymentRelationRequestMsg.setQueryPaymentRelationRequest(paymentRelationRequest);
		loggerV2.info("----queryPaymentRelatio BeforeCall -Response------" + subAccessCode.getPrimaryIdentity());
		QueryPaymentRelationResultMsg responseQPR = BcService.getInstance()
				.queryPaymentRelation(paymentRelationRequestMsg);
		loggerV2.info("----queryPaymentRelatio Method -Response------" + responseQPR.getResultHeader().getResultDesc());
		loggerV2.info("-----End queryPaymentRelation method------" + responseQPR.getResultHeader().getResultDesc());
		return responseQPR;
	}

	public static CreateOrderRspMsg changePaymentRealtion(String groupType, String msisdn,
			double mrcToBePassedChangePayment, String acccountKey, String paymentLimitKey)
			throws IOException, Exception {
		int myInt = (int) mrcToBePassedChangePayment;
		String valuelimitDivident = Integer.toString(myInt);
		loggerV2.info("Final Value toPAssed in changePAyment :" + mrcToBePassedChangePayment);

		Long limitValue = ((long) Double.parseDouble(valuelimitDivident) * Constants.MONEY_DIVIDEND);
		loggerV2.info("******************* Change Payment Relation **********************");
		CreateOrderReqMsg createOrderReqMsg = new CreateOrderReqMsg();
		createOrderReqMsg.setReqHeader(CreateOrderService.getCreateOderHeader());
		Order order = new Order();
		order.setOrderType("CO076");
		createOrderReqMsg.setOrder(order);
		PaymentPlanInfo paymentPlanInfo = new PaymentPlanInfo();
		paymentPlanInfo.setAccountID(Long.parseLong(paymentLimitKey));
		paymentPlanInfo.setServiceNumber(msisdn);

		PaymentRelation paymentRelation = new PaymentRelation();
		paymentRelation.setActionType("2");
		paymentRelation.setPaymentRelationId(acccountKey);

		paymentRelation.getServiceType().add("-1");
		PaymentLimit paymentLimit = new PaymentLimit();
		paymentLimit.setLimitMeasureUnit("101");
		if (groupType.equalsIgnoreCase("PartPay"))
			paymentLimit.setLimitPattern("1");
		else
			paymentLimit.setLimitPattern("3");
		paymentLimit.setLimitUnit("1");
		paymentLimit.setLimitValue(limitValue.toString());
		paymentRelation.setPaymentLimit(paymentLimit);

		paymentPlanInfo.getPaymentRelationList().add(paymentRelation);
		createOrderReqMsg.setPaymentPlanList(paymentPlanInfo);
		loggerV2.info("Change Payment Relation Request: " + Helper.ObjectToJson(createOrderReqMsg));
		CreateOrderRspMsg res = CreateOrderService.getInstance().createOrder(createOrderReqMsg);
		loggerV2.info("Change Payment Relation Response: " + Helper.ObjectToJson(res));
		loggerV2.info("**************** END of Change Payment Relation *****************");
		return res;
	}

	private OrderManagementResponse cancelPending(JSONObject jsonObject, String credential, Logs logs) {
		String credentials = null;
		OrderManagementResponse resp = new OrderManagementResponse();
		java.sql.Connection conn = DBFactory.getDbConnection();
		try {
			credentials = Decrypter.getInstance().decrypt(credential);
			if (credentials == null) {
				loggerV2.info(jsonObject.getString("msisdn").toString() + " - Credentials are Null");
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				resp.setResponseMsg("Credentials are not correct");
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			} else if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				try {
					loggerV2.info(jsonObject.getString("msisdn").toString() + " - Updating Order Details to Cancel");
					boolean falg = updateOrderDetailsToCancel(jsonObject.getString("orderId"), "U", conn,"3");
					if (falg) {
						loggerV2.info(jsonObject.getString("msisdn").toString() + " - Updating Order Status");
						falg = updateOrderStatus(jsonObject.getString("orderId"), "C", conn, null);
						// updateRetryCount(jsonObject.getString("orderId"),
						// conn);
						if (falg) {
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
							resp.setResponseMsg("Your Request is under Process Now");
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						} else {
							resp.setReturnCode(ResponseCodes.UNSUCCESS_CODE_CANCEL);
							resp.setReturnMsg(ResponseCodes.UNSUCCESS_CANCEL_DESC);
							resp.setResponseMsg("Requested Order Is Not In Pending State");
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} else {
						loggerV2.info(jsonObject.getString("msisdn").toString() + " - Order is Not in Pending Status");
						resp.setReturnCode(ResponseCodes.UNSUCCESS_CODE_CANCEL);
						resp.setReturnMsg(ResponseCodes.UNSUCCESS_CANCEL_DESC);
						resp.setResponseMsg("Requested Order Is Not In Pending State");
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				} catch (Exception e) {
					loggerV2.error(Helper.GetException(e));
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(e.getMessage());
					resp.setResponseMsg(e.getMessage());
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception e) {
			loggerV2.error(Helper.GetException(e));
			resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
			resp.setReturnMsg(e.getMessage());
			resp.setResponseMsg(e.getMessage());
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		return resp;
	}

	private OrderManagementResponse RetryFailed(JSONObject jsonObject, String credential, Logs logs) {
		String credentials = null;
		OrderManagementResponse resp = new OrderManagementResponse();
		java.sql.Connection conn = DBFactory.getDbConnection();
		try {
			credentials = Decrypter.getInstance().decrypt(credential);
			if (credentials == null) {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				resp.setResponseMsg("Credentials are not correct");
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			} else if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				// jsonObject.put("type", "Retry Failed Order");
				try {
					boolean flag = true;
					// checkFailedCount(jsonObject.getString("orderId"), conn);
					// loggerV2.info("Failed Order Found: " + flag);
					// if (flag) {
					// flag = updateOrderStatus(jsonObject.getString("orderId"),
					// "N", conn);
					flag = updateOrderStatusForRetry(jsonObject.getString("orderId"), "N", conn);
					loggerV2.info("Main Table Updated:" + flag);
					if (flag) {
						// updateRetryCount(jsonObject.getString("orderId"),
						// conn);
						// updateOrderDetails(jsonObject.getString("orderId"),
						// "", "P", "", "", conn);
						resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
						resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
						resp.setResponseMsg("Your Request is under Process Now");
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					} else {
						loggerV2.info("No Orders Found");
						resp.setReturnCode(ResponseCodes.UNSUCCESS_CODE_CANCEL);
						resp.setReturnMsg(ResponseCodes.UNSUCCESS_CANCEL_DESC);
						resp.setResponseMsg("Reuqest Order Doesn't Exist!");
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
					// } else {
					// resp.setReturnCode(ResponseCodes.NO_FAILED_CODE);
					// resp.setReturnMsg(ResponseCodes.NO_FAILED_DES);
					// resp.setResponseMsg("No Failed Orders Found!");
					// logs.setResponseCode(resp.getReturnCode());
					// logs.setResponseDescription(resp.getReturnMsg());
					// logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					// logs.updateLog(logs);
					// return resp;
					// }
				} catch (Exception e) {
					loggerV2.error(Helper.GetException(e));
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(e.getMessage());
					resp.setResponseMsg(e.getMessage());
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception e) {
			loggerV2.error(Helper.GetException(e));
			resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
			resp.setReturnMsg(e.getMessage());
			resp.setResponseMsg(e.getMessage());
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		return resp;
	}

	/**
	 * Checks if there are any failed orders against order id
	 * 
	 * @param order_id
	 * @param conn
	 * @return Returns true if any failed orders found else false
	 * @throws SQLException
	 */
	private boolean checkFailedCount(String order_id, Connection conn) throws SQLException {
		String query = "select count(*) from order_details where order_id_fk =? and status ='F'";
		PreparedStatement preparedStmt = conn.prepareStatement(query);
		preparedStmt = conn.prepareStatement(query);
		preparedStmt.setString(1, order_id);
		loggerV2.info("QUERY for update Order_purchase " + preparedStmt);
		ResultSet rSet = preparedStmt.executeQuery();
		int count = 0;
		while (rSet.next()) {
			count = rSet.getInt(1);
		}
		if (count > 0) {
			rSet.close();
			preparedStmt.close();
			return true;
		} else {
			rSet.close();
			preparedStmt.close();
			return false;
		}
	}

	public static void main(String[] args) {
		String request = "{\r\n" + "    \"lang\": \"3\",\r\n" + "    \"iP\": \"10.220.245.129\",\r\n"
				+ "    \"channel\": \"android\",\r\n" + "    \"msisdn\": \"pic_new1\",\r\n"
				+ "    \"orderKey\": \"P002\",\r\n" + "    \"recieverMsisdn\": [\r\n" + "        {\r\n"
				+ "            \"msisdn\": \"776480544\"\r\n" + "        }\r\n" + "    ],\r\n"
				+ "    \"actionType\": \"1\",\r\n" + "    \"offeringId\": \"1265026341\"\r\n" + "}";
		try {
			new OrderManagementLand().insertorders("RQ/Rjcf8Gn2+7srAiXNO0ZGuRnJORSir", "application/json", request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		
	}
}
