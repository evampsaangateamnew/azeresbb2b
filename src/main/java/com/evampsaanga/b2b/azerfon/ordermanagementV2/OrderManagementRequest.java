package com.evampsaanga.b2b.azerfon.ordermanagementV2;

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;

public class OrderManagementRequest extends BaseRequest {

	private String ids;
	private String actionType;
	private String userName;
	private String type;
	private String orderKey;

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getIds() {
		return ids;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}

	public String getOrderKey() {
		return orderKey;
	}

	public void setOrderKey(String orderKey) {
		this.orderKey = orderKey;
	}

}
