/**
 * 
 */
package com.evampsaanga.b2b.azerfon.ordermanagementV2.orderprocessing;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.evampsaanga.azerfon.appserver.refreshappservercache.BuildCacheRequestLand;
import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.azerfon.db.DBFactory;
import com.evampsaanga.b2b.azerfon.getUsersV2.UsersGroupData;
import com.evampsaanga.b2b.azerfon.getsubscriber.CRMSubscriberService;
import com.evampsaanga.b2b.azerfon.grouppermission.Datum;
import com.evampsaanga.b2b.azerfon.grouppermission.GroupPermissionMagentoResponse;
import com.evampsaanga.b2b.azerfon.limitpaymentrelation.LimitPaymentRelationRequestLand;
import com.evampsaanga.b2b.azerfon.thirdpartycall.AzerfonThirdPartyCalls;
import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.ThirdPartyNames;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.evampsaanga.b2b.magento.tariffdetailsv2.TariffDetailsMagentoResponse;
import com.huawei.bme.cbsinterface.arservices.QueryBalanceRequest;
import com.huawei.bme.cbsinterface.arservices.QueryBalanceRequest.QueryObj;
import com.huawei.bme.cbsinterface.arservices.QueryBalanceRequest.QueryObj.AcctAccessCode;
import com.huawei.bme.cbsinterface.arservices.QueryBalanceRequestMsg;
import com.huawei.bme.cbsinterface.arservices.QueryBalanceResultMsg;
import com.huawei.crm.azerfon.bss.query.GetSubscriberResponse;
import com.huawei.crm.query.GetGroupDataInfo;
import com.huawei.crm.query.GetGroupOut;
import com.huawei.crm.query.GetGroupResponse;
import com.huawei.crm.service.ens.SubmitOrderResponse;
import com.ngbss.evampsaanga.services.ThirdPartyCall;
import com.ngbss.evampsaanga.services.ThirdPartyRequestHeader;
import com.saanga.magento.apiclient.RestClient;

/**
 * @author HamzaFarooque
 *
 */
public class ChangeTariffOrder implements Order {

	public static final Logger loggerOrderV2 = Logger.getLogger("azerfonlogs-order");

	@Override
	public void processOrder(ArrayList<String> order_key, ArrayList<String> order_id) throws SQLException {

		loggerOrderV2.info("In Change Tariff Order ----------  Processing order");

		Logs logs = new Logs();
		int index = 0;
		String TrnsactionName = Transactions.CHANGE_TARRIF_TRANSACTION_NAME;
		String token;
		java.sql.Connection conn = DBFactory.getDbConnection();
		logs.setTransactionName(TrnsactionName);
		logs.setThirdPartyName(ThirdPartyNames.CHANGE_LIMIT);
		GetGroupResponse getGroupResponse = new GetGroupResponse();
		List<GetGroupDataInfo> grpDataList = new ArrayList<>();
		String groupType;
		String accountId = "";

		long currentLimit = 0;
		StringBuffer stringBuffer = new StringBuffer();
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy_HHmmss_SSSS");
		String dateTime = format.format(new Date());
		String fileName = "OM_" + dateTime + "_" + order_id.get(index) + ".log";
		// should be configurable in Constants
		String path = Constants.ORDER_MANAGEMENT_LOGS_FILEPATH;

		stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
		stringBuffer.append(System.getProperty("line.separator"));
		stringBuffer.append(Helper.getOMlogTimeStamp() + "***Change Tariff ");
		stringBuffer.append(System.getProperty("line.separator"));
		stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
		stringBuffer.append(System.getProperty("line.separator"));

		try {
			// update the main table status
			OrderProcessing.updateOrderStatus(order_id.get(index), "P", conn, stringBuffer);
			// get the order attributes so that further processing can
			// be done

			JSONObject jsonObject = getOrderAttributes(order_id.get(index), conn, stringBuffer);
			loggerOrderV2.info("Order Attributes: " + jsonObject);
			logFile(stringBuffer, " OrderAttributes " + jsonObject);

			// request packet to process

			JSONArray jArray = jsonObject.getJSONArray("users");
			jsonObject.remove("users");
			jsonObject.remove("type");
			jsonObject.remove("orderKey");

			for (int i = 0; i < jArray.length(); i++) {

				jsonObject.put("msisdn", jArray.getJSONObject(i).get("msisdn").toString());
				groupType = jArray.getJSONObject(i).getString("groupType");
				loggerOrderV2.info("msisdn is: " + jsonObject.getString("msisdn"));
				loggerOrderV2.info("Group Type:" + groupType);
				
				logs.setIp(jsonObject.getString("iP"));
				logs.setChannel(jsonObject.getString("channel"));
				logs.setMsisdn(jsonObject.getString("msisdn"));
				logs.setLang(jsonObject.getString("lang"));

				loggerOrderV2.info("Order Attributes: " + jsonObject);

				stringBuffer.append(Helper.getOMlogTimeStamp() + " MSISDN : " + jsonObject.getString("msisdn"));

				token = Helper.retrieveToken(TrnsactionName, jsonObject.getString("msisdn"));

				// group data call

				logFile(stringBuffer, "Call to QueryGroupData through azerfonThirdPArtyCall");
				getGroupResponse = AzerfonThirdPartyCalls.orderQueryGroupData(jsonObject.getString("msisdn"), token);
				logFile(stringBuffer, "QueryGroupData Response : " + Helper.ObjectToJson(getGroupResponse));

				// group data processing
				if (getGroupResponse.getGetGroupBody() == null) {
					GetGroupOut value = new GetGroupOut();
					// GetGroupDataInfo e =new GetGroupDataInfo();
					// value.getGetGroupDataList().add(e);
					getGroupResponse.setGetGroupBody(value);
				}

				if (getGroupResponse.getGetGroupBody() != null) {
					String groupids;
					StringBuilder st = new StringBuilder();
					grpDataList = getGroupResponse.getGetGroupBody().getGetGroupDataList();
					loggerOrderV2.info("----------LIST RETURNED FROM NGBSS----------");
					for (GetGroupDataInfo gdi : grpDataList) {
						st.append(gdi.getGroupNo());
						st.append(",");
					}

					if (st.length() > 0)
						st.replace(st.length() - 1, st.length(), "");
					groupids = st.toString();
					loggerOrderV2.info("---------------- Group Ids: " + groupids);
					logFile(stringBuffer, "---------------- Group Ids:" + groupids);

					// group data processing end

					// subscriber call
					logFile(stringBuffer, "Call to GetSubscriber");
					GetSubscriberResponse getSubscriberResponse = new CRMSubscriberService()
							.GetSubscriberRequest(jsonObject.getString("msisdn"));

					// Get Permissions call
					JSONObject jsonMagentoPermissionObject = new JSONObject();
					jsonMagentoPermissionObject.put("groupId", groupids);

					String mresponse = "";
					GroupPermissionMagentoResponse magentoresponse = new GroupPermissionMagentoResponse();
					if (groupids.equalsIgnoreCase("")) {

						loggerOrderV2.info(token + " - Customize Response of Magento Due to No group found from ngbss");
						magentoresponse.setResultCode(401);
						List<Datum> data = new ArrayList<Datum>();
						magentoresponse.setData(data);
					}

					else {
						loggerOrderV2.info(token + " - Request Pakect for Magento Restriction Api "
								+ jsonMagentoPermissionObject.toString());

						mresponse = RestClient.SendCallToMagento(token,
								ConfigurationManager.getConfigurationFromCache("magento.app.grouppermission"),
								jsonMagentoPermissionObject.toString());

						loggerOrderV2.info(
								jsonObject.getString("msisdn") + " - Response from restrictions api: " + mresponse);
						magentoresponse = Helper.JsonToObject(mresponse, GroupPermissionMagentoResponse.class);
					}

					loggerOrderV2.info(token + " - Response of Magento Api " + magentoresponse.toString());

					loggerOrderV2.info(token + " - Result code From Magento " + magentoresponse.getResultCode());

					List<String> restrictions = new ArrayList<String>();

					if (magentoresponse.getResultCode() == 401) {
						// Get Permissions data processing
						if (!magentoresponse.getData().isEmpty()) {
							for (Datum datum : magentoresponse.getData()) {
								String restrictionTemp = datum.getTariffRestrictions();
								loggerOrderV2.info(token + "----- Restriction elements ----:" + restrictionTemp);
								if (datum.getTariffRestrictions() != null && !datum.getTariffRestrictions().isEmpty()) {
									restrictions.addAll(Arrays.asList(restrictionTemp.split(",")));
								}
							}
						}

						logFile(stringBuffer, "----- Restrictions List----:" + Helper.listToString(restrictions));
						loggerOrderV2.info(token + "----- Restrictions List----:" + Helper.listToString(restrictions));

						// get subscriber data processing
						if (getSubscriberResponse.getGetSubscriberBody() != null) {
							loggerOrderV2.info("---------------Subscriber Info-------------");
							loggerOrderV2.info(Helper.ObjectToJson(getSubscriberResponse));

							logFile(stringBuffer, Helper.ObjectToJson(getSubscriberResponse));

							final String primaryOfferingId = getSubscriberResponse.getGetSubscriberBody()
									.getPrimaryOffering().getOfferingId().getOfferingId();
							final String destinationOfferingId = jsonObject.getString("destinationTariff");
							logs.setTariffId(destinationOfferingId);

							JSONObject jsonMagentoObject = new JSONObject();
							JSONArray jOfferingArray = new JSONArray();
							jOfferingArray.put(primaryOfferingId);
							jOfferingArray.put(destinationOfferingId);
							jsonMagentoObject.put("lang", "3");
							jsonMagentoObject.put("offeringId", jOfferingArray);
							jsonMagentoObject.put("msisdn", jsonObject.getString("msisdn"));

							loggerOrderV2.info(jsonObject.getString("msisdn") + "-Request body for TariffDetails: "
									+ jsonMagentoObject.toString());
							logFile(stringBuffer, "-Request body from TariffDetails: " + jsonMagentoObject.toString());
							// tariff details call for MRC
							TariffDetailsMagentoResponse dataTariffDetailsbulk = AzerfonThirdPartyCalls
									.tariffDetailsV2BulkResponseChangeLimit(jsonMagentoObject.toString());

							loggerOrderV2.info(jsonObject.getString("msisdn")
									+ " - Response From Magento for tariff details " + dataTariffDetailsbulk);
							logFile(stringBuffer, " - Response From Magento " + dataTariffDetailsbulk);

							// Get MRC

							if (dataTariffDetailsbulk.getResultCode()
									.equals(ResponseCodes.TARIFF_DETAILS_VERSION_2_SUCCESSFUL)) {
								if (!dataTariffDetailsbulk.getData().isEmpty()) {
									List<com.evampsaanga.b2b.magento.tariffdetailsv2.Datum> tariffDetailsList = dataTariffDetailsbulk
											.getData();
									com.evampsaanga.b2b.magento.tariffdetailsv2.Datum primaryOffringdetails = tariffDetailsList
											.stream()
											.filter(item -> primaryOfferingId.equals(item.getHeader().getOfferingId()))
											.findAny().orElse(null);
									com.evampsaanga.b2b.magento.tariffdetailsv2.Datum destinationOffringdetails = tariffDetailsList
											.stream().filter(item -> destinationOfferingId
													.equals(item.getHeader().getOfferingId()))
											.findAny().orElse(null);

									if (primaryOffringdetails == null || destinationOffringdetails == null) {
										loggerOrderV2.info(
												"--------MRC not found agaisnt Destiantion or Primary offring ID--------");
										OrderProcessing.updateOrderDetails(order_id.get(index),
												jsonObject.getString(" msisdn"), "F",
												dataTariffDetailsbulk.getResultCode(), "actionhistory.generic.failure.",
												conn, stringBuffer);
									} else {
										int primaryMrc = Integer
												.parseInt(primaryOffringdetails.getHeader().getMrcValue());
										int destMrc = Integer
												.parseInt(destinationOffringdetails.getHeader().getMrcValue());
										loggerOrderV2.info("Primary MRC :" + primaryMrc);
										loggerOrderV2.info("Destination MRC :" + destMrc);
										logFile(stringBuffer, "-------Primary MRC: " + primaryMrc);
										logFile(stringBuffer, "-------Destination MRC: " + destMrc);

										if (primaryMrc >= destMrc) {
											String user_action;

											if (primaryMrc == destMrc) {
												user_action = "Migrate";
											} else {
												user_action = "downgrade";
											}

											loggerOrderV2.info("-------User Action Case------- : " + user_action);
											logFile(stringBuffer, "-------User Action Case------- : " + user_action);

											if (restrictions.stream().noneMatch(s -> s.equalsIgnoreCase(user_action))) {

												// Third Party Call for change
												// Tariff

												SubmitOrderResponse submitOrderResponse = AzerfonThirdPartyCalls
														.changetariff(jsonObject.getString("msisdn"),
																destinationOfferingId, token);

												if (submitOrderResponse != null && submitOrderResponse
														.getResponseHeader().getRetCode().equalsIgnoreCase("0")) {
													loggerOrderV2
															.info(token + "--------Ngbss Change Tariff Reponse is : "
																	+ Helper.ObjectToJson(submitOrderResponse));
													logFile(stringBuffer, "--------Ngbss Change Tariff Reponse is : "
															+ Helper.ObjectToJson(submitOrderResponse));
													OrderProcessing.updateOrderDetails(order_id.get(index),
															jsonObject.getString("msisdn"), "C",
															submitOrderResponse.getResponseHeader().getRetCode(),
															Helper.getNgbssMessageFromResponseCode(
																	submitOrderResponse.getResponseHeader()
																			.getRetCode(),
																	submitOrderResponse.getResponseHeader().getRetMsg(),
																	jsonObject.getString("lang")),
															conn, stringBuffer);
													logs.setResponseCode(
															submitOrderResponse.getResponseHeader().getRetCode());
													logs.setResponseDescription(
															submitOrderResponse.getResponseHeader().getRetMsg());
													logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
													logs.updateLog(logs);
												}

												else {
													loggerOrderV2
															.info(token + "--------Ngbss Change Tariff Reponse is : "
																	+ Helper.ObjectToJson(submitOrderResponse));
													logFile(stringBuffer, "--------Ngbss Change Tariff Reponse is : "
															+ Helper.ObjectToJson(submitOrderResponse));
													OrderProcessing.updateOrderDetails(order_id.get(index),
															jsonObject.getString("msisdn"), "F",
															submitOrderResponse.getResponseHeader().getRetCode(),
															Helper.getNgbssMessageFromResponseCode(
																	submitOrderResponse.getResponseHeader()
																			.getRetCode(),
																	submitOrderResponse.getResponseHeader().getRetMsg(),
																	jsonObject.getString("lang")),
															conn, stringBuffer);
													logs.setResponseCode(
															submitOrderResponse.getResponseHeader().getRetCode());
													logs.setResponseDescription(
															submitOrderResponse.getResponseHeader().getRetMsg());
													logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
													logs.updateLog(logs);
												}
											} else {

												loggerOrderV2.info(
														token + "--------User do not have the permission--------");
												logFile(stringBuffer,
														"--------User do not have the permission--------");
												OrderProcessing.updateOrderDetails(order_id.get(index),
														jsonObject.getString("msisdn"), "F",
														ResponseCodes.ERROR_401_CODE, "actionhistory.restricted.", conn,
														stringBuffer);
												logs.setResponseCode(ResponseCodes.ERROR_401_CODE);
												logs.setResponseDescription("User Does not have the permission");
												logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
												logs.updateLog(logs);
											}

										} else if (destMrc > primaryMrc) {

											String user_action = "upgrade";
											loggerOrderV2.info("-------User Action Case------- : " + user_action);
											logFile(stringBuffer, "-------User Action Case------- : " + user_action);

											if (restrictions.stream().noneMatch(s -> s.equalsIgnoreCase(user_action))) {

												// Calling Balance Query for
												// Current Limit of User

												loggerOrderV2.info(token
														+ "----- Calling Balance Query for Current Limit of User----:");
												QueryBalanceRequestMsg queryBalanceRequestMsg = new QueryBalanceRequestMsg();
												QueryBalanceRequest BalanceRequest = new QueryBalanceRequest();
												QueryObj QueryObj = new QueryObj();
												AcctAccessCode accessCode = new AcctAccessCode();
												loggerOrderV2
														.info("#################################################3");
												loggerOrderV2.info("Testing:" + jsonObject.getString("msisdn"));
												loggerOrderV2
														.info("#################################################3");
												accessCode.setPrimaryIdentity(jsonObject.getString("msisdn"));
												QueryObj.setAcctAccessCode(accessCode);
												BalanceRequest.setQueryObj(QueryObj);
												queryBalanceRequestMsg.setQueryBalanceRequest(BalanceRequest);
												queryBalanceRequestMsg.setRequestHeader(
														ThirdPartyRequestHeader.getRequestPaymentRequestHeader());

												QueryBalanceResultMsg resultMsg = ThirdPartyCall
														.getQueryBalance(queryBalanceRequestMsg);
												SubmitOrderResponse submitOrderResponseChangeLimit = null;

												if (groupType.equalsIgnoreCase("PayBySubs")) {
													loggerOrderV2.info(token + "----- User is PayBySubs----:");
													if (resultMsg.getQueryBalanceResult().getAcctList() != null) {
														if (resultMsg.getQueryBalanceResult().getAcctList().get(0)
																.getAccountCredit() != null) {
															currentLimit = resultMsg.getQueryBalanceResult()
																	.getAcctList().get(0).getAccountCredit().get(0)
																	.getTotalCreditAmount();
															currentLimit = currentLimit / Constants.MONEY_DIVIDEND;
															loggerOrderV2.info(
																	token + "----- The CUrrent limit of user is ----:"
																			+ currentLimit);
														}

													}

													if (currentLimit < destMrc + 30) {

														List<UsersGroupData> list = BuildCacheRequestLand.usersCache
																.get(jsonObject.getString("virtualCorpCode") + "."
																		+ jsonObject.getString("lang"));
														for (UsersGroupData ugd : list) {
															if (ugd.getMsisdn()
																	.equals(jsonObject.getString("msisdn"))) {
																accountId = ugd.getCrmAccountId();
																break;
															}
														}

														String amount = LimitPaymentRelationRequestLand
																.getMaxFromDb(destMrc + "");

														String paramValue = ConfigurationManager
																.getConfigurationFromCache(
																		"changelimit.credit.limit." + amount);
														amount = amount + "00000";
														loggerOrderV2.info(token
																+ "--------PaybySub Case Submit Order Request Packet --------");
														loggerOrderV2
																.info(token + "--------PaybySub Case MSISDN =  --------"
																		+ jsonObject.getString("msisdn"));
														loggerOrderV2.info(token
																+ "--------PaybySub Case AMOUNT =  --------" + amount);
														loggerOrderV2.info(
																token + "--------PaybySub Case ACCOUNT_ID =  --------"
																		+ accountId);
														loggerOrderV2.info(
																token + "--------PaybySub Case PARAM VALUE =  --------"
																		+ paramValue);

														submitOrderResponseChangeLimit = AzerfonThirdPartyCalls
																.changeLimit(jsonObject.getString("msisdn"), accountId,
																		amount, paramValue);

														if (submitOrderResponseChangeLimit != null
																&& submitOrderResponseChangeLimit.getResponseHeader()
																		.getRetCode().equalsIgnoreCase("0"))

														{
															Thread.sleep(10000);

															// Third Party Call
															// for Change
															// Tariff
															SubmitOrderResponse submitOrderResponse = AzerfonThirdPartyCalls
																	.changetariff(jsonObject.getString("msisdn"),
																			destinationOfferingId, token);

															if (submitOrderResponse != null
																	&& submitOrderResponse.getResponseHeader()
																			.getRetCode().equalsIgnoreCase("0")) {
																loggerOrderV2.info(token
																		+ "--------Ngbss Change Tariff Reponse is : "
																		+ Helper.ObjectToJson(submitOrderResponse));
																logFile(stringBuffer,
																		"--------Ngbss Change Tariff Reponse is : "
																				+ Helper.ObjectToJson(
																						submitOrderResponse));
																OrderProcessing.updateOrderDetails(order_id.get(index),
																		jsonObject.getString("msisdn"), "C",
																		submitOrderResponse.getResponseHeader()
																				.getRetCode(),
																		Helper.getNgbssMessageFromResponseCode(
																				submitOrderResponse.getResponseHeader()
																						.getRetCode(),
																				submitOrderResponse.getResponseHeader()
																						.getRetMsg(),
																				jsonObject.getString("lang")),
																		conn, stringBuffer);
																logs.setResponseCode(submitOrderResponse
																		.getResponseHeader().getRetCode());
																logs.setResponseDescription(submitOrderResponse
																		.getResponseHeader().getRetMsg());
																logs.setResponseDateTime(
																		Helper.GenerateDateTimeToMsAccuracy());
																logs.updateLog(logs);
															}

															else {
																loggerOrderV2.info(token
																		+ "--------Ngbss Change Tariff Reponse is : "
																		+ Helper.ObjectToJson(submitOrderResponse));
																logFile(stringBuffer,
																		"--------Ngbss Change Tariff Reponse is : "
																				+ Helper.ObjectToJson(
																						submitOrderResponse));
																OrderProcessing.updateOrderDetails(order_id.get(index),
																		jsonObject.getString("msisdn"), "F",
																		submitOrderResponse.getResponseHeader()
																				.getRetCode(),
																		Helper.getNgbssMessageFromResponseCode(
																				submitOrderResponse.getResponseHeader()
																						.getRetCode(),
																				submitOrderResponse.getResponseHeader()
																						.getRetMsg(),
																				jsonObject.getString("lang")),
																		conn, stringBuffer);
																logs.setResponseCode(submitOrderResponse
																		.getResponseHeader().getRetCode());
																logs.setResponseDescription(submitOrderResponse
																		.getResponseHeader().getRetMsg());
																logs.setResponseDateTime(
																		Helper.GenerateDateTimeToMsAccuracy());
																logs.updateLog(logs);
															}

														}

														else {
															loggerOrderV2.info(
																	token + "--------Ngbss Change limit Reponse is : "
																			+ Helper.ObjectToJson(
																					submitOrderResponseChangeLimit));
															logFile(stringBuffer,
																	"--------Ngbss Change limit Reponse is : "
																			+ Helper.ObjectToJson(
																					submitOrderResponseChangeLimit));
															OrderProcessing.updateOrderDetails(order_id.get(index),
																	jsonObject.getString("msisdn"), "F",
																	submitOrderResponseChangeLimit.getResponseHeader()
																			.getRetCode(),
																	Helper.getNgbssMessageFromResponseCode(
																			submitOrderResponseChangeLimit
																					.getResponseHeader().getRetCode(),
																			submitOrderResponseChangeLimit
																					.getResponseHeader().getRetMsg(),
																			jsonObject.getString("lang")),
																	conn, stringBuffer);
															logs.setResponseCode(submitOrderResponseChangeLimit
																	.getResponseHeader().getRetCode());
															logs.setResponseDescription(submitOrderResponseChangeLimit
																	.getResponseHeader().getRetMsg());
															logs.setResponseDateTime(
																	Helper.GenerateDateTimeToMsAccuracy());
															logs.updateLog(logs);
														}
													}

													else {
														SubmitOrderResponse submitOrderResponse = AzerfonThirdPartyCalls
																.changetariff(jsonObject.getString("msisdn"),
																		destinationOfferingId, token);

														if (submitOrderResponse != null
																&& submitOrderResponse.getResponseHeader().getRetCode()
																		.equalsIgnoreCase("0")) {
															loggerOrderV2.info(
																	token + "--------Ngbss Change Tariff Reponse is : "
																			+ Helper.ObjectToJson(submitOrderResponse));
															logFile(stringBuffer,
																	"--------Ngbss Change Tariff Reponse is : "
																			+ Helper.ObjectToJson(submitOrderResponse));
															OrderProcessing.updateOrderDetails(order_id.get(index),
																	jsonObject.getString("msisdn"), "C",
																	submitOrderResponse.getResponseHeader()
																			.getRetCode(),
																	Helper.getNgbssMessageFromResponseCode(
																			submitOrderResponse.getResponseHeader()
																					.getRetCode(),
																			submitOrderResponse.getResponseHeader()
																					.getRetMsg(),
																			jsonObject.getString("lang")),
																	conn, stringBuffer);
															logs.setResponseCode(submitOrderResponse.getResponseHeader()
																	.getRetCode());
															logs.setResponseDescription(submitOrderResponse
																	.getResponseHeader().getRetMsg());
															logs.setResponseDateTime(
																	Helper.GenerateDateTimeToMsAccuracy());
															logs.updateLog(logs);
														}

														else {
															loggerOrderV2.info(
																	token + "--------Ngbss Change Tariff Reponse is : "
																			+ Helper.ObjectToJson(submitOrderResponse));
															logFile(stringBuffer,
																	"--------Ngbss Change Tariff Reponse is : "
																			+ Helper.ObjectToJson(submitOrderResponse));
															OrderProcessing.updateOrderDetails(order_id.get(index),
																	jsonObject.getString("msisdn"), "F",
																	submitOrderResponse.getResponseHeader()
																			.getRetCode(),
																	Helper.getNgbssMessageFromResponseCode(
																			submitOrderResponse.getResponseHeader()
																					.getRetCode(),
																			submitOrderResponse.getResponseHeader()
																					.getRetMsg(),
																			jsonObject.getString("lang")),
																	conn, stringBuffer);
															logs.setResponseCode(submitOrderResponse.getResponseHeader()
																	.getRetCode());
															logs.setResponseDescription(submitOrderResponse
																	.getResponseHeader().getRetMsg());
															logs.setResponseDateTime(
																	Helper.GenerateDateTimeToMsAccuracy());
															logs.updateLog(logs);
														}

													}

												}

												else if (groupType.equalsIgnoreCase("PartPay"))

												{

													if (resultMsg.getQueryBalanceResult().getAcctList() != null) {
														if (resultMsg.getQueryBalanceResult().getAcctList().get(0)
																.getPaymentLimitUsage() != null) {
															currentLimit = resultMsg.getQueryBalanceResult()
																	.getAcctList().get(0).getPaymentLimitUsage().get(0)
																	.getAmount();
															currentLimit = currentLimit / Constants.MONEY_DIVIDEND;
														}

														loggerOrderV2
																.info(token + "----- The CUrrent limit of user is ----:"
																		+ currentLimit);

													}

													if (currentLimit < destMrc + 30) {
														List<UsersGroupData> list = BuildCacheRequestLand.usersCache
																.get(jsonObject.getString("virtualCorpCode") + "."
																		+ jsonObject.getString("lang"));
														for (UsersGroupData ugd : list) {
															if (ugd.getMsisdn()
																	.equals(jsonObject.getString("msisdn"))) {
																accountId = ugd.getCrmAccountId();
																break;
															}
														}

														String amount = LimitPaymentRelationRequestLand
																.getMaxFromDb(destMrc + "");
														String paramValue = ConfigurationManager
																.getConfigurationFromCache(
																		"changelimit.credit.limit." + amount);

														loggerOrderV2.info(token
																+ "--------PartPay Case Submit Order Request Packet --------");
														loggerOrderV2
																.info(token + "--------PartPay Case MSISDN =  --------"
																		+ jsonObject.getString("msisdn"));
														loggerOrderV2.info(token
																+ "--------PartPay Case AMOUNT =  --------" + amount);
														loggerOrderV2.info(
																token + "--------PartPay Case ACCOUNT_ID =  --------"
																		+ accountId);
														loggerOrderV2.info(
																token + "--------PartPay Case PARAM VALUE =  --------"
																		+ paramValue);

														submitOrderResponseChangeLimit = AzerfonThirdPartyCalls
																.changeLimit(jsonObject.getString("msisdn"), accountId,
																		"", paramValue);

														if (submitOrderResponseChangeLimit != null
																&& submitOrderResponseChangeLimit.getResponseHeader()
																		.getRetCode().equalsIgnoreCase("0"))

														{
															Thread.sleep(10000);

															// Third Party Call
															// for Change
															// Tariff
															SubmitOrderResponse submitOrderResponse = AzerfonThirdPartyCalls
																	.changetariff(jsonObject.getString("msisdn"),
																			destinationOfferingId, token);

															if (submitOrderResponse != null
																	&& submitOrderResponse.getResponseHeader()
																			.getRetCode().equalsIgnoreCase("0")) {
																loggerOrderV2.info(token
																		+ "--------Ngbss Change Tariff Reponse is : "
																		+ Helper.ObjectToJson(submitOrderResponse));
																logFile(stringBuffer,
																		"--------Ngbss Change Tariff Reponse is : "
																				+ Helper.ObjectToJson(
																						submitOrderResponse));
																OrderProcessing.updateOrderDetails(order_id.get(index),
																		jsonObject.getString("msisdn"), "C",
																		submitOrderResponse.getResponseHeader()
																				.getRetCode(),
																		Helper.getNgbssMessageFromResponseCode(
																				submitOrderResponse.getResponseHeader()
																						.getRetCode(),
																				submitOrderResponse.getResponseHeader()
																						.getRetMsg(),
																				jsonObject.getString("lang")),
																		conn, stringBuffer);
																logs.setResponseCode(submitOrderResponse
																		.getResponseHeader().getRetCode());
																logs.setResponseDescription(submitOrderResponse
																		.getResponseHeader().getRetMsg());
																logs.setResponseDateTime(
																		Helper.GenerateDateTimeToMsAccuracy());
																logs.updateLog(logs);
															}

															else {
																loggerOrderV2.info(token
																		+ "--------Ngbss Change Tariff Reponse is : "
																		+ Helper.ObjectToJson(submitOrderResponse));
																logFile(stringBuffer,
																		"--------Ngbss Change Tariff Reponse is : "
																				+ Helper.ObjectToJson(
																						submitOrderResponse));
																OrderProcessing.updateOrderDetails(order_id.get(index),
																		jsonObject.getString("msisdn"), "F",
																		submitOrderResponse.getResponseHeader()
																				.getRetCode(),
																		Helper.getNgbssMessageFromResponseCode(
																				submitOrderResponse.getResponseHeader()
																						.getRetCode(),
																				submitOrderResponse.getResponseHeader()
																						.getRetMsg(),
																				jsonObject.getString("lang")),
																		conn, stringBuffer);
																logs.setResponseCode(submitOrderResponse
																		.getResponseHeader().getRetCode());
																logs.setResponseDescription(submitOrderResponse
																		.getResponseHeader().getRetMsg());
																logs.setResponseDateTime(
																		Helper.GenerateDateTimeToMsAccuracy());
																logs.updateLog(logs);
															}
														}

														else {
															loggerOrderV2.info(
																	token + "--------Ngbss Change limit Reponse is : "
																			+ Helper.ObjectToJson(
																					submitOrderResponseChangeLimit));
															logFile(stringBuffer,
																	"--------Ngbss Change limit Reponse is : "
																			+ Helper.ObjectToJson(
																					submitOrderResponseChangeLimit));
															OrderProcessing.updateOrderDetails(order_id.get(index),
																	jsonObject.getString("msisdn"), "F",
																	submitOrderResponseChangeLimit.getResponseHeader()
																			.getRetCode(),
																	Helper.getNgbssMessageFromResponseCode(
																			submitOrderResponseChangeLimit
																					.getResponseHeader().getRetCode(),
																			submitOrderResponseChangeLimit
																					.getResponseHeader().getRetMsg(),
																			jsonObject.getString("lang")),
																	conn, stringBuffer);
															logs.setResponseCode(submitOrderResponseChangeLimit
																	.getResponseHeader().getRetCode());
															logs.setResponseDescription(submitOrderResponseChangeLimit
																	.getResponseHeader().getRetMsg());
															logs.setResponseDateTime(
																	Helper.GenerateDateTimeToMsAccuracy());
															logs.updateLog(logs);
														}

													}

													else {
														SubmitOrderResponse submitOrderResponse = AzerfonThirdPartyCalls
																.changetariff(jsonObject.getString("msisdn"),
																		destinationOfferingId, token);

														if (submitOrderResponse != null
																&& submitOrderResponse.getResponseHeader().getRetCode()
																		.equalsIgnoreCase("0")) {
															loggerOrderV2.info(
																	token + "--------Ngbss Change Tariff Reponse is : "
																			+ Helper.ObjectToJson(submitOrderResponse));
															logFile(stringBuffer,
																	"--------Ngbss Change Tariff Reponse is : "
																			+ Helper.ObjectToJson(submitOrderResponse));
															OrderProcessing.updateOrderDetails(order_id.get(index),
																	jsonObject.getString("msisdn"), "C",
																	submitOrderResponse.getResponseHeader()
																			.getRetCode(),
																	Helper.getNgbssMessageFromResponseCode(
																			submitOrderResponse.getResponseHeader()
																					.getRetCode(),
																			submitOrderResponse.getResponseHeader()
																					.getRetMsg(),
																			jsonObject.getString("lang")),
																	conn, stringBuffer);
															logs.setResponseCode(submitOrderResponse.getResponseHeader()
																	.getRetCode());
															logs.setResponseDescription(submitOrderResponse
																	.getResponseHeader().getRetMsg());
															logs.setResponseDateTime(
																	Helper.GenerateDateTimeToMsAccuracy());
															logs.updateLog(logs);
														}

														else {
															loggerOrderV2.info(
																	token + "--------Ngbss Change Tariff Reponse is : "
																			+ Helper.ObjectToJson(submitOrderResponse));
															logFile(stringBuffer,
																	"--------Ngbss Change Tariff Reponse is : "
																			+ Helper.ObjectToJson(submitOrderResponse));
															OrderProcessing.updateOrderDetails(order_id.get(index),
																	jsonObject.getString("msisdn"), "F",
																	submitOrderResponse.getResponseHeader()
																			.getRetCode(),
																	Helper.getNgbssMessageFromResponseCode(
																			submitOrderResponse.getResponseHeader()
																					.getRetCode(),
																			submitOrderResponse.getResponseHeader()
																					.getRetMsg(),
																			jsonObject.getString("lang")),
																	conn, stringBuffer);
															logs.setResponseCode(submitOrderResponse.getResponseHeader()
																	.getRetCode());
															logs.setResponseDescription(submitOrderResponse
																	.getResponseHeader().getRetMsg());
															logs.setResponseDateTime(
																	Helper.GenerateDateTimeToMsAccuracy());
															logs.updateLog(logs);
														}

													}

												}

											} else {

												loggerOrderV2.info(
														token + "--------User do not have the permission--------");
												logFile(stringBuffer,
														"--------User do not have the permission--------");
												OrderProcessing.updateOrderDetails(order_id.get(index),
														jsonObject.getString("msisdn"), "F",
														ResponseCodes.ERROR_401_CODE, "actionhistory.restricted.", conn,
														stringBuffer);
												logs.setResponseCode(ResponseCodes.ERROR_401_CODE);
												logs.setResponseDescription("User Does not have the permission");
												logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
												logs.updateLog(logs);
											}
										}
									}
								}
							} else {
								loggerOrderV2.info("--------Error magento Tariff Group Details--------");
								logFile(stringBuffer, "--------Error magento Tariff Group Detail--------");
								OrderProcessing.updateOrderDetails(order_id.get(index), jsonObject.getString("msisdn"),
										"F", dataTariffDetailsbulk.getResultCode(),
										ConfigurationManager.getConfigurationFromCache(
												"actionhistory.generic.failure." + jsonObject.getString("lang")),
										conn, stringBuffer);
								logs.setResponseCode(dataTariffDetailsbulk.getResultCode());
								logs.setResponseDescription(dataTariffDetailsbulk.getMsg());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
							}

						} else {
							// get subscriber data else started

							loggerOrderV2.info("*************Error getting subscriber Info*************");
							logFile(stringBuffer, "Error getting Offring ID from Subscriber Info");

							// update single order detail
							OrderProcessing.updateOrderDetails(order_id.get(index), jsonObject.getString("msisdn"), "F",
									getSubscriberResponse.getResponseHeader().getRetCode(),
									Helper.getNgbssMessageFromResponseCode(
											getSubscriberResponse.getResponseHeader().getRetCode(),
											getSubscriberResponse.getResponseHeader().getRetMsg(),
											jsonObject.getString("lang")),
									conn, stringBuffer);

						}
						// get subscriber data else ended
					} else {
						// magento else started
						loggerOrderV2.info("--------Error magento permissions service--------");
						logFile(stringBuffer, "--------Error magento permissions service--------");
						OrderProcessing.updateOrderDetails(order_id.get(index), jsonObject.getString("msisdn"), "F",
								Integer.toString(magentoresponse.getResultCode()),
								ConfigurationManager.getConfigurationFromCache(
										"actionhistory.generic.failure." + jsonObject.getString("lang")),
								conn, stringBuffer);
						logs.setResponseCode(Integer.toString(magentoresponse.getResultCode()));
						logs.setResponseDescription(magentoresponse.getMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
					}
					// magento else ended
				} else {
					// group data else started

					loggerOrderV2.info(
							"----------Group Data list against Msisdn is : " + getGroupResponse.getGetGroupBody());
					loggerOrderV2.info("----------No Group data found Against Msisdn----------");

					logFile(stringBuffer, "No Group data found Against Msisdn ");
					String responseMsg;
					if (getGroupResponse.getResponseHeader().getRetCode() == "0") {
						responseMsg = "No Group data found Against Msisdn ";
					} else {
						responseMsg = "Bad resquest for calling Group data ";
					}
					// Update Order Details to failed and add reason in DB
					OrderProcessing.updateOrderDetails(order_id.get(index), jsonObject.getString("msisdn"), "F",
							getGroupResponse.getResponseHeader().getRetCode(),
							Helper.getNgbssMessageFromResponseCode(getGroupResponse.getResponseHeader().getRetCode(),
									getGroupResponse.getResponseHeader().getRetMsg(), jsonObject.getString("lang")),
							conn, stringBuffer);

				}
				// group data else end
			}

			// end of logic
			// updating purchase order
			OrderProcessing.updateOrderStatus(order_id.get(index), "C", conn, stringBuffer);
			// close files
			java.io.File file = new java.io.File(path + fileName);
			FileWriter fileWriter = new FileWriter(file);
			BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

			// write contents of StringBuffer to a file
			bufferedWriter.write(stringBuffer.toString());

			// flush the stream
			bufferedWriter.flush();

			// close the stream
			bufferedWriter.close();
			conn.close();

		} catch (Exception e) {
			loggerOrderV2.error("Containing Previuos Status because control comes in catch block of order");
			loggerOrderV2.error(Helper.GetException(e));
			loggerOrderV2.info((Helper.GetException(e)));
			//OrderProcessing.updateOrderStatus(order_id.get(index), "F", conn, stringBuffer);
		}

	}

	private JSONObject getOrderAttributes(String order_id, Connection conn, StringBuffer stringBuffer) {

		stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
		stringBuffer.append(System.getProperty("line.separator"));
		stringBuffer.append(Helper.getOMlogTimeStamp() + "*** Select Data from Order Attributes  ");
		stringBuffer.append(System.getProperty("line.separator"));
		stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
		stringBuffer.append(System.getProperty("line.separator"));
		JSONObject jsonObject = new JSONObject();

		try {
			String query = "select key_value, value from order_attributes where order_id_fk =?";
			PreparedStatement preparedStmt = conn.prepareStatement(query);
			preparedStmt.setString(1, order_id);
			stringBuffer.append(Helper.getOMlogTimeStamp() + " Executing Query :" + preparedStmt.toString());
			ResultSet resultSet = preparedStmt.executeQuery();

			while (resultSet.next()) {
				jsonObject.put(resultSet.getString(1), (resultSet.getString(2)));
				stringBuffer
						.append(Helper.getOMlogTimeStamp() + resultSet.getString(1) + " : " + resultSet.getString(2));
			}

			logFile(stringBuffer, "*** Select Data from Order Details For Change Tariff  ");

			query = "select msisdn,group_type,tariff_ids,corpCrmCustName from order_details where order_id_fk =?";
			preparedStmt = conn.prepareStatement(query);
			preparedStmt.setString(1, order_id);
			stringBuffer.append(Helper.getOMlogTimeStamp() + "Executing Query : " + preparedStmt.toString());
			stringBuffer.append(System.getProperty("line.separator"));
			resultSet = preparedStmt.executeQuery();
			JSONArray jArray = new JSONArray();
			while (resultSet.next()) {
				JSONObject jobj = new JSONObject();
				jobj.put("msisdn", resultSet.getString(1));
				stringBuffer.append(Helper.getOMlogTimeStamp() + " MSISDN: " + resultSet.getString(1));
				stringBuffer.append(System.getProperty("line.separator"));
				jobj.put("groupType", resultSet.getString(2));
				stringBuffer.append(Helper.getOMlogTimeStamp() + " GROUP_TYPE: " + resultSet.getString(2));
				stringBuffer.append(System.getProperty("line.separator"));
				jobj.put("tariff_ids", resultSet.getString(3));
				stringBuffer.append(Helper.getOMlogTimeStamp() + " TARIFF_IDS: " + resultSet.getString(3));
				stringBuffer.append(System.getProperty("line.separator"));
				jobj.put("corpCrmCustName", resultSet.getString(4));
				stringBuffer.append(Helper.getOMlogTimeStamp() + " corpCrmCustName: " + resultSet.getString(4));
				stringBuffer.append(System.getProperty("line.separator"));
				jArray.put(jobj);
			}
			jsonObject.put("users", jArray);
		} catch (Exception e) {
			loggerOrderV2.error(Helper.GetException(e));
		}
		return jsonObject;
	}

	StringBuffer logFile(StringBuffer stringBuffer, String msg) {
		stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
		stringBuffer.append(System.getProperty("line.separator"));
		stringBuffer.append(Helper.getOMlogTimeStamp() + msg);
		stringBuffer.append(System.getProperty("line.separator"));
		stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
		stringBuffer.append(System.getProperty("line.separator"));

		return stringBuffer;
	}
}
