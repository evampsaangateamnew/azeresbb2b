package com.evampsaanga.b2b.azerfon.ordermanagementV2.details;

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;

public class OrderDetailsRequest extends BaseRequest{

	private String orderId;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
}
