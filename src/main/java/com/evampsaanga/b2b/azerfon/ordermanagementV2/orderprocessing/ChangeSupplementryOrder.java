/**
 * 
 */
package com.evampsaanga.b2b.azerfon.ordermanagementV2.orderprocessing;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.azerfon.db.DBFactory;
import com.evampsaanga.b2b.azerfon.grouppermission.Datum;
import com.evampsaanga.b2b.azerfon.grouppermission.GroupPermissionMagentoResponse;
import com.evampsaanga.b2b.azerfon.thirdpartycall.AzerfonThirdPartyCalls;
import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.ThirdPartyNames;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.huawei.crm.service.ens.SubmitOrderResponse;

/**
 * @author HamzaFarooque
 *
 */
public class ChangeSupplementryOrder implements Order {

	public static final Logger loggerOrderV2 = Logger.getLogger("azerfonlogs-order");

	@Override
	public void processOrder(ArrayList<String> order_key, ArrayList<String> order_id) throws Exception {

		loggerOrderV2.info("In Change Limit Order ----------  Processing order");

		String TrnsactionName = Transactions.CHANGE_SUPPLIMENTRY_OFFERING_TRANSACTION_NAME_B2B;
		String token;
		java.sql.Connection conn = DBFactory.getDbConnection();
		StringBuffer stringBuffer = new StringBuffer();
		Logs logs = new Logs();
		int index = 0;
		logs.setTransactionName(TrnsactionName);
		logs.setThirdPartyName(ThirdPartyNames.CHANGE_SUPPLIMENTRY_OFFERING);
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy_HHmmss_SSSS");
		String dateTime = format.format(new Date());
		String fileName = "OM_" + dateTime + "_" + order_id.get(index) + ".log";
		// should be configurable in Constants
		String grouptype, actiontype, destinationOfferingId;
		String path = Constants.ORDER_MANAGEMENT_LOGS_FILEPATH;

		stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
		stringBuffer.append(System.getProperty("line.separator"));
		stringBuffer.append(Helper.getOMlogTimeStamp() + "***Change Limit ");
		stringBuffer.append(System.getProperty("line.separator"));
		stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
		stringBuffer.append(System.getProperty("line.separator"));

		try {
			// update the main table status
			OrderProcessing.updateOrderStatus(order_id.get(index), "P", conn, stringBuffer);
			// get the order attributes so that further processing can be done

			JSONObject jsonObject = getOrderAttributes(order_id.get(index), conn, stringBuffer);
			loggerOrderV2.info("Order Attributes: " + jsonObject);
			logFile(stringBuffer, " OrderAttributes " + jsonObject);

			// request packet to process

			JSONArray jArray = jsonObject.getJSONArray("users");
			jsonObject.remove("users");
			jsonObject.remove("type");
			jsonObject.remove("orderKey");

			for (int i = 0; i < jArray.length(); i++) {

				try {

					jsonObject.put("msisdn", jArray.getJSONObject(i).get("msisdn").toString());

					token = Helper.retrieveToken(TrnsactionName, jsonObject.getString("msisdn"));

					loggerOrderV2.info(token + "Json User Array" + jArray.toString());

					grouptype = jArray.getJSONObject(i).getString("groupType");

					actiontype = jsonObject.getString("actionType");

					destinationOfferingId = jsonObject.getString("offeringId");
					logs.setIp(jsonObject.getString("iP"));
					logs.setChannel(jsonObject.getString("channel"));
					logs.setMsisdn(jsonObject.getString("msisdn"));
					logs.setLang(jsonObject.getString("lang"));
					logs.setTariffId(destinationOfferingId);
					stringBuffer.append(
							Helper.getOMlogTimeStamp() + " --------------- MSISDN : " + jsonObject.getString("msisdn"));

					GroupPermissionMagentoResponse permissionsmagentoresponse = Helper
							.groupDataCall(jsonObject.getString("msisdn"), token, loggerOrderV2, stringBuffer);

					if (permissionsmagentoresponse != null) {
						List<String> restrictions = new ArrayList<String>();
						List<String> offeringids = new ArrayList<String>();
						// Check permissions Magento Response is success
						if (permissionsmagentoresponse.getResultCode() == 0) {
							// Permissions data processing
							if (permissionsmagentoresponse.getData().isEmpty()) {
								// if data is empty then no Restriction
								restrictions = new ArrayList<String>();
							} else {
								// check all Restrictions
								String limit;
								for (Datum datum : permissionsmagentoresponse.getData()) {
									limit = datum.getOffersRestrictions();
									loggerOrderV2.info(token + "----- Restriction elements ----:" + limit);
									if (datum.getOffersRestrictions() != null
											&& !datum.getOffersRestrictions().isEmpty()) {
										restrictions.addAll(Arrays.asList(limit.split(",")));
									}
								}
								// get all offering ids
								String offering;
								for (Datum datum : permissionsmagentoresponse.getData()) {
									offering = datum.getOfferingIds();
									loggerOrderV2.info(token + "----- offerings Id elements ----:" + offering);
									if (datum.getOfferingIds() != null && !datum.getOfferingIds().isEmpty()) {
										offeringids.addAll(Arrays.asList(offering.split(",")));
									}
								}
							}

							logFile(stringBuffer, "----- Restrictions List----:" + Helper.listToString(restrictions));
							loggerOrderV2
									.info(token + "----- Restrictions List----:" + Helper.listToString(restrictions));

							logFile(stringBuffer, "----- Restrictions List----:" + Helper.listToString(offeringids));
							loggerOrderV2
									.info(token + "----- Restrictions List----:" + Helper.listToString(offeringids));

							String user_action_type = null;
							if (actiontype.equals("1") || actiontype.equals("3") || actiontype.equals("2")) {

								// check action type and define user action
								if (actiontype.equals("1")) {
									user_action_type = "Subscribe";
								} else if (actiontype.equals("3")) {
									user_action_type = "Unsubscribe";
								} else if (actiontype.equals("2")) {
									user_action_type = "Renew";
									actiontype = "1";
								}

								loggerOrderV2.info(token + "User Action Type :: " + user_action_type);
								logFile(stringBuffer, "User Action Type :: " + user_action_type);

								if (!restrictions.contains(user_action_type)) {

									// check Offering ID restrictions for
									// subscribe
									if (user_action_type.equals("Subscribe")) {
										if (offeringids.isEmpty() || offeringids.contains(destinationOfferingId)) {
											// submit order for sub
											SubmitOrderResponse submitOrderResponse = AzerfonThirdPartyCalls
													.changeSupplimentaryOffers(jsonObject.getString("msisdn"),
															destinationOfferingId, actiontype);

											if (submitOrderResponse != null && submitOrderResponse.getResponseHeader()
													.getRetCode().equalsIgnoreCase("0")) {
												loggerOrderV2.info(token + "--------Offer Subscribed and Reponse is : "
														+ Helper.ObjectToJson(submitOrderResponse));
												logFile(stringBuffer, "--------Offer Subscribed and Reponse is : "
														+ Helper.ObjectToJson(submitOrderResponse));
												OrderProcessing.updateOrderDetails(order_id.get(index),
														jsonObject.getString("msisdn"), "C",
														submitOrderResponse.getResponseHeader().getRetCode(),
														Helper.getNgbssMessageFromResponseCode(
																submitOrderResponse.getResponseHeader().getRetCode(),
																submitOrderResponse.getResponseHeader().getRetMsg(),
																jsonObject.getString("lang")),
														conn, stringBuffer);
												logs.setResponseCode(
														submitOrderResponse.getResponseHeader().getRetCode());
												logs.setResponseDescription(
														submitOrderResponse.getResponseHeader().getRetMsg());
												logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
												logs.updateLog(logs);

											}

											else {
												loggerOrderV2.info(token + "--------Offer Subscribed and Reponse is : "
														+ Helper.ObjectToJson(submitOrderResponse));
												logFile(stringBuffer, "--------Offer Subscribed and Reponse is : "
														+ Helper.ObjectToJson(submitOrderResponse));
												OrderProcessing.updateOrderDetails(order_id.get(index),
														jsonObject.getString("msisdn"), "F",
														submitOrderResponse.getResponseHeader().getRetCode(),
														Helper.getNgbssMessageFromResponseCode(
																submitOrderResponse.getResponseHeader().getRetCode(),
																submitOrderResponse.getResponseHeader().getRetMsg(),
																jsonObject.getString("lang")),
														conn, stringBuffer);
												logs.setResponseCode(
														submitOrderResponse.getResponseHeader().getRetCode());
												logs.setResponseDescription(
														submitOrderResponse.getResponseHeader().getRetMsg());
												logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
												logs.updateLog(logs);
											}

										} else {
											loggerOrderV2.info(token
													+ "--------User do not have the permission to subscribe this offer--------");
											logFile(stringBuffer,
													"--------User do not have the permission to subscribe this offer--------");
											OrderProcessing.updateOrderDetails(order_id.get(index),
													jsonObject.getString("msisdn"), "F", ResponseCodes.ERROR_401_CODE,
													ConfigurationManager.getConfigurationFromCache(
															"actionhistory.restricted." + jsonObject.getString("lang")),
													conn, stringBuffer);
											logs.setResponseCode(ResponseCodes.ERROR_401_CODE);
											logs.setResponseDescription(
													"User do not have the permission to subscribe this offer");
											logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
											logs.updateLog(logs);
										}
									} else { // no restriction on Unsubscribe
										// submitOrder for unsub

										SubmitOrderResponse submitOrderResponse = AzerfonThirdPartyCalls
												.changeSupplimentaryOffers(jsonObject.getString("msisdn"),
														destinationOfferingId, actiontype);

										if (submitOrderResponse != null && submitOrderResponse.getResponseHeader()
												.getRetCode().equalsIgnoreCase("0")) {
											loggerOrderV2.info(token + "--------Offer Unsubscribed and Reponse is : "
													+ Helper.ObjectToJson(submitOrderResponse));
											logFile(stringBuffer, "--------Offer Unsubscribed and Reponse is : "
													+ Helper.ObjectToJson(submitOrderResponse));
											OrderProcessing.updateOrderDetails(order_id.get(index),
													jsonObject.getString("msisdn"), "C",
													submitOrderResponse.getResponseHeader().getRetCode(),
													Helper.getNgbssMessageFromResponseCode(
															submitOrderResponse.getResponseHeader().getRetCode(),
															submitOrderResponse.getResponseHeader().getRetMsg(),
															jsonObject.getString("lang")),
													conn, stringBuffer);
											logs.setResponseCode(submitOrderResponse.getResponseHeader().getRetCode());
											logs.setResponseDescription(
													submitOrderResponse.getResponseHeader().getRetMsg());
											logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
											logs.updateLog(logs);
										}

										else {
											loggerOrderV2.info(token + "--------Offer Unsubscribed and Reponse is : "
													+ Helper.ObjectToJson(submitOrderResponse));
											logFile(stringBuffer, "--------Offer Unsubscribed and Reponse is : "
													+ Helper.ObjectToJson(submitOrderResponse));
											OrderProcessing.updateOrderDetails(order_id.get(index),
													jsonObject.getString("msisdn"), "F",
													submitOrderResponse.getResponseHeader().getRetCode(),
													Helper.getNgbssMessageFromResponseCode(
															submitOrderResponse.getResponseHeader().getRetCode(),
															submitOrderResponse.getResponseHeader().getRetMsg(),
															jsonObject.getString("lang")),
													conn, stringBuffer);
											logs.setResponseCode(submitOrderResponse.getResponseHeader().getRetCode());
											logs.setResponseDescription(
													submitOrderResponse.getResponseHeader().getRetMsg());
											logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
											logs.updateLog(logs);
										}

									}
								} else { // Restriction check else

									loggerOrderV2.info(token + "--------User do not have the permission--------");
									logFile(stringBuffer, "--------User do not have the permission--------");
									OrderProcessing.updateOrderDetails(order_id.get(index),
											jsonObject.getString("msisdn"), "F", ResponseCodes.ERROR_401_CODE,
											"actionhistory.restricted.", conn, stringBuffer);
									logs.setResponseCode(ResponseCodes.ERROR_401_CODE);
									logs.setResponseDescription("User Does not have the permission");
									logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
									logs.updateLog(logs);
								}

							} else {
								loggerOrderV2.info(token + "--------Action Type Invalid--------");
								logFile(stringBuffer, "--------Action Type Invalid--------");
								OrderProcessing.updateOrderDetails(order_id.get(index), jsonObject.getString("msisdn"),
										"F", ResponseCodes.ERROR_400_CODE,
										ConfigurationManager.getConfigurationFromCache(
												"actionhistory.generic.failure." + jsonObject.getString("lang")),
										conn, stringBuffer);
								logs.setResponseCode(ResponseCodes.ERROR_400_CODE);
								logs.setResponseDescription("Action Type Invalid");
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
							}
						} else {
							// permissions magento else started
							loggerOrderV2.info(token + "--------Error from magento permissions service--------");
							logFile(stringBuffer, "--------Error from magento permissions service--------");
							OrderProcessing.updateOrderDetails(order_id.get(index), jsonObject.getString("msisdn"), "F",
									Integer.toString(permissionsmagentoresponse.getResultCode()),
									"actionhistory.generic.failure.", conn, stringBuffer);
							logs.setResponseCode(Integer.toString(permissionsmagentoresponse.getResultCode()));
							logs.setResponseDescription(permissionsmagentoresponse.getMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
						} // permissions magento else ended

					} else {
						// group data else started
						// Update Order Details to failed and add reason in DB
						OrderProcessing.updateOrderDetails(order_id.get(index), jsonObject.getString("msisdn"), "F",
								ResponseCodes.ERROR_400_CODE,
								ConfigurationManager.getConfigurationFromCache(
										"actionhistory.generic.failure." + jsonObject.getString("lang")),
								conn, stringBuffer);
						logs.setResponseCode(ResponseCodes.ERROR_400_CODE);
						logs.setResponseDescription("Error in Group data results");
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
					}
				} catch (Exception e) {
					OrderProcessing.updateOrderDetails(order_id.get(index), jsonObject.getString("msisdn"), "F",
							ResponseCodes.INTERNAL_SERVER_ERROR_CODE, "actionhistory.generic.failure.", conn,
							stringBuffer);
					loggerOrderV2.info(Helper.GetException(e));
				}

			} // for loop end bracket

			// end of logic
			// updating purchase order
			OrderProcessing.updateOrderStatus(order_id.get(index), "C", conn, stringBuffer);

			// close files
			java.io.File file = new java.io.File(path + fileName);
			FileWriter fileWriter = new FileWriter(file);
			BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

			// write contents of StringBuffer to a file
			bufferedWriter.write(stringBuffer.toString());

			// flush the stream
			bufferedWriter.flush();

			// close the stream
			bufferedWriter.close();
			conn.close();

		} catch (Exception e) {
			//OrderProcessing.updateOrderStatus(order_id.get(index), "F", conn, stringBuffer);
			loggerOrderV2.error("Containing Previuos Status because control comes in catch block of order");
			loggerOrderV2.info(Helper.GetException(e));
			loggerOrderV2.error((Helper.GetException(e)));
		}
	}

	private JSONObject getOrderAttributes(String order_id, Connection conn, StringBuffer stringBuffer) {

		stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
		stringBuffer.append(System.getProperty("line.separator"));
		stringBuffer.append(Helper.getOMlogTimeStamp() + "*** Select Data from Order Attributes  ");
		stringBuffer.append(System.getProperty("line.separator"));
		stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
		stringBuffer.append(System.getProperty("line.separator"));
		JSONObject jsonObject = new JSONObject();

		try {
			String query = "select key_value, value from order_attributes where order_id_fk =?";
			PreparedStatement preparedStmt = conn.prepareStatement(query);
			preparedStmt.setString(1, order_id);
			stringBuffer.append(Helper.getOMlogTimeStamp() + " Executing Query :" + preparedStmt.toString());
			ResultSet resultSet = preparedStmt.executeQuery();

			while (resultSet.next()) {
				jsonObject.put(resultSet.getString(1), (resultSet.getString(2)));
				stringBuffer
						.append(Helper.getOMlogTimeStamp() + resultSet.getString(1) + " : " + resultSet.getString(2));
			}

			logFile(stringBuffer, "***Select Data from Order Details Change Supplemantry ");

			query = "select msisdn,group_type from order_details where order_id_fk =?";
			preparedStmt = conn.prepareStatement(query);
			preparedStmt.setString(1, order_id);
			stringBuffer.append(Helper.getOMlogTimeStamp() + "Executing Query : " + preparedStmt.toString());
			stringBuffer.append(System.getProperty("line.separator"));
			resultSet = preparedStmt.executeQuery();
			JSONArray jArray = new JSONArray();
			while (resultSet.next()) {
				JSONObject jobj = new JSONObject();
				jobj.put("msisdn", resultSet.getString(1));
				stringBuffer.append(Helper.getOMlogTimeStamp() + " MSISDN: " + resultSet.getString(1));
				stringBuffer.append(System.getProperty("line.separator"));
				jobj.put("groupType", resultSet.getString(2));
				stringBuffer.append(Helper.getOMlogTimeStamp() + " GROUP_TYPE: " + resultSet.getString(2));
				stringBuffer.append(System.getProperty("line.separator"));
				jArray.put(jobj);
			}
			jsonObject.put("users", jArray);
			resultSet.close();
			preparedStmt.close();

		} catch (Exception e) {
			loggerOrderV2.debug(Helper.GetException(e));
		}
		return jsonObject;
	}

	StringBuffer logFile(StringBuffer stringBuffer, String msg) {
		stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
		stringBuffer.append(System.getProperty("line.separator"));
		stringBuffer.append(Helper.getOMlogTimeStamp() + msg);
		stringBuffer.append(System.getProperty("line.separator"));
		stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
		stringBuffer.append(System.getProperty("line.separator"));

		return stringBuffer;
	}
}
