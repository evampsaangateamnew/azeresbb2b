/**
 * 
 */
package com.evampsaanga.b2b.azerfon.ordermanagementV2.orderprocessing;

import java.util.ArrayList;

/**
 * @author HamzaFarooque
 *
 */
public interface Order {
	
	void processOrder(ArrayList<String> order_key, ArrayList<String> order_id) throws Exception;
	
}
