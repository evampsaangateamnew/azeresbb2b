/**
 * 
 */
package com.evampsaanga.b2b.azerfon.ordermanagementV2.orderprocessing;

import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.evampsaanga.b2b.azerfon.db.DBFactory;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Helper;

/**
 * @author HamzaFarooque
 *
 */
public class ChangeGroupOrder implements Order {

	public static final Logger loggerOrderV2 = Logger.getLogger("azerfonlogs-order");

	@Override
	public void processOrder(ArrayList<String> order_key, ArrayList<String> order_id) {

		String TrnsactionName = Transactions.CHANGE_GROUP;
		String token;
		java.sql.Connection conn = DBFactory.getDbConnection();

		StringBuffer stringBuffer = new StringBuffer();

		try {
			OrderProcessing.updateOrderDetails(order_id.get(0), "", "F", "07", "Not Developed yet", conn, stringBuffer);
			
//			updating purchase order
			OrderProcessing.updateOrderStatus(order_id.get(0), "C", conn, stringBuffer);
		} catch (SQLException e) {

			loggerOrderV2.info(Helper.GetException(e));
		}
	}

}
