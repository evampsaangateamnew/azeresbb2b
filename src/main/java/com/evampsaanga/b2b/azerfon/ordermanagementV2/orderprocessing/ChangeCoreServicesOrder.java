package com.evampsaanga.b2b.azerfon.ordermanagementV2.orderprocessing;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.azerfon.callforwarding.CallForwardResponse;
import com.evampsaanga.b2b.azerfon.callforwarding.CallForwardingRequestLand;
import com.evampsaanga.b2b.azerfon.callforwarding.ProcessCoreServicesRequestV2;
import com.evampsaanga.b2b.azerfon.db.DBFactory;
import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.ThirdPartyNames;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Helper;

public class ChangeCoreServicesOrder implements Order {

	public static final Logger loggerOrderV2 = Logger.getLogger("azerfonlogs-order");

	@Override
	public void processOrder(ArrayList<String> order_key, ArrayList<String> order_id) {

		String token;
		Logs logs = new Logs();
		String TrnsactionName = Transactions.CORE_SERVICES;
		java.sql.Connection conn = DBFactory.getDbConnection();
		logs.setTransactionName(TrnsactionName);
		logs.setThirdPartyName(ThirdPartyNames.GET_CORE_SERVICES);
		int index = 0;
		StringBuffer stringBuffer = new StringBuffer();
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy_HHmmss_SSSS");
		String dateTime = format.format(new Date());
		String fileName = "OM_" + dateTime + "_" + order_id.get(index) + ".log";
		// should be configurable in Constants
		String path = Constants.ORDER_MANAGEMENT_LOGS_FILEPATH;

		stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
		stringBuffer.append(System.getProperty("line.separator"));
		stringBuffer.append(Helper.getOMlogTimeStamp() + "***Loading Process Core Services ");
		stringBuffer.append(System.getProperty("line.separator"));
		stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
		stringBuffer.append(System.getProperty("line.separator"));

		ProcessCoreServicesRequestV2 requestV2 = new ProcessCoreServicesRequestV2();
		CallForwardingRequestLand coreServices = new CallForwardingRequestLand();
		CallForwardResponse response = new CallForwardResponse();
		// update the main table status

		try {
			OrderProcessing.updateOrderStatus(order_id.get(index), "P", conn, stringBuffer);
			// get the order attributes so that further processing can
			// be done

			JSONObject jsonObject = getOrderAttributes(order_id.get(index), conn, stringBuffer);
			loggerOrderV2.info("Order Attributes: " + jsonObject);
			// request packet to process
			JSONArray jArray = jsonObject.getJSONArray("users");
			jsonObject.remove("users");
			jsonObject.remove("type");
			jsonObject.remove("orderKey");

			stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
			stringBuffer.append(System.getProperty("line.separator"));
			stringBuffer
					.append(Helper.getOMlogTimeStamp() + "***Fetch data from order details for Process Core Services ");
			stringBuffer.append(System.getProperty("line.separator"));
			stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
			stringBuffer.append(System.getProperty("line.separator"));

			for (int i = 0; i < jArray.length(); i++) {
				jsonObject.put("msisdn", jArray.getJSONObject(i).get("msisdn").toString());
				loggerOrderV2.info("msisdn is: " + jsonObject.getString("msisdn"));
				stringBuffer.append(Helper.getOMlogTimeStamp() + " MSISDN : " + jsonObject.getString("msisdn"));
				String getSuborderQuery = "select msisdn from order_details where order_id_fk='" + order_id.get(index)
						+ "' and msisdn='" + jsonObject.getString("msisdn") + "' and (status='P')";
				PreparedStatement preparedStatement = DBFactory.getDbConnection().prepareStatement(getSuborderQuery);
				stringBuffer.append(Helper.getOMlogTimeStamp() + "Execute Query : " + preparedStatement.toString());
				ResultSet resultSet = preparedStatement.executeQuery();
				
				logs.setIp(jsonObject.getString("iP"));
				logs.setChannel(jsonObject.getString("channel"));
				logs.setMsisdn(jsonObject.getString("msisdn"));
				logs.setLang(jsonObject.getString("lang"));
				token = Helper.retrieveToken(TrnsactionName, jsonObject.getString("msisdn"));
				if (resultSet.next()) {

					stringBuffer
							.append(Helper.getOMlogTimeStamp() + "************************************************");
					stringBuffer.append(System.getProperty("line.separator"));
					stringBuffer.append(Helper.getOMlogTimeStamp() + "***Calling process CoreService on process order ");
					stringBuffer.append(System.getProperty("line.separator"));

					stringBuffer.append(Helper.getOMlogTimeStamp() + " REQUEST_PACKET " + jsonObject.toString());
					stringBuffer.append(System.getProperty("line.separator"));
					requestV2 = Helper.JsonToObject(jsonObject.toString(), ProcessCoreServicesRequestV2.class);

					response = coreServices.processCoreServices(requestV2, token);

					stringBuffer
							.append(Helper.getOMlogTimeStamp() + " RESPONSE_PACKET " + Helper.ObjectToJson(response));
					stringBuffer.append(System.getProperty("line.separator"));
					stringBuffer
							.append(Helper.getOMlogTimeStamp() + "************************************************");
					stringBuffer.append(System.getProperty("line.separator"));

					if (response.getReturnCode().equals(ResponseCodes.SUCESS_CODE_200)) {
						// updating order details
						OrderProcessing.updateOrderDetails(order_id.get(index), jsonObject.getString("msisdn"), "C",
								ResponseCodes.SUCESS_CODE_200,Helper.getNgbssMessageFromResponseCode(
										response.getReturnCode(),
										response.getReturnMsg(),
										requestV2.getLang()), conn, stringBuffer);
						logs.setResponseCode(ResponseCodes.SUCESS_CODE_200);
						logs.setResponseDescription(response.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						// updating purchase order
						OrderProcessing.updateOrderStatus(order_id.get(index), "C", conn, stringBuffer);
					} else {
						// updating order details
						OrderProcessing.updateOrderDetails(order_id.get(index), jsonObject.getString("msisdn"), "F",
								response.getReturnCode(), Helper.getNgbssMessageFromResponseCode(
										response.getReturnCode(),
										response.getReturnMsg(),
										requestV2.getLang()),
								conn, stringBuffer);
						logs.setResponseCode(response.getReturnCode());
						logs.setResponseDescription(response.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
					}
				} else {

					stringBuffer.append(Helper.getOMlogTimeStamp() + " No Record found");
					logs.setResponseCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
					logs.setResponseDescription("No Record found");
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
				}
			}
			// update the main table status
			OrderProcessing.updateOrderStatus(order_id.get(index), "C", conn, stringBuffer);
		} catch (Exception e) {
			// updating purchase order
			loggerOrderV2.error(Helper.GetException(e));
			try {
				logs.setResponseCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
				logs.setResponseDescription(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				OrderProcessing.updateOrderStatus(order_id.get(index), "F", conn, stringBuffer);
			} catch (SQLException e1) {
				loggerOrderV2.error(Helper.GetException(e1));
			}
		}
		try {
			java.io.File file = new java.io.File(path + fileName);
			FileWriter fileWriter = new FileWriter(file);
			BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
			// write contents of StringBuffer to a file
			bufferedWriter.write(stringBuffer.toString());

			// flush the stream
			bufferedWriter.flush();

			// close the stream
			bufferedWriter.close();
			conn.close();
		} catch (Exception e) {
			loggerOrderV2.error(Helper.GetException(e));
		}
	}

	@SuppressWarnings("resource")
	private JSONObject getOrderAttributes(String order_id, Connection conn, StringBuffer stringBuffer2)
			throws IOException, Exception {

		stringBuffer2.append(Helper.getOMlogTimeStamp() + "************************************************");
		stringBuffer2.append(System.getProperty("line.separator"));
		stringBuffer2.append(Helper.getOMlogTimeStamp() + "*** Select Data from Order Attributes  ");
		stringBuffer2.append(System.getProperty("line.separator"));
		stringBuffer2.append(Helper.getOMlogTimeStamp() + "************************************************");
		stringBuffer2.append(System.getProperty("line.separator"));

		String query = "select key_value, value from order_attributes where order_id_fk =?";
		PreparedStatement preparedStmt = conn.prepareStatement(query);
		preparedStmt.setString(1, order_id);
		stringBuffer2.append(Helper.getOMlogTimeStamp() + " Executing Query :" + preparedStmt.toString());
		ResultSet resultSet = preparedStmt.executeQuery();
		JSONObject jsonObject = new JSONObject();
		while (resultSet.next()) {
			jsonObject.put(resultSet.getString(1), (resultSet.getString(2)));
			stringBuffer2.append(Helper.getOMlogTimeStamp() + resultSet.getString(1) + " : " + resultSet.getString(2));

		}
		stringBuffer2.append(Helper.getOMlogTimeStamp() + "************************************************");
		stringBuffer2.append(System.getProperty("line.separator"));
		stringBuffer2.append(Helper.getOMlogTimeStamp() + "*** Select Data from Order Details PCS  ");
		stringBuffer2.append(System.getProperty("line.separator"));
		stringBuffer2.append(Helper.getOMlogTimeStamp() + "************************************************");
		stringBuffer2.append(System.getProperty("line.separator"));

		query = "select msisdn from order_details where order_id_fk =?";
		preparedStmt = conn.prepareStatement(query);
		preparedStmt.setString(1, order_id);
		stringBuffer2.append(Helper.getOMlogTimeStamp() + "Executing Query : " + preparedStmt.toString());
		resultSet = preparedStmt.executeQuery();
		JSONArray jArray = new JSONArray();
		while (resultSet.next()) {
			JSONObject jobj = new JSONObject();
			jobj.put("msisdn", resultSet.getString(1));
			stringBuffer2.append(Helper.getOMlogTimeStamp() + " MSISDN: " + resultSet.getString(1));
			stringBuffer2.append(System.getProperty("line.separator"));
			jArray.put(jobj);
		}
		jsonObject.put("users", jArray);
		jsonObject.remove("type");

		loggerOrderV2.info("attributes are: " + jsonObject);
		resultSet.close();
		preparedStmt.close();
		return jsonObject;
	}

}
