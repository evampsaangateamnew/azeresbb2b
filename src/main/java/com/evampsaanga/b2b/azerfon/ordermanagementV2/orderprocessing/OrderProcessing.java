/**
 * 
 */
package com.evampsaanga.b2b.azerfon.ordermanagementV2.orderprocessing;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.springframework.transaction.annotation.Transactional;

import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.azerfon.db.DBFactory;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.developer.utils.Helper;

/**
 * @author HamzaFarooque
 *
 */
public class OrderProcessing {
	public static final Logger loggerOrderV2 = Logger.getLogger("azerfonlogs-order");

	/**
	 * MAIN function to process orders. This function is executed via cron
	 * 
	 * @throws SQLException
	 * @throws Exception
	 */
	public void processingOrdersFromDB() throws SQLException, Exception {
		Logs logs = new Logs();
		try {

			java.sql.Connection conn = DBFactory.getDbConnection();
			Map<String, ArrayList<String>> map = getOrderID(conn);
			ArrayList<String> order_key_array = map.get("Order_Key");
			ArrayList<String> order_id_array = map.get("Order_Id");

			OrderFactory orderfactory = new OrderFactory();

			for (String orderkey : order_key_array) {

				Order order = orderfactory.getShape(orderkey, loggerOrderV2);
				if (order != null) {
					order.processOrder(order_key_array, order_id_array);
				} else {
					loggerOrderV2.info("Order Key Not Matched");
				}
			}

		} catch (Exception e) {
			loggerOrderV2.error(Helper.GetException(e));
			Constants.logger.error(Helper.GetException(e));
			logs.setResponseCode(ResponseCodes.ERROR_400_CODE);
			logs.setResponseDescription(e.getMessage());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return;
		}
	}

	private Map<String, ArrayList<String>> getOrderID(Connection conn) throws SQLException, JSONException {
		String query = "select order_key,order_id from purchase_order where status =? AND host=? LIMIT 1";

		PreparedStatement preparedStmt = conn.prepareStatement(query);
		preparedStmt.setString(1, "N");
		preparedStmt.setString(2, Helper.currentMachineIPAddress());
		loggerOrderV2.info(query);
		ResultSet rSet = preparedStmt.executeQuery();
		Map<String, ArrayList<String>> map = new HashMap<>();
		ArrayList<String> order_key = new ArrayList<>();
		ArrayList<String> order_id = new ArrayList<>();
		while (rSet.next()) {
			order_key.add(rSet.getString("ORDER_KEY"));
			order_id.add(rSet.getString("ORDER_ID"));
			Constants.logger.info("Pending Order Type is: " + rSet.getString("ORDER_KEY")
					+ "  >>>>> <<<<< Order Id is: " + rSet.getString("ORDER_ID"));

			loggerOrderV2.info("Pending Order Type is: " + rSet.getString("ORDER_KEY") + "  >>>>> <<<<< Order Id is: "
					+ rSet.getString("ORDER_ID"));
		}
		map.put("Order_Key", order_key);
		map.put("Order_Id", order_id);
		rSet.close();
		preparedStmt.close();
		return map;
	}

	/**
	 * helper function to update order status upon processing
	 * 
	 * @param order_id
	 * @param status
	 * @param conn
	 * @param stringBuffer
	 * @return Returns true if any failed orders found else false
	 * @throws SQLException
	 */
	@Transactional(rollbackFor = Exception.class)
	public static boolean updateOrderStatus(String order_id, String status, Connection conn, StringBuffer stringBuffer)
			throws SQLException {
		String query = "update purchase_order set status = ? where order_id= ?";
		if (conn.isClosed())
			conn = DBFactory.getDbConnection();
		PreparedStatement preparedStmt = conn.prepareStatement(query);
		preparedStmt = conn.prepareStatement(query);
		preparedStmt.setString(1, status);
		preparedStmt.setString(2, order_id);

		loggerOrderV2.info("QUERY for update Order_purchase" + preparedStmt.toString());
		preparedStmt.execute();
		int count = preparedStmt.getUpdateCount();
		// check is added for order management,in case of process order logging
		// will not
		// work
		if (stringBuffer != null) {

			stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
			stringBuffer.append(System.getProperty("line.separator"));
			stringBuffer.append(Helper.getOMlogTimeStamp() + "*** Update Purchase Order Details  ");
			stringBuffer.append(System.getProperty("line.separator"));
			stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
			stringBuffer.append(System.getProperty("line.separator"));
			stringBuffer.append(Helper.getOMlogTimeStamp() + "Executing Query : " + preparedStmt.toString());
			stringBuffer.append(System.getProperty("line.separator"));

			if (count > 0) {
				stringBuffer.append(Helper.getOMlogTimeStamp() + "Record updated : ");
				stringBuffer.append(System.getProperty("line.separator"));
			} else {
				stringBuffer.append(Helper.getOMlogTimeStamp() + "Record updated : ");
				stringBuffer.append(System.getProperty("line.separator"));
			}
		}
		if (count > 0)
			return true;
		else
			return false;
	}

	/**
	 * helper function to update order details
	 * 
	 * @param order_id
	 * @param msisdn
	 * @param status
	 * @param responseCode
	 * @param responseDesc
	 * @param conn
	 * @param stringBuffer
	 * @return Returns true if the update is successful else false
	 * @throws SQLException
	 */
	@Transactional(rollbackFor = Exception.class)
	public static boolean updateOrderDetails(String order_id, String msisdn, String status, String responseCode,
			String responseDesc, Connection conn, StringBuffer stringBuffer) throws SQLException {
		if (conn.isClosed())
			conn = DBFactory.getDbConnection();
		loggerOrderV2.info("=======now IN updateOrderDetails Method=======");

		stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
		stringBuffer.append(System.getProperty("line.separator"));
		stringBuffer.append(Helper.getOMlogTimeStamp() + "*** Update Order Details  ");
		stringBuffer.append(System.getProperty("line.separator"));
		stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
		stringBuffer.append(System.getProperty("line.separator"));

		String query = "";
		if (msisdn.equals("")) {
			query = "update order_details set status = ?, responsecode=?, responsedescription=? where order_id_fk= ? and status='F'";
		} else
			query = "update order_details set status = ?, responsecode=?, responsedescription=? where order_id_fk= ? and msisdn=?";
		// String
		PreparedStatement preparedStmt = conn.prepareStatement(query);
		preparedStmt = conn.prepareStatement(query);
		preparedStmt.setString(1, status);
		preparedStmt.setString(2, responseCode);
		preparedStmt.setString(3, responseDesc);
		preparedStmt.setString(4, order_id);
		if (!msisdn.equals(""))
			preparedStmt.setString(5, msisdn);

		stringBuffer.append(Helper.getOMlogTimeStamp() + "Execute Query : " + preparedStmt.toString());
		stringBuffer.append(System.getProperty("line.separator"));

		loggerOrderV2.info("QUERY for update Order_purchase" + preparedStmt.toString());
		preparedStmt.execute();
		int count = preparedStmt.getUpdateCount();
		loggerOrderV2.info("Count In updateOrderDetails " + count);
		if (count > 0) {
			conn.close();
			stringBuffer.append(Helper.getOMlogTimeStamp() + "Record updated : ");
			stringBuffer.append(System.getProperty("line.separator"));
			return true;
		} else {
			conn.close();
			stringBuffer.append(Helper.getOMlogTimeStamp() + "Record updation failed : ");
			stringBuffer.append(System.getProperty("line.separator"));
			return false;
		}
	}

}