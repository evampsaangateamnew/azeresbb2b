/**
 * 
 */
package com.evampsaanga.b2b.azerfon.ordermanagementV2.orderprocessing;

import org.apache.log4j.Logger;

import com.evampsaanga.b2b.configs.Constants;

/**
 * @author HamzaFarooque
 *
 */
public class OrderFactory {

	public Order getShape(String orderKey, Logger logger) {
		if (orderKey == null || orderKey.isEmpty()) {
			if (logger != null)
				logger.info("Order key is Null or Empty.");
			return null;
			
		} else if (orderKey.equalsIgnoreCase(Constants.BROADCAST_SMS)) {
			return new BroadcastSMSOrder();

		} else if (orderKey.equalsIgnoreCase(Constants.CHANGEL_GROUP)) {
			return new ChangeGroupOrder();

		} else if (orderKey.equalsIgnoreCase(Constants.CHANGE_SUPPLEMENTARY)) {
			return new ChangeSupplementryOrder();

		} else if (orderKey.equalsIgnoreCase(Constants.CHANGEL_TARIFF)) {
			return new ChangeTariffOrder();

		} else if (orderKey.equalsIgnoreCase(Constants.CORE_SERVICES)) {
			return new ChangeCoreServicesOrder();

		} else if (orderKey.equalsIgnoreCase(Constants.CHANGEL_PAYMENT_RELATION_KEY)) {
			return new ChangeLimitOrder();

		} else if (orderKey.equalsIgnoreCase(Constants.CHANGEL_CUG)) {
			return new ChangeCugOrder();
			
		} else if (orderKey.equalsIgnoreCase(Constants.SIM_SWAP)) {
			return new SimSwapOrder();
		}
		return null;
	}

}
