/**
 * 
 */
package com.evampsaanga.b2b.azerfon.ordermanagementV2.orderprocessing;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Logger;

import com.evampsaanga.b2b.azerfon.db.DBFactory;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Helper;

/**
 * @author HamzaFarooque
 *
 */
public class ChangeCugOrder implements Order {

	public static final Logger loggerOrderV2 = Logger.getLogger("azerfonlogs-order");

	@Override
	public void processOrder(ArrayList<String> order_key, ArrayList<String> order_id) throws Exception {

		String TrnsactionName = Transactions.CUG_TRANSACTION_NAME;

		int index = 0;
		loggerOrderV2.debug("Broadcast SMS Order ----------  Processing order");

		Connection conn = DBFactory.getDbConnection();

		StringBuffer stringBuffer = new StringBuffer();
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy_HHmmss_SSSS");
		String dateTime = format.format(new Date());
		String fileName = "OM_" + dateTime + "_" + order_id.get(index) + ".log";
		String path = Constants.ORDER_MANAGEMENT_LOGS_FILEPATH;
		
		try {
			OrderProcessing.updateOrderDetails(order_id.get(0), "", "F", "07", "Not Developed yet", conn, stringBuffer);

//			updating purchase order
			OrderProcessing.updateOrderStatus(order_id.get(0), "C", conn, stringBuffer);
		} catch (SQLException e) {

			loggerOrderV2.info(Helper.GetException(e));
		}
		
	}

}
