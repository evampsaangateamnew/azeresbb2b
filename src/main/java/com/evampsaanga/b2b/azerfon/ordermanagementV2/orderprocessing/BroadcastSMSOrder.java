package com.evampsaanga.b2b.azerfon.ordermanagementV2.orderprocessing;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;

import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.azerfon.db.DBFactory;
import com.evampsaanga.b2b.azerfon.freesms.SendSMSRequest;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.ThirdPartyNames;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Helper;

/**
 * @author HamzaFarooque
 *
 */
public class BroadcastSMSOrder implements Order {
	public static final Logger loggerOrderV2 = Logger.getLogger("azerfonlogs-order");

	@Override
	public void processOrder(ArrayList<String> order_key, ArrayList<String> order_id) {

		String TrnsactionName = Transactions.BROADCAST_SMS;
		Logs logs = new Logs();
		logs.setTransactionName(TrnsactionName);
		logs.setThirdPartyName(ThirdPartyNames.SEND_SMS_MAIN);
		int index = 0;
		loggerOrderV2.debug("Broadcast SMS Order ----------  Processing order");

		Connection conn = DBFactory.getDbConnection();
	
		
		StringBuffer stringBuffer = new StringBuffer();
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy_HHmmss_SSSS");
		String dateTime = format.format(new Date());
		String fileName = "OM_" + dateTime + "_" + order_id.get(index) + ".log";
		// should be configurable in Constants
		String path = Constants.ORDER_MANAGEMENT_LOGS_FILEPATH;
		// the following statement is used to log any messages

		stringBuffer.append(
				Helper.getOMlogTimeStamp() + "------------------------------------------------------------------");
		stringBuffer.append(System.getProperty("line.separator"));
		stringBuffer.append(
				Helper.getOMlogTimeStamp() + "                 Initialization of WorkOrder                      ");
		stringBuffer.append(System.getProperty("line.separator"));
		stringBuffer.append(
				Helper.getOMlogTimeStamp() + "------------------------------------------------------------------");
		stringBuffer.append(System.getProperty("line.separator"));

		stringBuffer.append(
				Helper.getOMlogTimeStamp() + "******************************************************************");
		stringBuffer.append(System.getProperty("line.separator"));
		stringBuffer.append(
				Helper.getOMlogTimeStamp() + "                 Loading Broadcast sms process                    ");
		stringBuffer.append(System.getProperty("line.separator"));
		stringBuffer.append(
				Helper.getOMlogTimeStamp() + "******************************************************************");
		stringBuffer.append(System.getProperty("line.separator"));

		SendSMSRequest request = new SendSMSRequest();

		stringBuffer.append(Helper.getOMlogTimeStamp() + " Updating status \"P\" of Orders " + order_id.get(index));
		stringBuffer.append(System.getProperty("line.separator"));

		stringBuffer.append(Helper.getOMlogTimeStamp() + "  Order ID: " + order_id.get(index));
		stringBuffer.append(System.getProperty("line.separator"));

		String token;
		try {

			// updating order status from N to P
			OrderProcessing.updateOrderStatus(order_id.get(index), "P", conn, stringBuffer);

			// selecting order attributes , logs are done in
			// seperate file

			JSONObject jsonObject = getOrderAttributes(order_id.get(index), conn, stringBuffer);
			loggerOrderV2.info("Order Attributes: " + jsonObject);
			String[] idString = jsonObject.get("recieverMsisdn").toString().split(",");
			jsonObject.remove("recieverMsisdn");
			jsonObject.remove("type");
			jsonObject.remove("orderKey");
			String senderName = jsonObject.getString("senderName");
			jsonObject.remove("senderName");
			jsonObject.remove("msisdn");
			jsonObject.put("msisdn", senderName);

			stringBuffer.append(
					Helper.getOMlogTimeStamp() + "******************************************************************");
			stringBuffer.append(System.getProperty("line.separator"));
			stringBuffer.append(Helper.getOMlogTimeStamp()
					+ " Fetch data from order details for processing Broadcast messages    ");
			stringBuffer.append(System.getProperty("line.separator"));
			stringBuffer.append(
					Helper.getOMlogTimeStamp() + "******************************************************************");
			stringBuffer.append(System.getProperty("line.separator"));

			for (int j = 0; j < idString.length; j++) {
				logs.setIp(jsonObject.getString("iP"));
				logs.setChannel(jsonObject.getString("channel"));
				logs.setLang(jsonObject.getString("lang"));
				
				jsonObject.put("recieverMsisdn", idString[j]);
				
				logs.setMsisdn(jsonObject.getString("recieverMsisdn"));
				// start of new logic
				String getSuborderQuery = "select msisdn from order_details where order_id_fk='" + order_id.get(index)
						+ "' and msisdn='" + jsonObject.getString("recieverMsisdn") + "' and (status='P')";
				PreparedStatement preparedStatement = DBFactory.getDbConnection().prepareStatement(getSuborderQuery);

				stringBuffer.append(Helper.getOMlogTimeStamp() + "Execute Query : " + getSuborderQuery);
				stringBuffer.append(System.getProperty("line.separator"));

				ResultSet resultSet = preparedStatement.executeQuery();
				if (resultSet.next()) {
					request = Helper.JsonToObject(jsonObject.toString(), SendSMSRequest.class);

					request.setTextmsg(new String(Base64.decodeBase64(request.getTextmsg().getBytes())));

					stringBuffer.append(Helper.getOMlogTimeStamp() + "Text Message : " + request.getTextmsg());
					stringBuffer.append(System.getProperty("line.separator"));

					token = Helper.retrieveToken(TrnsactionName, request.getmsisdn());

					loggerOrderV2.info(request.getTextmsg());

					loggerOrderV2.info("Calling insertMessageIntoEsmg ");
					int upFlag = DBFactory.insertMessageIntoEsmg(request.getRecieverMsisdn(), request.getTextmsg(),
							request.getLang(), request.getmsisdn(), stringBuffer, token);

					loggerOrderV2.info("Value of UpFlag is :::: " + upFlag);
					if (upFlag > 0) {

						stringBuffer.append(Helper.getOMlogTimeStamp() + " Record inserted into Database");
						stringBuffer.append(System.getProperty("line.separator"));

						stringBuffer.append(Helper.getOMlogTimeStamp() + " Update Order status to C ");
						stringBuffer.append(System.getProperty("line.separator"));

						OrderProcessing.updateOrderDetails(order_id.get(index), idString[j], "C",
								ResponseCodes.SUCESS_CODE_200, "actionhistory.generic.success.", conn, stringBuffer);
						logs.setResponseCode(ResponseCodes.SUCESS_CODE_200);
						logs.setResponseDescription(ResponseCodes.SUCESS_DES_200);
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);

					} else {
						stringBuffer.append(Helper.getOMlogTimeStamp() + " Record inserted Failed");
						stringBuffer.append(System.getProperty("line.separator"));

						// updating order details
						stringBuffer.append(Helper.getOMlogTimeStamp() + " Update Order status to F ");
						stringBuffer.append(System.getProperty("line.separator"));
						OrderProcessing.updateOrderDetails(order_id.get(index), idString[j], "F",
								ResponseCodes.UNSUCCESS_CODE, "actionhistory.generic.failure.", conn, stringBuffer);
						logs.setResponseCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
						logs.setResponseDescription(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
					}
				} else {
					stringBuffer.append(Helper.getOMlogTimeStamp() + "No record found ");
					stringBuffer.append(System.getProperty("line.separator"));
					stringBuffer.append(Helper.getOMlogTimeStamp() + "No record found ");
					stringBuffer.append(System.getProperty("line.separator"));
				}
			}
			// end of new logic
			// updating purchase order
			OrderProcessing.updateOrderStatus(order_id.get(index), "C", conn, stringBuffer);
			// close files
			java.io.File file = new java.io.File(path + fileName);
			FileWriter fileWriter = new FileWriter(file);
			BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

			// write contents of StringBuffer to a file
			bufferedWriter.write(stringBuffer.toString());

			// flush the stream
			bufferedWriter.flush();

			// close the stream
			bufferedWriter.close();
			conn.close();

		} catch (Exception e) {
			
			loggerOrderV2.error("Containing Previuos Status because control comes in catch block of order");
			loggerOrderV2.error((Helper.GetException(e)));

		}

	}

	/**
	 * Fetches order details from DB
	 * 
	 * @param order_id
	 * @param conn
	 * @param stringBuffer2
	 * @return Json object containing order details
	 * @throws IOException
	 * @throws Exception
	 */
	@SuppressWarnings("resource")
	private JSONObject getOrderAttributes(String order_id, Connection conn, StringBuffer stringBuffer2)
			throws IOException, Exception {

		stringBuffer2.append(Helper.getOMlogTimeStamp() + "************************************************");
		stringBuffer2.append(System.getProperty("line.separator"));
		stringBuffer2.append(Helper.getOMlogTimeStamp() + "*** Select Data from Order Attributes  ");
		stringBuffer2.append(System.getProperty("line.separator"));
		stringBuffer2.append(Helper.getOMlogTimeStamp() + "************************************************");
		stringBuffer2.append(System.getProperty("line.separator"));

		String query = "select key_value, value from order_attributes where order_id_fk =?";
		PreparedStatement preparedStmt = conn.prepareStatement(query);
		preparedStmt.setString(1, order_id);
		stringBuffer2.append(Helper.getOMlogTimeStamp() + " Executing Query :" + preparedStmt.toString());
		ResultSet resultSet = preparedStmt.executeQuery();
		JSONObject jsonObject = new JSONObject();
		while (resultSet.next()) {
			jsonObject.put(resultSet.getString(1), (resultSet.getString(2)));
			stringBuffer2.append(Helper.getOMlogTimeStamp() + resultSet.getString(1) + " : " + resultSet.getString(2));
		}
		stringBuffer2.append(Helper.getOMlogTimeStamp() + "************************************************");
		stringBuffer2.append(System.getProperty("line.separator"));
		stringBuffer2.append(Helper.getOMlogTimeStamp() + "*** Select Data from Order Details For Broadcast SMS  ");
		stringBuffer2.append(System.getProperty("line.separator"));
		stringBuffer2.append(Helper.getOMlogTimeStamp() + "************************************************");
		stringBuffer2.append(System.getProperty("line.separator"));

		query = "select msisdn from order_details where order_id_fk =?";
		preparedStmt = conn.prepareStatement(query);
		preparedStmt.setString(1, order_id);
		stringBuffer2.append(Helper.getOMlogTimeStamp() + "Executing Query : " + preparedStmt.toString());
		stringBuffer2.append(System.getProperty("line.separator"));

		resultSet = preparedStmt.executeQuery();
		StringBuffer stringBuffer = new StringBuffer();
		while (resultSet.next()) {

			stringBuffer.append(resultSet.getString(1)).append(",");
			stringBuffer2.append(Helper.getOMlogTimeStamp() + " MSISDN: " + resultSet.getString(1));

		}
		stringBuffer.replace(stringBuffer.length() - 1, stringBuffer.length(), "");
		jsonObject.put("recieverMsisdn", stringBuffer.toString());
		loggerOrderV2.info("attributes are: " + jsonObject);
		resultSet.close();
		preparedStmt.close();
		return jsonObject;
	}

}
