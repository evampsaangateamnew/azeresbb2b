package com.evampsaanga.b2b.azerfon.ordermanagementV2.orderprocessing;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.azerfon.db.DBFactory;
import com.evampsaanga.b2b.azerfon.thirdpartycall.AzerfonThirdPartyCalls;
import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.ThirdPartyNames;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.evampsaanga.b2b.mhmclient.SimSwapRequest;
import com.evampsaanga.b2b.mhmclient.SimSwapService;
import com.huawei.bme.cbsinterface.arservices.AdjustmentResultMsg;
import com.huawei.bme.cbsinterface.cbscommon.ResultHeader;
import com.huawei.crm.azerfon.bss.query.GetSubscriberResponse;

import https.www_e_gov.AddApplicationResponse;

public class SimSwapOrder implements Order {

	public static final Logger loggerOrderV2 = Logger.getLogger("azerfonlogs-order");

	@Override
	public void processOrder(ArrayList<String> order_key, ArrayList<String> order_id) throws Exception {

		String TrnsactionName = Transactions.SIM_SWAP;
		Logs logs = new Logs();
		int index = 0;
		loggerOrderV2.debug("Sim Swap Order ----------  Processing order");

		Connection conn = DBFactory.getDbConnection();
		String token;
		logs.setTransactionName(TrnsactionName);
		logs.setThirdPartyName(ThirdPartyNames.THIRD_PARTY_SIM_SWAP);
		StringBuffer stringBuffer = new StringBuffer();
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy_HHmmss_SSSS");
		String dateTime = format.format(new Date());
		String fileName = "OM_" + dateTime + "_" + order_id.get(index) + ".log";
		String path = Constants.ORDER_MANAGEMENT_LOGS_FILEPATH;

		stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
		stringBuffer.append(System.getProperty("line.separator"));
		stringBuffer.append(Helper.getOMlogTimeStamp() + "***Sim Swap ");
		stringBuffer.append(System.getProperty("line.separator"));
		stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
		stringBuffer.append(System.getProperty("line.separator"));

		try {
			// update the main table status
			OrderProcessing.updateOrderStatus(order_id.get(index), "P", conn, stringBuffer);
			// get the order attributes so that further processing can be done

			JSONObject jsonObject = getOrderAttributes(order_id.get(index), conn, stringBuffer);
			loggerOrderV2.info("Order Attributes: " + jsonObject);
			logFile(stringBuffer, " OrderAttributes " + jsonObject);

			// request packet to process

			JSONArray jArray = jsonObject.getJSONArray("users");
			jsonObject.remove("users");
			jsonObject.remove("type");
			jsonObject.remove("orderKey");

			for (int i = 0; i < jArray.length(); i++) {
				try {

					jsonObject.put("msisdn", jArray.getJSONObject(i).get("msisdn").toString());

					token = Helper.retrieveToken(TrnsactionName, jsonObject.getString("msisdn"));

					loggerOrderV2.info(token + "Json User Array" + jArray.toString());

					loggerOrderV2.info(token + "GSMnumber is: " + jsonObject.getString("msisdn"));

					loggerOrderV2.info(token + "Order Attributes: " + jsonObject);
					
					logs.setIp(jsonObject.getString("iP"));
					logs.setChannel(jsonObject.getString("channel"));
					logs.setMsisdn(jsonObject.getString("msisdn"));
					logs.setLang(jsonObject.getString("lang"));

					String gsmnumber = jsonObject.getString("msisdn");
					String iccid = jsonObject.getString("iccid");
					String transactionId = jsonObject.getString("transactionId");
					String customerId = jsonObject.getString("customerId");
					String lang = jsonObject.getString("lang");
					String contactNumber = jsonObject.getString("contactNumber");
					String specialGroup = jsonObject.getString("specialGroup");
					String imsi = "";

					stringBuffer.append(
							Helper.getOMlogTimeStamp() + " --------------- MSISDN : " + jsonObject.getString("msisdn"));

					com.huawei.crm.service.ens.SubmitOrderResponse submitOrderResponse = new SimSwapService()
							.ngbssForSimSWAP(jsonObject.getString("msisdn"), gsmnumber, iccid, customerId, token,
									specialGroup);

					loggerOrderV2
							.info(token + ":" + " SubmitOrder Response is " + Helper.ObjectToJson(submitOrderResponse));

					if (submitOrderResponse.getResponseHeader().getRetCode().equals("0")) {

						
						
						AdjustmentResultMsg responseAdjustment = new AdjustmentResultMsg() ;
						if(specialGroup.equalsIgnoreCase("1"))
						{
							loggerOrderV2.info(token + ":" + "Calling Adjustment Api");
							 responseAdjustment = AzerfonThirdPartyCalls.simSwapadjustmentResponse(token,
									transactionId, gsmnumber);
						}
						
						else
						{
							ResultHeader value = new ResultHeader();
							value.setResultCode("0");
							responseAdjustment.setResultHeader(value);
						}
						

						if (responseAdjustment.getResultHeader().getResultCode().equals("0")) {
							loggerOrderV2.info(token + ":" + "Waiting for The New Imsi");

							Thread.sleep(28000);

							loggerOrderV2.info(token + ":" + "Calling  GetSubscriber ");
							GetSubscriberResponse respns = new com.evampsaanga.b2b.azerfon.getsubscriber.CRMSubscriberService()
									.GetSubscriberRequest(gsmnumber);

							loggerOrderV2
									.info(token + ":" + " GetSubscriber Response is " + Helper.ObjectToJson(respns));

							if (respns.getResponseHeader().getRetCode().equalsIgnoreCase("0")) {

								imsi = respns.getGetSubscriberBody().getIMSI();
								loggerOrderV2.info(token + ":" + "New IMSI NUMBER IS " + imsi);

								loggerOrderV2.info(token + ":" + "Calling AddApplication after submit order success");

								SimSwapRequest cclient = new SimSwapRequest();
								cclient.setGsmNumber(gsmnumber);
								cclient.setIccid(iccid);
								cclient.setImsi(imsi);
								cclient.setContact(contactNumber);
								cclient.setTransactionId(transactionId);

								AddApplicationResponse addApplicationResponse = new SimSwapService()
										.addApplication(cclient);
								loggerOrderV2.info(token + ":" + Helper.ObjectToJson(addApplicationResponse));

								if (addApplicationResponse.getAddApplicationResult().getStatus().getCode() == 200) {
									OrderProcessing.updateOrderDetails(order_id.get(index),
											jsonObject.getString("msisdn"), "C",
											addApplicationResponse.getAddApplicationResult().getStatus().getCode() + "",
											"actionhistory.generic.success.", conn, stringBuffer);
									logs.setResponseCode(respns.getResponseHeader().getRetCode());
									logs.setResponseDescription(respns.getResponseHeader().getRetMsg());
									logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
									logs.updateLog(logs);
								}

								else {
									OrderProcessing.updateOrderDetails(order_id.get(index),
											jsonObject.getString("msisdn"), "F",
											addApplicationResponse.getAddApplicationResult().getStatus().getCode() + "",
											"actionhistory.generic.failure.", conn, stringBuffer);
									logs.setResponseCode(respns.getResponseHeader().getRetCode());
									logs.setResponseDescription(respns.getResponseHeader().getRetMsg());
									logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
									logs.updateLog(logs);
								}
							}

							else {

								loggerOrderV2.info(token + ":" + "GetSubscriberResponse is not success");

								OrderProcessing.updateOrderDetails(order_id.get(index), jsonObject.getString("msisdn"),
										"F", respns.getResponseHeader().getRetCode(),
										Helper.getNgbssMessageFromResponseCode(respns.getResponseHeader().getRetCode(),
												respns.getResponseHeader().getRetMsg(), jsonObject.getString("lang")),
										conn, stringBuffer);
								logs.setResponseCode(respns.getResponseHeader().getRetCode());
								logs.setResponseDescription(respns.getResponseHeader().getRetMsg());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);

							}
						}

						else {
							loggerOrderV2.info(
									token + ":" + "Returning response because Adjustment Api response was not success");
							OrderProcessing.updateOrderDetails(order_id.get(index), jsonObject.getString("msisdn"), "F",
									responseAdjustment.getResultHeader().getResultCode(),
									Helper.getNgbssMessageFromResponseCode(
											responseAdjustment.getResultHeader().getResultCode(),
											responseAdjustment.getResultHeader().getResultDesc(),
											jsonObject.getString("lang")),
									conn, stringBuffer);
							logs.setResponseCode(submitOrderResponse.getResponseHeader().getRetCode());
							logs.setResponseDescription(submitOrderResponse.getResponseHeader().getRetMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);

						}

					} else {
						loggerOrderV2.info(
								token + ":" + "Returning response because submit order api response was not success");
						OrderProcessing.updateOrderDetails(order_id.get(index), jsonObject.getString("msisdn"), "F",
								submitOrderResponse.getResponseHeader().getRetCode(),
								Helper.getNgbssMessageFromResponseCode(
										submitOrderResponse.getResponseHeader().getRetCode(),
										submitOrderResponse.getResponseHeader().getRetMsg(),
										jsonObject.getString("lang")),
								conn, stringBuffer);
						logs.setResponseCode(submitOrderResponse.getResponseHeader().getRetCode());
						logs.setResponseDescription(submitOrderResponse.getResponseHeader().getRetMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
					}

				} catch (Exception e) {
					OrderProcessing.updateOrderDetails(order_id.get(index), jsonObject.getString("msisdn"), "F",
							ResponseCodes.INTERNAL_SERVER_ERROR_CODE, "actionhistory.generic.failure.", conn,
							stringBuffer);
					loggerOrderV2.info(Helper.GetException(e));
				}
			}
			// end of logic
			OrderProcessing.updateOrderStatus(order_id.get(index), "C", conn, stringBuffer);
			// close files
			java.io.File file = new java.io.File(path + fileName);
			FileWriter fileWriter = new FileWriter(file);
			BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

			// write contents of StringBuffer to a file
			bufferedWriter.write(stringBuffer.toString());

			// flush the stream
			bufferedWriter.flush();

			// close the stream
			bufferedWriter.close();
			conn.close();

		} catch (Exception e) {
			//OrderProcessing.updateOrderStatus(order_id.get(index), "F", conn, stringBuffer);
			loggerOrderV2.error("Containing Previuos Status because control comes in catch block of order");
			loggerOrderV2.info(Helper.GetException(e));
			loggerOrderV2.error((Helper.GetException(e)));
		}
	}

	private JSONObject getOrderAttributes(String order_id, Connection conn, StringBuffer stringBuffer) {

		stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
		stringBuffer.append(System.getProperty("line.separator"));
		stringBuffer.append(Helper.getOMlogTimeStamp() + "*** Select Data from Order Attributes  ");
		stringBuffer.append(System.getProperty("line.separator"));
		stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
		stringBuffer.append(System.getProperty("line.separator"));
		JSONObject jsonObject = new JSONObject();

		try {
			String query = "select key_value, value from order_attributes where order_id_fk =?";
			PreparedStatement preparedStmt = conn.prepareStatement(query);
			preparedStmt.setString(1, order_id);
			stringBuffer.append(Helper.getOMlogTimeStamp() + " Executing Query :" + preparedStmt.toString());
			ResultSet resultSet = preparedStmt.executeQuery();

			while (resultSet.next()) {
				jsonObject.put(resultSet.getString(1), (resultSet.getString(2)));
				stringBuffer
						.append(Helper.getOMlogTimeStamp() + resultSet.getString(1) + " : " + resultSet.getString(2));
			}

			logFile(stringBuffer, "*** Select Data from Order Details For SIM Swap  ");

			query = "select msisdn,group_type,tariff_ids,corpCrmCustName from order_details where order_id_fk =?";
			preparedStmt = conn.prepareStatement(query);
			preparedStmt.setString(1, order_id);
			stringBuffer.append(Helper.getOMlogTimeStamp() + "Executing Query : " + preparedStmt.toString());
			stringBuffer.append(System.getProperty("line.separator"));
			resultSet = preparedStmt.executeQuery();
			JSONArray jArray = new JSONArray();
			while (resultSet.next()) {
				JSONObject jobj = new JSONObject();
				jobj.put("msisdn", resultSet.getString(1));
				stringBuffer.append(Helper.getOMlogTimeStamp() + " MSISDN: " + resultSet.getString(1));
				stringBuffer.append(System.getProperty("line.separator"));
				jobj.put("groupType", resultSet.getString(2));
				stringBuffer.append(Helper.getOMlogTimeStamp() + " GROUP_TYPE: " + resultSet.getString(2));
				stringBuffer.append(System.getProperty("line.separator"));
				jobj.put("tariff_ids", resultSet.getString(3));
				stringBuffer.append(Helper.getOMlogTimeStamp() + " TARIFF_IDS: " + resultSet.getString(3));
				stringBuffer.append(System.getProperty("line.separator"));
				jobj.put("corpCrmCustName", resultSet.getString(4));
				stringBuffer.append(Helper.getOMlogTimeStamp() + " corpCrmCustName: " + resultSet.getString(4));
				stringBuffer.append(System.getProperty("line.separator"));
				jArray.put(jobj);
			}
			jsonObject.put("users", jArray);
			resultSet.close();
			preparedStmt.close();

		} catch (Exception e) {
			loggerOrderV2.debug(Helper.GetException(e));
		}
		return jsonObject;
	}

	StringBuffer logFile(StringBuffer stringBuffer, String msg) {
		stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
		stringBuffer.append(System.getProperty("line.separator"));
		stringBuffer.append(Helper.getOMlogTimeStamp() + msg);
		stringBuffer.append(System.getProperty("line.separator"));
		stringBuffer.append(Helper.getOMlogTimeStamp() + "************************************************");
		stringBuffer.append(System.getProperty("line.separator"));

		return stringBuffer;
	}
}
