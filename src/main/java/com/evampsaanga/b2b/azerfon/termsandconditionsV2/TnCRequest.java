package com.evampsaanga.b2b.azerfon.termsandconditionsV2;

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;

public class TnCRequest extends BaseRequest{

	private String userName;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
}
