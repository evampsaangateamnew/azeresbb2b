package com.evampsaanga.b2b.azerfon.forgotpassword;

import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;

public class ForgotPasswordResponse extends BaseResponse {
	private String channel;
   private String entityId;
   
	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

}
