package com.evampsaanga.b2b.azerfon.coreservices;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.amqimplementationsesb.LogsType;
import com.evampsaanga.b2b.azerfon.db.DBFactory;
import com.evampsaanga.b2b.azerfon.getcoreservices.CoreServices;
import com.evampsaanga.b2b.azerfon.getcoreservices.Data;
import com.evampsaanga.b2b.azerfon.getcoreservices.GetCoreServicesRequest;
import com.evampsaanga.b2b.azerfon.getcoreservices.GetCoreServicesResponse;
import com.evampsaanga.b2b.azerfon.getsubscriber.CoreServicesCategory;
import com.evampsaanga.b2b.azerfon.getsubscriber.CoreServicesCategoryItem;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.ThirdPartyNames;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Decrypter;
import com.evampsaanga.b2b.developer.utils.Helper;

@Path("/azerfon")
public class CoreServicesLandV2 {
	public static final Logger logger = Logger.getLogger("azerfon-esb");

	@POST
	@Path("/getv2")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetCoreServicesResponse Getv2(@Header("credentials") String credential, @Body() String requestBody) {
		GetCoreServicesResponse resp = new GetCoreServicesResponse();
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.GET_CORE_SERVICES_TRANSACTION_NAME_B2B);
		logs.setThirdPartyName(ThirdPartyNames.GET_CORE_SERVICES);
		logs.setTableType(LogsType.GetCoreServices);
		String token = "";
		String TrnsactionName = Transactions.GET_CORE_SERVICES_TRANSACTION_NAME_B2B;

		try {

			token = Helper.retrieveToken(TrnsactionName, Helper.getValueFromJSON(requestBody, "msisdn"));

			logger.info(token + "Request Landed on GetCoreServicesRequestLand " + requestBody);

			String credentials = null;
			GetCoreServicesRequest cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, GetCoreServicesRequest.class);
			} catch (Exception ex1) {
				logger.info(token + Helper.GetException(ex1));
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				if (cclient.getIsB2B() != null && (cclient.getIsB2B().equals("true") || cclient.getIsB2B().equals("1"))) {
					logs.setTransactionName(Transactions.GET_CORE_SERVICES_TRANSACTION_NAME_B2B);
				}
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				try {
					
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					logger.info(token + Helper.GetException(ex));
				}
				if (credentials == null) {
					logger.info(token + "credentials Null check.....");
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				String verification = Helper.validateRequest(cclient);
				if (!verification.equals("")) {
					resp.setReturnCode(ResponseCodes.ERROR_400);
					resp.setReturnMsg(verification);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					logger.info(token + "credentials validated credentials matched.....");

					Data data = new Data();
					CoreServices coreServices = new CoreServices();

					List<CoreServicesCategory> listOfCoreServicesCategory = getCoreServicesCategoryList(
							cclient.getLang(), cclient.getIsB2B());

					for (CoreServicesCategory coreServicesCategory : listOfCoreServicesCategory) {
						coreServices = new CoreServices();
						coreServices.setCoreServiceCategory(coreServicesCategory.getName());
						ArrayList<CoreServicesCategoryItem> listOfCoreServicesCategoryItem = getCoreServicesCategoryItemList(
								coreServicesCategory.getId(), cclient.getLang(),cclient.getUserType());
						if (listOfCoreServicesCategoryItem != null && listOfCoreServicesCategoryItem.size() > 0) {
							coreServicesCategory.setListOfCoreServicesCategoryItem(listOfCoreServicesCategoryItem);
							coreServices.setCoreServicesList(listOfCoreServicesCategoryItem);
						}
						
						if(coreServices.getCoreServicesList().size() > 0)
						{
							data.getCoreServices().add(coreServices);
						}
						
					}

					resp.setData(data);
					resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
					resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				} else {
					logger.info(token + "credentials  check ELSe not mathed credeitials.....");
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
				resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		} catch (Exception ex) {
			logger.info(token + Helper.GetException(ex));
			resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
			resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
	}

	public static List<CoreServicesCategory> getCoreServicesCategoryList(String lang_id, String isb2b) {
		logger.info("<<<<<<<<<<<<<<<<<<<<< Generating cache with STID: " + lang_id);
		List<CoreServicesCategory> listOfCoreServicesCategory = new ArrayList<CoreServicesCategory>();
		try {
			Connection con = DBFactory.getMagentoDBConnection();
			String query = "";
			PreparedStatement pstmt = null;
			/*
			 * if (stId.equals("bulk")) { query =
			 * "select * from coreservice_category where store_id=?  order by sort_order " ;
			 * pstmt = con.prepareStatement(query); // create a statement pstmt.setString(1,
			 * stId); } else {
			 */
			query = "select * from coreservice_category where store_id=? AND is_b2b =? order by sort_order";
			pstmt = con.prepareStatement(query); // create a statement
			pstmt.setString(1, lang_id);
			pstmt.setString(2, isb2b);
			// /}

			logger.info("<<<<<<<< Query >>>>>>>>" + pstmt.toString());
			ResultSet rs = pstmt.executeQuery();
			// extract data from the ResultSet
			while (rs.next()) {
				Long id = rs.getLong(1);
				String name = rs.getString(2);
				Integer status = rs.getInt(3);
				String storeId = rs.getString(4);
				Integer sortOrder = rs.getInt(5);
				Integer is_b2b = rs.getInt(6);
				CoreServicesCategory csc = new CoreServicesCategory(id, name, status, storeId, sortOrder, is_b2b);
				logger.info("<<<<<<<<<< CoreServicesCategory >>>>>>>>>>" + csc.toString());
				listOfCoreServicesCategory.add(csc);
			}
			pstmt.close();
			rs.close();
			con.close();
		} catch (Exception e) {
			logger.error("EXCEPTION:", e);
		}
		logger.info("Number of core services" + listOfCoreServicesCategory.size());
		return listOfCoreServicesCategory;
	}

	public static ArrayList<CoreServicesCategoryItem> getCoreServicesCategoryItemList(Long cid, String lang,String userType) {

		ArrayList<CoreServicesCategoryItem> listOfCoreServicesCategoryItem = new ArrayList<CoreServicesCategoryItem>();
		try {
			Connection con = DBFactory.getMagentoDBConnection();
			String query = "";
			logger.info("<<<< lang_Id is: >>>>" + lang);
			PreparedStatement pstmt = null;
			query = "select * from coreservice_item where category_id=? and store_id=? and status=?  order by sort_order ";
			pstmt = con.prepareStatement(query); // create a statement
			pstmt.setLong(1, cid);
			pstmt.setString(2, lang);
			pstmt.setInt(3, 1);

			logger.info("<<<<<<<< Query for categorty item list >>>>>>>>" + pstmt.toString());
			ResultSet rs = pstmt.executeQuery();
			// extract data from the ResultSet
			while (rs.next()) {
				Long id = rs.getLong(1);
				String name = rs.getString(2);
				String description = rs.getString(3);
				Long categoryId = rs.getLong(4);
				String price = rs.getString(5);
				String validity = rs.getString(6);
				String offeringId = rs.getString(7);
				String storeId = rs.getString(9);
				Integer sortOrder = rs.getInt(10);
				Integer renewable = rs.getInt(11);
				String freeFor = rs.getString(12);
				String visibleFor = rs.getString(13);
				
				String [] values = visibleFor.split(",");
				
				logger.info("User Type is"+userType);
				for (int i = 0; i < values.length; i++) {
					
					logger.info("<<<<<<<<< After Spliting Visibe For First Iteration Visible For ==>>>>>>>>>" + values[i]);
					
					
					
					if(values[i].equalsIgnoreCase(userType))
					{
						CoreServicesCategoryItem categoryItem = new CoreServicesCategoryItem(id, name, description, categoryId,
								price, validity, offeringId, storeId, sortOrder, "", renewable, freeFor, visibleFor);
						listOfCoreServicesCategoryItem.add(categoryItem);
						logger.info("<<<<<<<<< CATEGORY ITEM: >>>>>>>>>" + categoryItem.toString());
					}
				}
				
				
			}
			pstmt.close();
			rs.close();
			con.close();

		} catch (Exception e) {
			logger.error(e);
		}

		return listOfCoreServicesCategoryItem;
	}
}
