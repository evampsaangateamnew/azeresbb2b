package com.evampsaanga.b2b.azerfon.coreservices;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.amqimplementationsesb.LogsType;
import com.evampsaanga.b2b.azerfon.getcoreservices.CoreServices;
import com.evampsaanga.b2b.azerfon.getcoreservices.GetCoreServicesRequest;
import com.evampsaanga.b2b.azerfon.getcoreservices.GetCoreServicesResponse;
import com.evampsaanga.b2b.azerfon.getsubscriber.CRMSubscriberService;
import com.evampsaanga.b2b.azerfon.getsubscriber.CoreServicesCategory;
import com.evampsaanga.b2b.azerfon.getsubscriber.CoreServicesCategoryItem;
import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.ThirdPartyNames;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Decrypter;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.huawei.crm.azerfon.bss.basetype.GetSubOfferingInfo;
import com.huawei.crm.azerfon.bss.query.GetSubscriberOut;
import com.huawei.crm.azerfon.bss.query.GetSubscriberResponse;

@Path("/azerfon")
public class CoreServicesIndividualLandV2 {

	public static final Logger logger = Logger.getLogger("azerfon-esb");

	@POST
	@Path("/getindividualv2")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetCoreServicesResponse Get(@Header("credentials") String credential, @Body() String requestBody) {
		GetCoreServicesResponse resp = new GetCoreServicesResponse();
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.GET_CORE_SERVICES_TRANSACTION_NAME_B2B);
		logs.setThirdPartyName(ThirdPartyNames.GET_CORE_SERVICES);
		logs.setTableType(LogsType.GetCoreServices);
		String token = "";
		String TrnsactionName = Transactions.GET_CORE_SERVICES_TRANSACTION_NAME_B2B;

		try {

			token = Helper.retrieveToken(TrnsactionName, Helper.getValueFromJSON(requestBody, "msisdn"));

			logger.info(token + "Request Landed on GetIndividualCoreServicesV2RequestLand " + requestBody);

			String credentials = credential;
			GetCoreServicesRequest cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, GetCoreServicesRequest.class);
			} catch (Exception ex1) {
				logger.info(token + Helper.GetException(ex1));
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				if (cclient.getIsB2B() != null && cclient.getIsB2B().equals("true")) {
					logs.setTransactionName(Transactions.GET_CORE_SERVICES_TRANSACTION_NAME_B2B);
				}
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				try {
					if (credential != null && !credential.equals(Constants.CREDENTIALS))
						credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					logger.info(token + Helper.GetException(ex));
				}
				if (credentials == null) {
					logger.info(token + "credentials Null check.....");
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				String verification = Helper.validateRequest(cclient);
				if (!verification.equals("")) {
					resp.setReturnCode(ResponseCodes.ERROR_400);
					resp.setReturnMsg(verification);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					logger.info(token + "credentials validated credentials matched.....");
					CRMSubscriberService crmSub = new CRMSubscriberService();
					GetSubscriberResponse subsResponse = crmSub.GetSubscriberRequest(cclient.getSelectedMsisdn());

					// As We need Status from GetNetWorkSettingsAPI so we will call
					// GetNetWork Settings API
//					GetNetworkSettingsRequestClient networkclient = new GetNetworkSettingsRequestClient();
//					networkclient.setMsisdn(cclient.getmsisdn());
//					GetNetworkSettingDataResponse responseFromNgbss = com.evampsaanga.azerfon.getnetworksettings.GetNetworkSettingsLand
//							.RequestSoap(com.evampsaanga.azerfon.getnetworksettings.GetNetworkSettingsLand
//									.getRequestHeader(), networkclient);
//					List<GetSubProductInfo> getsubproductinfo = new ArrayList<>();
//					if (responseFromNgbss != null && responseFromNgbss.getGetNetworkSettingDataBody() != null
//							&& responseFromNgbss.getGetNetworkSettingDataBody().getGetNetworkSettingDataList() != null
//							&& responseFromNgbss.getGetNetworkSettingDataBody().getGetNetworkSettingDataList()
//									.size() > 0) {
//						getsubproductinfo = responseFromNgbss.getGetNetworkSettingDataBody()
//								.getGetNetworkSettingDataList();
//					}
					if (Constants.CRMSUBACCESSCODE.equals(subsResponse.getResponseHeader().getRetCode())) {

						logger.info(
								token + "SubscriberRespnsecode....." + subsResponse.getResponseHeader().getRetCode());

						resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
						resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
//						String freeFor = getFreeForValue(cclient.getUserType(), cclient.getBrand(),
//								cclient.getAccountType(), cclient.getGroupType(), token);
//						String visibleFor = getVisibleForValue(cclient.getUserType(), cclient.getBrand(),
//								cclient.getAccountType(), cclient.getGroupType(), token);
//						logger.info( token+"Free FOR: " +freeFor);
//						logger.info( token+"Visisble FOR: " +visibleFor);
						// just commented this line because the GetNetwork setting ere not there in new
						// Get Subscriber Offers
						resp.getData()
								.setCoreServices(this.getIndividualCoreServicesV2(subsResponse.getGetSubscriberBody(),
										cclient.getLang(), cclient.getIsB2B(), cclient.getUserType(), token));
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					} else {
						resp.setReturnCode(subsResponse.getResponseHeader().getRetCode());
						resp.setReturnMsg(subsResponse.getResponseHeader().getRetMsg());
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					}
					logs.updateLog(logs);

					return resp;
				} else {
					logger.info(token + "credentials  check ELSe not mathed credeitials.....");
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else
				resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
			resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		} catch (Exception ex) {
			logger.info(token + Helper.GetException(ex));
			resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
			resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
	}

	public ArrayList<CoreServices> getIndividualCoreServicesV2(GetSubscriberOut data, String lang, String isFrom,
			String userType, String token) {

		logger.info(token + "*******START of getIndividualCoreServicesV2 Method in CRMSubscriberService Class********");
		ArrayList<CoreServices> coreServicesList = new ArrayList<CoreServices>();
		CoreServices coreServices; 
		
		List<CoreServicesCategory> listCoreServicesCategory = CoreServicesLandV2.getCoreServicesCategoryList(lang,
				isFrom);
		if (listCoreServicesCategory != null && listCoreServicesCategory.size() > 0) {
			for (CoreServicesCategory coreServicesCategory : listCoreServicesCategory) {
				coreServices = new CoreServices();
				coreServices.setCoreServiceCategory(coreServicesCategory.getName());
				
				ArrayList<CoreServicesCategoryItem> listOfCoreServicesCategoryItems = CoreServicesLandV2
						.getCoreServicesCategoryItemList(coreServicesCategory.getId(), lang,userType);
				
				logger.info(token + "<<<<<<<<<<<< listOfCoreServicesCategoryItems Size >>>>>>>>>>>>"
						+ listOfCoreServicesCategoryItems.size());
				
				if (listOfCoreServicesCategoryItems != null && listOfCoreServicesCategoryItems.size() > 0) {
					
					for (int i = 0; i < listOfCoreServicesCategoryItems.size(); i++) {
						logger.info(token + "<<<<<<< getSupplementaryOfferingList size:"
								+ data.getSupplementaryOfferingList().getGetSubOfferingInfo().size());
						boolean offerDound = false;
						for (GetSubOfferingInfo oneOffer : data.getSupplementaryOfferingList()
								.getGetSubOfferingInfo()) {

							// CoreServicesCategoryItem coreServicesCategoryItem
							// = listOfCoreServicesCategoryItems.get(i);
							if (listOfCoreServicesCategoryItems.get(i).getOfferingId()
									.equals(oneOffer.getOfferingId().getOfferingId())) {
								offerDound = true;
								listOfCoreServicesCategoryItems.get(i)
										.setStatus(ConfigurationManager.getConfigurationFromCache(
												ConfigurationManager.MAPPING_CRM_STATUS + oneOffer.getStatus()));

								logger.info(token + "Status: " + listOfCoreServicesCategoryItems.get(i).getStatus());
								
							}
						}
						if (!offerDound) {

							listOfCoreServicesCategoryItems.get(i).setStatus(ConfigurationManager
									.getConfigurationFromCache(ConfigurationManager.MAPPING_CRM_STATUS + "C02"));
							listOfCoreServicesCategoryItems.get(i).setForwardNumber("");
						}
					}
					coreServices.setCoreServicesList(listOfCoreServicesCategoryItems);
					
				}
				coreServicesList.add(coreServices);
			}
		}
		logger.info(token + "*******END of getCoreServices Method in CRMSubscriberService Class********");
		return coreServicesList;
	}

}
