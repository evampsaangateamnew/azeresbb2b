
package com.evampsaanga.b2b.azerfon.roamingcountriesdata;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "countryList",
    "detailList"
})
public class Data {

    @JsonProperty("countryList")
    private List<CountryList> countryList = null;
    @JsonProperty("detailList")
    private List<DetailList> detailList = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("countryList")
    public List<CountryList> getCountryList() {
        return countryList;
    }

    @JsonProperty("countryList")
    public void setCountryList(List<CountryList> countryList) {
        this.countryList = countryList;
    }

    @JsonProperty("detailList")
    public List<DetailList> getDetailList() {
        return detailList;
    }

    @JsonProperty("detailList")
    public void setDetailList(List<DetailList> detailList) {
        this.detailList = detailList;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
