
package com.evampsaanga.b2b.azerfon.roamingcountriesdata;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "service",
    "service_unit",
    "service_type_list"
})
public class ServiceList {

    @JsonProperty("service")
    private String service;
    @JsonProperty("service_unit")
    private String serviceUnit;
    @JsonProperty("service_type_list")
    private List<ServiceTypeList> serviceTypeList = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("service")
    public String getService() {
        return service;
    }

    @JsonProperty("service")
    public void setService(String service) {
        this.service = service;
    }

    @JsonProperty("service_unit")
    public String getServiceUnit() {
        return serviceUnit;
    }

    @JsonProperty("service_unit")
    public void setServiceUnit(String serviceUnit) {
        this.serviceUnit = serviceUnit;
    }

    @JsonProperty("service_type_list")
    public List<ServiceTypeList> getServiceTypeList() {
        return serviceTypeList;
    }

    @JsonProperty("service_type_list")
    public void setServiceTypeList(List<ServiceTypeList> serviceTypeList) {
        this.serviceTypeList = serviceTypeList;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
