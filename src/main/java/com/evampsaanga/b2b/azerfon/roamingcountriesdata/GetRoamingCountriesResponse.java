package com.evampsaanga.b2b.azerfon.roamingcountriesdata;

import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;

public class GetRoamingCountriesResponse extends BaseResponse {
	private com.evampsaanga.b2b.azerfon.roamingcountriesdata.Data data=new com.evampsaanga.b2b.azerfon.roamingcountriesdata.Data();

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

}
