package com.evampsaanga.b2b.azerfon.roamingcountriesdata;

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;

public class GetRoamingCountriesRequest extends BaseRequest {
	
	private String brandName="";

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

}
