/**
 * 
 */
package com.evampsaanga.b2b.azerfon.pdf;

import java.io.IOException;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.ws.rs.Path;

import org.apache.log4j.Logger;

import com.evampsaanga.b2b.azerfon.sendemail.SendEmailResponse;
import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.developer.utils.Helper;

/**
 * @author HamzaFarooque
 *
 */

@Path("/azerfon")
public class PdfToEmailLand {

	public SendEmailResponse getResponse(String subject, String to, String attachmentPath, String filename,
			Logger logger, String token) {
		SendEmailResponse resp = new SendEmailResponse();
		String from = "";
		logger.info(token + "NOW LANDED IN getResponseMethod");
		// start of sendEmail

		/*
		 * final String username = "Business.Services@bakcell.com"; final String
		 * password = "B2525akcell++";
		 */

		final String username = ConfigurationManager.getConfigurationFromCache("email.smtp.username.b2b");
		final String password = ConfigurationManager.getConfigurationFromCache("email.smtp.password.b2b");

		logger.info(token + "USERNAME VALUE CONFIG " + username);
		logger.info(token + "PASSWORD VALUE CONFIG " + password);
		// JSONObject jsonObject = new JSONObject();
		// try {
		// jsonObject.put("lang", cclient.getLang());
		// } catch (JSONException e1) {
		//
		// logger.info(token+Helper.GetException(e1));
		// }
		try {
			from = ConfigurationManager.getConfigurationFromCache("email.from.b2b");
			logger.info(token + "FROM VALUE CONFIG " + from);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			logger.info(token + Helper.GetException(e1));
		}

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.host", ConfigurationManager.getConfigurationFromCache("email.mail.smtp.host"));
		props.put("mail.smtp.port", "25");
		logger.info(
				token + "HOST VALUE CONFIG " + ConfigurationManager.getConfigurationFromCache("email.mail.smtp.host"));
		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
			message.setSubject(subject);

			// message.setText(text);

			// Create a multipar message
			Multipart multipart = new MimeMultipart();

			// Part two is attachment
			BodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart = new MimeBodyPart();
			DataSource source = new FileDataSource(attachmentPath);
			messageBodyPart.setDataHandler(new DataHandler(source));
			messageBodyPart.setFileName(filename);
			multipart.addBodyPart(messageBodyPart);

			// Send the complete message parts
			message.setContent(multipart);

			logger.info(token + " Ready to send Email.");
			Transport.send(message);

			logger.info(token + "RESULT IN TRY EMAIL SEND SUCCESSFULLY");

		} catch (MessagingException e) {
			logger.info(token + Helper.GetException(e));
			resp.setReturnCode(ResponseCodes.GENERIC_ERROR_CODE);
			resp.setReturnMsg(ResponseCodes.GENERIC_ERROR_DES);
			return resp;
		}

		// end of sendEmail

		resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
		resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
		try {
			logger.info(token + "Response FROM ESB " + Helper.ObjectToJson(resp));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.info(token + Helper.GetException(e));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.info(token + Helper.GetException(e));
		}
		return resp;
	}

}
