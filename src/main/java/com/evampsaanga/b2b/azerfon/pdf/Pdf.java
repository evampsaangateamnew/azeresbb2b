package com.evampsaanga.b2b.azerfon.pdf;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;

import org.apache.log4j.Logger;

import com.evampsaanga.b2b.azerfon.companyinvoice.CompanyInvoiceData;
import com.evampsaanga.b2b.azerfon.companyinvoice.CompanyInvoiceResponse;
import com.evampsaanga.b2b.azerfon.companyinvoice.InvoiceModel;
import com.evampsaanga.b2b.azerfon.getcdrsbydate.CDRDetails;
import com.evampsaanga.b2b.azerfon.getcdrsbydate.GetCDRsByDateRequestResponse;
import com.evampsaanga.b2b.azerfon.msisdninvoice.DetailsData;
import com.evampsaanga.b2b.azerfon.msisdninvoice.MSISDNDetail;
import com.evampsaanga.b2b.azerfon.msisdninvoice.MSISDNDetailsResponse;
import com.evampsaanga.b2b.azerfon.msisdninvoice.MSISDNSummary;
import com.evampsaanga.b2b.azerfon.msisdninvoice.MSISDNSummaryResponse;
import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * @author HamzaFarooque
 *
 */
public class Pdf {

	public static final Logger loggerV2 = Logger.getLogger("azerfon-esb");

	public String generateCompanyInvoicePdf(String msisdn, CompanyInvoiceResponse companyInvoiceResponse, String lang) {

		// Path where PDF is Created

		List<CompanyInvoiceData> invoiceData = companyInvoiceResponse.getData().getInvoiceData();
		List<InvoiceModel> results = null;

		String path = ConfigurationManager.getConfigurationFromCache("invoice.details.pdfLocalPath");
		String ext = Constants.PDF_EXT;
		// String datetime = Helper.getPdfTimeStamp();
		String filename = msisdn + ext;
		String pdfPath = path + filename;
		loggerV2.info("  PdfPath is :" + pdfPath);

		File file = new File(pdfPath);
		while (file.exists()) {
			loggerV2.info("-------File exist at this path : " + pdfPath);
			file.delete();
		}
		Document document = new Document(PageSize.A4);
		if (invoiceData != null) {
			try {
				// Get writer and Open Document
				PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(pdfPath));
				document.open();

				// Add Header
				String headerPath = ConfigurationManager.getConfigurationFromCache("invoice.details.pdfHeaderPath");
				Image imgheader = Image.getInstance(headerPath);
				imgheader.scaleToFit(document.getPageSize().getWidth() - 50, imgheader.getHeight());
				imgheader.setAlignment(Element.ALIGN_CENTER);
				document.add(imgheader);
				loggerV2.info("  Logo path is : " + headerPath);

				// Register Custom Font
				FontFactory.register(ConfigurationManager.getConfigurationFromCache("invoice.details.pdfFontPath"),
						"alssans");
				// Font for heading
				Font headingFont = FontFactory.getFont("alssans", BaseFont.IDENTITY_H, true, 16, Font.BOLD);
				// Font for normal
				Font tableFont = FontFactory.getFont("alssans", BaseFont.IDENTITY_H, true, 12);
				// Font for bold
				Font tableFontBold = FontFactory.getFont("alssans", BaseFont.IDENTITY_H, true, 12, Font.BOLD);

				// Create heading
				document.add(new Paragraph(Chunk.NEWLINE));
				Paragraph paragraph = new Paragraph(
						ConfigurationManager.getConfigurationFromCache("invoice.details.company.invoice." + lang),
						headingFont);
				paragraph.setAlignment(Element.ALIGN_CENTER);
				paragraph.setSpacingAfter(20);
				document.add(paragraph);
				// document.add(Chunk.createWhitespace(""));

				// Create Table
				PdfPTable table = new PdfPTable(2);
				table.setWidthPercentage(95);
				float[] columnWidths = { 2f, 1f };
				table.setWidths(columnWidths);

				loggerV2.info(" Filling data in table ------------- pdf Generator  ");

				// Filling data in table
				PdfPCell cell;

				for (int i = 0; i < invoiceData.size(); i++) {

					cell = new PdfPCell(new Paragraph(invoiceData.get(i).getLabel(), tableFontBold));
					cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					cell.setFixedHeight(22f);
					cell.setBorderWidth(0.5f);
					table.addCell(cell);

					cell = new PdfPCell(new Paragraph((invoiceData.get(i).getAmount() + " AZN"), tableFontBold));
					cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					cell.setFixedHeight(22f);
					cell.setBorderWidth(0.5f);
					table.addCell(cell);

					results = invoiceData.get(i).getResults();

					if (results != null) {
						for (int j = 0; j < results.size(); j++) {
							cell = new PdfPCell(new Paragraph(results.get(j).getLabel(), tableFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setFixedHeight(22f);
							cell.setBorderWidth(0.5f);
							table.addCell(cell);

							cell = new PdfPCell(new Paragraph((results.get(j).getAmount() + " AZN"), tableFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setFixedHeight(22f);
							cell.setBorderWidth(0.5f);
							table.addCell(cell);
						}
					}
				}
				document.add(table);

				// Close Document and Writer
				document.close();
				writer.close();
				String finalPath = ConfigurationManager.getConfigurationFromCache("invoice.details.pdfExternalPath");
				// comented for testing
				// uncomment after testing
				loggerV2.info("pdf path:" + finalPath);
				finalPath = finalPath + msisdn + ext;
				// finalPath =
				// "http://10.220.48.216:8080/azerfon/static/pic_new22019-11-05-12.28.23.pdf";
				// hard coded for testing
				return finalPath;

			} catch (FileNotFoundException e1) {
				loggerV2.info(Helper.GetException(e1));
				e1.printStackTrace();
			} catch (DocumentException e1) {
				loggerV2.info(Helper.GetException(e1));
				e1.printStackTrace();
			} catch (MalformedURLException e) {
				loggerV2.info(Helper.GetException(e));
				e.printStackTrace();
			} catch (IOException e) {
				loggerV2.info(Helper.GetException(e));
				e.printStackTrace();
			}
		}
		return "";
	}

	public String generateMsisdndetailPdf(String msisdn, String selectedMsisdn,
			MSISDNDetailsResponse msisdnDetailsResponse, String lang) {

		// Path where PDF is Created

		MSISDNDetail data = msisdnDetailsResponse.getData();
		List<DetailsData> results = data.getResults();

		String path = ConfigurationManager.getConfigurationFromCache("invoice.details.pdfLocalPath");
		String ext = Constants.PDF_EXT;
		String pdfPath = path + msisdn + "_" + selectedMsisdn + "_detail" + ext;
		loggerV2.info("  PdfPath is :" + pdfPath);

		File file = new File(pdfPath);
		while (file.exists()) {
			loggerV2.info("-------File exist at this path : " + pdfPath);
			file.delete();
		}

		Document document = new Document(PageSize.A4);
		if (data != null) {
			try {
				// Get writer and Open Document
				PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(pdfPath));
				document.open();

				// Add Header
				String headerPath = ConfigurationManager.getConfigurationFromCache("invoice.details.pdfHeaderPath");
				Image imgheader = Image.getInstance(headerPath);
				imgheader.scaleToFit(document.getPageSize().getWidth() - 50, imgheader.getHeight());
				imgheader.setAlignment(Element.ALIGN_CENTER);
				document.add(imgheader);
				loggerV2.info("  Logo path is :" + headerPath);

				// Register Custom Font
				FontFactory.register(ConfigurationManager.getConfigurationFromCache("invoice.details.pdfFontPath"),
						"alssans");
				// Font for heading
				Font headingFont = FontFactory.getFont("alssans", BaseFont.IDENTITY_H, true, 16, Font.BOLD);
				// Font for normal
				Font tableFont = FontFactory.getFont("alssans", BaseFont.IDENTITY_H, true, 12);
				// Font for bold
				Font tableFontBold = FontFactory.getFont("alssans", BaseFont.IDENTITY_H, true, 12, Font.BOLD);

				// Create heading
				document.add(new Paragraph(Chunk.NEWLINE));
				Paragraph paragraph = new Paragraph(ConfigurationManager
						.getConfigurationFromCache("invoice.details.msisdn.invoice.detailed." + lang), headingFont);
				paragraph.setAlignment(Element.ALIGN_CENTER);
				document.add(paragraph);
				document.add(Chunk.NEWLINE);

				// Create Table
				PdfPTable table = new PdfPTable(2);
				table.setWidthPercentage(90);
				float[] columnWidths = { 2f, 1f };
				table.setWidths(columnWidths);

				loggerV2.info(" Filling data in table ------------- pdf Generator ");

				// Filling data in table
				PdfPCell cell;

				cell = new PdfPCell(new Paragraph(data.getMsisdn().getLabel(), tableFontBold));
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setFixedHeight(15f);
				cell.setBorderWidth(0.5f);
				table.addCell(cell);

				cell = new PdfPCell(new Paragraph((data.getMsisdn().getAmount()), tableFontBold));
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setFixedHeight(22f);
				cell.setBorderWidth(0.5f);
				table.addCell(cell);

				cell = new PdfPCell(new Paragraph(data.getTariff().getLabel(), tableFontBold));
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setFixedHeight(22f);
				cell.setBorderWidth(0.5f);
				table.addCell(cell);

				cell = new PdfPCell(new Paragraph((data.getTariff().getAmount()), tableFontBold));
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setFixedHeight(22f);
				cell.setBorderWidth(0.5f);
				table.addCell(cell);

				if (results != null) {
					for (int j = 0; j < results.size(); j++) {
						cell = new PdfPCell(new Paragraph(results.get(j).getLabel(), tableFont));
						cell.setHorizontalAlignment(Element.ALIGN_CENTER);
						cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
						cell.setFixedHeight(22f);
						cell.setBorderWidth(0.5f);
						table.addCell(cell);

						cell = new PdfPCell(new Paragraph((results.get(j).getAmount() + " AZN"), tableFont));
						cell.setHorizontalAlignment(Element.ALIGN_CENTER);
						cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
						cell.setFixedHeight(22f);
						cell.setBorderWidth(0.5f);
						table.addCell(cell);
					}
				}
				cell = new PdfPCell(new Paragraph(data.getTotal().getLabel(), tableFontBold));
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setFixedHeight(22f);
				cell.setBorderWidth(0.5f);
				table.addCell(cell);

				cell = new PdfPCell(new Paragraph((data.getTotal().getAmount() + " AZN"), tableFontBold));
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setFixedHeight(22f);
				cell.setBorderWidth(0.5f);
				table.addCell(cell);

				document.add(table);

				// Close Document and Writer
				document.close();
				String finalPath = ConfigurationManager.getConfigurationFromCache("invoice.details.pdfExternalPath");
				// comented for testing
				// uncomment after testing
				// loggerV2.info("pdf path:" + finalPath);
				finalPath = finalPath + msisdn + "_" + selectedMsisdn + "_detail" + ext;
				// finalPath = finalPath + "pic_new22019-11-05-12.28.23.pdf";
				// hard coded for testing
				return finalPath;

			} catch (FileNotFoundException e1) {
				loggerV2.info(Helper.GetException(e1));
				e1.printStackTrace();
			} catch (DocumentException e1) {
				loggerV2.info(Helper.GetException(e1));
				e1.printStackTrace();
			} catch (MalformedURLException e) {
				loggerV2.info(Helper.GetException(e));
				e.printStackTrace();
			} catch (IOException e) {
				loggerV2.info(Helper.GetException(e));
				e.printStackTrace();
			}
		}
		return "";
	}

	public String generateUsageHistoryPdf(String msisdn, GetCDRsByDateRequestResponse getCDRsByDateRequestResponse,
			String lang) {

		// Path where PDF is Created

		List<CDRDetails> summaryData = getCDRsByDateRequestResponse.getRecords();

		String path = ConfigurationManager.getConfigurationFromCache("invoice.details.pdfLocalPath");
		String ext = Constants.PDF_EXT;
		String pdfPath = path + msisdn + "_usagehistory" + ext;
		loggerV2.info("  PdfPath is :" + pdfPath);

		File file = new File(pdfPath);
		while (file.exists()) {
			loggerV2.info("-------File exist at this path : " + pdfPath);
			file.delete();
		}

		Document document = new Document(PageSize.A4.rotate());
		if (summaryData != null && !summaryData.isEmpty()) {
			try {
				// Get writer and Open Document
				PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(pdfPath));
				document.open();

				// Add Header
				String headerPath = ConfigurationManager.getConfigurationFromCache("invoice.details.pdfHeaderPath");
				Image imgheader = Image.getInstance(headerPath);
				imgheader.scaleToFit(document.getPageSize().getWidth() - 50, imgheader.getHeight());
				imgheader.setAlignment(Element.ALIGN_CENTER);
				document.add(imgheader);
				loggerV2.info("  Header path is : " + headerPath);

				// Register Custom Font
				FontFactory.register(ConfigurationManager.getConfigurationFromCache("invoice.details.pdfFontPath"),
						"alssans");
				// Font for heading
				Font headingFont = FontFactory.getFont("alssans", BaseFont.IDENTITY_H, true, 16, Font.BOLD);
				// Font for normal
				Font tableFont = FontFactory.getFont("alssans", BaseFont.IDENTITY_H, true, 12);
				// Font for bold
				Font tableFontBold = FontFactory.getFont("alssans", BaseFont.IDENTITY_H, true, 12, Font.BOLD);

				// Create heading
				document.add(new Paragraph(Chunk.NEWLINE));
				Paragraph paragraph = new Paragraph(ConfigurationManager
						.getConfigurationFromCache("invoice.details.usagedetails.label.heading." + lang), headingFont);
				paragraph.setAlignment(Element.ALIGN_CENTER);
				document.add(paragraph);
				document.add(Chunk.NEWLINE);

				// Create Table
				PdfPTable table = new PdfPTable(8);
				table.setWidthPercentage(100);
				float[] columnWidths = { 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f };
				table.setWidths(columnWidths);

				loggerV2.info(" Filling data in Table ------------- pdf Generator ");

				// Filling data in table
				PdfPCell cell;

				cell = new PdfPCell(new Paragraph(ConfigurationManager.getConfigurationFromCache(
						"invoice.details.usagedetails.label.Date/Time." + lang), tableFontBold));
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setFixedHeight(22f);
				cell.setBorderWidth(0.5f);
				table.addCell(cell);

				cell = new PdfPCell(new Paragraph(ConfigurationManager.getConfigurationFromCache(
						"invoice.details.usagedetails.label.service." + lang), tableFontBold));
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setFixedHeight(22f);
				cell.setBorderWidth(0.5f);
				table.addCell(cell);

				cell = new PdfPCell(new Paragraph(ConfigurationManager.getConfigurationFromCache(
						"invoice.details.usagedetails.label.number." + lang), tableFontBold));
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setFixedHeight(22f);
				cell.setBorderWidth(0.5f);
				table.addCell(cell);

				cell = new PdfPCell(new Paragraph(ConfigurationManager
						.getConfigurationFromCache("invoice.details.usagedetails.label.usage." + lang), tableFontBold));
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setFixedHeight(22f);
				cell.setBorderWidth(0.5f);
				table.addCell(cell);

				cell = new PdfPCell(new Paragraph(ConfigurationManager.getConfigurationFromCache(
						"invoice.details.usagedetails.label.period." + lang), tableFontBold));
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setFixedHeight(22f);
				cell.setBorderWidth(0.5f);
				table.addCell(cell);

				cell = new PdfPCell(new Paragraph(ConfigurationManager.getConfigurationFromCache(
						"invoice.details.usagedetails.label.destination." + lang), tableFontBold));
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setFixedHeight(22f);
				cell.setBorderWidth(0.5f);
				table.addCell(cell);

				cell = new PdfPCell(new Paragraph(ConfigurationManager.getConfigurationFromCache(
						"invoice.details.usagedetails.label.amount." + lang), tableFontBold));
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setFixedHeight(22f);
				cell.setBorderWidth(0.5f);
				table.addCell(cell);

				cell = new PdfPCell(new Paragraph(ConfigurationManager.getConfigurationFromCache(
						"invoice.details.usagedetails.label.region." + lang), tableFontBold));
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setFixedHeight(22f);
				cell.setBorderWidth(0.5f);
				table.addCell(cell);

				for (int j = 0; j < summaryData.size(); j++) {

					cell = new PdfPCell(new Paragraph(summaryData.get(j).getStartDateTime(), tableFont));
					cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					cell.setFixedHeight(22f);
					cell.setBorderWidth(0.5f);
					table.addCell(cell);

					cell = new PdfPCell(new Paragraph(summaryData.get(j).getService(), tableFont));
					cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					cell.setFixedHeight(22f);
					cell.setBorderWidth(0.5f);
					table.addCell(cell);

					cell = new PdfPCell(new Paragraph(summaryData.get(j).getNumber(), tableFont));
					cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					cell.setFixedHeight(22f);
					cell.setBorderWidth(0.5f);
					table.addCell(cell);

					cell = new PdfPCell(new Paragraph(summaryData.get(j).getUsage(), tableFont));
					cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					cell.setFixedHeight(22f);
					cell.setBorderWidth(0.5f);
					table.addCell(cell);

					cell = new PdfPCell(new Paragraph(summaryData.get(j).getPeriod(), tableFont));
					cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					cell.setFixedHeight(22f);
					cell.setBorderWidth(0.5f);
					table.addCell(cell);

					cell = new PdfPCell(new Paragraph(summaryData.get(j).getDestination(), tableFont));
					cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					cell.setFixedHeight(22f);
					cell.setBorderWidth(0.5f);
					table.addCell(cell);

					cell = new PdfPCell(new Paragraph(summaryData.get(j).getChargedAmount() + "AZN", tableFont));
					cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					cell.setFixedHeight(22f);
					cell.setBorderWidth(0.5f);
					table.addCell(cell);

					cell = new PdfPCell(new Paragraph(summaryData.get(j).getZone(), tableFont));
					cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					cell.setFixedHeight(22f);
					cell.setBorderWidth(0.5f);
					table.addCell(cell);
				}

				document.add(table);
				// Close Document and Writer
				document.close();
				writer.close();
				String finalPath = ConfigurationManager.getConfigurationFromCache("invoice.details.pdfExternalPath");
				// comented for testing
				// uncomment after testing
				// loggerV2.info("pdf path:" + finalPath);
				finalPath = finalPath + msisdn + "_usagehistory" + ext;
				// finalPath =
				// "http://10.220.48.216:8080/azerfon/static/pic_new22019-11-05-12.28.23.pdf";
				// hard coded for testing
				return finalPath;

			} catch (FileNotFoundException e1) {
				loggerV2.info(Helper.GetException(e1));
				e1.printStackTrace();
			} catch (DocumentException e1) {
				loggerV2.info(Helper.GetException(e1));
				e1.printStackTrace();
			} catch (MalformedURLException e) {
				loggerV2.info(Helper.GetException(e));
				e.printStackTrace();
			} catch (IOException e) {
				loggerV2.info(Helper.GetException(e));
				e.printStackTrace();
			}
		} else {
			loggerV2.info("Data is empty.");
		}
		return "";
	}

	public String generateMsisdnSummaryPdf(String msisdn, String selctedMsisdn,
			MSISDNSummaryResponse msisdnSummaryResponse, String lang) {

		// Path where PDF is Created

		List<MSISDNSummary> summaryData = msisdnSummaryResponse.getData().getSummaryData();

		String path = ConfigurationManager.getConfigurationFromCache("invoice.details.pdfLocalPath");
		String ext = Constants.PDF_EXT;
		String pdfPath = path + msisdn + "_" + selctedMsisdn + "_summary" + ext;
		loggerV2.info("  PdfPath is :" + pdfPath);

		File file = new File(pdfPath);
		while (file.exists()) {
			loggerV2.info("-------File exist at this path : " + pdfPath);
			file.delete();
		}

		Document document = new Document(PageSize.A4);
		if (summaryData != null && !summaryData.isEmpty()) {
			try {
				// Get writer and Open Document
				PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(pdfPath));
				document.open();

				// Add Header
				String headerPath = ConfigurationManager.getConfigurationFromCache("invoice.details.pdfHeaderPath");
				Image imgheader = Image.getInstance(headerPath);
				imgheader.scaleToFit(document.getPageSize().getWidth() - 50, imgheader.getHeight());
				imgheader.setAlignment(Element.ALIGN_CENTER);
				document.add(imgheader);
				loggerV2.info("  Header path is : " + headerPath);

				// Register Custom Font
				FontFactory.register(ConfigurationManager.getConfigurationFromCache("invoice.details.pdfFontPath"),
						"alssans");
				// Font for heading
				Font headingFont = FontFactory.getFont("alssans", BaseFont.IDENTITY_H, true, 16, Font.BOLD);
				// Font for normal
				Font tableFont = FontFactory.getFont("alssans", BaseFont.IDENTITY_H, true, 12);
				// Font for bold
				Font tableFontBold = FontFactory.getFont("alssans", BaseFont.IDENTITY_H, true, 12, Font.BOLD);

				// Create heading
				document.add(new Paragraph(Chunk.NEWLINE));
				Paragraph paragraph = new Paragraph(ConfigurationManager
						.getConfigurationFromCache("invoice.details.msisdn.invoice.summary." + lang), headingFont);
				paragraph.setAlignment(Element.ALIGN_CENTER);
				document.add(paragraph);
				document.add(Chunk.NEWLINE);

				// Create Table
				PdfPTable table = new PdfPTable(3);
				table.setWidthPercentage(70);
				float[] columnWidths = { 1f, 1f, 1f };
				table.setWidths(columnWidths);

				loggerV2.info(" Filling data in Table ------------- pdf Generator ");

				// Filling data in table
				PdfPCell cell;

				cell = new PdfPCell(new Paragraph(ConfigurationManager
						.getConfigurationFromCache("invoice.details.number."+lang), tableFont));
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setFixedHeight(22f);
				cell.setBorderWidth(0.5f);
				table.addCell(cell);

				cell = new PdfPCell(new Paragraph(ConfigurationManager
						.getConfigurationFromCache("invoice.details.tariff."+lang), tableFont));
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setFixedHeight(22f);
				cell.setBorderWidth(0.5f);
				table.addCell(cell);

				cell = new PdfPCell(new Paragraph(ConfigurationManager
						.getConfigurationFromCache("invoice.details.debt."+lang), tableFontBold));
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setFixedHeight(22f);
				cell.setBorderWidth(0.5f);
				table.addCell(cell);

				for (int j = 0; j < summaryData.size(); j++) {

					cell = new PdfPCell(new Paragraph(summaryData.get(j).getMsisdn(), tableFont));
					cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					cell.setFixedHeight(22f);
					cell.setBorderWidth(0.5f);
					table.addCell(cell);

					cell = new PdfPCell(new Paragraph(summaryData.get(j).getTariff(), tableFont));
					cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					cell.setFixedHeight(22f);
					cell.setBorderWidth(0.5f);
					table.addCell(cell);

					cell = new PdfPCell(new Paragraph(summaryData.get(j).getDebt(), tableFontBold));
					cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					cell.setFixedHeight(22f);
					cell.setBorderWidth(0.5f);
					table.addCell(cell);
				}

				document.add(table);
				// Close Document and Writer
				document.close();
				writer.close();
				String finalPath = ConfigurationManager.getConfigurationFromCache("invoice.details.pdfExternalPath");
				// comented for testing
				// uncomment after testing
				// loggerV2.info("pdf path:" + finalPath);
				finalPath = finalPath + msisdn + "_" + selctedMsisdn + "_summary" + ext;
				// finalPath =
				// "http://10.220.48.216:8080/azerfon/static/pic_new22019-11-05-12.28.23.pdf";
				// hard coded for testing
				return finalPath;

			} catch (FileNotFoundException e1) {
				loggerV2.info(Helper.GetException(e1));
				e1.printStackTrace();
			} catch (DocumentException e1) {
				loggerV2.info(Helper.GetException(e1));
				e1.printStackTrace();
			} catch (MalformedURLException e) {
				loggerV2.info(Helper.GetException(e));
				e.printStackTrace();
			} catch (IOException e) {
				loggerV2.info(Helper.GetException(e));
				e.printStackTrace();
			}
		}
		return "";
	}

}
