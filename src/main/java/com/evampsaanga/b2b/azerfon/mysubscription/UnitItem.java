package com.evampsaanga.b2b.azerfon.mysubscription;

import java.text.ParseException;

import org.apache.log4j.Logger;

import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.developer.utils.Helper;

public class UnitItem {
	
	public static final Logger logger = Logger.getLogger("azerfon-esb");
	
	private String offerName = "";
	private String totalUnits = "0";
	private String remainingUnits = "0";
	private String unitName = "";
	private String initialDate = "";
	private String expiryDate = "";
	private String type = "";
	private String icon = "";
	private int sorting = 0;

	/*public UnitItem(String offerName, String totalUnits, String remainingUnits, String unitName, String initialDate,
			String expiryDate, String icon, int sorting) {
		super();
		logger.info("Contructor without type called with following values");
		logger.info("OFFER NAME: "+offerName);
		logger.info("TOTAL UNITS: "+totalUnits);
		logger.info("REMAINING UNITS: "+remainingUnits);
		logger.info("UNIT NAME: "+unitName);
		logger.info("INITIAL DATE: "+initialDate);
		logger.info("EXPIRY DATE: "+expiryDate);
		logger.info("ICON: "+icon);
		logger.info("SORTING: "+sorting);
		
		if(unitName.equalsIgnoreCase("Minutes"))
		{
			unitName = "Seconds";
			remainingUnits = Integer.toString(Integer.parseInt(remainingUnits) * 60);
			totalUnits = Integer.toString(Integer.parseInt(totalUnits) * 60);
		}
		
		this.offerName = offerName;
		this.totalUnits = totalUnits;
		this.remainingUnits = remainingUnits;
		this.unitName = unitName;
		this.initialDate = initialDate;
		this.expiryDate = expiryDate;
		this.icon = icon;
		this.sorting = sorting;
	}*/

	public UnitItem(String offerName, String totalUnits, String remainingUnits, String unitName, String initialDate,
			String expiryDate, String icon, String type,int sorting) throws ParseException {
		super();
		
		logger.info("Contructor with type called with following values");
		logger.info("OFFER NAME: "+offerName);
		logger.info("TOTAL UNITS: "+totalUnits);
		logger.info("REMAINING UNITS: "+remainingUnits);
		logger.info("UNIT NAME: "+unitName);
		
		
		
		
		
		
		logger.info("INITIAL DATE: "+initialDate);
		logger.info("ICON: "+icon);
		logger.info("SORTING: "+sorting);
		logger.info("TYPE: "+type);
		
		if(unitName.equalsIgnoreCase("Minutes"))
		{
			
			unitName = "Seconds";
			remainingUnits = Integer.toString(Integer.parseInt(remainingUnits) * 60);
			totalUnits = Integer.toString(Integer.parseInt(totalUnits) * 60);
		}
		if(unitName.contains("."))
		{
			unitName =unitName.replace(".", "");

		}
		
		this.offerName = offerName;
		this.totalUnits = totalUnits;
		this.remainingUnits = remainingUnits;
		this.unitName = unitName;
		
		if(expiryDate!=null && !expiryDate.equals(""))
		{
			logger.info("EXPIRY DATE: "+Helper.addOneSecondForNGBSSExpiryDate(expiryDate));
			this.initialDate = initialDate;
			this.expiryDate = Helper.addOneSecondForNGBSSExpiryDate(expiryDate);
		}
		else 
		{
			this.expiryDate = expiryDate;
		}
		
		if(type.equalsIgnoreCase("SMS")){
			this.type=Constants.MY_SUBSCRIPTIONS_FREE_USAGE_SMS;
		}else if(type.equals("VOICE")){
			this.type=Constants.MY_SUBSCRIPTIONS_FREE_USAGE_CALL;;
		}else if(type.equals("DATA")){
			this.type=Constants.MY_SUBSCRIPTIONS_FREE_USAGE_INTERNET;
		}else{
			this.type=type; //""
		}
		
		
		this.icon = icon;
		this.sorting = sorting;
	}

	/**
	 * @return the offerName
	 */
	public String getOfferName() {
		return offerName;
	}

	/**
	 * @param offerName
	 *            the offerName to set
	 */
	public void setOfferName(String offerName) {
		this.offerName = offerName;
	}

	/**
	 * @return the totalUnits
	 */
	public String getTotalUnits() {
		return totalUnits;
	}

	/**
	 * @param totalUnits
	 *            the totalUnits to set
	 */
	public void setTotalUnits(String totalUnits) {
		this.totalUnits = totalUnits;
	}

	/**
	 * @return the remainingUnits
	 */
	public String getRemainingUnits() {
		return remainingUnits;
	}

	/**
	 * @param remainingUnits
	 *            the remainingUnits to set
	 */
	public void setRemainingUnits(String remainingUnits) {
		this.remainingUnits = remainingUnits;
	}

	/**
	 * @return the unitName
	 */
	public String getUnitName() {
		return unitName;
	}

	/**
	 * @param unitName
	 *            the unitName to set
	 */
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	/**
	 * @return the initialDate
	 */
	public String getInitialDate() {
		return initialDate;
	}

	/**
	 * @param initialDate
	 *            the initialDate to set
	 */
	public void setInitialDate(String initialDate) {
		this.initialDate = initialDate;
	}

	/**
	 * @return the expiryDate
	 */
	public String getExpiryDate() {
		return expiryDate;
	}

	/**
	 * @param expiryDate
	 *            the expiryDate to set
	 */
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	/**
	 * @return the icon
	 */
	public String getIcon() {
		return icon;
	}

	/**
	 * @param icon
	 *            the icon to set
	 */
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getSorting() {
		return sorting;
	}

	public void setSorting(int sorting) {
		this.sorting = sorting;
	}
	
	public static void main(String[] args) {
		String base = "loved.";
		
		String arr=base.replace(".", "");
		System.out.println("VALUE :"+arr);
	}
}
