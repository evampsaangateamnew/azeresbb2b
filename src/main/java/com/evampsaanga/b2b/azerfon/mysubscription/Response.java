package com.evampsaanga.b2b.azerfon.mysubscription;

import java.util.ArrayList;
import java.util.HashMap;

import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;

public class Response extends BaseResponse {
	private ArrayList<FreeUnitItemDetails> items = new ArrayList<FreeUnitItemDetails>();
	private HashMap<String, ArrayList<FreeUnitItemDetails>> freeResources = new HashMap<>();

	/**
	 * @return the items
	 */
	public ArrayList<FreeUnitItemDetails> getItems() {
		return items;
	}

	/**
	 * @param items
	 *            the items to set
	 */
	public void setItems(ArrayList<FreeUnitItemDetails> items) {
		this.items = items;
	}

	public HashMap<String, ArrayList<FreeUnitItemDetails>> getFreeResources() {
		return freeResources;
	}

	public void setFreeResources(HashMap<String, ArrayList<FreeUnitItemDetails>> freeResources) {
		this.freeResources = freeResources;
	}
}
