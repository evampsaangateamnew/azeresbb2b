package com.evampsaanga.b2b.azerfon.mysubscription;

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Request extends BaseRequest {
	
	 @JsonProperty("groupAccessCode")
	 private String groupAccessCode;

	 @JsonProperty("groupAccessCode")
	public String getGroupAccessCode() {
		return groupAccessCode;
	}

	 @JsonProperty("groupAccessCode")
	public void setGroupAccessCode(String groupAccessCode) {
		this.groupAccessCode = groupAccessCode;
	}
}
