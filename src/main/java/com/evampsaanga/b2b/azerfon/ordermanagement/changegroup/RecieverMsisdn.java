
package com.evampsaanga.b2b.azerfon.ordermanagement.changegroup;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "tariffIds",
    "msisdn",
    "groupType",
    "groupIdFrom"
})
public class RecieverMsisdn {

    @JsonProperty("tariffIds")
    private String tariffIds;
    @JsonProperty("msisdn")
    private String msisdn;
    @JsonProperty("groupType")
    private String groupType;
    @JsonProperty("groupIdFrom")
    private String groupIdFrom;
    @JsonProperty("corpCrmCustName")
    private String corpCrmCustName;
    

    
    @JsonProperty("corpCrmCustName")
    public String getCorpCrmCustName() {
		return corpCrmCustName;
	}
    @JsonProperty("corpCrmCustName")
	public void setCorpCrmCustName(String corpCrmCustName) {
		this.corpCrmCustName = corpCrmCustName;
	}

	@JsonProperty("tariffIds")
    public String getTariffIds() {
        return tariffIds;
    }

    @JsonProperty("tariffIds")
    public void setTariffIds(String tariffIds) {
        this.tariffIds = tariffIds;
    }

    @JsonProperty("msisdn")
    public String getMsisdn() {
        return msisdn;
    }

    @JsonProperty("msisdn")
    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    @JsonProperty("groupType")
    public String getGroupType() {
        return groupType;
    }

    @JsonProperty("groupType")
    public void setGroupType(String groupType) {
        this.groupType = groupType;
    }

    @JsonProperty("groupIdFrom")
    public String getGroupIdFrom() {
        return groupIdFrom;
    }

    @JsonProperty("groupIdFrom")
    public void setGroupIdFrom(String groupIdFrom) {
        this.groupIdFrom = groupIdFrom;
    }

    

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("tariffIds", tariffIds).append("msisdn", msisdn).append("groupType", groupType).append("groupIdFrom", groupIdFrom).toString();
    }

}
