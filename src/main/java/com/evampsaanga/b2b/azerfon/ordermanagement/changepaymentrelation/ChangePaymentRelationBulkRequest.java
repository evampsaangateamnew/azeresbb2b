package com.evampsaanga.b2b.azerfon.ordermanagement.changepaymentrelation;

import java.util.ArrayList;

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class ChangePaymentRelationBulkRequest extends BaseRequest {

	private String amount="";
	
	private ArrayList<BulkPaymentRelationMsisdns> paymentsMsisdns=null;
	
	public ArrayList<BulkPaymentRelationMsisdns> getPaymentsMsisdns() {
		return paymentsMsisdns;
	}
	public void setPaymentsMsisdns(ArrayList<BulkPaymentRelationMsisdns> paymentsMsisdns) {
		this.paymentsMsisdns = paymentsMsisdns;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}

}
