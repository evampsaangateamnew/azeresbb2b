
package com.evampsaanga.b2b.azerfon.ordermanagement.changegroup;

import java.util.List;

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"actPrice",
"msgLang",
"orderKey",
"recieverMsisdn",
"destinationTariff",
"tariffPermissions",
"textmsg",
"senderName",
"amount",
"offeringId",
"number",
"actionType",
"companyValue",
"totalLimit",
"orderId",
"accountId",
"groupTypeTo",
"groupIdTo"
})
public class ChangeGroupAttributes extends BaseRequest {

@JsonProperty("actPrice")
private String actPrice;
@JsonProperty("msgLang")
private String msgLang;
@JsonProperty("orderKey")
private String orderKey;
@JsonProperty("recieverMsisdn")
private List<RecieverMsisdn> recieverMsisdn = null;
@JsonProperty("destinationTariff")
private String destinationTariff;
@JsonProperty("tariffPermissions")
private String tariffPermissions;
@JsonProperty("textmsg")
private String textmsg;
@JsonProperty("senderName")
private String senderName;
@JsonProperty("amount")
private String amount;
@JsonProperty("offeringId")
private String offeringId;
@JsonProperty("number")
private String number;
@JsonProperty("actionType")
private String actionType;
@JsonProperty("companyValue")
private String companyValue;
@JsonProperty("totalLimit")
private String totalLimit;
@JsonProperty("orderId")
private String orderId;
@JsonProperty("accountId")
private String accountId;
@JsonProperty("groupTypeTo")
private String groupTypeTo;
@JsonProperty("groupIdTo")
private String groupIdTo;
@JsonProperty("customerId")
private String customerId;



@JsonProperty("customerId")
public String getCustomerId() {
	return customerId;
}
@JsonProperty("customerId")
public void setCustomerId(String customerId) {
	this.customerId = customerId;
}

@JsonProperty("actPrice")
public String getActPrice() {
return actPrice;
}

@JsonProperty("actPrice")
public void setActPrice(String actPrice) {
this.actPrice = actPrice;
}

@JsonProperty("msgLang")
public String getMsgLang() {
return msgLang;
}

@JsonProperty("msgLang")
public void setMsgLang(String msgLang) {
this.msgLang = msgLang;
}

@JsonProperty("orderKey")
public String getOrderKey() {
return orderKey;
}

@JsonProperty("orderKey")
public void setOrderKey(String orderKey) {
this.orderKey = orderKey;
}

@JsonProperty("recieverMsisdn")
public List<RecieverMsisdn> getRecieverMsisdn() {
return recieverMsisdn;
}

@JsonProperty("recieverMsisdn")
public void setRecieverMsisdn(List<RecieverMsisdn> recieverMsisdn) {
this.recieverMsisdn = recieverMsisdn;
}

@JsonProperty("destinationTariff")
public String getDestinationTariff() {
return destinationTariff;
}

@JsonProperty("destinationTariff")
public void setDestinationTariff(String destinationTariff) {
this.destinationTariff = destinationTariff;
}

@JsonProperty("tariffPermissions")
public String getTariffPermissions() {
return tariffPermissions;
}

@JsonProperty("tariffPermissions")
public void setTariffPermissions(String tariffPermissions) {
this.tariffPermissions = tariffPermissions;
}

@JsonProperty("textmsg")
public String getTextmsg() {
return textmsg;
}

@JsonProperty("textmsg")
public void setTextmsg(String textmsg) {
this.textmsg = textmsg;
}

@JsonProperty("senderName")
public String getSenderName() {
return senderName;
}

@JsonProperty("senderName")
public void setSenderName(String senderName) {
this.senderName = senderName;
}

@JsonProperty("amount")
public String getAmount() {
return amount;
}

@JsonProperty("amount")
public void setAmount(String amount) {
this.amount = amount;
}

@JsonProperty("offeringId")
public String getOfferingId() {
return offeringId;
}

@JsonProperty("offeringId")
public void setOfferingId(String offeringId) {
this.offeringId = offeringId;
}

@JsonProperty("number")
public String getNumber() {
return number;
}

@JsonProperty("number")
public void setNumber(String number) {
this.number = number;
}

@JsonProperty("actionType")
public String getActionType() {
return actionType;
}

@JsonProperty("actionType")
public void setActionType(String actionType) {
this.actionType = actionType;
}

@JsonProperty("companyValue")
public String getCompanyValue() {
return companyValue;
}

@JsonProperty("companyValue")
public void setCompanyValue(String companyValue) {
this.companyValue = companyValue;
}

@JsonProperty("totalLimit")
public String getTotalLimit() {
return totalLimit;
}

@JsonProperty("totalLimit")
public void setTotalLimit(String totalLimit) {
this.totalLimit = totalLimit;
}

@JsonProperty("orderId")
public String getOrderId() {
return orderId;
}

@JsonProperty("orderId")
public void setOrderId(String orderId) {
this.orderId = orderId;
}

@JsonProperty("accountId")
public String getAccountId() {
return accountId;
}

@JsonProperty("accountId")
public void setAccountId(String accountId) {
this.accountId = accountId;
}

@JsonProperty("groupTypeTo")
public String getGroupTypeTo() {
return groupTypeTo;
}

@JsonProperty("groupTypeTo")
public void setGroupTypeTo(String groupTypeTo) {
this.groupTypeTo = groupTypeTo;
}

@JsonProperty("groupIdTo")
public String getGroupIdTo() {
return groupIdTo;
}

@JsonProperty("groupIdTo")
public void setGroupIdTo(String groupIdTo) {
this.groupIdTo = groupIdTo;
}

@Override
public String toString() {
return new ToStringBuilder(this).append("actPrice", actPrice).append("msgLang", msgLang).append("orderKey", orderKey).append("recieverMsisdn", recieverMsisdn).append("destinationTariff", destinationTariff).append("tariffPermissions", tariffPermissions).append("textmsg", textmsg).append("senderName", senderName).append("amount", amount).append("offeringId", offeringId).append("number", number).append("actionType", actionType).append("companyValue", companyValue).append("totalLimit", totalLimit).append("orderId", orderId).append("accountId", accountId).append("groupTypeTo", groupTypeTo).append("groupIdTo", groupIdTo).toString();
}

}