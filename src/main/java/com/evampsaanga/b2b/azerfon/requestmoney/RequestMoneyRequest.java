package com.evampsaanga.b2b.azerfon.requestmoney;

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;

public class RequestMoneyRequest extends BaseRequest {

	private String friendMsisdn;
	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	private String amount;

	public String getFriendMsisdn() {
		return friendMsisdn;
	}

	public void setFriendMsisdn(String friendMsisdn) {
		this.friendMsisdn = friendMsisdn;
	}

	@Override
	public String toString() {
		return "RequestMoneyRequest [friendMsisdn=" + friendMsisdn + "]";
	}

}
