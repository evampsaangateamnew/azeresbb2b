package com.evampsaanga.b2b.azerfon.getcdrsoperationhistory;

import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.amqimplementationsesb.LogsType;
import com.evampsaanga.b2b.azerfon.db.DBAzerfonFactory;
import com.evampsaanga.b2b.azerfon.getcdrsbydate.CustomerID;
import com.evampsaanga.b2b.azerfon.getcdrsbydate.GetCDRsByDateLand;
import com.evampsaanga.b2b.azerfon.getcdrssummary.GetCDRsSummaryRequestResponse;
import com.evampsaanga.b2b.azerfon.utilities.ConversionUtilities;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.ThirdPartyNames;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Decrypter;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.evampsaanga.b2b.developer.utils.SOAPLoggingHandler;
@Path("/azerfon/")
public class GetCDRsOperationHistoryLand {
    public static final Logger logger = Logger.getLogger("azerfon-esb");
 
    @POST
    @Path("/get")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public CDRsOperationHistoryRequestResponse Get(@Header("credentials") String credential,
            @Header("Content-Type") String contentType, @Body() String requestBody) {
 
        Logs logs = new Logs();
        logs.setTransactionName(Transactions.GET_CDRS_OPERATION_HISTORY_TRANSACTION_NAME);
        logs.setThirdPartyName(ThirdPartyNames.GET_CDRS_OPERATION_HISTORY);
        logs.setTableType(LogsType.GetCdrsOperationHistory);
        String token = "";
        String TrnsactionName = Transactions.GET_CDRS_OPERATION_HISTORY_TRANSACTION_NAME;
 
        try {
 
            token = Helper.retrieveToken(TrnsactionName, Helper.getValueFromJSON(requestBody, "msisdn"));
            logger.info(token + "Request Landed on GetCDRsOperationHistoryLand:" + requestBody);
            GetCDRsOperationHistoryRequest cclient = new GetCDRsOperationHistoryRequest();
            try {
                cclient = Helper.JsonToObject(requestBody, GetCDRsOperationHistoryRequest.class);
                if (cclient != null) {
                    if (cclient.getIsB2B() != null && cclient.getIsB2B().equals("true"))
                        logs.setTransactionName(Transactions.GET_CDRS_OPERATION_HISTORY_TRANSACTION_NAME_B2B);
                    logs.setIp(cclient.getiP());
                    logs.setChannel(cclient.getChannel());
                    logs.setMsisdn(cclient.getmsisdn());
                }
            } catch (Exception ex) {
                logger.info(token + Helper.GetException(ex));
                CDRsOperationHistoryRequestResponse resp = new CDRsOperationHistoryRequestResponse();
                resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
                resp.setReturnMsg(ResponseCodes.ERROR_400);
                logs.setResponseCode(resp.getReturnCode());
                logs.setResponseDescription(resp.getReturnMsg());
                logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
                logs.updateLog(logs);
                return resp;
            }
            try {
                if (cclient != null) {
                    Date datestart = new SimpleDateFormat("yyyy-MM-dd").parse(cclient.getStartDate());
                    Date dateend = new SimpleDateFormat("yyyy-MM-dd").parse(cclient.getEndDate());
                    Date today = new Date();
                    if (datestart.compareTo(dateend) > 0) {
                        CDRsOperationHistoryRequestResponse resp = new CDRsOperationHistoryRequestResponse();
                        resp.setReturnCode(ResponseCodes.START_DATE_GRATER_END_DATE_CODE);
                        resp.setReturnMsg(ResponseCodes.START_DATE_GRATER_END_DATE_DES);
                        logs.setResponseCode(resp.getReturnCode());
                        logs.setResponseDescription(resp.getReturnMsg());
                        logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
                        logs.updateLog(logs);
                        return resp;
                    } else if (today.compareTo(datestart) < 0) {
                        CDRsOperationHistoryRequestResponse resp = new CDRsOperationHistoryRequestResponse();
                        resp.setReturnCode(ResponseCodes.START_DATE_CANNOT_BE_IN_FUTURE_CODE);
                        resp.setReturnMsg(ResponseCodes.START_DATE_CANNOT_BE_IN_FUTURE_DES);
                        logs.setResponseCode(resp.getReturnCode());
                        logs.setResponseDescription(resp.getReturnMsg());
                        logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
                        logs.updateLog(logs);
                        return resp;
                    } else if (today.compareTo(dateend) < 0) {
                        CDRsOperationHistoryRequestResponse resp = new CDRsOperationHistoryRequestResponse();
                        resp.setReturnCode(ResponseCodes.END_DATE_CANNOT_BE_IN_FUTURE_CODE);
                        resp.setReturnMsg(ResponseCodes.END_DATE_CANNOT_BE_IN_FUTURE_DES);
                        logs.setResponseCode(resp.getReturnCode());
                        logs.setResponseDescription(resp.getReturnMsg());
                        logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
                        logs.updateLog(logs);
                        return resp;
                    }
                }
            } catch (Exception e) {
                logger.info(token + Helper.GetException(e));
            }
            if (cclient.getAccountId().equals("")) {
                CDRsOperationHistoryRequestResponse resp1 = new CDRsOperationHistoryRequestResponse();
                resp1.setReturnCode(ResponseCodes.MISSING_PARAMETER_CODE);
                resp1.setReturnMsg(ResponseCodes.MISSING_PARAMETER_DES);
                logs.setResponseCode(resp1.getReturnCode());
                logs.setResponseDescription(resp1.getReturnMsg());
                logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
                logs.updateLog(logs);
                return resp1;
            }
            if (cclient != null) {
                String credentials = null;
                try {
                    credentials = Decrypter.getInstance().decrypt(credential);
                } catch (Exception ex) {
                    SOAPLoggingHandler.logger.info(token + Helper.GetException(ex));
                }
                if (credentials == null) {
                    CDRsOperationHistoryRequestResponse resp = new CDRsOperationHistoryRequestResponse();
                    resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
                    resp.setReturnMsg(ResponseCodes.ERROR_401);
                    logs.setResponseCode(resp.getReturnCode());
                    logs.setResponseDescription(resp.getReturnMsg());
                    logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
                    logs.updateLog(logs);
                    return resp;
                }
                if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
                    String verification = Helper.validateRequest(cclient);
                    if (!verification.equals("")) {
                        CDRsOperationHistoryRequestResponse res = new CDRsOperationHistoryRequestResponse();
                        res.setReturnCode(ResponseCodes.ERROR_400);
                        res.setReturnMsg(verification);
                        logs.setResponseCode(res.getReturnCode());
                        logs.setResponseDescription(res.getReturnMsg());
                        logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
                        logs.updateLog(logs);
                        return res;
                    }
                } else {
                    CDRsOperationHistoryRequestResponse resp = new CDRsOperationHistoryRequestResponse();
                    resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
                    resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
                    logs.setResponseCode(resp.getReturnCode());
                    logs.setResponseDescription(resp.getReturnMsg());
                    logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
                    logs.updateLog(logs);
                    return resp;
                }
                if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
                    String lang = "2002";
                    if (cclient.getLang().equalsIgnoreCase("3"))
                        lang = "2002";
                    if (cclient.getLang().equalsIgnoreCase("2"))
                        lang = "2052";
                    if (cclient.getLang().equalsIgnoreCase("4"))
                        lang = "2060";
 
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
 
                    String start = "00:00:00";
                    Date startime = simpleDateFormat.parse(start);
                    Date endtime = simpleDateFormat.parse("05:00:00");
 
                    // current time
                    String timeStamp = new SimpleDateFormat("HH:mm:ss").format(new Date());
 
                    logger.info(token + "TimeStamp :" + timeStamp);
                    Date current_time = simpleDateFormat.parse(timeStamp);
 
                    if (current_time.after(startime) && current_time.before(endtime)) {
                        CDRsOperationHistoryRequestResponse resp = new CDRsOperationHistoryRequestResponse();
                        logger.info(token + "Time is between 00:00:00 and 05:00:00 ");
                        resp.setReturnCode(ResponseCodes.ERROR_CODE_SERVICE_UNAVAILABLE_AT_12AM_TO_5AM);
                        resp.setReturnMsg(ResponseCodes.ERROR_MESSAGE_SERVICE_UNAVAILABLE_AT_12AM_TO_5AM);
                        logs.setResponseCode(resp.getReturnCode());
                        logs.setResponseDescription(resp.getReturnMsg());
                        logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
                        logs.updateLog(logs);
                        logger.info(token + Helper.ObjectToJson(resp));
                        return resp;
 
                    } else {
                        return getResponse(cclient.getStartDate(), cclient.getEndDate(), cclient.getmsisdn(),
                                cclient.getAccountId(), logs, cclient.getCustomerId(), lang, logger, token);
                    }
 
                } else {
                    CDRsOperationHistoryRequestResponse resp = new CDRsOperationHistoryRequestResponse();
                    resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
                    resp.setReturnMsg(ResponseCodes.ERROR_401);
                    logs.setResponseCode(resp.getReturnCode());
                    logs.setResponseDescription(resp.getReturnMsg());
                    logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
                    logs.updateLog(logs);
                    return resp;
                }
            }
        } catch (Exception ex) {
            logger.info(token + Helper.GetException(ex));
        }
        CDRsOperationHistoryRequestResponse resp = new CDRsOperationHistoryRequestResponse();
        resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
        resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
        logs.setResponseCode(resp.getReturnCode());
        logs.setResponseDescription(resp.getReturnMsg());
        logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
        logs.updateLog(logs);
        return resp;
    }
 
    public CustomerID getCustomerIDFromView(String msisdn, Logger logger, String token) {
        CustomerID customerId = new CustomerID();
        Connection myConnection = DBAzerfonFactory.getConnection();
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            String sql = "select CBS_CUST_ID,CRM_CUST_ID from V_E_CARE_CUST_INFO t where MSISDN = substr(?,-9) and rn = 1";
            logger.debug(sql);
            statement = myConnection.prepareStatement(sql);
            statement.setString(1, msisdn);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                customerId.setCbscustomerId(resultSet.getString("CBS_CUST_ID"));
                customerId.setCrmcustomerId(resultSet.getString("CRM_CUST_ID"));
            }
            statement.close();
            resultSet.close();
        } catch (Exception e) {
            logger.info(token + Helper.GetException(e));
        } finally {
            try {
                if (resultSet != null)
                    resultSet.close();
                if (statement != null)
                    statement.close();
            } catch (Exception e) {
                logger.info(token + Helper.GetException(e));
            }
        }
        return customerId;
    }
 
    public CDRsOperationHistoryRequestResponse getResponse(String startDate, String endDate, String msisdn,
            String accountId, Logs logs, String customerIdAPI, String lang, final Logger logger, final String token) {
        if (GetCDRsByDateLand.usageHistoryTranslationMapping.isEmpty())
            GetCDRsByDateLand.populateUsageHistoryTranslationMapping(token);
        logger.info(token+"*********************Customer id*************************");
        CustomerID customerId = getCustomerIDFromView(msisdn, logger, token);
        logger.info(token+"*******************Customer id***************************");
        ArrayList<CDRsOperationDetails> list = new ArrayList<>();
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
//          customerId.setCrmcustomerId(customerIdAPI);
            if (customerId != null && customerId.getCrmcustomerId() != null&& customerId.getCrmcustomerId().equalsIgnoreCase(customerIdAPI))
            {
            	logger.info(token + "-------------------------------------------------------------");
                logger.info(token + "MSISDN-" + msisdn + "-CustomerID From DB-" + customerId.getCrmcustomerId()
                        + "-CustomerID From API-" + customerIdAPI + "-ID matched.");
 
                String newStartDate[] = startDate.split(" ");
                String finalStartDate = "";
                String newEndDate[] = endDate.split(" ");
                String finalEndDate = "";
 
                finalStartDate = newStartDate[0] + " 00:00:00";
                finalEndDate = newEndDate[0] + " 23:59:59";
                String msisdnInt = Constants.AZERI_COUNTRY_CODE + msisdn;
                String sql = "select * from V_E_CARE_TRANSACTION where CUST_LOCAL_START_DATE >= to_date(?,'yyyy-mm-dd hh24:mi:ss') and CUST_LOCAL_START_DATE <="
                        + " to_date(?,'yyyy-mm-dd hh24:mi:ss') and PRI_IDENTITY=?";
 
                Connection conn = DBAzerfonFactory.getConnection();
                statement = conn.prepareStatement(sql);
 
                statement.setString(1, finalStartDate);
                statement.setString(2, finalEndDate);
                statement.setString(3, msisdnInt);
                logger.info(token + "<<<<  " + statement + ">>>>");
                resultSet = statement.executeQuery();
                String endingbalance = "";
                String clarification = "";
                String amount = "";
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                String datetimefinal = "";
                logger.info(token + "<<<<<< Result Set size:>>>>>>>" + resultSet.getFetchSize());
                while (resultSet.next()) 
                {
                	logger.info(token + "-------------------------------------------------------------");
                    logger.info(token + "<<<<<<<< INFO >>>>>>>>" + resultSet.getString("OWNER_CUST_ID")
                            + "-------Matching with -------" + customerId.getCbscustomerId());
                    if (resultSet.getString("OWNER_CUST_ID").equalsIgnoreCase(customerId.getCbscustomerId())) 
                    {
                        String dateonly = resultSet.getString("DATE_");
                        String timeonly = resultSet.getString("TIME_");
                        try {
                            Date date = new Date();
                            date = formatter.parse(dateonly);
                            DateFormat date1 = new SimpleDateFormat("dd/MM/yy");
                            String dateextract = date1.format(date);
                            datetimefinal = dateextract + " " + timeonly;
                        } catch (Exception e) {
                            logger.info(token + e.getMessage());
                        }
                        logger.info(token + "--------------------------");
                        logger.info(token + resultSet.getString("TYPE"));
                        logger.info(token + resultSet.getString("TRANSACTION_TYPE"));
                        logger.info(token + resultSet.getString("COMMENT_"));
                        logger.info(token + resultSet.getString("DESCRIPTION"));
                        logger.info(token + resultSet.getString("MEASURE_ID"));
                        logger.info(token + "--------------------------");
                        String transactionType = "";
                        String description = "";
                        String measureid = "";
                        if (GetCDRsByDateLand.usageHistoryTranslationMapping
                                .containsKey(GetCDRsByDateLand.cdrsColumnMapping.get("COMMENT")
                                        + Constants.TRANSLATION_CDRS_KEY_SEPARATOR + resultSet.getString("TYPE")
                                        + Constants.TRANSLATION_CDRS_KEY_SEPARATOR + resultSet.getString("COMMENT_")
                                        + Constants.TRANSLATION_CDRS_KEY_SEPARATOR + lang))
                            clarification = GetCDRsByDateLand.usageHistoryTranslationMapping
                                    .get(GetCDRsByDateLand.cdrsColumnMapping.get("COMMENT")
                                            + Constants.TRANSLATION_CDRS_KEY_SEPARATOR + resultSet.getString("TYPE")
                                            + Constants.TRANSLATION_CDRS_KEY_SEPARATOR + resultSet.getString("COMMENT_")
                                            + Constants.TRANSLATION_CDRS_KEY_SEPARATOR + lang);
                        else
                            clarification = resultSet.getString("COMMENT_");
                        if (GetCDRsByDateLand.usageHistoryTranslationMapping
                                .containsKey(GetCDRsByDateLand.cdrsColumnMapping.get("TRANSACTION_TYPE")
                                        + Constants.TRANSLATION_CDRS_KEY_SEPARATOR + resultSet.getString("TYPE")
                                        + Constants.TRANSLATION_CDRS_KEY_SEPARATOR
                                        + resultSet.getString("TRANSACTION_TYPE")
                                        + Constants.TRANSLATION_CDRS_KEY_SEPARATOR + lang))
                            transactionType = GetCDRsByDateLand.usageHistoryTranslationMapping
                                    .get(GetCDRsByDateLand.cdrsColumnMapping.get("TRANSACTION_TYPE")
                                            + Constants.TRANSLATION_CDRS_KEY_SEPARATOR + resultSet.getString("TYPE")
                                            + Constants.TRANSLATION_CDRS_KEY_SEPARATOR
                                            + resultSet.getString("TRANSACTION_TYPE")
                                            + Constants.TRANSLATION_CDRS_KEY_SEPARATOR + lang);
                        else
                            transactionType = resultSet.getString("TRANSACTION_TYPE");
                        logger.info("--------------------------------------------");
                        logger.info("DESCRIPTION VALUE : "+GetCDRsByDateLand.cdrsColumnMapping.get("DESCRIPTION")
                        + Constants.TRANSLATION_CDRS_KEY_SEPARATOR + resultSet.getString("TYPE")
                        + Constants.TRANSLATION_CDRS_KEY_SEPARATOR + resultSet.getString("DESCRIPTION")
                        + Constants.TRANSLATION_CDRS_KEY_SEPARATOR + lang);
                        logger.info("DESCRIPTION KEY VALUE : "+GetCDRsByDateLand.cdrsColumnMapping.get("DESCRIPTION")
                                        + Constants.TRANSLATION_CDRS_KEY_SEPARATOR + resultSet.getString("TYPE")
                                        + Constants.TRANSLATION_CDRS_KEY_SEPARATOR + resultSet.getString("DESCRIPTION")
                                        + Constants.TRANSLATION_CDRS_KEY_SEPARATOR + lang);
                        
                        logger.info("--------------------------------------------");
                        if (GetCDRsByDateLand.usageHistoryTranslationMapping
                                .containsKey(GetCDRsByDateLand.cdrsColumnMapping.get("DESCRIPTION")
                                        + Constants.TRANSLATION_CDRS_KEY_SEPARATOR + resultSet.getString("TYPE")
                                        + Constants.TRANSLATION_CDRS_KEY_SEPARATOR + resultSet.getString("DESCRIPTION")
                                        + Constants.TRANSLATION_CDRS_KEY_SEPARATOR + lang))
                            description = GetCDRsByDateLand.usageHistoryTranslationMapping
                                    .get(GetCDRsByDateLand.cdrsColumnMapping.get("DESCRIPTION")
                                            + Constants.TRANSLATION_CDRS_KEY_SEPARATOR + resultSet.getString("TYPE")
                                            + Constants.TRANSLATION_CDRS_KEY_SEPARATOR
                                            + resultSet.getString("DESCRIPTION")
                                            + Constants.TRANSLATION_CDRS_KEY_SEPARATOR + lang);
                        else if (resultSet.getString("DESCRIPTION") != null
                                && !resultSet.getString("DESCRIPTION").isEmpty()) {
                            description = resultSet.getString("DESCRIPTION");
                        } else {
                            description = "";
                        }
 
                        if (GetCDRsByDateLand.usageHistoryTranslationMapping
                                .containsKey(GetCDRsByDateLand.cdrsColumnMapping.get("MEASURE_ID")
                                        + Constants.TRANSLATION_CDRS_KEY_SEPARATOR + resultSet.getString("TYPE")
                                        + Constants.TRANSLATION_CDRS_KEY_SEPARATOR + resultSet.getString("MEASURE_ID")
                                        + Constants.TRANSLATION_CDRS_KEY_SEPARATOR + lang))
                            measureid = GetCDRsByDateLand.usageHistoryTranslationMapping
                                    .get(GetCDRsByDateLand.cdrsColumnMapping.get("MEASURE_ID")
                                            + Constants.TRANSLATION_CDRS_KEY_SEPARATOR + resultSet.getString("TYPE")
                                            + Constants.TRANSLATION_CDRS_KEY_SEPARATOR
                                            + resultSet.getString("MEASURE_ID")
                                            + Constants.TRANSLATION_CDRS_KEY_SEPARATOR + lang);
                        else
                            measureid = resultSet.getString("MEASURE_ID");
                        endingbalance = resultSet.getString("ENDING_BALANCE");
                        amount = resultSet.getString("AMOUNT");
                        NumberFormat df = new DecimalFormat("#0.00");
                        df.setRoundingMode(RoundingMode.FLOOR);
                        logger.info(token + endingbalance);
                        if (endingbalance != null && (endingbalance.equals("0") || endingbalance.equals(""))) {
                            endingbalance = resultSet.getString("ENDING_BALANCE");
                        } else if (endingbalance != null) {
                            double blncnumber = Double.parseDouble(endingbalance);
                            endingbalance = df.format(blncnumber);
                        }
                        if (amount != null && (amount.equals("0") || amount.equals(""))) {
                            endingbalance = resultSet.getString("ENDING_BALANCE");
                        } else if (amount != null) {
                            double Snumber = Helper.round(Double.parseDouble(amount),2);
                            
                            logger.info(token + "AZN : " + measureid);
                            if (measureid != null && measureid.equalsIgnoreCase("AZN"))
                                amount = df.format(Snumber);
                            else
                                amount = Double.toString(Snumber);
                        }
                        if (clarification == null && endingbalance == null) {
                            list.add(new CDRsOperationDetails(resultSet.getString("PRI_IDENTITY"), datetimefinal,
                                    transactionType, description, amount, "", "", measureid));
                        } else if (endingbalance == null) {
                            list.add(new CDRsOperationDetails(resultSet.getString("PRI_IDENTITY"), datetimefinal,
                                    transactionType, description, amount, "", clarification, measureid));
                        } else if (clarification == null) {
                            list.add(new CDRsOperationDetails(resultSet.getString("PRI_IDENTITY"), datetimefinal,
                                    transactionType, description, amount,
                                    ConversionUtilities.numberFormattor(resultSet.getString("ENDING_BALANCE")), "",
                                    measureid));
                        } else {
                            list.add(new CDRsOperationDetails(resultSet.getString("PRI_IDENTITY"), datetimefinal,
                                    transactionType, description, amount,
                                    ConversionUtilities.numberFormattor(resultSet.getString("ENDING_BALANCE")),
                                    clarification, measureid));
                        }
                    }
                }
            } 
            else
                logger.info(token + "MSISDN-" + msisdn + "-CustomerID From DB-" + customerId.getCrmcustomerId()
                        + "-CustomerID From API-" + customerIdAPI + "-ID not matched.");
            CDRsOperationHistoryRequestResponse rep11 = new CDRsOperationHistoryRequestResponse();
 
            Collections.sort(list, new Comparator<CDRsOperationDetails>() {
                public int compare(CDRsOperationDetails o1, CDRsOperationDetails o2) {
                    Date d1 = null;
                    Date d2 = null;
                    if (o1.getDate() == null || o2.getDate() == null)
                        return 0;
                    try {
                        d1 = new SimpleDateFormat("dd/MM/yy HH:mm:ss").parse(o1.getDate());
                        d2 = new SimpleDateFormat("dd/MM/yy HH:mm:ss").parse(o2.getDate());
                    } catch (ParseException e) {
                        logger.info(token + Helper.GetException(e));
                    }
                    return -1 * (d1.compareTo(d2));
                }
            });
 
            rep11.setRecords(list);
            rep11.setReturnCode(ResponseCodes.SUCESS_CODE_200);
            rep11.setReturnMsg(ResponseCodes.SUCESS_DES_200);
            logs.setResponseCode(rep11.getReturnCode());
            logs.setResponseDescription(rep11.getReturnMsg());
            logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
            logs.updateLog(logs);
            return rep11;
        } catch (Exception ex) {
            logger.info(token + Helper.GetException(ex));
        } finally {
            try {
                if (resultSet != null)
                    resultSet.close();
                if (statement != null)
                    statement.close();
            } catch (Exception e) {
                logger.info(token + Helper.GetException(e));
            }
        }
        CDRsOperationHistoryRequestResponse resp = new CDRsOperationHistoryRequestResponse();
        resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
        resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
        logs.setResponseCode(resp.getReturnCode());
        logs.setResponseDescription(resp.getReturnMsg());
        logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
        logs.updateLog(logs);
        return resp;
    }
 
    public static void main(String[] args) throws ParseException {
        Date date = new SimpleDateFormat("yyyy-MM-dd").parse("2020-01-24");
        Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse("2022-01-24");
        System.out.println(date.compareTo(date1));
    }
 
}