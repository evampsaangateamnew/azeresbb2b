package com.evampsaanga.b2b.azerfon.getcdrsoperationhistory;

import java.text.ParseException;

public class CDRsOperationDetails {
	private String primaryIdentity = "";
	private String date = "";
	private String transactionType = "";
	private String description = "";
	private String amount = "";
	private String currency = "";
	private String endingBalance = "";
	private String clarification = "";

	public CDRsOperationDetails(String primaryIdentity, String date, String transactionType, String description,
			String amount, String endingBalance, String clarification, String measureid) throws ParseException {
		super();
		this.primaryIdentity = primaryIdentity;
		this.date = date;
		this.transactionType = transactionType;
		this.description = description;
		this.amount = amount;
		this.endingBalance = endingBalance;
		this.clarification = clarification;
		this.currency = measureid;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getPrimaryIdentity() {
		return primaryIdentity;
	}

	public void setPrimaryIdentity(String primaryIdentity) {
		this.primaryIdentity = primaryIdentity;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getEndingBalance() {
		return endingBalance;
	}

	public void setEndingBalance(String endingBalance) {
		this.endingBalance = endingBalance;
	}

	public String getClarification() {
		return clarification;
	}

	public void setClarification(String clarification) {
		this.clarification = clarification;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}
}
