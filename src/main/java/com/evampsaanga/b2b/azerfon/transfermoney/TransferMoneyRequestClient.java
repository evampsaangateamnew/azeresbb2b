package com.evampsaanga.b2b.azerfon.transfermoney;

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;

public class TransferMoneyRequestClient extends BaseRequest {
	private String transferee = "";
	private String amount = "";

	public String getTransferee() {
		return transferee;
	}

	public void setTransferee(String transferee) {
		this.transferee = transferee;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}
}
