package com.evampsaanga.b2b.azerfon.transfermoney;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.amqimplementationsesb.LogsType;
import com.evampsaanga.b2b.azerfon.getsubscriber.CRMSubscriberService;
import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.ThirdPartyNames;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Decrypter;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.evampsaanga.b2b.developer.utils.SOAPLoggingHandler;
import com.huawei.bme.cbsinterface.arservices.TransferBalanceRequest;
import com.huawei.bme.cbsinterface.arservices.TransferBalanceRequest.TransfereeAcct;
import com.huawei.bme.cbsinterface.arservices.TransferBalanceRequest.TransferorAcct;
import com.huawei.bme.cbsinterface.arservices.TransferBalanceRequestMsg;
import com.huawei.bme.cbsinterface.arservices.TransferBalanceResultMsg;
import com.huawei.bme.cbsinterface.cbscommon.OperatorInfo;
import com.huawei.bme.cbsinterface.cbscommon.OwnershipInfo;
import com.huawei.bme.cbsinterface.cbscommon.SecurityInfo;
import com.huawei.crm.azerfon.bss.query.GetSubscriberResponse;
import com.ngbss.evampsaanga.services.ThirdPartyCall;

@Path("/azerfon")
public class TransferMoneyLand {
	public static final Logger logger = Logger.getLogger("azerfon-esb");

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public TransferMoneyResponseClient Get(@Header("credentials") String credential,
			@Header("Content-Type") String contentType, @Body() String requestBody) {
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.TRANSFER_MONEY_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.TRANSFER_MONEY);
		logs.setTableType(LogsType.TransferMoney);

		String token = "";
		String TrnsactionName = Transactions.TRANSFER_MONEY_TRANSACTION_NAME;

		try {

			token = Helper.retrieveToken(TrnsactionName, Helper.getValueFromJSON(requestBody, "msisdn"));
			logger.info(token + "Request Landed on TransferMoneyLand:" + requestBody);
			TransferMoneyRequestClient cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, TransferMoneyRequestClient.class);
				if (cclient != null) {
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
				}
			} catch (Exception ex) {
				logger.info(token + Helper.GetException(ex));
				TransferMoneyResponseClient resp = new TransferMoneyResponseClient();
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(ResponseCodes.ERROR_400_CODE);
				logs.setResponseDescription(ResponseCodes.ERROR_400);
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.info(token + Helper.GetException(ex));
				}
				if (credentials == null) {
					TransferMoneyResponseClient resp = new TransferMoneyResponseClient();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(ResponseCodes.ERROR_401_CODE);
					logs.setResponseDescription(ResponseCodes.ERROR_401);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						TransferMoneyResponseClient res = new TransferMoneyResponseClient();
						res.setReturnCode(ResponseCodes.ERROR_400);
						res.setReturnMsg(verification);
						logs.setResponseCode(ResponseCodes.ERROR_400);
						logs.setResponseDescription(verification);
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return res;
					}
				} else {
					TransferMoneyResponseClient resp = new TransferMoneyResponseClient();
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode((ResponseCodes.ERROR_MSISDN_CODE));
					logs.setResponseDescription(ResponseCodes.ERROR_MSISDN);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					logger.info(token + "Test if");
					TransferMoneyResponseClient resp = new TransferMoneyResponseClient();

					if (cclient.getTransferee().equalsIgnoreCase(cclient.getmsisdn())) {
						resp.setReturnCode(ResponseCodes.TRANSFERER_AND_TRANSFREE_SHOUULD_BE_DIFFERENT_CODE);
						resp.setReturnMsg(ResponseCodes.TRANSFERER_AND_TRANSFREE_SHOUULD_BE_DIFFERENT_DES);
						logs.setResponseDescription(ResponseCodes.TRANSFERER_AND_TRANSFREE_SHOUULD_BE_DIFFERENT_CODE);
						logs.setResponseCode(ResponseCodes.TRANSFERER_AND_TRANSFREE_SHOUULD_BE_DIFFERENT_DES);
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.setReceiverMsisdn(cclient.getTransferee());
						logs.setAmount(cclient.getAmount());
						logs.updateLog(logs);
						return resp;
					}

					try {
						if (Double.parseDouble(cclient.getAmount()) <= 0) {
							logger.info(token + "Try if");
							resp.setReturnCode(ResponseCodes.INVALID_AMOUNT_ERROR_CODE);
							resp.setReturnMsg(ResponseCodes.INVALID_AMOUNT_ERROR_DES);
							logs.setResponseDescription(ResponseCodes.INVALID_AMOUNT_ERROR_DES);
							logs.setResponseCode(ResponseCodes.INVALID_AMOUNT_ERROR_CODE);
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.setReceiverMsisdn(cclient.getTransferee());
							logs.setAmount(cclient.getAmount());
							logs.updateLog(logs);
							return resp;
						}
					} catch (Exception ex) {
						resp.setReturnCode(ResponseCodes.INVALID_AMOUNT_ERROR_CODE);
						resp.setReturnMsg(ResponseCodes.INVALID_AMOUNT_ERROR_DES);
						logs.setResponseDescription(ResponseCodes.INVALID_AMOUNT_ERROR_DES);
						logs.setResponseCode(ResponseCodes.INVALID_AMOUNT_ERROR_CODE);
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.setReceiverMsisdn(cclient.getTransferee());
						logs.setAmount(cclient.getAmount());
						logs.updateLog(logs);
						return resp;
					}
					TransferBalanceResultMsg transferBalanceResponse = null;
					try {
						CRMSubscriberService service = new CRMSubscriberService();
						GetSubscriberResponse custInf = service.GetSubscriberRequest(cclient.getmsisdn());
						if (custInf.getResponseHeader().getRetCode().equals(Constants.CRMSUBACCESSCODE)) {
							String subscriberType = ConfigurationManager
									.getConfigurationFromCache(ConfigurationManager.MAPPING_HLR_SUBSCRIBER_TYPE
											+ custInf.getGetSubscriberBody().getSubscriberType());
							if (subscriberType.equalsIgnoreCase(Constants.USER_TYPE_POSTPAID)) {
								resp.setReturnCode(ResponseCodes.MONEY_TRANSFER_TO_POSTPAID_CODE);
								resp.setReturnMsg(ResponseCodes.MONEY_TRANSFER_TO_POSTPAID_DES);
								logs.setResponseCode(resp.getReturnCode());
								logs.setResponseDescription(resp.getReturnMsg());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
								return resp;
							}
						} else {
							resp.setReturnCode(custInf.getResponseHeader().getRetCode());
							resp.setReturnMsg(custInf.getResponseHeader().getRetMsg());
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
						transferBalanceResponse = transferBalance(cclient.getmsisdn(), cclient.getTransferee(),
								(new Double(Double.parseDouble(cclient.getAmount()) * Constants.MONEY_DIVIDEND)
										.longValue()) + "");
						if (transferBalanceResponse.getResultHeader().getResultCode().equals("0")) {
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
							logs.setResponseDescription(ResponseCodes.SUCESS_DES_200);
							logs.setResponseCode(ResponseCodes.SUCESS_CODE_200);
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.setReceiverMsisdn(cclient.getTransferee());
							logs.setAmount(cclient.getAmount());
							try {
								resp.setNewBalance(
										Helper.getBakcellMoney(transferBalanceResponse.getTransferBalanceResult()
												.getTransferor().getBalanceChgInfo().get(0).getNewBalanceAmt()));
								resp.setOldBalance(
										Helper.getBakcellMoney(transferBalanceResponse.getTransferBalanceResult()
												.getTransferor().getBalanceChgInfo().get(0).getOldBalanceAmt()));
							} catch (Exception ex) {
								logger.error(Helper.GetException(ex));
							}
							logs.updateLog(logs);
							return resp;
						} else {
							resp.setReturnCode(transferBalanceResponse.getResultHeader().getResultCode());
							resp.setReturnMsg(transferBalanceResponse.getResultHeader().getResultDesc());
							logs.setResponseDescription(transferBalanceResponse.getResultHeader().getResultDesc());
							logs.setResponseCode(transferBalanceResponse.getResultHeader().getResultCode());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.setReceiverMsisdn(cclient.getTransferee());
							logs.setAmount(cclient.getAmount());
							logs.updateLog(logs);
							return resp;
						}
					} catch (Exception ex) {
						logger.error(Helper.GetException(ex));
					}
				} else {
					TransferMoneyResponseClient resp = new TransferMoneyResponseClient();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseDescription(ResponseCodes.ERROR_401);
					logs.setResponseCode(ResponseCodes.ERROR_401_CODE);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.setReceiverMsisdn(cclient.getTransferee());
					logs.setAmount(cclient.getAmount());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		TransferMoneyResponseClient resp = new TransferMoneyResponseClient();
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseDescription(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.setResponseCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
		return resp;
	}

	public TransferBalanceResultMsg transferBalance(String tranferor, String transferee, String amount) {
		TransferBalanceRequestMsg transferBalanceRequestMsg = new TransferBalanceRequestMsg();
		transferBalanceRequestMsg.setRequestHeader(getLoanLogRequestHeader());
		TransferBalanceRequest tbReqValue = new TransferBalanceRequest();
		TransfereeAcct trasfeeaccuontvalue = new TransfereeAcct();
		trasfeeaccuontvalue.setPrimaryIdentity(transferee);
		TransferorAcct value1 = new TransferorAcct();
		value1.setPrimaryIdentity(tranferor);
		tbReqValue.setTransferType(Constants.MONEY_TRANSFER_TYPE);
		tbReqValue.setTransferorAcct(value1);
		tbReqValue.setTransfereeAcct(trasfeeaccuontvalue);
		tbReqValue.setSrcBalanceType(Constants.MONEY_TRANSFER_SRC_BALANCE_TYPE);
		tbReqValue.setDestBalanceType(Constants.MONEY_TRANSFER_DEST_BALANCE_TYPE);
		tbReqValue.setTransferAmount(amount);
		transferBalanceRequestMsg.setTransferBalanceRequest(tbReqValue);
//		TransferBalanceResultMsg TBalance = CBSARService.getInstance().transferBalance(transferBalanceRequestMsg);
		TransferBalanceResultMsg TBalance = ThirdPartyCall.getTransferBalance(transferBalanceRequestMsg);
		return TBalance;
	}

	public com.huawei.bme.cbsinterface.cbscommon.RequestHeader getLoanLogRequestHeader() {
		com.huawei.bme.cbsinterface.cbscommon.RequestHeader loanRequestHeader = new com.huawei.bme.cbsinterface.cbscommon.RequestHeader();
		try {
			loanRequestHeader.setVersion(Constants.MONEY_TRANSFER_VERSION);
			loanRequestHeader.setBusinessCode(Constants.MONEY_TRANSFER_BUSINESS_CODE);
			OwnershipInfo ownershipinf = new OwnershipInfo();
			ownershipinf.setBEID(Constants.MONEY_TRANSFER_BEID);
			ownershipinf.setBRID(Constants.MONEY_TRANSFER_BRID);
			loanRequestHeader.setOwnershipInfo(ownershipinf);
			SecurityInfo value = new SecurityInfo();
			value.setLoginSystemCode(ConfigurationManager.getConfigurationFromCache("cbs.username"));
			value.setPassword(ConfigurationManager.getConfigurationFromCache("cbs.password"));
			loanRequestHeader.setAccessSecurity(value);
			OperatorInfo opInfo = new OperatorInfo();
			opInfo.setOperatorID(Constants.MONEY_TRANSFER_OPERATORID);
			loanRequestHeader.setOperatorInfo(opInfo);
			loanRequestHeader.setMessageSeq(Helper.generateTransactionID());
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		return loanRequestHeader;
	}
}