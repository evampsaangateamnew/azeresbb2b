package com.evampsaanga.b2b.azerfon.changepaymentrelation;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.amqimplementationsesb.LogsType;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.ThirdPartyNames;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Decrypter;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.evampsaanga.b2b.developer.utils.SOAPLoggingHandler;


@Path("/bakcell/")
public class ChangePaymentRelationRequestLand {
	
	public static final Logger logger = Logger.getLogger("azerfon-esb");

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ChangePaymentRelationResponse Get(@Header("credentials") String credential, @Body() String requestBody) {
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.PAYMENT_LOG);
		logs.setThirdPartyName(ThirdPartyNames.PAYMENT_LOG);
		logs.setTableType(LogsType.AppFaq);
		ChangePaymentRelationResponse resp=new ChangePaymentRelationResponse();
		try {
			logger.info("Request Landed on ChangePaymentRelationRequestLand:" + requestBody);
			ChangePaymentRelationRequest cclient = new ChangePaymentRelationRequest();
			
			try {
				cclient = Helper.JsonToObject(requestBody, ChangePaymentRelationRequest.class);
				if (cclient != null) {
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());
					logs.setLang(cclient.getLang());
				}
				
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				}
				if (credentials == null) {
					
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						ChangePaymentRelationResponse res = new ChangePaymentRelationResponse();
						res.setReturnCode(ResponseCodes.ERROR_400);
						res.setReturnMsg(verification);
						return res;
					}
				} else {

					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					//
					if(cclient.getGroupType().equalsIgnoreCase("Pay By Subs") || cclient.getGroupType().equalsIgnoreCase("Pay By Sub"))
					{
						if(cclient.getUsers().size()==1)
						{
							
						}
						else
						{
							
						}
					}
					else if(cclient.getGroupType().equalsIgnoreCase("Part Pay") || cclient.getGroupType().equalsIgnoreCase("Full Pay"))
					{
						if(cclient.getUsers().size()==1)
						{
							resp.setMinimumLimit("0");
							resp.setMaximumLimi("100");
						}
						else
						{
							
						}
					}
					
					return resp;
				} else {
					
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		return resp;
	
	}
}
