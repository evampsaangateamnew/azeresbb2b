package com.evampsaanga.b2b.azerfon.getappfaq;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "GetAppFaqRequest")
public class GetAppFaqRequest extends BaseRequest {
}
