package com.evampsaanga.b2b.azerfon.mnpgetsubscriber;

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;

public class MNPGetSubscriberRequestClient extends BaseRequest {
	private String passportNumber = "";
	private String customerId;
	private String accountId;

	/**
	 * @return the passportNumber
	 */
	public String getPassportNumber() {
		return passportNumber;
	}

	/**
	 * @param passportNumber
	 *            the passportNumber to set
	 */
	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	
	
	
}
