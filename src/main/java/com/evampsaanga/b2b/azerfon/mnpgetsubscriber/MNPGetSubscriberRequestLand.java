package com.evampsaanga.b2b.azerfon.mnpgetsubscriber;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.azerfon.validator.RequestValidator;
import com.evampsaanga.b2b.azerfon.validator.ResponseValidator;
import com.evampsaanga.b2b.azerfon.validator.ValidatorService;
import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.huawei.bss.soaif._interface.common.ReqHeader;
import com.mnp.huawei.crm.query.GetCustomerDataListIn;
import com.mnp.huawei.crm.query.GetCustomerDataListRequest;
import com.mnp.huawei.crm.query.GetCustomerDataListResponse;


@Path("/azerfon")
public class MNPGetSubscriberRequestLand {
	public static final Logger logger = Logger.getLogger("azerfon-esb");

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public MNPGetSubscriberResponseClient Get(@Header("credentials") String credential, @Body() String requestBody) {

		Logs logs = new Logs();

		try {

			// ------------------------- Setting Transaction Name ---------------------- //

			MNPGetSubscriberRequestClient cclient = null;
			MNPGetSubscriberResponseClient resp = new MNPGetSubscriberResponseClient();

			String transactionName = Transactions.MNP_GET_SUBSCRIBER;
			String token = Helper.retrieveToken(transactionName, Helper.getValueFromJSON(requestBody, "msisdn"));

			Constants.logger.info(token + "-Request Landed In-" + transactionName);
			Constants.logger.info(token + "-Request Data-" + requestBody);

			// ----------------------------- Validating Text --------------------------- //

			cclient = Helper.JsonToObject(requestBody, MNPGetSubscriberRequestClient.class);

			ValidatorService validatorService = new ValidatorService();
			ResponseValidator responseValidator = new ResponseValidator();
			RequestValidator requestValidator = new RequestValidator();

			requestValidator.setChannel(cclient.getChannel());
			requestValidator.setiP(cclient.getiP());
			requestValidator.setIsB2B(cclient.getIsB2B());
			requestValidator.setLang(cclient.getLang());
			requestValidator.setMsisdn(cclient.getmsisdn());
			responseValidator = validatorService.processValidation(requestValidator, credential, token);

			if (responseValidator.getResponseCode().equals("00")) {

				GetCustomerDataListRequest getCustomerDataListRequest = new GetCustomerDataListRequest();
				getCustomerDataListRequest.setRequestHeader(getReqHeaderForMNP());
				GetCustomerDataListIn getCustomerDataListIn = new GetCustomerDataListIn();
				getCustomerDataListIn.setServiceNumber(cclient.getmsisdn());
				getCustomerDataListRequest.setGetCustomerDataListBody(getCustomerDataListIn);

				
//				com.huawei.crm.query.order.query.GetCustomerDataListRequest getCustomerDataListRequestMsgReq = new com.huawei.crm.query.order.query.GetCustomerDataListRequest();
//				getCustomerDataListRequestMsgReq.setRequestHeader(QueryOrderService.getQueryOderHeader());
//				com.huawei.crm.query.order.query.GetCustomerDataListIn getCustomerDataListIn2 = new com.huawei.crm.query.order.query.GetCustomerDataListIn();
//				getCustomerDataListIn2.setServiceNumber(cclient.getmsisdn());
//				getCustomerDataListRequestMsgReq.setGetCustomerDataListBody(getCustomerDataListIn2 );
//				com.huawei.crm.query.order.query.GetCustomerDataListResponse getCustomerDataListResponse = QueryOrderService.getInstance().getCustomerDataList(getCustomerDataListRequestMsgReq);
				
				GetCustomerDataListResponse getCustomerDataListResponse = new GetCustomerDataListResponse();
				getCustomerDataListResponse = OrderQueryService.getInstance().getCustomerDataList(getCustomerDataListRequest);

				if (getCustomerDataListResponse.getResponseHeader().getRetCode().equals("0000")) {

					for (int i = 0; i < getCustomerDataListResponse.getGetCustomerDataListBody().getCustomerInfoList()
							.get(0).getExtParamList().getParameterInfo().size(); i++) {

						if (getCustomerDataListResponse.getGetCustomerDataListBody().getCustomerInfoList().get(0)
								.getExtParamList().getParameterInfo().get(i).getParamName().equals("pinCode")) {

							if (cclient.getPassportNumber().toUpperCase()
									.equals(getCustomerDataListResponse.getGetCustomerDataListBody()
											.getCustomerInfoList().get(0).getExtParamList().getParameterInfo().get(i)
											.getParamValue())) {

								resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
								resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
								logs.setResponseCode(resp.getReturnCode());
								logs.setResponseDescription(resp.getReturnMsg());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
								return resp;

							} else {

								resp.setReturnCode(ResponseCodes.MISSING_PARAMETER_CODE);
								resp.setReturnMsg(
										ResponseCodes.MISSING_PARAMETER_DES + " " + "In valid passport number");
								logs.setResponseCode(resp.getReturnCode());
								logs.setResponseDescription(resp.getReturnMsg());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
								return resp;
							}
						}
					}

				} else {

					resp.setReturnCode(getCustomerDataListResponse.getResponseHeader().getRetCode());
					resp.setReturnMsg(getCustomerDataListResponse.getResponseHeader().getRetMsg());
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}

			} else {

				resp.setReturnCode(responseValidator.getResponseCode());
				resp.setReturnMsg(responseValidator.getResponseDescription());
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
			}

			Constants.logger
					.info(token + "-Response Returned From-" + transactionName + "-" + Helper.ObjectToJson(resp));

			return resp;

		} catch (Exception ex) {

			Constants.logger.info(Helper.GetException(ex));
			MNPGetSubscriberResponseClient resp = new MNPGetSubscriberResponseClient();
			resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
			resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);

			return resp;
		}
	}
	public static com.mnp.huawei.crm.basetype.RequestHeader getReqHeaderForMNP() {
		com.mnp.huawei.crm.basetype.RequestHeader reqH = new  com.mnp.huawei.crm.basetype.RequestHeader();
		reqH.setVersion("1");
		reqH.setChannelId("3");
		reqH.setTechnicalChannelId("53");
		reqH.setAccessUser("dpc");
		reqH.setAccessPwd("Abc1234%");
		reqH.setTransactionId(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
		reqH.setTenantId("101");
		reqH.setTestFlag("0");
		reqH.setLanguage("2002");
		
		return reqH;
	}

	public ReqHeader getRequestHeaderForMNP() {
		ReqHeader reqheaderForMNP = new ReqHeader();
		reqheaderForMNP.setAccessUser(ConfigurationManager.getConfigurationFromCache("mnp.sub.AccessUser"));
		reqheaderForMNP.setAccessPwd(ConfigurationManager.getConfigurationFromCache("mnp.sub.AccessPwd"));
		reqheaderForMNP.setChannelId(ConfigurationManager.getConfigurationFromCache("mnp.sub.channelId"));
		reqheaderForMNP.setTransactionId(Helper.generateTransactionID());
		return reqheaderForMNP;
	}
}
