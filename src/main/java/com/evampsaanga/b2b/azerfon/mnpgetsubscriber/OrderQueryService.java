package com.evampsaanga.b2b.azerfon.mnpgetsubscriber;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.evampsaanga.b2b.developer.utils.SoapHandlerService;
import com.mnp.huawei.crm.service.HuaweiCRMPortType;
import com.mnp.huawei.crm.service.OrderQuery;

public class OrderQueryService {
	private static HuaweiCRMPortType crmPortType = null;

	private OrderQueryService() {
	}

	public static synchronized HuaweiCRMPortType getInstance() {
		if (crmPortType == null) {
			crmPortType = new OrderQuery().getHuaweiCRMPort();
			SoapHandlerService.configureBinding(crmPortType);
		}
		return crmPortType;
	}

	// TODO
	public static com.huawei.crm.basetype.RequestHeader getReqHeaderForGETNetworkSetting() {
		com.huawei.crm.basetype.RequestHeader reqH = new com.huawei.crm.basetype.RequestHeader();
		reqH.setVersion("1");
		reqH.setChannelId("3");
		reqH.setTechnicalChannelId("53");
		reqH.setAccessUser("ecare");
		reqH.setAccessPwd("r8q0a5WwGNboj9I35XzNcQ==");
		reqH.setTransactionId(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
		reqH.setTenantId("101");
		reqH.setTestFlag("0");
		reqH.setLanguage("2002");

		return reqH;
	}

	public static com.huawei.crm.basetype.RequestHeader getReqHeaderForGETMNP() {
		com.huawei.crm.basetype.RequestHeader reqH = new com.huawei.crm.basetype.RequestHeader();
		reqH.setVersion("1");
		reqH.setChannelId("42");
		reqH.setTechnicalChannelId("53");
		reqH.setAccessUser("dpc");
		reqH.setAccessPwd("AAss11!!");
		reqH.setTransactionId(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
		reqH.setTenantId("101");
		// reqH.setTestFlag("0");
		reqH.setLanguage("2002");

		return reqH;
	}
}
