package com.evampsaanga.b2b.azerfon.broadcastsms;

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;
/**
 * Request Container For Broadcast Sms
 * @author Aqeel Abbas
 *
 */
public class BroadcastSMSRequest extends BaseRequest{
	private String recieverMsisdn = "";
	private String textmsg = "";
	private String msgLang;
	private String orderKey;

	public String getRecieverMsisdn() {
		return recieverMsisdn;
	}

	public void setRecieverMsisdn(String recieverMsisdn) {
		this.recieverMsisdn = recieverMsisdn;
	}

	public String getTextmsg() {
		return textmsg;
	}

	public void setTextmsg(String textmsg) {
		this.textmsg = textmsg;
	}

	public String getMsgLang() {
		return msgLang;
	}

	public void setMsgLang(String msgLang) {
		this.msgLang = msgLang;
	}

	public String getOrderKey() {
		return orderKey;
	}

	public void setOrderKey(String orderKey) {
		this.orderKey = orderKey;
	}

}
