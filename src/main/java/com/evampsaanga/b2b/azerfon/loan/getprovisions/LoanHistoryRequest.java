/**
 * 
 */
package com.evampsaanga.b2b.azerfon.loan.getprovisions;

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;

/**
 * @author Evamp & Saanga
 *
 */
public class LoanHistoryRequest extends BaseRequest {

	private String startDate;
	private String endDate;

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

//	@Override
//	public String toString() {
//		return "LoanHistoryRequest [startDate=" + startDate + ", endDate=" + endDate + ", getLang()=" + getLang()
//				+ ", getiP()=" + getiP() + ", getChannel()=" + getChannel() + ", getMsisdn()=" + getmsisdn() + "]";
//	}

}
