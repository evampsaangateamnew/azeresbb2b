package com.evampsaanga.b2b.azerfon.loan.getprovisions;

public class Provisions {

	private Integer debtId;
	private String subscriberId;
	private Integer initialAmount;
	private Double serviceFee;
	private Double currentAmount;
	private String creditDate;
	private String smsNotificationContent;

	public Integer getDebtId() {
		return debtId;
	}

	public void setDebtId(Integer debtId) {
		this.debtId = debtId;
	}

	public String getSubscriberId() {
		return subscriberId;
	}

	public void setSubscriberId(String subscriberId) {
		this.subscriberId = subscriberId;
	}

	public Integer getInitialAmount() {
		return initialAmount;
	}

	public void setInitialAmount(Integer initialAmount) {
		this.initialAmount = initialAmount;
	}

	public Double getServiceFee() {
		return serviceFee;
	}

	public void setServiceFee(Double serviceFee) {
		this.serviceFee = serviceFee;
	}

	public Double getCurrentAmount() {
		return currentAmount;
	}

	public void setCurrentAmount(Double currentAmount) {
		this.currentAmount = currentAmount;
	}

	public String getCreditDate() {
		return creditDate;
	}

	public void setCreditDate(String creditDate) {
		this.creditDate = creditDate;
	}

	public String getSmsNotificationContent() {
		return smsNotificationContent;
	}

	public void setSmsNotificationContent(String smsNotificationContent) {
		this.smsNotificationContent = smsNotificationContent;
	}

	@Override
	public String toString() {
		return "Provisions [debtId=" + debtId + ", subscriberId=" + subscriberId + ", initialAmount=" + initialAmount
				+ ", serviceFee=" + serviceFee + ", currentAmount=" + currentAmount + ", creditDate=" + creditDate
				+ ", smsNotificationContent=" + smsNotificationContent + "]";
	}

}
