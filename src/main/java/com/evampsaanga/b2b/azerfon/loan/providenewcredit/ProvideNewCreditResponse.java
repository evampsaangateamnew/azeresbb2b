package com.evampsaanga.b2b.azerfon.loan.providenewcredit;

import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProvideNewCreditResponse extends BaseResponse {

}