package com.evampsaanga.b2b.azerfon.loan.getloan;

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;

public class GetLoanRequest extends BaseRequest {

	private String friendMsisdn;

	public String getFriendMsisdn() {
		return friendMsisdn;
	}

	public void setFriendMsisdn(String friendMsisdn) {
		this.friendMsisdn = friendMsisdn;
	}

	@Override
	public String toString() {
		return "GetLoanRequest [friendMsisdn=" + friendMsisdn + "]";
	}

}
