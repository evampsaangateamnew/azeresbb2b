package com.evampsaanga.b2b.azerfon.loan.getloan;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.azerfon.thirdpartycall.AzerfonThirdPartyCalls;
import com.evampsaanga.b2b.azerfon.validator.RequestValidator;
import com.evampsaanga.b2b.azerfon.validator.ResponseValidator;
import com.evampsaanga.b2b.azerfon.validator.ValidatorService;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Helper;

@Path("/azerfon/")
public class GetLoanController {

	public static final Logger logger = Logger.getLogger("azerfon-esb");

	@POST
	@Path("/getloan")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetLoanResponse Get(@Header("credentials") String credential, @Body() String requestBody) {

		Logs logs = new Logs();

		try {

			// ------------------------- Setting Transaction Name
			// ---------------------- //

			GetLoanResponse getLoanResponse = new GetLoanResponse();
			GetLoanRequest cclient = null;

			String transactionName = Transactions.LOAN_GET_LOAN;
			String token = Helper.retrieveToken(transactionName, Helper.getValueFromJSON(requestBody, "msisdn"));

			Constants.logger.info(token + "-Request Landed In-" + transactionName);
			Constants.logger.info(token + "-Request Data-" + requestBody);

			// ----------------------------- Validating Text
			// --------------------------- //

			cclient = Helper.JsonToObject(requestBody, GetLoanRequest.class);

			ValidatorService validatorService = new ValidatorService();
			ResponseValidator responseValidator = new ResponseValidator();
			RequestValidator requestValidator = new RequestValidator();

			requestValidator.setChannel(cclient.getChannel());
			requestValidator.setiP(cclient.getiP());
			requestValidator.setIsB2B(cclient.getIsB2B());
			requestValidator.setLang(cclient.getLang());
			requestValidator.setMsisdn(cclient.getmsisdn());
			responseValidator = validatorService.processValidation(requestValidator, credential, token);

			if (responseValidator.getResponseCode().equals("00")) {

				GetLoanRequest getLoanRequest = new GetLoanRequest();

				getLoanRequest = Helper.JsonToObject(requestBody, GetLoanRequest.class);

				getLoanResponse = AzerfonThirdPartyCalls.getLoan(token, transactionName, getLoanRequest,
						getLoanResponse);
				if (getLoanResponse.getReturnCode().equalsIgnoreCase(ResponseCodes.SUCESS_CODE_200)) {
					/**
					 * skipped on 05-08-2020. Now onwards, Simberella will
					 * handle charging at their end.
					 */
					// AzerfonThirdPartyCalls.responseChargecustomer(token,
					// getLoanRequest.getmsisdn(), "500328");
				}

			} else {

				getLoanResponse.setReturnCode(responseValidator.getResponseCode());
				getLoanResponse.setReturnMsg(responseValidator.getResponseDescription());
				logs.setResponseCode(getLoanResponse.getReturnCode());
				logs.setResponseDescription(getLoanResponse.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);

			}

			Constants.logger.info(
					token + "-Response Returned From-" + transactionName + "-" + Helper.ObjectToJson(getLoanResponse));

			return getLoanResponse;

		} catch (Exception ex) {

			Constants.logger.info(Helper.GetException(ex));
			GetLoanResponse resp = new GetLoanResponse();
			resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
			resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);

			return resp;
		}
	}
}
