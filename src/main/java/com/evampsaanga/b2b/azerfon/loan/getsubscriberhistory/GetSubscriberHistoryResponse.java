
package com.evampsaanga.b2b.azerfon.loan.getsubscriberhistory;

import java.util.List;

import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetSubscriberHistoryResponse extends BaseResponse {

	private List<LoanRequest> loanRequest;
	private List<LoanPayment> loanPayment;

	public List<LoanRequest> getLoanRequest() {
		return loanRequest;
	}

	public void setLoanRequest(List<LoanRequest> loanRequest) {
		this.loanRequest = loanRequest;
	}

	public List<LoanPayment> getLoanPayment() {
		return loanPayment;
	}

	public void setLoanPayment(List<LoanPayment> loanPayment) {
		this.loanPayment = loanPayment;
	}

	@Override
	public String toString() {
		return "GetSubscriberHistoryResponse [loanRequest=" + loanRequest + ", loanPayment=" + loanPayment + "]";
	}

}
