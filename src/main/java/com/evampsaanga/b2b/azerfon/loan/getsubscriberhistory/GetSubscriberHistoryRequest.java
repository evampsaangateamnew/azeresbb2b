package com.evampsaanga.b2b.azerfon.loan.getsubscriberhistory;

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;

public class GetSubscriberHistoryRequest extends BaseRequest {

	private String startDate;
	private String endDate;
	private String useCase;

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getUseCase() {
		return useCase;
	}

	public void setUseCase(String useCase) {
		this.useCase = useCase;
	}

	@Override
	public String toString() {
		return "GetSubscriberHistoryRequest [startDate=" + startDate + ", endDate=" + endDate + ", useCase=" + useCase
				+ "]";
	}

}
