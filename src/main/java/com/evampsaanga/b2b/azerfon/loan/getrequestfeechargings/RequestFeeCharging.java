package com.evampsaanga.b2b.azerfon.loan.getrequestfeechargings;

public class RequestFeeCharging {

	private Integer debtId;
	private Double requestFee;

	public Integer getDebtId() {
		return debtId;
	}

	public void setDebtId(Integer debtId) {
		this.debtId = debtId;
	}

	public Double getRequestFee() {
		return requestFee;
	}

	public void setRequestFee(Double requestFee) {
		this.requestFee = requestFee;
	}

	@Override
	public String toString() {
		return "RequestFeeCharging [debtId=" + debtId + ", requestFee=" + requestFee + "]";
	}

}
