package com.evampsaanga.b2b.azerfon.loan.getsubscriberhistory;

public class LoanPayment {

	private String loanID;
	private String dateTime;
	private String amount;

	public String getLoanID() {
		return loanID;
	}

	public void setLoanID(String loanID) {
		this.loanID = loanID;
	}

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "LoanPayment [loanID=" + loanID + ", dateTime=" + dateTime + ", amount=" + amount + "]";
	}

}
