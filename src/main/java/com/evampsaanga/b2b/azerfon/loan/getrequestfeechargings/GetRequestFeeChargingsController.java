package com.evampsaanga.b2b.azerfon.loan.getrequestfeechargings;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.amqimplementationsesb.LogsType;
import com.evampsaanga.b2b.azerfon.thirdpartycall.AzerfonThirdPartyCalls;
import com.evampsaanga.b2b.azerfon.validator.RequestValidator;
import com.evampsaanga.b2b.azerfon.validator.ResponseValidator;
import com.evampsaanga.b2b.azerfon.validator.ValidatorService;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Helper;

@Path("/azerfon/")
public class GetRequestFeeChargingsController {

	public static final Logger logger = Logger.getLogger("azerfon-esb");

	@POST
	@Path("/getrequestfeechargings")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetRequestFeeChargingsResponse Get(@Header("credentials") String credential, @Body() String requestBody) {

		Logs logs = new Logs();

		try {

			// ------------------------- Setting Transaction Name ---------------------- //

			GetRequestFeeChargingsResponse getRequestFeeChargingsResponse = new GetRequestFeeChargingsResponse();
			GetRequestFeeChargingsRequest cclient = null;

			String transactionName = Transactions.LOAN_GET_REQUEST_FEE_CHARGINGS;
			String token = Helper.retrieveToken(transactionName, Helper.getValueFromJSON(requestBody, "msisdn"));

			Constants.logger.info(token + "-Request Landed In-" + transactionName);
			Constants.logger.info(token + "-Request Data-" + requestBody);

			// ----------------------------- Validating Text --------------------------- //

			cclient = Helper.JsonToObject(requestBody, GetRequestFeeChargingsRequest.class);

			ValidatorService validatorService = new ValidatorService();
			ResponseValidator responseValidator = new ResponseValidator();
			RequestValidator requestValidator = new RequestValidator();

			requestValidator.setChannel(cclient.getChannel());
			requestValidator.setiP(cclient.getiP());
			requestValidator.setIsB2B(cclient.getIsB2B());
			requestValidator.setLang(cclient.getLang());
			requestValidator.setMsisdn(cclient.getmsisdn());
			responseValidator = validatorService.processValidation(requestValidator, credential, token);

			if (responseValidator.getResponseCode().equals("00")) {

				GetRequestFeeChargingsRequest getProvisionsRequest = new GetRequestFeeChargingsRequest();

				getProvisionsRequest = Helper.JsonToObject(requestBody, GetRequestFeeChargingsRequest.class);

				getRequestFeeChargingsResponse = AzerfonThirdPartyCalls.loanGetRequestFeeChargings(token,
						transactionName, getProvisionsRequest, getRequestFeeChargingsResponse);

			} else {

				getRequestFeeChargingsResponse.setReturnCode(responseValidator.getResponseCode());
				getRequestFeeChargingsResponse.setReturnMsg(responseValidator.getResponseDescription());
				logs.setResponseCode(getRequestFeeChargingsResponse.getReturnCode());
				logs.setResponseDescription(getRequestFeeChargingsResponse.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);

			}

			Constants.logger.info(token + "-Response Returned From-" + transactionName + "-"
					+ Helper.ObjectToJson(getRequestFeeChargingsResponse));

			return getRequestFeeChargingsResponse;

		} catch (Exception ex) {

			Constants.logger.info(Helper.GetException(ex));
			GetRequestFeeChargingsResponse resp = new GetRequestFeeChargingsResponse();
			resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
			resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);

			return resp;
		}
	}
}
