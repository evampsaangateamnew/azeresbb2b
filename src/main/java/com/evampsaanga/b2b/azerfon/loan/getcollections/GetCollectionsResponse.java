package com.evampsaanga.b2b.azerfon.loan.getcollections;

import java.util.List;

import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetCollectionsResponse extends BaseResponse {

	private List<Collection> collections;

	public List<Collection> getCollections() {
		return collections;
	}

	public void setCollections(List<Collection> collections) {
		this.collections = collections;
	}

	@Override
	public String toString() {
		return "GetCollectionsResponse [collections=" + collections + "]";
	}

}