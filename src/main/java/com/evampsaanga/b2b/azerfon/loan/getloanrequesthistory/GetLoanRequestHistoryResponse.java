package com.evampsaanga.b2b.azerfon.loan.getloanrequesthistory;

import java.util.List;

import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetLoanRequestHistoryResponse extends BaseResponse {

	private List<LoanRequest> loanRequest;

	public List<LoanRequest> getLoanRequest() {
		return loanRequest;
	}

	public void setLoanRequest(List<LoanRequest> loanRequest) {
		this.loanRequest = loanRequest;
	}

	@Override
	public String toString() {
		return "GetLoanRequestHistoryResponse [loanRequest=" + loanRequest + "]";
	}

}