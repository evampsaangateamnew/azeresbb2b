package com.evampsaanga.b2b.azerfon.loan.providenewcredit;

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;

public class ProvideNewCreditRequest extends BaseRequest {

	private String creditType;

	public String getCreditType() {
		return creditType;
	}

	public void setCreditType(String creditType) {
		this.creditType = creditType;
	}

	@Override
	public String toString() {
		return "ProvideNewCreditRequest [creditType=" + creditType + "]";
	}

}
