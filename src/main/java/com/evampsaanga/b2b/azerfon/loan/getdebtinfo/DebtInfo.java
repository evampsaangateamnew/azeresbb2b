package com.evampsaanga.b2b.azerfon.loan.getdebtinfo;

public class DebtInfo {

	private Integer debtsCount;
	private Double totalDebtsAmount;

	public Integer getDebtsCount() {
		return debtsCount;
	}

	public void setDebtsCount(Integer debtsCount) {
		this.debtsCount = debtsCount;
	}

	public Double getTotalDebtsAmount() {
		return totalDebtsAmount;
	}

	public void setTotalDebtsAmount(Double totalDebtsAmount) {
		this.totalDebtsAmount = totalDebtsAmount;
	}

	@Override
	public String toString() {
		return "DebtInfo [debtsCount=" + debtsCount + ", totalDebtsAmount=" + totalDebtsAmount + "]";
	}

}
