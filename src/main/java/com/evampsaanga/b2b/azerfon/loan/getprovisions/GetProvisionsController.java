package com.evampsaanga.b2b.azerfon.loan.getprovisions;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;
import org.springframework.jca.cci.connection.CciLocalTransactionManager;

import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.azerfon.thirdpartycall.AzerfonThirdPartyCalls;
import com.evampsaanga.b2b.azerfon.validator.RequestValidator;
import com.evampsaanga.b2b.azerfon.validator.ResponseValidator;
import com.evampsaanga.b2b.azerfon.validator.ValidatorService;
import com.evampsaanga.b2b.configs.Config;
import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Helper;

@Path("/azerfon/")
public class GetProvisionsController {

	public static final Logger logger = Logger.getLogger("azerfon-esb");

	@POST
	@Path("/getprovisions")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LoanHistoryResponseData Get(@Header("credentials") String credential, @Body() String requestBody) {

		Logs logs = new Logs();
		LoanHistoryResponseData loanHistoryResponseData = new LoanHistoryResponseData();
		try {

			// ------------------------- Setting Transaction Name ---------------------- //

			GetProvisionsResponse getProvisionsResponse = new GetProvisionsResponse();
			GetProvisionsRequest cclient = null;

			String transactionName = Transactions.LOAN_GET_PROVISIONS;
			String token = Helper.retrieveToken(transactionName, Helper.getValueFromJSON(requestBody, "msisdn"));

			Constants.logger.info(token + "-Request Landed In-" + transactionName);
			Constants.logger.info(token + "-Request Data-" + requestBody);

			// ----------------------------- Validating Text --------------------------- //

			cclient = Helper.JsonToObject(requestBody, GetProvisionsRequest.class);

			ValidatorService validatorService = new ValidatorService();
			ResponseValidator responseValidator = new ResponseValidator();
			RequestValidator requestValidator = new RequestValidator();

			requestValidator.setChannel(cclient.getChannel());
			requestValidator.setiP(cclient.getiP());
			requestValidator.setIsB2B(cclient.getIsB2B());
			requestValidator.setLang(cclient.getLang());
			requestValidator.setMsisdn(cclient.getmsisdn());
			responseValidator = validatorService.processValidation(requestValidator, credential, token);

			if (responseValidator.getResponseCode().equals("00")) {

				GetProvisionsRequest getProvisionsRequest = new GetProvisionsRequest();

				getProvisionsRequest = Helper.JsonToObject(requestBody, GetProvisionsRequest.class);

				getProvisionsResponse = AzerfonThirdPartyCalls.loanGetProvisions(token, transactionName,
						getProvisionsRequest, getProvisionsResponse);

				loanHistoryResponseData = getLoanHistory(cclient.getmsisdn(), getProvisionsResponse,cclient.getLang());

			} else {

				getProvisionsResponse.setReturnCode(responseValidator.getResponseCode());
				getProvisionsResponse.setReturnMsg(responseValidator.getResponseDescription());
				logs.setResponseCode(getProvisionsResponse.getReturnCode());
				logs.setResponseDescription(getProvisionsResponse.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);

			}

			Constants.logger.info(token + "-Response Returned From-" + transactionName + "-"
					+ Helper.ObjectToJson(loanHistoryResponseData));

//			return getProvisionsResponse;
			return loanHistoryResponseData;

		} catch (Exception ex) {

			Constants.logger.info(Helper.GetException(ex));
			LoanHistoryResponseData resp = new LoanHistoryResponseData();
			resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
			resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);

			return resp;
		}
	}

	public static LoanHistoryResponseData getLoanHistory(String msisdn, GetProvisionsResponse getProvisionsResponse,String lang)
			throws Exception {

		LoanHistoryResponseData loanHistoryResponse = new LoanHistoryResponseData();
		List<Loan> loanList = new ArrayList<Loan>();

		for (int i = 0; i < getProvisionsResponse.getProvisions().size(); i++) {

			if (getProvisionsResponse.getProvisions().get(i).getDebtId() != -1) {

				Loan loan = new Loan();

				loan.setLoanID(getProvisionsResponse.getProvisions().get(i).getDebtId().toString());
				logger.info("Current Amount"+getProvisionsResponse.getProvisions().get(i).getCurrentAmount());
				logger.info("Current Amount"+Helper.getBakcellMoneyDouble(getProvisionsResponse.getProvisions().get(i).getCurrentAmount()));
				loan.setRemaining(Helper.getBakcellMoneyDouble(getProvisionsResponse.getProvisions().get(i).getCurrentAmount()));

//				logger.info("getProvisionsResponse.getProvisions().get(i).getInitialAmount()"+getProvisionsResponse.getProvisions().get(i).getInitialAmount());
//				logger.info("getProvisionsResponse.getProvisions().get(i).getServiceFee()"+getProvisionsResponse.getProvisions().get(i).getServiceFee());
//				logger.info("getProvisionsResponse.getProvisions().get(i).getInitialAmount()"+getProvisionsResponse.getProvisions().get(i).getInitialAmount());
				double paidAmount = ((getProvisionsResponse.getProvisions().get(i).getInitialAmount()
						+ getProvisionsResponse.getProvisions().get(i).getServiceFee()
						- getProvisionsResponse.getProvisions().get(i).getCurrentAmount()));
				
				
				
				loan.setPaid(Helper.getBakcellMoneyDouble(paidAmount));

				if (getProvisionsResponse.getProvisions().get(i).getCurrentAmount() == 0) {

					loan.setStatus(ConfigurationManager.getConfigurationFromCache("azerfon.loan.status.paid.column." + Helper.getLang(lang)));

				} else {

					loan.setStatus(ConfigurationManager.getConfigurationFromCache("azerfon.loan.status.unpaid.column." + Helper.getLang(lang)));
				}

				loan.setDateTime(getProvisionsResponse.getProvisions().get(i).getCreditDate());
				loanList.add(loan);
			}
		}

		List<Loan> newLoanList = new ArrayList<Loan>();
		int flag = 1;

		for (int i = 0; i < loanList.size(); i++) {

			flag = 1;

			for (int j = i + 1; j < loanList.size(); j++) {

				if (loanList.get(i).getLoanID().equalsIgnoreCase(loanList.get(j).getLoanID())) {

					flag = 0;
					break;
				}

			}

			if (flag == 1) {

				newLoanList.add(loanList.get(i));
			}

		}

		loanHistoryResponse.setLoan(newLoanList);
		loanHistoryResponse.setReturnCode("200");
		loanHistoryResponse.setReturnMsg("Success");
		return loanHistoryResponse;
	}
}
