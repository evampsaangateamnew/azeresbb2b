package com.evampsaanga.b2b.azerfon.loan.providenewcredit;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.azerfon.thirdpartycall.AzerfonThirdPartyCalls;
import com.evampsaanga.b2b.azerfon.validator.RequestValidator;
import com.evampsaanga.b2b.azerfon.validator.ResponseValidator;
import com.evampsaanga.b2b.azerfon.validator.ValidatorService;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Helper;

@Path("/azerfon/")
public class ProvideNewCreditController {

	public static final Logger logger = Logger.getLogger("azerfon-esb");

	@POST
	@Path("/providenewcredit")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ProvideNewCreditResponse Get(@Header("credentials") String credential, @Body() String requestBody) {

		Logs logs = new Logs();

		try {

			// ------------------------- Setting Transaction Name ---------------------- //

			ProvideNewCreditResponse provideNewCreditResponse = new ProvideNewCreditResponse();
			ProvideNewCreditRequest cclient = null;

			String transactionName = Transactions.LOAN_PROVIDE_NEW_CREDIT;
			String token = Helper.retrieveToken(transactionName, Helper.getValueFromJSON(requestBody, "msisdn"));

			Constants.logger.info(token + "-Request Landed In-" + transactionName);
			Constants.logger.info(token + "-Request Data-" + requestBody);

			// ----------------------------- Validating Text --------------------------- //

			cclient = Helper.JsonToObject(requestBody, ProvideNewCreditRequest.class);

			ValidatorService validatorService = new ValidatorService();
			ResponseValidator responseValidator = new ResponseValidator();
			RequestValidator requestValidator = new RequestValidator();

			requestValidator.setChannel(cclient.getChannel());
			requestValidator.setiP(cclient.getiP());
			requestValidator.setIsB2B(cclient.getIsB2B());
			requestValidator.setLang(cclient.getLang());
			requestValidator.setMsisdn(cclient.getmsisdn());
			responseValidator = validatorService.processValidation(requestValidator, credential, token);

			if (responseValidator.getResponseCode().equals("00")) {

				ProvideNewCreditRequest provideNewCreditRequest = new ProvideNewCreditRequest();

				provideNewCreditRequest = Helper.JsonToObject(requestBody, ProvideNewCreditRequest.class);

				provideNewCreditResponse = AzerfonThirdPartyCalls.loanProvideNewCredit(token, transactionName,
						provideNewCreditRequest, provideNewCreditResponse);

			} else {

				provideNewCreditResponse.setReturnCode(responseValidator.getResponseCode());
				provideNewCreditResponse.setReturnMsg(responseValidator.getResponseDescription());
				logs.setResponseCode(provideNewCreditResponse.getReturnCode());
				logs.setResponseDescription(provideNewCreditResponse.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);

			}

			Constants.logger.info(token + "-Response Returned From-" + transactionName + "-"
					+ Helper.ObjectToJson(provideNewCreditResponse));

			return provideNewCreditResponse;

		} catch (Exception ex) {

			Constants.logger.info(Helper.GetException(ex));
			ProvideNewCreditResponse resp = new ProvideNewCreditResponse();
			resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
			resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);

			return resp;
		}
	}
}
