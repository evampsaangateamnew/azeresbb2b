/**
 * 
 */
package com.evampsaanga.b2b.azerfon.loan.getprovisions;

import java.util.List;

import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class LoanHistoryResponseData extends BaseResponse  {

	private List<Loan> loan;

	public List<Loan> getLoan() {
		return loan;
	}

	public void setLoan(List<Loan> loan) {
		this.loan = loan;
	}

	@Override
	public String toString() {
		return "LoanHistoryResponseData [loan=" + loan + "]";
	}

}
