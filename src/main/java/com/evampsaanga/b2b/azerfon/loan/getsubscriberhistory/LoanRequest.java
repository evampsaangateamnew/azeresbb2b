package com.evampsaanga.b2b.azerfon.loan.getsubscriberhistory;

public class LoanRequest {

	private String loanID;
	private String dateTime;
	private String amount;
	private String status;
	private String paid;
	private String remaining;

	public String getLoanID() {
		return loanID;
	}

	public void setLoanID(String loanID) {
		this.loanID = loanID;
	}

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPaid() {
		return paid;
	}

	public void setPaid(String paid) {
		this.paid = paid;
	}

	public String getRemaining() {
		return remaining;
	}

	public void setRemaining(String remaining) {
		this.remaining = remaining;
	}

	@Override
	public String toString() {
		return "LoanRequest [loanID=" + loanID + ", dateTime=" + dateTime + ", amount=" + amount + ", status=" + status
				+ ", paid=" + paid + ", remaining=" + remaining + "]";
	}

}
