package com.evampsaanga.b2b.azerfon.loan.getdebtinfo;

import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetDebtInfoResponse extends BaseResponse {

	private DebtInfo debtInfo;

	public DebtInfo getDebtInfo() {
		return debtInfo;
	}

	public void setDebtInfo(DebtInfo debtInfo) {
		this.debtInfo = debtInfo;
	}

	@Override
	public String toString() {
		return "GetDebtInfoResponse [debtInfo=" + debtInfo + "]";
	}

}