package com.evampsaanga.b2b.azerfon.loan.getprovisions;

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;

public class GetProvisionsRequest extends BaseRequest {

	private String reportStartDate;
	private String reportEndDate;

	public String getReportStartDate() {
		return reportStartDate;
	}

	public void setReportStartDate(String reportStartDate) {
		this.reportStartDate = reportStartDate;
	}

	public String getReportEndDate() {
		return reportEndDate;
	}

	public void setReportEndDate(String reportEndDate) {
		this.reportEndDate = reportEndDate;
	}

	@Override
	public String toString() {
		return "GetProvisionsRequest [reportStartDate=" + reportStartDate + ", reportEndDate=" + reportEndDate + "]";
	}

}
