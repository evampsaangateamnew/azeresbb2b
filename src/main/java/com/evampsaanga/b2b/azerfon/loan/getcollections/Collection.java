package com.evampsaanga.b2b.azerfon.loan.getcollections;

public class Collection {

	private Integer debtId;
	private String lastChargingDate;
	private Double amountToCharge;
	private String chargedAmount;
	private String collectionOrigin;
	private Double amountPriorCharging;
	private Double customerBalance;
	private Integer resultCode;
	private String resultDesc;
	private String smsNotificationContent;

	public Integer getDebtId() {
		return debtId;
	}

	public void setDebtId(Integer debtId) {
		this.debtId = debtId;
	}

	public String getLastChargingDate() {
		return lastChargingDate;
	}

	public void setLastChargingDate(String lastChargingDate) {
		this.lastChargingDate = lastChargingDate;
	}

	public Double getAmountToCharge() {
		return amountToCharge;
	}

	public void setAmountToCharge(Double amountToCharge) {
		this.amountToCharge = amountToCharge;
	}

	

	public String getChargedAmount() {
		return chargedAmount;
	}

	public void setChargedAmount(String chargedAmount) {
		this.chargedAmount = chargedAmount;
	}

	public String getCollectionOrigin() {
		return collectionOrigin;
	}

	public void setCollectionOrigin(String collectionOrigin) {
		this.collectionOrigin = collectionOrigin;
	}

	public Double getAmountPriorCharging() {
		return amountPriorCharging;
	}

	public void setAmountPriorCharging(Double amountPriorCharging) {
		this.amountPriorCharging = amountPriorCharging;
	}

	public Double getCustomerBalance() {
		return customerBalance;
	}

	public void setCustomerBalance(Double customerBalance) {
		this.customerBalance = customerBalance;
	}

	public Integer getResultCode() {
		return resultCode;
	}

	public void setResultCode(Integer resultCode) {
		this.resultCode = resultCode;
	}

	public String getResultDesc() {
		return resultDesc;
	}

	public void setResultDesc(String resultDesc) {
		this.resultDesc = resultDesc;
	}

	public String getSmsNotificationContent() {
		return smsNotificationContent;
	}

	public void setSmsNotificationContent(String smsNotificationContent) {
		this.smsNotificationContent = smsNotificationContent;
	}

	@Override
	public String toString() {
		return "Collection [debtId=" + debtId + ", lastChargingDate=" + lastChargingDate + ", amountToCharge="
				+ amountToCharge + ", chargedAmount=" + chargedAmount + ", collectionOrigin=" + collectionOrigin
				+ ", amountPriorCharging=" + amountPriorCharging + ", customerBalance=" + customerBalance
				+ ", resultCode=" + resultCode + ", resultDesc=" + resultDesc + ", smsNotificationContent="
				+ smsNotificationContent + "]";
	}

}
