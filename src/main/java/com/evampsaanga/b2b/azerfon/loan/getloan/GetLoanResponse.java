package com.evampsaanga.b2b.azerfon.loan.getloan;

import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetLoanResponse extends BaseResponse {

}