package com.evampsaanga.b2b.azerfon.getcoreservices;

import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;

public class GetCoreServicesResponse extends BaseResponse {
	private Data data = new Data();

	/**
	 * @return the data
	 */
	public Data getData() {
		return data;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	public void setData(Data data) {
		this.data = data;
	}
}
