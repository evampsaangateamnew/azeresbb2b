package com.evampsaanga.b2b.azerfon.getcoreservices;

import java.util.ArrayList;

public class Data {
	private ArrayList<CoreServices> coreServices = new ArrayList<CoreServices>();

	/**
	 * @return the coreServices
	 */
	public ArrayList<CoreServices> getCoreServices() {
		return coreServices;
	}

	/**
	 * @param coreServices
	 *            the coreServices to set
	 */
	public void setCoreServices(ArrayList<CoreServices> coreServices) {
		this.coreServices = coreServices;
	}
}
