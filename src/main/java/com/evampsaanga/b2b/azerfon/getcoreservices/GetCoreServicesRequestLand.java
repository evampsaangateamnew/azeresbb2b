package com.evampsaanga.b2b.azerfon.getcoreservices;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.amqimplementationsesb.LogsType;
import com.evampsaanga.b2b.azerfon.getnetworksettings.GetNetworkSettingsRequestClient;
import com.evampsaanga.b2b.azerfon.getsubscriber.CRMSubscriberService;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.ThirdPartyNames;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Decrypter;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.huawei.crm.azerfon.bss.query.GetSubscriberResponse;
import com.huawei.crm.basetype.GetSubProductInfo;
import com.huawei.crm.query.GetNetworkSettingDataResponse;

@Path("/azerfon")
public class GetCoreServicesRequestLand {
	public static final Logger logger = Logger.getLogger("azerfon-esb");

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetCoreServicesResponse Get(@Header("credentials") String credential, @Body() String requestBody) {
		GetCoreServicesResponse resp = new GetCoreServicesResponse();
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.GET_CORE_SERVICES_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.GET_CORE_SERVICES);
		logs.setTableType(LogsType.GetCoreServices);
		String token = "";
		String TrnsactionName = Transactions.GET_CORE_SERVICES_TRANSACTION_NAME;

		try {

			token = Helper.retrieveToken(TrnsactionName, Helper.getValueFromJSON(requestBody, "msisdn"));

			logger.info(token + "Request Landed on GetCoreServicesRequestLand " + requestBody);

			String credentials = null;
			GetCoreServicesRequest cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, GetCoreServicesRequest.class);
			} catch (Exception ex1) {
				logger.info(token + Helper.GetException(ex1));
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				if (cclient.getIsB2B() != null && cclient.getIsB2B().equals("true")) {
					logs.setTransactionName(Transactions.GET_CORE_SERVICES_TRANSACTION_NAME_B2B);
				}
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					logger.info(token + Helper.GetException(ex));
				}
				if (credentials == null) {
					logger.info(token + "credentials Null check.....");
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				String verification = Helper.validateRequest(cclient);
				if (!verification.equals("")) {
					resp.setReturnCode(ResponseCodes.ERROR_400);
					resp.setReturnMsg(verification);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					logger.info(token + "credentials validated credentials matched.....");
					CRMSubscriberService crmSub = new CRMSubscriberService();
					GetSubscriberResponse subsResponse = crmSub.GetSubscriberRequest(cclient.getmsisdn());

					GetNetworkSettingsRequestClient networkclient = new GetNetworkSettingsRequestClient();
					networkclient.setMsisdn(cclient.getmsisdn());
					GetNetworkSettingDataResponse responseFromBakcell = com.evampsaanga.b2b.azerfon.getnetworksettings.GetNetworkSettingsLand
							.RequestSoap(com.evampsaanga.b2b.azerfon.getnetworksettings.GetNetworkSettingsLand
									.getRequestHeader(), networkclient);
					List<GetSubProductInfo> getsubproductinfo = new ArrayList<>();
					if (responseFromBakcell != null && responseFromBakcell.getGetNetworkSettingDataBody() != null
							&& responseFromBakcell.getGetNetworkSettingDataBody().getGetNetworkSettingDataList() != null
							&& responseFromBakcell.getGetNetworkSettingDataBody().getGetNetworkSettingDataList()
									.size() > 0) {
						getsubproductinfo = responseFromBakcell.getGetNetworkSettingDataBody()
								.getGetNetworkSettingDataList();
					}
					if (Constants.CRMSUBACCESSCODE.equals(subsResponse.getResponseHeader().getRetCode())) {

						logger.info(
								token + "SubscriberRespnsecode....." + subsResponse.getResponseHeader().getRetCode());

						resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
						resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
						String freeFor = getFreeForValue(cclient.getUserType(), cclient.getBrand(),
								cclient.getAccountType(), cclient.getGroupType(), token);
						String visibleFor = getVisibleForValue(cclient.getUserType(), cclient.getBrand(),
								cclient.getAccountType(), cclient.getGroupType(), token);
						logger.info( token+"Free FOR: " +freeFor);
						logger.info( token+"Visisble FOR: " +visibleFor);
						// just commented this line because the GetNetwork setting ere not there in new
						// Get Subscriber Offers
						resp.getData()
								.setCoreServices(crmSub.getCoreServices(subsResponse.getGetSubscriberBody(),
										cclient.getLang(), cclient.getIsFrom(), freeFor, visibleFor, getsubproductinfo,
										cclient.getAccountType(), token));
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					} else {
						resp.setReturnCode(subsResponse.getResponseHeader().getRetCode());
						resp.setReturnMsg(subsResponse.getResponseHeader().getRetMsg());
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					}
					logs.updateLog(logs);

					return resp;
				} else {
					logger.info(token + "credentials  check ELSe not mathed credeitials.....");
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else
				resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
			resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		} catch (Exception ex) {
			logger.info(token + Helper.GetException(ex));
			resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
			resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
	}

	private String getVisibleForValue(String userType, String brand, String accountType, String groupType,
			String token) {
		logger.info(token + "*******START of getVisibleForValue Method********");
		logger.info(token + "userType: " + userType + " :brand :" + brand + " :accountType: " + accountType
				+ " :groupType: " + groupType);
		StringBuffer visibleFor = new StringBuffer();
		visibleFor.append(userType);
		//visibleFor.append("-" + brand);

		if (accountType != null && !accountType.trim().equals("")) {
			/*if (!userType.equalsIgnoreCase("prepaid")) {*/
				visibleFor.append("-" + accountType);
			//}
		}

		if (groupType != null && !groupType.trim().equals("")) {
			visibleFor.append("-" + groupType);
		}
		logger.info(token + "Final Value- Visible For: " + visibleFor);
		logger.info(token + "*******END of getVisibleForValue Method******");
		return visibleFor.toString();

	}

	private String getFreeForValue(String userType, String brand, String accountType, String groupType, String token) {

		logger.info(token + "*******START of getFreeForValue Method********");
		logger.info(token + "userType: " + userType + " :brand :" + brand + " :accountType: " + accountType
				+ " :groupType: " + groupType);

		StringBuffer freeFor = new StringBuffer();
		freeFor.append(userType);
		freeFor.append("-" + brand);

		if (accountType != null && !accountType.trim().equals("")) {
			if (!userType.equalsIgnoreCase("prepaid")) {
				freeFor.append("-" + accountType);
			}
		}

		if (groupType != null && !groupType.trim().equals("")) {
			freeFor.append("-" + groupType);
		}
		logger.info(token + "final value- Free FOR: " + freeFor.toString());
		logger.info(token + "*******END of getFreeForValue Method********");
		return freeFor.toString();
	}
	public static void main(String[] args) {
	
		        ArrayList<String> listOne = new ArrayList<>(Arrays.asList("1009268412365"));
		        
		        ArrayList<String> listTwo = new ArrayList<>(Arrays.asList("a", "b", "c", "d", "e"));
		        
		        //remove all elements of second list
		        boolean missing =listOne.removeAll(listTwo);
		       
		        if(missing)
		        {
		        	 System.out.println("found");
		        System.out.println(listOne);
		          for(int i=0;i<listOne.size();i++)
		          {
		        	 System.out.println("value :"+listOne.get(i));
		          }
		        }
		        else
		        {
		        	 System.out.println("NO found");
		        }
  }
	
}
