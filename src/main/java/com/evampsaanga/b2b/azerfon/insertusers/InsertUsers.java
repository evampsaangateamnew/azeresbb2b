package com.evampsaanga.b2b.azerfon.insertusers;

/**
 * @author Syed Wasay
 *
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.evampsaanga.azerfon.appserver.refreshappservercache.BuildCacheRequestLand;
import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.amqimplementationsesb.LogsType;
import com.evampsaanga.b2b.azerfon.db.DBFactory;
import com.evampsaanga.b2b.configs.Config;
import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.ThirdPartyNames;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.google.gson.JsonObject;

@Path("/azerfon")
public class InsertUsers {

	public static final Logger logger = Logger.getLogger("azerfon-esb");

	@POST
	@Path("/insertUsers")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public InsertUsersResponse insertUsers(@Body String requestBody) {
		Helper.logInfoMessageV2("Request Data: insertusers V2" + requestBody);

		Logs logs = new Logs();
		logs.setTransactionName(Transactions.INSERT_USERS_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.INSERT_USERS);
		logs.setTableType(LogsType.InsertUsers);
		InsertUsersResponse resp = new InsertUsersResponse();
		String TrnsactionName = Transactions.BASE_MAGENTO_TRANSACTION_NAME + " "
				+ Transactions.INSERT_USERS_TRANSACTION_NAME;
		String filePath = ConfigurationManager.getDBProperties("file.path");

		try {
	
			Helper.logInfoMessageV2("Calling: AddBatchToDBUsingFile" + requestBody);
			int count = AddBatchToDBUsingFile(filePath);
			if (count > 0) {
				resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
				resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}

			else if (count == -1) {
				resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
				resp.setReturnMsg(ResponseCodes.INVALID_CSV_MSG);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}

			else {
				resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
				resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}

		} catch (Exception ex) {
			logger.info(TrnsactionName + Helper.GetException(ex));
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_401);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}

	}

	/* 
	 * 
	 */
	@SuppressWarnings("resource")
	public int AddBatchToDBUsingFile(String filePath) {
		Helper.logInfoMessageV2("Request Landed: AddBatchToDBUsingFile with file path:"+filePath.toString());
		String row = "";
		int count = 0;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		
		int queryReturnCount = 0;
		try {
			Helper.logInfoMessageV2("Buffer Reader Call");
			BufferedReader csvReader = new BufferedReader(
					new InputStreamReader(new FileInputStream(filePath), "UTF-8"));
			Helper.logInfoMessageV2("Buffer Reader finished");

		
			
			connection = DBFactory.getDbConnection();
			// Creating new backup table
			PreparedStatement preparedStatement_create;
			String query_create = "CREATE TABLE `user_data_backup` (`id` bigint(255) NOT NULL AUTO_INCREMENT,`MSISDN` varchar(255) NOT NULL,`TARIFF_PLAN` varchar(255) NOT NULL,`CRM_SUB_ID` varchar(255) NOT NULL,`CRM_CUST_ID` varchar(255) NOT NULL,`CUST_CODE` varchar(255) NOT NULL,`VIRTUAL_CORP_CODE` varchar(255) NOT NULL,`CRM_CORP_CUST_ID` varchar(255) NOT NULL,`CUG_GROUP_CODE` varchar(255) NOT NULL,`GROUP_CODE` varchar(255) NOT NULL,`CRM_ACCT_ID` varchar(255) NOT NULL,`ACCT_CODE` varchar(255) NOT NULL,`CRM_ACCT_ID_PAID` varchar(255) NOT NULL,PRIMARY KEY (`id`)) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1";
			Helper.logInfoMessageV2("Creating user_data_backup start" + query_create);
			preparedStatement_create = connection.prepareStatement(query_create);
			logger.info("Executed for Creating Backup Table " + preparedStatement_create.executeUpdate());
			Helper.logInfoMessageV2("Backup table created success");
			String query = "INSERT INTO user_data_backup (MSISDN,TARIFF_PLAN,CRM_SUB_ID,CRM_CUST_ID,CUST_CODE,VIRTUAL_CORP_CODE,CRM_CORP_CUST_ID,CUG_GROUP_CODE,GROUP_CODE,CRM_ACCT_ID,ACCT_CODE,CRM_ACCT_ID_PAID) values (?,?,?,?,?,?,?,?,?,?,?,?)";
			preparedStatement = connection.prepareStatement(query);

			int batchCount = 0;
			int batchNumber = 0;
			final int batchSize = 5000;

			Helper.logInfoMessageV2("loop started");

				row = csvReader.readLine();
			while ((row = csvReader.readLine()) != null) {

				//Helper.logInfoMessageV2("In While loop count" + count++);
				// Helper.logInfoMessageV2("In While loop row"+row);
				if (!row.matches(".*[a-zA-Z]+.*")) {
					
					if((row.contains("\"")))
					{
						row =row.replace("\"", "");
					}
					
					String[] data = row.split(",", -1);
					// do something with the data

					preparedStatement.setString(1, data[0]);

					preparedStatement.setString(2, data[1]);

					preparedStatement.setString(3, data[2]);

					preparedStatement.setString(4, data[3]);

					preparedStatement.setString(5, data[4]);

					preparedStatement.setString(6, data[5]);

					preparedStatement.setString(7, data[6]);

					preparedStatement.setString(8, data[7]);

					preparedStatement.setString(9, data[8]);

					preparedStatement.setString(10, data[9]);

					preparedStatement.setString(11, data[10]);

					preparedStatement.setString(12, data[11]);

					preparedStatement.addBatch();

					if (++batchCount % batchSize == 0) {
						batchNumber++;
						Helper.logInfoMessageV2("Executing Batch No: " + batchNumber);
						preparedStatement.executeBatch();
						queryReturnCount++;
					}
				}
				else
				{
					Helper.logInfoMessageV2("Calling Method to Insert CSV Status");
					csvFileStatus("B2B_F_SUB.csv", "0", "Invalid Csv.Regular Expression Failed");
					Helper.logInfoMessageV2("Invalid Csv " );
					String query_drop = "DROP TABLE IF EXISTS user_data_backup";
					Helper.logInfoMessageV2("Droping user_data_backup start" + query_drop);
					preparedStatement = connection.prepareStatement(query_drop);
					logger.info("Executed for Droping Backup Table " + preparedStatement.executeUpdate());
					
					return (queryReturnCount = -1);
				}
			}
			Helper.logInfoMessageV2("loop end");
			preparedStatement.executeBatch();
			queryReturnCount++;
			Helper.logInfoMessageV2("Remaining  Batch executed");
			csvReader.close();
		} catch (SQLException e) {
			Helper.logInfoMessageV2("Calling Method to Insert CSV Status");
			csvFileStatus("B2B_F_SUB.csv", "0", e.getMessage().toString());
			Helper.logInfoMessageV2(Helper.GetException(e));
			
			try{
				String query_drop = "DROP TABLE IF EXISTS user_data_backup";
			Helper.logInfoMessageV2("Droping user_data_backup start" + query_drop);
			preparedStatement = connection.prepareStatement(query_drop);
			logger.info("Executed for Droping Backup Table " + preparedStatement.executeUpdate());
			}
			catch (SQLException ex)
			{
				Helper.logInfoMessageV2("Calling Method to Insert CSV Status");
				csvFileStatus("B2B_F_SUB.csv", "0", e.getMessage().toString());
				Helper.logInfoMessageV2(Helper.GetException(e));
				return (queryReturnCount = -1);
			}
			
			return (queryReturnCount = -1);

		} catch (IOException e) {
			Helper.logInfoMessageV2("Calling Method to Insert CSV Status");
			csvFileStatus("B2B_F_SUB.csv", "0", e.getMessage().toString());
			Helper.logInfoMessageV2(Helper.GetException(e));
			
			try{
				String query_drop = "DROP TABLE IF EXISTS user_data_backup";
			Helper.logInfoMessageV2("Droping user_data_backup start" + query_drop);
			preparedStatement = connection.prepareStatement(query_drop);
			logger.info("Executed for Droping Backup Table " + preparedStatement.executeUpdate());
			}
			catch (SQLException ex)
			{
				Helper.logInfoMessageV2("Calling Method to Insert CSV Status");
				csvFileStatus("B2B_F_SUB.csv", "0", e.getMessage().toString());
				Helper.logInfoMessageV2(Helper.GetException(e));
				return (queryReturnCount = -1);
			}
			return (queryReturnCount = -1);
		}

		File f1 = new File(ConfigurationManager.getDBProperties("file.path"));
		File f2 = new File(ConfigurationManager.getDBProperties("backup.file.path"));
		try {
			org.apache.commons.io.FileUtils.copyFile(f1, f2);
		} catch (IOException e) {
			Helper.logInfoMessageV2("Calling Method to Insert CSV Status");
			csvFileStatus("B2B_F_SUB.csv", "0", e.getMessage().toString());
			Helper.logInfoMessageV2(Helper.GetException(e));
			try{
				String query_drop = "DROP TABLE IF EXISTS user_data_backup";
			Helper.logInfoMessageV2("Droping user_data_backup start" + query_drop);
			preparedStatement = connection.prepareStatement(query_drop);
			logger.info("Executed for Droping Backup Table " + preparedStatement.executeUpdate());
			}
			catch (SQLException ex)
			{
				Helper.logInfoMessageV2("Calling Method to Insert CSV Status");
				csvFileStatus("B2B_F_SUB.csv", "0", e.getMessage().toString());
				Helper.logInfoMessageV2(Helper.GetException(e));
				return (queryReturnCount = -1);
			}
			
			return (queryReturnCount = -1);
		}

		// droping table
		PreparedStatement preparedStatement_drop;
		Helper.logInfoMessageV2("Droping Table");
		String query_drop = "DROP TABLE user_data";
		Helper.logInfoMessageV2("Droping user_data_start" + query_drop);
		try {
			preparedStatement_drop = connection.prepareStatement(query_drop);
			preparedStatement_drop.execute();
			PreparedStatement preparedStatement_rename;
			Helper.logInfoMessageV2("Drop Table Successfully");

			Helper.logInfoMessageV2("Renaming Table");
			String query_rename = "RENAME TABLE user_data_backup TO user_data";
			Helper.logInfoMessageV2("Renaming user_data_backup start" + query_rename);
			preparedStatement_rename = connection.prepareStatement(query_rename);
			boolean check_rename = preparedStatement_rename.execute();
			if (check_rename)
				Helper.logInfoMessageV2("Renamed Table Successfully");
		} catch (SQLException e) {
			Helper.logInfoMessageV2("Calling Method to Insert CSV Status");
			csvFileStatus("B2B_F_SUB.csv", "0", e.getMessage().toString());
			Helper.logInfoMessageV2(Helper.GetException(e));
			try{
				String query_drop_backup = "DROP TABLE IF EXISTS user_data_backup";
			Helper.logInfoMessageV2("Droping user_data_backup start" + query_drop_backup);
			preparedStatement = connection.prepareStatement(query_drop_backup);
			logger.info("Executed for Droping Backup Table " + preparedStatement.executeUpdate());
			}
			catch (SQLException ex)
			{
				Helper.logInfoMessageV2("Calling Method to Insert CSV Status");
				csvFileStatus("B2B_F_SUB.csv", "0", e.getMessage().toString());
				Helper.logInfoMessageV2(Helper.GetException(e));
				
				
				return (queryReturnCount = -1);
			}
			
			return (queryReturnCount = -1);
		}
//		if (Objects.nonNull(connection)) {
//			try {
//				connection.close();
//			} catch (SQLException e) {
//				Helper.logInfoMessageV2(Helper.GetException(e));
//				return (queryReturnCount = -1);
//			}
//		}
		Helper.logInfoMessageV2("Calling Method to Insert CSV Status");
		csvFileStatus("B2B_F_SUB.csv", "1", "Successfully Inserted");
		Helper.logInfoMessageV2("Returning Count: exportEntriesUser" + queryReturnCount);
		if(BuildCacheRequestLand.usersCache != null)
		{
			BuildCacheRequestLand.usersCache.clear();
		}
		
		return queryReturnCount;

	}
	
	
	
	public void csvFileStatus(String filename,String status,String comment)
	{
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		String queryStatus ="INSERT INTO csv_file_status ( `filename`, `status`, `comment` ) VALUES (?,?,?)";
		try
		{
			
			connection = DBFactory.getDbConnection();
			preparedStatement = connection.prepareStatement(queryStatus);
			preparedStatement.setString(1, filename);
			preparedStatement.setString(2, status);
			preparedStatement.setString(3, comment);
			preparedStatement.executeUpdate();
//			connection.close();
		}
		catch (Exception e) {
			Helper.logInfoMessageV2(Helper.GetException(e));
		
		}
		
	}

//	public static List<Users> readFromCsv(String filePath) {
//		String row = "";
//		Users user = null;
//		List<Users> list = new ArrayList<Users>();
//		Helper.logInfoMessageV2("Buffer Reader Call");
//		try {
//			int count = 0;
//			BufferedReader csvReader = new BufferedReader(
//					new InputStreamReader(new FileInputStream(filePath), "UTF-8"));
//			Helper.logInfoMessageV2("Buffer Reader finished");
//
//			while ((row = csvReader.readLine()) != null) {
//
//				Helper.logInfoMessageV2("In While loop count" + count++);
//				// Helper.logInfoMessageV2("In While loop row"+row);
//				if (!(row.contains("\"")) && !row.matches(".*[a-zA-Z]+.*")) {
//					String[] data = row.split(",", -1);
//					// do something with the data
//
//					user = new Users();
//
//					user.setMsisdn(data[0]);
//					user.setTariffPlan(data[1]);
//					user.setCrmSubId(data[2]);
//					user.setCrmCustId(data[3]);
//					user.setCustCode(data[4]);
//					user.setVirtualCorpCode(data[5]);
//					user.setCrmCorpCustId(data[6]);
//					user.setCugGroupCode(data[7]);
//					user.setGroupCode(data[8]);
//					user.setCrmAcctID(data[9]);
//					user.setAcctCode(data[10]);
//					user.setCrmAcctIdPaid(data[11]);
//
//					list.add(user);
//					user = null;
//
//				} else {
//					Helper.logInfoMessageV2("returning list null because of invalid data in row" + row);
//					csvReader.close();
//					return list = null;
//
//				}
//			}
//
//			csvReader.close();
//		} catch (Exception e) {
//			Helper.logInfoMessageV2(Helper.GetException(e));
//		}
//		Helper.logInfoMessageV2("CSV result list size is: " + list.size());
//		return list;
//	}
	
	public void runCronJob(){
		String s = "Pic_new1";
		this.insertUsers(s);
	}
	
	public static void main(String[] args) throws IOException {

		String s = "\"123aaa\"";
		if (!(s.contains("\"")) && !s.matches(".*[a-zA-Z]+.*"))
			System.out.println("String is Clean" + s);
		else
			System.out.println("String is invalid");

	}

}