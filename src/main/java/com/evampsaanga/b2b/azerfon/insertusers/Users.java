package com.evampsaanga.b2b.azerfon.insertusers;

/**
 * @author Syed Wasay
 *
 */

public class Users {
	private String msisdn;
	private String tariffPlan;
	private String crmSubId;
	private String crmCustId;
	private String custCode;
	private String virtualCorpCode;
	private String crmCorpCustId;
	private String cugGroupCode;
	private String groupCode;
	private String crmAcctID;
	private String acctCode;
	private String crmAcctIdPaid;
	
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	public String getTariffPlan() {
		return tariffPlan;
	}
	public void setTariffPlan(String tariffPlan) {
		this.tariffPlan = tariffPlan;
	}
	public String getCrmSubId() {
		return crmSubId;
	}
	public void setCrmSubId(String crmSubId) {
		this.crmSubId = crmSubId;
	}
	public String getCrmCustId() {
		return crmCustId;
	}
	public void setCrmCustId(String crmCustId) {
		this.crmCustId = crmCustId;
	}
	public String getCustCode() {
		return custCode;
	}
	public void setCustCode(String custCode) {
		this.custCode = custCode;
	}
	public String getVirtualCorpCode() {
		return virtualCorpCode;
	}
	public void setVirtualCorpCode(String virtualCorpCode) {
		this.virtualCorpCode = virtualCorpCode;
	}
	public String getCrmCorpCustId() {
		return crmCorpCustId;
	}
	public void setCrmCorpCustId(String crmCorpCustId) {
		this.crmCorpCustId = crmCorpCustId;
	}
	public String getCugGroupCode() {
		return cugGroupCode;
	}
	public void setCugGroupCode(String cugGroupCode) {
		this.cugGroupCode = cugGroupCode;
	}
	public String getGroupCode() {
		return groupCode;
	}
	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}
	public String getCrmAcctID() {
		return crmAcctID;
	}
	public void setCrmAcctID(String crmAcctID) {
		this.crmAcctID = crmAcctID;
	}
	public String getAcctCode() {
		return acctCode;
	}
	public void setAcctCode(String acctCode) {
		this.acctCode = acctCode;
	}
	public String getCrmAcctIdPaid() {
		return crmAcctIdPaid;
	}
	public void setCrmAcctIdPaid(String crmAcctIdPaid) {
		this.crmAcctIdPaid = crmAcctIdPaid;
	}
}
