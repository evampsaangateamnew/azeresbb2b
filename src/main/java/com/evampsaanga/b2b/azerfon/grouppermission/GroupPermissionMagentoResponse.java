
package com.evampsaanga.b2b.azerfon.grouppermission;

/**
 * @author Syed Wasay
 *
 */

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "data",
    "resultCode",
    "msg",
    "execTime"
})
public class GroupPermissionMagentoResponse {

    @JsonProperty("data")
    private List<Datum> data = null;
    @JsonProperty("resultCode")
    private Integer resultCode;
    @JsonProperty("msg")
    private String msg;
    @JsonProperty("execTime")
    private Double execTime;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("data")
    public List<Datum> getData() {
        return data;
    }

    @JsonProperty("data")
    public void setData(List<Datum> data) {
        this.data = data;
    }

    @JsonProperty("resultCode")
    public Integer getResultCode() {
        return resultCode;
    }

    @JsonProperty("resultCode")
    public void setResultCode(Integer resultCode) {
        this.resultCode = resultCode;
    }

    @JsonProperty("msg")
    public String getMsg() {
        return msg;
    }

    @JsonProperty("msg")
    public void setMsg(String msg) {
        this.msg = msg;
    }

    @JsonProperty("execTime")
    public Double getExecTime() {
        return execTime;
    }

    @JsonProperty("execTime")
    public void setExecTime(Double execTime) {
        this.execTime = execTime;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

	@Override
	public String toString() {
		return "GroupPermissionMagentoResponse [data=" + data + ", resultCode=" + resultCode + ", msg=" + msg
				+ ", execTime=" + execTime + ", additionalProperties=" + additionalProperties + "]";
	}

}
