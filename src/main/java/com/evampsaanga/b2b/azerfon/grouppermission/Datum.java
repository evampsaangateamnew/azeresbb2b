
package com.evampsaanga.b2b.azerfon.grouppermission;

import java.io.Serializable;

/**
 * @author Syed Wasay
 *
 */

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

//@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "group_id",
    "offering_ids",
    "offers_restrictions",
    "tariff_restrictions",
    "limit_restrictions",
    "status",
    "suspend_number"
})
public class Datum implements Serializable {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("group_id")
    private String groupId;
    @JsonProperty("offering_ids")
    private String offeringIds;
    @JsonProperty("offers_restrictions")
    private String offersRestrictions;
    @JsonProperty("tariff_restrictions")
    private String tariffRestrictions;
    @JsonProperty("limit_restrictions")
    private String limitRestrictions;
    @JsonProperty("status")
    private String status;
    @JsonProperty("suspend_number")
    private String suspendNumber;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("group_id")
    public String getGroupId() {
        return groupId;
    }

    @JsonProperty("group_id")
    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    @JsonProperty("offering_ids")
    public String getOfferingIds() {
        return offeringIds;
    }

    @JsonProperty("offering_ids")
    public void setOfferingIds(String offeringIds) {
        this.offeringIds = offeringIds;
    }

    @JsonProperty("offers_restrictions")
    public String getOffersRestrictions() {
        return offersRestrictions;
    }

    @JsonProperty("offers_restrictions")
    public void setOffersRestrictions(String offersRestrictions) {
        this.offersRestrictions = offersRestrictions;
    }

    @JsonProperty("tariff_restrictions")
    public String getTariffRestrictions() {
        return tariffRestrictions;
    }

    @JsonProperty("tariff_restrictions")
    public void setTariffRestrictions(String tariffRestrictions) {
        this.tariffRestrictions = tariffRestrictions;
    }

    @JsonProperty("limit_restrictions")
    public String getLimitRestrictions() {
        return limitRestrictions;
    }

    @JsonProperty("limit_restrictions")
    public void setLimitRestrictions(String limitRestrictions) {
        this.limitRestrictions = limitRestrictions;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("suspend_number")
    public String getSuspendNumber() {
        return suspendNumber;
    }

    @JsonProperty("suspend_number")
    public void setSuspendNumber(String suspendNumber) {
        this.suspendNumber = suspendNumber;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
