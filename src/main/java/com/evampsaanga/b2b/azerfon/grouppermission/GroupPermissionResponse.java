package com.evampsaanga.b2b.azerfon.grouppermission;

/**
 * @author Syed Wasay
 *
 */

import java.util.List;

import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;

public class GroupPermissionResponse extends BaseResponse {
	private List<Datum> data = null;

	public List<Datum> getData() {
		return data;
	}

	public void setData(List<Datum> data) {
		this.data = data;
	}
}
