package com.evampsaanga.b2b.azerfon.grouppermission;
/**
 * @author Syed Wasay
 *
 */

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;

public class GroupPermissionRequest extends BaseRequest {
	private String groupId;

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
}
