package com.evampsaanga.b2b.azerfon.getcustomerrequest;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.ws.WebServiceException;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.ngbss.evampsaanga.services.CRMServices;
import com.ngbss.evampsaanga.services.QueryOrderService;
import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.amqimplementationsesb.LogsType;
import com.evampsaanga.b2b.azerfon.db.DBAzerfonFactory;
import com.evampsaanga.b2b.azerfon.getcdrsbydate.CustomerID;
import com.evampsaanga.b2b.azerfon.hlrweb.getquerybalanceRequest.GetQueryBalanceLand;
import com.evampsaanga.b2b.azerfon.hlrweb.getquerybalanceRequest.HLRBalanceServices;
import com.evampsaanga.b2b.azerfon.thirdpartycall.AzerfonThirdPartyCalls;
import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.ThirdPartyNames;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Decrypter;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.evampsaanga.b2b.developer.utils.SOAPLoggingHandler;
import com.evampsaanga.b2b.magento.specialoffers.ProcessSpecialcustomerData;
import com.evampsaanga.b2b.magento.specialoffers.ProcessingSpecialsOffersInternal;
import com.evampsaanga.b2b.magento.specialoffers.SpecialOffersRequest;
import com.huawei.bme.cbsinterface.arservices.QueryBalanceResultMsg;
import com.huawei.bss.soaif._interface.common.ServiceNum;
import com.huawei.bss.soaif._interface.corporateservice.QueryCorporateCustReqMsg;
import com.huawei.bss.soaif._interface.corporateservice.QueryCorporateCustRspMsg;
import com.huawei.bss.soaif._interface.hlrwebservice.AcctList;
import com.huawei.bss.soaif._interface.hlrwebservice.AcctList.CorPayRelaList;
import com.huawei.bss.soaif._interface.hlrwebservice.QueryBalanceResponse;
import com.huawei.crm.azerfon.bss.query.GetSubscriberResponse;
import com.huawei.crm.basetype.RequestHeader;
import com.huawei.crm.query.GetCustomerIn;
import com.huawei.crm.query.GetCustomerRequest;
import com.huawei.crm.query.GetCustomerResponse;
import com.huawei.crm.query.GetGroupDataIn;
import com.huawei.crm.query.GetGroupRequest;
import com.huawei.crm.query.GetGroupResponse;
import com.ngbss.evampsaanga.services.ThirdPartyCall;

@Path("/azerfon")
public class GetCustomerDataLand {

	public static final Logger logger = Logger.getLogger("azerfon-esb");

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetCustomerRequestResponse Get(@Header("credentials") String credential,
			@Header("Content-Type") String contentType, @Body() String requestBody) {
		Logs logs = new Logs();
		GetCustomerRequestResponse resp = new GetCustomerRequestResponse();

		String token = "";
		String TrnsactionName = Transactions.CUSTOMER_DATA_TRANSACTION_NAME;

		try {

			token = Helper.retrieveToken(TrnsactionName, Helper.getValueFromJSON(requestBody, "msisdn"));

			logger.info(token + "Request Landed on getCustomerData:" + requestBody);
			logs.setRequestDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.setTransactionName(Transactions.CUSTOMER_DATA_TRANSACTION_NAME);
			logs.setThirdPartyName(ThirdPartyNames.CUSTOMER_DATA);
			logs.setTableType(LogsType.CustomerData);

			GetCustomerRequestClient cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, GetCustomerRequestClient.class);
				if (cclient != null) {
					if (cclient.getIsB2B() != null && cclient.getIsB2B().equals("true")) {
						logs.setTransactionName(Transactions.CUSTOMER_DATA_TRANSACTION_NAME_B2B);
					}
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());

					logger.info(token + "TransactionName: " + logs.getTransactionName());
				}
			} catch (Exception ex) {
				logger.info(token + Helper.GetException(ex));
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					logger.info(token + "Unable to decrypt credentials");
					SOAPLoggingHandler.logger.info(token + Helper.GetException(ex));
				}
				if (credentials == null) {
					logger.info(token + "<<<<<<<< <<<<< credentials = null >>>>> >>>>>>>>");
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						logger.info(token + "<<<<<<<< <<<<< verification = null >>>>> >>>>>>>>");
						GetCustomerRequestResponse res = new GetCustomerRequestResponse();
						res.setReturnCode(ResponseCodes.ERROR_400_CODE);
						res.setReturnMsg(verification);
						logs.setResponseCode(res.getReturnCode());
						logs.setResponseDescription(res.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return res;
					}
				} else {
					logger.info(token + "<<<<<<<< <<<<< ERROR_MSISDN_CODE >>>>> >>>>>>>>");
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {

					resp = RequestSoap(cclient, logs, token);
					resp.getCustomerData().setPopupTitle(ConfigurationManager
							.getConfigurationFromCache("predefined.popuptitle." + cclient.getLang()));
					resp.getCustomerData().setPopupContent(ConfigurationManager
							.getConfigurationFromCache("predefined.popupcontent." + cclient.getLang()));
					resp.getCustomerData()
							.setFirstPopup(ConfigurationManager.getConfigurationFromCache("predefined.firstpopup"));
					resp.getCustomerData()
							.setLateOnPopup(ConfigurationManager.getConfigurationFromCache("predefined.lateronpopup"));

					String tariffIdsNumber = ConfigurationManager
							.getConfigurationFromCache("hidenumbers.forcoreservices");
					ArrayList<String> items = new ArrayList<String>(Arrays.asList(tariffIdsNumber.split(",")));
					resp.getCustomerData().setHideNumberTariffIds(items);
					logger.info(token + "<<<<<<<< <<<<< Resp >>>>> >>>>>>>>" + resp.toString());
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				} else {
					logger.info(token + "<<<<<<<< <<<<< ERROR_401_CODE >>>>> >>>>>>>>");
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.info(token + "Error", ex);
			logger.info(token + Helper.GetException(ex));
		}
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.updateLog(logs);
		return resp;
	}

	public static RequestHeader getRequestHeader() {

		// these are commenred because we do not hqve configurations in DB. So
		// we are hard coding. these should be from configurations in future.
		/*
		 * RequestHeader reqh = new RequestHeader(); String accessUser =
		 * ConfigurationManager.getConfigurationFromCache("crm.sub.AccessUser").
		 * trim();
		 * reqh.setChannelId(ConfigurationManager.getConfigurationFromCache(
		 * "crm.sub.ChannelId").trim());
		 * reqh.setTechnicalChannelId(ConfigurationManager.
		 * getConfigurationFromCache("crm.sub.TechnicalChannelId").trim());
		 * reqh.setAccessUser(accessUser);
		 * reqh.setTenantId(ConfigurationManager.getConfigurationFromCache(
		 * "crm.sub.TenantId").trim());
		 * reqh.setAccessPwd(ConfigurationManager.getConfigurationFromCache(
		 * "crm.sub.AccessPwd").trim());
		 * reqh.setTestFlag(ConfigurationManager.getConfigurationFromCache(
		 * "crm.sub.TestFlag").trim());
		 * reqh.setLanguage(ConfigurationManager.getConfigurationFromCache(
		 * "crm.sub.Language").trim());
		 * reqh.setTransactionId(Helper.generateTransactionID());
		 * logger.info("REQUEST HEADER IS >>>>>>>>>>>>>>>>>>> :" + reqh);
		 */
		com.huawei.crm.basetype.RequestHeader reqH = new com.huawei.crm.basetype.RequestHeader();
		reqH.setVersion("1");
		reqH.setChannelId("3");
		reqH.setTechnicalChannelId("53");
		reqH.setAccessUser("ecare");
		reqH.setAccessPwd("Abc1234%");
		reqH.setTransactionId("201901191000");
		reqH.setTenantId("101");
		reqH.setTestFlag("0");
		reqH.setLanguage("2002");

		return reqH;
	}

	public static GetCustomerRequestResponse RequestSoap(GetCustomerRequestClient cclient, Logs logs, String token) {
		GetCustomerRequestResponse rep11 = new GetCustomerRequestResponse();
		GetCustomerRequest cDR = new GetCustomerRequest();
		cDR.setRequestHeader(getRequestHeader());
		GetCustomerIn gCI = new GetCustomerIn();
		// gCI.setCustomerId(1010000134480l);
		gCI.setServiceNumber(cclient.getmsisdn());

		cDR.setGetCustomerBody(gCI);
		try {
			logger.info(token + " GetCustomerRequ" + cDR.toString());
//			GetCustomerResponse sR = CRMServices.getInstance().getCustomerData(cDR);
			GetCustomerResponse sR = ThirdPartyCall.getCustomerData(cDR);
			logger.info(token + " GetCustomerDataResponse " + Helper.ObjectToJson(sR));
			if (sR != null) {
				if (sR.getResponseHeader().getRetCode().equals("0")) {
					GetSubscriberResponse respns = new com.evampsaanga.b2b.azerfon.getsubscriber.CRMSubscriberService()
							.GetSubscriberRequest(cclient.getmsisdn());
					logger.info(token + " GetSubscriberResponse " + Helper.ObjectToJson(respns));
					if (respns.getResponseHeader().getRetCode().equals("0")) {
						CustomerData customer = new CustomerData();
						customer = customer.setResponseBody(sR, respns, token,cclient.getLang());

						rep11.setCustomerData(customer);
						logger.info(token + " CustomerResponseAfter SetBody " + Helper.ObjectToJson(customer));
						logger.info(token + " CustomerResponseAfter SubscriberType "
								+ rep11.getCustomerData().getSubscriberType());
						rep11.getCustomerData().getSubscriberType();
						// START Inititalizing Sepcial Offers Tariffs Data for
						// customer Data

						// We have to call orderquery GroupData API to get
						// GroupIds from Bakcell
						com.huawei.crm.query.GetGroupResponse responsegroups = AzerfonThirdPartyCalls
								.orderQueryGroupData(cclient.getmsisdn(), token);
					
						StringBuilder resultString = new StringBuilder();
				
						if(responsegroups.getGetGroupBody()!=null && responsegroups.getGetGroupBody().getGetGroupDataList()!=null && !responsegroups.getGetGroupBody().getGetGroupDataList().isEmpty() && responsegroups.getGetGroupBody().getGetGroupDataList().size()>0)
						{
							for(int i=0;i< responsegroups.getGetGroupBody().getGetGroupDataList().size();i++)
						   {
								resultString.append(responsegroups.getGetGroupBody().getGetGroupDataList().get(i).getGroupNo());
								if(i<responsegroups.getGetGroupBody().getGetGroupDataList().size()-1)
								{
									resultString.append(",");
								}
						   }
						logger.info(token+"GroupIds to be Passed To Magento GetGroup Azerfon API :"+resultString.toString());
						SpecialOffersRequest cclienrspecial = new SpecialOffersRequest();
						cclienrspecial.setLang(cclient.getLang());
						
						cclienrspecial.setCustomerGroup(resultString.toString());

						ProcessSpecialcustomerData specialData = ProcessingSpecialsOffersInternal
								.specialsForcustomerData(cclienrspecial, logger, logs, token);
						logger.info(
								token + "Response specialData Special TariffId :" + specialData.getTariffSpecialIds());
						rep11.getCustomerData().setSpecialOffersTariffData(specialData);
						rep11.getCustomerData().setGroupIds(resultString.toString());
						}
						else
						{
							logger.info(token+"GroupData and Groups are null or emptyfrom azerfon");
							ProcessSpecialcustomerData specialData=new ProcessSpecialcustomerData();
							specialData.setSpecialOffersMenu("hide");
							specialData.setSpecialTariffMenu("hide");
							rep11.getCustomerData().setSpecialOffersTariffData(specialData);
						}
						if(rep11.getCustomerData().getSubscriberType().equalsIgnoreCase("postpaid"))
						{
							//check from ODS if corporate Customer,Individual Customer in case of ODS is down we will set data as Individual Customer 
							String customerType=getCustomerIDFromView(cclient.getmsisdn(),logger,token)	;
							logger.info(token+"customerType From view :"+customerType);

								if(customerType!=null && customerType.equalsIgnoreCase("1"))
								{
									
									if(rep11.getCustomerData().getCustomerType().equalsIgnoreCase("Data Sim Postpaid"))
									{
										rep11.getCustomerData().setCustomerType(Constants.WTTX_CORPORATE_CUSTOMER_TYPE_ID_FROM_ODS);
									}
									else{
									rep11.getCustomerData().setCustomerType(Constants.CORPORATE_CUSTOMER_TYPE_ID_FROM_ODS);
									}
								}
								else if(customerType!=null && customerType.equalsIgnoreCase("0"))
								{
									//individual customer type check
									if(rep11.getCustomerData().getCustomerType().equalsIgnoreCase("Data Sim Postpaid"))
									{
										rep11.getCustomerData().setCustomerType(Constants.WTTX_INDIVIDUAL_CUSTOMER_TYPE_ID_FROM_ODS);
									}
									else{
									    rep11.getCustomerData().setCustomerType(Constants.DEFAULT_CUSTOMER_TYPE_IF_ODS_IS_DOWN);
									}
								}
								else
								{
									//if ODS is down set as corporate
									if(rep11.getCustomerData().getCustomerType().equalsIgnoreCase("Data Sim Postpaid"))
									{
										rep11.getCustomerData().setCustomerType(Constants.WTTX_INDIVIDUAL_CUSTOMER_TYPE_ID_FROM_ODS);
									}
									else{
									    rep11.getCustomerData().setCustomerType(Constants.DEFAULT_CUSTOMER_TYPE_IF_ODS_IS_DOWN);
									}
								}
							
						}
					
						logs.setUserType(rep11.getCustomerData().getSubscriberType());
						logs.setTariffId(rep11.getCustomerData().getOfferingId());
						logger.info(token + " ---SubscriberType-1--" + rep11.getCustomerData().getSubscriberType());
						rep11.setReturnCode(ResponseCodes.SUCESS_CODE_200);
						rep11.setReturnMsg(ResponseCodes.SUCESS_DES_200);
					//	return rep11;
					} else {
						rep11.setReturnCode(respns.getResponseHeader().getRetCode());
						rep11.setReturnMsg(respns.getResponseHeader().getRetMsg());
					//	return rep11;
					}
				} else if (sR.getResponseHeader().getRetCode().equals(ResponseCodes.MSISDN_NOT_FOUND_BK_CODE)) {
					rep11.setReturnCode(ResponseCodes.MSISDN_NOT_FOUND_BK_CODE);
					rep11.setReturnMsg(ResponseCodes.MSISDN_NOT_FOUND_BK_DES);
				} else {
					rep11.setReturnCode(sR.getResponseHeader().getRetCode());
					rep11.setReturnMsg(sR.getResponseHeader().getRetMsg());
				}
			}
		} catch (Exception ee) {
			if (ee instanceof WebServiceException) {
				rep11 = new GetCustomerRequestResponse();
				rep11.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
				rep11.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
			} else {
				rep11 = new GetCustomerRequestResponse();
				rep11.setReturnCode(ResponseCodes.SUCESS_CODE_200);
				rep11.setReturnMsg(ResponseCodes.SUCESS_DES_200);
			}
			logger.info(token + Helper.GetException(ee));
		}
		//start hard coding group just because we not getting any groups from azerfon
		logger.info(token +"Msisdn For GroupType : "+cclient.getmsisdn());
		/*if(cclient.getmsisdn().equals("776480544"))
		{
		rep11.getCustomerData().setGroupType("Full");
		logger.info(token +"Msisdn For GroupType Group : "+rep11.getCustomerData().getGroupType());
		}*/
		//start hard coding group just because we not getting any groups from azerfon
		return rep11;
	}
	public static String getCustomerIDFromView(String msisdn,Logger logger,String token) {
		logger.info("********START of getCustomerIDFromView***********"+msisdn);
		String customerType="";
		Connection myConnection = DBAzerfonFactory.getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try 
		{
			
			String sql = "select CUST_TYPE from V_E_CARE_CUST_INFO t where MSISDN = substr(?,-9)";
			logger.info(token+sql);
			statement = myConnection.prepareStatement(sql);
			statement.setString(1, msisdn);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				logger.info(token+"customerType: "+resultSet.getString("CUST_TYPE"));
				customerType=(resultSet.getString("CUST_TYPE"));
				
			}
			
		} catch (Exception e) {
			logger.info(token+Helper.GetException(e));
		}
		finally {
			try {
				if(statement!=null)
				{
					statement.close();
				}
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				logger.info(Helper.GetException(e));
			}
			try {
				if(resultSet!=null)
				{
					resultSet.close();
				}
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				logger.info(Helper.GetException(e));
			}
		}
		logger.info(token+"customerType From view :"+customerType);
	
		logger.info("********END of getCustomerIDFromView***********");
		return customerType;
	}
	public static void main(String[] args) {
		String name = "PIC_E-Care(Paybysubs)_Test";
		String name1 = name.toLowerCase();
		System.out.println(name1);
		if (name1.contains("paybysubs")) {
			System.out.println("YES Contains");
		} else {
			System.out.println("NO");
		}

	}
}
