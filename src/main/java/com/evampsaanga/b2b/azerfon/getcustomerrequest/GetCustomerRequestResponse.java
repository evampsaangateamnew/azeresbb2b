package com.evampsaanga.b2b.azerfon.getcustomerrequest;

import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;

public class GetCustomerRequestResponse extends BaseResponse {
	private com.evampsaanga.b2b.azerfon.getcustomerrequest.CustomerData customerData = new CustomerData();

	/**
	 * @return the customerData
	 */
	public com.evampsaanga.b2b.azerfon.getcustomerrequest.CustomerData getCustomerData() {
		return customerData;
	}

	/**
	 * @param customerData
	 *            the customerData to set
	 */
	public void setCustomerData(com.evampsaanga.b2b.azerfon.getcustomerrequest.CustomerData customerData) {
		this.customerData = customerData;
	}
}
