package com.evampsaanga.b2b.azerfon.getcustomerrequest;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.evampsaanga.b2b.developer.utils.SoapHandlerService;
import com.huawei.bss.soaif._interface.common.ReqHeader;
import com.huawei.bss.soaif._interface.corporateservice.CorporateInterfaces;
import com.huawei.bss.soaif._interface.corporateservice.CorporateServices;

public class CorporateServiceFactory {
	private static CorporateInterfaces portType = null;

	private CorporateServiceFactory() {
	}

	public static synchronized CorporateInterfaces getInstance() {
		if (portType == null) {
			portType = new CorporateServices().getCorporateServicePort();
			SoapHandlerService.configureBinding(portType);
		}
		return portType;
	}

	public static ReqHeader getRequestHeader() {
		ReqHeader reqHeader = new ReqHeader();
		reqHeader.setAccessPwd("r8q0a5WwGNboj9I35XzNcQ==");
		reqHeader.setChannelId("42");
		reqHeader.setAccessUser("dpc");
		reqHeader.setTransactionId(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
		reqHeader.setTenantId("101");
		return reqHeader;
	}
	
	public static void main(String[] args) {
		String msisdn = "555952541";
		String prefix = msisdn.substring(0, 2);
		String number = msisdn.substring(2, msisdn.length());
		System.out.println("Prefix= "+prefix+" , Number= "+number);
	}
}
