package com.evampsaanga.b2b.azerfon.getcustomerrequest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import org.apache.log4j.Logger;

import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.evampsaanga.b2b.developer.utils.SOAPLoggingHandler;
import com.evampsaanga.b2b.magento.specialoffers.ProcessSpecialcustomerData;
import com.huawei.crm.azerfon.bss.basetype.ExtParameterInfo;
import com.huawei.crm.azerfon.bss.basetype.GetSubOfferingInfo;
import com.huawei.crm.query.GetCustomerResponse;
import com.huawei.crm.azerfon.bss.query.GetSubscriberResponse;

public class CustomerData {
	public static final Logger logger = Logger.getLogger("azerfon-esb");
	private String entityId = "";

	/**
	 * @return the entityId
	 */
	public String getEntityId() {
		return entityId;
	}

	@Override
	public String toString() {
		return "CustomerData [entityId=" + entityId + ", title=" + title + ", email=" + email + ", simNumber=" + simNumber + ", firstName="
				+ firstName + ", middleName=" + middleName + ", lastName=" + lastName + ", customerType=" + customerType + ", gender=" + gender
				+ ", dob=" + dob + ", accountId=" + accountId + ", language=" + language + ", effectiveDate=" + effectiveDate + ", expiryDate="
				+ expiryDate + ", subscriberType=" + subscriberType + ", status=" + status + ", statusCode=" + statusCode + ", pinCode=" + pinCode
				+ ", pukCode=" + pukCode + ", pin2Code=" + pin2Code + ", puk2Code=" + puk2Code + ", billingLanguage=" + billingLanguage
				+ ", statusDetails=" + statusDetails + ", brandId=" + brandId + ", loyaltySegment=" + loyaltySegment + ", offeringId=" + offeringId
				+ ", supplementaryOfferingList=" + supplementaryOfferingList + ", brandName=" + brandName + ", msisdn=" + msisdn + ", offeringName="
				+ offeringName + ", offeringNameDisplay=" + offeringNameDisplay + ", groupType=" + groupType + ", PrimaryOffering=" + PrimaryOffering
				+ ", customerId=" + customerId + "]";
	}

	/**
	 * @param entityId
	 *            the entityId to set
	 */
	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	ArrayList<String> hideNumberTariffIds = null;
	private String title = "";
	private String email = "";

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	private String simNumber = "";

	/**
	 * @return the simNumber
	 */
	public String getSimNumber() {
		return simNumber;
	}

	/**
	 * @param simNumber
	 *            the simNumber to set
	 */
	public void setSimNumber(String simNumber) {
		this.simNumber = simNumber;
	}

	private String firstName = "";
	private String middleName = "";
	private String lastName = "";
	private String customerType = "";
	private String gender = "";
	private String dob = "";
	private Long accountId = 0L;
	private String language = "";
	private String effectiveDate = "";
	private String expiryDate = "";
	private String subscriberType = "";
	private String status = "";
	private String statusCode = "";
	public String getGroupIds() {
		return groupIds;
	}

	public void setGroupIds(String groupIds) {
		this.groupIds = groupIds;
	}

	private String groupIds="";

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	private String pinCode = "";
	private String pukCode = "";
	private String pin2Code = "";
	private String puk2Code = "";
	private String billingLanguage = "";
	private String statusDetails = "";
	private String brandId = "";
	private String loyaltySegment = "";
	private String offeringId = "";
	private List<com.evampsaanga.b2b.azerfon.getcustomerrequest.OfferingInfo> supplementaryOfferingList = new ArrayList<com.evampsaanga.b2b.azerfon.getcustomerrequest.OfferingInfo>();
	private String brandName = "";
	private String msisdn;
	private String offeringName = "";
	private String offeringNameDisplay = "";

	private String groupType;

	private String firstPopup;
	private String lateOnPopup;
	private String popupTitle;
	private String popupContent;

	public String getFirstPopup() {
		return firstPopup;
	}

	public void setFirstPopup(String firstPopup) {
		this.firstPopup = firstPopup;
	}

	public String getLateOnPopup() {
		return lateOnPopup;
	}

	public void setLateOnPopup(String lateOnPopup) {
		this.lateOnPopup = lateOnPopup;
	}

	public String getPopupTitle() {
		return popupTitle;
	}

	public void setPopupTitle(String popupTitle) {
		this.popupTitle = popupTitle;
	}

	public String getPopupContent() {
		return popupContent;
	}

	public void setPopupContent(String popupContent) {
		this.popupContent = popupContent;
	}

	/**
	 * @return the brandName
	 */
	public String getBrandName() {
		return brandName;
	}

	/**
	 * @param brandName
	 *            the brandName to set
	 */
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	/**
	 * @return the supplementaryOfferingList
	 */
	public List<com.evampsaanga.b2b.azerfon.getcustomerrequest.OfferingInfo> getSupplementaryOfferingList() {
		return supplementaryOfferingList;
	}

	/**
	 * @param supplementaryOfferingList
	 *            the supplementaryOfferingList to set
	 */
	public void setSupplementaryOfferingList(List<com.evampsaanga.b2b.azerfon.getcustomerrequest.OfferingInfo> supplementaryOfferingList) {
		this.supplementaryOfferingList = supplementaryOfferingList;
	}

	/**
	 * @return the msisdn
	 */
	public String getMsisdn() {
		return msisdn;
	}

	/**
	 * @param msisdn
	 *            the msisdn to set
	 */
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	/**
	 * @return the primaryOffering
	 */
	/**
	 * @return the pinCode
	 */
	public String getPinCode() {
		return pinCode;
	}

	/**
	 * @param pinCode
	 *            the pinCode to set
	 */
	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	/**
	 * @return the pukCode
	 */
	public String getPukCode() {
		return pukCode;
	}

	/**
	 * @param pukCode
	 *            the pukCode to set
	 */
	public void setPukCode(String pukCode) {
		this.pukCode = pukCode;
	}

	@XmlElement(name = "PrimaryOffering")
	private OfferingInfo PrimaryOffering = new OfferingInfo();

	/**
	 * @return the primaryOffering
	 */
	@XmlElement(name = "PrimaryOffering")
	public OfferingInfo getPrimaryOffering() {
		return PrimaryOffering;
	}

	/**
	 * @param primaryOffering
	 *            the primaryOffering to set
	 */
	public void setPrimaryOffering(OfferingInfo primaryOffering) {
		PrimaryOffering = primaryOffering;
	}

	private String customerId = "";

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public static void main(String[] args) throws ParseException {
		System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new SimpleDateFormat("yyyyMMddHHmmss").parse("20361231200000")));
	}

	public CustomerData setResponseBody(GetCustomerResponse responseBody, GetSubscriberResponse respns, String token,String language) {
		this.customerId = "" + responseBody.getGetCustomerBody().getCustomerId();
		if (responseBody.getGetCustomerBody() != null) {
			SOAPLoggingHandler.logger.info(token + " Setting customer Body");
			simNumber = respns.getGetSubscriberBody().getICCID();
			try {

				// in Aerphone Subscriber response the PIN, PUK is not encrypted
				// so commenteing these lines
				pinCode = /* TopUpEncrypterService.decrypt_data( */respns.getGetSubscriberBody().getPIN1();
				pukCode = /* TopUpEncrypterService.decrypt_data( */respns.getGetSubscriberBody().getPUK1();
				pin2Code = /* TopUpEncrypterService.decrypt_data( */respns.getGetSubscriberBody().getPIN2();
				puk2Code = /* TopUpEncrypterService.decrypt_data( */respns.getGetSubscriberBody().getPUK2();
			} catch (Exception e) {
				logger.info(token+Helper.GetException(e));
			}
			billingLanguage = respns.getGetSubscriberBody().getWrittenLanguage();
			// tobeSent
			if (responseBody.getGetCustomerBody().getTitle() != null) {
				this.title = ConfigurationManager.getConfigurationFromCache(ConfigurationManager.MAPPING_CRM_TITLE
						+ responseBody.getGetCustomerBody().getTitle());
			}
			if (respns.getGetSubscriberBody().getPrimaryOffering() != null) {
				String offeringshortname = "";
				String networkType = "";
				String offeringName = "";
				
				//commenting this loop. because in bakcell this was used to get Offeringname display,
			/*	for (ExtParameterInfo parmInfo : respns.getGetSubscriberBody().getPrimaryOffering().getExtParamList().getParameterInfo()) {
					if (parmInfo.getParamName().equals("OfferingShortName"))
						offeringshortname = parmInfo.getParamValue();
					if (parmInfo.getParamName().equals("NetworkType"))
						networkType = parmInfo.getParamValue();
					if (parmInfo.getParamName().equals("OfferingName")) {
						offeringName = parmInfo.getParamValue();
						this.offeringName = offeringName;
						this.offeringNameDisplay = ConfigurationManager.getConfigurationFromCache("tariff.name."
								+ respns.getGetSubscriberBody().getPrimaryOffering().getOfferingId().getOfferingId());
					}
				}*/
				this.offeringNameDisplay = ConfigurationManager.getConfigurationFromCache("tariff.name."
						+ respns.getGetSubscriberBody().getPrimaryOffering().getOfferingId().getOfferingId()+"."+language);
				logger.info(token+"offeringName Display After tariff.name."+this.offeringNameDisplay);
				this.offeringName = respns.getGetSubscriberBody().getPrimaryOffering().getOfferingName();
				try {
					// yyyy-MM-dd HH:mm:ss.SSS
					this.PrimaryOffering = new OfferingInfo("" + respns.getGetSubscriberBody().getPrimaryOffering().getOfferingId().getOfferingId(),
							offeringName, respns.getGetSubscriberBody().getPrimaryOffering().getOfferingId().getOfferingCode(), offeringshortname,
							ConfigurationManager.getConfigurationFromCache(ConfigurationManager.MAPPING_CRM_STATUS
									+ respns.getGetSubscriberBody().getPrimaryOffering().getStatus()), networkType, new SimpleDateFormat(
									"yyyy-MM-dd HH:mm:ss").format(new SimpleDateFormat("yyyyMMddHHmmss").parse(respns.getGetSubscriberBody()
									.getPrimaryOffering().getEffectiveTime())),
							new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new SimpleDateFormat("yyyyMMddHHmmss").parse(respns
									.getGetSubscriberBody().getPrimaryOffering().getExpiredTime())));
				} catch (ParseException ex) {
					logger.info(token+Helper.GetException(ex));
				}
			}
			if (respns.getGetSubscriberBody().getServiceNumber() != null)
				msisdn = respns.getGetSubscriberBody().getServiceNumber();

			if (responseBody.getGetCustomerBody().getFirstName() != null)
				this.firstName = getDummyNameOrMSISDN(responseBody.getGetCustomerBody().getFirstName(), msisdn);
			else
				this.firstName = "";

			if (responseBody.getGetCustomerBody().getMiddleName() != null)
				this.middleName = getDummyNameOrMSISDN(responseBody.getGetCustomerBody().getMiddleName(), msisdn);
			else
				this.middleName = "";

			if (responseBody.getGetCustomerBody().getLastName() != null)
				this.lastName = getDummyNameOrMSISDN(responseBody.getGetCustomerBody().getLastName(), msisdn);
			else
				this.lastName = "";

			if (responseBody.getGetCustomerBody().getCustomerType() != null) {
				logger.info(token+ "CustomerData customerType bakell :"+responseBody.getGetCustomerBody().getCustomerType());
				/*this.customerType = ConfigurationManager.getConfigurationFromCache(ConfigurationManager.MAPPING_CRM_CUSTOMER_TYPE
						+ respns.getGetSubscriberBody().getPrimaryOffering().getOfferingId().getOfferingId());*/
				logger.info(token+ "CustomerData customerType config :"+ConfigurationManager.MAPPING_CRM_CUSTOMER_TYPE
						+ respns.getGetSubscriberBody().getPrimaryOffering().getOfferingId().getOfferingId());
				
				String customerType=ConfigurationManager.getConfigurationFromCache(ConfigurationManager.MAPPING_CRM_CUSTOMER_TYPE
						+ respns.getGetSubscriberBody().getPrimaryOffering().getOfferingId().getOfferingId());
				if(customerType!=null && !customerType.isEmpty() && !customerType.equals(""))
				{
				this.customerType=ConfigurationManager.getConfigurationFromCache(ConfigurationManager.MAPPING_CRM_CUSTOMER_TYPE
						+ respns.getGetSubscriberBody().getPrimaryOffering().getOfferingId().getOfferingId());
				}
				else
				{
					this.customerType="Individual Customer";
				}
			
				logger.info(token+ "CustomerData customerType afterset1 :"+this.customerType);
			}
			if (responseBody.getGetCustomerBody().getGender() != null) {
				this.gender = ConfigurationManager.getConfigurationFromCache(ConfigurationManager.MAPPING_CRM_GENDER
						+ responseBody.getGetCustomerBody().getGender());
			}
			if (respns.getGetSubscriberBody().getBrandId() != null) {
				this.brandName = ConfigurationManager.getConfigurationFromCache(ConfigurationManager.MAPPING_HLR_BRANDID
						+ respns.getGetSubscriberBody().getBrandId());
			}
			if (responseBody.getGetCustomerBody().getDateOfBirth() != null) {
				try {
					this.dob = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new SimpleDateFormat("yyyyMMdd").parse(responseBody
							.getGetCustomerBody().getDateOfBirth()));
				} catch (ParseException ex) {
					SOAPLoggingHandler.logger.info(token+Helper.GetException(ex));
				}
			}
			if (respns.getGetSubscriberBody().getEffectiveDate() != null)
				try { //
					this.effectiveDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new SimpleDateFormat("yyyyMMddHHmmss").parse(respns
							.getGetSubscriberBody().getEffectiveDate()));
				} catch (ParseException ex1) {
					logger.info(token+Helper.GetException(ex1));
				}
			if (respns.getGetSubscriberBody().getExpireDate() != null)
				try {
					this.expiryDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new SimpleDateFormat("yyyyMMddHHmmss").parse(respns
							.getGetSubscriberBody().getExpireDate()));
				} catch (ParseException ex1) {
					logger.info(token+Helper.GetException(ex1));
				}
			if (respns.getGetSubscriberBody().getSubscriberType() != null)
				this.subscriberType = ConfigurationManager.getConfigurationFromCache(
						ConfigurationManager.MAPPING_HLR_SUBSCRIBER_TYPE + respns.getGetSubscriberBody().getSubscriberType()).toLowerCase();
			if (respns.getGetSubscriberBody().getStatus() != null) {
				this.status = ConfigurationManager.getConfigurationFromCache(ConfigurationManager.MAPPING_HLR_STATUS
						+ respns.getGetSubscriberBody().getStatus()
				// "B01"
						);
			}
			if (respns.getGetSubscriberBody().getStatusReason() != null)
				this.statusDetails = respns.getGetSubscriberBody().getStatusReason();
			if (respns.getGetSubscriberBody().getBrandId() != null)
				this.brandId = respns.getGetSubscriberBody().getBrandId();
			try {
				for (ExtParameterInfo offeringInfo : respns.getGetSubscriberBody().getExtParamList().getParameterInfo()) {
					if (offeringInfo.getParamName().equals("LoyaltySegment")) {
						String loyalty = ConfigurationManager.getConfigurationFromCache("loyalty." + offeringInfo.getParamValue());
						if (loyalty.isEmpty())
							loyalty = "Low";
						this.loyaltySegment = loyalty;
					}
				}
			} catch (Exception ex) {
				SOAPLoggingHandler.logger.error(Helper.GetException(ex));
			}
			if (respns.getGetSubscriberBody().getAccountId() != null)
				this.accountId = respns.getGetSubscriberBody().getAccountId();
			if (respns.getGetSubscriberBody().getLanguage() != null) {
				this.language = ConfigurationManager.getConfigurationFromCache(ConfigurationManager.MAPPING_CRM_LANGUAGE
						+ respns.getGetSubscriberBody().getLanguage());
			}
			if (respns.getGetSubscriberBody().getSupplementaryOfferingList() != null) {
				for (GetSubOfferingInfo offeringInfo : respns.getGetSubscriberBody().getSupplementaryOfferingList().getGetSubOfferingInfo()) {
					String getOfferingShortName = "";
					String networkType = "";
					String OfferingName = "";
					if (offeringInfo.getExtParamList() != null) {
						for (ExtParameterInfo paramInfo : offeringInfo.getExtParamList().getParameterInfo()) {
							if (paramInfo.getParamName().equals("OfferingShortName"))
								getOfferingShortName = paramInfo.getParamValue();
							if (paramInfo.getParamName().equals("NetworkType"))
								networkType = paramInfo.getParamValue();
							if (paramInfo.getParamName().equals("OfferingName"))
								OfferingName = paramInfo.getParamValue();
						}
					}
					try {
						this.supplementaryOfferingList.add(new com.evampsaanga.b2b.azerfon.getcustomerrequest.OfferingInfo(offeringInfo.getOfferingId()
								.getOfferingId(), OfferingName, offeringInfo.getOfferingId().getOfferingCode(), getOfferingShortName,
								ConfigurationManager.getConfigurationFromCache(ConfigurationManager.MAPPING_CRM_STATUS + offeringInfo.getStatus()),
								networkType, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new SimpleDateFormat("yyyyMMddHHmmss")
										.parse(offeringInfo.getEffectiveTime())), new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
										.format(new SimpleDateFormat("yyyyMMddHHmmss").parse(offeringInfo.getExpiredTime()))));
					} catch (ParseException ex) {
						logger.error(Helper.GetException(ex));
					}
				}
			}
			if (respns.getGetSubscriberBody().getPrimaryOffering() != null) {
				offeringId = respns.getGetSubscriberBody().getPrimaryOffering().getOfferingId().getOfferingId();
			}
			this.statusCode = respns.getGetSubscriberBody().getStatus();
			if (respns.getGetSubscriberBody().getPrimaryOffering().getOfferingName() != null) {
				// offeringName =
				// respns.getGetSubscriberBody().getPrimaryOffering().getOfferingName();
			}
		}

		if (this.firstName.equals(this.lastName)) {
			this.middleName = "";
			this.lastName = "";
		}
		return this;
	}

	private String getDummyNameOrMSISDN(String name, String msisdn) {
		String customerName = "";
		if (name.equalsIgnoreCase("No")) {
			customerName = msisdn;
		} else if (name.equalsIgnoreCase("Name")) {
			customerName = msisdn;
		} else if (name.equalsIgnoreCase("No Name")) {
			customerName = msisdn;
		} else if (name.equalsIgnoreCase("first")) {
			customerName = msisdn;
		} else if (name.equalsIgnoreCase("last")) {
			customerName = msisdn;
		} else if (name.equalsIgnoreCase("first name")) {
			customerName = msisdn;
		} else if (name.equalsIgnoreCase("last name")) {
			customerName = msisdn;
		} else if (name.equalsIgnoreCase("first name last name")) {
			customerName = msisdn;
		} else if (name.equalsIgnoreCase("Null")) {
			customerName = msisdn;
		} else {
			customerName = name;
		}
		return customerName;
	}

	/**
	 * @return the supplementaryOfferingList
	 */
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the middleName
	 */
	public String getMiddleName() {
		return middleName;
	}

	/**
	 * @param middleName
	 *            the middleName to set
	 */
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the customerType
	 */
	public String getCustomerType() {
		return customerType;
	}

	/**
	 * @param customerType
	 *            the customerType to set
	 */
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @param gender
	 *            the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * @return the dateOfBirth
	 */
	/**
	 * @return the dob
	 */
	public String getDob() {
		return dob;
	}

	/**
	 * @param dob
	 *            the dob to set
	 */
	public void setDob(String dob) {
		this.dob = dob;
	}

	/**
	 * @return the pin2Code
	 */
	public String getPin2Code() {
		return pin2Code;
	}

	/**
	 * @param pin2Code
	 *            the pin2Code to set
	 */
	public void setPin2Code(String pin2Code) {
		this.pin2Code = pin2Code;
	}

	/**
	 * @return the puk2Code
	 */
	public String getPuk2Code() {
		return puk2Code;
	}

	/**
	 * @param puk2Code
	 *            the puk2Code to set
	 */
	public void setPuk2Code(String puk2Code) {
		this.puk2Code = puk2Code;
	}

	/**
	 * @return the billingLanguage
	 */
	public String getBillingLanguage() {
		return billingLanguage;
	}

	/**
	 * @param billingLanguage
	 *            the billingLanguage to set
	 */
	public void setBillingLanguage(String billingLanguage) {
		this.billingLanguage = billingLanguage;
	}

	/**
	 * @return the offeringName
	 */
	public String getOfferingName() {
		return offeringName;
	}

	/**
	 * @param offeringName
	 *            the offeringName to set
	 */
	public void setOfferingName(String offeringName) {
		this.offeringName = offeringName;
	}

	/**
	 * @return the accountId
	 */
	public Long getAccountId() {
		return accountId;
	}

	/**
	 * @param accountId
	 *            the accountId to set
	 */
	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	/**
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * @param language
	 *            the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

	/**
	 * @return the effectiveDate
	 */
	public String getEffectiveDate() {
		return effectiveDate;
	}

	/**
	 * @param effectiveDate
	 *            the effectiveDate to set
	 */
	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	/**
	 * @return the expireDate
	 */
	public String getExpiryDate() {
		return expiryDate;
	}

	/**
	 * @param expireDate
	 *            the expireDate to set
	 */
	public void setExpiryDate(String expireDate) {
		this.expiryDate = expireDate;
	}

	/**
	 * @return the subscriberType
	 */
	public String getSubscriberType() {
		return subscriberType;
	}

	/**
	 * @param subscriberType
	 *            the subscriberType to set
	 */
	public void setSubscriberType(String subscriberType) {
		this.subscriberType = subscriberType;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the statusDetail
	 */
	public String getStatusDetails() {
		return statusDetails;
	}

	/**
	 * @param statusDetail
	 *            the statusDetail to set
	 */
	public void setStatusDetails(String statusDetail) {
		this.statusDetails = statusDetail;
	}

	/**
	 * @return the brandId
	 */
	public String getBrandId() {
		return brandId;
	}

	/**
	 * @param brandId
	 *            the brandId to set
	 */
	public void setBrandId(String brandId) {
		this.brandId = brandId;
	}

	/**
	 * @return the loyaltySegment
	 */
	public String getLoyaltySegment() {
		return loyaltySegment;
	}

	/**
	 * @param loyaltySegment
	 *            the loyaltySegment to set
	 */
	public void setLoyaltySegment(String loyaltySegment) {
		this.loyaltySegment = loyaltySegment;
	}

	/**
	 * @return the offering_id
	 */
	public String getOfferingId() {
		return offeringId;
	}

	/**
	 * @param offering_id
	 *            the offering_id to set
	 */
	public void setOfferingId(String offering_id) {
		this.offeringId = offering_id;
	}

	public String getOfferingNameDisplay() {
		return offeringNameDisplay;
	}

	public void setOfferingNameDisplay(String offeringNameDisplay) {
		this.offeringNameDisplay = offeringNameDisplay;
	}

	public String getGroupType() {
		return groupType;
	}

	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}

	public ArrayList<String> getHideNumberTariffIds() {
		return hideNumberTariffIds;
	}

	public void setHideNumberTariffIds(ArrayList<String> hideNumberTariffIds) {
		this.hideNumberTariffIds = hideNumberTariffIds;
	}

	public ProcessSpecialcustomerData getSpecialOffersTariffData() {
		return specialOffersTariffData;
	}

	public void setSpecialOffersTariffData(ProcessSpecialcustomerData specialOffersTariffData) {
		this.specialOffersTariffData = specialOffersTariffData;
	}

	private ProcessSpecialcustomerData specialOffersTariffData=new ProcessSpecialcustomerData();
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
}
