package com.evampsaanga.b2b.azerfon.querypaymentlog;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "QueryPaymentLogRequestClient")
public class QueryPaymentLogRequestClient extends BaseRequest {
	@XmlElement(name = "startDate", required = true)
	String startDate = "";
	@XmlElement(name = "endDate", required = true)
	String endDate = "";

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
}
