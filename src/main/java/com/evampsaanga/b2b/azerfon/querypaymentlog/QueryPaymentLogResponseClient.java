package com.evampsaanga.b2b.azerfon.querypaymentlog;

import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;
import com.huawei.bme.cbsinterface.arservices.QueryPaymentLogResult;

public class QueryPaymentLogResponseClient extends BaseResponse {
	QueryPaymentLogResult querypaymentlogresult = new QueryPaymentLogResult();

	public void setQueryPaymentLog(QueryPaymentLogResult queryPaymentLogResult) {
		querypaymentlogresult = queryPaymentLogResult;
	}
}
