package com.evampsaanga.b2b.azerfon.utilities;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class ConversionUtilities {
	public static String getDurationBreakdown(long millis) {
		if (millis < 0) {
			return "0 seconds";
		}
		long days = TimeUnit.MILLISECONDS.toDays(millis);
		millis -= TimeUnit.DAYS.toMillis(days);
		long hours = TimeUnit.MILLISECONDS.toHours(millis);
		millis -= TimeUnit.HOURS.toMillis(hours);
		long minutes = TimeUnit.MILLISECONDS.toMinutes(millis);
		millis -= TimeUnit.MINUTES.toMillis(minutes);
		long seconds = TimeUnit.MILLISECONDS.toSeconds(millis);
		StringBuilder sb = new StringBuilder(64);
		if (days > 0) {
			sb.append(days);
			sb.append(" Days ");
		}
		if (hours > 0) {
			sb.append(hours);
			sb.append(" Hours ");
		}
		if (minutes > 0) {
			sb.append(minutes);
			sb.append(" Minutes ");
		}
		if (seconds > 0) {
			sb.append(seconds);
			sb.append(" Seconds");
		}
		return (sb.toString());
	}

	public static String convertSecondToHourMinuteTime(String seconds) {
		String requiredFormat = "";
		Date datetime = new Date(((int) Double.parseDouble(seconds)) * 1000L);
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
		simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
		requiredFormat = simpleDateFormat.format(datetime);
		return requiredFormat;
	}

	public static String dataConversion(String bytesString, boolean si) {
		double bytes = Double.parseDouble(bytesString);
		int unit = si ? 1000 : 1024;
		if (bytes < unit)
			return bytes + " B";
		int exp = (int) (Math.log(bytes) / Math.log(unit));
		String pre = (si ? "KMGTPE" : "KMGTPE").charAt(exp - 1) + (si ? "" : "");
		String result = String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
		if (result.endsWith("KB") && result.contains(".")) {
			Double decimalValue = Double.parseDouble(result.substring(0, result.length() - 2));
			int intValue = decimalValue.intValue();
			decimalValue = decimalValue - intValue;
			return intValue + " KB " + ((long) (bytes) % 1024) + " B";
		} else if (result.endsWith("MB") && result.contains(".")) {
			Double decimalValue = Double.parseDouble(result.substring(0, result.length() - 2));
			int intValue = decimalValue.intValue();
			decimalValue = decimalValue - intValue;
			return intValue + " MB " + ((long) (bytes) % 1024) + " KB";
		} else if (result.endsWith("GB") && result.contains(".")) {
			Double decimalValue = Double.parseDouble(result.substring(0, result.length() - 2));
			int intValue = decimalValue.intValue();
			decimalValue = decimalValue - intValue;
			return intValue + " GB " + ((long) (bytes) % 1024) + " MB";
		} else if (result.endsWith("TB") && result.contains(".")) {
			Double decimalValue = Double.parseDouble(result.substring(0, result.length() - 2));
			int intValue = decimalValue.intValue();
			decimalValue = decimalValue - intValue;
			return intValue + " TB " + ((long) (bytes) % 1024) + " GB";
		} else if (result.endsWith("PB") && result.contains(".")) {
			Double decimalValue = Double.parseDouble(result.substring(0, result.length() - 2));
			int intValue = decimalValue.intValue();
			decimalValue = decimalValue - intValue;
			return intValue + " PB " + ((long) (bytes) % 1024) + " TB";
		} else if (result.endsWith("EB") && result.contains(".")) {
			Double decimalValue = Double.parseDouble(result.substring(0, result.length() - 2));
			int intValue = decimalValue.intValue();
			decimalValue = decimalValue - intValue;
			return intValue + " EB " + ((long) (bytes) % 1024) + " PB";
		}
		return "";
	}

	public static String numberFormattor(String number) {
		String formattedNumber = "";
		DecimalFormat decimalFormat = new DecimalFormat("#########0.00");
		double numberDouble = Double.parseDouble(number);
		formattedNumber = decimalFormat.format(numberDouble);
		return formattedNumber;
	}

	public static String numberFormattor(double number) {
		String formattedNumber = "";
		DecimalFormat decimalFormat = new DecimalFormat("#########0.00");
		formattedNumber = decimalFormat.format(number);
		return formattedNumber;
	}

	public static void main(String[] args) {
		System.out.println(getDurationBreakdown(1002893740));
	}

	public static String dataConversionkbs(String bytesString, boolean si) {
		double bytes = Double.parseDouble(bytesString);
		int unit = si ? 1000 : 1024;
		if (bytes < unit)
			return bytes + " B";
		int exp = (int) (Math.log(bytes) / Math.log(unit));
		String pre = (si ? "KMGTPE" : "KMGTPE").charAt(exp - 1) + (si ? "" : "");
		String result = String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
		if (result.endsWith("KB") && result.contains(".")) {
			Double decimalValue = Double.parseDouble(result.substring(0, result.length() - 2));
			int intValue = decimalValue.intValue();
			decimalValue = decimalValue - intValue;
			return intValue + " KB " + ((long) (bytes) % 1024) + " B";
		} else if (result.endsWith("MB") && result.contains(".")) {
			Double decimalValue = Double.parseDouble(result.substring(0, result.length() - 2));
			int intValue = decimalValue.intValue();
			decimalValue = decimalValue - intValue;
			return intValue + " MB " + ((long) (bytes) % 1024) + " KB";
		} else if (result.endsWith("GB") && result.contains(".")) {
			Double decimalValue = Double.parseDouble(result.substring(0, result.length() - 2));
			int intValue = decimalValue.intValue();
			decimalValue = decimalValue - intValue;
			return intValue + " GB " + ((long) (bytes) % 1024) + " MB";
		} else if (result.endsWith("TB") && result.contains(".")) {
			Double decimalValue = Double.parseDouble(result.substring(0, result.length() - 2));
			int intValue = decimalValue.intValue();
			decimalValue = decimalValue - intValue;
			return intValue + " TB " + ((long) (bytes) % 1024) + " GB";
		} else if (result.endsWith("PB") && result.contains(".")) {
			Double decimalValue = Double.parseDouble(result.substring(0, result.length() - 2));
			int intValue = decimalValue.intValue();
			decimalValue = decimalValue - intValue;
			return intValue + " PB " + ((long) (bytes) % 1024) + " TB";
		} else if (result.endsWith("EB") && result.contains(".")) {
			Double decimalValue = Double.parseDouble(result.substring(0, result.length() - 2));
			int intValue = decimalValue.intValue();
			decimalValue = decimalValue - intValue;
			return intValue + " EB " + ((long) (bytes) % 1024) + " PB";
		}
		return "";
	}
}
