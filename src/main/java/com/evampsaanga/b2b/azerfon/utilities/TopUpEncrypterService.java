package com.evampsaanga.b2b.azerfon.utilities;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class TopUpEncrypterService {
	private static String key = "1234567890111110";

	private TopUpEncrypterService() {
	}

	@SuppressWarnings("restriction")
	public static String encrypt_data(String data) throws Exception {
		SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(), "AES");
		Cipher cipher = Cipher.getInstance("AES");
		cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
		return new sun.misc.BASE64Encoder().encode(cipher.doFinal(data.getBytes()));
	}

	public static String decrypt_data(String data) throws Exception {
		SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(), "AES");
		Cipher cipher = Cipher.getInstance("AES");
		cipher.init(Cipher.DECRYPT_MODE, skeySpec);
		@SuppressWarnings("restriction")
		byte[] original = cipher.doFinal(new sun.misc.BASE64Decoder().decodeBuffer(data));
		return new String(original);
	}
	public static void main(String[] args) {
		try {
			System.out.println(decrypt_data("yEu5EVd2BZuj9ZAxhM8L8w=="));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
