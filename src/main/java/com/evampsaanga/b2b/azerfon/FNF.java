package com.evampsaanga.b2b.azerfon;

public class FNF {
	private String msisdn = "";
	private String date = "";

	public FNF(String msisdn, String date) {
		super();
		this.msisdn = msisdn;
		this.date = date;
	}

	/**
	 * @return the msisdn
	 */
	public String getMsisdn() {
		return msisdn;
	}

	/**
	 * @param msisdn the msisdn to set
	 */
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}
}
