package com.evampsaanga.b2b.azerfon.exchangeservice;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;
import org.springframework.util.SystemPropertyUtils;

import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.amqimplementationsesb.LogsType;
import com.evampsaanga.b2b.azerfon.db.DBFactory;
import com.evampsaanga.b2b.azerfon.getcustomerrequest.GetCustomerRequestClient;
import com.evampsaanga.b2b.azerfon.getcustomerrequest.GetCustomerRequestResponse;
import com.evampsaanga.b2b.azerfon.getvalues.exchangeservicecount.ExchangeServiceCountLand;
import com.evampsaanga.b2b.azerfon.getvalues.exchangeservicecount.ExchangeServiceCountResponse;
import com.evampsaanga.b2b.azerfon.hlrweb.getquerybalanceRequest.HLRBalanceServices;
import com.evampsaanga.b2b.azerfon.thirdpartycall.AzerfonThirdPartyCalls;
import com.evampsaanga.b2b.azerfon.ussdgwqueryfreeresource.FreeResourcesUSSDservices;
import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.ThirdPartyNames;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Decrypter;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.evampsaanga.b2b.developer.utils.SOAPLoggingHandler;
import com.evampsaanga.b2b.gethomepage.Balance;
import com.hazelcast.internal.util.ResultSet;
import com.huawei.bme.cbsinterface.arservices.AdjustmentResultMsg;
import com.huawei.bme.cbsinterface.arservices.QueryBalanceResultMsg;
import com.huawei.bme.cbsinterface.bbservices.QueryFreeUnitResultMsg;
import com.huawei.bss.soaif.chargesubscriber._interface.rechargeservice.ChargeForSubscriberRspMsg;

@Path("/azerfon")
public class ExchangeServiceRequestLand {
	public static final Logger logger = Logger.getLogger("azerfon-esb");

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ExchangeServiceResponse Get(@Header("credentials") String credential,
			@Header("Content-Type") String contentType, @Body() String requestBody) {
		Logs logs = new Logs();
		ExchangeServiceResponse resp = new ExchangeServiceResponse();

		String token = "";
		String TrnsactionName = Transactions.EXCHANGE_SERVICE_TRANSACTION_NAME;

		try {

			token = Helper.retrieveToken(TrnsactionName, Helper.getValueFromJSON(requestBody, "msisdn"));

			logger.info(token + "Request Landed on ExchangeServiceRequestLand:" + requestBody);
			logs.setRequestDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.setTransactionName(Transactions.EXCHANGE_SERVICE_TRANSACTION_NAME);
			logs.setThirdPartyName(ThirdPartyNames.EXCHANGE_SERVICE);
			logs.setTableType(LogsType.ExchangeService);

			ExchangeServiceRequestClient cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, ExchangeServiceRequestClient.class);
				if (cclient != null) {
					if (cclient.getIsB2B() != null && cclient.getIsB2B().equals("true")) {
						logs.setTransactionName(Transactions.EXCHANGE_SERVICE_TRANSACTION_NAME);
					}
					logs.setIp(cclient.getiP());
					logs.setChannel(cclient.getChannel());
					logs.setMsisdn(cclient.getmsisdn());

					logger.info(token + "TransactionName: " + logs.getTransactionName());
				}
			} catch (Exception ex) {
				logger.info(token + Helper.GetException(ex));
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					logger.info(token + "Unable to decrypt credentials");
					SOAPLoggingHandler.logger.info(token + Helper.GetException(ex));
				}
				if (credentials == null) {
					logger.info(token + "<<<<<<<< <<<<< credentials = null >>>>> >>>>>>>>");
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						logger.info(token + "<<<<<<<< <<<<< verification = null >>>>> >>>>>>>>");
						ExchangeServiceResponse res = new ExchangeServiceResponse();
						res.setReturnCode(ResponseCodes.ERROR_400_CODE);
						res.setReturnMsg(verification);
						logs.setResponseCode(res.getReturnCode());
						logs.setResponseDescription(res.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return res;
					}
				} else {
					logger.info(token + "<<<<<<<< <<<<< ERROR_MSISDN_CODE >>>>> >>>>>>>>");
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					
				
					ExchangeServiceCountResponse responCount=ExchangeServiceCountLand.exchangecount(cclient.getmsisdn(), token);
				
					if((responCount.getReturnCode().equals(ResponseCodes.SUCESS_CODE_200) &&Integer.parseInt(responCount.getCount())>=5))
					{
						resp.setReturnCode(ResponseCodes.EXCHNAGE_SERVICE_NOT_ELIGITBLE_CODE);
						resp.setReturnMsg(ResponseCodes.EXCHNAGE_SERVICE_NOT_ELIGITBLE_DESC);
						return resp;
					}
					
					//STRT OF balance logic. if balance is less than 0.20 azn then user is not eligible for exchange service
					
					HLRBalanceServices service = new HLRBalanceServices();
					QueryBalanceResultMsg balanceResponse = AzerfonThirdPartyCalls
							.queryBalanceResponseArServices(cclient.getmsisdn(), token);
				
					Balance balance=service.getBalance(cclient.getmsisdn(), "prepaid", balanceResponse, token);
					if(Double.parseDouble(balance.getPrepaid().getMainWallet().getAmount())<0.20)
					{
						resp.setReturnCode(ResponseCodes.BALANCE_LESS_THAN_REQUIRED_CODE);
						resp.setReturnMsg(ResponseCodes.BALANCE_LESS_THAN_REQUIRED_DESC);
						return resp;
					}
					
					// END OF balance logic. if balance is less than 0.20 azn then user is not eligible for exchange service
					// -------------------calling Free unit ---------------------//
					QueryFreeUnitResultMsg freeunitresp = AzerfonThirdPartyCalls.queryfreeUnitResponseThirdParty(token,
							cclient.getmsisdn(), logger);
				
					
					 String creditValues="";
					 String debitValues="";
					 
					 String creditUnitType="";
					 String debitUnitType="";
					 String expiryData="";
					 long crditValuetoBePassed=0;
						long DebitValuetoBePassed=0;
					if(freeunitresp.getQueryFreeUnitResult()!=null)
					{
						
						logger.info(token+"FREE UNIT LIST :"+Helper.ObjectToJson(freeunitresp.getQueryFreeUnitResult().getFreeUnitItem()));
					
						
						 
						   String fullPrepaidOfferingIds_2=ConfigurationManager.getConfigurationFromCache("exchange.service.prepaid.full.29.39.49").trim();
						   logger.info(token+ "--IN Ful 29, 39 ,49 TariffsIDs fullPrepaidOfferingIds_2 FROM cache or DB: --"+fullPrepaidOfferingIds_2);
						   String [] itemsFull_2 = fullPrepaidOfferingIds_2.split(",");
							java.util.List<String> listOfferingIDsFull_2 = Arrays.asList(itemsFull_2);
							
							  String fullPrepaidOfferingIds_1=ConfigurationManager.getConfigurationFromCache("exchange.service.prepaid.full.9.14.19").trim();
							  logger.info(token+ "--IN Ful 9, 14 ,19 TariffsIDs listOfferingIDsFull_1 FROM cache or DB:--"+fullPrepaidOfferingIds_1);
							  String [] itemsFull_1 = fullPrepaidOfferingIds_1.split(",");
								java.util.List<String> listOfferingIDsFull_1 = Arrays.asList(itemsFull_1);
								
								logger.info(token+ "--IN Ful 9, 14 ,19 TariffsIDs listOfferingIDsFull_1--"+listOfferingIDsFull_1);
								logger.info(token+ "--IN Ful 29, 39 ,49 TariffsIDs listOfferingIDsFull_2--"+listOfferingIDsFull_2);
						for(int i=0; i<freeunitresp.getQueryFreeUnitResult().getFreeUnitItem().size();i++)
						{
						   String unityTpe=freeunitresp.getQueryFreeUnitResult().getFreeUnitItem().get(i).getFreeUnitType();
						  logger.info(token+"UNIT-TYPE :"+unityTpe);
						  
							
							//START OF for full 9-14-19 tariffIds
						
								
								//  listOfferingIDsFull_1= full 9/14/19     , 
								if(listOfferingIDsFull_1.contains(cclient.getOfferingId()))
								{
									logger.info(token+ "--IN Ful 9, 14 ,19 Tariffs Contans CHeck--");
									if(cclient.getIsDataToVoiceConversion().equals("true"))
									{
										/*DB : <bbs:FreeUnitType>C_Nar_Garden_Data</bbs:FreeUnitType>
									    CB : <bbs:FreeUnitType>C_Full_All_net_minutes</bbs:FreeUnitType> */
										logger.info(token+ "--IN Ful 9, 14 ,19--  DATA TO VOICE CHECK, We have to get Debit (DATA) and Credit (MINUTES) Values--");
									   if(freeunitresp.getQueryFreeUnitResult().getFreeUnitItem().get(i).getFreeUnitType().equalsIgnoreCase("C_Nar_Garden_Data"))
									   {
										  debitValues=""+freeunitresp.getQueryFreeUnitResult().getFreeUnitItem().get(i).getTotalUnusedAmount();
										  logger.info(token+ "--IN Ful 9, 14 ,19-- debitValue Data FreeUnit C_Nar_Garden_Data :"+creditValues);
										  
										  
										  debitUnitType=freeunitresp.getQueryFreeUnitResult().getFreeUnitItem().get(i).getFreeUnitType();
										  logger.info(token+ "--IN Ful 9, 14 ,19-- debitUnitType  FreeUnit C_Nar_Garden_Data :"+creditUnitType);
										  
										  
										  creditValues=cclient.getVoiceValue();
										  logger.info(token+ "--IN Ful 9, 14 ,19-- creditValues Minutes FreeUnit C_Full_All_net_minutes :"+creditValues);
										  
										  creditUnitType="C_Full_All_net_minutes";
										  logger.info(token+ "--IN Ful 9, 14 ,19-- creditUnitType  FreeUnit C_Full_All_net_minutes :"+creditUnitType);
										  for(int j=0 ;j<freeunitresp.getQueryFreeUnitResult().getFreeUnitItem().get(i).getFreeUnitItemDetail().size(); j++)
										  {
											  expiryData=freeunitresp.getQueryFreeUnitResult().getFreeUnitItem().get(i).getFreeUnitItemDetail().get(j).getExpireTime();
										  }
									   }								
					
									}
									else
									{
										logger.info(token+ "--IN Ful 9, 14 ,19--  VOICE TO DATA CHECK e have to get Debit (MINUTES) and Credit (DATA) Values----");
									/*	DB : <bbs:FreeUnitType>C_Nar_Garden_All_net_minutes</bbs:FreeUnitType> 
								     	CB :  <bbs:FreeUnitType>C_FULL_Data_Trans</bbs:FreeUnitType>*/
									
										   if(freeunitresp.getQueryFreeUnitResult().getFreeUnitItem().get(i).getFreeUnitType().equalsIgnoreCase("C_Nar_Garden_All_net_minutes"))
										   {
											  debitValues=""+freeunitresp.getQueryFreeUnitResult().getFreeUnitItem().get(i).getTotalUnusedAmount();
											  logger.info(token+ "--IN Ful 9, 14 ,19-- debitValues MINUTES FreeUnit C_Nar_Garden_All_net_minutes :"+debitValues);
											  
											  debitUnitType=freeunitresp.getQueryFreeUnitResult().getFreeUnitItem().get(i).getFreeUnitType();
											  logger.info(token+ "--IN Ful 9, 14 ,19-- debitUnitType  FreeUnit C_Nar_Garden_All_net_minutes :"+debitUnitType);
										       
											  creditValues=cclient.getDataValue();
											  logger.info(token+ "--IN Ful 9, 14 ,19-- creditValues Data FreeUnit C_FULL_Data_Trans :"+creditValues);
											  
											  creditUnitType="C_FULL_Data_Trans";
											  logger.info(token+ "--IN Ful 9, 14 ,19-- creditUnitType  FreeUnit C_FULL_Data_Trans :"+creditUnitType);
											  for(int j=0 ;j<freeunitresp.getQueryFreeUnitResult().getFreeUnitItem().get(i).getFreeUnitItemDetail().size(); j++)
											  {
												  expiryData=freeunitresp.getQueryFreeUnitResult().getFreeUnitItem().get(i).getFreeUnitItemDetail().get(j).getExpireTime();
											  }
										   }

									}
								}
								//END OF for full 9-14-19 tariffIds
								 
									
									if(listOfferingIDsFull_2.contains(cclient.getOfferingId()))
									{
										logger.info(token+ "--IN Ful 29, 39 ,49 Tariffs ContainsCHeck--");
										if(cclient.getIsDataToVoiceConversion().equals("true"))
										{
											logger.info(token+ "--IN Ful 29, 39 ,49--  DATA TO VOICE CHECK--");
											/*DB : <bbs:FreeUnitType>C_Nar_Garden_Data</bbs:FreeUnitType>
										    CB : <bbs:FreeUnitType>C_FULL_Off_net_minutes</bbs:FreeUnitType>*/
											

											   if(freeunitresp.getQueryFreeUnitResult().getFreeUnitItem().get(i).getFreeUnitType().equalsIgnoreCase("C_Nar_Garden_Data"))
											   {
												  debitValues=""+freeunitresp.getQueryFreeUnitResult().getFreeUnitItem().get(i).getTotalUnusedAmount();
												  logger.info(token+ "--IN Ful 29, 39 ,49--debitValues Data FreeUnit C_Nar_Garden_Data :"+debitValues);
												  
												  debitUnitType=freeunitresp.getQueryFreeUnitResult().getFreeUnitItem().get(i).getFreeUnitType();
												  logger.info(token+ "--IN Ful 29, 39 ,49--debitUnitType Data FreeUnit C_Nar_Garden_Data :"+debitUnitType);
											     
												  creditValues=cclient.getVoiceValue();
												  logger.info(token+ "--IN Ful 29, 39 ,49-- creditValues Minutes FreeUnit C_FULL_Off_net_minutes :"+creditValues);
												  
												  creditUnitType="C_FULL_Off_net_minutes";
												  logger.info(token+ "--IN Ful 29, 39 ,49-- creditUnitType  FreeUnit C_FULL_Off_net_minutes :"+creditUnitType);
												  for(int j=0 ;j<freeunitresp.getQueryFreeUnitResult().getFreeUnitItem().get(i).getFreeUnitItemDetail().size(); j++)
												  {
													  expiryData=freeunitresp.getQueryFreeUnitResult().getFreeUnitItem().get(i).getFreeUnitItemDetail().get(j).getExpireTime();
												  }
											   }
											   
										}
										else
										{
											/*DB :  <bbs:FreeUnitType>C_Nar_Garden_Off_net_minutes</bbs:FreeUnitType>
										    CB :  <bbs:FreeUnitType>C_FULL_Data_Trans</bbs:FreeUnitType>*/
											logger.info(token+ "--IN Ful 29, 39 ,49--  VOICE TO DATA CHECK--");
										
											   if(freeunitresp.getQueryFreeUnitResult().getFreeUnitItem().get(i).getFreeUnitType().equalsIgnoreCase("C_Nar_Garden_Off_net_minutes"))
											   {
												  debitValues=""+freeunitresp.getQueryFreeUnitResult().getFreeUnitItem().get(i).getTotalUnusedAmount();
												  logger.info(token+ "--IN Ful 29, 39 ,49--debitValues Minutes FreeUnit C_Nar_Garden_Off_net_minutes :"+debitValues);
												  debitUnitType=""+freeunitresp.getQueryFreeUnitResult().getFreeUnitItem().get(i).getFreeUnitType();
												  logger.info(token+ "--IN Ful 29, 39 ,49--debitUnitType Minutes FreeUnit C_Nar_Garden_Off_net_minutes :"+debitUnitType);
											      
												  creditValues=cclient.getDataValue();
												  logger.info(token+ "--IN Ful 29, 39 ,49--creditValues Data FreeUnit C_FULL_Data_Trans :"+creditValues);
												  
												  creditUnitType="C_FULL_Data_Trans";
												  logger.info(token+ "--IN Ful 29, 39 ,49--creditUnitType Data FreeUnit C_FULL_Data_Trans :"+creditUnitType);
												  for(int j=0 ;j<freeunitresp.getQueryFreeUnitResult().getFreeUnitItem().get(i).getFreeUnitItemDetail().size(); j++)
												  {
													  expiryData=freeunitresp.getQueryFreeUnitResult().getFreeUnitItem().get(i).getFreeUnitItemDetail().get(j).getExpireTime();
												  }
											   }
										}
									}
						   }
					
						logger.info(token+ "creditValues :" +creditValues);
						logger.info(token+ "debitValues :" +debitValues);
						
						
					//	AdjustmentResultMsg responseAdjustment=AzerfonThirdPartyCalls.adjustmentResponse(token,debitValues, creditValues, debitUnitType, creditUnitType,cclient.getmsisdn());
					if(creditValues!=null && !creditValues.isEmpty() && !creditValues.equalsIgnoreCase("") && debitValues!=null && !debitValues.isEmpty() && !debitValues.equals(""))
					{
						if(cclient.getIsDataToVoiceConversion().equals("true"))
						{
							DebitValuetoBePassed=Long.parseLong(cclient.getDataValue())*1024;
							crditValuetoBePassed=Long.parseLong(cclient.getVoiceValue())*60;
						   if( (Long.parseLong(cclient.getDataValue())*1000)> Long.parseLong(debitValues))
						   {
							resp.setReturnCode(ResponseCodes.MBS_GREATER_THAN_FREEUNIT_DATA_CODE);
							resp.setReturnMsg(ResponseCodes.MBS_GREATER_THAN_FREEUNIT_DATA_DESC);
							logs.setResponseCode(ResponseCodes.MBS_GREATER_THAN_FREEUNIT_DATA_CODE);
							logs.setResponseDescription(ResponseCodes.MBS_GREATER_THAN_FREEUNIT_DATA_CODE);
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						   }
						   
						}
						else
						{
							crditValuetoBePassed=Long.parseLong(cclient.getDataValue())*1024;
							DebitValuetoBePassed	=Long.parseLong(cclient.getVoiceValue())*60;
							if( (Long.parseLong(cclient.getVoiceValue())*60)> Long.parseLong(debitValues))
							{
								resp.setReturnCode(ResponseCodes.MINUTES_GREATER_THAN_FREEUNIT_MINUTES_CODE);
								resp.setReturnMsg(ResponseCodes.MINUTES_GREATER_THAN_FREEUNIT_MINUTES_DESC);
								logs.setResponseCode(ResponseCodes.MINUTES_GREATER_THAN_FREEUNIT_MINUTES_CODE);
								logs.setResponseDescription(ResponseCodes.MINUTES_GREATER_THAN_FREEUNIT_MINUTES_DESC);
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
								return resp;
							}
						}
					}
					else
					{
						resp.setReturnCode(ResponseCodes.MINUTES_MBs_EMPT_FROM_AZERFON_FREEUNIT_CODE);
						resp.setReturnMsg(ResponseCodes.MINUTES_MBs_EMPT_FROM_AZERFON_FREEUNIT_DESC);
						logs.setResponseCode(ResponseCodes.MINUTES_MBs_EMPT_FROM_AZERFON_FREEUNIT_CODE);
						logs.setResponseDescription(ResponseCodes.MINUTES_MBs_EMPT_FROM_AZERFON_FREEUNIT_DESC);
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
					} //if query unit is not null if 
					AdjustmentResultMsg responseAdjustment=AzerfonThirdPartyCalls.adjustmentResponse(token,DebitValuetoBePassed,crditValuetoBePassed,debitUnitType,creditUnitType,cclient.getmsisdn(),expiryData);
					
					if(responseAdjustment.getResultHeader().getResultCode().equals("0"))
					{
						// START Here we have to implement the logic for chaargeCustomer
						ChargeForSubscriberRspMsg responsECharge=AzerfonThirdPartyCalls.responseChargecustomer(token, cclient.getmsisdn(),"405661");
                           
						// ENDHere we have to implement the logic for chaargeCustomer
						java.sql.PreparedStatement preparedStatement=null;
						/*if(responCount.getReturnCode().equals(ResponseCodes.SUCESS_CODE_200) && responCount.getCount().equals("0"))
						{
						 logger.info(token+"Record Already Exists, but count is Zero due to days >30");
							//java.sql.ResultSet resultSet=null;
							String sql="UPDATE exchangeservices_usercount SET count=? , created_at=NOW() , updated_at=NOW() WHERE msisdn=?";
							
							try
							{
								logger.info("msisdn : "+cclient.getmsisdn());
								String msisdn=cclient.getmsisdn();
								preparedStatement =DBFactory.getDbConnection().prepareStatement(sql);
								preparedStatement.setInt(1, 1);
							
								preparedStatement.setString(1, msisdn);
								//logger.info(token+preparedStatement);
							int resultSet=preparedStatement.executeUpdate();
							logger.info(token+"from zero to 1 Result Update: "+resultSet);
						   
						    resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
							logs.setResponseCode(ResponseCodes.SUCESS_CODE_200);
							logs.setResponseDescription(ResponseCodes.SUCESS_DES_200);
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							logger.info(token+"--Record updated from zero to 1--");
							return resp;
							}
							catch (Exception e) {
								logger.info(token+Helper.GetException(e));
								resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
								resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
								
								logs.setResponseCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
								logs.setResponseDescription(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
							}
							
						}
						else
						{*/
							logger.info(token+"Record doest not Exist or days are <30  so we have to update count ot insert new record");
							String sql="INSERT INTO exchangeservices_usercount(msisdn, created_at) VALUES (?,NOW())";
						
							try
							{
								logger.info("msisdn : "+cclient.getmsisdn());
								String msisdn=cclient.getmsisdn();
								preparedStatement =DBFactory.getDbConnection().prepareStatement(sql);
								preparedStatement.setString(1, msisdn);
								//logger.info(token+preparedStatement);
							int resultSet=preparedStatement.executeUpdate();
							logger.info(token+"Result Update: "+resultSet);
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
							logs.setResponseCode(ResponseCodes.SUCESS_CODE_200);
							logs.setResponseDescription(ResponseCodes.SUCESS_DES_200);
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
					
							}
							catch (Exception e) {
								logger.info(token+Helper.GetException(e));
								resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
								resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
								logs.setResponseCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
								logs.setResponseDescription(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
								return resp;
							}
						//}
						
					}
					else
					{
						resp.setReturnCode(responseAdjustment.getResultHeader().getResultCode());
						resp.setReturnMsg(responseAdjustment.getResultHeader().getResultDesc());
						logs.setResponseCode(responseAdjustment.getResultHeader().getResultCode());
						logs.setResponseDescription(responseAdjustment.getResultHeader().getResultDesc());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
					
				} else {
					logger.info(token + "<<<<<<<< <<<<< ERROR_401_CODE >>>>> >>>>>>>>");
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.info(token + "Error", ex);
			logger.info(token + Helper.GetException(ex));
		}
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.updateLog(logs);
		return resp;
	}
	
	public static void main(String[] args) {
		
		List<String> HideForArrrayAfterConversion = null;
     String comma="123,123,123,,345";
     HideForArrrayAfterConversion = new ArrayList<String>(Arrays.asList(comma.split(",")));
     System.out.println(HideForArrrayAfterConversion);
     
//     String comma1=null;
// 	String [] arr=comma1.split(",");
    
  
//    System.out.println(arr);
	}
		
		
		
		
	

}
