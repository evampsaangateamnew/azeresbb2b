package com.evampsaanga.b2b.azerfon.queryinvoiceV2;

public class QueryInvoiceResponseData {
	private String invoiceAmount;
	private String invoiceDate;
	private String dueDate;
	private String SettleDate;
	private String invoiceDateDisp;
	private String dueDateDisp;
	private String status;
	private String SettleDateDisp;
	private String daysDisp;
	private String totalAmountToBePaidLabel;
	private String invoiceLabel;
	private String totalDays;
	private String remainingDays;
	


	public String getSettleDateDisp() {
		return SettleDateDisp;
	}

	public void setSettleDateDisp(String settleDateDisp) {
		SettleDateDisp = settleDateDisp;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getInvoiceAmount() {
		return invoiceAmount;
	}

	public void setInvoiceAmount(String invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}

	public String getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getDueDate() {
		return dueDate;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}

	public String getInvoiceDateDisp() {
		return invoiceDateDisp;
	}

	public void setInvoiceDateDisp(String invoiceDateDisp) {
		this.invoiceDateDisp = invoiceDateDisp;
	}

	public String getDueDateDisp() {
		return dueDateDisp;
	}

	public void setDueDateDisp(String dueDateDisp) {
		this.dueDateDisp = dueDateDisp;
	}

	public String getSettleDate() {
		return SettleDate;
	}

	public void setSettleDate(String settleDate) {
		SettleDate = settleDate;
	}

	public String getTotalAmountToBePaidLabel() {
		return totalAmountToBePaidLabel;
	}

	public void setTotalAmountToBePaidLabel(String totalAmountToBePaidLabel) {
		this.totalAmountToBePaidLabel = totalAmountToBePaidLabel;
	}

	public String getInvoiceLabel() {
		return invoiceLabel;
	}

	public void setInvoiceLabel(String invoiceLabel) {
		this.invoiceLabel = invoiceLabel;
	}

	public String getTotalDays() {
		return totalDays;
	}

	public void setTotalDays(String totalDays) {
		this.totalDays = totalDays;
	}

	public QueryInvoiceResponseData() {
		this.invoiceAmount = "";
		this.invoiceDate = "";
		this.dueDate = "";
		this.SettleDate = "";
		this.invoiceDateDisp = "";
		this.dueDateDisp = "";
		this.status = "";
		this.SettleDateDisp = "";
		this.daysDisp = "";
		this.totalAmountToBePaidLabel = "";
		this.invoiceLabel = "";
		this.totalDays = "";
	}

	public QueryInvoiceResponseData(String invoiceAmount, String invoiceDate, String dueDate, String settleDate,
			String invoiceDateDisp, String dueDateDisp, String status, String settleDateDisp, String days,
			String totalAmountToBePaidLabel, String invoiceLabel, String totalDays,String remainingDays) {
		super();
		this.invoiceAmount = invoiceAmount;
		this.invoiceDate = invoiceDate;
		this.dueDate = dueDate;
		this.SettleDate = settleDate;
		this.invoiceDateDisp = invoiceDateDisp;
		this.dueDateDisp = dueDateDisp;
		this.status = status;
		this.SettleDateDisp = settleDateDisp;
		this.daysDisp = days;
		this.totalAmountToBePaidLabel = totalAmountToBePaidLabel;
		this.invoiceLabel = invoiceLabel;
		this.totalDays = totalDays;
		this.remainingDays=remainingDays;
		
	}

	public String getRemainingDays() {
		return remainingDays;
	}

	public void setRemainingDays(String remainingDays) {
		this.remainingDays = remainingDays;
	}

	public String getDaysDisp() {
		return daysDisp;
	}

	public void setDaysDisp(String daysDisp) {
		this.daysDisp = daysDisp;
	}

	
}
