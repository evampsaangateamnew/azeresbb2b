package com.evampsaanga.b2b.azerfon.queryinvoiceV2;

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;

public class QueryInvoiceRequestData extends BaseRequest {
	private String customerID;
	private String startTime;
	private String endTime;

	public String getCustomerID() {
		return customerID;
	}

	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

}
