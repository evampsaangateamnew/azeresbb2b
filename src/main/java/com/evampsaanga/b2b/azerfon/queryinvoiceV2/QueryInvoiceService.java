package com.evampsaanga.b2b.azerfon.queryinvoiceV2;

import java.io.IOException;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.amqimplementationsesb.LogsType;
import com.evampsaanga.b2b.authorization.AuthorizationAndAuthentication;
import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.ThirdPartyNames;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.huawei.bme.cbsinterface.arservices.QueryInvoiceRequest;
import com.huawei.bme.cbsinterface.arservices.QueryInvoiceRequest.TimePeriod;
import com.huawei.bme.cbsinterface.arservices.QueryInvoiceRequestMsg;
import com.huawei.bme.cbsinterface.arservices.QueryInvoiceResult.InvoiceInfo;
import com.huawei.bme.cbsinterface.arservices.QueryInvoiceResultMsg;
import com.ngbss.evampsaanga.services.ThirdPartyCall;
import com.ngbss.evampsaanga.services.ThirdPartyRequestHeader;

/**
 * 
 * @author Aqeel Abbas Queries the individual invoices under the pic
 *
 */
@Path("/bakcell")
public class QueryInvoiceService {
	public static final Logger logger = Logger.getLogger("azerfon-esb");

	/**
	 * 
	 * @param requestBody
	 * @param credential
	 * @return invoice Response
	 * @throws IOException
	 * @throws Exception
	 */
	@POST
	@Path("/queryinvoice")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public QueryInvoiceResponse queryInvoice(@Body String requestBody, @Header("credentials") String credential)
			throws IOException, Exception {
		// Logging incoming request to log file

		// Below is declaration block for variables to be used
		// Logs object to store values which are to be inserted in database for
		// reporting
		QueryInvoiceRequestData queryInvoiceRequest = new QueryInvoiceRequestData();
		QueryInvoiceResponse queryInvoiceResponse = new QueryInvoiceResponse();

		Logs logs = new Logs();
		// Generic information about logs is being set to logs object
		logs.setRequestDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.setTransactionName(Transactions.INVOICE);
		logs.setThirdPartyName(ThirdPartyNames.LOGIN);
		logs.setTableType(LogsType.CustomerData);
		// Authenticating the request using credentials string received in
		// header
		boolean authenticationresult = AuthorizationAndAuthentication.authenticateAndAuthorizeCredentials(credential);

		// if request is not authenticated adding unauthorized response
		// codes to response and logs object
		if (!authenticationresult)
			prepareErrorResponse(queryInvoiceResponse, logs, ResponseCodes.ERROR_401_CODE, ResponseCodes.ERROR_401);

		try {
			queryInvoiceRequest = Helper.JsonToObject(requestBody, QueryInvoiceRequestData.class);
			Helper.logInfoMessageV2(queryInvoiceRequest.getmsisdn()
					+ " - Request lanaded on queryinvoice with data as :" + requestBody);
			// logger.info("@@<<<<<<<<<<<<<<<<<<<<< query invoice start
			// >>>>>>>>>>>>>>>>>>>>>@@");
		} catch (Exception ex) {
			// block catches the exception from mapper and sets the 400 bad
			// request response code
			logger.error(Helper.GetException(ex));
			prepareErrorResponse(queryInvoiceResponse, logs, ResponseCodes.ERROR_400_CODE, ResponseCodes.ERROR_400);
		}
		if (queryInvoiceRequest != null && authenticationresult) {

			queryInvoiceResponse = this.queryInvoiceCall(queryInvoiceRequest, logs);
		}
		return queryInvoiceResponse;
	}

	public QueryInvoiceResponse queryInvoiceCall(QueryInvoiceRequestData queryInvoiceRequest, Logs logs)
			throws Exception {

		QueryInvoiceResponseData responseData = new QueryInvoiceResponseData();
		List<QueryInvoiceResponseData> responseDataList = new ArrayList<>();
		QueryInvoiceResponse queryInvoiceResponse = new QueryInvoiceResponse();

		QueryInvoiceRequestMsg queryInvoiceRequestMsg = new QueryInvoiceRequestMsg();
		QueryInvoiceRequest invoicerequest = new QueryInvoiceRequest();
		com.huawei.bme.cbsinterface.arservices.QueryInvoiceRequest.AcctAccessCode actAccessCode = new com.huawei.bme.cbsinterface.arservices.QueryInvoiceRequest.AcctAccessCode();
		actAccessCode.setAccountCode(queryInvoiceRequest.getCustomerID());
		invoicerequest.setAcctAccessCode(actAccessCode);
		TimePeriod TimePeriod = new TimePeriod();
		TimePeriod.setEndTime(queryInvoiceRequest.getEndTime());
		TimePeriod.setStartTime(queryInvoiceRequest.getStartTime());
		invoicerequest.setTimePeriod(TimePeriod);
		queryInvoiceRequestMsg.setQueryInvoiceRequest(invoicerequest);
		// queryInvoiceRequestMsg.setRequestHeader(CBSARService.getRequestPaymentRequestHeader());
		queryInvoiceRequestMsg.setRequestHeader(ThirdPartyRequestHeader.getRequestPaymentRequestHeader());
		// QueryInvoiceResultMsg resultmsg =
		// CBSARService.getInstance().queryInvoice(queryInvoiceRequestMsg);
		QueryInvoiceResultMsg resultmsg = ThirdPartyCall.getQueryInvoice(queryInvoiceRequestMsg);
		Helper.logInfoMessageV2(
				queryInvoiceRequest.getmsisdn() + "- Response from CBSAR: " + Helper.ObjectToJson(resultmsg));
		if (resultmsg.getQueryInvoiceResult() != null) {
			
			logger.info("Calling checkIfInvoicesPaid Method with Request:" + Helper.ObjectToJson(resultmsg));
		InvoiceInfo invInfo  = checkIfInvoicesPaid(resultmsg);
			
			
				String dueDate = invInfo.getDueDate();
				DateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
				Date date = (Date) formatter.parse(dueDate);
				SimpleDateFormat newFormat = new SimpleDateFormat("yyyy-MM-dd");
				String dueDateNDisp = newFormat.format(date);
				// date = (Date) newFormat.parse(dueDateNDisp);
				responseData.setDueDate(dueDateNDisp);
				String invoiceDate = invInfo.getInvoiceDate();
				date = (Date) formatter.parse(invoiceDate);
				String invoiceDateNDisp = newFormat.format(date);
				NumberFormat df = new DecimalFormat("#0.00");
				df.setRoundingMode(RoundingMode.FLOOR);
				Helper.logInfoMessageV2(queryInvoiceRequest.getmsisdn() + "- Invoice amount without formating "
						+ invInfo.getInvoiceAmount());
				
				
				Long invAmount = new Long(invInfo.getInvoiceAmount());
				responseData.setInvoiceAmount(df.format((invAmount.doubleValue() / Constants.MONEY_DIVIDEND)));

				responseData.setInvoiceDate(invoiceDateNDisp);
				responseData.setTotalAmountToBePaidLabel(ConfigurationManager.getConfigurationFromCache(
						"home.page.totalAmountToBePaid.label." + queryInvoiceRequest.getLang()));
				responseData.setInvoiceLabel(ConfigurationManager
						.getConfigurationFromCache("home.page.invoice.label." + queryInvoiceRequest.getLang()));

				// Total No of Days
				logger.info("Invoice Date Due Date:" + dueDateNDisp);
				logger.info("Invoice Date Invoice Date:" + invoiceDateNDisp);
				Date d2 = (Date) newFormat.parse(dueDateNDisp);
				Date d1 = (Date) newFormat.parse(invoiceDateNDisp);
				long diff = d2.getTime() - d1.getTime();
				long totalDays = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
				logger.info("totalDays:" + totalDays);
				responseData.setTotalDays(totalDays + "");

				// No.of days and Invoice Status
				if (invInfo.getStatus().equals("C")) {
					Helper.logInfoMessageV2(
							queryInvoiceRequest.getmsisdn() + " - Invoice Status: " + invInfo.getStatus());
					date = (Date) formatter.parse(invInfo.getSettleDate());
					responseData.setSettleDate(newFormat.format(date));
					newFormat = new SimpleDateFormat("dd/MM/yyyy");
					responseData.setSettleDateDisp(ConfigurationManager
							.getConfigurationFromCache("home.page.invoice.settle.label." + queryInvoiceRequest.getLang())+": "+newFormat.format(date));
					
					
					
					responseData.setDaysDisp(ConfigurationManager
							.getConfigurationFromCache("home.page.invoice.days." + queryInvoiceRequest.getLang()));
					responseData.setDueDate(newFormat.format(date));
					responseData.setRemainingDays(null);
				} else {
					invInfo.setSettleDate(null);
					Date due = new SimpleDateFormat("yyyy-MM-dd").parse(dueDateNDisp);
					long days = getDifferenceDays(due);
					
					if(days<0)
					{
						responseData.setDaysDisp(ConfigurationManager
								.getConfigurationFromCache("home.page.days.expired.label." + queryInvoiceRequest.getLang()));
					}
					else
					{
						responseData.setDaysDisp(days + " " + ConfigurationManager
								.getConfigurationFromCache("home.page.days.label." + queryInvoiceRequest.getLang()));
						
					}
					responseData.setRemainingDays(days + "");
					

				}
				responseData.setStatus(invInfo.getStatus());
				newFormat = new SimpleDateFormat("yyyy-MM-dd");
				date = (Date) formatter.parse(dueDate);
				responseData.setDueDateDisp(newFormat.format(date));

				date = (Date) formatter.parse(invoiceDate);
				responseData.setInvoiceDateDisp(newFormat.format(date));

				// newFormat = new SimpleDateFormat("yyyy-MM-dd");
				// date = (Date) formatter.parse(dueDate);
				// responseData.setDueDateDisp(ConfigurationManager.getConfigurationFromCache(
				// new
				// StringBuilder("labels.duedate.").append(queryInvoiceRequest.getLang()).toString())
				// + ": "
				// + newFormat.format(date));
				//
				// date = (Date) formatter.parse(invoiceDate);
				// responseData.setInvoiceDateDisp(ConfigurationManager.getConfigurationFromCache(
				// new
				// StringBuilder("labels.issuedate.").append(queryInvoiceRequest.getLang()).toString())
				// + ": "
				// + newFormat.format(date));

				responseDataList.add(responseData);
			
			queryInvoiceResponse.setQueryInvoiceResponseData(responseDataList);
			queryInvoiceResponse.setReturnCode("200");
			queryInvoiceResponse.setReturnMsg("Sucessfull");
			if (logs != null) {
				logs.setResponseCode(queryInvoiceResponse.getReturnCode());
				logs.setResponseDescription(queryInvoiceResponse.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
			}
		} else {
			prepareErrorResponse(queryInvoiceResponse, logs, ResponseCodes.ERROR_401_CODE, ResponseCodes.ERROR_401);
		}
		return queryInvoiceResponse;
	}

	/**
	 * 
	 * @param queryInvoiceResponse
	 * @param logs
	 * @param returnCode
	 * @param returnMessage
	 */
	public void prepareErrorResponse(QueryInvoiceResponse queryInvoiceResponse, Logs logs, String returnCode,
			String returnMessage) {
		queryInvoiceResponse.setReturnCode(returnCode);
		queryInvoiceResponse.setReturnMsg(returnMessage);
		logs.setResponseCode(queryInvoiceResponse.getReturnCode());
		logs.setResponseDescription(queryInvoiceResponse.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);

	}
	
	public InvoiceInfo checkIfInvoicesPaid(QueryInvoiceResultMsg resultmsg )
	{
		
		logger.info("Landed in checkIfInvoicesPaid Method");
		
		InvoiceInfo invoiceInfo = new InvoiceInfo();
		
		int index=-1;
		long invoiceAmount = 0;
		int billInvoiceIndex=-1;
		for(int i =0;i<resultmsg.getQueryInvoiceResult().getInvoiceInfo().size();i++)
		{
			if(resultmsg.getQueryInvoiceResult().getInvoiceInfo().get(i).getStatus().equalsIgnoreCase("O"))
			{
				index = i;
				
				break;
			}
		}
		
		if(index >-1)
		{
			logger.info("Loop End Open Invoice at index ::"+index);
			for(int i =0;i<resultmsg.getQueryInvoiceResult().getInvoiceInfo().size();i++) {
				
				invoiceAmount =invoiceAmount + resultmsg.getQueryInvoiceResult().getInvoiceInfo().get(i).getOpenAmount();
			}
			
			if(invoiceAmount> 0)
			{
				
				
				for(int i =0;i<resultmsg.getQueryInvoiceResult().getInvoiceInfo().size();i++) {
					
					invoiceAmount =invoiceAmount + resultmsg.getQueryInvoiceResult().getInvoiceInfo().get(i).getInvoiceAmount();
					if(resultmsg.getQueryInvoiceResult().getInvoiceInfo().get(i).getTransType().equalsIgnoreCase("BLL"))
					{
						billInvoiceIndex=i;
					}
				}
				logger.info("Open invoices found bill invoice index is ::"+billInvoiceIndex);
				invoiceInfo = resultmsg.getQueryInvoiceResult().getInvoiceInfo().get(billInvoiceIndex);
				invoiceInfo.setInvoiceAmount(invoiceAmount);
				logger.info("All open amounts sum is ::"+invoiceAmount);
				return invoiceInfo;	
			}
		}
		
		if(index==-1)
		{
			for(int i =0;i<resultmsg.getQueryInvoiceResult().getInvoiceInfo().size();i++) {
				
				invoiceAmount =invoiceAmount + resultmsg.getQueryInvoiceResult().getInvoiceInfo().get(i).getInvoiceAmount();
				if(resultmsg.getQueryInvoiceResult().getInvoiceInfo().get(i).getTransType().equalsIgnoreCase("BLL"))
				{
					billInvoiceIndex=i;
				}
			}
		}
		
			invoiceInfo = resultmsg.getQueryInvoiceResult().getInvoiceInfo().get(billInvoiceIndex);
			invoiceInfo.setInvoiceAmount(invoiceAmount);
			logger.info("Returning BLL object because all invoices are paid ::");
			return invoiceInfo;	
		

		
	}
	
	
	

	public static long getDifferenceDays(Date d2) throws ParseException {

		Date current = java.util.Calendar.getInstance().getTime();
		SimpleDateFormat newFormat = new SimpleDateFormat("dd/MM/yyyy");
		String currentDate = newFormat.format(current);
		Date d1 = new SimpleDateFormat("dd/MM/yyyy").parse(currentDate);

		long diff = d2.getTime() - d1.getTime();
		return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
	}

}
