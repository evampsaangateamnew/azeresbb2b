package com.evampsaanga.b2b.azerfon.querybalance;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.amqimplementationsesb.LogsType;
import com.evampsaanga.b2b.azerfon.hlrweb.getquerybalanceRequest.GetQueryBalanceResponseClient;
import com.evampsaanga.b2b.azerfon.hlrweb.getquerybalanceRequest.HLRBalanceServices;
import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.ThirdPartyNames;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Decrypter;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.evampsaanga.b2b.developer.utils.SOAPLoggingHandler;
import com.evampsaanga.b2b.gethomepage.Balance;
import com.huawei.bss.soaif._interface.common.ReqHeader;

/**
 * A bean which we use in the route
 */
@Path("/bakcell")
public class GetQueryBalanceLand {
	private static final Logger logger = Logger.getLogger("azerfon-esb");

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetQueryBalanceResponseClient Get(@Header("credentials") String credential,
			@Header("Content-Type") String contentType, @Body() String requestBody) {
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.HLR_QUERY_BALANCE_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.HLR_QUERY_BALANCE);
		logs.setTableType(LogsType.HlrQueryBalance);
		try {
			
			String trnsactionName = Transactions.HLR_QUERY_BALANCE_TRANSACTION_NAME + " " + Transactions.HLR_QUERY_BALANCE_TRANSACTION_NAME;
			String token = Helper.retrieveToken(trnsactionName, Helper.getValueFromJSON(requestBody, "msisdn"));
			logger.info("Request Landed on GetQueryBalanceLand:" + requestBody);
			GetQueryBalanceRequestClient cclient = null;
			try {
				cclient = Helper.JsonToObject(requestBody, GetQueryBalanceRequestClient.class);
				logs.setUserType(cclient.getCustomerType());	} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
				GetQueryBalanceResponseClient resp = new GetQueryBalanceResponseClient();
				resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_400);
				logs.setResponseCode(ResponseCodes.ERROR_400_CODE);
				logs.setResponseDescription(ResponseCodes.ERROR_400);
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);	return resp;
			}
			if (cclient != null) {
				String credentials = null;
				try {
					credentials = Decrypter.getInstance().decrypt(credential);
				} catch (Exception ex) {
					SOAPLoggingHandler.logger.error(Helper.GetException(ex));
					GetQueryBalanceResponseClient resp = new GetQueryBalanceResponseClient();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
				}
				if (credentials == null) {
					GetQueryBalanceResponseClient resp = new GetQueryBalanceResponseClient();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					logs.setResponseCode(ResponseCodes.ERROR_401_CODE);
					logs.setResponseDescription(ResponseCodes.ERROR_401);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
				if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
					String verification = Helper.validateRequest(cclient);
					if (!verification.equals("")) {
						GetQueryBalanceResponseClient res = new GetQueryBalanceResponseClient();
						res.setReturnCode(ResponseCodes.ERROR_400);
						res.setReturnMsg(verification);
						logs.setResponseCode(ResponseCodes.ERROR_400);
						logs.setResponseDescription(verification);
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);	return res;
					}
				} else {
					GetQueryBalanceResponseClient resp = new GetQueryBalanceResponseClient();
					resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
					logs.setResponseCode((ResponseCodes.ERROR_MSISDN_CODE));
					logs.setResponseDescription(ResponseCodes.ERROR_MSISDN);
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);return resp;
				}
				if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
					GetQueryBalanceResponseClient balanceResponse = new GetQueryBalanceResponseClient();
					try {
						HLRBalanceServices service = new HLRBalanceServices();
						Balance responseFromBakcell = service.getBalance(cclient.getmsisdn(),
								cclient.getCustomerType(),null,token);
						if (responseFromBakcell.getReturnCode().equals("0")) {
							balanceResponse.setBalance(responseFromBakcell);
							balanceResponse.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							balanceResponse.setReturnMsg(ResponseCodes.SUCESS_DES_200);
							logs.setResponseDescription(ResponseCodes.SUCESS_DES_200);
							logs.setResponseCode(ResponseCodes.SUCESS_CODE_200);
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						
							
							logs.updateLog(logs);					
							
							return balanceResponse;
						} else {
							balanceResponse.setReturnCode(responseFromBakcell.getReturnCode());
							balanceResponse.setReturnMsg(responseFromBakcell.getReturnMsg());
							logs.setResponseDescription(responseFromBakcell.getReturnMsg());
							logs.setResponseCode(responseFromBakcell.getReturnCode());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						
							
							logs.updateLog(logs);					
												return balanceResponse;
						}
					} catch (Exception ex) {
						GetQueryBalanceResponseClient resp = new GetQueryBalanceResponseClient();
						logger.error(Helper.GetException(ex));
						resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
						resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
						return resp;
					}
				} else {
					GetQueryBalanceResponseClient resp = new GetQueryBalanceResponseClient();
					resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
					resp.setReturnMsg(ResponseCodes.ERROR_401);
					return resp;
				}
			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		GetQueryBalanceResponseClient resp = new GetQueryBalanceResponseClient();
		resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
		resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
		logs.updateLog(logs);			
		return resp;
	}

	public ReqHeader getReqHeaderForQueryBalanceHLR() {
		ReqHeader reqHForQBalance = new ReqHeader();
		reqHForQBalance.setChannelId(ConfigurationManager.getConfigurationFromCache("hlrweb.reqh.ChannelId").trim());
		reqHForQBalance.setAccessUser(ConfigurationManager.getConfigurationFromCache("hlrweb.reqh.AccessUser").trim());
		reqHForQBalance.setAccessPwd(ConfigurationManager.getConfigurationFromCache("hlrweb.reqh.AccessPwd").trim());
		reqHForQBalance
				.setMIDWAREChannelID(ConfigurationManager.getConfigurationFromCache("hlrweb.reqh.MIDWAREChannelID").trim());
		reqHForQBalance
				.setMIDWAREAccessPwd(ConfigurationManager.getConfigurationFromCache("hlrweb.reqh.MIDWAREAccessPwd").trim());
		reqHForQBalance
				.setMIDWAREAccessUser(ConfigurationManager.getConfigurationFromCache("hlrweb.reqh.MIDWAREAccessUser").trim());
		reqHForQBalance.setTransactionId(Helper.generateTransactionID());
		return reqHForQBalance;
	}
}
