package com.evampsaanga.b2b.azerfon.querybalance;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetQueryBalanceRequestClient", propOrder = { "requestBody" })
@XmlRootElement(name = "GetQueryBalanceRequestClient")
public class GetQueryBalanceRequestClient extends com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest {
	private String customerType = "";
	private String brandId = "";

	/**
	 * @return the customerType
	 */
	public String getCustomerType() {
		return customerType;
	}

	/**
	 * @param customerType
	 *            the customerType to set
	 */
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	/**
	 * @return the brandId
	 */
	public String getBrandId() {
		return brandId;
	}

	/**
	 * @param brandId
	 *            the brandId to set
	 */
	public void setBrandId(String brandId) {
		this.brandId = brandId;
	}
}
