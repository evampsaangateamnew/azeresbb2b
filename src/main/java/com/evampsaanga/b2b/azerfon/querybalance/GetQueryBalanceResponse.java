package com.evampsaanga.b2b.azerfon.querybalance;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;
import com.huawei.bme.cbsinterface.arservices.QueryLoanLogResultMsg;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetQueryBalanceResponseClient", propOrder = { "responseBody" })
@XmlRootElement(name = "GetQueryBalanceResponseClient")
public class GetQueryBalanceResponse extends BaseResponse {
	@XmlElement(name = "responseBody", required = true)
	private QueryLoanLogResultMsg responseBody = null;

	public GetQueryBalanceResponse() {
		super();
	}

	public GetQueryBalanceResponse(QueryLoanLogResultMsg responseBody) {
		super();
		this.responseBody = responseBody;
	}

	public QueryLoanLogResultMsg getResponseBody() {
		return responseBody;
	}

	public void setResponseBody(QueryLoanLogResultMsg responseBody) {
		this.responseBody = responseBody;
	}
}
