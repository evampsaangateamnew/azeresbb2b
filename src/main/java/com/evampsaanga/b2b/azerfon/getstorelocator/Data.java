package com.evampsaanga.b2b.azerfon.getstorelocator;

public class Data {
	private Stores[] stores;
	private String[] type;
	private String[] city;

	public Stores[] getStores() {
		return stores;
	}

	public void setStores(Stores[] stores) {
		this.stores = stores;
	}

	public String[] getType() {
		return type;
	}

	public void setType(String[] type) {
		this.type = type;
	}

	public String[] getCity() {
		return city;
	}

	public void setCity(String[] city) {
		this.city = city;
	}
}
