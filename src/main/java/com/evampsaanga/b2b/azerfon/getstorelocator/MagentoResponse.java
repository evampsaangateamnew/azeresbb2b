package com.evampsaanga.b2b.azerfon.getstorelocator;

public class MagentoResponse {
	private Data data;
	private String resultCode;
	private String msg;
	private String execTime;

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getExecTime() {
		return execTime;
	}

	public void setExecTime(String execTime) {
		this.execTime = execTime;
	}

	@Override
	public String toString() {
		return "ClassPojo [data = " + data + ", resultCode = " + resultCode + ", msg = " + msg + ", execTime = "
				+ execTime + "]";
	}
}