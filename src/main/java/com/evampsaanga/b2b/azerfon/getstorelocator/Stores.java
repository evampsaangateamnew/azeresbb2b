package com.evampsaanga.b2b.azerfon.getstorelocator;

public class Stores {
	private String id;
	private String address;
	private String[] contactNumbers;
	private String longitude;
	private String latitude;
	private Timing[] timing;
	private String type;
	private String store_name;
	private String city;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String[] getContactNumbers() {
		return contactNumbers;
	}

	public void setContactNumbers(String[] contactNumbers) {
		this.contactNumbers = contactNumbers;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public Timing[] getTiming() {
		return timing;
	}

	public void setTiming(Timing[] timing) {
		this.timing = timing;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getStore_name() {
		return store_name;
	}

	public void setStore_name(String store_name) {
		this.store_name = store_name;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
}