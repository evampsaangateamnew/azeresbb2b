package com.evampsaanga.b2b.azerfon.getstorelocator;

import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;

public class GetStoreLocatorResponseClient extends BaseResponse {
	Data data = new Data();

	/**
	 * @return the data
	 */
	public Data getData() {
		return data;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	public void setData(Data data) {
		this.data = data;
	}
}
