package com.evampsaanga.b2b.azerfon.getUsersV2;

import java.util.ArrayList;

/**
 * Response Data Container For User Group Information Phase 2
 * @author Aqeel Abbas
 *
 */
public class UsersGroupResponseData {

	private String groupName;
	private int userCount;
	private ArrayList<UsersGroupData> usersGroupData;
	

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public int getUserCount() {
		return userCount;
	}

	public void setUserCount(int userCount) {
		this.userCount = userCount;
	}

	public ArrayList< UsersGroupData> getUsersGroupData() {
		return usersGroupData;
	}

	public void setUsersGroupData(ArrayList<UsersGroupData> usersGroupData) {
		this.usersGroupData = usersGroupData;
	}

}
