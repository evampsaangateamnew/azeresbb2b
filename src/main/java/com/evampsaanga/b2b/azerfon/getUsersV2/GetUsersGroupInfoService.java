package com.evampsaanga.b2b.azerfon.getUsersV2;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.amqimplementationsesb.LogsType;
import com.evampsaanga.b2b.authorization.AuthorizationAndAuthentication;
import com.evampsaanga.b2b.azerfon.db.DBFactory;
import com.evampsaanga.b2b.cache.UserGroupDataCache;
import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.ThirdPartyNames;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.evampsaanga.b2b.validator.rules.ChannelNotEmpty;
import com.evampsaanga.b2b.validator.rules.IPNotEmpty;
import com.evampsaanga.b2b.validator.rules.LangNotEmpty;
import com.evampsaanga.b2b.validator.rules.MSISDNNotEmpty;
import com.evampsaanga.b2b.validator.rules.ValidationResult;

/**
 * Generates user group data and user data
 * 
 * @author Aqeel Abbas
 *
 */
@Path("/azerfon")
public class GetUsersGroupInfoService {
	public static final Logger logger = Logger.getLogger("azerfon-esb");

	/**
	 * 
	 * @param requestBody
	 * @param credential
	 * @return user groups
	 * @throws SQLException
	 * @throws InterruptedException
	 */
	@POST
	@Path("/getusers")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UsersGroupResponse getUsersGroupData(@Body String requestBody, @Header("credentials") String credential)
			throws SQLException, InterruptedException {

		// Logging incoming request to log file

		Helper.logInfoMessageV2("--------------Test data as--------------");

		// Below is declaration block for variables to be used
		// Logs object to store values which are to be inserted in database for
		// reporting
		Logs logs = new Logs();

		// Generic information about logs is being set to logs object
		logs.setRequestDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.setTransactionName(Transactions.USERS_GROUP_DATA);

		// SEND_FREE_SMS = "Database Internal";
		logs.setThirdPartyName(ThirdPartyNames.SEND_FREE_SMS);
		logs.setTableType(LogsType.CustomerData);

		// Request object to store parsed data from requested string
		UsersGroupRequest usersGroupRequest = null;
		// response object which is to be returned to user
		ArrayList<UsersGroupData> usersGroupData = new ArrayList<UsersGroupData>();
		UsersGroupResponse usersgroupResponse = new UsersGroupResponse();
		Map<String, UsersGroupData> usersMap = new HashMap<>();
		// Authenticating the request using credentials string received in
		// header

		String token = "";
		String TrnsactionName = Transactions.USERS_GROUP_DATA;

		try {

			token = Helper.retrieveToken(TrnsactionName, Helper.getValueFromJSON(requestBody, "msisdn"));

			Helper.logInfoMessageV2(token + "-Request Data-" + requestBody);
			usersGroupRequest = Helper.JsonToObject(requestBody, UsersGroupRequest.class);
		} catch (Exception ex) {
			// block catches the exception from mapper and sets the 400 bad
			// request response code
			Helper.logInfoMessageV2(token + Helper.GetException(ex));
			prepareErrorResponse(usersgroupResponse, logs, ResponseCodes.ERROR_400_CODE, ResponseCodes.ERROR_400);
		}

		boolean authenticationresult = AuthorizationAndAuthentication.authenticateAndAuthorizeCredentials(credential);

		// if request is not authenticated adding unauthorized response
		// codes to response and logs object
		if (!authenticationresult)
			prepareErrorResponse(usersgroupResponse, logs, ResponseCodes.ERROR_401_CODE, ResponseCodes.ERROR_401);

		// if requested string is converted to Java class then processing of
		// request goes below
		if (usersGroupRequest != null) {
			// block sets the mandatory parameters to logs object which are
			// taken from request body

			logs.setIp(usersGroupRequest.getiP());
			logs.setChannel(usersGroupRequest.getChannel());
			logs.setMsisdn(usersGroupRequest.getmsisdn());

			// if authentication is successful request forwarded for
			// validation of parameters
			if (authenticationresult) {
				ValidationResult validationResult = validateRequest(usersGroupRequest);

				if (validationResult.isValidationResult()) {
					logs.setResponseCode(usersgroupResponse.getReturnCode());
					logs.setResponseDescription(usersgroupResponse.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
				} else
					// if request parameters fails validation response is
					// prepared with validation error
					prepareErrorResponse(usersgroupResponse, logs, validationResult.getValidationCode(),
							validationResult.getValidationMessage());
			}
		}
		// getting the data from DB view
		UserGroupDataCache userCache = new UserGroupDataCache();

		try {
			if (usersGroupRequest.getSearchUsers() != null && !usersGroupRequest.getSearchUsers().isEmpty()) {
				Helper.logInfoMessageV2(token + " - Start Searching");
				usersGroupData = (ArrayList<UsersGroupData>) userCache.searchUsers(requestBody, token);
				if (usersGroupData == null || usersGroupData.isEmpty()) {
					Helper.logInfoMessageV2(token + " - No Users Found");
					usersgroupResponse.setReturnCode(ResponseCodes.SUCESS_CODE_200);
					usersgroupResponse.setReturnMsg("No Users Found");
					/*prepareErrorResponse(usersgroupResponse, logs, ResponseCodes.ERROR_401_CODE,
							ResponseCodes.ERROR_401);*/

				} else {
					HashMap<String, UsersGroupResponseData> map = new HashMap<String, UsersGroupResponseData>();
					ArrayList<UsersGroupData> usersData = new ArrayList<UsersGroupData>();

					for (int i = 0; i < usersGroupData.size(); i++) {
						// String groupID =
						// usersGroupData.get(i).getGroup_cust_name();
						String groupID = usersGroupData.get(i).getGroupName();
						String groupName = usersGroupData.get(i).getGroupNameDisplay();
						usersMap.put(usersGroupData.get(i).getMsisdn(), usersGroupData.get(i));
						if (map.containsKey(groupID)) {
							((UsersGroupResponseData) map.get(groupID)).getUsersGroupData()
									.add((UsersGroupData) usersGroupData.get(i));
							((UsersGroupResponseData) map.get(groupID)).setUserCount(
									((UsersGroupResponseData) map.get(groupID)).getUsersGroupData().size());
							// map.get(groupID).getUsersGroupData().add(usersGroupData.get(i));
						} else {
							UsersGroupResponseData data = new UsersGroupResponseData();
							usersData = new ArrayList<UsersGroupData>();
							usersData.add(usersGroupData.get(i));
							data.setUsersGroupData(usersData);
							data.setGroupName(groupName);
							data.setUserCount(data.getUsersGroupData().size());
							map.put(groupID, data);
						}
					}

					/*
					 * Iterator it = map.entrySet().iterator(); while
					 * (it.hasNext()) { Map.Entry pair = (Map.Entry) it.next();
					 * ((UsersGroupResponseData) pair.getValue())
					 * .setUserCount(((UsersGroupResponseData)
					 * pair.getValue()).getUsersGroupData().size()); }
					 */
					usersgroupResponse.setGroupData(map);
					usersgroupResponse.setUsers(usersMap);
					// usersgroupResponse.setUsers(usersGroupData);
					logger.info("sizeCount&GroupData :" + usersGroupData.size());
					Helper.logInfoMessageV2("sizeCount&GroupData :" + usersGroupData.size());
					usersgroupResponse.setUserCount(usersGroupData.size());
					Helper.logInfoMessageV2(usersGroupRequest.getmsisdn() + " - Current Page No is : "+usersGroupRequest.getPageNum());
					if (usersGroupRequest.getPageNum()
							.equalsIgnoreCase(totalPagesSearch(usersGroupRequest,usersGroupData))) {
						Helper.logInfoMessageV2(usersGroupRequest.getmsisdn() + " - Entering  in if to Set LAst Page True : "+usersGroupRequest.getPageNum());

						usersgroupResponse.setIsLastPage("true");
					} else
						usersgroupResponse.setIsLastPage("false");
					usersgroupResponse.setReturnCode(ResponseCodes.SUCESS_CODE_200);
					usersgroupResponse.setReturnMsg(ResponseCodes.SUCESS_DES_200);
				}
			} else {
				usersGroupData = (ArrayList<UsersGroupData>) userCache.checkUsersInCache(requestBody, token);
				if (usersGroupData == null || usersGroupData.isEmpty()) {
					Helper.logInfoMessageV2(token + " - No Users Found");
					usersgroupResponse.setReturnMsg("No Users Found");
					usersgroupResponse.setReturnCode(ResponseCodes.SUCESS_CODE_200);
					
//					prepareErrorResponse(usersgroupResponse, logs, ResponseCodes.ERROR_401_CODE,
//							ResponseCodes.ERROR_401);

				} else {
					HashMap<String, UsersGroupResponseData> map = new HashMap<String, UsersGroupResponseData>();
					ArrayList<UsersGroupData> usersData = new ArrayList<UsersGroupData>();

					for (int i = 0; i < usersGroupData.size(); i++) {
						// String groupID =
						// usersGroupData.get(i).getGroup_cust_name();
						String groupID = usersGroupData.get(i).getGroupName();
						String groupName = usersGroupData.get(i).getGroupNameDisplay();
						usersMap.put(usersGroupData.get(i).getMsisdn(), usersGroupData.get(i));
						if (map.containsKey(groupID)) {
							((UsersGroupResponseData) map.get(groupID)).getUsersGroupData()
									.add((UsersGroupData) usersGroupData.get(i));
							((UsersGroupResponseData) map.get(groupID)).setUserCount(
									((UsersGroupResponseData) map.get(groupID)).getUsersGroupData().size());
							// map.get(groupID).getUsersGroupData().add(usersGroupData.get(i));
						} else {
							UsersGroupResponseData data = new UsersGroupResponseData();
							usersData = new ArrayList<UsersGroupData>();
							usersData.add(usersGroupData.get(i));
							data.setUsersGroupData(usersData);
							data.setGroupName(groupName);
							data.setUserCount(data.getUsersGroupData().size());
							map.put(groupID, data);
						}
					}

					/*
					 * Iterator it = map.entrySet().iterator(); while
					 * (it.hasNext()) { Map.Entry pair = (Map.Entry) it.next();
					 * ((UsersGroupResponseData) pair.getValue())
					 * .setUserCount(((UsersGroupResponseData)
					 * pair.getValue()).getUsersGroupData().size()); }
					 */
					usersgroupResponse.setGroupData(map);
					usersgroupResponse.setUsers(usersMap);
					// usersgroupResponse.setUsers(usersGroupData);
					logger.info("sizeCount&GroupData :" + usersGroupData.size());
					Helper.logInfoMessageV2("sizeCount&GroupData :" + usersGroupData.size());
					usersgroupResponse.setUserCount(usersGroupData.size());
					Helper.logInfoMessageV2(usersGroupRequest.getmsisdn() + " - Current Page No is : "+usersGroupRequest.getPageNum());
					if (usersGroupRequest.getPageNum()
							.equalsIgnoreCase(totalPages(usersGroupRequest,usersGroupData))) {
						Helper.logInfoMessageV2(usersGroupRequest.getmsisdn() + " - Entering  in if to Set LAst Page True : "+usersGroupRequest.getPageNum());

						usersgroupResponse.setIsLastPage("true");
					} else
						usersgroupResponse.setIsLastPage("false");
					usersgroupResponse.setReturnCode(ResponseCodes.SUCESS_CODE_200);
					usersgroupResponse.setReturnMsg(ResponseCodes.SUCESS_DES_200);
				}
			}
		} catch (Exception e1) {
			logger.error(Helper.GetException(e1));
			prepareErrorResponse(usersgroupResponse, logs, ResponseCodes.ERROR_401_CODE, ResponseCodes.ERROR_401);

		}
		Helper.logInfoMessageV2(usersGroupRequest.getmsisdn() + " - getusers End point reached");
		return usersgroupResponse;

	}

	public String getUsersCount(String virtualCorpCode, String groupCode, String groupType) throws JSONException {
		logger.info("Group Code is: " + groupCode);
		UserGroupDataCache userCache = new UserGroupDataCache();
		String usersCount = userCache.countUsers(virtualCorpCode,groupCode,groupType);
		logger.info("UserCont-- " + usersCount);
		return usersCount;

	}
	
	
	public String getUsersCountSearch(UsersGroupRequest request) throws JSONException {
		UserGroupDataCache userCache = new UserGroupDataCache();
		String usersCount = userCache.getUsersCountSearch(request);
		logger.info("UserCont-- " + usersCount);
		return usersCount;

	}

	/**
	 * Validates the request
	 * 
	 * @param client
	 * @return validation response
	 */
	private ValidationResult validateRequest(UsersGroupRequest client) {
		ValidationResult validationResult = new ValidationResult();
		validationResult.setValidationResult(true);
		// Validating all the fields against non empty rule
		validationResult = new MSISDNNotEmpty().validateObject(client.getmsisdn());
		validationResult = new ChannelNotEmpty().validateObject(client.getChannel());
		validationResult = new IPNotEmpty().validateObject(client.getiP());
		validationResult = new LangNotEmpty().validateObject(client.getLang());
		return validationResult;
	}

	/**
	 * prepares error response and update logs in queue
	 * 
	 * @param usersGroupDataResponse
	 * @param logs
	 * @param returnCode
	 * @param returnMessage
	 */
	public void prepareErrorResponse(UsersGroupResponse usersGroupDataResponse, Logs logs, String returnCode,
			String returnMessage) {
		usersGroupDataResponse.setReturnCode(returnCode);
		usersGroupDataResponse.setReturnMsg(returnMessage);
		logs.setResponseCode(usersGroupDataResponse.getReturnCode());
		logs.setResponseDescription(usersGroupDataResponse.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
	}

	public String totalPages(UsersGroupRequest request,ArrayList<UsersGroupData> usersGroupDatas) throws JSONException {
		Helper.logInfoMessageV2(request.getmsisdn() + " - Landed in totalPages Method");
		double total = 0.0;
	
		String size="";
		size=getUsersCount(request.getVirtualCorpCode(),request.getGroupId(),request.getType());
//		size=usersGroupDatas.size()+"";
		
		String totalPages = "";
		double userLimit = Double.parseDouble(request.getLimitUsers());
		total = Double.parseDouble(size) / userLimit;
		Helper.logInfoMessageV2(request.getmsisdn() + " - Returning total pages without ceil :"+ total);
		total = Math.ceil(total);
		int value = (int) total;
		
		totalPages =value+"";
		Helper.logInfoMessageV2(request.getmsisdn() + " - Returning total pages :"+ totalPages);
		return totalPages;
	}
	
	public String totalPagesSearch(UsersGroupRequest request,ArrayList<UsersGroupData> usersGroupDatas) throws JSONException {
		Helper.logInfoMessageV2(request.getmsisdn() + " - Landed in totalPages Method");
		double total = 0.0;
	
		String size="";
		size=getUsersCountSearch(request);
		//size=usersGroupDatas.size()+"";
		
		String totalPages = "";
		double userLimit = Double.parseDouble(request.getLimitUsers());
		total = Double.parseDouble(size) / userLimit;
		Helper.logInfoMessageV2(request.getmsisdn() + " - Returning total pages without ceil :"+ total);
		total = Math.ceil(total);
		int value = (int) total;
		
		totalPages =value+"";
		Helper.logInfoMessageV2(request.getmsisdn() + " - Returning total pages :"+ totalPages);
		return totalPages;
	}
	
	
}
