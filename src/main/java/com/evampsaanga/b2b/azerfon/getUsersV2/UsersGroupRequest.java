package com.evampsaanga.b2b.azerfon.getUsersV2;

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Request Container For User and Group Data info for Phase 2
 * @author Aqeel Abbas
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UsersGroupRequest extends BaseRequest {
	private String virtualCorpCode;
	private String accountCode;
	private String type;
	private String pageNum;
	private String limitUsers;
	private String searchUsers;
	private String groupType;
	private String groupId;
	//private String customerID;

	public String getPageNum() {
		return pageNum;
	}
	public void setPageNum(String pageNum) {
		this.pageNum = pageNum;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public UsersGroupRequest(String channel, String lang, String msisdn, String iP) {
		super(channel, lang, msisdn, iP);
		// TODO Auto-generated constructor stub
	}
	public UsersGroupRequest(){}
	
	public String getVirtualCorpCode() {
		return virtualCorpCode;
	}
	public void setVirtualCorpCode(String virtualCorpCode) {
		this.virtualCorpCode = virtualCorpCode;
	}
	public String getAccountCode() {
		return accountCode;
	}
	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}
	public String getLimitUsers() {
		return limitUsers;
	}
	public void setLimitUsers(String limitUsers) {
		this.limitUsers = limitUsers;
	}
	public String getSearchUsers() {
		return searchUsers;
	}
	public void setSearchUsers(String searchUsers) {
		this.searchUsers = searchUsers;
	}
	public String getGroupType() {
		return groupType;
	}
	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}
	public String getGroupId() {
		return groupId;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}


	

/*	public String getCustomerID() {
		return customerID;
	}
	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}*/

}
