package com.evampsaanga.b2b.azerfon.getUsersV2;

import java.io.Serializable;

/**
 * User Group Data Container For Phase 2
 * @author Aqeel Abbas
 *
 */
public class UsersGroupData implements Serializable {
	
	private String msisdn;
	private String tariffPlan;
	private String crmSubId;
	private String crmCustId;
	private String crmCustCode;
	private String virtualCorpCode;
	private String crmCorpCustId;
	private String cugGroupCode;
	private String groupCode;
	private String crmAccountId;
	private String accCode;
	private String crmAccIdPaid;
	private String groupName;
	private String groupNameDisplay;
	private String tariffNameDisplay;

	public String getGroupNameDisplay() 
	{
		return groupNameDisplay;
	}
	public void setGroupNameDisplay(String groupNameDisplay) {
		this.groupNameDisplay = groupNameDisplay;
	}
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	public String getTariffPlan() {
		return tariffPlan;
	}
	public void setTariffPlan(String tariffPlan) {
		this.tariffPlan = tariffPlan;
	}
	public String getCrmSubId() {
		return crmSubId;
	}
	public void setCrmSubId(String crmSubId) {
		this.crmSubId = crmSubId;
	}
	public String getCrmCustId() {
		return crmCustId;
	}
	public void setCrmCustId(String crmCustId) {
		this.crmCustId = crmCustId;
	}
	public String getCrmCustCode() {
		return crmCustCode;
	}
	public void setCrmCustCode(String crmCustCode) {
		this.crmCustCode = crmCustCode;
	}
	public String getVirtualCorpCode() {
		return virtualCorpCode;
	}
	public void setVirtualCorpCode(String virtualCorpCode) {
		this.virtualCorpCode = virtualCorpCode;
	}
	public String getCrmCorpCustId() {
		return crmCorpCustId;
	}
	public void setCrmCorpCustId(String crmCorpCustId) {
		this.crmCorpCustId = crmCorpCustId;
	}
	public String getCugGroupCode() {
		return cugGroupCode;
	}
	public void setCugGroupCode(String cugGroupCode) {
		this.cugGroupCode = cugGroupCode;
	}
	public String getGroupCode() {
		return groupCode;
	}
	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}
	public String getCrmAccountId() {
		return crmAccountId;
	}
	public void setCrmAccountId(String crmAccountId) {
		this.crmAccountId = crmAccountId;
	}
	public String getAccCode() {
		return accCode;
	}
	public void setAccCode(String accCode) {
		this.accCode = accCode;
	}

	public String getCrmAccIdPaid() {
		return crmAccIdPaid;
	}
	public void setCrmAccIdPaid(String crmAccIdPaid) {
		this.crmAccIdPaid = crmAccIdPaid;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getTariffNameDisplay() {
		return tariffNameDisplay;
	}
	public void setTariffNameDisplay(String tariffNameDisplay) {
		this.tariffNameDisplay = tariffNameDisplay;
	}

	

		/*private String msisdn;
	private String tarif_id;
	private String brand_id;
	private String brand_name;
	private String corp_id;
	private String group_code;
	private String group_name;
	private String group_cust_name;
	private String group_id;
	private String corp_crm_cust_name;
	private String pic_name;
	private String corp_acct_code;
	private String corp_crm_acct_id;
	private String cust_fst_name;
	private String cust_last_name;
	private String group_name_disp;
	
	public String getBrand_name() {
		return brand_name;
	}

	public void setBrand_name(String brand_name) {
		this.brand_name = brand_name;
	}

	public String getGroup_name() {
		return group_name;
	}

	public void setGroup_name(String group_name) {
		this.group_name = group_name;
	}

	public String getCorp_acct_code() {
		return corp_acct_code;
	}

	public void setCorp_acct_code(String corp_acct_code) {
		this.corp_acct_code = corp_acct_code;
	}

	public String getCorp_crm_acct_id() {
		return corp_crm_acct_id;
	}

	public void setCorp_crm_acct_id(String corp_crm_acct_id) {
		this.corp_crm_acct_id = corp_crm_acct_id;
	}

	public String getCust_fst_name() {
		return cust_fst_name;
	}

	public void setCust_fst_name(String cust_fst_name) {
		this.cust_fst_name = cust_fst_name;
	}

	public String getCust_last_name() {
		return cust_last_name;
	}

	public void setCust_last_name(String cust_last_name) {
		this.cust_last_name = cust_last_name;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getTarif_id() {
		return tarif_id;
	}

	public void setTarif_id(String tarif_id) {
		this.tarif_id = tarif_id;
	}

	public String getBrand_id() {
		return brand_id;
	}

	public void setBrand_id(String brand_id) {
		this.brand_id = brand_id;
	}

	public String getCorp_id() {
		return corp_id;
	}

	public void setCorp_id(String corp_id) {
		this.corp_id = corp_id;
	}

	public String getGroup_code() {
		return group_code;
	}

	public void setGroup_code(String group_code) {
		this.group_code = group_code;
	}

	public String getGroup_cust_name() {
		return group_cust_name;
	}

	public void setGroup_cust_name(String group_cust_name) {
		this.group_cust_name = group_cust_name;
	}

	public String getGroup_id() {
		return group_id;
	}

	public void setGroup_id(String group_id) {
		this.group_id = group_id;
	}

	public String getCorp_crm_cust_name() {
		return corp_crm_cust_name;
	}

	public void setCorp_crm_cust_name(String corp_crm_cust_name) {
		this.corp_crm_cust_name = corp_crm_cust_name;
	}

	public String getPic_name() {
		return pic_name;
	}

	public void setPic_name(String pic_name) {
		this.pic_name = pic_name;
	}

	public String getGroup_name_disp() {
		return group_name_disp;
	}

	public void setGroup_name_disp(String group_name_disp) {
		this.group_name_disp = group_name_disp;
	}*/
	

}
