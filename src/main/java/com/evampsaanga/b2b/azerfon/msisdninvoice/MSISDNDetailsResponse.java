
package com.evampsaanga.b2b.azerfon.msisdninvoice;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.ArrayList;

import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "returnCode",
    "returnMsg",
    "data"
    
})
public class MSISDNDetailsResponse {

    @JsonProperty("returnCode")
    private String returnCode;
    @JsonProperty("returnMsg")
    private String returnMsg;
    @JsonProperty("data")
    private MSISDNDetail data;

    @JsonProperty("returnCode")
    public String getReturnCode() {
        return returnCode;
    }

    @JsonProperty("returnCode")
    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    @JsonProperty("returnMsg")
    public String getReturnMsg() {
        return returnMsg;
    }

    @JsonProperty("returnMsg")
    public void setReturnMsg(String returnMsg) {
        this.returnMsg = returnMsg;
    }
    
    @JsonProperty("data")
    public MSISDNDetail getData() {
		return data;
	}
    
    @JsonProperty("data")
	public void setData(MSISDNDetail data) {
		this.data = data;
	}

	@Override
    public String toString() {
        return new ToStringBuilder(this).append("returnCode", returnCode).append("returnMsg", returnMsg).append("data", data).toString();
    }

}
