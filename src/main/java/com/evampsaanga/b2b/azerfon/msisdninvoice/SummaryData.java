/**
 * 
 */
package com.evampsaanga.b2b.azerfon.msisdninvoice;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author HamzaFarooque
 *
 */
public class SummaryData {
	
	@JsonProperty("exportLink")
	private String exportLink;
	@JsonProperty("summaryData")
    private List<MSISDNSummary> summaryData = null;
	
	public String getExportLink() {
		return exportLink;
	}
	public void setExportLink(String exportLink) {
		this.exportLink = exportLink;
	}
	public List<MSISDNSummary> getSummaryData() {
		return summaryData;
	}
	public void setSummaryData(List<MSISDNSummary> summaryData) {
		this.summaryData = summaryData;
	}
	
}
