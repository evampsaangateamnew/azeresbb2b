
package com.evampsaanga.b2b.azerfon.msisdninvoice;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MSISDNDetail {
	
	DetailsData msisdn;
	DetailsData tariff;
	List<DetailsData> results = null;
	DetailsData total;
	String exportLink;
	
	public DetailsData getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(DetailsData msisdn) {
		this.msisdn = msisdn;
	}
	public DetailsData getTariff() {
		return tariff;
	}
	public void setTariff(DetailsData tariff) {
		this.tariff = tariff;
	}
	public List<DetailsData> getResults() {
		return results;
	}
	public void setResults(List<DetailsData> results) {
		this.results = results;
	}
	public DetailsData getTotal() {
		return total;
	}
	public void setTotal(DetailsData total) {
		this.total = total;
	}
	public String getExportLink() {
		return exportLink;
	}
	public void setExportLink(String exportLink) {
		this.exportLink = exportLink;
	}
	
}
