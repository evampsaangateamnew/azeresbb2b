
package com.evampsaanga.b2b.azerfon.msisdninvoice;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "msisdn",
    "tariff",
    "debt"
})
public class MSISDNSummary {

    @JsonProperty("msisdn")
    private String msisdn;
    @JsonProperty("tariff")
    private String tariff;
    @JsonProperty("debt")
    private String debt;
   
    
	@JsonProperty("msisdn")
	public String getMsisdn() {
		return msisdn;
	}
    @JsonProperty("msisdn")
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
    @JsonProperty("tariff")
	public String getTariff() {
		return tariff;
	}
    @JsonProperty("tariff")
	public void setTariff(String tariff) {
		this.tariff = tariff;
	}
    @JsonProperty("debt")
	public String getDebt() {
		return debt;
	}
    @JsonProperty("debt")
	public void setDebt(String debt) {
		this.debt = debt;
	}
    
}
