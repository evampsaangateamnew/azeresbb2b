/**
 * 
 */
package com.evampsaanga.b2b.azerfon.msisdninvoice;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * @author HamzaFarooque
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "returnCode", "returnMsg", "data" })
public class MSISDNSummaryResponse {

	@JsonProperty("returnCode")
	private String returnCode;
	@JsonProperty("returnMsg")
	private String returnMsg;
	@JsonProperty("data")
	private SummaryData data = null;

	@JsonProperty("returnCode")
	public String getReturnCode() {
		return returnCode;
	}

	@JsonProperty("returnCode")
	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}

	@JsonProperty("returnMsg")
	public String getReturnMsg() {
		return returnMsg;
	}

	@JsonProperty("returnMsg")
	public void setReturnMsg(String returnMsg) {
		this.returnMsg = returnMsg;
	}

	public SummaryData getData() {
		return data;
	}

	public void setData(SummaryData data) {
		this.data = data;
	}

}
