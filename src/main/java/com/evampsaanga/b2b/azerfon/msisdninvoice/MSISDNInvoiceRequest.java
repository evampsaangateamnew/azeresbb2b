package com.evampsaanga.b2b.azerfon.msisdninvoice;

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "acctCode", "billMonth", "selectedMsisdn", "customerId" })
public class MSISDNInvoiceRequest extends BaseRequest {

	@JsonProperty("acctCode")
	private String acctCode;
	@JsonProperty("billMonth")
	private String billMonth;
	@JsonProperty("selectedMsisdn")
	private String selectedMsisdn;
	@JsonProperty("customerId")
	private String customerId;
	@JsonProperty("isSendMail")
	private String isSendMail;
	@JsonProperty("mailTo")
	private String mailTo;

	@JsonProperty("customerId")
	public String getCustomerId() {
		return customerId;
	}

	@JsonProperty("customerId")
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	@JsonProperty("selectedMsisdn")
	public String getSelectedMsisdn() {
		return selectedMsisdn;
	}

	@JsonProperty("selectedMsisdn")
	public void setSelectedMsisdn(String selectedMsisdn) {
		this.selectedMsisdn = selectedMsisdn;
	}

	@JsonProperty("acctCode")
	public String getAcctCode() {
		return acctCode;
	}

	@JsonProperty("acctCode")
	public void setAcctCode(String acctCode) {
		this.acctCode = acctCode;
	}

	@JsonProperty("billMonth")
	public String getBillMonth() {
		return billMonth;
	}

	@JsonProperty("billMonth")
	public void setBillMonth(String billMonth) {
		this.billMonth = billMonth;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("acctCode", acctCode).append("billMonth", billMonth)
				.append("isSendMail", isSendMail).append("mailTo", mailTo).toString();
	}

	@JsonProperty("isSendMail")
	public String getIsSendMail() {
		return isSendMail;
	}

	@JsonProperty("isSendMail")
	public void setIsSendMail(String isSendMail) {
		this.isSendMail = isSendMail;
	}

	@JsonProperty("mailTo")
	public String getMailTo() {
		return mailTo;
	}

	@JsonProperty("mailTo")
	public void setMailTo(String mailTo) {
		this.mailTo = mailTo;
	}

}