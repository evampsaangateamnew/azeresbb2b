package com.evampsaanga.b2b.azerfon.msisdninvoice;

import java.io.File;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.amqimplementationsesb.LogsType;
import com.evampsaanga.b2b.azerfon.companyinvoice.LangAndDesc;
import com.evampsaanga.b2b.azerfon.companyinvoice.ODSData;
import com.evampsaanga.b2b.azerfon.db.DBAzerfonFactory;
import com.evampsaanga.b2b.azerfon.pdf.Pdf;
import com.evampsaanga.b2b.azerfon.pdf.PdfToEmailLand;
import com.evampsaanga.b2b.azerfon.sendemail.SendEmailResponse;
import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.ThirdPartyNames;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Decrypter;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.evampsaanga.b2b.developer.utils.SOAPLoggingHandler;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Path("/azerfon")
public class MSISDNInvoiceLand {

	public static final Logger loggerV2 = Logger.getLogger("azerfon-esb");
	public static String modeulName = "MSISDN_INVOICE";

	@POST
	@Path("/getsummary")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public MSISDNSummaryResponse getSummary(@Body String requestBody, @Header("credentials") String credential) {

		Logs logs = new Logs();
		logs.setTransactionName(Transactions.MSISDN_INVOICE);
		logs.setThirdPartyName(ThirdPartyNames.ODS);
		logs.setTableType(LogsType.CompanyInvoice);
		MSISDNInvoiceRequest cclient = null;
		MSISDNSummaryResponse resp = new MSISDNSummaryResponse();

		String token;

		try {
			cclient = Helper.JsonToObject(requestBody, MSISDNInvoiceRequest.class);
			token = Helper.retrieveToken(Transactions.MSISDN_INVOICE, Helper.getValueFromJSON(requestBody, "msisdn"));
			logMessage("REQUEST: " + Helper.ObjectToJson(cclient));
			if (cclient != null) {
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				logs.setLang(cclient.getLang());
			}
		} catch (Exception ex) {
			loggerV2.info(Helper.GetException(ex));
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_401);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		if (cclient != null) {
			String credentials = null;
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
			} catch (Exception ex) {
				SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials == null) {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}

			if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				try {

					if (cclient.getIsSendMail().equalsIgnoreCase("true")) { // if
																			// send
																			// mail
																			// is
																			// true

						if (!cclient.getMailTo().isEmpty()) { // check if email
																// is given

							String path = ConfigurationManager
									.getConfigurationFromCache("invoice.details.pdfLocalPath");
							String ext = Constants.PDF_EXT;
							// String datetime = Helper.getPdfTimeStamp();
							String filename = cclient.getmsisdn() + "_" + cclient.getSelectedMsisdn() + "_summary"
									+ Constants.PDF_EXT;
							String pdfPath = path + filename;
							
							File file = new File(pdfPath);
							if(!file.exists()) {
								resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
								resp.setReturnMsg(ConfigurationManager
										.getConfigurationFromCache("invoice.details.pdf.not.exists."+cclient.getLang()));
								logs.setResponseCode(resp.getReturnCode());
								logs.setResponseDescription(resp.getReturnMsg());
								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
								logs.updateLog(logs);
								return resp;
							}

							SendEmailResponse emailResponse = new PdfToEmailLand().getResponse(ConfigurationManager
									.getConfigurationFromCache("invoice.details.msisdn.invoice.summary.email.subject."+cclient.getLang()),
									cclient.getMailTo(), pdfPath, filename, loggerV2, token);

							resp.setReturnCode(emailResponse.getReturnCode());
							resp.setReturnMsg(emailResponse.getReturnMsg());
							return resp;

						} else { // if email is not given,this will return error
									// reponse

							resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
							resp.setReturnMsg(ConfigurationManager
									.getConfigurationFromCache("actionhistory.generic.failure."+cclient.getLang()));
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} else {
						// Below both funtion fetch account ID if msisdn is
						// PayBySubs
						SummaryData data = new SummaryData();
						List<MSISDNSummary> summaryData = new ArrayList<MSISDNSummary>();

						logMessage(cclient.getmsisdn() + "-Get Number invoice summary from ODS.");
						Connection myConnection = DBAzerfonFactory.getConnection();
						PreparedStatement preparedStatement = null;
						ResultSet resultSet = null;

						try {
							String sql = "select msisdn, t.tariffname , round(sum(AMT+ t.tax_amt),2) from E_CARE_DEV.V_E_Care_TOTAL_INVOICE  t where msisdn=? and  ACCT_CODE = ?  and OBJ_ID is not null and DATA_DAY = to_date(?,'yyyymmdd') group by msisdn, t.tariffname";
							preparedStatement = myConnection.prepareStatement(sql);
							preparedStatement.setString(1, cclient.getSelectedMsisdn());
							preparedStatement.setString(2, cclient.getAcctCode());
							preparedStatement.setString(3, getBillingMonth(cclient.getBillMonth()));

							// preparedStatement.setString(1, "772778891");
							// preparedStatement.setString(2, "11000005804150");
							// preparedStatement.setString(3, "20200101");
							loggerV2.debug(cclient.getmsisdn() + "- Number invoice Query-" + sql);
							loggerV2.debug(cclient.getmsisdn() + "- Number Invoice Query params| AccountCode-"
									+ cclient.getAcctCode() + "| Billing month-"
									+ getBillingMonth(cclient.getBillMonth()) + "| MSISDN-"
									+ cclient.getSelectedMsisdn());
							resultSet = preparedStatement.executeQuery();

							while (resultSet.next()) {
								loggerV2.debug(cclient.getmsisdn() + "-Number from ODS -" + resultSet.getString(1));
								loggerV2.debug(
										cclient.getmsisdn() + "-Number Invoice Tariff Name-" + resultSet.getString(2));
								loggerV2.debug(cclient.getmsisdn() + "-Number Invoice Debt-" + resultSet.getString(3));
								MSISDNSummary msisdnSummary = new MSISDNSummary();

								msisdnSummary.setMsisdn(resultSet.getString(1));
								msisdnSummary.setTariff(resultSet.getString(2));
								msisdnSummary.setDebt(FormatValue(resultSet.getString(3)));
								summaryData.add(msisdnSummary);

							}
							preparedStatement.close();
							resultSet.close();
						} catch (Exception e) {
							loggerV2.info(cclient.getmsisdn() + Helper.GetException(e));
						} finally {
							try {
								if (resultSet != null)
									resultSet.close();
								if (preparedStatement != null)
									preparedStatement.close();
							} catch (Exception e) {
								loggerV2.info(Helper.GetException(e));
							}
						}
						loggerV2.info("ODS Response-" + new ObjectMapper().writeValueAsString(summaryData));
						data.setSummaryData(summaryData);
						resp.setData(data);
						String pdfPath = new Pdf().generateMsisdnSummaryPdf(cclient.getmsisdn(),
								cclient.getSelectedMsisdn(), resp, cclient.getLang());
						loggerV2.info("PDF Generated :" + pdfPath);

						resp.getData().setExportLink(pdfPath);

						resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
						resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
						///////////////////////////////////////////////////////////////////////////
						logMessage("RESPONSE: " + Helper.ObjectToJson(resp));

						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;

					}
				} catch (Exception ex) {
					loggerV2.info(Helper.GetException(ex));
					resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
					resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		}
		// logs.updateLog(logs);

		return resp;
	}


	private String getBillingMonth(String billMonth) {
		loggerV2.info("Billing Month in request: " + billMonth);
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		Calendar aCalendar = Calendar.getInstance();
		// add -1 month to current month

		aCalendar.add(Calendar.MONTH, Integer.parseInt(billMonth));
		// set DATE to 1, so first date of previous month
		aCalendar.set(Calendar.DATE, 1);
		return format.format(aCalendar.getTime());
	}

	private String getLangMap(String lang) {
		if (lang.equalsIgnoreCase("2"))
			return "2052";
		else if (lang.equalsIgnoreCase("4"))
			return "2060";
		else if (lang.equalsIgnoreCase("3"))
			return "2002";
		else
			return "2002";
	}

	@POST
	@Path("/getdetails")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public MSISDNDetailsResponse getDetails(@Body String requestBody, @Header("credentials") String credential) {
		logMessage("***************************************************************");
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.MSISDN_INVOICE);
		logs.setThirdPartyName(ThirdPartyNames.ODS);
		logs.setTableType(LogsType.CompanyInvoice);
		MSISDNInvoiceRequest cclient = null;
		MSISDNDetailsResponse resp = new MSISDNDetailsResponse();
		String token = "";
		try {
			cclient = Helper.JsonToObject(requestBody, MSISDNInvoiceRequest.class);
			token = Helper.retrieveToken(Transactions.MSISDN_INVOICE, cclient.getmsisdn());
			logMessage("REQUEST: " + Helper.ObjectToJson(cclient));
			if (cclient != null) {
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				logs.setLang(cclient.getLang());
			}
		} catch (Exception ex) {
			loggerV2.info(Helper.GetException(ex));
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_401);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		if (cclient != null) {
			String credentials = null;
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
			} catch (Exception ex) {
				SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials == null) {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}

			if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				try {

					if (cclient.getIsSendMail().equalsIgnoreCase("true")) { // if
																			// send
																			// mail
																			// is
																			// true

						if (!cclient.getMailTo().isEmpty()) { // check if email
																// is given

							String path = ConfigurationManager
									.getConfigurationFromCache("invoice.details.pdfLocalPath");
							String ext = Constants.PDF_EXT;
							// String datetime = Helper.getPdfTimeStamp();
							String filename = cclient.getmsisdn() + "_" + cclient.getSelectedMsisdn() + "_detail"
									+ Constants.PDF_EXT;
							String pdfPath = path + filename;

							SendEmailResponse emailResponse = new PdfToEmailLand().getResponse(ConfigurationManager
									.getConfigurationFromCache("invoice.details.msisdn.invoice.detailed.email.subject."+cclient.getLang()),
									cclient.getMailTo(), pdfPath, filename, loggerV2, token);

							resp.setReturnCode(emailResponse.getReturnCode());
							resp.setReturnMsg(emailResponse.getReturnMsg());
							return resp;

						} else { // if email is not given,this will return error
									// Response

							resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
							resp.setReturnMsg(ConfigurationManager
									.getConfigurationFromCache("actionhistory.generic.failure."+cclient.getLang()));
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} else {
						logMessage(cclient.getmsisdn()
								+ "=============Preparing Number Invoice Details =================");
						HashMap<String, LangAndDesc> listLangDesc = getCategoryWithLangAndDescFromODS(
								cclient.getmsisdn(), getLangMap(cclient.getLang()));
						HashMap<String, ODSData> hashmapODS_Data = getIndividualInvoiceFromODS(cclient, listLangDesc);

						MSISDNDetail msisdndetail = new MSISDNDetail();
						DetailsData detailsdata = new DetailsData();
						List<DetailsData> results = new ArrayList<DetailsData>();
						// Setting MSISDN
						detailsdata.setLabel(ConfigurationManager
								.getConfigurationFromCache("invoice.details.number." + cclient.getLang()));
						detailsdata.setAmount(cclient.getSelectedMsisdn());
						msisdndetail.setMsisdn(detailsdata);

						// -----------------------------------------------------

						String[] arrSecondHeadingSplitup = ConfigurationManager
								.getConfigurationFromCache("invoice.details.number.invoice.header.id.1.splitup")
								.split(",");
						String tariffName = "";
						for (int i = 0; i < arrSecondHeadingSplitup.length; i++) {
							if (listLangDesc.get(arrSecondHeadingSplitup[i]).getDesc() != null
									&& hashmapODS_Data.get(arrSecondHeadingSplitup[i]) != null) {
								detailsdata = new DetailsData();
								detailsdata.setLabel(listLangDesc.get(arrSecondHeadingSplitup[i]).getDesc());
								detailsdata.setAmount(
										FormatValue(hashmapODS_Data.get(arrSecondHeadingSplitup[i]).getAmount()));
								results.add(detailsdata);
								tariffName = hashmapODS_Data.get(arrSecondHeadingSplitup[0]).getTariffName();
							}

							else {
								detailsdata = new DetailsData();
								detailsdata.setLabel(
										ConfigurationManager.getConfigurationFromCache("invoice.details.header.id."
												+ arrSecondHeadingSplitup[i] + "." + cclient.getLang()));
								detailsdata.setAmount("0.00");
								results.add(detailsdata);
							}
						}
						msisdndetail.setResults(results);

						// Setting Tariff Name
						detailsdata = new DetailsData();
						detailsdata.setLabel(ConfigurationManager
								.getConfigurationFromCache("invoice.details.tariff." + cclient.getLang()));
						detailsdata.setAmount(tariffName);
						msisdndetail.setTariff(detailsdata);

						// ------------ TOTAL
						detailsdata = new DetailsData();
						detailsdata.setLabel(ConfigurationManager.getConfigurationFromCache(
								"invoice.details.number.invoice.total." + cclient.getLang()));
						double totalSum = 0.00;
						for (int i = 0; i < msisdndetail.getResults().size(); i++) {
							totalSum += Double.parseDouble(msisdndetail.getResults().get(i).getAmount());
						}
						loggerV2.info(cclient.getmsisdn() + "Total Amount is  :" + totalSum);
						detailsdata.setAmount(FormatValue(totalSum + ""));
						msisdndetail.setTotal(detailsdata);
						resp.setData(msisdndetail);
						String pdfPath = new Pdf().generateMsisdndetailPdf(cclient.getmsisdn(),
								cclient.getSelectedMsisdn(), resp, cclient.getLang());
						loggerV2.info("PDF Generated :" + pdfPath);

						resp.getData().setExportLink(pdfPath);
						resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
						resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);

						logMessage("RESPONSE: " + Helper.ObjectToJson(resp));

						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						return resp;
					}
				} catch (Exception ex) {
					loggerV2.info(Helper.GetException(ex));
					resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
					resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		}

		return resp;

	}

	private String FormatValue(String amount) {
		NumberFormat df = new DecimalFormat("#0.00");
		df.setRoundingMode(RoundingMode.FLOOR);
		return df.format(Double.parseDouble(amount));
	}

	private HashMap<String, ODSData> getIndividualInvoiceFromODS(MSISDNInvoiceRequest cclient,
			HashMap<String, LangAndDesc> listLangDesc) throws JsonProcessingException {

		logMessage(cclient.getmsisdn() + "-Get Number invoice data from ODS.");
		Connection myConnection = DBAzerfonFactory.getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		HashMap<String, ODSData> hashMapODSData = new HashMap<String, ODSData>();
		try {
			String sql = "select CATEGORY,   round(sum(AMT+ t.tax_amt),2), t.tariffname from E_CARE_DEV.V_E_Care_TOTAL_INVOICE  t where msisdn=? and ACCT_CODE = ? and OBJ_ID is not null and DATA_DAY = to_date(?,'yyyymmdd') group by  CATEGORY, msisdn, BILL_CYCLE_ID, t.tariffname";
			preparedStatement = myConnection.prepareStatement(sql);
			preparedStatement.setString(1, cclient.getSelectedMsisdn());
			preparedStatement.setString(2, cclient.getAcctCode());
			preparedStatement.setString(3, getBillingMonth(cclient.getBillMonth()));

			// preparedStatement.setString(1, "772778891");
			// preparedStatement.setString(2, "11000005804150");
			// preparedStatement.setString(3, "20200101");

			loggerV2.debug(cclient.getmsisdn() + "-Number Invoice Query-" + sql);
			loggerV2.debug(cclient.getmsisdn() + "-Number Invoice Query params| AccountCode-" + cclient.getAcctCode()
					+ "| Billing month" + getBillingMonth(cclient.getBillMonth()) + "| MSISDN"
					+ getBillingMonth(cclient.getBillMonth()));
			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				loggerV2.debug(cclient.getmsisdn() + "-Number Invoice CAT ID-" + resultSet.getString(1));
				loggerV2.debug(cclient.getmsisdn() + "-Number Invoice Amount-" + resultSet.getString(2));
				loggerV2.debug(cclient.getmsisdn() + "-Number Invoice Tariff-" + resultSet.getString(3));
				ODSData odsData = new ODSData();
				odsData.setCategoryId(resultSet.getString(1));
				odsData.setAmount(resultSet.getString(2));
				odsData.setTariffName(resultSet.getString(3));
				hashMapODSData.put(odsData.getCategoryId(), odsData);

			}
			preparedStatement.close();
			resultSet.close();
		} catch (Exception e) {
			loggerV2.info(cclient.getmsisdn() + Helper.GetException(e));
		} finally {
			try {
				if (resultSet != null)
					resultSet.close();
				if (preparedStatement != null)
					preparedStatement.close();
			} catch (Exception e) {
				loggerV2.info(Helper.GetException(e));
			}
		}
		loggerV2.info("ODS Data  -" + new ObjectMapper().writeValueAsString(hashMapODSData));
		return hashMapODSData;
	}

	private HashMap<String, LangAndDesc> getCategoryWithLangAndDescFromODS(String msisdn, String lang) {
		logMessage(msisdn + "-Number Invoice Details- Get Language Description and category ID in laguage-" + lang);

		HashMap<String, LangAndDesc> hashmap = new HashMap<>();
		Connection myConnection = DBAzerfonFactory.getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			String sql = "select ID,ID_LANG,DESCRIPTION from E_CARE_DEV.E_CARE_ID_DESCRIPTIONS t where COLUMN_ID=10 and ID_LANG=?";
			loggerV2.debug(sql);
			preparedStatement = myConnection.prepareStatement(sql);
			preparedStatement.setString(1, lang);
			loggerV2.debug(msisdn + "-ODS Number Invoice Categories Query-" + sql);
			loggerV2.debug(msisdn + "-ODS Number Invoice Categories  Query params| Language-" + lang);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				LangAndDesc langAndDesc = new LangAndDesc();
				langAndDesc.setCategoryId(resultSet.getString(1));
				langAndDesc.setLangId(resultSet.getString(2));
				langAndDesc.setDesc(resultSet.getString(3));
				hashmap.put(langAndDesc.getCategoryId(), langAndDesc);
			}
			preparedStatement.close();
			resultSet.close();
		} catch (Exception e) {
			loggerV2.info(msisdn + Helper.GetException(e));
		} finally {
			try {
				if (resultSet != null)
					resultSet.close();
				if (preparedStatement != null)
					preparedStatement.close();
			} catch (Exception e) {
				loggerV2.info(Helper.GetException(e));
			}
		}
		try {
			loggerV2.info("ODS LANGUAGE DESC-" + new ObjectMapper().writeValueAsString(hashmap));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			loggerV2.info(Helper.GetException(e));
		}
		return hashmap;
	}

	private void logMessage(String message) {
		loggerV2.info(modeulName + ": " + message);
	}

}
