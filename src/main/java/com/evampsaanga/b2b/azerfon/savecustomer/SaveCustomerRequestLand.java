package com.evampsaanga.b2b.azerfon.savecustomer;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.amqimplementationsesb.LogsType;
import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.ThirdPartyNames;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Decrypter;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.evampsaanga.b2b.developer.utils.SOAPLoggingHandler;
import com.saanga.magento.apiclient.RestClient;

@Path("/backcell")
public class SaveCustomerRequestLand {
	public static final Logger logger = Logger.getLogger("azerfon-esb");

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public SaveCustomerResponse Get(@Header("credentials") String credential, @Body() String requestBody)
			throws Exception {
		SOAPLoggingHandler.logger.info("Request" + requestBody);
		SaveCustomerRequest cclient = null;
		SaveCustomerResponse resp = new SaveCustomerResponse();
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.SAVE_CUSTOMER_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.SAVE_CUSTOMER);
		logs.setTableType(LogsType.SaveCustomer);
		String token = "";
		String TrnsactionName =Transactions.SAVE_CUSTOMER_TRANSACTION_NAME;

		try {

			token = Helper.retrieveToken(TrnsactionName, Helper.getValueFromJSON(requestBody, "msisdn"));

			logger.info(token + "-Request Data-" + requestBody);
			cclient = Helper.JsonToObject(requestBody, SaveCustomerRequest.class);
		} catch (Exception ex) {
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_400);
			return resp;
		}
		if (cclient != null) {
			String credentials = null;
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
			} catch (Exception ex) {
				 SOAPLoggingHandler.logger.error(Helper.GetException(ex));
			}
			if (credentials == null) {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				return resp;
			}
			if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
				String verification = Helper.validateRequest(cclient);
				if (!verification.equals("")) {
					resp.setReturnCode(ResponseCodes.ERROR_400);
					resp.setReturnMsg(verification);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
				return resp;
			}
			if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				// return RequestSoap( cclient);
				try {
					// com.cloudcontrolled.api.client.security.DumbX509TrustManager
					// se=null;
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("password", cclient.getPassword());
					jsonObject.put("confirm_password", cclient.getConfirmPassword());
					jsonObject.put("terms_and_conditions", cclient.getTermsAndConditions());
					jsonObject.put("temp", cclient.getTemp());
					JSONObject jsonObjectCustomer = new JSONObject();
					jsonObjectCustomer.put("title", cclient.getCustomerData().getTitle());
					jsonObjectCustomer.put("firstName", cclient.getCustomerData().getFirstName());
					jsonObjectCustomer.put("middleName", cclient.getCustomerData().getMiddleName());
					jsonObjectCustomer.put("lastName", cclient.getCustomerData().getLastName());
					jsonObjectCustomer.put("customerType", cclient.getCustomerData().getCustomerType());
					jsonObjectCustomer.put("gender", cclient.getCustomerData().getGender());
					jsonObjectCustomer.put("dob", cclient.getCustomerData().getDob());
					jsonObjectCustomer.put("accountId", cclient.getCustomerData().getAccountId());
					jsonObjectCustomer.put("language", cclient.getCustomerData().getLanguage());
					jsonObjectCustomer.put("effectiveDate", cclient.getCustomerData().getEffectiveDate());
					jsonObjectCustomer.put("expiryDate", cclient.getCustomerData().getExpiryDate());
					jsonObjectCustomer.put("subscriberType", cclient.getCustomerData().getSubscriberType());
					jsonObjectCustomer.put("status", cclient.getCustomerData().getStatus());
					jsonObjectCustomer.put("statusDetails", cclient.getCustomerData().getStatusDetails());
					jsonObjectCustomer.put("brandId", cclient.getCustomerData().getBrandId());
					jsonObjectCustomer.put("loyaltySegment", cclient.getCustomerData().getLoyaltySegment());
					jsonObjectCustomer.put("offeringId", cclient.getCustomerData().getOfferingId());
					jsonObjectCustomer.put("msisdn", cclient.getCustomerData().getMsisdn());
					jsonObject.put("customerData", jsonObjectCustomer);
					logger.info(token+"Magento Request:" + jsonObject.toString());
					String responseFromOTP = RestClient.SendCallToMagento(token,
							ConfigurationManager.getConfigurationFromCache("magento.app.signupflow.savecustomer"),
							jsonObject.toString());
					SOAPLoggingHandler.logger.info(token+"Response from otp: " + responseFromOTP);
					// MagentoResponseVerifyOTP
					try {
						MagentoResponse dataFromVerifyOTP = Helper.JsonToObject(responseFromOTP, MagentoResponse.class);
						logger.info(token+"Result code " + dataFromVerifyOTP.getResultCode());
						if (dataFromVerifyOTP.getResultCode().equals("32")) {
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
							return resp;
						} else {
							resp.setReturnCode(dataFromVerifyOTP.getResultCode());
							resp.setReturnMsg(dataFromVerifyOTP.getMsg());
							return resp;
						}
					} catch (Exception ex) {
						resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
						resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
						logger.info(token+Helper.GetException(ex));
						return resp;
					}
				} catch (JSONException ex) {
					resp.setReturnCode(ResponseCodes.MAGENTO_SERVER_ERROR_CODE);
					resp.setReturnMsg(ResponseCodes.MAGENTO_SERVER_ERROR_DES);
					logger.info(token+Helper.GetException(ex));
					return resp;
				} catch (Exception ex) {
					resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
					resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
					logger.info(token+Helper.GetException(ex));
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				return resp;
			}
		}
		return resp;
	}
}
