package com.evampsaanga.b2b.azerfon.cug;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;

public class CloseUserGroupResponse extends BaseResponse {
		
	private String isLastPage;
	private HashMap<String, CloseUserResponseData> groupData;
	private Map<String, CloseUserGroupData> users;
	public HashMap<String, CloseUserResponseData> getGroupData() {
		return groupData;
	}

	public void setGroupData(HashMap<String, CloseUserResponseData> groupData) {
		this.groupData = groupData;
	}

	public Map<String, CloseUserGroupData> getUsers() {
		return users;
	}

	public void setUsers(Map<String, CloseUserGroupData> users) {
		this.users = users;
	}

	

	

	public String getIsLastPage() {
		return isLastPage;
	}

	public void setIsLastPage(String isLastPage) {
		this.isLastPage = isLastPage;
	}
}
