package com.evampsaanga.b2b.azerfon.cug;

public class CloseUserGroupData {
	private String msisdn;
	private String tariffPlan;
	private String crmSubId;
	private String crmCustId;
	private String crmCustCode;
	private String virtualCorpCode;
	private String crmCorpCustId;
	private String cugGroupCode;
	private String groupCode;
	private String crmAccountId;
	private String accCode;
	private String crmAccIdPaid;
	private String groupName;
	private String groupNameDisplay;
	private String tariffNameDisplay;

	public String getGroupNameDisplay() {
		return groupNameDisplay;
	}
	public void setGroupNameDisplay(String groupNameDisplay) {
		this.groupNameDisplay = groupNameDisplay;
	}
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	public String getTariffPlan() {
		return tariffPlan;
	}
	public void setTariffPlan(String tariffPlan) {
		this.tariffPlan = tariffPlan;
	}
	public String getCrmSubId() {
		return crmSubId;
	}
	public void setCrmSubId(String crmSubId) {
		this.crmSubId = crmSubId;
	}
	public String getCrmCustId() {
		return crmCustId;
	}
	public void setCrmCustId(String crmCustId) {
		this.crmCustId = crmCustId;
	}
	public String getCrmCustCode() {
		return crmCustCode;
	}
	public void setCrmCustCode(String crmCustCode) {
		this.crmCustCode = crmCustCode;
	}
	public String getVirtualCorpCode() {
		return virtualCorpCode;
	}
	public void setVirtualCorpCode(String virtualCorpCode) {
		this.virtualCorpCode = virtualCorpCode;
	}
	public String getCrmCorpCustId() {
		return crmCorpCustId;
	}
	public void setCrmCorpCustId(String crmCorpCustId) {
		this.crmCorpCustId = crmCorpCustId;
	}
	public String getCugGroupCode() {
		return cugGroupCode;
	}
	public void setCugGroupCode(String cugGroupCode) {
		this.cugGroupCode = cugGroupCode;
	}
	public String getGroupCode() {
		return groupCode;
	}
	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}
	public String getCrmAccountId() {
		return crmAccountId;
	}
	public void setCrmAccountId(String crmAccountId) {
		this.crmAccountId = crmAccountId;
	}
	public String getAccCode() {
		return accCode;
	}
	public void setAccCode(String accCode) {
		this.accCode = accCode;
	}

	public String getCrmAccIdPaid() {
		return crmAccIdPaid;
	}
	public void setCrmAccIdPaid(String crmAccIdPaid) {
		this.crmAccIdPaid = crmAccIdPaid;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getTariffNameDisplay() {
		return tariffNameDisplay;
	}
	public void setTariffNameDisplay(String tariffNameDisplay) {
		this.tariffNameDisplay = tariffNameDisplay;
	}

}
