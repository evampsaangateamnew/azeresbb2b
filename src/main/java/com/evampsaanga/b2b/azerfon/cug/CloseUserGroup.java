package com.evampsaanga.b2b.azerfon.cug;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;

import com.evampsaanga.azerfon.appserver.refreshappservercache.BuildCacheRequestLand;
import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.amqimplementationsesb.LogsType;
import com.evampsaanga.b2b.authorization.AuthorizationAndAuthentication;
import com.evampsaanga.b2b.azerfon.db.DBFactory;
import com.evampsaanga.b2b.azerfon.getUsersV2.UsersGroupData;
import com.evampsaanga.b2b.azerfon.getUsersV2.UsersGroupRequest;
import com.evampsaanga.b2b.azerfon.getUsersV2.UsersGroupResponseData;
import com.evampsaanga.b2b.cache.UserGroupDataCache;
import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.ThirdPartyNames;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.evampsaanga.b2b.validator.rules.ChannelNotEmpty;
import com.evampsaanga.b2b.validator.rules.IPNotEmpty;
import com.evampsaanga.b2b.validator.rules.LangNotEmpty;
import com.evampsaanga.b2b.validator.rules.MSISDNNotEmpty;
import com.evampsaanga.b2b.validator.rules.ValidationResult;

@Path("/azerfon")
public class CloseUserGroup {

	public static final Logger logger = Logger.getLogger("azerfon-esb");

	@POST
	@Path("/cugV2")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public CloseUserGroupResponse closeUserGroup(@Body String requestBody, @Header("credentials") String credential)
			throws SQLException, InterruptedException {

		// Logging incoming request to log file

		Helper.logInfoMessageV2("--------------Test data as--------------");

		// Below is declaration block for variables to be used
		// Logs object to store values which are to be inserted in database for
		// reporting
		Logs logs = new Logs();

		// Generic information about logs is being set to logs object
		logs.setRequestDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.setTransactionName(Transactions.CUG_TRANSACTION_NAME);

		// SEND_FREE_SMS = "Database Internal";
		logs.setThirdPartyName(ThirdPartyNames.SEND_FREE_SMS);
		logs.setTableType(LogsType.CloseUserGroup);

		// Request object to store parsed data from requested string
		CloseUserGroupRequest closeUserGroupRequest = null;
		// response object which is to be returned to user
		ArrayList<CloseUserGroupData> usersGroupData = new ArrayList<CloseUserGroupData>();
		CloseUserGroupResponse usersgroupResponse = new CloseUserGroupResponse();
		Map<String, CloseUserGroupData> usersMap = new HashMap<>();
		// Authenticating the request using credentials string received in
		// header

		String token = "";
		String TrnsactionName = Transactions.CUG_TRANSACTION_NAME;

		try {

			token = Helper.retrieveToken(TrnsactionName, Helper.getValueFromJSON(requestBody, "msisdn"));

			Helper.logInfoMessageV2(token + "-Request Data-" + requestBody);
			closeUserGroupRequest = Helper.JsonToObject(requestBody, CloseUserGroupRequest.class);
		} catch (Exception ex) {
			// block catches the exception from mapper and sets the 400 bad
			// request response code
			Helper.logInfoMessageV2(token + Helper.GetException(ex));
			prepareErrorResponse(usersgroupResponse, logs, ResponseCodes.ERROR_400_CODE, ResponseCodes.ERROR_400);
		}

		boolean authenticationresult = AuthorizationAndAuthentication.authenticateAndAuthorizeCredentials(credential);

		// if request is not authenticated adding unauthorized response
		// codes to response and logs object
		if (!authenticationresult)
			prepareErrorResponse(usersgroupResponse, logs, ResponseCodes.ERROR_401_CODE, ResponseCodes.ERROR_401);

		// if requested string is converted to Java class then processing of
		// request goes below
		if (closeUserGroupRequest != null) {
			// block sets the mandatory parameters to logs object which are
			// taken from request body

			logs.setIp(closeUserGroupRequest.getiP());
			logs.setChannel(closeUserGroupRequest.getChannel());
			logs.setMsisdn(closeUserGroupRequest.getmsisdn());

			// if authentication is successful request forwarded for
			// validation of parameters
			if (authenticationresult) {
				ValidationResult validationResult = validateRequest(closeUserGroupRequest);

				if (validationResult.isValidationResult()) {
					logs.setResponseCode(usersgroupResponse.getReturnCode());
					logs.setResponseDescription(usersgroupResponse.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
				} else
					// if request parameters fails validation response is
					// prepared with validation error
					prepareErrorResponse(usersgroupResponse, logs, validationResult.getValidationCode(),
							validationResult.getValidationMessage());
			}
		}
		// getting the data from DB view

		try {

			if (closeUserGroupRequest.getSearchUsers() != null && !closeUserGroupRequest.getSearchUsers().isEmpty()) {
				Helper.logInfoMessageV2(token + " - Start Searching");
				usersGroupData = (ArrayList<CloseUserGroupData>) getCloseUsersGroupDataSearch(closeUserGroupRequest,
						token);
				if (usersGroupData == null || usersGroupData.isEmpty()) {
					Helper.logInfoMessageV2(token + " - No Users Found");
					usersgroupResponse.setReturnCode(ResponseCodes.SUCESS_CODE_200);
					usersgroupResponse.setReturnMsg("No Users Found");
					/*
					 * prepareErrorResponse(usersgroupResponse, logs,
					 * ResponseCodes.ERROR_401_CODE, ResponseCodes.ERROR_401);
					 */

				} else {
					HashMap<String, CloseUserResponseData> map = new HashMap<String, CloseUserResponseData>();
					ArrayList<CloseUserGroupData> usersData = new ArrayList<CloseUserGroupData>();

					for (int i = 0; i < usersGroupData.size(); i++) {
						// String groupID =
						// usersGroupData.get(i).getGroup_cust_name();
						String groupID = usersGroupData.get(i).getGroupName();
						String groupName = usersGroupData.get(i).getGroupNameDisplay();
						usersMap.put(usersGroupData.get(i).getMsisdn(), usersGroupData.get(i));
						if (map.containsKey(groupID)) {
							((CloseUserResponseData) map.get(groupID)).getUsersGroupData()
									.add((CloseUserGroupData) usersGroupData.get(i));
							((CloseUserResponseData) map.get(groupID)).setUserCount(
									((CloseUserResponseData) map.get(groupID)).getUsersGroupData().size());
							// map.get(groupID).getUsersGroupData().add(usersGroupData.get(i));
						} else {
							CloseUserResponseData data = new CloseUserResponseData();
							usersData = new ArrayList<CloseUserGroupData>();
							usersData.add(usersGroupData.get(i));
							data.setUsersGroupData(usersData);
							data.setGroupName(groupName);
							data.setUserCount(data.getUsersGroupData().size());
							map.put(groupID, data);
						}
					}

					/*
					 * Iterator it = map.entrySet().iterator(); while
					 * (it.hasNext()) { Map.Entry pair = (Map.Entry) it.next();
					 * ((UsersGroupResponseData) pair.getValue())
					 * .setUserCount(((UsersGroupResponseData)
					 * pair.getValue()).getUsersGroupData().size()); }
					 */
					usersgroupResponse.setGroupData(map);
					usersgroupResponse.setUsers(usersMap);
					// usersgroupResponse.setUsers(usersGroupData);
					logger.info("sizeCount&GroupData :" + usersGroupData.size());
					Helper.logInfoMessageV2("sizeCount&GroupData :" + usersGroupData.size());
					// usersgroupResponse.setUserCount(usersGroupData.size());
					Helper.logInfoMessageV2(closeUserGroupRequest.getmsisdn() + " - Current Page No is : "
							+ closeUserGroupRequest.getPageNum());
					if (closeUserGroupRequest.getPageNum().equalsIgnoreCase(totalPagesSearch(closeUserGroupRequest,usersGroupData))) {
						Helper.logInfoMessageV2(closeUserGroupRequest.getmsisdn()
								+ " - Entering  in if to Set LAst Page True : " + closeUserGroupRequest.getPageNum());

						usersgroupResponse.setIsLastPage("true");
					} else
						usersgroupResponse.setIsLastPage("false");
					usersgroupResponse.setReturnCode(ResponseCodes.SUCESS_CODE_200);
					usersgroupResponse.setReturnMsg(ResponseCodes.SUCESS_DES_200);
				}

			}

			else {
				usersGroupData = (ArrayList<CloseUserGroupData>) getCloseUserGroupFromCache(closeUserGroupRequest,
						token);
				if (usersGroupData == null || usersGroupData.isEmpty()) {
					Helper.logInfoMessageV2(token + " - No Users Found");
					usersgroupResponse.setReturnCode(ResponseCodes.SUCESS_CODE_200);
					usersgroupResponse.setReturnMsg("No Users Found");
					// prepareErrorResponse(usersgroupResponse, logs,
					// ResponseCodes.ERROR_401_CODE,
					// ResponseCodes.ERROR_401);

				} else {
					HashMap<String, CloseUserResponseData> map = new HashMap<String, CloseUserResponseData>();
					ArrayList<CloseUserGroupData> usersData = new ArrayList<CloseUserGroupData>();
					for (int i = 0; i < usersGroupData.size(); i++) {
						// String groupID =
						// usersGroupData.get(i).getGroup_cust_name();
						String groupID = usersGroupData.get(i).getGroupName();
						String groupName = usersGroupData.get(i).getGroupNameDisplay();
						usersMap.put(usersGroupData.get(i).getMsisdn(), usersGroupData.get(i));
						if (map.containsKey(groupID)) {
							((CloseUserResponseData) map.get(groupID)).getUsersGroupData()
									.add((CloseUserGroupData) usersGroupData.get(i));
							((CloseUserResponseData) map.get(groupID)).setUserCount(
									((CloseUserResponseData) map.get(groupID)).getUsersGroupData().size());
							// map.get(groupID).getUsersGroupData().add(usersGroupData.get(i));
						} else {
							CloseUserResponseData data = new CloseUserResponseData();
							usersData = new ArrayList<CloseUserGroupData>();
							usersData.add(usersGroupData.get(i));
							data.setUsersGroupData(usersData);
							data.setGroupName(groupName);
							data.setUserCount(data.getUsersGroupData().size());
							map.put(groupID, data);
						}
					}

					usersgroupResponse.setGroupData(map);
					usersgroupResponse.setUsers(usersMap);

					if (closeUserGroupRequest.getPageNum().equalsIgnoreCase(totalPages(closeUserGroupRequest,usersGroupData))) {
						Helper.logInfoMessageV2(closeUserGroupRequest.getmsisdn()
								+ " - Entering  in if to Set LAst Page True : " + closeUserGroupRequest.getPageNum());

						usersgroupResponse.setIsLastPage("true");
					} else
						usersgroupResponse.setIsLastPage("false");
					// usersgroupResponse.setData(usersGroupData);
					usersgroupResponse.setReturnCode(ResponseCodes.SUCESS_CODE_200);
					usersgroupResponse.setReturnMsg(ResponseCodes.SUCESS_DES_200);
				}

			}
		} catch (Exception e1) {
			logger.error(Helper.GetException(e1));
			prepareErrorResponse(usersgroupResponse, logs, ResponseCodes.ERROR_401_CODE, ResponseCodes.ERROR_401);

		}
		Helper.logInfoMessageV2(closeUserGroupRequest.getmsisdn() + " - getusers End point reached");
		return usersgroupResponse;

	}

	/**
	 * Validates the request
	 * 
	 * @param client
	 * @return validation response
	 */
	private ValidationResult validateRequest(CloseUserGroupRequest client) {
		ValidationResult validationResult = new ValidationResult();
		validationResult.setValidationResult(true);
		// Validating all the fields against non empty rule
		validationResult = new MSISDNNotEmpty().validateObject(client.getmsisdn());
		validationResult = new ChannelNotEmpty().validateObject(client.getChannel());
		validationResult = new IPNotEmpty().validateObject(client.getiP());
		validationResult = new LangNotEmpty().validateObject(client.getLang());
		return validationResult;
	}

	/**
	 * prepares error response and update logs in queue
	 * 
	 * @param usersGroupDataResponse
	 * @param logs
	 * @param returnCode
	 * @param returnMessage
	 */
	public void prepareErrorResponse(CloseUserGroupResponse usersGroupDataResponse, Logs logs, String returnCode,
			String returnMessage) {
		usersGroupDataResponse.setReturnCode(returnCode);
		usersGroupDataResponse.setReturnMsg(returnMessage);
		logs.setResponseCode(usersGroupDataResponse.getReturnCode());
		logs.setResponseDescription(usersGroupDataResponse.getReturnMsg());
		logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
		logs.updateLog(logs);
	}

	public String totalPagesSearch(CloseUserGroupRequest request,ArrayList<CloseUserGroupData> closeUserGroupDatas) {
		Helper.logInfoMessageV2(request.getmsisdn() + " - Landed in totalPages Method");
		double total = 0.0;

		String size = "";
//		size = getUsersCountSearch(request);
		size=closeUserGroupDatas.size()+"";

		String totalPages = "";
		double userLimit = Double.parseDouble(request.getLimitUsers());
		total = Double.parseDouble(size) / userLimit;
		Helper.logInfoMessageV2(request.getmsisdn() + " - Returning total pages without ceil :" + total);
		total = Math.ceil(total);
		int value = (int) total;

		totalPages = value + "";
		Helper.logInfoMessageV2(request.getmsisdn() + " - Returning total pages :" + totalPages);
		return totalPages;
	}

	public String getUsersCountSearch(CloseUserGroupRequest request) {
		String userCount = "0";
		String query = "select count(*) from user_data WHERE MSISDN Like " + "'%" + request.getSearchUsers() + "%'"
				+ "AND VIRTUAL_CORP_CODE='" + request.getVirtualCorpCode() + "' AND CUG_GROUP_CODE=''";
		try {
			Helper.logInfoMessageV2("Query to count users: " + query);
			ResultSet rs = DBFactory.getDbConnection().prepareStatement(query).executeQuery();
			while (rs.next()) {
				userCount = rs.getString(1);
			}
		} catch (SQLException e) {
			logger.info("ERROR: ", e);
		}
		Helper.logInfoMessageV2("User Count is: " + userCount);
		return userCount;
	}

	public List<CloseUserGroupData> getCloseUserGroupFromCache(CloseUserGroupRequest closeUserGroupRequest,
			String token) {
		String virtualCorpCode = closeUserGroupRequest.getVirtualCorpCode();

		if (BuildCacheRequestLand.usersCache.containsKey(virtualCorpCode + "." + closeUserGroupRequest.getLang())) {
			Helper.logInfoMessageV2("User with virtualCorpCode " + virtualCorpCode + " is found in Cache");
			// return BuildCacheRequestLand.usersCache.get(virtualCorpCode + "."
			// + usersGroupRequest.getLang());

			List<UsersGroupData> list = BuildCacheRequestLand.usersCache
					.get(virtualCorpCode + "." + closeUserGroupRequest.getLang());

			Helper.logInfoMessageV2("List size against virtual corp code" + closeUserGroupRequest.getVirtualCorpCode()
					+ "is :::" + list.size());

			List<UsersGroupData> listType = new ArrayList<UsersGroupData>();
			if (closeUserGroupRequest.getType().equalsIgnoreCase("voice")) {
				for (int i = 0; i < list.size(); i++) {

					if (list.get(i).getGroupCode() == null || list.get(i).getGroupCode().isEmpty()
							|| list.get(i).getGroupCode().equals(closeUserGroupRequest.getGroupId())) {
						listType.add(list.get(i));
					}
				}
			}

			else if (closeUserGroupRequest.getType().equalsIgnoreCase("data")) {
				for (int i = 0; i < list.size(); i++) {

					if (list.get(i).getGroupCode().equals(closeUserGroupRequest.getGroupId())) {
						listType.add(list.get(i));
					}
				}

			}

			List<CloseUserGroupData> listCug = new ArrayList<CloseUserGroupData>();
			for (int i = 0; i < listType.size(); i++) {
				if (listType.get(i).getCugGroupCode().equalsIgnoreCase("")) {

					CloseUserGroupData users = new CloseUserGroupData();
					users.setMsisdn(listType.get(i).getMsisdn());
					users.setTariffPlan(listType.get(i).getTariffPlan());
					users.setCrmSubId(listType.get(i).getCrmSubId());
					users.setCrmCustId(listType.get(i).getCrmCustId());
					users.setCrmCustCode(listType.get(i).getCrmCustCode());
					users.setVirtualCorpCode(listType.get(i).getVirtualCorpCode());
					users.setCrmCorpCustId(listType.get(i).getCrmCorpCustId());
					users.setCugGroupCode(listType.get(i).getCugGroupCode());
					users.setGroupCode(listType.get(i).getGroupCode());
					users.setCrmAccountId(listType.get(i).getCrmAccountId());
					users.setAccCode(listType.get(i).getAccCode());
					users.setCrmAccIdPaid(listType.get(i).getCrmAccIdPaid());
					users.setGroupName(listType.get(i).getGroupName());
					users.setGroupNameDisplay(listType.get(i).getGroupNameDisplay());
					users.setTariffNameDisplay(listType.get(i).getTariffNameDisplay());
					listCug.add(users);
				}
			}
			Helper.logInfoMessageV2("Pagination Started");
			int limitRecords = Integer.parseInt(closeUserGroupRequest.getLimitUsers());
			limitRecords = (Integer.parseInt(closeUserGroupRequest.getPageNum())) * limitRecords;
			Helper.logInfoMessageV2("Max limit is" + limitRecords);
			int start = (Integer.parseInt(closeUserGroupRequest.getPageNum()) - 1)
					* Integer.parseInt(closeUserGroupRequest.getLimitUsers());
			Helper.logInfoMessageV2("Start limit is" + start);
			List<CloseUserGroupData> usersGroupData = new ArrayList<CloseUserGroupData>();
			Helper.logInfoMessageV2("Loop Started for Pagination");
			for (int i = start; i < limitRecords && i < listCug.size(); i++) {
				usersGroupData.add(listCug.get(i));
			}
			Helper.logInfoMessageV2("Loop Ended of Pagination");
			Helper.logInfoMessageV2("Returning Part Pay Users");

			return usersGroupData;
		} else {
			Helper.logInfoMessageV2(token + "checkCloseUsersFromDb:  with virtualCorpCode: " + virtualCorpCode);
			List<UsersGroupData> list = getCloseUsersGroupDataDB(closeUserGroupRequest, token);
			Helper.logInfoMessageV2("List size against virtual corp code" + closeUserGroupRequest.getVirtualCorpCode()
					+ "is :::" + list.size());
			BuildCacheRequestLand.usersCache.put(virtualCorpCode + "." + closeUserGroupRequest.getLang(), list);

			Helper.logInfoMessageV2("List size against virtual corp code" + closeUserGroupRequest.getVirtualCorpCode()
					+ "is :::" + list.size());
			
			
			List<UsersGroupData> listType = new ArrayList<UsersGroupData>();
			if(closeUserGroupRequest.getType().equalsIgnoreCase("voice")){
				for (int i = 0; i < list.size(); i++) {
					
					if(list.get(i).getGroupCode()==null ||list.get(i).getGroupCode().isEmpty() || list.get(i).getGroupCode().equals(closeUserGroupRequest.getGroupId()))
					{
						listType.add(list.get(i));
					}
				}
			}
			
			else if(closeUserGroupRequest.getType().equalsIgnoreCase("data"))
			{
					for (int i = 0; i < list.size(); i++) {
					
					if(list.get(i).getGroupCode().equals(closeUserGroupRequest.getGroupId()))
					{
						listType.add(list.get(i));
					}
				}
				
			}
			
			
			List<CloseUserGroupData> listCug = new ArrayList<CloseUserGroupData>();
			for (int i = 0; i < listType.size(); i++) {
				if (listType.get(i).getCugGroupCode().equalsIgnoreCase("")) {

					CloseUserGroupData users = new CloseUserGroupData();
					users.setMsisdn(listType.get(i).getMsisdn());
					users.setTariffPlan(listType.get(i).getTariffPlan());
					users.setCrmSubId(listType.get(i).getCrmSubId());
					users.setCrmCustId(listType.get(i).getCrmCustId());
					users.setCrmCustCode(listType.get(i).getCrmCustCode());
					users.setVirtualCorpCode(listType.get(i).getVirtualCorpCode());
					users.setCrmCorpCustId(listType.get(i).getCrmCorpCustId());
					users.setCugGroupCode(listType.get(i).getCugGroupCode());
					users.setGroupCode(listType.get(i).getGroupCode());
					users.setCrmAccountId(listType.get(i).getCrmAccountId());
					users.setAccCode(listType.get(i).getAccCode());
					users.setCrmAccIdPaid(listType.get(i).getCrmAccIdPaid());
					users.setGroupName(listType.get(i).getGroupName());
					users.setGroupNameDisplay(listType.get(i).getGroupNameDisplay());
					users.setTariffNameDisplay(listType.get(i).getTariffNameDisplay());
					listCug.add(users);
				}
			}
			Helper.logInfoMessageV2("Pagination Started");
			int limitRecords = Integer.parseInt(closeUserGroupRequest.getLimitUsers());
			limitRecords = (Integer.parseInt(closeUserGroupRequest.getPageNum())) * limitRecords;
			Helper.logInfoMessageV2("Max limit is" + limitRecords);
			int start = (Integer.parseInt(closeUserGroupRequest.getPageNum()) - 1)
					* Integer.parseInt(closeUserGroupRequest.getLimitUsers());
			Helper.logInfoMessageV2("Start limit is" + start);
			List<CloseUserGroupData> usersGroupData = new ArrayList<CloseUserGroupData>();
			Helper.logInfoMessageV2("Loop Started for Pagination");
			for (int i = start; i < limitRecords && i < listCug.size(); i++) {
				usersGroupData.add(listCug.get(i));
			}
			Helper.logInfoMessageV2("Loop Ended of Pagination");
			Helper.logInfoMessageV2("Returning Part Pay Users");

			return usersGroupData;

		}
	}

	private List<UsersGroupData> getCloseUsersGroupDataDB(CloseUserGroupRequest usersGroupRequest, String token) {
		List<UsersGroupData> usersData = new ArrayList<UsersGroupData>();
		// MagentoServices services = new MagentoServices();
		// printing DB Data
		// services.printDB();

		try {

			// int
			// limitRecords=Integer.parseInt(usersGroupRequest.getLimitUsers());
			// Helper.logInfoMessageV2(token+ "Limit record Value :
			// "+limitRecords);
			// int limits=
			// (Integer.parseInt(usersGroupRequest.getPageNum())-1)*limitRecords;
			// Helper.logInfoMessageV2(token+ "Limit alculated record Value :
			// "+limits);

			String sqlQuery = "SELECT * FROM user_data WHERE VIRTUAL_CORP_CODE='"
					+ usersGroupRequest.getVirtualCorpCode() + "'";
			Helper.logInfoMessageV2(token + " --   SQL Query @@GetusersGroupData is :" + sqlQuery);
			ResultSet rs = DBFactory.getDbConnection().prepareStatement(sqlQuery).executeQuery();
			/*
			 * ResultSetMetaData metadata = rs.getMetaData(); int columnCount =
			 * metadata.getColumnCount(); for (int i = 1; i <= columnCount; i++)
			 * { Helper.logInfoMessage(metadata.getColumnName(i) + ", "); }
			 */

			String row = "";
			while (rs.next()) {
				Helper.logInfoMessageV2(token + " SQL Query Result :" + row);
				UsersGroupData users = new UsersGroupData();
				users.setMsisdn(rs.getString("MSISDN"));
				users.setTariffPlan(rs.getString("TARIFF_PLAN"));
				users.setCrmSubId(rs.getString("CRM_SUB_ID"));
				users.setCrmCustId(rs.getString("CRM_CUST_ID"));
				users.setCrmCustCode(rs.getString("CUST_CODE"));
				users.setVirtualCorpCode(rs.getString("VIRTUAL_CORP_CODE"));
				users.setCrmCorpCustId(rs.getString("CRM_CORP_CUST_ID"));
				users.setCugGroupCode(rs.getString("CUG_GROUP_CODE"));
				users.setGroupCode(rs.getString("GROUP_CODE"));
				users.setCrmAccountId(rs.getString("CRM_ACCT_ID"));
				users.setAccCode(rs.getString("ACCT_CODE"));
				users.setCrmAccIdPaid(rs.getString("CRM_ACCT_ID_PAID"));
				users.setTariffNameDisplay(ConfigurationManager.getConfigurationFromCache(
						"tariff.name." + rs.getString("TARIFF_PLAN") + "." + usersGroupRequest.getLang()));

				users.setGroupName(
						groupName(users.getVirtualCorpCode(), users.getGroupCode(), usersGroupRequest.getType()));
				String disp_name = ConfigurationManager
						.getConfigurationFromCache(ConfigurationManager.MAPPING_GROUPIDTRANS
								+ usersGroupRequest.getLang() + "." + users.getGroupName());
				users.setGroupNameDisplay(disp_name);
				// logger.info("@@@@@@@@@@@@@@@@ users data :" + users);
				usersData.add(users);
			}
			rs.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			Helper.logInfoMessageV2(token + "SQLException", Helper.GetException(e));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Helper.logInfoMessageV2(token + "Exception", Helper.GetException(e));
		}
		return usersData;
	}

	private List<CloseUserGroupData> getCloseUsersGroupDataSearch(CloseUserGroupRequest usersGroupRequest,
			String token) {
		List<CloseUserGroupData> usersData = new ArrayList<CloseUserGroupData>();
		// MagentoServices services = new MagentoServices();
		// printing DB Data
		// services.printDB();

		try {
			/*
			 * String sqlQuery =
			 * "select PRI_IDENTITY,PROFILE_ID,BRAND_ID,CORP_ID,GROUP_SUBS_ID,GROUP_CUST_NAME,GROUP_ID,"
			 * +
			 * "CORP_CRM_CUST_ID,PIC_NAME,CORP_ACCT_CODE,CORP_CRM_ACCT_ID,CUST_FST_NAME,CUST_LAST_NAME from V_E_CARE_CORP_INFO where CORP_CRM_CUST_ID = "
			 * + customerID + "order by PRI_IDENTITY";
			 */
			int limitRecords = Integer.parseInt(usersGroupRequest.getLimitUsers());
			Helper.logInfoMessageV2(token + "Limit record Value : " + limitRecords);
			int limits = (Integer.parseInt(usersGroupRequest.getPageNum()) - 1) * limitRecords;
			Helper.logInfoMessageV2(token + "Limit alculated record Value : " + limits);

			String sqlQuery = "SELECT * FROM user_data WHERE VIRTUAL_CORP_CODE='"
					+ usersGroupRequest.getVirtualCorpCode() + "' AND MSISDN LIKE '%"
					+ usersGroupRequest.getSearchUsers() + "%' AND CUG_GROUP_CODE=''" + " LIMIT " + limits + " , "
					+ limitRecords + "  ";
			Helper.logInfoMessageV2(token + " --   SQL Query @@GetusersGroupDataSearch is :" + sqlQuery);
			ResultSet rs = DBFactory.getDbConnection().prepareStatement(sqlQuery).executeQuery();

			String row = "";
			while (rs.next()) {
				Helper.logInfoMessageV2(token + " SQL Query Result :" + row);
				CloseUserGroupData users = new CloseUserGroupData();
				users.setMsisdn(rs.getString("MSISDN"));
				users.setTariffPlan(rs.getString("TARIFF_PLAN"));
				users.setCrmSubId(rs.getString("CRM_SUB_ID"));
				users.setCrmCustId(rs.getString("CRM_CUST_ID"));
				users.setCrmCustCode(rs.getString("CUST_CODE"));
				users.setVirtualCorpCode(rs.getString("VIRTUAL_CORP_CODE"));
				users.setCrmCorpCustId(rs.getString("CRM_CORP_CUST_ID"));
				users.setCugGroupCode(rs.getString("CUG_GROUP_CODE"));
				users.setGroupCode(rs.getString("GROUP_CODE"));
				users.setCrmAccountId(rs.getString("CRM_ACCT_ID"));
				users.setAccCode(rs.getString("ACCT_CODE"));
				users.setCrmAccIdPaid(rs.getString("CRM_ACCT_ID_PAID"));
				users.setTariffNameDisplay(ConfigurationManager.getConfigurationFromCache(
						"tariff.name." + rs.getString("TARIFF_PLAN") + "." + usersGroupRequest.getLang()));

				users.setGroupName(
						groupName(users.getVirtualCorpCode(), users.getGroupCode(), usersGroupRequest.getType()));
				String disp_name = ConfigurationManager
						.getConfigurationFromCache(ConfigurationManager.MAPPING_GROUPIDTRANS
								+ usersGroupRequest.getLang() + "." + users.getGroupName());
				users.setGroupNameDisplay(disp_name);
				// logger.info("@@@@@@@@@@@@@@@@ users data :" + users);
				usersData.add(users);
			}
			rs.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			Helper.logInfoMessageV2(token + "SQLException", Helper.GetException(e));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Helper.logInfoMessageV2(token + "Exception", Helper.GetException(e));
		}
		
		
		List<CloseUserGroupData> usersGroupDataSearch = new ArrayList<CloseUserGroupData>();
		if(usersGroupRequest.getType().equalsIgnoreCase("data"))
			{
			Helper.logInfoMessageV2(token+"Data Block Before Filter List Size is:: " + usersData.size());
			usersGroupDataSearch =usersData.stream().filter(item-> item.getGroupCode().equalsIgnoreCase(usersGroupRequest.getGroupId())).collect(Collectors.toList());
					
			Helper.logInfoMessageV2(token+"Data Block After Filter List Size is:: " + usersGroupDataSearch.size());

			}
		
		if(usersGroupRequest.getType().equalsIgnoreCase("voice"))
		{
			Helper.logInfoMessageV2(token+"Voice Block Before Filter List Size is:: " + usersData.size());
			Predicate<CloseUserGroupData> con1 =item-> item.getGroupCode().equalsIgnoreCase(usersGroupRequest.getGroupId());
			Predicate<CloseUserGroupData> con2 =item-> item.getGroupCode().equalsIgnoreCase("");
			
			usersGroupDataSearch =usersData.stream().filter(con1.or(con2)).collect(Collectors.toList());
			Helper.logInfoMessageV2(token+"Voice Block After Filter List Size is:: " + usersGroupDataSearch.size());
		}
		
		
		return usersGroupDataSearch;
	}

	// public ArrayList<CloseUserGroupData>
	// getCloseUserGroup(CloseUserGroupRequest closeUserGroupRequest, String
	// token)
	// {
	// ArrayList<CloseUserGroupData> usersData = new
	// ArrayList<CloseUserGroupData>();
	//
	// try {
	//
	// int limitRecords=Integer.parseInt(closeUserGroupRequest.getLimitUsers());
	// Helper.logInfoMessageV2(token+ "Limit record Value : "+limitRecords);
	// int limits=
	// (Integer.parseInt(closeUserGroupRequest.getPageNum())-1)*limitRecords;
	// Helper.logInfoMessageV2(token+ "Limit alculated record Value : "+limits);
	//
	// String sqlQuery="SELECT * FROM user_data WHERE
	// VIRTUAL_CORP_CODE='"+closeUserGroupRequest.getVirtualCorpCode()+"' AND
	// CUG_GROUP_CODE ='' LIMIT "+limits+" , "+limitRecords+" ";
	// Helper.logInfoMessageV2(token+ " -- SQL Query @@GetusersGroupData is :" +
	// sqlQuery);
	// ResultSet rs =
	// DBFactory.getDbConnection().prepareStatement(sqlQuery).executeQuery();
	// /*
	// * ResultSetMetaData metadata = rs.getMetaData(); int columnCount =
	// * metadata.getColumnCount(); for (int i = 1; i <= columnCount; i++)
	// * { Helper.logInfoMessage(metadata.getColumnName(i) + ", "); }
	// */
	//
	// String row = "";
	// while (rs.next()) {
	// Helper.logInfoMessageV2(token+" SQL Query Result :" + row);
	// CloseUserGroupData users = new CloseUserGroupData();
	// users.setMsisdn(rs.getString("MSISDN"));
	// users.setTariffPlan(rs.getString("TARIFF_PLAN"));
	// users.setCrmSubId(rs.getString("CRM_SUB_ID"));
	// users.setCrmCustId(rs.getString("CRM_CUST_ID"));
	// users.setCrmCustCode(rs.getString("CUST_CODE"));
	// users.setVirtualCorpCode(rs.getString("VIRTUAL_CORP_CODE"));
	// users.setCrmCorpCustId(rs.getString("CRM_CORP_CUST_ID"));
	// users.setCugGroupCode(rs.getString("CUG_GROUP_CODE"));
	// users.setGroupCode(rs.getString("GROUP_CODE"));
	// users.setCrmAccountId(rs.getString("CRM_ACCT_ID"));
	// users.setAccCode(rs.getString("ACCT_CODE"));
	// users.setCrmAccIdPaid(rs.getString("CRM_ACCT_ID_PAID"));
	//
	//
	// users.setGroupName( groupName( users.getVirtualCorpCode(),
	// users.getGroupCode(), closeUserGroupRequest.getType()));
	// String disp_name =
	// ConfigurationManager.getConfigurationFromCache(ConfigurationManager.MAPPING_GROUPIDTRANS
	// + closeUserGroupRequest.getLang() + "." + users.getGroupName());
	// users.setGroupNameDisplay(disp_name);
	// // logger.info("@@@@@@@@@@@@@@@@ users data :" + users);
	// usersData.add(users);
	// }
	// rs.close();
	//
	// } catch (SQLException e) {
	// // TODO Auto-generated catch block
	// Helper.logInfoMessageV2(token+"SQLException", Helper.GetException(e));
	// } catch (Exception e) {
	// // TODO Auto-generated catch block
	// Helper.logInfoMessageV2(token+"Exception", Helper.GetException(e));
	// }
	// return usersData;
	// }

	public String groupName(String virtualCorpCode, String groupCode, String type) {
		String groupName = "";

		if (type.equalsIgnoreCase("data")) {
			if (virtualCorpCode != null && !virtualCorpCode.isEmpty() && !virtualCorpCode.equalsIgnoreCase("")
					&& groupCode != null && !groupCode.isEmpty() && !groupCode.equalsIgnoreCase("")) {
				groupName = "PartPay";
			}
		} else if (type.equalsIgnoreCase("voice")) {
			if (virtualCorpCode != null && !virtualCorpCode.isEmpty() && !virtualCorpCode.equalsIgnoreCase("")
					&& groupCode != null && !groupCode.isEmpty() && !groupCode.equalsIgnoreCase("")) {
				groupName = "PartPay";
			} else if (virtualCorpCode != null && !virtualCorpCode.isEmpty() && !virtualCorpCode.equalsIgnoreCase("")
					&& (groupCode == null || groupCode.isEmpty() && groupCode.equalsIgnoreCase(""))) {
				groupName = "PayBySubs";
			}
		}
		return groupName;

	}

	public String totalPages(CloseUserGroupRequest request,ArrayList<CloseUserGroupData> closeUserGroupDatas) {
		Helper.logInfoMessageV2(request.getmsisdn() + " - Landed in totalPages Method");
		double total = 0.0;

		String size = "";
//		size = getUsersCount(request.getVirtualCorpCode());
		size=closeUserGroupDatas.size()+"";

		String totalPages = "";
		double userLimit = Double.parseDouble(request.getLimitUsers());
		total = Double.parseDouble(size) / userLimit;
		Helper.logInfoMessageV2(request.getmsisdn() + " - Returning total pages without ceil :" + total);
		total = Math.ceil(total);
		int value = (int) total;

		totalPages = value + "";
		Helper.logInfoMessageV2(request.getmsisdn() + " - Returning total pages :" + totalPages);
		return totalPages;
	}

//	private String getUsersCount(String virtualCorpCode) {
//		String userCount = "0";
//		String query = "select count(*) from user_data where VIRTUAL_CORP_CODE = " + virtualCorpCode
//				+ " AND CUG_GROUP_CODE =''";
//		try {
//			Helper.logInfoMessageV2("Query to count users: " + query);
//			ResultSet rs = DBFactory.getDbConnection().prepareStatement(query).executeQuery();
//			while (rs.next()) {
//				userCount = rs.getString(1);
//			}
//		} catch (SQLException e) {
//			logger.info("ERROR: ", e);
//		}
//		Helper.logInfoMessageV2("User Count is: " + userCount);
//		return userCount;
//	}

}
