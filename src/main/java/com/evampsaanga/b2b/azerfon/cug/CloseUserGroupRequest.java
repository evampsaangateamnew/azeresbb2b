package com.evampsaanga.b2b.azerfon.cug;

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;

public class CloseUserGroupRequest extends BaseRequest {

	private String virtualCorpCode;
	private String pageNum;
	private String limitUsers;
	private String searchUsers;
	private String type;
	private String groupId;

	public String getVirtualCorpCode() {
		return virtualCorpCode;
	}

	public void setVirtualCorpCode(String virtualCorpCode) {
		this.virtualCorpCode = virtualCorpCode;
	}

	public String getPageNum() {
		return pageNum;
	}

	public void setPageNum(String pageNum) {
		this.pageNum = pageNum;
	}

	public String getLimitUsers() {
		return limitUsers;
	}

	public void setLimitUsers(String limitUsers) {
		this.limitUsers = limitUsers;
	}

	public String getSearchUsers() {
		return searchUsers;
	}

	public void setSearchUsers(String searchUsers) {
		this.searchUsers = searchUsers;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
}
