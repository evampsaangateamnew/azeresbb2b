package com.evampsaanga.b2b.azerfon.cug;

import java.util.ArrayList;

import com.evampsaanga.b2b.azerfon.getUsersV2.UsersGroupData;

public class CloseUserResponseData {
	private String groupName;
	private int userCount;
	private ArrayList<CloseUserGroupData> usersGroupData;
	

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public int getUserCount() {
		return userCount;
	}

	public void setUserCount(int userCount) {
		this.userCount = userCount;
	}

	public ArrayList<CloseUserGroupData> getUsersGroupData() {
		return usersGroupData;
	}

	public void setUsersGroupData(ArrayList<CloseUserGroupData> usersGroupData) {
		this.usersGroupData = usersGroupData;
	}
}
