package com.evampsaanga.b2b.security;

import java.io.UnsupportedEncodingException;
import org.apache.log4j.Logger;

import com.evampsaanga.b2b.developer.utils.Helper;

public class Encrypter {
	public static void main(String[] args) throws UnsupportedEncodingException {
		System.out.println(
				Encrypter.encodeString("jdbc:mysql://10.220.48.10:3306/esmg?useUnicode=true&characterEncoding=UTF-8"));
		System.out.println(Encrypter.decodeString(
				"gLjaGkSdNBgK1zA9fA0z9QqP9D9+9anhBA/tNI+7jpC/cXDg3QBJWbXgNs0kHoZ+2UNRqjBvsvKctqmxRo7TXXgqDuKCFNsEB4+vG8mc4od0Dpqn6HuC7nTygySL+yskeJTWA8ZVwGg="));
		System.out.println("hi");
		// System.out.println(new
		// String("eeee".getBytes("ISO-8859-1"),"UTF-8").toString());
	}

	public static final Logger logger = Logger.getLogger("azerfon-esb");

	public static String encodeString(String data) {
		try {
			String encoded = DesEncrypter.encodeString(AESEncrypter.encodeString(data));
			return encoded;
		} catch (Exception e) {
			logger.error(Helper.GetException(e));
			return "";
		}
	}

	public static String decodeString(String data) {
		try {
			String decode = DesEncrypter.decodeString(data);
			return AESEncrypter.decodeString(decode);
		} catch (Exception e) {
			System.out.println(Helper.GetException(e));
			e.printStackTrace();
			return "";
		}
	}
}
