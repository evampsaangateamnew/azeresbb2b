package com.evampsaanga.b2b.developer.utils;

import java.util.Map;
import java.util.List;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.Handler;

//import org.apache.camel.Handler;

import com.huawei.bme.cbsinterface.bbservices.BbServices;
import com.huawei.bme.cbsinterface.bcservices.BcServices;
import com.huawei.bss.soaif._interface.hlrwebservice.HLRWEBPortType;
import com.huawei.bss.soaif._interface.ussdgateway.USSDGatewayInterfaces;
import com.huawei.crm.service.HuaweiCRMPortType;
import com.ngbss.evampsaanga.services.SadmHandler;
import com.nsn.devicemanagement.wsdl.provisioning._2.ProvisioningIf;
//import com.sun.xml.bind.v2.schemagen.xmlschema.List;

public class SoapHandlerService {
	@SuppressWarnings("rawtypes")
	public static <T> void configureBinding(T PortType) {
		Map<String, Object> reqC = ((BindingProvider) PortType).getRequestContext();
		if (PortType instanceof ProvisioningIf) {
			BindingProvider bindingProvider = ((BindingProvider) PortType);
			List<Handler> handlerChain = bindingProvider.getBinding().getHandlerChain();
			handlerChain.add(new SadmHandler());
			bindingProvider.getBinding().setHandlerChain(handlerChain);
			// Set timeout until a connection is established
			reqC.put("javax.xml.ws.client.connectionTimeout",20000);
			// Set timeout until the response is received
			reqC.put("javax.xml.ws.client.receiveTimeout",20000);
			return;
		}
		BindingProvider bindingProvider = ((BindingProvider) PortType);
		List<Handler> handlerChain = bindingProvider.getBinding().getHandlerChain();
		handlerChain.add(new SOAPLoggingHandler());
		bindingProvider.getBinding().setHandlerChain(handlerChain);
		if (PortType instanceof HuaweiCRMPortType) {
			// Set timeout until a connection is established
			reqC.put("javax.xml.ws.client.connectionTimeout",20000);
			// Set timeout until the response is received
			reqC.put("javax.xml.ws.client.receiveTimeout",20000);
		} else if (PortType instanceof HLRWEBPortType) {
			// Set timeout until a connection is established
			reqC.put("javax.xml.ws.client.connectionTimeout",20000);
			// Set timeout until the response is received
			reqC.put("javax.xml.ws.client.receiveTimeout",20000);
		} else if (PortType instanceof USSDGatewayInterfaces) {
			// Set timeout until a connection is established
			reqC.put("javax.xml.ws.client.connectionTimeout",20000);
			// Set timeout until the response is received
			reqC.put("javax.xml.ws.client.receiveTimeout",20000);
		} else if (PortType instanceof BbServices) {
			// Set timeout until a connection is established
			reqC.put("javax.xml.ws.client.connectionTimeout",20000);
			// Set timeout until the response is received
			reqC.put("javax.xml.ws.client.receiveTimeout",20000);
			return;
		} else if (PortType instanceof BcServices) {
			// Set timeout until a connection is established
			reqC.put("javax.xml.ws.client.connectionTimeout",20000);
			// Set timeout until the response is received
			reqC.put("javax.xml.ws.client.receiveTimeout",20000);
			return;
		} else {
			// Set timeout until a connection is established
			reqC.put("javax.xml.ws.client.connectionTimeout",20000);
			// Set timeout until the response is received
			reqC.put("javax.xml.ws.client.receiveTimeout",20000);
		}
	}
}
