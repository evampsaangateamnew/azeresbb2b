package com.evampsaanga.b2b.developer.utils;

import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SampleClass {

	public static void main(String[] args)
			throws ParseException, ClassNotFoundException, UnsupportedEncodingException, SQLException {
		//
		// // Total No of Days
		// String dueDateNDisp = "2020/03/02 15:33:22";
		// Date date1 = new SimpleDateFormat("yyyy/MM/dd
		// hh:mm:ss").parse(dueDateNDisp);
		// System.out.println(new SimpleDateFormat("dd/MM/yy
		// HH:mm:ss").format(date1));

		// System.out.println(DBFactory.insertMessageIntoEsmg("705556899", "Test
		// Message B2B", "3", "705556899",
		// new StringBuffer(), "token"));
		// Calendar aCalendar = Calendar.getInstance();
		// add -1 month to current month
		// aCalendar.add(Calendar.MONTH, -1);
		// set DATE to 1, so first date of previous month
		// aCalendar.set(Calendar.DATE, 1);

		// Date firstDateOfPreviousMonth = aCalendar.getTime();

		// set actual maximum date of previous month
		// aCalendar.set(Calendar.DATE,
		// aCalendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		// read it
		// Date lastDateOfPreviousMonth = aCalendar.getTime();

		String str = "10,330,320,0,310,311,313,315,.1,110,111,112,113";
		System.out.println(Arrays.asList(str.split(",")));

		List<Integer> arr = new ArrayList<>();

		arr.add(1);
		arr.add(2);
		arr.add(3);
		arr.add(4);
		arr.add(5);

		System.out.println(arr.get(arr.size() - 1));

	}
}
