package com.evampsaanga.b2b.developer.utils;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import javax.crypto.NoSuchPaddingException;

public class Decrypter {
	private static DesEncrypter instance = null;

	private Decrypter() {
	}

	public static DesEncrypter getInstance() throws InvalidKeyException, NoSuchAlgorithmException,
			UnsupportedEncodingException, InvalidKeySpecException, NoSuchPaddingException {
		if (instance == null) {
			instance = new DesEncrypter();
		}
		return instance;
	}
}