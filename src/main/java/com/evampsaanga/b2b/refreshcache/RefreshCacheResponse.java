package com.evampsaanga.b2b.refreshcache;

import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;

public class RefreshCacheResponse extends BaseResponse{

    private String entityId;

    public String getEntityId() {
	return entityId;
    }

    public void setEntityId(String entityId) {
	this.entityId = entityId;
    }
    
}
