package com.evampsaanga.b2b.refreshcache;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.azerfon.appserver.refreshappservercache.BuildCacheRequestLand;
import com.evampsaanga.azerfon.appserver.refreshappservercache.GroupRestrictionCacheManager;
import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.amqimplementationsesb.LogsType;
import com.evampsaanga.b2b.amqimplementationsesb.QueueMessageListener;
import com.evampsaanga.b2b.azerfon.getcdrsbydate.GetCDRsByDateLand;
import com.evampsaanga.b2b.azerfon.getsubscriber.CRMSubscriberService;
import com.evampsaanga.b2b.cache.UserGroupDataCache;
import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.ThirdPartyNames;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Decrypter;
import com.evampsaanga.b2b.developer.utils.Helper;

@Path("/cacheManagement")
public class RefreshCacheRequestLand {

	public static final Logger logger = Logger.getLogger("azerfon-esb");

	@POST
	@Path("/refreshconfigcache")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public RefreshCacheResponse RefreshConfigCache(@Header("credentials") String credential,
			@Body() String requestBody) {
		logger.info("Refreh ESB Cache Request" + requestBody);
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.REFRESH_ESB_CACHE_NAME);
		logs.setThirdPartyName(ThirdPartyNames.INTERNAL_CACHE);
		logs.setTableType(LogsType.Login);
		RefreshCacheRequest cclient = null;
		RefreshCacheResponse resp = new RefreshCacheResponse();
		try {
			cclient = Helper.JsonToObject(requestBody, RefreshCacheRequest.class);
			logs.setIp(cclient.getiP());
			logs.setChannel(cclient.getChannel());
			logs.setMsisdn(cclient.getmsisdn());
			logs.setLang(cclient.getLang());
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_400);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}

		{
			String credentials = null;
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
			} catch (Exception ex) {
				logger.error(Helper.GetException(ex));
			}
			if (credentials == null) {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
				String verification = Helper.validateRequest(cclient);
				if (!verification.equals("")) {
					resp.setReturnCode(ResponseCodes.ERROR_400);
					resp.setReturnMsg(verification);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				if (cclient.getIsB2B().equalsIgnoreCase("B2CEsb")) {
					QueueMessageListener.hashmapTransactionMappings.clear();
					CRMSubscriberService.coreServicesCategoryCacheHashMap.clear();
					ConfigurationManager.propertiesMessageTemplates.clear();
//					HashMap<String, String> propertiesCacheHashMap= ConfigurationManager.propertiesCacheHashMap;
					if (BuildCacheRequestLand.configurationCache == null)
						BuildCacheRequestLand.initHazelcast();
//					propertiesCacheHashMap.clear();

				} else if (cclient.getIsB2B().equalsIgnoreCase("B2BPic")) {
					CRMSubscriberService.coreServicesCategoryCacheHashMap.clear();
					BuildCacheRequestLand.usersCache.clear();
					UserGroupDataCache.usersCount.clear();
				} else if (cclient.getIsB2B().equalsIgnoreCase("B2CCdrs")) {

					GetCDRsByDateLand.usageHistoryTranslationMapping.clear();
					GetCDRsByDateLand.cdrsColumnMapping.clear();
				}
				resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
				resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.updateLog(logs);
				return resp;

			} else {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.updateLog(logs);
				return resp;
			}
		}
	}

}
