package com.evampsaanga.b2b.ulduzum;

import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;
import com.evampsaanga.b2b.ulduzum.codegenerate.GetCodegenerateData;

public class UlduzumCodeGenerateResponse extends BaseResponse
{
	private GetCodegenerateData data;

	public GetCodegenerateData getData() 
	{
		return data;
	}

	public void setData(GetCodegenerateData data) 
	{
		this.data = data;
	}
	
	
	
	

}
