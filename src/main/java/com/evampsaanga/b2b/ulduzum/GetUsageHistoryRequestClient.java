package com.evampsaanga.b2b.ulduzum;

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;

public class GetUsageHistoryRequestClient extends BaseRequest{
	
	
	
	//format For 3rdParty   datebeg=28-07-2014 , dateend=15-12-2014
	private String startDate="";
   private String endDate="";
public String getStartDate() {
	return startDate;
}
public void setStartDate(String startDate) {
	this.startDate = startDate;
}
public String getEndDate() {
	return endDate;
}
public void setEndDate(String endDate) {
	this.endDate = endDate;
}
	

}
