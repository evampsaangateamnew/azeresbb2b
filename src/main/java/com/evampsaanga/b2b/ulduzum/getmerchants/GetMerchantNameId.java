package com.evampsaanga.b2b.ulduzum.getmerchants;

public class GetMerchantNameId {
	
	private String id="";
	private String categoryName="";
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

}
