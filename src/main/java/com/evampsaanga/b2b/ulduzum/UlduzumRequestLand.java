package com.evampsaanga.b2b.ulduzum;

import java.io.IOException;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.amqimplementationsesb.LogsType;
import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.ThirdPartyNames;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Decrypter;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.evampsaanga.b2b.developer.utils.SOAPLoggingHandler;
import com.evampsaanga.b2b.ulduzum.codegenerate.Codegenerate;
import com.evampsaanga.b2b.ulduzum.codegenerate.GetCodegenerateData;
import com.evampsaanga.b2b.ulduzum.codegenerate.UlduzumCodeGenerateResponse;
import com.evampsaanga.b2b.ulduzum.getcategories.GetCategoriesUlduzumResponse;
import com.evampsaanga.b2b.ulduzum.getmerchants.Datum;
import com.evampsaanga.b2b.ulduzum.getmerchants.GetMerchantUlduzumResponse;
import com.evampsaanga.b2b.ulduzum.getunusedcodes.GetUnusedCodesUlduzumResponse;
import com.evampsaanga.b2b.ulduzum.getusagehistory.GetUsageHistoryUlduzumResponse;
import com.evampsaanga.b2b.ulduzum.getusagetotals.GetUsageTotalsUlduzumResponse;
import com.evampsaanga.b2b.ulduzum.getusagetotals.TestGetUsageWithSerielazation;
import com.saanga.magento.apiclient.RestClient;

@Path("/bakcell")
public class UlduzumRequestLand {

	public static final Logger logger = Logger.getLogger("azerfon-esb");
	public static final Logger loggerV2 = Logger.getLogger("azerfon-esb");

	@POST
	@Path("/getcategoires")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetCategoriesResponseclient GetCategories(@Body String requestBody, @Header("credentials") String credential)
			throws JSONException {
		logger.info("Request Landed on UlduzumRequestLand GetCategories: " + requestBody);
		logger.info("credential: getcategoires" + credential);
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.ULDUZUM_GET_CATEGORIES_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.ULDUZUM);
		logs.setTableType(LogsType.UlduzumGetCategories);
		UlduzumRequestClient cclient = null;
		GetCategoriesResponseclient resp = new GetCategoriesResponseclient();
		try {
			cclient = Helper.JsonToObject(requestBody, UlduzumRequestClient.class);
			if (cclient != null) {
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				logs.setLang(cclient.getLang());
			}
		} catch (Exception ex) {
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_401);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logger.info(Helper.GetException(ex));
			logs.updateLog(logs);
			return resp;
		}
		if (cclient != null) {
			String credentials = null;
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
			} catch (Exception ex) {
				SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				logger.info(Helper.GetException(ex));
				return resp;
			}
			if (credentials == null) {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
				String verification = Helper.validateRequest(cclient);
				if (!verification.equals("")) {
					resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
					resp.setReturnMsg(verification);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				try {

					// getConfigurationFromCache baseUrl, suburl, secretKey,
					// LanguageMapping

					String secretKey = ConfigurationManager.getConfigurationFromCache("ulduzum.subscriber.secretkey");
					String suburl = ConfigurationManager.getConfigurationFromCache("ulduzum.subscriber.baseurl")
							+ ConfigurationManager.getConfigurationFromCache("ulduzum.subscriber.getcategories")
							+ "secKey=" + secretKey + "&lang=" + ConfigurationManager.getConfigurationFromCache(
									ConfigurationManager.ULDUZUM_LANGUAGE_MAPPING + cclient.getLang());

					String response = RestClient.SendCallToUlduzum(suburl);

					try {
						GetCategoriesUlduzumResponse data = Helper.JsonToObject(response,
								GetCategoriesUlduzumResponse.class);
						if (data.getErrorcode().equalsIgnoreCase("0")) {
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);

							resp.setData(data.getData());
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							logger.info("ESB Response: " + resp);
							return resp;
						} else {
							resp.setReturnCode(data.getErrorcode());
							resp.setReturnMsg(data.getErrorstr());

							logs.setResponseCode(data.getErrorcode());
							logs.setResponseDescription(data.getErrorcode());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} catch (Exception ex) {
						resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
						resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						logger.info(Helper.GetException(ex));
						return resp;
					}
				} catch (Exception ex) {
					resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
					resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					logger.info(Helper.GetException(ex));
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		}
		logs.updateLog(logs);
		return resp;
	}

	@POST
	@Path("/getmerchants")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetMerchantsResponseclient GetMerchants(@Body String requestBody, @Header("credentials") String credential)
			throws JSONException {
		logger.info("Request Landed on UlduzumRequestLand GetMerchants: " + requestBody);
		logger.info("credential: getcategoires" + credential);
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.ULDUZUM_GET_MERCHANTS_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.ULDUZUM);
		logs.setTableType(LogsType.UlduzumGetMerchants);
		UlduzumRequestClient cclient = null;
		GetMerchantsResponseclient resp = new GetMerchantsResponseclient();
		try {
			cclient = Helper.JsonToObject(requestBody, UlduzumRequestClient.class);
			if (cclient != null) {
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				logs.setLang(cclient.getLang());
			}
		} catch (Exception ex) {
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_401);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			logger.info(Helper.GetException(ex));
			return resp;
		}
		if (cclient != null) {
			String credentials = null;
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
			} catch (Exception ex) {
				SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				logger.info(Helper.GetException(ex));
				return resp;
			}
			if (credentials == null) {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);

				return resp;
			}
			if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
				String verification = Helper.validateRequest(cclient);
				if (!verification.equals("")) {
					resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
					resp.setReturnMsg(verification);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				try {

					// getConfigurationFromCache baseUrl, suburl, secretKey,
					// LanguageMapping

					String secretKey = ConfigurationManager.getConfigurationFromCache("ulduzum.subscriber.secretkey");
					String suburl = ConfigurationManager.getConfigurationFromCache("ulduzum.subscriber.baseurl")
							+ ConfigurationManager.getConfigurationFromCache("ulduzum.subscriber.getmerchants")
							+ "secKey=" + secretKey + "&lang=" + ConfigurationManager.getConfigurationFromCache(
									ConfigurationManager.ULDUZUM_LANGUAGE_MAPPING + cclient.getLang());

					String response = RestClient.SendCallToUlduzum(suburl);

					
					try {
						GetMerchantUlduzumResponse data = Helper.JsonToObject(response,
								GetMerchantUlduzumResponse.class);
						if (data.getErrorcode().equalsIgnoreCase("0")) {
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
                      
						logger.info("SIZE Origignal BEFORE :"+data.getData().size());
						int count=0;
						ArrayList<Datum> dataMerchantsUpdated=new ArrayList<Datum>();
							for (int i = 0; i < data.getData().size(); i++) {

								if (data.getData().get(i).getCoorLat() == null
										|| data.getData().get(i).getCoorLat().isEmpty()
										|| data.getData().get(i).getCoorLat().equals("")
										|| data.getData().get(i).getCoorLng() == null
										|| data.getData().get(i).getCoorLng().isEmpty()
										|| data.getData().get(i).getCoorLng().equals("")
										) {
									
									//data.getData().remove(data.getData().get(i));
									
									count++;
								
								} 
								else
								{
									dataMerchantsUpdated.add(data.getData().get(i));
								}
								
								/*else
								{
									if (cclient.getLoyaltySegment().equalsIgnoreCase("Ulduzum")) {

										data.getData().get(i)
												.setLoyality_segment(data.getData().get(i).getSegmentBDiscount());
									} else if (cclient.getLoyaltySegment().equalsIgnoreCase("Ulduzum Premium")) {
										data.getData().get(i)
												.setLoyality_segment(data.getData().get(i).getSegmentADiscount());

									} else if (cclient.getLoyaltySegment().equalsIgnoreCase("Ulduzum Caspian")) {
										data.getData().get(i)
												.setLoyality_segment(data.getData().get(i).getSegmentCDiscount());
									}
								}*/
							}
							logger.info("NEw Size SIZE :"+dataMerchantsUpdated.size());
							logger.info("COUNT SIZE :"+count);
							for(int i = 0; i < dataMerchantsUpdated.size(); i++){
									if (cclient.getLoyaltySegment().equalsIgnoreCase("Ulduzum")) {

										dataMerchantsUpdated.get(i)
												.setLoyality_segment(data.getData().get(i).getSegmentBDiscount());
									} else if (cclient.getLoyaltySegment().equalsIgnoreCase("Ulduzum Premium")) {
										dataMerchantsUpdated.get(i)
												.setLoyality_segment(data.getData().get(i).getSegmentADiscount());

									} else if (cclient.getLoyaltySegment().equalsIgnoreCase("Ulduzum Caspian")) {
										dataMerchantsUpdated.get(i)
												.setLoyality_segment(data.getData().get(i).getSegmentCDiscount());
									}

								}

							
							logger.info("AFTER SIZE :"+dataMerchantsUpdated.size());

							String secretKey1 = ConfigurationManager
									.getConfigurationFromCache("ulduzum.subscriber.secretkey");
							String suburl1 = ConfigurationManager
									.getConfigurationFromCache("ulduzum.subscriber.baseurl")
									+ ConfigurationManager.getConfigurationFromCache("ulduzum.subscriber.getcategories")
									+ "secKey=" + secretKey1 + "&lang="
									+ ConfigurationManager.getConfigurationFromCache(
											ConfigurationManager.ULDUZUM_LANGUAGE_MAPPING + cclient.getLang());

							String responsegetcat = RestClient.SendCallToUlduzum(suburl1);

							com.evampsaanga.b2b.ulduzum.getcategories.GetCategoriesUlduzumResponse datacategory = Helper
									.JsonToObject(responsegetcat,
											com.evampsaanga.b2b.ulduzum.getcategories.GetCategoriesUlduzumResponse.class);

							resp.setData(dataMerchantsUpdated);
							resp.setCategoryNamelist(datacategory.getData());
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						} else {
							resp.setReturnCode(data.getErrorcode());
							resp.setReturnMsg(data.getErrorstr());

							logs.setResponseCode(data.getErrorcode());
							logs.setResponseDescription(data.getErrorcode());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} catch (Exception ex) {
						resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
						resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						logger.info(Helper.GetException(ex));
						return resp;
					}
				} catch (Exception ex) {
					resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
					resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					logger.info(Helper.GetException(ex));
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		}
		logs.updateLog(logs);
		return resp;
	}

	@POST
	@Path("/getusagetotals")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetUsageTotalResponseclient GetUsageTotals(@Body String requestBody,
			@Header("credentials") String credential) throws JSONException {
		logger.info("Request Landed on UlduzumRequestLand GetUsageTotals: " + requestBody);
		logger.info("credential: getcategoires" + credential);
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.ULDUZUM_GET_USAGE_TOTAL_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.ULDUZUM);
		logs.setTableType(LogsType.UlduzumGetUsageTotals);
		UlduzumRequestClient cclient = null;
		GetUsageTotalResponseclient resp = new GetUsageTotalResponseclient();
		try {
			cclient = Helper.JsonToObject(requestBody, UlduzumRequestClient.class);
			if (cclient != null) {
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				logs.setLang(cclient.getLang());
			}
		} catch (Exception ex) {
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_401);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			logger.info(Helper.GetException(ex));
			return resp;
		}
		if (cclient != null) {
			String credentials = null;
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
			} catch (Exception ex) {
				SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				logger.info(Helper.GetException(ex));
				return resp;
			}
			if (credentials == null) {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
				String verification = Helper.validateRequest(cclient);
				if (!verification.equals("")) {
					resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
					resp.setReturnMsg(verification);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				try {

					// getConfigurationFromCache baseUrl, suburl, secretKey,
					// LanguageMapping

					String secretKey = ConfigurationManager.getConfigurationFromCache("ulduzum.subscriber.secretkey");
					String suburl = ConfigurationManager.getConfigurationFromCache("ulduzum.subscriber.baseurl")
							+ ConfigurationManager.getConfigurationFromCache("ulduzum.subscriber.getusagetotals")
							+ "secKey=" + secretKey + "&lang="
							+ ConfigurationManager.getConfigurationFromCache(
									ConfigurationManager.ULDUZUM_LANGUAGE_MAPPING + cclient.getLang())
							+ "&msisdn=" + ConfigurationManager.MSISDN_CODE_START + cclient.getmsisdn();

					String response = RestClient.SendCallToUlduzum(suburl);

					/*
					 * JSONObject responseobject=new JSONObject(response);
					 * 
					 * if(responseobject.get("data") instanceof JSONArray) {
					 * logger.info("-----Yes ITS Array-----"); } else {
					 * logger.info("-----ITS OBJECT-----"); JSONArray
					 * jsonArray=new JSONArray();
					 * jsonArray.put(responseobject.get("data"));
					 * logger.info("-----Converted Object to JSON: "+jsonArray);
					 * 
					 * 
					 * }
					 */

					try {
						GetUsageTotalsUlduzumResponse data = Helper.JsonToObject(response,
								GetUsageTotalsUlduzumResponse.class);
						if (data.getErrorcode().equalsIgnoreCase("0")) {
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
//							data.getData().setDaily_limit_total("10");
							data.getData().setDaily_limit_total(ConfigurationManager.getConfigurationFromCache("ulduzum.subscriber.getusage.totallimit"));
							resp.setData(data.getData());
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						} else {
							resp.setReturnCode(data.getErrorcode());
							resp.setReturnMsg(data.getErrorstr());

							logs.setResponseCode(data.getErrorcode());
							logs.setResponseDescription(data.getErrorcode());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} catch (Exception ex) {
						resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
						logger.info(Helper.GetException(ex));
						ex.printStackTrace();
						resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						logger.info(Helper.GetException(ex));
						return resp;
					}
				} catch (Exception ex) {
					resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
					resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					logger.info(Helper.GetException(ex));
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		}
		logs.updateLog(logs);
		return resp;
	}

	@POST
	@Path("/getusagehistory")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetUsageHistoryResponseclient GetUsageHistory(@Body String requestBody,
			@Header("credentials") String credential) throws JSONException {
		logger.info("Request Landed on UlduzumRequestLand GetUsageHistory: " + requestBody);
		logger.info("credential: getcategoires" + credential);
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.ULDUZUM_GET_USAGE_HISTORY_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.ULDUZUM);
		logs.setTableType(LogsType.UlduzumGetUsageHistory);
		GetUsageHistoryRequestClient cclient = null;
		GetUsageHistoryResponseclient resp = new GetUsageHistoryResponseclient();

		try {
			cclient = Helper.JsonToObject(requestBody, GetUsageHistoryRequestClient.class);
			if (cclient != null) {
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				logs.setLang(cclient.getLang());
			}
		} catch (Exception ex) {
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_401);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			logger.info(Helper.GetException(ex));
			return resp;
		}
		if (cclient != null) {
			String credentials = null;
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
			} catch (Exception ex) {
				SOAPLoggingHandler.logger.error(Helper.GetException(ex));
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				logger.info(Helper.GetException(ex));
				return resp;
			}
			if (credentials == null) {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
				String verification = Helper.validateRequest(cclient);
				if (!verification.equals("")) {
					resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
					resp.setReturnMsg(verification);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}

			if (!checkDateFormat(cclient.getStartDate())) {
				resp.setReturnCode(ResponseCodes.START_DATE_INVALID_CODE);
				resp.setReturnMsg(ResponseCodes.START_DATE_INVALID_DES);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (!checkDateFormat(cclient.getEndDate())) {
				resp.setReturnCode(ResponseCodes.END_DATE_INVALID_CODE);
				resp.setReturnMsg(ResponseCodes.END_DATE_INVALID_DES);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}

			try {
				Date datestart = new SimpleDateFormat("yyyy-MM-dd").parse(cclient.getStartDate());
				Date dateend = new SimpleDateFormat("yyyy-MM-dd").parse(cclient.getEndDate());
				Date today = new Date();
				if (datestart.compareTo(dateend) > 0) {

					resp.setReturnCode(ResponseCodes.MISSING_PARAMETER_CODE);
					resp.setReturnMsg(ResponseCodes.MISSING_PARAMETER_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				} else if (today.compareTo(datestart) < 0) {

					resp.setReturnCode(ResponseCodes.START_DATE_CANNOT_BE_IN_FUTURE_CODE);
					resp.setReturnMsg(ResponseCodes.START_DATE_CANNOT_BE_IN_FUTURE_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				} else if (today.compareTo(dateend) < 0) {

					resp.setReturnCode(ResponseCodes.END_DATE_CANNOT_BE_IN_FUTURE_CODE);
					resp.setReturnMsg(ResponseCodes.END_DATE_CANNOT_BE_IN_FUTURE_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} catch (Exception e) {
				// TODO: handle exception
				logger.error("Date Comparison Exceptiom" + e.getMessage());
			}

			if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				try {

					// getConfigurationFromCache baseUrl, suburl, secretKey,
					// LanguageMapping

					SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd");
					Date startdate = parser.parse(cclient.getStartDate());
					SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
					String formattedDate = formatter.format(startdate);
					cclient.setStartDate(formattedDate);

					Date enddat = parser.parse(cclient.getEndDate());
					cclient.setEndDate(formatter.format(enddat));

					logger.info("StartDAteTO passed : " + cclient.getStartDate());
					logger.info("End DAteTO passed : " + cclient.getStartDate());

					String secretKey = ConfigurationManager.getConfigurationFromCache("ulduzum.subscriber.secretkey");
					String suburl = ConfigurationManager.getConfigurationFromCache("ulduzum.subscriber.baseurl")
							+ ConfigurationManager.getConfigurationFromCache("ulduzum.subscriber.getusagehistory")
							+ "secKey=" + secretKey + "&lang="
							+ ConfigurationManager.getConfigurationFromCache(
									ConfigurationManager.ULDUZUM_LANGUAGE_MAPPING + cclient.getLang())
							+ "&msisdn=" + ConfigurationManager.MSISDN_CODE_START + cclient.getmsisdn() + "&datebeg="
							+ cclient.getStartDate() + "&dateend=" + cclient.getEndDate();

					String response = RestClient.SendCallToUlduzum(suburl);

					try {
						GetUsageHistoryUlduzumResponse data = Helper.JsonToObject(response,
								GetUsageHistoryUlduzumResponse.class);
						if (data.getErrorcode().equalsIgnoreCase("0")) {
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);

							resp.setData(data.getData());
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						} else {
							resp.setReturnCode(data.getErrorcode());
							resp.setReturnMsg(data.getErrorstr());

							logs.setResponseCode(data.getErrorcode());
							logs.setResponseDescription(data.getErrorcode());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}
					} catch (Exception ex) {
						resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
						resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);
						logger.info(Helper.GetException(ex));
						return resp;
					}
				} catch (Exception ex) {
					resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
					resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					logger.info(Helper.GetException(ex));
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		}
		logs.updateLog(logs);
		return resp;
	}

	@POST
	@Path("/getunusedcodes")
	@Consumes(MediaType.APPLICATION_JSON)
	public GetUnusedCodesResponseclient GetUnusedCodes(@Body String requestBody,
			@Header("credentials") String credential) throws JSONException {
		logger.info("Request Landed on UlduzumRequestLand GetUnusedCodes: " + requestBody);
		logger.info("credential: getcategoires" + credential);
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.ULDUZUM_GET_UNUSED_CODES_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.ULDUZUM);
		logs.setTableType(LogsType.UlduzumGetUnusedCodes);
		UlduzumRequestClient cclient = null;
		GetUnusedCodesResponseclient resp = new GetUnusedCodesResponseclient();
		try {
			cclient = Helper.JsonToObject(requestBody, UlduzumRequestClient.class);
			if (cclient != null) {
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				logs.setLang(cclient.getLang());
			}
		} catch (Exception ex) {
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_401);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			logger.info(Helper.GetException(ex));
			return resp;
		}
		if (cclient != null) {
			String credentials = null;
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
			} catch (Exception ex) {
				logger.info(Helper.GetException(ex));
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials == null) {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
				String verification = Helper.validateRequest(cclient);
				if (!verification.equals("")) {
					resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
					resp.setReturnMsg(verification);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				try {

					// getConfigurationFromCache baseUrl, suburl, secretKey,
					// LanguageMapping

					String secretKey = ConfigurationManager.getConfigurationFromCache("ulduzum.subscriber.secretkey");
					String suburl = ConfigurationManager.getConfigurationFromCache("ulduzum.subscriber.baseurl")
							+ ConfigurationManager.getConfigurationFromCache("ulduzum.subscriber.getunusedcodes")
							+ "secKey=" + secretKey + "&lang="
							+ ConfigurationManager.getConfigurationFromCache(
									ConfigurationManager.ULDUZUM_LANGUAGE_MAPPING + cclient.getLang())
							+ "&msisdn=" + ConfigurationManager.MSISDN_CODE_START + cclient.getmsisdn();

					String response = RestClient.SendCallToUlduzum(suburl);

					try {
						GetUnusedCodesUlduzumResponse data = Helper.JsonToObject(response,
								GetUnusedCodesUlduzumResponse.class);
						if (data.getErrorcode().equalsIgnoreCase("0")) {
							resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
							resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);

							resp.setData(data.getData());
							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						} else {

							resp.setReturnCode(data.getErrorcode());
							resp.setReturnMsg(data.getErrorstr());

							logs.setResponseCode(resp.getReturnCode());
							logs.setResponseDescription(resp.getReturnMsg());
							logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
							logs.updateLog(logs);
							return resp;
						}

					} catch (Exception ex) {
						resp.setReturnCode(ResponseCodes.INTERNAL_SERVER_ERROR_CODE);
						resp.setReturnMsg(ResponseCodes.INTERNAL_SERVER_ERROR_DES);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logger.info(Helper.GetException(ex));
						logs.updateLog(logs);
						return resp;
					}
				} catch (Exception ex) {
					resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
					resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					logger.info(Helper.GetException(ex));
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		}
		logs.updateLog(logs);
		return resp;

	}

	@POST
	@Path("/codegenerate")
	@Consumes(MediaType.APPLICATION_JSON)
	public UlduzumCodeGenerateResponse codeGenerate(@Body String requestBody, @Header("credentials") String credential)
			throws JSONException {
		logger.info("Request Landed on UlduzumRequestLand GetUnusedCodes: " + requestBody);
		logger.info("credential: getcategoires" + credential);
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.ULDUZUM_GET_UNUSED_CODES_TRANSACTION_NAME);
		logs.setThirdPartyName(ThirdPartyNames.ULDUZUM);
		logs.setTableType(LogsType.UlduzumGetUnusedCodes);
		UlduzumRequestClient cclient = null;
		UlduzumCodeGenerateResponse resp = new UlduzumCodeGenerateResponse();
		try {
			cclient = Helper.JsonToObject(requestBody, UlduzumRequestClient.class);
			if (cclient != null) {
				logs.setIp(cclient.getiP());
				logs.setChannel(cclient.getChannel());
				logs.setMsisdn(cclient.getmsisdn());
				logs.setLang(cclient.getLang());
			}
		} catch (Exception ex) {
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_401);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			logger.info(Helper.GetException(ex));
			return resp;
		}
		if (cclient != null) {
			String credentials = null;
			try {
				credentials = Decrypter.getInstance().decrypt(credential);
			} catch (Exception ex) {
				logger.info(Helper.GetException(ex));
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials == null) {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
				String verification = Helper.validateRequest(cclient);
				if (!verification.equals("")) {
					resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
					resp.setReturnMsg(verification);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
			if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {
				try {

					// getConfigurationFromCache baseUrl, suburl, secretKey,
					// LanguageMapping

					String secretKey = ConfigurationManager
							.getConfigurationFromCache("ulduzum.subscriber.codegenerate.secretkey");
					String suburl = ConfigurationManager
							.getConfigurationFromCache("ulduzum.subscriber.codegenerate.url");
					String postXML = getPostXMLPacket(ConfigurationManager.MSISDN_CODE_START + cclient.getmsisdn());
					String hashCode = getHashCode(secretKey, postXML);
					String response = RestClient.SendCallEMobile(hashCode, postXML);

					logger.info("Response: " + response);

					logger.info("response Getcode Ulduzum :" + response);

					JAXBContext jaxbContext = JAXBContext.newInstance(Codegenerate.class);
					Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

					StringReader reader = new StringReader(response.trim());
					Codegenerate marshalledResponse = (Codegenerate) unmarshaller.unmarshal(reader);
					if (marshalledResponse.getCode() != null) // if code is
																// recieved in
																// case of
																// success
																// response
					{

						GetCodegenerateData getCodegenerateData = new GetCodegenerateData();
						getCodegenerateData.setCode(marshalledResponse.getCode());
						resp.setData(getCodegenerateData);
						resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
						resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);

					} else {

						GetCodegenerateData getCodegenerateData = new GetCodegenerateData();
						getCodegenerateData.setCode(ResponseCodes.CODE_GENERATE_ERROR_CODE);
						resp.setData(getCodegenerateData);

						resp.setReturnCode(ResponseCodes.CODE_GENERATE_ERROR_CODE);
						resp.setReturnMsg(ConfigurationManager.getConfigurationFromCache(
								"ulduzum.subscriber.codegenerate." + marshalledResponse.getErrormess()));
						logs.setResponseCode(resp.getReturnCode());
						logs.setResponseDescription(resp.getReturnMsg());
						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
						logs.updateLog(logs);

					}

					// release values for GC to clear the memory
					jaxbContext = null;
					unmarshaller = null;
					reader = null;

				} catch (Exception ex) {
					resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
					resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
					logs.setResponseCode(resp.getReturnCode());
					logs.setResponseDescription(resp.getReturnMsg());
					logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
					logs.updateLog(logs);
					logger.info(Helper.GetException(ex));
					return resp;
				}
			} else {
				resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
				resp.setReturnMsg(ResponseCodes.ERROR_401);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		}
		logs.updateLog(logs);
		return resp;

	}

	private String getPostXMLPacket(String msisdn) {

		String postXml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> <codegenerate> <msisdn>" + msisdn
				+ "</msisdn> </codegenerate>";
		return postXml;
	}

	private String getHashCode(String secretKey, String postXML) throws NoSuchAlgorithmException {
		String password = postXML + secretKey;

		MessageDigest md = MessageDigest.getInstance("MD5");
		byte[] hashInBytes = md.digest(password.getBytes(StandardCharsets.UTF_8));

		StringBuilder sb = new StringBuilder();
		for (byte b : hashInBytes) 
		{
			sb.append(String.format("%02x", b));
		}
		return sb.toString();
	}

	public static boolean checkDateFormat(String dateinput) {
		Date date = null;
		boolean checkformat;
		try {
			// SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			dateFormat.setLenient(false);
			date = dateFormat.parse(dateinput);
			checkformat = true;
		} catch (ParseException ex) {
			ex.printStackTrace();
			System.out.println("NO PARSED");
			checkformat = false;
		}
		return checkformat;
	}

	@SuppressWarnings("null")
	public static void main(String[] args) {

		String merchantsresponse = "{\"returnMsg\": \"Sucessfull\", \"returnCode\": \"200\", \"data\": [ { \"id\": \"10360\", \"parent_id\": \"0\", \"name\": \"123 Game Lounge\", \"category_id\": \"41\", \"category_name\": \"Caf\u00E9 and restaurants\", \"short_code\": \"\", \"contact_person\": \"\"}, { \"id\": \"10324\", \"parent_id\": \"0\", \"name\": \"156 Evakuasiya\", \"category_id\": \"21\", \"category_name\": \"Auto\", \"short_code\": \"\", \"contact_person\": \"\"}, { \"id\": \"10324\", \"parent_id\": \"0\", \"name\": \"156 Evakuasiya\", \"category_id\": \"21\", \"category_name\": \"saboor\", \"short_code\": \"\", \"contact_person\": \"\"}, { \"id\": \"10324\", \"parent_id\": \"0\", \"name\": \"156 Evakuasiya\", \"category_id\": \"21\", \"category_name\": \"saboor\", \"short_code\": \"\", \"contact_person\": \"\" } ] }";

		try {
			GetMerchantsResponseclient response = Helper.JsonToObject(merchantsresponse,
					GetMerchantsResponseclient.class);
			System.out.println("CAT NAME :" + response.getData().get(0).getCategoryName());

			ArrayList<String> categoryNamelist = new ArrayList<String>();
			for (com.evampsaanga.b2b.ulduzum.getmerchants.Datum categores : response.getData()) {
				categoryNamelist.add(categores.getCategoryName());

			}
			Set<String> set = new HashSet<>(categoryNamelist);
			categoryNamelist.clear();
			categoryNamelist.addAll(set);
			System.out.println("UNIQUE LIST :" + categoryNamelist);

		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// START : this code check wehter the coming json is object or arra, if
		// array then it converts into objject before mapping to the pojo
		String jsonStrResponse = "{\"errorcode\":\"0\",\"errorstr\":\"\",\"data\":{\"segment_type\":\"Ulduzum Premium\",\"daily_limit_left\":\"9\",\"last_unused_code\":\"\",\"total_redemptions\":\"62\",\"total_amount\":\"863.7\",\"total_discounted\":\"565.5\",\"total_discount\":\"298.2\"}}";
		try {

			JSONObject responseobject = new JSONObject(jsonStrResponse);
			System.out.println("JsonObject: " + responseobject);
			if (responseobject.get("data") instanceof JSONArray) {
				System.out.println("YES ARRAY");

				System.out.println("JsonArray : " + responseobject);
			} else {
				System.out.println("-------------ITS OBJECT--------------");
				JSONArray jsonrr = new JSONArray();
				jsonrr.put(responseobject.get("data"));
				responseobject.remove("data");
				responseobject.put("data", jsonrr);
				System.out.println("NEW: " + responseobject);

				TestGetUsageWithSerielazation data = Helper.JsonToObject(responseobject.toString(),
						TestGetUsageWithSerielazation.class);

				for (com.evampsaanga.b2b.ulduzum.getusagetotals.Data infordata : data.getData()) {
					System.out.println(infordata.getDailyLimitLeft());
				}

				System.out.println("UpdatedJson: " + Helper.ObjectToJson(data));

			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
		// END : this code check wehter the coming json is object or arra, if
		// array then it converts into objject before mapping to the pojo

	}

}
