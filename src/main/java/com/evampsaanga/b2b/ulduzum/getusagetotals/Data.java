
package com.evampsaanga.b2b.ulduzum.getusagetotals;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "segment_type",
    "daily_limit_left",
    "last_unused_code",
    "total_redemptions",
    "total_amount",
    "total_discounted",
    "total_discount"
})
public class Data {

    @JsonProperty("segment_type")
    private String segmentType;
    @JsonProperty("daily_limit_left")
    private String dailyLimitLeft;
    @JsonProperty("last_unused_code")
    private String lastUnusedCode;
    @JsonProperty("total_redemptions")
    private String totalRedemptions;
    @JsonProperty("total_amount")
    private String totalAmount;
    @JsonProperty("total_discounted")
    private String totalDiscounted;
    @JsonProperty("total_discount")
    private String totalDiscount;
    
    @JsonInclude(Include.NON_NULL)
    private String daily_limit_total;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("segment_type")
    public String getSegmentType() {
        return segmentType;
    }

    @JsonProperty("segment_type")
    public void setSegmentType(String segmentType) {
        this.segmentType = segmentType;
    }

    @JsonProperty("daily_limit_left")
    public String getDailyLimitLeft() {
        return dailyLimitLeft;
    }

    @JsonProperty("daily_limit_left")
    public void setDailyLimitLeft(String dailyLimitLeft) {
        this.dailyLimitLeft = dailyLimitLeft;
    }

    @JsonProperty("last_unused_code")
    public String getLastUnusedCode() {
        return lastUnusedCode;
    }

    @JsonProperty("last_unused_code")
    public void setLastUnusedCode(String lastUnusedCode) {
        this.lastUnusedCode = lastUnusedCode;
    }

    @JsonProperty("total_redemptions")
    public String getTotalRedemptions() {
        return totalRedemptions;
    }

    @JsonProperty("total_redemptions")
    public void setTotalRedemptions(String totalRedemptions) {
        this.totalRedemptions = totalRedemptions;
    }

    @JsonProperty("total_amount")
    public String getTotalAmount() {
        return totalAmount;
    }

    @JsonProperty("total_amount")
    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    @JsonProperty("total_discounted")
    public String getTotalDiscounted() {
        return totalDiscounted;
    }

    @JsonProperty("total_discounted")
    public void setTotalDiscounted(String totalDiscounted) {
        this.totalDiscounted = totalDiscounted;
    }

    @JsonProperty("total_discount")
    public String getTotalDiscount() {
        return totalDiscount;
    }

    @JsonProperty("total_discount")
    public void setTotalDiscount(String totalDiscount) {
        this.totalDiscount = totalDiscount;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

	public String getDaily_limit_total() {
		return daily_limit_total;
	}

	public void setDaily_limit_total(String daily_limit_total) {
		this.daily_limit_total = daily_limit_total;
	}

}
