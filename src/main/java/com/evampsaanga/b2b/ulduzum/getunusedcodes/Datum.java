
package com.evampsaanga.b2b.ulduzum.getunusedcodes;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "identical_code",
    "insert_date"
})
public class Datum {

    @JsonProperty("identical_code")
    private String identicalCode;
    @JsonProperty("insert_date")
    private String insertDate;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("identical_code")
    public String getIdenticalCode() {
        return identicalCode;
    }

    @JsonProperty("identical_code")
    public void setIdenticalCode(String identicalCode) {
        this.identicalCode = identicalCode;
    }

    @JsonProperty("insert_date")
    public String getInsertDate() {
        return insertDate;
    }

    @JsonProperty("insert_date")
    public void setInsertDate(String insertDate) {
        this.insertDate = insertDate;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
