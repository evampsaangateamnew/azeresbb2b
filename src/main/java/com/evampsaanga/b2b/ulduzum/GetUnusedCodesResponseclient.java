package com.evampsaanga.b2b.ulduzum;

import java.util.ArrayList;
import java.util.List;

import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;
import com.evampsaanga.b2b.ulduzum.getunusedcodes.Datum;

public class GetUnusedCodesResponseclient extends BaseResponse {
	
	List<Datum> data=new ArrayList<>();

	public List<Datum> getData() {
		return data;
	}

	public void setData(List<Datum> data) {
		this.data = data;
	}

}
