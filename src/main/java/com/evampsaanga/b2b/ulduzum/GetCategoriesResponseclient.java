package com.evampsaanga.b2b.ulduzum;

import java.util.ArrayList;
import java.util.List;

import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;


public class GetCategoriesResponseclient extends BaseResponse {

	List<com.evampsaanga.b2b.ulduzum.getcategories.Datum> data = new ArrayList<>();

	public List<com.evampsaanga.b2b.ulduzum.getcategories.Datum> getData() {
		return data;
	}

	public void setData(List<com.evampsaanga.b2b.ulduzum.getcategories.Datum> data) {
		this.data = data;
	}

}
