
package com.evampsaanga.b2b.ulduzum.getusagehistory;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "merchant_id",
    "merchant_name",
    "category_id",
    "category_name",
    "identical_code",
    "redemption_date",
    "redemption_time",
    "amount",
    "discount_amount",
    "discounted_amount",
    "discount_percent",
    "payment_type"
})
public class Datum {

    @JsonProperty("id")
    private String id;
    @JsonProperty("merchant_id")
    private String merchantId;
    @JsonProperty("merchant_name")
    private String merchantName;
    @JsonProperty("category_id")
    private String categoryId;
    @JsonProperty("category_name")
    private String categoryName;
    @JsonProperty("identical_code")
    private String identicalCode;
    @JsonProperty("redemption_date")
    private String redemptionDate;
    @JsonProperty("redemption_time")
    private String redemptionTime;
    @JsonProperty("amount")
    private String amount;
    @JsonProperty("discount_amount")
    private String discountAmount;
    @JsonProperty("discounted_amount")
    private String discountedAmount;
    @JsonProperty("discount_percent")
    private String discountPercent;
    @JsonProperty("payment_type")
    private String paymentType;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("merchant_id")
    public String getMerchantId() {
        return merchantId;
    }

    @JsonProperty("merchant_id")
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    @JsonProperty("merchant_name")
    public String getMerchantName() {
        return merchantName;
    }

    @JsonProperty("merchant_name")
    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    @JsonProperty("category_id")
    public String getCategoryId() {
        return categoryId;
    }

    @JsonProperty("category_id")
    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    @JsonProperty("category_name")
    public String getCategoryName() {
        return categoryName;
    }

    @JsonProperty("category_name")
    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    @JsonProperty("identical_code")
    public String getIdenticalCode() {
        return identicalCode;
    }

    @JsonProperty("identical_code")
    public void setIdenticalCode(String identicalCode) {
        this.identicalCode = identicalCode;
    }

    @JsonProperty("redemption_date")
    public String getRedemptionDate() {
        return redemptionDate;
    }

    @JsonProperty("redemption_date")
    public void setRedemptionDate(String redemptionDate) {
        this.redemptionDate = redemptionDate;
    }

    @JsonProperty("redemption_time")
    public String getRedemptionTime() {
        return redemptionTime;
    }

    @JsonProperty("redemption_time")
    public void setRedemptionTime(String redemptionTime) {
        this.redemptionTime = redemptionTime;
    }

    @JsonProperty("amount")
    public String getAmount() {
        return amount;
    }

    @JsonProperty("amount")
    public void setAmount(String amount) {
        this.amount = amount;
    }

    @JsonProperty("discount_amount")
    public String getDiscountAmount() {
        return discountAmount;
    }

    @JsonProperty("discount_amount")
    public void setDiscountAmount(String discountAmount) {
        this.discountAmount = discountAmount;
    }

    @JsonProperty("discounted_amount")
    public String getDiscountedAmount() {
        return discountedAmount;
    }

    @JsonProperty("discounted_amount")
    public void setDiscountedAmount(String discountedAmount) {
        this.discountedAmount = discountedAmount;
    }

    @JsonProperty("discount_percent")
    public String getDiscountPercent() {
        return discountPercent;
    }

    @JsonProperty("discount_percent")
    public void setDiscountPercent(String discountPercent) {
        this.discountPercent = discountPercent;
    }

    @JsonProperty("payment_type")
    public String getPaymentType() {
        return paymentType;
    }

    @JsonProperty("payment_type")
    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
