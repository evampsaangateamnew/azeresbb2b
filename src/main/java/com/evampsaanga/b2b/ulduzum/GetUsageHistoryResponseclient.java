package com.evampsaanga.b2b.ulduzum;

import java.util.ArrayList;
import java.util.List;

import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;

public class GetUsageHistoryResponseclient extends BaseResponse {
	
	List<com.evampsaanga.b2b.ulduzum.getusagehistory.Datum> data=new ArrayList<>();

	public List<com.evampsaanga.b2b.ulduzum.getusagehistory.Datum> getData() {
		return data;
	}

	public void setData(List<com.evampsaanga.b2b.ulduzum.getusagehistory.Datum> data) {
		this.data = data;
	}

}
