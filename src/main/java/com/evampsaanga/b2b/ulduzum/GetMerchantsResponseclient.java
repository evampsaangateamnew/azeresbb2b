package com.evampsaanga.b2b.ulduzum;

import java.util.ArrayList;
import java.util.List;

import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;

public class GetMerchantsResponseclient extends BaseResponse {
	
	
	List<com.evampsaanga.b2b.ulduzum.getmerchants.Datum> data=new ArrayList<>();
	List<com.evampsaanga.b2b.ulduzum.getcategories.Datum> categoryNamelist=null;

	public List<com.evampsaanga.b2b.ulduzum.getmerchants.Datum> getData() {
		return data;
	}

	public void setData(List<com.evampsaanga.b2b.ulduzum.getmerchants.Datum> data) {
		this.data = data;
	}

	public List<com.evampsaanga.b2b.ulduzum.getcategories.Datum> getCategoryNamelist() {
		return categoryNamelist;
	}

	public void setCategoryNamelist(List<com.evampsaanga.b2b.ulduzum.getcategories.Datum> categoryNamelist) {
		this.categoryNamelist = categoryNamelist;
	}






}
