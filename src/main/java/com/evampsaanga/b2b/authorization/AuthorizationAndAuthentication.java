package com.evampsaanga.b2b.authorization;

import org.apache.log4j.Logger;

import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.developer.utils.Decrypter;
import com.evampsaanga.b2b.developer.utils.Helper;

public class AuthorizationAndAuthentication {

	public static final Logger logger = Logger.getLogger("azerfon-esb");
	
	public static boolean authenticateAndAuthorizeCredentials(String credentials)
	{
		try {
			credentials = Decrypter.getInstance().decrypt(credentials);
			if (credentials == null || !credentials.equals(Constants.CREDENTIALS)) 
				return false;
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		return true;
	}
	
}
