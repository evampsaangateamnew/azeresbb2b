package com.evampsaanga.b2b.validator.rules;

public class ValidationResult {

	private boolean validationResult;
	private String validationCode;
	private String validationMessage;
	
	public ValidationResult(){
	}
	
	public ValidationResult(boolean validationResult, String validationCode){
		this.validationResult = validationResult;
		this.validationCode = validationCode;
		//TODO (logic needs to be revised with configuration module)
		this.validationMessage = this.validationCode;
	}
	
	public boolean isValidationResult() {
		return validationResult;
	}
	public void setValidationResult(boolean validationResult) {
		this.validationResult = validationResult;
	}
	public String getValidationCode() {
		return validationCode;
	}
	public void setValidationCode(String validationCode) {
		this.validationCode = validationCode;
	}
	public String getValidationMessage() {
		return validationMessage;
	}
	public void setValidationMessage(String validationMessage) {
		this.validationMessage = validationMessage;
	}
}
