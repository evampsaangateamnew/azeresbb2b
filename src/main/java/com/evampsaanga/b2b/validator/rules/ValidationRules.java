package com.evampsaanga.b2b.validator.rules;

public interface ValidationRules {

	public ValidationResult validateObject(Object object);
	
}
