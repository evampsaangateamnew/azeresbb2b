package com.evampsaanga.b2b.validator.rules;

public class ValidationCode {
	public final static String SUCCESS_VALIDATION_CODE = "0000";
	public final static String MSISDN_EMPTY_VALIDATION_CODE = "0001";
	public final static String CHANNEL_EMPTY_VALIDATION_CODE = "0002";
	public final static String IP_EMPTY_VALIDATION_CODE = "0003";
	public final static String LANG_EMPTY_VALIDATION_CODE = "0004";
}
