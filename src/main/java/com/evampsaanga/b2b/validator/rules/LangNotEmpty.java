package com.evampsaanga.b2b.validator.rules;

public class LangNotEmpty implements ValidationRules {

	@Override
	public ValidationResult validateObject(Object object) {
		if(object == null || object.toString().isEmpty() || object.toString().length() == 0)
			return new ValidationResult(false,ValidationCode.LANG_EMPTY_VALIDATION_CODE);
		return new ValidationResult(true,ValidationCode.SUCCESS_VALIDATION_CODE);	
	}

}
