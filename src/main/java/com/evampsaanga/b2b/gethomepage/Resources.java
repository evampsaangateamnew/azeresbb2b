package com.evampsaanga.b2b.gethomepage;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Resources {
	@JsonProperty("installments")
	private List<Installment> installments = new ArrayList<Installment>();

	@JsonProperty("installments")
	public List<Installment> getInstallments() {
		return installments;
	}

	@JsonProperty("installments")
	public void setInstallments(List<Installment> installments) {
		this.installments = installments;
	}
}
