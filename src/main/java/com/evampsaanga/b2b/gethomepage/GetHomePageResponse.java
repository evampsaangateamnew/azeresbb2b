package com.evampsaanga.b2b.gethomepage;

import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;
import com.fasterxml.jackson.annotation.JsonProperty;

public class GetHomePageResponse extends BaseResponse {
	@JsonProperty("data")
	private Data_ data = new Data_();

	/**
	 * @return the data
	 */
	public Data_ getData() {
		return data;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	public void setData(Data_ data) {
		this.data = data;
	}
}
