package com.evampsaanga.b2b.gethomepage;

import com.evampsaanga.b2b.developer.utils.Helper;
import com.huawei.bme.cbsinterface.arservices.QueryLoanLogRequest;
import com.huawei.bme.cbsinterface.arservices.QueryLoanLogRequestMsg;
import com.huawei.bme.cbsinterface.arservices.QueryLoanLogResultMsg;
import com.huawei.bme.cbsinterface.cbscommon.OperatorInfo;
import com.huawei.bme.cbsinterface.cbscommon.OwnershipInfo;
import com.huawei.bme.cbsinterface.cbscommon.SecurityInfo;
import com.huawei.cbs.ar.wsservice.arcommon.SubAccessCode;

public class QueryLoanLog {
	public QueryLoanLogResultMsg queryLoanLog(String msisdn) {
		QueryLoanLogRequestMsg msg = new QueryLoanLogRequestMsg();
		QueryLoanLogRequest qLLR = new QueryLoanLogRequest();
		SubAccessCode subAC = new SubAccessCode();
		subAC.setPrimaryIdentity(msisdn);
		qLLR.setSubAccessCode(subAC);
		qLLR.setBeginRowNum("0");
		qLLR.setFetchRowNum("100");
		qLLR.setTotalRowNum("300");
		msg.setRequestHeader(getLoanLogRequestHeader());
		msg.setQueryLoanLogRequest(qLLR);
		//return CBSARService.getInstance().queryLoanLog(msg);
		QueryLoanLogResultMsg resp=new QueryLoanLogResultMsg();
		return resp;
	}

	public com.huawei.bme.cbsinterface.cbscommon.RequestHeader getLoanLogRequestHeader() {
		com.huawei.bme.cbsinterface.cbscommon.RequestHeader loanRequestHeader = null;
		try {
			loanRequestHeader = new com.huawei.bme.cbsinterface.cbscommon.RequestHeader();
			loanRequestHeader.setVersion("1");
			loanRequestHeader.setBusinessCode("1");
			loanRequestHeader.setMessageSeq(Helper.generateTransactionID());
			OwnershipInfo ownershipinf = new OwnershipInfo();
			ownershipinf.setBEID("101");
			ownershipinf.setBRID("101");
			loanRequestHeader.setOwnershipInfo(ownershipinf);
			SecurityInfo value = new SecurityInfo();
			value.setLoginSystemCode("NGCBS");
			value.setPassword("123456");
			loanRequestHeader.setAccessSecurity(value);
			OperatorInfo opInfo = new OperatorInfo();
			opInfo.setChannelID("1");
			opInfo.setOperatorID("101");
			loanRequestHeader.setOperatorInfo(opInfo);
		} catch (Exception ex) {
		}
		return loanRequestHeader;
	}
}
