package com.evampsaanga.b2b.gethomepage;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.ALWAYS)
@JsonPropertyOrder({ "resourcesTitleLabel", "resourceType", "resourceInitialUnits", "resourceRemainingUnits",
		"resourceUnitName", "resourceDiscountedText" })
public class FreeResource {
	String remainingFormatted = "";
	@JsonProperty("resourcesTitleLabel")
	private String resourcesTitleLabel = "";
	@JsonProperty("resourceType")
	private String resourceType = "";
	@JsonProperty("resourceInitialUnits")
	private String resourceInitialUnits = "";
	@JsonProperty("resourceRemainingUnits")
	private String resourceRemainingUnits = "";
	@JsonProperty("resourceUnitName")
	private String resourceUnitName = "";
	@JsonProperty("resourceDiscountedText")
	private String resourceDiscountedText = "";
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	/**
	 * @return the remainingFormatted
	 */
	public String getRemainingFormatted() {
		return remainingFormatted;
	}

	/**
	 * @param remainingFormatted
	 *            the remainingFormatted to set
	 */
	public void setRemainingFormatted(String remainingFormatted) {
		this.remainingFormatted = remainingFormatted;
	}

	public FreeResource(String remainingFormatted, String resourcesTitleLabel, String resourceType,
			String resourceInitialUnits, String resourceRemainingUnits, String resourceUnitName,
			String resourceDiscountedText) {
		super();
		this.remainingFormatted = remainingFormatted;
		this.resourcesTitleLabel = resourcesTitleLabel;
		this.resourceType = resourceType;
		this.resourceInitialUnits = resourceInitialUnits;
		this.resourceRemainingUnits = resourceRemainingUnits;
		this.resourceUnitName = resourceUnitName;
		this.resourceDiscountedText = resourceDiscountedText;
	}

	@JsonProperty("resourcesTitleLabel")
	public String getResourcesTitleLabel() {
		return resourcesTitleLabel;
	}

	@JsonProperty("resourcesTitleLabel")
	public void setResourcesTitleLabel(String resourcesTitleLabel) {
		this.resourcesTitleLabel = resourcesTitleLabel;
	}

	@JsonProperty("resourceType")
	public String getResourceType() {
		return resourceType;
	}

	@JsonProperty("resourceType")
	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}

	@JsonProperty("resourceInitialUnits")
	public String getResourceInitialUnits() {
		return resourceInitialUnits;
	}

	@JsonProperty("resourceInitialUnits")
	public void setResourceInitialUnits(String resourceInitialUnits) {
		this.resourceInitialUnits = resourceInitialUnits;
	}

	@JsonProperty("resourceRemainingUnits")
	public String getResourceRemainingUnits() {
		return resourceRemainingUnits;
	}

	@JsonProperty("resourceRemainingUnits")
	public void setResourceRemainingUnits(String resourceRemainingUnits) {
		this.resourceRemainingUnits = resourceRemainingUnits;
	}

	@JsonProperty("resourceUnitName")
	public String getResourceUnitName() {
		return resourceUnitName;
	}

	@JsonProperty("resourceUnitName")
	public void setResourceUnitName(String resourceUnitName) {
		this.resourceUnitName = resourceUnitName;
	}

	@JsonProperty("resourceDiscountedText")
	public String getResourceDiscountedText() {
		return resourceDiscountedText;
	}

	@JsonProperty("resourceDiscountedText")
	public void setResourceDiscountedText(String resourceDiscountedText) {
		this.resourceDiscountedText = resourceDiscountedText;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}
}
