package com.evampsaanga.b2b.gethomepage;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.ALWAYS)
public class FreeResourcesRoaming {
	String remainingFormatted = "";
	String resourceType = "";
	long resourceInitialUnits = 0L;
	long resourceRemainingUnits = 0L;
	String resourceUnitName = "";

	/**
	 * @return the resourceType
	 */
	public String getResourceType() {
		return resourceType;
	}

	/**
	 * @param resourceType
	 *            the resourceType to set
	 */
	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}

	/**
	 * @return the resourceInitialUnits
	 */
	public long getResourceInitialUnits() {
		return resourceInitialUnits;
	}

	/**
	 * @return the remainingFormatted
	 */
	public String getRemainingFormatted() {
		return remainingFormatted;
	}

	/**
	 * @param remainingFormatted
	 *            the remainingFormatted to set
	 */
	public void setRemainingFormatted(String remainingFormatted) {
		this.remainingFormatted = remainingFormatted;
	}

	/**
	 * @param resourceInitialUnits
	 *            the resourceInitialUnits to set
	 */
	public void setResourceInitialUnits(long resourceInitialUnits) {
		this.resourceInitialUnits = resourceInitialUnits;
	}

	/**
	 * @return the resourceRemainingUnits
	 */
	public long getResourceRemainingUnits() {
		return resourceRemainingUnits;
	}

	/**
	 * @param resourceRemainingUnits
	 *            the resourceRemainingUnits to set
	 */
	public void setResourceRemainingUnits(long resourceRemainingUnits) {
		this.resourceRemainingUnits = resourceRemainingUnits;
	}

	/**
	 * @return the resourceUnitName
	 */
	public String getResourceUnitName() {
		return resourceUnitName;
	}

	/**
	 * @param resourceUnitName
	 *            the resourceUnitName to set
	 */
	public void setResourceUnitName(String resourceUnitName) {
		this.resourceUnitName = resourceUnitName;
	}

	public FreeResourcesRoaming(String resourceType, long resourceInitialUnits, long resourceRemainingUnits,
			String resourceUnitName, String remainingFormatted) {
		super();
		this.resourceType = resourceType;
		this.resourceInitialUnits = resourceInitialUnits;
		this.resourceRemainingUnits = resourceRemainingUnits;
		this.resourceUnitName = resourceUnitName;
		this.remainingFormatted = remainingFormatted;
	}

	public FreeResourcesRoaming() {
		super();
	}
}
