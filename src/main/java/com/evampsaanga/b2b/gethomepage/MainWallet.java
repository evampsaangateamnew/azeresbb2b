package com.evampsaanga.b2b.gethomepage;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "balanceTypeName", "currency", "amount", "effectiveTime", "expireTime", "lowerLimit" })
public class MainWallet {
	@JsonProperty("balanceTypeName")
	private String balanceTypeName = "Main Balance";
	@JsonProperty("currency")
	private String currency = "AZN";
	@JsonProperty("amount")
	private String amount = "0.00";
	@JsonProperty("effectiveTime")
	private String effectiveTime = "";
	@JsonProperty("expireTime")
	private String expireTime = "";
	@JsonProperty("lowerLimit")
	private String lowerLimit = "0.00";
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	public MainWallet() {
		super();
	}

	public MainWallet(String balanceTypeName, String currency, String amount, String effectiveTime, String expireTime,
			String lowerLimit) {
		super();
		this.balanceTypeName = "Main Balance";
		this.currency = currency;
		this.amount = amount;
		this.effectiveTime = effectiveTime;
		this.expireTime = expireTime;
		this.lowerLimit = lowerLimit;
	}

	@JsonProperty("balanceTypeName")
	public String getBalanceTypeName() {
		return balanceTypeName;
	}

	@JsonProperty("balanceTypeName")
	public void setBalanceTypeName(String balanceTypeName) {
		this.balanceTypeName = balanceTypeName;
	}

	@JsonProperty("currency")
	public String getCurrency() {
		return currency;
	}

	@JsonProperty("currency")
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	@JsonProperty("amount")
	public String getAmount() {
		return amount;
	}

	@JsonProperty("amount")
	public void setAmount(String amount) {
		this.amount = amount;
	}

	@JsonProperty("effectiveTime")
	public String getEffectiveTime() {
		return effectiveTime;
	}

	@JsonProperty("effectiveTime")
	public void setEffectiveTime(String effectiveTime) {
		this.effectiveTime = effectiveTime;
	}

	@JsonProperty("expireTime")
	public String getExpireTime() {
		return expireTime;
	}

	@JsonProperty("expireTime")
	public void setExpireTime(String expireTime) {
		this.expireTime = expireTime;
	}

	@JsonProperty("lowerLimit")
	public String getLowerLimit() {
		return lowerLimit;
	}

	@JsonProperty("lowerLimit")
	public void setLowerLimit(String lowerLimit) {
		this.lowerLimit = lowerLimit;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}
}
