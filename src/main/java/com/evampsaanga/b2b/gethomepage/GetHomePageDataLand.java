package com.evampsaanga.b2b.gethomepage;

import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.amqimplementationsesb.LogsType;
import com.evampsaanga.b2b.azerfon.hlrweb.getquerybalanceRequest.HLRBalanceServices;
import com.evampsaanga.b2b.azerfon.loan.getdebtinfo.GetDebtInfoRequest;
import com.evampsaanga.b2b.azerfon.loan.getdebtinfo.GetDebtInfoResponse;
import com.evampsaanga.b2b.azerfon.thirdpartycall.AzerfonThirdPartyCalls;
import com.evampsaanga.b2b.azerfon.ussdgwqueryfreeresource.FreeResourcesUSSDservices;
import com.evampsaanga.b2b.azerfon.validator.RequestValidator;
import com.evampsaanga.b2b.azerfon.validator.ResponseValidator;
import com.evampsaanga.b2b.azerfon.validator.ValidatorService;
import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.huawei.bme.cbsinterface.arservices.QueryBalanceResultMsg;
import com.huawei.bme.cbsinterface.bbservices.QueryFreeUnitResultMsg;
import com.huawei.bme.cbsinterface.bccommon.OfferingKey;
import com.huawei.bme.cbsinterface.bccommon.SubAccessCode;
import com.huawei.bme.cbsinterface.bcservices.QueryOfferingRentCycleRequest;
import com.huawei.bme.cbsinterface.bcservices.QueryOfferingRentCycleRequest.OfferingInst;
import com.huawei.bme.cbsinterface.bcservices.QueryOfferingRentCycleRequest.OfferingInst.OfferingOwner;
import com.huawei.bme.cbsinterface.bcservices.QueryOfferingRentCycleRequestMsg;
import com.huawei.bme.cbsinterface.bcservices.QueryOfferingRentCycleResult.OfferingRentCycle;
import com.huawei.bme.cbsinterface.bcservices.QueryOfferingRentCycleResultMsg;
import com.huawei.bme.cbsinterface.bcservices.QuerySubLifeCycleRequest;
import com.huawei.bme.cbsinterface.bcservices.QuerySubLifeCycleRequestMsg;
import com.huawei.bme.cbsinterface.bcservices.QuerySubLifeCycleResultMsg;
import com.huawei.bss.soaif._interface.common.Installment;
import com.huawei.bss.soaif._interface.common.ReqHeader;
import com.huawei.bss.soaif._interface.subscriberservice.QueryHandsetInstallmentReqMsg;
import com.huawei.bss.soaif._interface.subscriberservice.QueryHandsetInstallmentRspMsg;
import com.huawei.crm.azerfon.bss.query.GetSubscriberResponse;
import com.huawei.crm.basetype.RequestHeader;
import com.ngbss.evampsaanga.services.ThirdPartyCall;
import com.ngbss.evampsaanga.services.ThirdPartyRequestHeader;

@Path("/azerfon/")
public class GetHomePageDataLand {

	public static final Logger logger = Logger.getLogger("azerfon-esb");

	public GetHomePageResponse queryLoanLog(String msisdn, GetHomePageResponse res,String token,String transactionName ) {
	/*	QueryLoanLog lo = new QueryLoanLog();
		QueryLoanLogResultMsg loanResponse = lo.queryLoanLog(msisdn);
		if (loanResponse.getResultHeader().getResultCode().equals("0")) {
			Long totalCredit = 0L;
			QueryLoanLogResult loanLog = loanResponse.getQueryLoanLogResult();
			if (loanLog != null && loanLog.getRepaymentLogDetail() != null) {
				List<LoanLogDetail> loanLogDetails = loanLog.getLoanLogDetail();
				for (int i = 0; i < loanLogDetails.size(); i++) {
					if (loanLogDetails.get(i).getLoanStatus().equalsIgnoreCase(Constants.LOAN_STATUS_OPEN)) {
						totalCredit += loanLogDetails.get(i).getRepaymentAMT() - loanLogDetails.get(i).getPaidAMT();
					}
				}
			}*/
		GetDebtInfoResponse getDebtInfoResponse=new GetDebtInfoResponse();
		GetDebtInfoRequest getDebtInfoRequest=new GetDebtInfoRequest();
		getDebtInfoRequest.setMsisdn(msisdn);
		
		try {
			getDebtInfoResponse = AzerfonThirdPartyCalls.loanGetDebitInfo(token,  transactionName,
					 getDebtInfoRequest,  getDebtInfoResponse);
			logger.info(token+Helper.ObjectToJson(getDebtInfoResponse));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.info(token+ Helper.GetException(e));
		}
		  
			Credit credit = new Credit();
			credit.setCreditTitleValue(""+Helper.getBakcellMoneyDouble(getDebtInfoResponse.getDebtInfo().getTotalDebtsAmount()));
			/*credit.setCreditTitleValue(Helper.getBakcellMoney(totalCredit));*/
			res.getData().setCredit(credit);
		
		return res;
	}

	@POST
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GetHomePageResponse Get(@Header("credentials") String credential, @Body() String requestBody) {
		Logs logs = new Logs();
		try {

			// ------------------------- Setting Transaction Name ----------------------//
			logs.setTableType(LogsType.HomePage);
			logs.setTransactionName(LogsType.HomePage.name());	
			GetHomePageResponse res = new GetHomePageResponse();
			GetHomePageRequestClient cclient = null;
			String trnsactionName = "GHomePageDataLand" + Transactions.HOME_PAGE_TRANSACTION_NAME;
			String token = Helper.retrieveToken(trnsactionName, Helper.getValueFromJSON(requestBody, "msisdn"));
			Constants.logger.info(token + "GetHomePageDataLand request Landed:" + requestBody);
			Constants.logger.info("");
			// -------------------------------------------------------------------------//
			// ----------------------------- Validating text ---------------------------//
			cclient = Helper.JsonToObject(requestBody, GetHomePageRequestClient.class);
			ValidatorService validatorService = new ValidatorService();
			ResponseValidator responseValidator = new ResponseValidator();
			RequestValidator requestValidator = new RequestValidator();
			requestValidator.setChannel(cclient.getChannel());
			requestValidator.setiP(cclient.getiP());
			requestValidator.setIsB2B(cclient.getIsB2B());
			requestValidator.setLang(cclient.getLang());
			requestValidator.setMsisdn(cclient.getmsisdn());

			responseValidator = validatorService.processValidation(requestValidator, credential, token);
			if (responseValidator.getResponseCode().equals("00")) 
			{
				///code added to check status while refresh page
				GetSubscriberResponse getSubscriberResponse = new com.evampsaanga.b2b.azerfon.getsubscriber.CRMSubscriberService()
						.GetSubscriberRequest(cclient.getmsisdn());
//				if (getSubscriberResponse.getResponseHeader().getRetCode().equals("0")) 
//				{
					res.getData().setStatus(ConfigurationManager.getConfigurationFromCache(ConfigurationManager.MAPPING_CRM_HOMEPAGE_STATUS+ getSubscriberResponse.getGetSubscriberBody().getStatus()+"."+Helper.getLang(cclient.getLang())));
					
//				}
//				else
//				{
//					res.getData().setStatus("");
//				}
				res.getData().setOfferingNameDisplay(ConfigurationManager.getConfigurationFromCache("tariff.name."
						+cclient.getOfferingId()+"."+cclient.getLang()));
				
				logger.info(token + "*******************   offering id        ********************"+cclient.getOfferingId());
				logger.info(token + "*******************   offering name display        ********************"+ConfigurationManager.getConfigurationFromCache("tariff.name."
						+cclient.getOfferingId()+"."+cclient.getLang()));
				
				logger.info(token + "*******************   Fetching MRC        ********************");
				
				res.getData().setMrc(getMRC(cclient, token));
				logger.info(token + "*MRC=" + res.getData().getMrc().getMrcLimit());

				// -------------------calling Free unit ---------------------//
				QueryFreeUnitResultMsg freeunitresp = AzerfonThirdPartyCalls.queryfreeUnitResponseThirdParty(token,
						cclient.getmsisdn(), logger);
				if(freeunitresp.getQueryFreeUnitResult()!=null)
				{
						res.getData().setFreeResources(
						FreeResourcesUSSDservices.processUnityTypesHome(freeunitresp, cclient.getTariffType(),token));
				}
				else
				{
					res.getData().setFreeResources(
							FreeResourcesUSSDservices.processUnityTypesHomeInCaseOfNull(freeunitresp, cclient.getTariffType(),token));
				}
				
				logger.info(token + "*******************  Query Loan FAILED PENDING   ********************");
				// ----------------------TODO-----------------------------------
				res = queryLoanLog(cclient.getmsisdn(), res,token,trnsactionName);
				// --------------------------------------------------------------

				HLRBalanceServices service = new HLRBalanceServices();
				QueryBalanceResultMsg balanceResponse = AzerfonThirdPartyCalls
						.queryBalanceResponseArServices(cclient.getmsisdn(), token);
				res.getData().setBalance(
						service.getBalance(cclient.getmsisdn(), cclient.getCustomerType(), balanceResponse, token));

				logger.info(token + "****************************************************");
				logger.info(token + "AMOUNT_TO_BE_PAID: check for B01 B02 Postpaid");
				logger.info(token + "STATUS_CODE: " + cclient.getStatusCode());
				logger.info(token + "chevck if B03 " + cclient.getStatusCode().equalsIgnoreCase("B03"));
				logger.info(token + "check if B04 " + cclient.getStatusCode().equalsIgnoreCase("B04"));
				logger.info(token + "check if Postpaid " + cclient.getSubscriberType().equalsIgnoreCase("postpaid"));
				logger.info(token + "STATUS_CODE: " + cclient.getStatusCode());
				Constants.logger.info("");
				ObjectMapper mapper = new ObjectMapper();
				logger.info(token + "BALANCE IS: " + mapper.writeValueAsString(res.getData().getBalance()));
				Constants.logger.info("");
				QuerySubLifeCycleResultMsg response = AzerfonThirdPartyCalls.querySubLifeCycle(cclient.getmsisdn(),
						token);
				if (response.getResultHeader().getResultCode().equalsIgnoreCase("0")) {
					String index = response.getQuerySubLifeCycleResult().getCurrentStatusIndex();
					logger.info(token + "SUbLifeCyclecurrentStatusIndex :" + index);
					for (int lifecycle = 0; lifecycle < response.getQuerySubLifeCycleResult().getLifeCycleStatus()
							.size(); lifecycle++) 
					{
						logger.info(token + "SUbLifeCycleDate :" + response.getQuerySubLifeCycleResult()
								.getLifeCycleStatus().get(lifecycle).getStatusExpireTime());
						if (response.getQuerySubLifeCycleResult().getLifeCycleStatus().get(lifecycle).getStatusIndex()
								.equalsIgnoreCase(index)) {

							res.getData().getCredit().setCreditDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(
									new SimpleDateFormat("yyyyMMddHHmmss").parse(response.getQuerySubLifeCycleResult()
											.getLifeCycleStatus().get(lifecycle).getStatusExpireTime())));
							Calendar calendar = Calendar.getInstance();
							calendar.add(Calendar.DATE, -10);
							res.getData().getCredit().setCreditInitialDate(
									new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calendar.getTime()));
							////////////////////////////////////////////////////////////////////////////////
							
							long startMili = Helper.getMiliSecondsFromDate(
									new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calendar.getTime()));
							
                            long endMili = Helper.getMiliSecondsFromDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(
									new SimpleDateFormat("yyyyMMddHHmmss").parse(response.getQuerySubLifeCycleResult()
											.getLifeCycleStatus().get(lifecycle).getStatusExpireTime())));
                            
                            endMili = Helper.getOneDayMinus(endMili);
                            long currentMili = Helper.getCurrentMiliSeconds();

                            int daysDiffCurrent = Helper.getDaysDifferenceBetweenMiliseconds(startMili, currentMili);
                            int daysDiffTotal = Helper.getDaysDifferenceBetweenMiliseconds(startMili, endMili);
                            res.getData().getCredit().setDaysDifferenceCurrent(String.valueOf(daysDiffCurrent));
                            res.getData().getCredit().setDaysDifferenceTotal(String.valueOf(daysDiffTotal));

                            ////////////////////////////////////////////////////////////////////////////////
							if (index.equalsIgnoreCase("2"))
								res.getData().getCredit()
										.setCreditDateLabel(Constants.QUERY_SUBSCRIBER_LIFE_CYCLE_STATUS_B1W);
							else if (index.equalsIgnoreCase("3"))
								res.getData().getCredit()
										.setCreditDateLabel(Constants.QUERY_SUBSCRIBER_LIFE_CYCLE_STATUS_B2W);
							else if (index.equalsIgnoreCase("4"))
								res.getData().getCredit()
										.setCreditDateLabel(Constants.QUERY_SUBSCRIBER_LIFE_CYCLE_STATUS_EXP);
						}
					}
					// dd-MM-yyyy 20190116161440
					/*res.getData().getCredit().setCreditDate("2019-03-30 00:00:00");
					res.getData().getCredit().setCreditInitialDate("2019-03-24 16:14:40");*/
				}

				
				
				
				// -------------------------TODO if we get API till
				// 15-April------------------------------------//
//				QueryHandsetInstallmentRspMsg handsetInstallment = null;
////				handsetInstallment = AzerfonThirdPartyCalls.queryHandSetInstallments(cclient.getmsisdn(), token);
//				if (handsetInstallment != null)
//				{
//					if (handsetInstallment.getRspHeader().getReturnCode().equals("0000")) 
//					{
//						for (Resource rs : handsetInstallment.getResource()) 
//						{
//							Installment inst = getLastInstallment(rs.getInstallment());
//							if (!inst.getInstallmentStatus().equalsIgnoreCase("F")) 
//							{
//								com.evampsaanga.gethomepage.Installment instObject = new com.evampsaanga.gethomepage.Installment();
//								if (rs.getResourceType().equals("40"))
//									instObject.setName(rs.getResourceModel());
//								instObject.setAmountValue((inst.getTotalAmount() / Constants.MONEY_DIVIDEND) + "");
//								instObject.setInstallmentFeeLimit(ConfigurationManager.getConfigurationFromCache("handsetinstallment.limitfee").trim() + "");
//								instObject.setCurrency(Constants.CURRENCY_AZN);
//								instObject.setRemainingAmountTotalValue(inst.getTotalAmount() / Constants.MONEY_DIVIDEND + "");
//								instObject.setRemainingAmountCurrentValue(inst.getRemainingAmount() / Constants.MONEY_DIVIDEND + "");
//								instObject.setRemainingCurrentPeriod(inst.getRemainingPeriods() + "");
//								instObject.setRemainingTotalPeriod(inst.getTotalPeriods() + "");
//								instObject.setRemainingPeriodBeginDateValue(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new SimpleDateFormat("yyyyMMddHHmmss").parse(inst.getBeginDate())));
//								instObject.setRemainingPeriodEndDateValue(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new SimpleDateFormat("yyyyMMddHHmmss").parse(inst.getEndDate())));
//								instObject.setPurchaseDateValue(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new SimpleDateFormat("yyyyMMddHHmmss").parse(rs.getPurchaseDate())));
//															
//								instObject.setInstallmentStatus(ConfigurationManager.getConfigurationFromCache(ConfigurationManager.MAPPING_INSTALLMENTSTATUS+ inst.getInstallmentStatus()));
//								
//								res.getData().getInstallments().add(instObject);
//								
//								res.setReturnCode(ResponseCodes.SUCESS_CODE_200);
//								res.setReturnMsg(ResponseCodes.SUCESS_DES_200);
//								logs.setResponseCode(res.getReturnCode());
//								logs.setResponseDescription(res.getReturnMsg());
//								logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
//								logs.updateLog(logs);
//							}
//						}
//					}
//					else
//					{
//						res.setReturnCode(ResponseCodes.SUCESS_CODE_200);
//						res.setReturnMsg(ResponseCodes.SUCESS_DES_200);
//						logs.setResponseCode(res.getReturnCode());
//						logs.setResponseDescription(res.getReturnMsg());
//						logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
//						logs.updateLog(logs);
//					}
			}

			else {
				GetHomePageResponse resp = new GetHomePageResponse();
				resp.setReturnCode(responseValidator.getResponseCode());
				resp.setReturnMsg(responseValidator.getResponseDescription());
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);

			}

			Constants.logger.info("");

			res.setReturnMsg(ResponseCodes.SUCESS_DES_200);
			res.setReturnCode(ResponseCodes.SUCESS_CODE_200);
			Constants.logger.info(token + "HomPage RESPONSE Returned From ESB:" + Helper.ObjectToJson(res));
			return res;

		} catch (Exception ex) {
			Constants.logger.info(Helper.GetException(ex));
			GetHomePageResponse resp = new GetHomePageResponse();
			resp.setReturnCode(ResponseCodes.CONNECTIVITY_PROBLEM_CODE);
			resp.setReturnMsg(ResponseCodes.CONNECTIVITY_PROBLEM_DES);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}

	}

	public Installment getLastInstallment(List<Installment> installment) throws ParseException {
		Installment lastInstallment = new Installment();
		Date date = new SimpleDateFormat("yyyyMMddHHmmss").parse("19930101000000");
		for (Installment instObject : installment) {
			try {
				Date latestD = new SimpleDateFormat("yyyyMMddHHmmss").parse(instObject.getBeginDate());
				if (latestD.after(date)) {
					date = latestD;
					lastInstallment = instObject;
				}
			} catch (ParseException ex) {
				logger.error(Helper.GetException(ex));
			}
		}
		return lastInstallment;
	}

//<<<<<<< HEAD
	public QueryHandsetInstallmentRspMsg getHandsetInstallments(String msisdn) {
		com.huawei.bss.soaif._interface.common.ReqHeader reqHHLR = new com.huawei.bss.soaif._interface.common.ReqHeader();
		reqHHLR.setChannelId(getRequestHeaderForCRM().getChannelId());
		reqHHLR.setAccessUser(getRequestHeaderForCRM().getAccessUser());
		reqHHLR.setAccessPwd(getRequestHeaderForCRM().getAccessPwd());
		reqHHLR.setTransactionId(Helper.generateTransactionID());
		reqHHLR.setTenantId(getRequestHeaderForCRM().getTenantId());
		reqHHLR.setOperatorId(getRequestHeaderForCRM().getAccessUser());
		QueryHandsetInstallmentReqMsg reqMsg = new QueryHandsetInstallmentReqMsg();
		reqMsg.setServiceNumber(msisdn);
		reqMsg.setReqHeader(reqHHLR);
//		return SubscriberService.getInstance().queryHandsetInstallment(reqMsg);
		return ThirdPartyCall.setQueryHandsetInstallment(reqMsg);
	}

//	public GetHomePageResponse getQueryBalance(GetHomePageRequestClient cclient, GetHomePageResponse responseMain) {
//		try {
//			HLRBalanceServices service = new HLRBalanceServices();
//			Balance balance = service.getBalance(cclient.getmsisdn(), cclient.getCustomerType(), null);
//			if (balance.getReturnCode().equals("0")) {
//				responseMain.getData().setBalance(balance);
//			}
//		} catch (Exception ee) {
//			logger.error(Helper.GetException(ee));
//			if (ee instanceof WebServiceException) {
//			}
//		}
//		return responseMain;
//	}
//=======

	public ReqHeader getReqHeaderForQueryBalanceHLR() {
		ReqHeader reqHForQBalance = new ReqHeader();
		reqHForQBalance.setChannelId(ConfigurationManager.getConfigurationFromCache("hlrweb.reqh.ChannelId").trim());
		reqHForQBalance.setAccessUser(ConfigurationManager.getConfigurationFromCache("hlrweb.reqh.AccessUser").trim());
		reqHForQBalance.setAccessPwd(ConfigurationManager.getConfigurationFromCache("hlrweb.reqh.AccessPwd").trim());
		reqHForQBalance.setMIDWAREChannelID(
				ConfigurationManager.getConfigurationFromCache("hlrweb.reqh.MIDWAREChannelID").trim());
		reqHForQBalance.setMIDWAREAccessPwd(
				ConfigurationManager.getConfigurationFromCache("hlrweb.reqh.MIDWAREAccessPwd").trim());
		reqHForQBalance.setMIDWAREAccessUser(
				ConfigurationManager.getConfigurationFromCache("hlrweb.reqh.MIDWAREAccessUser").trim());
		reqHForQBalance.setTransactionId(Helper.generateTransactionID());
		return reqHForQBalance;
	}

	public RequestHeader getRequestHeaderForCRM() {
		RequestHeader reqhForCRM = new RequestHeader();
		reqhForCRM.setChannelId(ConfigurationManager.getConfigurationFromCache("crm.sub.ChannelId").trim());
		reqhForCRM.setTechnicalChannelId(
				ConfigurationManager.getConfigurationFromCache("crm.sub.TechnicalChannelId").trim());
		reqhForCRM.setAccessUser(ConfigurationManager.getConfigurationFromCache("crm.sub.AccessUser").trim());
		reqhForCRM.setTenantId(ConfigurationManager.getConfigurationFromCache("crm.sub.TenantId").trim());
		reqhForCRM.setAccessPwd(ConfigurationManager.getConfigurationFromCache("crm.sub.AccessPwd").trim());
		reqhForCRM.setTestFlag(ConfigurationManager.getConfigurationFromCache("crm.sub.TestFlag").trim());
		reqhForCRM.setLanguage(ConfigurationManager.getConfigurationFromCache("crm.sub.Language").trim());
		reqhForCRM.setTransactionId(Helper.generateTransactionID());
		return reqhForCRM;
	}

	public QuerySubLifeCycleResultMsg getsublife(String msisdn) {
		QuerySubLifeCycleRequestMsg requestMsg = new QuerySubLifeCycleRequestMsg();
//		requestMsg.setRequestHeader(BcService.getRequestHeader());
		requestMsg.setRequestHeader(ThirdPartyRequestHeader.getCBserviceRequestHeader());
		QuerySubLifeCycleRequest lifeC = new QuerySubLifeCycleRequest();
		SubAccessCode subACode = new SubAccessCode();
		subACode.setPrimaryIdentity(msisdn);
		lifeC.setSubAccessCode(subACode);
		requestMsg.setQuerySubLifeCycleRequest(lifeC);
//		QuerySubLifeCycleResultMsg result = BcService.getInstance().querySubLifeCycle(requestMsg);
		QuerySubLifeCycleResultMsg result = ThirdPartyCall.getQuerySubLifeCycle(requestMsg);
		return result;
	}

	private Mrc getMRC(GetHomePageRequestClient cClient, String token) {
		Mrc mrc = new Mrc();
		// check if user is GUNBOUY or KLASS
		logger.info(token + " Get MRC for " + cClient.getmsisdn() + " BrandID :" + cClient.getBrandId());

    if(cClient.getCustomerType().equalsIgnoreCase("prepaid") && !cClient.getOfferingId().equalsIgnoreCase(Constants.WTTX_PREPAID_OFFERINGID))
    {
			
    	String offeringId = cClient.getOfferingId();
    	boolean suppl_primary_check=false;
    	if(
    			ConfigurationManager.getConfigurationFromCache("mrc.offering." + cClient.getOfferingId())!=null 
    			&& (!ConfigurationManager.getConfigurationFromCache("mrc.offering." + cClient.getOfferingId()).equals(""))
				&& (!ConfigurationManager.getConfigurationFromCache("mrc.offering." + cClient.getOfferingId()).isEmpty()))
		{
			logger.info(token+ "OffferingID From Request :"+cClient.getOfferingId());
			cClient.setOfferingId(ConfigurationManager.getConfigurationFromCache("mrc.offering." + cClient.getOfferingId()));
			suppl_primary_check=true;
			logger.info(token+ "the offeringId is mapped and TRUE :"+suppl_primary_check);
			logger.info(token+ "OffferingID Final :"+cClient.getOfferingId());
		}
			// changing offering id in request object as per MRC mapping
			/*cClient.setOfferingId(
					ConfigurationManager.getConfigurationFromCache("mrc.offering." + cClient.getOfferingId()));*/
			mrc.setMrcTitleValue(ConfigurationManager.getConfigurationFromCache("mrc.price." + offeringId));
			mrc.setMrcType(getMRCType(offeringId));
			logger.info(token + "Offering Key Mapping: " + cClient.getOfferingId());
			try {
				GetSubscriberResponse respns = new com.evampsaanga.b2b.azerfon.getsubscriber.CRMSubscriberService()
						.GetSubscriberRequest(cClient.getmsisdn());
				if (respns.getResponseHeader().getRetCode().equals("0")) {
					
					if(suppl_primary_check)
					{
						logger.info("-Boolean check True-- Supplmentary Offer CHECK");
					for (int i = 0; i < respns.getGetSubscriberBody().getSupplementaryOfferingList()
							.getGetSubOfferingInfo().size(); i++) 
					{
						logger.info(token + "First check: "
								+ respns.getGetSubscriberBody().getSupplementaryOfferingList().getGetSubOfferingInfo()
										.get(i).getOfferingId().getOfferingId()
										.equalsIgnoreCase(cClient.getOfferingId()));
						logger.info(token + "Second check: "
								+ respns.getGetSubscriberBody().getSupplementaryOfferingList().getGetSubOfferingInfo()
										.get(i).getStatus().equalsIgnoreCase(Constants.CORE_SERVICE_ACTIVE_STATUS));
						if (respns.getGetSubscriberBody().getSupplementaryOfferingList().getGetSubOfferingInfo().get(i).getOfferingId().getOfferingId().equalsIgnoreCase(cClient.getOfferingId())
								&& respns.getGetSubscriberBody().getSupplementaryOfferingList().getGetSubOfferingInfo().get(i).getStatus().equalsIgnoreCase(Constants.CORE_SERVICE_ACTIVE_STATUS)) 
						{
							
							logger.info(token + "----------Befor Calling MRC------------- ");
							mrc = new Mrc();
							mrc=setMrcForPrepaidSupplememntaryPrimary(mrc,token, cClient,offeringId);
						} // END OF CHECK status and offeringID
						else
						{
							logger.info(token+"---OfferingId not found from Subscriber or status inactive---");
							mrc=setMrcForPrepaidSupplememntaryPrimary(mrc,token, cClient,offeringId);
//							mrc.setMrcDate("2015-06-13 00:00:00");
						}
					}//END OF FOR LOoP for iterating ssupplmentrayOFFers
				  }   // END OF check boolean whether supplmentary 
				
				else
				{
					logger.info("---Boolean check is false as its primary Offering--");
				
				
					logger.info(token + "First check: "
						+ respns.getGetSubscriberBody().getPrimaryOffering().getOfferingId().getOfferingId()
								.equalsIgnoreCase(cClient.getOfferingId()));
					logger.info(token + "Second check: "
						+respns.getGetSubscriberBody().getPrimaryOffering().getStatus().equalsIgnoreCase(Constants.CORE_SERVICE_ACTIVE_STATUS));
				if (respns.getGetSubscriberBody().getPrimaryOffering().getOfferingId().getOfferingId().equalsIgnoreCase(cClient.getOfferingId())
						&& respns.getGetSubscriberBody().getPrimaryOffering().getStatus().equalsIgnoreCase(Constants.CORE_SERVICE_ACTIVE_STATUS)) {
					
					logger.info(token + "----------Befor Calling MRC------------- ");
					mrc = new Mrc();
					mrc=setMrcForPrepaidSupplememntaryPrimary(mrc,token, cClient,offeringId);
				}
				else
				{
					logger.info(token+"---OfferingId not found from Subscriber or status inactive---");
					mrc=setMrcForPrepaidSupplememntaryPrimary(mrc,token, cClient,offeringId);
//					mrc.setMrcDate("2015-06-13 00:00:00");
				}
				} //END OF ELSE means primary offers logic
			}// END OF if (boolean) check wthere the offerinf id is supplementary of primary

				
			return mrc;
			} catch (Exception ex) {
				logger.error(token + Helper.GetException(ex));
			}
		}
		// //If user is prepaid and neither KLASS nor GUNBOUY returning null for
		// MRC
		// return mrc;
		// }
		else if (cClient.getCustomerType().equalsIgnoreCase("postpaid")) {
			try {
				logger.info("ID FROM CACHE CHECK :"+ConfigurationManager.getConfigurationFromCache("mrc.offering." + cClient.getOfferingId()));
				if(ConfigurationManager.getConfigurationFromCache("mrc.offering." + cClient.getOfferingId())!=null && 
						!ConfigurationManager.getConfigurationFromCache("mrc.offering." + cClient.getOfferingId()).equals("")
						&& !ConfigurationManager.getConfigurationFromCache("mrc.offering." + cClient.getOfferingId()).isEmpty())
				{
					logger.info(token+ "OffferingID From Request :"+cClient.getOfferingId());
					cClient.setOfferingId(
						ConfigurationManager.getConfigurationFromCache("mrc.offering." + cClient.getOfferingId()));
				logger.info(token+ "OffferingID From Cache :"+cClient.getOfferingId());
				}
				QueryOfferingRentCycleResultMsg mrcResponse = getMRCValueForPostPaid(cClient);
				if (mrcResponse != null && mrcResponse.getResultHeader().getResultCode().equals("0")) {
					if (mrcResponse.getQueryOfferingRentCycleResult() != null
							&& mrcResponse.getQueryOfferingRentCycleResult().getOfferingRentCycle() != null) {
						List<OfferingRentCycle> cycles = mrcResponse.getQueryOfferingRentCycleResult()
								.getOfferingRentCycle();
						for (Iterator<OfferingRentCycle> iterator = cycles.iterator(); iterator.hasNext();) {
							OfferingRentCycle offeringRentCycle = iterator.next();
							mrc.setMrcCurrency(ConfigurationManager
									.getConfigurationFromCache(offeringRentCycle.getCurrencyID() + ""));
							mrc.setMrcInitialDate(new SimpleDateFormat(Constants.SQL_DATE_FORMAT_required)
									.format(new SimpleDateFormat(Constants.SQL_DATE_FORMAT_TRIMMED)
											.parse(offeringRentCycle.getOpenDay())));
							mrc.setMrcDate(new SimpleDateFormat(Constants.SQL_DATE_FORMAT_required)
									.format(new SimpleDateFormat(Constants.SQL_DATE_FORMAT_TRIMMED)
											.parse(offeringRentCycle.getEndDay())));
							mrc.setMrcTitleValue(com.evampsaanga.b2b.developer.utils.Helper
									.getBakcellMoneyMRC(offeringRentCycle.getRentAmount()));
							if (mrc.getMrcTitleValue().equalsIgnoreCase("FREE"))
								mrc.setMrcCurrency("");
							mrc.setMrcType(Constants.MRC_TYPE_MONTHLY);
							mrc.setMrcLimit(Constants.MRC_LIMIT);
						}
					}
				}
				return mrc;
			} catch (Exception ex) {
				logger.error(token + Helper.GetException(ex));
			}
		} else {
			mrc = new Mrc();
			mrc.setMrcStatus("");
			mrc.setMrcType("");
		}
		return mrc;
	}
   public Mrc setMrcForPrepaidSupplememntaryPrimary(Mrc mrc, String token,GetHomePageRequestClient cClient,String offeringId)
   {
	  try{
	   QueryOfferingRentCycleResultMsg mrcResponse = getMRCValueForPostPaid(cClient);
		if (mrcResponse != null && mrcResponse.getResultHeader().getResultCode().equals("0")) {
			if (mrcResponse.getQueryOfferingRentCycleResult() != null && mrcResponse
					.getQueryOfferingRentCycleResult().getOfferingRentCycle() != null) {
				List<OfferingRentCycle> cycles = mrcResponse.getQueryOfferingRentCycleResult()
						.getOfferingRentCycle();
				for (Iterator<OfferingRentCycle> iterator = cycles.iterator(); iterator
						.hasNext();) {
					OfferingRentCycle offeringRentCycle = iterator.next();
					if (offeringRentCycle.getOpenDay() != null) {
						mrc.setMrcCurrency(ConfigurationManager
								.getConfigurationFromCache(offeringRentCycle.getCurrencyID() + ""));
						mrc.setMrcInitialDate(
								new SimpleDateFormat(Constants.SQL_DATE_FORMAT_required).format(
										new SimpleDateFormat(Constants.SQL_DATE_FORMAT_TRIMMED)
												.parse(offeringRentCycle.getOpenDay())));
						mrc.setMrcDate(new SimpleDateFormat(Constants.SQL_DATE_FORMAT_required)
								.format(new SimpleDateFormat(Constants.SQL_DATE_FORMAT_TRIMMED)
										.parse(offeringRentCycle.getEndDay())));
						mrc.setMrcTitleValue(com.evampsaanga.b2b.developer.utils.Helper
								.getBakcellMoneyMRC(offeringRentCycle.getRentAmount()));

						if (mrc.getMrcTitleValue().equalsIgnoreCase("FREE")) {
							mrc.setMrcCurrency("");
						}
						mrc.setMrcType(getMRCType(offeringId));
						mrc.setMrcLimit(Constants.MRC_LIMIT);
						Date mrcDate = new SimpleDateFormat(Constants.SQL_DATE_FORMAT_TRIMMED)
								.parse(offeringRentCycle.getEndDay());
						if (mrcDate.after(new Date()))
							mrc.setMrcStatus(Constants.MRC_STATUS_PAID);
						else
							mrc.setMrcStatus(Constants.MRC_STATUS_UNPAID);
					}
				}
			}
		}
	  }
		catch (Exception e) {
			logger.info(token+Helper.GetException(e));
		}
		return mrc;
   }
	public QueryOfferingRentCycleResultMsg getMRCValueForPostPaid(GetHomePageRequestClient cclient) {
		QueryOfferingRentCycleRequestMsg qORCR = new QueryOfferingRentCycleRequestMsg();
//		qORCR.setRequestHeader(BcService.getRequestHeader());
		qORCR.setRequestHeader(ThirdPartyRequestHeader.getCBserviceRequestHeader());
		QueryOfferingRentCycleRequest qORC = new QueryOfferingRentCycleRequest();
		OfferingInst of = new OfferingInst();
		OfferingKey offeringKey = new OfferingKey();
		
		offeringKey.setOfferingID(new BigInteger(cclient.getOfferingId()));

		of.setOfferingKey(offeringKey);
		OfferingOwner oO = new OfferingOwner();
		SubAccessCode subACode = new SubAccessCode();
		subACode.setPrimaryIdentity(cclient.getmsisdn());
		oO.setSubAccessCode(subACode);
		of.setOfferingOwner(oO);
		qORC.getOfferingInst().add(of);
		qORCR.setQueryOfferingRentCycleRequest(qORC);
//		return BcService.getInstance().queryOfferingRentCycle(qORCR);
		return ThirdPartyCall.getQueryOfferingRentCycle(qORCR);
	}

	public String getMRCType(String key) {
		String value = "";
		try {
			value = ConfigurationManager.getConfigurationFromCache("mrc." + key);
			if (value.equalsIgnoreCase("daily"))
				value = Constants.MRC_TYPE_DAILY;
			else if (value.equalsIgnoreCase("weekly"))
				value = Constants.MRC_TYPE_WEEKLY;
			else
				value = Constants.MRC_TYPE_MONTHLY;
		} catch (Exception e) {
			logger.error(Helper.GetException(e));
			value = Constants.MRC_TYPE_MONTHLY;
		}
		return value;
	}
	
	public static void main(String arg[])
	{
		try {
			System.out.println(new SimpleDateFormat(Constants.SQL_DATE_FORMAT_required).format(
							new SimpleDateFormat(Constants.SQL_DATE_FORMAT_TRIMMED)
									.parse("20190617000000")));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			logger.info(Helper.GetException(e));
		}
	}
	

}
