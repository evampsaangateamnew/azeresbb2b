package com.evampsaanga.b2b.gethomepage;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "creditTitleValue", "creditCurrency", "creditDate", "creditInitialDate", "creditLimit",
		"creditDateLabel","daysDifferenceTotal","daysDifferenceCurrent" })
public class Credit {
	@JsonProperty("creditTitleValue")
	private String creditTitleValue = "0.00";
	@JsonProperty("creditCurrency")
	private String creditCurrency = "AZN";
	@JsonProperty("creditDate")
	private String creditDate = "";
	@JsonProperty("creditInitialDate")
	private String creditInitialDate = "";
	@JsonProperty("creditLimit")
	private String creditLimit = "0.00";
	@JsonProperty("creditDateLabel")
	private String creditDateLabel = "";
	@JsonProperty("daysDifferenceTotal")
	private String daysDifferenceTotal = "";
	@JsonProperty("daysDifferenceCurrent")
	private String daysDifferenceCurrent = "";

	
	
	
	public String getDaysDifferenceTotal() {
		return daysDifferenceTotal;
	}

	public void setDaysDifferenceTotal(String daysDifferenceTotal) {
		this.daysDifferenceTotal = daysDifferenceTotal;
	}

	public String getDaysDifferenceCurrent() {
		return daysDifferenceCurrent;
	}

	public void setDaysDifferenceCurrent(String daysDifferenceCurrent) {
		this.daysDifferenceCurrent = daysDifferenceCurrent;
	}

	public String getCreditDateLabel() {
		return creditDateLabel;
	}

	public void setCreditDateLabel(String creditDateLabel) {
		this.creditDateLabel = creditDateLabel;
	}

	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	public Credit() {
	}

	public Credit(String creditTitleValue, String creditCurrency, String creditDate, String creditInitialDate,
			String creditLimit) {
		super();
		this.creditTitleValue = creditTitleValue;
		this.creditCurrency = creditCurrency;
		this.creditDate = creditDate;
		this.creditInitialDate = creditInitialDate;
		this.creditLimit = creditLimit;
	}

	@JsonProperty("creditTitleValue")
	public String getCreditTitleValue() {
		return creditTitleValue;
	}

	@JsonProperty("creditTitleValue")
	public void setCreditTitleValue(String creditTitleValue) {
		this.creditTitleValue = creditTitleValue;
	}

	@JsonProperty("creditCurrency")
	public String getCreditCurrency() {
		return creditCurrency;
	}

	@JsonProperty("creditCurrency")
	public void setCreditCurrency(String creditCurrency) {
		this.creditCurrency = creditCurrency;
	}

	@JsonProperty("creditDate")
	public String getCreditDate() {
		return creditDate;
	}

	@JsonProperty("creditDate")
	public void setCreditDate(String creditDate) {
		this.creditDate = creditDate;
	}

	@JsonProperty("creditInitialDate")
	public String getCreditInitialDate() {
		return creditInitialDate;
	}

	@JsonProperty("creditInitialDate")
	public void setCreditInitialDate(String creditInitialDate) {
		this.creditInitialDate = creditInitialDate;
	}

	@JsonProperty("creditLimit")
	public String getCreditLimit() {
		return creditLimit;
	}

	@JsonProperty("creditLimit")
	public void setCreditLimit(String creditLimit) {
		this.creditLimit = creditLimit;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}
}
