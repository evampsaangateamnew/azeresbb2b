package com.evampsaanga.b2b.gethomepage;

public class FreeResourcesTypes {
	String resourceType = "";
	long resourceInitialUnits;
	long resourceRemainingUnits;
	String resourceUnitName = "";
	String remainingFormatted = "";

	/**
	 * @return the remainingFormatted
	 */
	public String getRemainingFormatted() {
		return remainingFormatted;
	}

	/**
	 * @param remainingFormatted
	 *            the remainingFormatted to set
	 */
	public void setRemainingFormatted(String remainingFormatted) {
		this.remainingFormatted = remainingFormatted;
	}

	public long getResourceInitialUnits() {
		return resourceInitialUnits;
	}

	public void setResourceInitialUnits(long resourceInitialUnits) {
		this.resourceInitialUnits = resourceInitialUnits;
	}

	public long getResourceRemainingUnits() {
		return resourceRemainingUnits;
	}

	public void setResourceRemainingUnits(long resourceRemainingUnits) {
		this.resourceRemainingUnits = resourceRemainingUnits;
	}

	/**
	 * @return the resourceType
	 */
	public String getResourceType() {
		return resourceType;
	}

	/**
	 * @param resourceType
	 *            the resourceType to set
	 */
	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}

	/**
	 * @return the resourceInitialUnits
	 */
	public String getResourceUnitName() {
		return resourceUnitName;
	}

	/**
	 * @param resourceUnitName
	 *            the resourceUnitName to set
	 */
	public void setResourceUnitName(String resourceUnitName) {
		this.resourceUnitName = resourceUnitName;
	}

	public FreeResourcesTypes(String resourceType, long resourceInitialUnits, long resourceRemainingUnits,
			String resourceUnitName) {
		super();
		this.resourceType = resourceType;
		this.resourceInitialUnits = resourceInitialUnits;
		this.resourceRemainingUnits = resourceRemainingUnits;
		this.resourceUnitName = resourceUnitName;
	}

	public FreeResourcesTypes() {
		super();
	}
}
