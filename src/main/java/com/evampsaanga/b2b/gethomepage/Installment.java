package com.evampsaanga.b2b.gethomepage;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "name", "amountValue", "currency", "nextPaymentValue", "nextPaymentInitialDate",
		"remainingAmountTotalValue", "remainingAmountCurrentValue", "remainingCurrentPeriod", "remainingTotalPeriod",
		"remainingPeriodBeginDateValue", "remainingPeriodEndDateValue", "purchaseDateValue", "installmentFeeLimit",
		"installmentStatus" })
public class Installment {
	@JsonProperty("name")
	private String name = "";
	@JsonProperty("amountValue")
	private String amountValue = "";
	@JsonProperty("currency")
	private String currency = "";
	@JsonProperty("remainingAmountTotalValue")
	private String remainingAmountTotalValue = "";
	@JsonProperty("remainingAmountCurrentValue")
	private String remainingAmountCurrentValue = "";
	@JsonProperty("remainingCurrentPeriod")
	private String remainingCurrentPeriod = "";
	@JsonProperty("remainingTotalPeriod")
	private String remainingTotalPeriod = "";
	@JsonProperty("remainingPeriodBeginDateValue")
	private String remainingPeriodBeginDateValue = "";
	@JsonProperty("remainingPeriodEndDateValue")
	private String remainingPeriodEndDateValue = "";
	@JsonProperty("purchaseDateValue")
	private String purchaseDateValue = "";
	@JsonProperty("installmentFeeLimit")
	private String installmentFeeLimit = "";
	@JsonProperty("installmentStatus")
	private String installmentStatus = "";

	public Installment() {
	}

	public Installment(String name, String amountValue, String currency, String nextPaymentInitialDate,
			String remainingAmountTotalValue, String remainingAmountCurrentValue, String remainingCurrentPeriod,
			String remainingTotalPeriod, String remainingPeriodBeginDateValue, String remainingPeriodEndDateValue,
			String purchaseDateValue, String installmentFeeLimit, String installmentStatus) {
		super();
		this.name = name;
		this.amountValue = amountValue;
		this.currency = currency;
		this.remainingAmountTotalValue = remainingAmountTotalValue;
		this.remainingAmountCurrentValue = remainingAmountCurrentValue;
		this.remainingCurrentPeriod = remainingCurrentPeriod;
		this.remainingTotalPeriod = remainingTotalPeriod;
		this.remainingPeriodBeginDateValue = remainingPeriodBeginDateValue;
		this.remainingPeriodEndDateValue = remainingPeriodEndDateValue;
		this.purchaseDateValue = purchaseDateValue;
		this.installmentFeeLimit = installmentFeeLimit;
		this.installmentStatus = installmentStatus;
	}

	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("amountValue")
	public String getAmountValue() {
		return amountValue;
	}

	@JsonProperty("amountValue")
	public void setAmountValue(String amountValue) {
		this.amountValue = amountValue;
	}

	@JsonProperty("currency")
	public String getCurrency() {
		return currency;
	}

	@JsonProperty("currency")
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	@JsonProperty("remainingAmountTotalValue")
	public String getRemainingAmountTotalValue() {
		return remainingAmountTotalValue;
	}

	@JsonProperty("remainingAmountTotalValue")
	public void setRemainingAmountTotalValue(String remainingAmountTotalValue) {
		this.remainingAmountTotalValue = remainingAmountTotalValue;
	}

	@JsonProperty("remainingAmountCurrentValue")
	public String getRemainingAmountCurrentValue() {
		return remainingAmountCurrentValue;
	}

	@JsonProperty("remainingAmountCurrentValue")
	public void setRemainingAmountCurrentValue(String remainingAmountCurrentValue) {
		this.remainingAmountCurrentValue = remainingAmountCurrentValue;
	}

	@JsonProperty("remainingCurrentPeriod")
	public String getRemainingCurrentPeriod() {
		return remainingCurrentPeriod;
	}

	@JsonProperty("remainingCurrentPeriod")
	public void setRemainingCurrentPeriod(String remainingCurrentPeriod) {
		this.remainingCurrentPeriod = remainingCurrentPeriod;
	}

	@JsonProperty("remainingTotalPeriod")
	public String getRemainingTotalPeriod() {
		return remainingTotalPeriod;
	}

	@JsonProperty("remainingTotalPeriod")
	public void setRemainingTotalPeriod(String remainingTotalPeriod) {
		this.remainingTotalPeriod = remainingTotalPeriod;
	}

	@JsonProperty("remainingPeriodBeginDateValue")
	public String getRemainingPeriodBeginDateValue() {
		return remainingPeriodBeginDateValue;
	}

	@JsonProperty("remainingPeriodBeginDateValue")
	public void setRemainingPeriodBeginDateValue(String remainingPeriodBeginDateValue) {
		this.remainingPeriodBeginDateValue = remainingPeriodBeginDateValue;
	}

	@JsonProperty("remainingPeriodEndDateValue")
	public String getRemainingPeriodEndDateValue() {
		return remainingPeriodEndDateValue;
	}

	@JsonProperty("remainingPeriodEndDateValue")
	public void setRemainingPeriodEndDateValue(String remainingPeriodEndDateValue) {
		this.remainingPeriodEndDateValue = remainingPeriodEndDateValue;
	}

	@JsonProperty("purchaseDateValue")
	public String getPurchaseDateValue() {
		return purchaseDateValue;
	}

	@JsonProperty("purchaseDateValue")
	public void setPurchaseDateValue(String purchaseDateValue) {
		this.purchaseDateValue = purchaseDateValue;
	}

	@JsonProperty("installmentFeeLimit")
	public String getInstallmentFeeLimit() {
		return installmentFeeLimit;
	}

	@JsonProperty("installmentFeeLimit")
	public void setInstallmentFeeLimit(String installmentFeeLimit) {
		this.installmentFeeLimit = installmentFeeLimit;
	}

	@JsonProperty("installmentStatus")
	public String getInstallmentStatus() {
		return installmentStatus;
	}

	@JsonProperty("installmentStatus")
	public void setInstallmentStatus(String installmentStatus) {
		this.installmentStatus = installmentStatus;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}
}
