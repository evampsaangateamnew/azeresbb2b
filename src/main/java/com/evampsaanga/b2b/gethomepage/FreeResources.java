package com.evampsaanga.b2b.gethomepage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "freeResources", "freeResourcesRoaming" })
public class FreeResources {
	@JsonProperty("freeResources")
	private List<FreeResource> freeResources = new ArrayList<FreeResource>();
	@JsonProperty("freeResourcesRoaming")
	private List<FreeResourcesRoaming> freeResourcesRoaming = new ArrayList<FreeResourcesRoaming>();
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();
	@JsonIgnore
	private String returnMsg = "";
	@JsonIgnore
	private String returnCode = "";

	/**
	 * @return the returnMsg
	 */
	public String getReturnMsg() {
		return returnMsg;
	}

	/**
	 * @param returnMsg
	 *            the returnMsg to set
	 */
	public void setReturnMsg(String returnMsg) {
		this.returnMsg = returnMsg;
	}

	/**
	 * @return the returnCode
	 */
	public String getReturnCode() {
		return returnCode;
	}

	/**
	 * @param returnCode
	 *            the returnCode to set
	 */
	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}

	@JsonProperty("freeResources")
	public List<FreeResource> getFreeResources() {
		return freeResources;
	}

	@JsonProperty("freeResources")
	public void setFreeResources(List<FreeResource> freeResources) {
		this.freeResources = freeResources;
	}

	@JsonProperty("freeResourcesRoaming")
	public List<FreeResourcesRoaming> getFreeResourcesRoaming() {
		return freeResourcesRoaming;
	}

	@JsonProperty("freeResourcesRoaming")
	public void setFreeResourcesRoaming(List<FreeResourcesRoaming> freeResourcesRoaming) {
		this.freeResourcesRoaming = freeResourcesRoaming;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}
}
