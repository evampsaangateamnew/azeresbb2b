package com.evampsaanga.b2b.amqimplementationsesb;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Model class for Logs
 * 
 * @author EvampSaanga
 *
 */
public class Logs {
	private String msisdn = "";
	private String requestDateTime = "";
	private String responseDateTime = "";
	private String responseCode = "";
	private String responseDescription = "";
	private String ip = "";
	private String channel = "";
	private String transactionName = "";
	private String thirdPartyName = "";
	private String userType = "";
	private LogsType tableType = LogsType.AppMenu;
	private String receiverMsisdn = "";
	private String mediumForTopup = "";
	private String amount = "";
	private String scratchCardNumber = "";
	private String apiurl = "";
	private String lang="";
	private boolean onNet=false;
	private String activatedOfferId="";
	private String tariffId="";
	

	

	/**
	 * @return the tariffId
	 */
	public String getTariffId() {
		return tariffId;
	}

	/**
	 * @param tariffId the tariffId to set
	 */
	public void setTariffId(String tariffId) {
		this.tariffId = tariffId;
	}

	/**
	 * @return the activatedOfferId
	 */
	public String getActivatedOfferId() {
		return activatedOfferId;
	}

	/**
	 * @param activatedOfferId the activatedOfferId to set
	 */
	public void setActivatedOfferId(String activatedOfferId) {
		this.activatedOfferId = activatedOfferId;
	}



	

	/**
	 * @return the lang
	 */
	public String getLang() {
		return lang;
	}

	/**
	 * @param lang the lang to set
	 */
	public void setLang(String lang) {
		this.lang = lang;
	}

	public Logs() {
		super();
		String currentDate = new SimpleDateFormat(Constants.SQL_DATE_FORMAT).format(new Date());
		requestDateTime = currentDate;
		responseDateTime = currentDate;
	}

	/**
	 * @return the tableType
	 */
	public LogsType getTableType() {
		return tableType;
	}

	/**
	 * @param tableType
	 *            the tableType to set
	 */
	public void setTableType(LogsType tableType) {
		this.tableType = tableType;
	}

	/**
	 * @return the msisdn
	 */
	public String getMsisdn() {
		return msisdn;
	}

	/**
	 * @param msisdn
	 *            the msisdn to set
	 */
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	/**
	 * @return the requestDateTime
	 */
	public String getRequestDateTime() {
		return requestDateTime;
	}

	/**
	 * @param requestDateTime
	 *            the requestDateTime to set
	 */
	public void setRequestDateTime(String requestDateTime) {
		this.requestDateTime = requestDateTime;
	}

	/**
	 * @return the responseDateTime
	 */
	public String getResponseDateTime() {
		return responseDateTime;
	}

	/**
	 * @param responseDateTime
	 *            the responseDateTime to set
	 */
	public void setResponseDateTime(String responseDateTime) {
		this.responseDateTime = responseDateTime;
	}

	/**
	 * @return the responseCode
	 */
	public String getResponseCode() {
		return responseCode;
	}

	/**
	 * @param responseCode
	 *            the responseCode to set
	 */
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	/**
	 * @return the responseDescription
	 */
	public String getResponseDescription() {
		return responseDescription;
	}

	/**
	 * @param responseDescription
	 *            the responseDescription to set
	 */
	public void setResponseDescription(String responseDescription) {
		this.responseDescription = responseDescription;
	}

	/**
	 * @return the ip
	 */
	public String getIp() {
		return ip;
	}

	/**
	 * @param ip
	 *            the ip to set
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}

	/**
	 * @return the channel
	 */
	public String getChannel() {
		return channel;
	}

	/**
	 * @param channel
	 *            the channel to set
	 */
	public void setChannel(String channel) {
		this.channel = channel;
	}

	/**
	 * @return the transactionName
	 */
	public String getTransactionName() {
		return transactionName;
	}

	/**
	 * @param transactionName
	 *            the transactionName to set
	 */
	public void setTransactionName(String transactionName) {
		this.transactionName = transactionName;
	}

	/**
	 * @return the thirdPartyName
	 */
	public String getThirdPartyName() {
		return thirdPartyName;
	}

	/**
	 * @param thirdPartyName
	 *            the thirdPartyName to set
	 */
	public void setThirdPartyName(String thirdPartyName) {
		this.thirdPartyName = thirdPartyName;
	}

	/**
	 * @return the userType
	 */
	public String getUserType() {
		return userType;
	}

	/**
	 * @param userType
	 *            the userType to set
	 */
	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getReceiverMsisdn() {
		return receiverMsisdn;
	}

	public void setReceiverMsisdn(String receiverMsisdn) {
		this.receiverMsisdn = receiverMsisdn;
	}

	public String getMediumForTopup() {
		return mediumForTopup;
	}

	public void setMediumForTopup(String mediumForTopup) {
		this.mediumForTopup = mediumForTopup;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getScratchCardNumber() {
		return scratchCardNumber;
	}

	public void setScratchCardNumber(String scratchCardNumber) {
		this.scratchCardNumber = scratchCardNumber;
	}

	public String getApiurl() {
		return apiurl;
	}

	public void setApiurl(String apiurl) {
		this.apiurl = apiurl;
	}

	public boolean isOnNet() {
		return onNet;
	}

	public void setOnNet(boolean onNet) {
		this.onNet = onNet;
	}

	public void updateLog(Logs logs) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			String json = mapper.writeValueAsString(logs);
			SendMessageToQueue.sendMessagetoQueue(json);
		} catch (Exception ex) {
			QueueMessageListener.logger.error(Helper.GetException(ex));
		}
	}
}