package com.evampsaanga.b2b.amqimplementationsesb;

public class ESBLogsTable {
	public static final String APP_MENU_TABLE = "esb_logs_app_menu";
	public static final String APP_FAQ_TABLE = "esb_logs_app_faq";
	public static final String LOGIN_TABLE = "esb_logs_app_login";
	public static final String SAVE_CUSTOMER_TABLE = "esb_logs_save_customer";
	public static final String SEND_OTP_TABLE = "esb_logs_app_sendotp";
	public static final String RESEND_OTP_TABLE = "esb_logs_app_resendotp";
	public static final String SINGUP_VERIFY_OTP_TABLE = "esb_logs_app_signup_verify_otp";
	public static final String CHANGE_PASS_TABLE = "esb_logs_app_Change_Password";
	public static final String FORGOT_PASS_TABLE = "esb_logs_app_Forgot_Password";
	public static final String CUSTOMER_DATA_TABLE = "esb_logs_app_customer_data";
	public static final String CONTACT_TABLE = "esb_logs_app_contactus";
	public static final String TARRIF_DETAILS_TABLE = "esb_logs_app_Tarrif_Details";
	public static final String SUPPLEMENTARY_SERVICES_TABLE = "esb_logs_app_Supplementary_Services";
	public static final String Home_PAGE_TABLE = "esb_logs_app_home_page";
	public static final String TOPUP_TABLE = "esb_logs_app_topup";
	public static final String TRANSFER_MONEY_TABLE = "esb_logs_app_transfer_money";
	public static final String LOAN_REQUEST_TABLE = "esb_logs_app_loan_request";
	public static final String LOAN_REQUEST_TABLE_HISTORY = "esb_logs_app_loan_request_history";
	public static final String LOAN_PAYMENT_TABLE_HISTORY = "esb_logs_app_loan_payment_history";
	public static final String GET_FNF_TABLE = "esb_logs_app_get_fnf";
	public static final String GET_CDRSBYDATE_TABLE = "esb_logs_app_cdrs_by_date";
	public static final String GET_CDRSSUMMARY_TABLE = "esb_logs_app_cdrs_summary";
	public static final String GET_CDRSS_OPERATION_HISTORY_TABLE = "esb_logs_app_cdrs_operation_history";
	public static final String MANIPULATE_FNF_TABLE = "esb_logs_app_manipulate_fnf";
	public static final String GETCDRSBYDATE_OTP_TABLE = "esb_logs_app_gercdrsbydate_otp";
	public static final String VERIFYCDRS_OTP_TABLE = "esb_logs_app_verifycdrs_otp";
	public static final String UPDATE_CUSTOMER_EMAIL_TABLE = "esb_logs_app_update_customer_email";
	public static final String GET_STORE_LOCATOR_TABLE = "esb_logs_app_get_store_locator";
	public static final String HLR_QUERY_BALANCE = "esb_logs_app_hlr_query_balance";
	public static final String GET_NOTIFICATIONS = "esb_logs_app_get_notifications";
	public static final String GENERAL_REPORTING = "esb_logs_app_generalreporting";
}
