package com.evampsaanga.b2b.magentoswervices.mysubscriptionofferingids;

import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;
import com.evampsaanga.b2b.azerfon.suplementryservices.Data;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.ALWAYS)
public class MySubscriptionsResponseClient extends BaseResponse {
	@JsonInclude(JsonInclude.Include.ALWAYS)
	com.evampsaanga.b2b.azerfon.suplementryservices.Data data = new Data();

	public com.evampsaanga.b2b.azerfon.suplementryservices.Data getData() {
		return data;
	}

	public void setData(com.evampsaanga.b2b.azerfon.suplementryservices.Data data) {
		this.data = data;
	}
}
