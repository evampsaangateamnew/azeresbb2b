package com.evampsaanga.b2b.magentoswervices.mysubscriptionofferingids;

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;

public class MySubscriptionsRequestClient extends BaseRequest {
	private String offeringIds = "";

	public String getOfferingIds() {
		return offeringIds;
	}

	public void setOfferingIds(String offeringIds) {
		this.offeringIds = offeringIds;
	}
}
