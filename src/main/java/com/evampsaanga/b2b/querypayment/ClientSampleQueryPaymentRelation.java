package com.evampsaanga.b2b.querypayment;

import com.huawei.bme.cbsinterface.bccommon.SubAccessCode;
import com.huawei.bme.cbsinterface.bcservices.QueryPaymentRelationRequest;
import com.huawei.bme.cbsinterface.bcservices.QueryPaymentRelationRequest.PaymentObj;
import com.huawei.bme.cbsinterface.bcservices.QueryPaymentRelationRequestMsg;

public class ClientSampleQueryPaymentRelation {

	public static void main(String[] args) {
	        System.out.println("***********************");
	        System.out.println("Create Web Service Client...");
	        
	        QueryPaymentRelationRequestMsg paymentRelationRequestMsg = new QueryPaymentRelationRequestMsg();
	        
	        paymentRelationRequestMsg.setRequestHeader(BcService.getRequestHeader());
	        QueryPaymentRelationRequest paymentRelationRequest = new QueryPaymentRelationRequest();
	        
	        PaymentObj paymentObj = new PaymentObj();
	        SubAccessCode subAccessCode = new SubAccessCode();
	        subAccessCode.setPrimaryIdentity("555956006");
			paymentObj.setSubAccessCode(subAccessCode);
			paymentRelationRequest.setPaymentObj(paymentObj);
	
			paymentRelationRequestMsg.setQueryPaymentRelationRequest(paymentRelationRequest);
			
			BcService.getInstance().queryPaymentRelation(paymentRelationRequestMsg);
	        System.out.println("***********************");
	        System.out.println("Call Over!");
	}
}
