package com.evampsaanga.b2b.querypayment;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.evampsaanga.b2b.developer.utils.SoapHandlerService;
import com.huawei.bme.cbsinterface.bcservices.BcServices;
import com.huawei.bme.cbsinterface.bcservices.BcServices_Service;
import com.huawei.bme.cbsinterface.cbscommon.OperatorInfo;
import com.huawei.bme.cbsinterface.cbscommon.OwnershipInfo;
import com.huawei.bme.cbsinterface.cbscommon.SecurityInfo;

public class BcService {
	private static BcServices portType = null;

	private BcService() {
	}

	public static synchronized BcServices getInstance() {
		if (portType == null) {
			portType = new BcServices_Service().getBcServicesPort();
		//	SoapHandlerService.configureBinding(portType);
			
			SoapHandlerService.configureBinding(portType);
		}
		return portType;
	}

	public static com.huawei.bme.cbsinterface.cbscommon.RequestHeader getRequestHeader() {
		com.huawei.bme.cbsinterface.cbscommon.RequestHeader requestHeader = new com.huawei.bme.cbsinterface.cbscommon.RequestHeader();
		requestHeader.setVersion("1");
		requestHeader.setBusinessCode("1");
		OwnershipInfo ownershipinf = new OwnershipInfo();
		ownershipinf.setBEID("101");
		ownershipinf.setBRID("101");
		requestHeader.setOwnershipInfo(ownershipinf);
		SecurityInfo value = new SecurityInfo();
		value.setLoginSystemCode("ecare");
		value.setPassword("r8q0a5WwGNboj9I35XzNcQ==");
		requestHeader.setAccessSecurity(value);
		OperatorInfo opInfo = new OperatorInfo();
		opInfo.setOperatorID("101");
		requestHeader.setOperatorInfo(opInfo);
		requestHeader.setMessageSeq(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
		return requestHeader;
	}
}
