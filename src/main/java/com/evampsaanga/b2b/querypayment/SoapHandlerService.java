package com.evampsaanga.b2b.querypayment;

import java.util.List;
import java.util.Map;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.Handler;

public class SoapHandlerService {
	@SuppressWarnings("rawtypes")
	public static <T> void configureBinding(T PortType) {
		Map<String, Object> reqC = ((BindingProvider) PortType).getRequestContext();
		
		BindingProvider bindingProvider = ((BindingProvider) PortType);
		List<Handler> handlerChain = bindingProvider.getBinding().getHandlerChain();
		handlerChain.add(new SOAPLoggingHandler());
		bindingProvider.getBinding().setHandlerChain(handlerChain);
	}
}
