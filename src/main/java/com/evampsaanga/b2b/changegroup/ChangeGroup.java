package com.evampsaanga.b2b.changegroup;

import java.io.IOException;
import java.util.List;

import org.codehaus.jettison.json.JSONObject;

import com.evampsaanga.b2b.azerfon.createorder.clientsample.CreateOrderService;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.huawei.bss.soaif._interface.common.createorder.GroupSubscriberInfo;
import com.huawei.bss.soaif._interface.common.createorder.MemberSubscriberInfo;
import com.huawei.bss.soaif._interface.orderservice.createorder.CreateOrderReqMsg;
import com.huawei.bss.soaif._interface.orderservice.createorder.CreateOrderReqMsg.Order;

public class ChangeGroup 
{
	private List<String> recieverMsisdn;
	private List<String> activeMsisdns;
	private long groupIdFrom;
	private String groupTypeFrom;
	private long groupIdTo;
	private String groupTypeTo;
	private StringBuffer stringBuffer;
	private ChangeGroupAttributes changeGroupAttributes;
	
	
	

	public ChangeGroup(JSONObject jsonObject, StringBuffer stringBuffer) throws IOException, Exception 
	{
		// TODO Auto-generated constructor stub
		this.stringBuffer = stringBuffer;//Helper.JsonToObject(jsonObject.toString(), OrderManagementRequest.class);
		this.changeGroupAttributes = Helper.JsonToObject(jsonObject.toString(), ChangeGroupAttributes.class);
		
		
	}

	
	
	public StringBuffer getStringBuffer() {
		return stringBuffer;
	}



	public void setStringBuffer(StringBuffer stringBuffer) {
		this.stringBuffer = stringBuffer;
	}



	public ChangeGroupAttributes getChangeGroupAttributes() {
		return changeGroupAttributes;
	}



	public void setChangeGroupAttributes(ChangeGroupAttributes changeGroupAttributes) {
		this.changeGroupAttributes = changeGroupAttributes;
	}



	public String getGroupTypeFrom() {
		return groupTypeFrom;
	}

	public void setGroupTypeFrom(String groupTypeFrom) {
		this.groupTypeFrom = groupTypeFrom;
	}

	public String getGroupTypeTo() {
		return groupTypeTo;
	}

	public void setGroupTypeTo(String groupTypeTo) {
		this.groupTypeTo = groupTypeTo;
	}

	public long getGroupIdFrom() {
		return groupIdFrom;
	}

	public void setGroupIdFrom(long groupIdFrom) {
		this.groupIdFrom = groupIdFrom;
	}

	public long getGroupIdTo() {
		return groupIdTo;
	}

	public void setGroupIdTo(long groupIdTo) {
		this.groupIdTo = groupIdTo;
	}

	public List<String> getActiveMsisdns() {
		return activeMsisdns;
	}

	public void setActiveMsisdns(List<String> activeMsisdns) {
		this.activeMsisdns = activeMsisdns;
	}

	public List<String> getRecieverMsisdn() {
		return recieverMsisdn;
	}

	public void setRecieverMsisdn(List<String> recieverMsisdn) {
		this.recieverMsisdn = recieverMsisdn;
	}
	//check msisdn active
	public boolean checkMsisdnActive()
	{
		return false;
		
	}
	//check balance Individual to be positive
	public void checkBalance()
	{
		
	}
	//removing from group
	public void removeFromGroup(long groupID,String msisdn)
	{
		System.out.println("***********************");
        System.out.println("Create Web Service Client...");
        CreateOrderReqMsg createOrderReqMsg = new CreateOrderReqMsg();
        createOrderReqMsg.setReqHeader(CreateOrderService.getCreateOderHeader());
        Order order = new Order();
        order.setOrderType("CG012");
		createOrderReqMsg.setOrder(order );
		
		GroupSubscriberInfo groupSubscriberInfo = new GroupSubscriberInfo();
		groupSubscriberInfo.setGroupId(groupID);//(1010003262919L);
		
		MemberSubscriberInfo memberSubscriberInfo = new MemberSubscriberInfo();
		memberSubscriberInfo.setMemberServiceNumber(msisdn);//("555956002");
		
		createOrderReqMsg.setGroupSubscriber(groupSubscriberInfo);
		createOrderReqMsg.setGroupMember(memberSubscriberInfo);
		
		CreateOrderService.getInstance().createOrder(createOrderReqMsg);

        System.out.println("***********************");
        System.out.println("Call Over!");
	}
	//adding into group
	public void addIntoGroup(long groupID,String msisdn)
	{
		System.out.println("***********************");
        System.out.println("Create Web Service Client...");
        CreateOrderReqMsg createOrderReqMsg = new CreateOrderReqMsg();
        createOrderReqMsg.setReqHeader(CreateOrderService.getCreateOderHeader());
        Order order = new Order();
        order.setOrderType("CG011");
		createOrderReqMsg.setOrder(order );
		
		GroupSubscriberInfo groupSubscriberInfo = new GroupSubscriberInfo();
		groupSubscriberInfo.setGroupId(groupID);//(1010005949215L);
		
		MemberSubscriberInfo memberSubscriberInfo = new MemberSubscriberInfo();
		memberSubscriberInfo.setMemberServiceNumber(msisdn);//("555956002");
		
		createOrderReqMsg.setGroupSubscriber(groupSubscriberInfo);
		createOrderReqMsg.setGroupMember(memberSubscriberInfo);
		
		CreateOrderService.getInstance().createOrder(createOrderReqMsg);

        System.out.println("***********************");
        System.out.println("Call Over!");
	}
	public void processOrder()
	{
		//all active msisdns are saved in List of activeMsisdns
		checkMsisdnActive();
		//iterate loop for change group of msisdns	
		for(int i=0;i<activeMsisdns.size();i++)
		{
			//if(Pay by Sub to Part Pay or Full Pay)
//			if((groupTypeFrom.equals("PayBySub")) && (groupTypeTo.equals("PartPay") || groupTypeTo.equals("FullPay") ) )
//			{
//				////check balance individual for +ive
//				HLRBalanceServices service = new HLRBalanceServices();
//				Balance balance = service.getBalance(activeMsisdns.get(i), "postpaid",null);
//				//check if individual balance is +ive
//				if(Long.parseLong(balance.getPostpaid().getAvailableBalanceIndividualValue())>0)
//				{
//					////remove from groupID
//					removeFromGroup(groupID, activeMsisdns.get(i));
//					////add into groupID
//					addIntoGroup(groupID, activeMsisdns.get(i));
//				}
//				
//				
//				
//			}
//			//else if(Part Pay to Full Pay (Same for vice versa))
//			else if((groupTypeFrom.equalsIgnoreCase("PartPay") && groupTypeTo.equals("FullPay")) || (groupTypeTo.equalsIgnoreCase("PartPay") && groupTypeFrom.equals("FullPay")))
//			{
//
//				////remove from groupID
//				removeFromGroup(groupID, activeMsisdns.get(i));
//				////add into groupID
//				addIntoGroup(groupID, activeMsisdns.get(i));
//				////Add payment relation with values saved in step 1
//				
//				
//			}
//			//else if(Part Pay / Full Pay to Pay By Subs)
//			else if((groupTypeFrom.equalsIgnoreCase("PartPay") || groupTypeFrom.equalsIgnoreCase("FullPay") && (groupTypeTo.equals("PayBySub"))))
//			{
//				////Remove from group
//				removeFromGroup(groupID, activeMsisdns.get(i));
//				////Add to pay by subs group
//				addIntoGroup(groupID, activeMsisdns.get(i));
//				
//			}

				
				

			
		}
		
		
		
		
		
	}
	

}
