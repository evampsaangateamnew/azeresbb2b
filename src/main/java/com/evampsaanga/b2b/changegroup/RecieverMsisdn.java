
package com.evampsaanga.b2b.changegroup;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "msisdn",
    "groupType",
    "groupIdFrom"
})
public class RecieverMsisdn {

    @JsonProperty("msisdn")
    private String msisdn;
    @JsonProperty("groupType")
    private String groupType;
    @JsonProperty("groupIdFrom")
    private String groupIdFrom;

    @JsonProperty("msisdn")
    public String getMsisdn() {
        return msisdn;
    }

    @JsonProperty("msisdn")
    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    @JsonProperty("groupType")
    public String getGroupType() {
        return groupType;
    }

    @JsonProperty("groupType")
    public void setGroupType(String groupType) {
        this.groupType = groupType;
    }

    @JsonProperty("groupIdFrom")
    public String getGroupIdFrom() {
        return groupIdFrom;
    }

    @JsonProperty("groupIdFrom")
    public void setGroupIdFrom(String groupIdFrom) {
        this.groupIdFrom = groupIdFrom;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("msisdn", msisdn).append("groupType", groupType).append("groupIdFrom", groupIdFrom).toString();
    }

}
