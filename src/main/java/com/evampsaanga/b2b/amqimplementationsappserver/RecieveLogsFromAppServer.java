package com.evampsaanga.b2b.amqimplementationsappserver;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.log4j.Logger;

import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.developer.utils.Helper;

/**
 * Class contains methods for receiving messages from APP Server which are
 * processed to be inserted in logs table.
 * 
 * @author EvampSaanga
 *
 */
public class RecieveLogsFromAppServer {
	public static final Logger logger = Logger.getLogger("azerfon-esb");
	private static ConnectionFactory factory = null;
	private static Connection connection = null;
	private static Session session = null;
	private static Destination destination = null;
	private static MessageConsumer consumer = null;
	private static String url = "";
	private static String username = "";
	private static String password = "";
	private static String queueName = "";

	/**
	 * Initialize connection for Queue
	 */
	private static void init() {
		try {
			logger.debug("Initializing queue connection");
			initializeQueueConfiguration();
			factory = new ActiveMQConnectionFactory(url);
			connection = factory.createConnection(username, password);
			connection.start();
			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			destination = session.createQueue(queueName);
		} catch (Exception ex) {
			System.out.println(ex.toString());
			logger.error(Helper.GetException(ex));
		}
	}

	/**
	 * Method to send message to Queue, receives message as input
	 * 
	 * @param queueInputRequest
	 */
	public static void sendMessagetoQueue(String queueInputRequest) {
		try {
			logger.debug("Message received in send message : " + queueInputRequest);
			if (consumer == null) {
				init();
				consumer = session.createConsumer(destination);
				QueueMessageListener msgLis = new QueueMessageListener();
				consumer.setMessageListener(msgLis);
			} else {
				// TODO
			}
		} catch (JMSException e) {System.out.println(e.toString());
			logger.error(Helper.GetException(e));
		}
	}

	/**
	 * Method to initialize all configurations for Queue
	 */
	private static void initializeQueueConfiguration() {
		logger.debug("Initializing APP SERVER queue configuration");
		url = ConfigurationManager.getConfigurationFromCache("app.logs.url");
		username = ConfigurationManager.getConfigurationFromCache("app.logs.username");
		password = ConfigurationManager.getConfigurationFromCache("app.logs.password");
		queueName = ConfigurationManager.getConfigurationFromCache("app.logs.queuename");
	}
}
