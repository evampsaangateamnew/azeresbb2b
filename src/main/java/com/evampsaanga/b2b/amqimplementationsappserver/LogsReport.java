package com.evampsaanga.b2b.amqimplementationsappserver;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.evampsaanga.b2b.amqimplementationsappserver.QueueMessageListener;
import com.evampsaanga.b2b.azerfon.db.DBFactory;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.developer.utils.Helper;

public class LogsReport {

	private String msisdn;
	private String lang;
	private String isCached;
	private String requestTime;
	private String responseTime;
	private String request;
	private String response;
	private String responseCode;
	private String esbRequestTime;
	private String esbResponseTime;
	private String esbRequest;
	private String esbResponse;
	private String esbResponseCode;
	private String esbResponseDescription;
	private String ip;
	private String channel;
	private String transactionName;
	private String transactionID;
	private String thirdParyName;
	private String supplementaryOfferingName;
	private String supplementaryOfferingId;
	private String actionType;
	private String primaryOfferingName;
	private String receiverMsisdn;
	private String mediumForTopUp;
	private String scratchCardNumber;
	private String amount;
	private String createdTimeStamp;
	private String userType;
	private String tariffType;
	private String tariffOfferingId;
	private String uniqueSessionID;
	private String billingLanugage;
	private String callForwardNumber;
	private String accountID;
	private String apiType;

	private String requestDateTime = "";
	private String responseDateTime = "";
	private String responseDescription = "";
	private String thirdPartyName = "";
	private String mediumForTopup = "";
	private String apiurl = "";
	private String onNet = "";
	private String activatedOfferId = "";
	private String tariffId = "";

	// adding appversion for phase 2
	private String appVersion;

	/**
	 * @param msisdn
	 * @param lang
	 * @param isCached
	 * @param requestTime
	 * @param responseTime
	 * @param request
	 * @param response
	 * @param responseCode
	 * @param esbRequestTime
	 * @param esbResponseTime
	 * @param esbRequest
	 * @param esbResponse
	 * @param esbResponseCode
	 * @param esbResponseDescription
	 * @param ip
	 * @param channel
	 * @param transactionName
	 * @param thirdParyName
	 * @param supplementaryOfferingName
	 * @param supplementaryOfferingId
	 * @param actionType
	 * @param primaryOfferingName
	 * @param receiverMsisdn
	 * @param mediumForTopUp
	 * @param scratchCardNumber
	 * @param amount
	 * @param createdTimeStamp
	 * @param userType
	 * @param tariffType
	 * @param tariffOfferingId
	 * @param uniqueSessionID
	 * @param billingLanugage
	 * @param callForwardNumber
	 * @param accountID
	 * @param apiType
	 * @param transactionID
	 */
	public LogsReport() {

		this.msisdn = "";
		this.lang = "";
		this.isCached = "false";
		this.requestTime = "";
		this.responseTime = "";
		this.request = "";
		this.response = "";
		this.responseCode = "";
		this.esbRequestTime = new SimpleDateFormat(Constants.SQL_DATE_FORMAT).format(new Date());
		this.esbResponseTime = new SimpleDateFormat(Constants.SQL_DATE_FORMAT).format(new Date());
		this.esbRequest = "";
		this.esbResponse = "";
		this.esbResponseCode = "";
		this.esbResponseDescription = "";
		this.ip = "";
		this.channel = "";
		this.transactionName = "";
		this.transactionID = "";
		this.thirdParyName = "";
		this.supplementaryOfferingName = "";
		this.supplementaryOfferingId = "";
		this.actionType = "";
		this.primaryOfferingName = "";
		this.receiverMsisdn = "";
		this.mediumForTopUp = "";
		this.scratchCardNumber = "";
		this.amount = "";
		this.createdTimeStamp = "";
		this.userType = "";
		this.tariffType = "";
		this.tariffOfferingId = "";
		this.uniqueSessionID = "";
		this.billingLanugage = "";
		this.callForwardNumber = "";
		this.accountID = "";
		this.apiType = "";
		this.onNet = "";
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getIsCached() {
		return isCached;
	}

	public void setIsCached(String isCached) {
		this.isCached = isCached;
	}

	public String getRequestTime() {
		return requestTime;
	}

	public void setRequestTime(String requestTime) {
		this.requestTime = requestTime;
	}

	public String getResponseTime() {
		return responseTime;
	}

	public void setResponseTime(String responseTime) {
		this.responseTime = responseTime;
	}

	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getEsbRequestTime() {
		return esbRequestTime;
	}

	public void setEsbRequestTime(String esbRequestTime) {
		this.esbRequestTime = esbRequestTime;
	}

	public String getEsbResponseTime() {
		return esbResponseTime;
	}

	public void setEsbResponseTime(String esbResponseTime) {
		this.esbResponseTime = esbResponseTime;
	}

	public String getEsbRequest() {
		return esbRequest;
	}

	public void setEsbRequest(String esbRequest) {
		this.esbRequest = esbRequest;
	}

	public String getEsbResponse() {
		return esbResponse;
	}

	public void setEsbResponse(String esbResponse) {
		this.esbResponse = esbResponse;
	}

	public String getEsbResponseCode() {
		return esbResponseCode;
	}

	public void setEsbResponseCode(String esbResponseCode) {
		this.esbResponseCode = esbResponseCode;
	}

	public String getEsbResponseDescription() {
		return esbResponseDescription;
	}

	public void setEsbResponseDescription(String esbResponseDescription) {
		this.esbResponseDescription = esbResponseDescription;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getTransactionName() {
		return transactionName;
	}

	public void setTransactionName(String transactionName) {
		this.transactionName = transactionName;
	}

	public String getTransactionID() {
		return transactionID;
	}

	public void setTransactionID(String transactionID) {
		this.transactionID = transactionID;
	}

	public String getThirdParyName() {
		return thirdParyName;
	}

	public void setThirdParyName(String thirdParyName) {
		this.thirdParyName = thirdParyName;
	}

	public String getSupplementaryOfferingName() {
		return supplementaryOfferingName;
	}

	public void setSupplementaryOfferingName(String supplementaryOfferingName) {
		this.supplementaryOfferingName = supplementaryOfferingName;
	}

	public String getSupplementaryOfferingId() {
		return supplementaryOfferingId;
	}

	public void setSupplementaryOfferingId(String supplementaryOfferingId) {
		this.supplementaryOfferingId = supplementaryOfferingId;
	}

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public String getPrimaryOfferingName() {
		return primaryOfferingName;
	}

	public void setPrimaryOfferingName(String primaryOfferingName) {
		this.primaryOfferingName = primaryOfferingName;
	}

	public String getReceiverMsisdn() {
		return receiverMsisdn;
	}

	public void setReceiverMsisdn(String receiverMsisdn) {
		this.receiverMsisdn = receiverMsisdn;
	}

	public String getMediumForTopUp() {
		return mediumForTopUp;
	}

	public void setMediumForTopUp(String mediumForTopUp) {
		this.mediumForTopUp = mediumForTopUp;
	}

	public String getScratchCardNumber() {
		return scratchCardNumber;
	}

	public void setScratchCardNumber(String scratchCardNumber) {
		this.scratchCardNumber = scratchCardNumber;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getCreatedTimeStamp() {
		return createdTimeStamp;
	}

	public void setCreatedTimeStamp(String createdTimeStamp) {
		this.createdTimeStamp = createdTimeStamp;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getTariffType() {
		return tariffType;
	}

	public void setTariffType(String tariffType) {
		this.tariffType = tariffType;
	}

	public String getTariffOfferingId() {
		return tariffOfferingId;
	}

	public void setTariffOfferingId(String tariffOfferingId) {
		this.tariffOfferingId = tariffOfferingId;
	}

	public String getUniqueSessionID() {
		return uniqueSessionID;
	}

	public void setUniqueSessionID(String uniqueSessionID) {
		this.uniqueSessionID = uniqueSessionID;
	}

	public String getBillingLanugage() {
		return billingLanugage;
	}

	public void setBillingLanugage(String billingLanugage) {
		this.billingLanugage = billingLanugage;
	}

	public String getCallForwardNumber() {
		return callForwardNumber;
	}

	public void setCallForwardNumber(String callForwardNumber) {
		this.callForwardNumber = callForwardNumber;
	}

	public String getAccountID() {
		return accountID;
	}

	public void setAccountID(String accountID) {
		this.accountID = accountID;
	}

	public String getApiType() {
		return apiType;
	}

	public void setApiType(String apiType) {
		this.apiType = apiType;
	}

	public String getRequestDateTime() {
		return requestDateTime;
	}

	public void setRequestDateTime(String requestDateTime) {
		this.requestDateTime = requestDateTime;
	}

	public String getResponseDateTime() {
		return responseDateTime;
	}

	public void setResponseDateTime(String responseDateTime) {
		this.responseDateTime = responseDateTime;
	}

	public String getResponseDescription() {
		return responseDescription;
	}

	public void setResponseDescription(String responseDescription) {
		this.responseDescription = responseDescription;
	}

	public String getThirdPartyName() {
		return thirdPartyName;
	}

	public void setThirdPartyName(String thirdPartyName) {
		this.thirdPartyName = thirdPartyName;
	}

	public String getMediumForTopup() {
		return mediumForTopup;
	}

	public void setMediumForTopup(String mediumForTopup) {
		this.mediumForTopup = mediumForTopup;
	}

	public String getApiurl() {
		return apiurl;
	}

	public void setApiurl(String apiurl) {
		this.apiurl = apiurl;
	}

	public String getOnNet() {
		return onNet;
	}

	public void setOnNet(String onNet) {
		this.onNet = onNet;
	}

	public String getActivatedOfferId() {
		return activatedOfferId;
	}

	public void setActivatedOfferId(String activatedOfferId) {
		this.activatedOfferId = activatedOfferId;
	}

	public String getTariffId() {
		return tariffId;
	}

	public void setTariffId(String tariffId) {
		this.tariffId = tariffId;
	}

	public String getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}

	@Override
	public String toString() {
		return "LogsReport [msisdn=" + msisdn + ", lang=" + lang + ", isCached=" + isCached + ", requestTime="
				+ requestTime + ", responseTime=" + responseTime + ", request=" + request + ", response=" + response
				+ ", responseCode=" + responseCode + ", esbRequestTime=" + esbRequestTime + ", esbResponseTime="
				+ esbResponseTime + ", esbRequest=" + esbRequest + ", esbResponse=" + esbResponse + ", esbResponseCode="
				+ esbResponseCode + ", esbResponseDescription=" + esbResponseDescription + ", ip=" + ip + ", channel="
				+ channel + ", transactionName=" + transactionName + ", transactionID=" + transactionID
				+ ", thirdParyName=" + thirdParyName + ", supplementaryOfferingName=" + supplementaryOfferingName
				+ ", supplementaryOfferingId=" + supplementaryOfferingId + ", actionType=" + actionType
				+ ", primaryOfferingName=" + primaryOfferingName + ", receiverMsisdn=" + receiverMsisdn
				+ ", mediumForTopUp=" + mediumForTopUp + ", scratchCardNumber=" + scratchCardNumber + ", amount="
				+ amount + ", createdTimeStamp=" + createdTimeStamp + ", userType=" + userType + ", tariffType="
				+ tariffType + ", tariffOfferingId=" + tariffOfferingId + ", uniqueSessionID=" + uniqueSessionID
				+ ", billingLanugage=" + billingLanugage + ", callForwardNumber=" + callForwardNumber + ", accountID="
				+ accountID + ", apiType=" + apiType + ", requestDateTime=" + requestDateTime + ", responseDateTime="
				+ responseDateTime + ", responseDescription=" + responseDescription + ", thirdPartyName="
				+ thirdPartyName + ", mediumForTopup=" + mediumForTopup + ", apiurl=" + apiurl + ", onNet=" + onNet
				+ ", activatedOfferId=" + activatedOfferId + ", tariffId=" + tariffId + ", appVersion=" + appVersion
				+ "]";
	}

	public static String getQueryFromPrepareStatement(java.sql.PreparedStatement preparedStatement) {
		return preparedStatement.toString().split(":")[1];
	}

	public void insertLogsIntoDB(LogsReport logsReport) throws SQLException, ClassNotFoundException {

		String queryStr = "insert into appserver_reports_from_esb (msisdn,ip,channel,lang,isCached,requestTime,responseTime,request,response,responseCode,esbRequestTime,esbResponseTime,esbRequest,esbResponse,esbResponseCode,esbResponseDescription,transactionName,thirdPartyName,supplementaryOfferingName,supplementaryOfferingId,actionType,primaryOfferingName,tariffOfferingId,receiverMsisdn,mediumForTopup,scratchCardNumber,amount,createdTimeStamp,userType,tariffType,uniqueSessionId,billingLanguage,callForwardNumber,accountId,apiType) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		java.sql.Connection connection = null;
		java.sql.PreparedStatement preparedStatement = null;
		try {
			connection = DBFactory.getAppConnection();
			preparedStatement = connection.prepareStatement(queryStr);
			preparedStatement.setString(1, logsReport.getMsisdn());
			preparedStatement.setString(2, logsReport.getIp());
			preparedStatement.setString(3, logsReport.getChannel());
			preparedStatement.setString(4, logsReport.getLang());
			preparedStatement.setString(5, logsReport.getIsCached());
			preparedStatement.setString(6, logsReport.getRequestTime());
			preparedStatement.setString(7, logsReport.getResponseTime());
			preparedStatement.setString(8, logsReport.getRequest());
			preparedStatement.setString(9, logsReport.getResponse());
			preparedStatement.setString(10, logsReport.getResponseCode());
			preparedStatement.setString(11, logsReport.getEsbRequestTime());
			preparedStatement.setString(12, logsReport.getEsbResponseTime());
			preparedStatement.setString(13, logsReport.getEsbRequest());
			preparedStatement.setString(14, logsReport.getEsbResponse());
			preparedStatement.setString(15, logsReport.getEsbResponseCode());
			preparedStatement.setString(16, logsReport.getEsbResponseDescription());
			preparedStatement.setString(17, logsReport.getTransactionID());
			preparedStatement.setString(18, logsReport.getThirdParyName());
			preparedStatement.setString(19, logsReport.getSupplementaryOfferingName());
			preparedStatement.setString(20, logsReport.getSupplementaryOfferingId());
			preparedStatement.setString(21, logsReport.getActionType());
			preparedStatement.setString(22, logsReport.getPrimaryOfferingName());
			preparedStatement.setString(23, logsReport.getTariffOfferingId());
			preparedStatement.setString(24, logsReport.getReceiverMsisdn());
			preparedStatement.setString(25, logsReport.getMediumForTopUp());
			preparedStatement.setString(26, logsReport.getScratchCardNumber());
			preparedStatement.setString(27, logsReport.getAmount());
			preparedStatement.setString(28, logsReport.getCreatedTimeStamp());
			preparedStatement.setString(29, logsReport.getUserType());
			preparedStatement.setString(30, logsReport.getTariffType());
			preparedStatement.setString(31, logsReport.getUniqueSessionID());
			preparedStatement.setString(32, logsReport.getBillingLanugage());
			preparedStatement.setString(33, logsReport.getCallForwardNumber());
			preparedStatement.setString(34, logsReport.getAccountID());
			preparedStatement.setString(35, logsReport.getApiType());
			// preparedStatement.setString(36, logsReport.getAppVersion());

			QueueMessageListener.logger.info(logsReport.getMsisdn() + "-REPORT APP SERVER (TRANSACTION:"
					+ logsReport.getTransactionName() + ") QUERY:" + getQueryFromPrepareStatement(preparedStatement));

			QueueMessageListener.logger.info(
					logsReport.getMsisdn() + "-REPORT APP SERVER INSERT RESULT:" + preparedStatement.executeUpdate());
		} catch (Exception ex) {
			QueueMessageListener.logger.error(Helper.GetException(ex));
		} finally {
			if (preparedStatement != null)
				preparedStatement.close();
		}

	}
}