package com.evampsaanga.b2b.amqimplementationsappserver;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.apache.log4j.Logger;

import com.evampsaanga.b2b.developer.utils.Helper;

/**
 * Class implements MessageListener and overrides methods which are called on
 * new message in queue
 * 
 * @author EvampSaanga
 *
 */
public class QueueMessageListener implements MessageListener {
	public static Logger logger = null;

	// public static final Logger loggerV2 = Logger.getLogger("azerfon-esb");
	/**
	 * Overrides interface method to receive new entry in queue
	 */
	@Override
	public void onMessage(Message MessagefromQueue) {
		try {
			LogsReport logs = null;
			if (MessagefromQueue instanceof TextMessage) {
				TextMessage msg = (TextMessage) MessagefromQueue;
				if (msg.getText().contains("B2B")) {
					logger = Logger.getLogger("azerfon-esb");
					logger.info("Message Came To APP AMQ consumer" + msg.getText());
				} else {
					logger = Logger.getLogger("azerfon-esb");
					logger.info("Message Came To APP AMQ consumer" + msg.getText());
				}
				logs = Helper.JsonToObject(msg.getText(), LogsReport.class);
				logs.insertLogsIntoDB(logs);

			}
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		logger.debug("Request Completed for App Server");

	}
}
