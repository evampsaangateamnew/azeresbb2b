package com.evampsaanga.azerfon.appserver.refreshappservercache;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;

import com.evampsaanga.b2b.azerfon.db.DBFactory;
import com.evampsaanga.b2b.azerfon.getUsersV2.UsersGroupData;
import com.evampsaanga.b2b.azerfon.grouppermission.Datum;
import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.client.config.ClientNetworkConfig;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;

@Path("/user")
public class BuildCacheRequestLand {
	public static final Logger logger = Logger.getLogger("azerfon-esb");

//	public static HashMap<String, CustomerModelCache> customerCache;
	public static IMap<String, CustomerModelCache> customerCache;
	public static ClientConfig clientConfig = null;
	public static HazelcastInstance client = null;
	static ClientNetworkConfig clientNetworkConfig = null;
	
	public static IMap<String, String> configurationCache;
	public static IMap<String, List<UsersGroupData>> usersCache;
	public static IMap<String, Datum> groupRestrictionCache;
	

	@POST
	@Path("/build")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String Get(@Header("credentials") String credential, @Body() String requestBody) {
//		String query = "select customer_entity.entity_id,customer_entity.password_hash,customer_entity.email, customer_entity_varchar.`value` msisdn from customer_entity, customer_entity_varchar WHERE customer_entity.entity_id=customer_entity_varchar.entity_id and customer_entity_varchar.attribute_id=212 AND customer_entity.group_id=1";
//		
//		PreparedStatement preparedStatement;
//		try {
//			preparedStatement = DBFactory.getMagentoDBConnection().prepareStatement(query);
//		
//		ResultSet resultSet = preparedStatement.executeQuery();
//		while(resultSet.next())
//		{
//			CustomerModelCache customerModelCache = new CustomerModelCache();
//			customerModelCache.setEmail(resultSet.getString("email"));
//			customerModelCache.setEntity_id(resultSet.getString("entity_id"));
//			customerModelCache.setMsisdn(resultSet.getString("msisdn"));
//			customerModelCache.setPassword_hash(resultSet.getString("password_hash"));
//			
//			customerCache.put(customerModelCache.getMsisdn(), customerModelCache);
//			//-------------------------------------- HAZLECAST -------------------------
//			Helper.loadCache();
//			//-------------------------------------- END -------------------------------			
//		}
//		}catch (Exception e) {
//			logger.error(Helper.GetException(e));
//		}
//		return "";

		logger.info("***********************************************");
		logger.info("Loading cache for customers");

		if (customerCache == null||configurationCache==null||usersCache==null || groupRestrictionCache == null)
			initHazelcast();

//		cacheMap = client.getMap("cacheMap");

		//

		if (customerCache != null && customerCache.size() > 0)
			customerCache.clear();
		String query = "SELECT entity_id, password_hash, email, sum(msisdn) msisdn, sum(customerid) customerid FROM ( SELECT customer_entity.entity_id, customer_entity.password_hash, customer_entity.email, ( CASE WHEN ( `customer_entity_varchar`.`attribute_id` = '212' ) THEN `customer_entity_varchar`.`value` END ) AS `msisdn`, ( CASE WHEN ( `customer_entity_varchar`.`attribute_id` = '582' ) THEN `customer_entity_varchar`.`value` END ) AS `customerid` FROM customer_entity, customer_entity_varchar WHERE customer_entity.entity_id = customer_entity_varchar.entity_id AND customer_entity_varchar.attribute_id IN (212, 582) AND customer_entity.group_id = 1 ) AS customer GROUP BY entity_id, password_hash,email";

		PreparedStatement preparedStatement;
		try {
			preparedStatement = DBFactory.getMagentoDBConnection().prepareStatement(query);

			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				CustomerModelCache customerModelCache = new CustomerModelCache();
				customerModelCache.setEmail(resultSet.getString("email"));
				customerModelCache.setEntity_id(resultSet.getString("entity_id"));
				customerModelCache.setMsisdn(resultSet.getString("msisdn"));
				customerModelCache.setPassword_hash(resultSet.getString("password_hash"));
				customerModelCache.setCustomerId(resultSet.getString("customerid"));
				customerModelCache.setIsFromDB("false");
//			customerCache.put(customerModelCache.getMsisdn(), customerModelCache);
				customerCache.put(customerModelCache.getMsisdn(), customerModelCache);
			}
		} catch (Exception e) {
			logger.error(Helper.GetException(e));
		}
		logger.info("Map Size:" + customerCache.size());
		logger.info("***********************************************");
		return "";

	}

	public static void initHazelcast() {
//		clientConfig = new ClientConfig();
//		String ipAddress= Constants.CACHE_IP_ADDRESS;
//		logger.info("IP:"+ipAddress);
//		clientNetworkConfig=new ClientNetworkConfig().addAddress(ipAddress);
//		clientNetworkConfig.setConnectionAttemptLimit(0).setConnectionTimeout(1);
//		clientConfig.setNetworkConfig(clientNetworkConfig);
//		customerCache = instance.getMap("customers");

		clientConfig = new ClientConfig();
//		String ipAddress = ConfigurationManager.getConfigurationFromCache("esb.logs.esb.hazlecastcache.ip");// Constants.CACHE_IP_ADDRESS;
		String ipAddress=ConfigurationManager.getDBProperties("hazelCastIp");
		logger.info("IP:" + ipAddress);
		ClientNetworkConfig clientNetworkConfig = new ClientNetworkConfig().addAddress(ipAddress);
		clientNetworkConfig.setConnectionAttemptLimit(0).setConnectionTimeout(1);
		clientConfig.setNetworkConfig(clientNetworkConfig);
		client = HazelcastClient.newHazelcastClient(clientConfig);
		customerCache = client.getMap("azercustomers");
		configurationCache=client.getMap("middlewareconfiguration");
		usersCache=client.getMap("usersCache");
		groupRestrictionCache = client.getMap("groupRestrictionCache");
	}

	public static CustomerModelCache getCustomerModel(String msisdn) {
		if (customerCache.containsKey(msisdn)) {
			return customerCache.get(msisdn);
		} else if (Helper.checkMsisdnExistInDB(msisdn)) {
			return customerCache.get(msisdn);
		} else
			return null;
	}

//	public static void initConfigurationHazelcast() {
//		// TODO Auto-generated method stub
//		clientConfig = new ClientConfig();
//		String ipAddress = ConfigurationManager.getConfigurationFromCache("esb.logs.esb.hazlecastcache.ip");// Constants.CACHE_IP_ADDRESS;
//		// String ipAddress="10.220.48.135:5701";
//		logger.info("IP:" + ipAddress);
//		ClientNetworkConfig clientNetworkConfig = new ClientNetworkConfig().addAddress(ipAddress);
//		clientNetworkConfig.setConnectionAttemptLimit(0).setConnectionTimeout(1);
//		clientConfig.setNetworkConfig(clientNetworkConfig);
//		client = HazelcastClient.newHazelcastClient(clientConfig);
//		configurationCache=client.getMap("middlewareconfiguration");
//	}
	
//	public static String getMiddleWareConfiguration(String key) {
//		if (configurationCache.containsKey(key)) {
//			return configurationCache.get(key);
//		} else if (Helper.fetchConfigurationExistInDB(key)) {
//			return configurationCache.get(key);
//		} else
//			return null;
//	}

}
