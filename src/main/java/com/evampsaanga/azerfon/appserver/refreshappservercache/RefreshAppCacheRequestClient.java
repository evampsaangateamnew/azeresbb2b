package com.evampsaanga.azerfon.appserver.refreshappservercache;

import com.evampsaanga.b2b.azerfon.requestheaders.BaseRequest;

/**
 * Request model class for refreshing APP Server cache
 * 
 * @author EvampSaanga
 *
 */
public class RefreshAppCacheRequestClient extends BaseRequest {
	private String cacheType = "";

	public String getCacheType() {
		return cacheType;
	}

	public void setCacheType(String cacheType) {
		this.cacheType = cacheType;
	}
}
