/**
 * 
 */
package com.evampsaanga.azerfon.appserver.refreshappservercache;

import java.io.IOException;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;

import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.amqimplementationsesb.LogsType;
import com.evampsaanga.b2b.azerfon.grouppermission.Datum;
import com.evampsaanga.b2b.azerfon.grouppermission.GroupPermissionMagentoResponse;
import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.ThirdPartyNames;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Decrypter;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.evampsaanga.b2b.refreshcache.RefreshCacheRequest;
import com.evampsaanga.b2b.refreshcache.RefreshCacheResponse;
import com.saanga.magento.apiclient.RestClient;

/**
 * @author HamzaFarooque
 *
 */
@Path("/restrictionCacheManagement")
public class GroupRestrictionCacheManager {

	public static final Logger logger = Logger.getLogger("azerfon-esb");

	// This refresh Cache is sepeate because it is only used in b2b and relaod
	// mechanism of group restriction is
	// only in B2b that why it is has seperate route

	@POST
	@Path("/refreshRestrictionCache")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public RefreshCacheResponse refreshRestrictionCache(@Header("credentials") String credential,
			@Body() String requestBody) {

		logger.info("Refreh ESB Group Restriction Cache Request: " + requestBody);
		Logs logs = new Logs();
		logs.setTransactionName(Transactions.REFRESH_ESB_CACHE_NAME);
		logs.setThirdPartyName(ThirdPartyNames.INTERNAL_CACHE);
		logs.setTableType(LogsType.Login);
		RefreshCacheRequest cclient = null;
		RefreshCacheResponse resp = new RefreshCacheResponse();
		try {
			cclient = Helper.JsonToObject(requestBody, RefreshCacheRequest.class);
			logs.setIp(cclient.getiP());
			logs.setChannel(cclient.getChannel());
			logs.setMsisdn(cclient.getmsisdn());
			logs.setLang(cclient.getLang());
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
			resp.setReturnCode(ResponseCodes.ERROR_400_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_400);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}

		String credentials = null;
		try {
			credentials = Decrypter.getInstance().decrypt(credential);
		} catch (Exception ex) {
			logger.error(Helper.GetException(ex));
		}
		if (credentials == null) {
			resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_401);
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		if (cclient.getmsisdn() != null && !cclient.getmsisdn().isEmpty()) {
			String verification = Helper.validateRequest(cclient);
			if (!verification.equals("")) {
				resp.setReturnCode(ResponseCodes.ERROR_400);
				resp.setReturnMsg(verification);
				logs.setResponseCode(resp.getReturnCode());
				logs.setResponseDescription(resp.getReturnMsg());
				logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
				logs.updateLog(logs);
				return resp;
			}
		} else {
			resp.setReturnCode(ResponseCodes.ERROR_MSISDN_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_MSISDN);
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.updateLog(logs);
			return resp;
		}
		if (credentials != null && credentials.equals(Constants.CREDENTIALS)) {

			logger.info("Refreshing Group Restriction Cache");
			if (BuildCacheRequestLand.groupRestrictionCache != null)
				BuildCacheRequestLand.groupRestrictionCache.clear();
			reloadGroupRestrictionCache();

			resp.setReturnCode(ResponseCodes.SUCESS_CODE_200);
			resp.setReturnMsg(ResponseCodes.SUCESS_DES_200);
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.updateLog(logs);
			return resp;

		} else {
			resp.setReturnCode(ResponseCodes.ERROR_401_CODE);
			resp.setReturnMsg(ResponseCodes.ERROR_401);
			logs.setResponseDateTime(Helper.GenerateDateTimeToMsAccuracy());
			logs.setResponseCode(resp.getReturnCode());
			logs.setResponseDescription(resp.getReturnMsg());
			logs.updateLog(logs);
			return resp;
		}
	}

	public static Datum getGroupRestrictionFromCache(String key) throws IOException, Exception {

		if (BuildCacheRequestLand.groupRestrictionCache == null)
			BuildCacheRequestLand.initHazelcast();

		logger.debug("Message against key: "+ key + " returned from cache.");
		
		if (BuildCacheRequestLand.groupRestrictionCache.containsKey(key)) {

			logger.debug("Message against key: "+ key + " returned from cache.");
			
			
			logger.debug("Group Restriction from cache against key: "+ key + " ::" + Helper.ObjectToJson(BuildCacheRequestLand.groupRestrictionCache.get(key)));
			return BuildCacheRequestLand.groupRestrictionCache.get(key);

		} else {

			logger.debug("Message against key: "+ key + " returned from cache.");

			callMagentoForGroupRestriction(key);

			logger.debug("Cache check in HashMap after Magento Call"
					+ BuildCacheRequestLand.groupRestrictionCache.containsKey(key));

			if (BuildCacheRequestLand.groupRestrictionCache.containsKey(key))
				return BuildCacheRequestLand.groupRestrictionCache.get(key);
			else
				return null;
		}
	}

	public static void reloadGroupRestrictionCache() {

		if (BuildCacheRequestLand.groupRestrictionCache == null)
			BuildCacheRequestLand.initHazelcast();
		if (BuildCacheRequestLand.groupRestrictionCache != null
				&& BuildCacheRequestLand.groupRestrictionCache.size() == 0) {
			GroupPermissionMagentoResponse magentoresponse = new GroupPermissionMagentoResponse();
			try {
				JSONObject jsonMagentoPermissionObject = new JSONObject();
				jsonMagentoPermissionObject.put("groupId", "");

				logger.info("Sending call to magento for all Group Restrictions and REQUEST is : "
						+ jsonMagentoPermissionObject.toString());

				String mresponse = RestClient.SendCallToMagento("",
						ConfigurationManager.getConfigurationFromCache("magento.app.grouppermission"),
						jsonMagentoPermissionObject.toString());

				logger.info("----- Response from magento restrictions api: " + mresponse);

				magentoresponse = Helper.JsonToObject(mresponse, GroupPermissionMagentoResponse.class);
				for (Datum data : magentoresponse.getData())
					BuildCacheRequestLand.groupRestrictionCache.put(data.getGroupId(), data);
			} catch (Exception e) {
				logger.info(Helper.GetException(e));
			}
		}
	}

	private static void callMagentoForGroupRestriction(String key) {

		GroupPermissionMagentoResponse magentoresponse = new GroupPermissionMagentoResponse();
		try {
			// Get Permissions call
			JSONObject jsonMagentoPermissionObject = new JSONObject();
			jsonMagentoPermissionObject.put("groupId", key);

			logger.info("Sending call to magento for Group Restrictions and REQUEST is :"
					+ jsonMagentoPermissionObject.toString());

			String mresponse = RestClient.SendCallToMagento("",
					ConfigurationManager.getConfigurationFromCache("magento.app.grouppermission"),
					jsonMagentoPermissionObject.toString());

			logger.info("----- Response from magento restrictions api: " + mresponse);

			magentoresponse = Helper.JsonToObject(mresponse, GroupPermissionMagentoResponse.class);

			if (magentoresponse.getData() != null && !magentoresponse.getData().isEmpty() )
				
				for (int i = 0; i < magentoresponse.getData().size(); i++) {
					BuildCacheRequestLand.groupRestrictionCache.put(magentoresponse.getData().get(i).getGroupId(), magentoresponse.getData().get(i));
				}
				

		} catch (Exception e) {
			logger.info(Helper.GetException(e));
		}
	}
}
