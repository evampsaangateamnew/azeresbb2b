package com.evampsaanga.azerfon.appserver.refreshappservercache;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.evampsaanga.b2b.amqimplementationsesb.Logs;
import com.evampsaanga.b2b.amqimplementationsesb.LogsType;
import com.evampsaanga.b2b.azerfon.db.DBFactory;
import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.configs.Constants;
import com.evampsaanga.b2b.configs.ResponseCodes;
import com.evampsaanga.b2b.configs.ThirdPartyNames;
import com.evampsaanga.b2b.configs.Transactions;
import com.evampsaanga.b2b.developer.utils.Decrypter;
import com.evampsaanga.b2b.developer.utils.Helper;
import com.evampsaanga.b2b.developer.utils.SOAPLoggingHandler;
import com.saanga.magento.apiclient.RestClient;

@Path("/user")
public class DeleteCustomerFromCache {
	public static final Logger logger = Logger.getLogger("azerfon-esb");

//	public static HashMap<String, CustomerModelCache> customerCache = new HashMap<>();

	
	
	
	@POST
	@Path("/delete")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String Get(@Body() String requestBody) {

		if(BuildCacheRequestLand.customerCache == null)
			BuildCacheRequestLand.initHazelcast();
		try {
			JSONObject jsonObject = new JSONObject(requestBody);
			if (BuildCacheRequestLand.customerCache.containsKey(jsonObject.getString("msisdn"))) {
				BuildCacheRequestLand.customerCache.remove(jsonObject.getString("msisdn"));
				return "{\"result\":\"success\"}";
			} else {
				return "{\"result\":\"Number Does not exists in cache\"}";
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "{\"result\":\"failed\"}";
		}
	}

	@POST
	@Path("/cacheinformation")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DeleteCustomerFromCacheResponse GetUserCache(@Body() String requestBody) {
		DeleteCustomerFromCacheResponse response = new DeleteCustomerFromCacheResponse();
		try {
			JSONObject jsonObject = new JSONObject(requestBody);

			if(BuildCacheRequestLand.customerCache == null)
				BuildCacheRequestLand.initHazelcast();
			if (BuildCacheRequestLand.customerCache.containsKey(jsonObject.getString("msisdn"))) {

				CustomerModelCache customerModelCache = BuildCacheRequestLand.customerCache
						.get(jsonObject.getString("msisdn"));
				response.setReturnCode(ResponseCodes.SUCESS_CODE_200);
				response.setReturnMsg(ResponseCodes.SUCESS_DES_200);
				response.setEmail(customerModelCache.getEmail());
				response.setEntity_id(customerModelCache.getEntity_id());
				response.setPassword_hash((customerModelCache.getPassword_hash()));
				response.setMsisdn(customerModelCache.getMsisdn());
				response.setCustomerId(customerModelCache.getCustomerId());
				response.setIsFromDB(customerModelCache.getIsFromDB());

				return response;
			} else {
				response.setReturnCode(ResponseCodes.USER_NOT_FOUND_CODE);
				response.setReturnMsg(ResponseCodes.USER_NOT_FOUND_DESCRIPTION + " In Chache");
				return response;
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(Helper.GetException(e));
			response.setReturnCode(ResponseCodes.GENERIC_ERROR_CODE);
			response.setReturnMsg(ResponseCodes.GENERIC_ERROR_DES);
			return response;
		}

	}
	
	@POST
	@Path("/update")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DeleteCustomerFromCacheResponse updateCache(@Body() String requestBody) 
	{
		DeleteCustomerFromCacheResponse response = new DeleteCustomerFromCacheResponse();
		try 
		{
			JSONObject jsonObject = new JSONObject(requestBody);

			if(BuildCacheRequestLand.customerCache == null)
				BuildCacheRequestLand.initHazelcast();
			

			if(Helper.checkMsisdnExistInDB(jsonObject.getString("msisdn")))
			{
				response.setReturnCode(ResponseCodes.SUCESS_CODE_200);
				response.setReturnMsg(ResponseCodes.SUCESS_DES_200);
				response.setEmail(BuildCacheRequestLand.customerCache.get(jsonObject.getString("msisdn")).getEmail());
				response.setEntity_id(BuildCacheRequestLand.customerCache.get(jsonObject.getString("msisdn")).getEntity_id());
				response.setPassword_hash(BuildCacheRequestLand.customerCache.get(jsonObject.getString("msisdn")).getPassword_hash());
				response.setMsisdn(BuildCacheRequestLand.customerCache.get(jsonObject.getString("msisdn")).getMsisdn());
				response.setCustomerId(BuildCacheRequestLand.customerCache.get(jsonObject.getString("msisdn")).getCustomerId());
				response.setIsFromDB(BuildCacheRequestLand.customerCache.get(jsonObject.getString("msisdn")).getIsFromDB());
			}
			else 
			{
				response.setReturnCode(ResponseCodes.USER_NOT_FOUND_CODE);
				response.setReturnMsg(ResponseCodes.USER_NOT_FOUND_DESCRIPTION + " In Chache");
				
			}
				
			return response;
			 

		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(Helper.GetException(e));
			response.setReturnCode(ResponseCodes.GENERIC_ERROR_CODE);
			response.setReturnMsg(ResponseCodes.GENERIC_ERROR_DES);
			return response;
		}

	}
	
	
	
	
	
	
	// String query = "select
	// customer_entity.entity_id,customer_entity.password_hash,customer_entity.email,
	// customer_entity_varchar.`value` msisdn from customer_entity,
	// customer_entity_varchar WHERE
	// customer_entity.entity_id=customer_entity_varchar.entity_id and
	// customer_entity_varchar.attribute_id=212 AND customer_entity.group_id=1";
	//
	// PreparedStatement preparedStatement;
	// try {
	// preparedStatement =
	// DBFactory.getMagentoDBConnection().prepareStatement(query);
	//
	// ResultSet resultSet = preparedStatement.executeQuery();
	// while(resultSet.next())
	// {
	// CustomerModelCache customerModelCache = new CustomerModelCache();
	// customerModelCache.setEmail(resultSet.getString("email"));
	// customerModelCache.setEntity_id(resultSet.getString("entity_id"));
	// customerModelCache.setMsisdn(resultSet.getString("msisdn"));
	// customerModelCache.setPassword_hash(resultSet.getString("password_hash"));
	//
	// customerCache.put(customerModelCache.getMsisdn(), customerModelCache);
	//
	// }
	// }catch (Exception e) {
	// logger.error(Helper.GetException(e));
	// }
	// return "";

}
