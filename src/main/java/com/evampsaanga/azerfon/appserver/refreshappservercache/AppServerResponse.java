package com.evampsaanga.azerfon.appserver.refreshappservercache;

/**
 * Response model class for refreshing app server cache request
 * 
 * @author EvampSaanga
 *
 */
public class AppServerResponse {
	private String callStatus = "";
	private String resultCode = "";
	private String resultDesc = "";

	public String getCallStatus() {
		return callStatus;
	}

	public void setCallStatus(String callStatus) {
		this.callStatus = callStatus;
	}

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public String getResultDesc() {
		return resultDesc;
	}

	public void setResultDesc(String resultDesc) {
		this.resultDesc = resultDesc;
	}
}
