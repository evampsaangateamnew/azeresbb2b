package com.evampsaanga.azerfon.appserver.refreshappservercache;

import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;

public class RefreshAppCacheResponseClient extends BaseResponse {
	
	String appServer1="";
	String appServer2="";
	
	public String getAppServer1() {
		return appServer1;
	}
	public void setAppServer1(String appServer1) {
		this.appServer1 = appServer1;
	}
	public String getAppServer2() {
		return appServer2;
	}
	public void setAppServer2(String appServer2) {
		this.appServer2 = appServer2;
	}
}
