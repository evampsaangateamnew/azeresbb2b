package com.evampsaanga.azerfon.appserver.refreshappservercache;

import com.evampsaanga.b2b.azerfon.responseheaders.BaseResponse;

public class DeleteCustomerFromCacheResponse extends BaseResponse {
	
	private String msisdn;
	private String entity_id;
	private String email;
	private String password_hash;
	private String customerId;
	private String isFromDB;
	
	
	public String getIsFromDB() {
		return isFromDB;
	}
	public void setIsFromDB(String isFromDB) {
		this.isFromDB = isFromDB;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	public String getEntity_id() {
		return entity_id;
	}
	public void setEntity_id(String entity_id) {
		this.entity_id = entity_id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword_hash() {
		return password_hash;
	}
	public void setPassword_hash(String password_hash) {
		this.password_hash = password_hash;
	}

}
