package com.saanga.magento.apiclient;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import javax.net.ssl.HttpsURLConnection;
import org.apache.log4j.Logger;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;

import org.codehaus.jettison.json.JSONObject;

import com.evampsaanga.b2b.configs.ConfigurationManager;
import com.evampsaanga.b2b.developer.utils.Helper;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.security.cert.X509Certificate;

/**
 * Class to implement Rest Client for Magento Services and Future integrations
 * will be added here too.
 * 
 * @author Adeel
 * 
 */
public class RestClient {

	public static final Logger logger = Logger.getLogger("azerfon-esb");

	public static String sendCallToAzerfonNarTvSubscribe(String token, String paramUrl, String data,
			String managerToken) throws Exception {

		logger.info(token + "-Rest Client-Request URL-" + paramUrl);

		try {
			// Create a trust manager that does not validate certificate chains
			TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
					return null;
				}

				public void checkClientTrusted(X509Certificate[] certs, String authType) {
				}

				public void checkServerTrusted(X509Certificate[] certs, String authType) {
				}
			} };
			// Install the all-trusting trust manager
			final SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
			// Create all-trusting host name verifier
			HostnameVerifier allHostsValid = new HostnameVerifier() {
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			};
			// Install the all-trusting host verifier
			HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

			URL url = new URL(paramUrl);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("X-Auth-Token", managerToken);
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setDoOutput(true);
			Integer timeout = Integer.parseInt("20000");
			connection.setConnectTimeout(timeout);
			connection.setReadTimeout(timeout);
			connection.setInstanceFollowRedirects(false);
			HttpsURLConnection.setFollowRedirects(false);
			byte[] outputInBytes = data.getBytes("UTF-8");
			OutputStream os = connection.getOutputStream();
			os.write(outputInBytes);
			os.close();
			if ((connection.getResponseCode() != 200)) {
				BufferedReader bin = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
				StringBuffer sb = new StringBuffer();
				String line;
				while ((line = bin.readLine()) != null) {
					sb.append(line);
				}
				logger.info(token + "-Rest Client-Response-" + sb.toString());
				return sb.toString();
			}
			if ((connection.getResponseCode() == 200)) {
				BufferedReader bin = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				StringBuffer sb = new StringBuffer();
				String line;
				while ((line = bin.readLine()) != null) {
					sb.append(line);
				}
				logger.info(token + "-Rest Client-Response-" + sb.toString());
				return sb.toString();
			}
		} catch (Exception e) {

		}
		return "";
	}

	public static String SendCallToMagento(String token, String url, String input) throws Exception {

		logger.info(token + "Rest Client: Magento URL: " + url);
		logger.info(token + "Rest Client: Magento Request: " + input);
		url = ConfigurationManager.getConfigurationFromCache("magento.app.baseurl") + url;
		logger.info(token + "Rest Client: Magento Complete URL: " + url);
		if (url.startsWith("http") && !url.startsWith("https")) {
			HttpURLConnection connection = null;
			URL urlCon = new URL(url);
			connection = (HttpURLConnection) urlCon.openConnection();
			connection.setConnectTimeout(5000);
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json");
			// connection.setRequestProperty("credentials","RQ/Rjcf8Gn2+7srAiXNO0ZGuRnJORSir");
			connection.setRequestProperty("User-Agent", "ESB");
			connection.setDoOutput(true);
			DataOutputStream writer = new DataOutputStream(connection.getOutputStream());
			writer.write(input.getBytes(StandardCharsets.UTF_8));
			String responseString = myInputStreamReader(connection.getInputStream());
			logger.info(token + "Rest Client: Magento Response: " + responseString);
			return responseString;
		} else if (url.startsWith("https")) {
			HttpsURLConnection ssl = null;
			// HttpURLConnection connection = null;
			URL urlCon = new URL(url);
			ssl = (HttpsURLConnection) urlCon.openConnection();
			ssl.setConnectTimeout(5000);
			ssl.setRequestMethod("POST");
			ssl.setRequestProperty("Content-Type", "application/json");
			// connection.setRequestProperty("credentials","RQ/Rjcf8Gn2+7srAiXNO0ZGuRnJORSir");
			ssl.setRequestProperty("User-Agent", "ESB");
			ssl.setDoOutput(true);
			DataOutputStream writer = new DataOutputStream(ssl.getOutputStream());
			writer.write(input.getBytes(StandardCharsets.UTF_8));
			String responseString = myInputStreamReader(ssl.getInputStream());
			logger.info(token + "Rest Client: Magento Response: " + responseString);
			return responseString;
		}
		return input;
	}

	public static String sendCallToAzerfonLoan(String token, String url) throws Exception {

		url = ConfigurationManager.getConfigurationFromCache("azerfon.loan.baseurl") + url;

		logger.info(token + "Rest Client-Request URL: " + url);

		String responseString = "";

		if (url.startsWith("http") && !url.startsWith("https")) 
		{

			HttpURLConnection connection = null;
			URL urlCon = new URL(url);
			connection = (HttpURLConnection) urlCon.openConnection();
			connection.setConnectTimeout(5000);
			connection.setRequestMethod("GET");
			connection.setDoOutput(true);
			responseString = myInputStreamReader(connection.getInputStream());

		} else if (url.startsWith("https")) {

			HttpsURLConnection ssl = null;
			URL urlCon = new URL(url);
			ssl = (HttpsURLConnection) urlCon.openConnection();
			ssl.setConnectTimeout(5000);
			ssl.setRequestMethod("GET");
			ssl.setDoOutput(true);
			responseString = myInputStreamReader(ssl.getInputStream());
		}

		logger.info(token + "Rest Client-Response-" + responseString);

		return responseString;
	}

	public static String sendCallToAzerfonNarTvManagerToken(String token, String url, String data) throws Exception {

		logger.info(token + "-Rest Client-Request URL-" + url);

		String responseString = "";

		if (url.startsWith("http") && !url.startsWith("https")) {

			HttpURLConnection connection = null;
			URL urlCon = new URL(url);
			connection = (HttpURLConnection) urlCon.openConnection();
			connection.setConnectTimeout(5000);
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			connection.setDoOutput(true);
			DataOutputStream writer = new DataOutputStream(connection.getOutputStream());
			writer.write(data.getBytes(StandardCharsets.UTF_8));
			responseString = myInputStreamReader(connection.getInputStream());
			return responseString;

		} else if (url.startsWith("https")) {

			HttpsURLConnection ssl = null;
			URL urlCon = new URL(url);
			ssl = (HttpsURLConnection) urlCon.openConnection();
			ssl.setConnectTimeout(5000);
			ssl.setRequestMethod("POST");
			ssl.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			ssl.setDoOutput(true);
			DataOutputStream writer = new DataOutputStream(ssl.getOutputStream());
			writer.write(data.getBytes(StandardCharsets.UTF_8));
			responseString = myInputStreamReader(ssl.getInputStream());
			return responseString;
		}

		logger.info(token + "-Rest Client-Response-" + responseString);
		return responseString;
	}

	public static String sendCallToAzerfonNarTvSubscriptions(String token, String paramUrl, String data)
			throws Exception {

		logger.info(token + "-Rest Client-Request URL-" + paramUrl);

		try {
			// Create a trust manager that does not validate certificate chains
			TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
					return null;
				}

				public void checkClientTrusted(X509Certificate[] certs, String authType) {
				}

				public void checkServerTrusted(X509Certificate[] certs, String authType) {
				}
			} };
			// Install the all-trusting trust manager
			final SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
			// Create all-trusting host name verifier
			HostnameVerifier allHostsValid = new HostnameVerifier() {
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			};
			// Install the all-trusting host verifier
			HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

			URL url = new URL(paramUrl);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setRequestProperty("X-Auth-Token", data);
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setDoOutput(true);
			Integer timeout = Integer.parseInt("20000");
			connection.setConnectTimeout(timeout);
			connection.setReadTimeout(timeout);
			if ((connection.getResponseCode() != 200)) {
				BufferedReader bin = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
				StringBuffer sb = new StringBuffer();
				String line;
				while ((line = bin.readLine()) != null) {
					sb.append(line);
				}
				logger.info(token + "-Rest Client-Response-" + sb.toString());
				return sb.toString();
			}
			if ((connection.getResponseCode() == 200)) {
				BufferedReader bin = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				StringBuffer sb = new StringBuffer();
				String line;
				while ((line = bin.readLine()) != null) {
					sb.append(line);
				}
				logger.info(token + "-Rest Client-Response-" + sb.toString());
				return sb.toString();
			}
		} catch (Exception e) {

		}
		return "";
	}

	public static String SendCallEMobile(String hashCode, String input) throws Exception {

		String url = ConfigurationManager.getConfigurationFromCache("ulduzum.subscriber.codegenerate.url") + hashCode;
		logger.info("Rest Client: URL: " + url);
		logger.info("Rest Client: Request: " + input);
		if (url.startsWith("http") && !url.startsWith("https")) {
			HttpURLConnection connection = null;
			URL urlCon = new URL(url);
			connection = (HttpURLConnection) urlCon.openConnection();
			connection.setConnectTimeout(5000);
			connection.setRequestMethod("POST");
//			connection.setRequestProperty("Content-Type", "application/json");
			// connection.setRequestProperty("credentials","RQ/Rjcf8Gn2+7srAiXNO0ZGuRnJORSir");
//			connection.setRequestProperty("User-Agent", "ESB");
			connection.setDoOutput(true);
			DataOutputStream writer = new DataOutputStream(connection.getOutputStream());
			writer.write(input.getBytes(StandardCharsets.UTF_8));
			String responseString = myInputStreamReader(connection.getInputStream());
			logger.info("Rest Client: Response: " + responseString);
			return responseString;
		} else if (url.startsWith("https")) {
			HttpsURLConnection ssl = null;
			// HttpURLConnection connection = null;
			URL urlCon = new URL(url);
			ssl = (HttpsURLConnection) urlCon.openConnection();
			ssl.setConnectTimeout(5000);
			ssl.setRequestMethod("POST");
//			ssl.setRequestProperty("Content-Type", "application/json");
			// connection.setRequestProperty("credentials","RQ/Rjcf8Gn2+7srAiXNO0ZGuRnJORSir");
//			ssl.setRequestProperty("User-Agent", "ESB");
			ssl.setDoOutput(true);
			DataOutputStream writer = new DataOutputStream(ssl.getOutputStream());
			writer.write(input.getBytes(StandardCharsets.UTF_8));
			String responseString = myInputStreamReader(ssl.getInputStream());
			System.out.println("response : " + responseString);
			return responseString;
		}
		return input;
	}

	public static String SendCallToAppserver(String url, String input) throws Exception {
		if (url.startsWith("http") && !url.startsWith("https")) {
			HttpURLConnection connection = null;
			URL urlCon = new URL(url);
			connection = (HttpURLConnection) urlCon.openConnection();
			connection.setConnectTimeout(5000);
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json");
			// connection.setRequestProperty("credentials","RQ/Rjcf8Gn2+7srAiXNO0ZGuRnJORSir");
			connection.setRequestProperty("credentials", "RQ/Rjcf8Gn2+7srAiXNO0ZGuRnJORSir");
			// connection.setRequestProperty("User-Agent", "ESB");
			connection.setDoOutput(true);
			DataOutputStream writer = new DataOutputStream(connection.getOutputStream());
			writer.write(input.getBytes(StandardCharsets.UTF_8));
			String responseString = myInputStreamReader(connection.getInputStream());
			System.out.println("response : " + responseString);
			return responseString;
		} else if (url.startsWith("https")) {
			HttpsURLConnection ssl = null;
			// HttpURLConnection connection = null;
			URL urlCon = new URL(url);
			ssl = (HttpsURLConnection) urlCon.openConnection();
			ssl.setConnectTimeout(5000);
			ssl.setRequestMethod("POST");
			ssl.setRequestProperty("Content-Type", "application/json");
			// connection.setRequestProperty("credentials","RQ/Rjcf8Gn2+7srAiXNO0ZGuRnJORSir");
			// ssl.setRequestProperty("User-Agent", "ESB");
			ssl.setRequestProperty("credentials", "RQ/Rjcf8Gn2+7srAiXNO0ZGuRnJORSir");
			ssl.setDoOutput(true);
			DataOutputStream writer = new DataOutputStream(ssl.getOutputStream());
			writer.write(input.getBytes(StandardCharsets.UTF_8));
			String responseString = myInputStreamReader(ssl.getInputStream());
			System.out.println("response : " + responseString);
			return responseString;
		}
		return input;
	}

	public static String SendCallToUlduzum(String url) {
		logger.info("Rest Client ulduzum: URL & Response: " + url);
		try {
			HttpsURLConnection ssl = null;
			// HttpURLConnection connection = null;
			URL urlCon = new URL(url);
			ssl = (HttpsURLConnection) urlCon.openConnection();
			ssl.setConnectTimeout(5000);
			ssl.setRequestMethod("GET");

			ssl.setDoOutput(true);
			DataOutputStream writer = new DataOutputStream(ssl.getOutputStream());

			String responseString = myInputStreamReader(ssl.getInputStream());
			System.out.println("response : " + responseString);
			return responseString;
		} catch (Exception e) {
			// TODO: handle exception
			logger.info("Exception : " + e.getMessage());
			logger.info(Helper.GetException(e));
		}
		return url;

	}
	
	
	public static String SendCallToB2CEsb(String token, String url, String input) throws Exception {

		logger.info(token + "Rest Client: SendCallToB2CEsb URL: " + url);
		logger.info(token + "Rest Client: SendCallToB2CEsb Request: " + input);
		url = ConfigurationManager.getConfigurationFromCache("magento.app.baseurl") + url;
		logger.info(token + "Rest Client: SendCallToB2CEsb Complete URL: " + url);
		if (url.startsWith("http") && !url.startsWith("https")) {
			HttpURLConnection connection = null;
			URL urlCon = new URL(url);
			connection = (HttpURLConnection) urlCon.openConnection();
			connection.setConnectTimeout(5000);
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json");
			 connection.setRequestProperty("credentials","RQ/Rjcf8Gn2+7srAiXNO0ZGuRnJORSir");
			connection.setRequestProperty("User-Agent", "ESB");
			connection.setDoOutput(true);
			DataOutputStream writer = new DataOutputStream(connection.getOutputStream());
			writer.write(input.getBytes(StandardCharsets.UTF_8));
			String responseString = myInputStreamReader(connection.getInputStream());
			logger.info(token + "Rest Client: SendCallToB2CEsb Response: " + responseString);
			return responseString;
		}
		return input;
	}
	
	
	

	static public String myInputStreamReader(InputStream in) throws IOException {
		StringBuilder sb = new StringBuilder();
		InputStreamReader reader = null;
		try {
			reader = new InputStreamReader(in);
			int c = reader.read();
			while (c != -1) {
				sb.append((char) c);
				c = reader.read();
			}
			reader.close();
			return sb.toString();
		} catch (Exception e) {
			logger.error(Helper.GetException(e));
		} finally {
			if (reader != null)
				reader.close();
		}
		return sb.toString();
	}
}
